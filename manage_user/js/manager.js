// JavaScript Document

function check_username()
{


	//Function to check whether the username exists or not for a particular student user.
	var entered_username = document.getElementById("username").value;

	if(entered_username == "")
	{
		$('#show_availability').html('<img src="images/icon/color_18/cross.png" /><strong style="color:#FF5252">Username field cannot empty</strong>');
	}
	else
	{
	$.ajax({
		type: 'POST',
		
		url: 'manage_user/check_username.php',
		data: {username:entered_username, priv:0},
		success: function(data) {
				$('#show_availability').html(data);
		}
		});
	}
}

function check_admin()
{


	//Function to check whether the username exists or not for a particular student user.
	var entered_username = document.getElementById("username").value;

	if(entered_username == "")
	{
		$('#show_availability').html('<img src="images/icon/color_18/cross.png" /><strong style="color:#FF5252">Username field cannot empty</strong>');
	}
	else
	{
	$.ajax({
		type: 'POST',
		
		url: 'manage_user/check_admin.php',
		data: {username:entered_username, priv:1},
		success: function(data) {
				$('#show_availability').html(data);
		}
		});
	}
}
