<?php
//function to called by employee information details
function employee_info_details()//fill infomation of employee
{	
    //Author By:Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Employee Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	echo'            
	<form id="validation_demo" action="payroll/add_employee_detail.php" method="get">              			 
	<div class="section ">
	<label>Employee Name<small></small></label>   
	<div> 
	<input type="text" class="validate[required] medium" name="emp_name"/>
	</div>
	</div>	
	<div class="section ">
	<label>Department<small></small></label>   
	<div> 
	<input type="text" class="validate[required] medium" name="dept"/>
	</div>
	</div>	
	<div class="section ">
	<label>Post<small></small></label>   
	<div> 
	<input type="text" class="validate[required] medium"  name="post"/>
	</div>
	</div>	
	<div class="section ">
	<label>Employee Salary<small></small></label>   
	<div> 
	<input type="text" class="validate[required] medium" name="emp_salary" />
	</div>
	</div>	
	<div class="section">
	<label>Address <small></small></label>   
	<div > <textarea name="address" id="Textareaelastic"  class="medium"  cols="" rows=""  ></textarea>
	</div>                                              
	</div>
	<div class="section ">
	<label> Email<small></small></label>   
	<div> 
	<input type="text" class="validate[required,custom[email]]  medium" name="email" id="e_required"/>
	</div>
	</div>
	<div class="section ">
	<label>Contact<small></small></label>   
	<div> 
	<input type="text" class="validate[required] medium" name="contact" />
	</div>
	</div>				 	
	<div class="section last">
	<div><button class="uibutton submit" title="Saving" rel="1" type="submit" >submit</button>
	<button class="uibutton special"type="reset"  >Reset</button> 
	</div>			  
	</form>                              
	';
	echo'						
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';										
	}
	
//function to calledby  payroll_view_yearly_employee_salary_details.php
function   yearly_employee_salary_details()//function to select year
{
	//Author By:Manish Mishra
	echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Year & Month</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo' 
	<form action="payroll_view_employee.php" method="get" name="year">
	<div class="section ">
	<label>Select Year<small></small></label>           
	<div >
	<select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" name="year" >        
	<option value=""></option>'; 								
	//query to get year		
	$get_year="
		SELECT DISTINCT `year`
		FROM  `dates_d`
		";
	$execute_year=mysql_query($get_year);
	while($fetch_year=mysql_fetch_array($execute_year))//looping for getting year
		{   
		$year=$fetch_year['year'];
		echo'
		<option value="'.$fetch_year['year'].'">'. $year.'</option>';
		}		   
	echo'
    </select> 
	</div>
	</div> 
	';				
	echo' 
	<div class="section ">
	<label>Select Month<small></small></label>              
	<div >
	<select  data-placeholder="Choose  month..." class="chzn-select" tabindex="2" id="month" name=month>        
	<option value=""></option>'; 								
	//query to get month and month of year		
	$get_month="
		SELECT DISTINCT `month` ,`month_of_year`
		FROM  `dates_d`
		";
	$execute_month=mysql_query($get_month);
	while($fetch_month=mysql_fetch_array($execute_month))//looping for getting month
		{   
		$month=$fetch_month['month'];
		echo'
		<option value="'.$fetch_month[1].'">'. $month.'</option>';
		}		   
	echo'   
	</select> 
	</div> 
	</div> 
	<div class="section last">
	<div><button class="uibutton submit_form" rel="1"  value="submit" >Go</button>
	</div></div>
	</form>  
	';			
	echo'				
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';					
}

// function to called By view details of all employee
function  view_employee()//view employee detyails
{	
    //Author By:Manish Mishra			
	echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                           
	<div class="widget-header">
	<span><i class="icon-align-center"></i> View Employee Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';		
//	if(($_GET['year']=="")&&($_GET['month']==""))
//	{
//	header('location:payroll_view_yaerly_employee_salary_details.php');
//	}
//	if(($_GET['year']==""))
//	{
//	header('location:payroll_view_yaerly_employee_salary_details.php');
//	}
//	if(($_GET['month']==""))
//	{
//	header('location:payroll_view_yaerly_employee_salary_details.php');
//	}


	
	echo' 	                                                                         	 
	<table  class="table table-bordered table-striped" >
	<thead>
	<tr>
	<th>ID</th>
	<th>Name</th>
	<th>Designation</th>
	<th>DOB</th>
	
	<th>Address</th>
	<th>Phone</th>
	<th>Gender</th>
	<th>D.O.J.</th>
	<th>D.O.I.</th>
	<th>Qualification</th>
	<th>Profi. Qualification</th>
	<th>Nature of Job</th>
	<th>BASIC PAY</th>
	<th>GRADE PAY</th>
	<th>DA</th>
	<th>HRA</th>
	<th>TOTAL</th>
	<th>PF</th>
	<th>ESI</th>
	<th>NET SALARY</th>
	<th>Action</th>
							
	</tr>
	</thead>	 	
	<tbody align="center"> 
	';	$sn=1;
		$data="SELECT * FROM payroll_emp ";
	$exe_data=mysql_query($data);
	while($fetch_data=mysql_fetch_array($exe_data))
	{
	
		$desi="SELECT * FROM `salary_calculator` WHERE id=".$fetch_data[14]."";
		$exe_desi=mysql_query($desi);
		$fetch_desi=mysql_fetch_array($exe_desi);
		$designation = $fetch_desi[1];
		echo '<tr>
		<th>'.$sn++.'</th>
		<th><a href="payroll/print_employee_salary_structure2.php?id='.$fetch_data[0].'">'.$fetch_data[1].'</a></th>
		<th>'.$designation.'</th>
		<th>'.$fetch_data[3].'</th>
		
		<th>'.$fetch_data[5].'</th>
		<th>'.$fetch_data[6].'</th>
		<th>'.$fetch_data[7].'</th>
		<th>'.$fetch_data[9].'</th>
		<th>'.$fetch_data[10].'</th>
		<th>'.$fetch_data[11].'</th>
		<th>'.$fetch_data[12].'</th>
		<th>'.$fetch_data[13].'</th>
		<th>'.$fetch_data[19].'</th>
		
		<th>'.$fetch_desi[2].'</th>
		<th>'.$fetch_data[17].'</th>
		<th>'.$fetch_data[15].'</th>
		<th>TOTAL</th>
		<th>'.$fetch_data[16].'</th>
		<th>'.$fetch_data[18].'</th>
		<th>NET SALARY</th>
		<th>
			<span>
				<a href="payroll_add_new_emp1.php?id='.$fetch_data[0].'">
					<img src="images/icon/icon_edit.png" />
				</a>
			</span>
			<span class="tip">
		<a href="payroll/delete_emp.php?id='.$fetch_data[0].'" 
		class="table-icon delete" title="Delete">
		<img src="images/icon/icon_delete.png"></a>
		</span>
		</th>
		
		</tr>';
	
	
	}
	
				 					                   
	echo'		
	</tbody>
	</table>			
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';														
	}
	// function to called By view details of all employee
function  view_employee22()//view employee detyails
{	
    //Author By:Manish Mishra			
	echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                           
	<div class="widget-header">
	<span><i class="icon-align-center"></i> View Employee Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';		
//	if(($_GET['year']=="")&&($_GET['month']==""))
//	{
//	header('location:payroll_view_yaerly_employee_salary_details.php');
//	}
//	if(($_GET['year']==""))
//	{
//	header('location:payroll_view_yaerly_employee_salary_details.php');
//	}
//	if(($_GET['month']==""))
//	{
//	header('location:payroll_view_yaerly_employee_salary_details.php');
//	}


	$priv=$_SESSION['user_id'];
	 $data1="SELECT * FROM users WHERE uId=".$priv."";
	$exe_d1=mysql_query($data1);
	$fetch_d1=mysql_fetch_array($exe_d1);
	echo' 	                                                                         	 
	<table  class="table table-bordered table-striped" >
	<thead>
	<tr>
	
	<th>Name</th>
	<th>Designation</th>
	<th>DOB</th>
	<th>Email</th>
	<th>Address</th>
	<th>Phone</th>
	<th>Gender</th>
	
	<th>Qualification</th>
	<th>Profi. Qualification</th>
	<th>Nature of Job</th>
	<th>BASIC PAY</th>

	
	<th>Action</th>
							
	</tr>
	</thead>	 	
	<tbody align="center"> 
	';	$sn=1;
		 $data="SELECT * FROM payroll_emp WHERE Name LIKE '%".$fetch_d1[1]."%';";
	$exe_data=mysql_query($data);
	while($fetch_data=mysql_fetch_array($exe_data))
	{
	
		$desi="SELECT * FROM `salary_calculator` WHERE id=".$fetch_data[14]."";
		$exe_desi=mysql_query($desi);
		$fetch_desi=mysql_fetch_array($exe_desi);
		$designation = $fetch_desi[1];
		echo '<tr>
		<th>'.$sn++.'</th>
		<th><a href="payroll/print_employee_salary_structure2.php?id='.$fetch_data[0].'">'.$fetch_data[1].'</a></th>
		<th>'.$designation.'</th>
		<th>'.$fetch_data[3].'</th>
		<th>'.$fetch_data[4].'</th>
		<th>'.$fetch_data[5].'</th>
		<th>'.$fetch_data[6].'</th>
		<th>'.$fetch_data[7].'</th>
		
		<th>'.$fetch_data[11].'</th>
		<th>'.$fetch_data[12].'</th>
		<th>'.$fetch_data[13].'</th>
		<th>'.$fetch_data[19].'</th>
		

	
		<th>

			<span>
				<a href="payroll_add_new_emp1.php?id='.$fetch_data[0].'">
					<img src="images/icon/icon_edit.png" />
				</a>

			</span>
			
		</th>
		
		</tr>';
	
	
	}
	
				 					                   
	echo'		
	</tbody>
	</table>			

	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->

	';														
	}

//function to called by  employee salary
function employee_salary_details()//add designation and salary
{	
    //Author By:Manish Mishra		
	echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Employee Salary Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	
	echo' 		                                                                         	 
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>		   
	<th>Emp Id</th>
	<th>Name</th>
	<th>Designation</th>
	<th>Net Salary(Rs.)</th>
	<th>Contact</th>					
	</tr>
	</thead>	 	
	<tbody align="center">  ';
	$pf="";
	$sal_net=0;
	$year=$_GET['year'];
	$month=$_GET['month'];
	$id=$_GET['id'];	
	$net=array();
	
	//query to gat date
	$get_date="
		SELECT `year`,`emp_id`,`month_of_year`
		FROM `employee_salary_details` 
		WHERE `year`=".$year." AND `month_of_year`=".$month."";
	$exe_date=mysql_query($get_date);
	while($fetch_date=mysql_fetch_array($exe_date))
		{    
	
		//get name fom users
		echo $get_name=
		"SELECT  DISTINCT *    FROM employee_table   WHERE  e_id NOT IN (SELECT emp_id FROM  employee_deleted_table)
                                                                                         where e_id = ".$id."                                                       
                                                                                      ORDER BY  ord_post+0                                                               
		";
		$exe_name=mysql_query($get_name);
		$fetch_name=mysql_fetch_array($exe_name);
		$name=$fetch_name[0];
		
		$emp_id=$fetch_date['emp_id'];
	
		//query to gate name ,id
		$get_emp_name=
			"SELECT DISTINCT *
			FROM employee_salary_details
			WHERE `emp_id`=".$id." AND `year`=".$year." AND `month_of_year`=".$month." 
			";
		$exe_name=mysql_query($get_emp_name); 
		
		$fetch_name=mysql_fetch_array($exe_name);
		$uId=$emp_id;
		$pf=$fetch_name['PF'];
		//get the deduction on the id
		$get_deduction="
			SELECT SUM(amount)
			FROM employee_deduction_details
			WHERE emp_id =".$id." AND year=".$year."";
		$exe_deduction=mysql_query($get_deduction);
		$fetch_deduction=mysql_fetch_array($exe_deduction);
		$ded_sum=$fetch_deduction[0];
		$sal_net=$fetch_name['net_salary']-$ded_sum;
		$net_pf=$fetch_name['net_salary']-$pf;
		$sal_net=$net_pf-($ded_sum+$pf);//NET SALARY WITHOUT DEDUCTION
		}
	//query to select to uid from users
	//query to select employee salary details
	$get_emp_name="
		SELECT  DISTINCT *    FROM employee_table   WHERE  e_id NOT IN (SELECT emp_id FROM  employee_deleted_table)                                                            
		where  e_id = =".$_GET['id']." ";
	$exe_name=mysql_query($get_emp_name); 
	while($fetch_name=mysql_fetch_array($exe_name))   
		{
		$get_salary="
			SELECT `basic_salary`,`net_salary`
			FROM `employee_salary_details` 
			WHERE `emp_id`=".$_GET['id']." AND year=".$year." AND month_of_year=".$month."";	               
		$exe_salary=mysql_query($get_salary);
		$fetch_result=mysql_fetch_array($exe_salary);
		//query to inner jon to designation and users_designation_mapping table
		
		echo' <tr>
		<td>'.$_GET['id'].'</td>
		<td>'.$fetch_name['e_name'].'</td>
		<td>'.$fetch_desig[0].'</td>
		<td><a href="payroll_view_total_net_employee_salary_details.php?name='.$name.'&&id='.$_GET['id'].'&&year='.$_GET['year'].'&&month='.$month.'" style="cursor:pointer">'.$sal_net.'</td>       
		<td>'.$fetch_name['Phone No'].'</td>					
		</tr>';	
	}
	//<td><a href="payroll_employee_net_salary.php?id='.$_GET['id'].'&&year='.$_GET['year'].'&&month='.$month.'" style="cursor:pointer">'.$fetch_result['net_salary'].'</a>      </td>				
	echo'
	</tbody>
	</table>						
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';										
}
//function to called by payroll_view_total_net_employee_salary_details.php
function employee_total_net_salary_details()//employee salary details
{     
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Employee Salary</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
        
        
	$pf="";
	$deduction_cal=0;
	$name=$_GET['name'];
	$year=$_GET['year'];
	$month=$_GET['month'];
	$total_salary=0;
	$total_measure=0;
	//query to get month and year
	$date="
		SELECT `month`,`year`,month_of_year
		FROM `dates_d`
		WHERE `month_of_year`=".$month."";
	$exe_date=mysql_query($date);
	$fetch_date=mysql_fetch_array($exe_date);
	$month_name=$fetch_date['month'];	
	
	//view details of `employee_salary_details` table 						
	$view_salary_calculate=" 
		SELECT DISTINCT * 
		FROM `employee_salary_details`
		WHERE `emp_id`=".$_GET['id']." AND `year`=".$year." AND `month_of_year`=".$month."
		";
	$view_result=mysql_query($view_salary_calculate);
	$fetch_result=mysql_fetch_array($view_result) ;   
	$status=$fetch_result['status'];
        $e_id=$_GET['id'];
           echo' <a href="payroll/payroll_employee_salary_pay_slip.php?year='.$year.'&month='.$month.'&name='.$name.'&e_id='.$e_id.'"><button class="uibutton normal">Print</button></a>';
        echo'<td></td>';
        
        
	echo '<h5 style="color:brown">Emp Id:  '.$fetch_result['emp_id'].'</h5>
	<h5 style="color:brown">Name:  '.$name.'</h5>
	<h5 style="color:brown" align="center"> '.$year.'   '.$month_name.'</h5>		
	';
	if($fetch_result['basic_salary']==0)
		{
			echo '<h5 style="color:red" align="center"> BASIC SALARY NOT APPROVED THIS MONTH</h5>';	
		}
		else
		{
			echo' 	                                                                         	 
			<table  class="table table-bordered table-striped" id="dataTable" >
			<thead>
			<tr>   
			';
			//query to get measure details
			$get_mea="
				SELECT  DISTINCT measures
				FROM salary_calculator
				ORDER BY id DESC";
			$exe_mea_name=mysql_query($get_mea);
			$k=0;
			$mea=array();
			while($fetch_mea_name=mysql_fetch_array($exe_mea_name))
				{
				$mea[$k]=$fetch_mea_name['measures'];
				echo '';	
				$k++;
				}
			echo '	
			</tr>
			</thead>	 	
			<tbody align="left"> 
			';	
			echo'<tr style="color:green"><td><b>Earnings</td>
			<td align="center"><b>Amount</td></tr>
			<tr align="center"> <th>Basic Salary</th>
			<td>'.$fetch_result['basic_salary'].'</td></tr></tr>	
			';
			
			$q=3;
			for($i=0;$i<$k;$i++)//loop fo getting measures
				{
				if($fetch_result[$q]==0)//loop for geeting measures values
				{
				}
				else if($mea[$i]=='PF')
					{
					$pf=$fetch_result[$q];
					}
					else
					{
					echo '
					<tr align="center"><th>'.$mea[$i].'</th>
					<td>'.$fetch_result[$q].'</td>
					';
				$total_measure=$total_measure+$fetch_result[$q];  //total measure calculate
					 }
				++$q;
				
				}
				$total_salary=$total_salary+$fetch_result['basic_salary'];
				$total_earning=$total_salary+$total_measure;   //total earning
			echo '<tr align="center"><td><b>Total Salary</td>
			<td><b>Rs.'.$total_earning.'</td></tr>
			<tr style="color:green"><td><b>Deductions</td>
			<td></td></tr> ';
			//query to select deduction details
			$get_deduction_details="
				SELECT * 
				FROM `employee_deduction_details`
				WHERE `emp_id`=".$_GET['id']." AND `year`=".$year."";
			$exe_deducation_details=mysql_query($get_deduction_details);
			while($fetch_deduction_details=mysql_fetch_array($exe_deducation_details))
				{
				$deduction=$fetch_deduction_details['amount'];
				$deduction_did=$fetch_deduction_details['did'];
				//query to get deduction type
				$get_deduction_type="
					SELECT *
					FROM `finance_deduction_details`
					WHERE `id`=".$deduction_did." AND `year`=".$year."";
				$exe_deduction_type=mysql_query($get_deduction_type);
				while($fetch_deduction_type=mysql_fetch_array($exe_deduction_type))
					{  
					echo'<tr align="center">
					<td><b>'.$deduction_type=$fetch_deduction_type['deduction_type'].'</td>
					<td>'.$deduction.'</td>	
					</tr>				
					'; 	
					$deduction_cal=$deduction_cal+$deduction;//deduction cal
					}     
			}//end of while
			$deduction_cal+=$pf;
			$total_net_salary=$total_earning-$deduction_cal;//calculate tot net sal
			echo'
			<tr align="center"><td><b>PF</td><td>'.$pf.'</td></tr>
			<tr align="center"><td><b>Deductions Amount</td>
			<td><b>Rs.'.$deduction_cal.'</td></tr>
			<tr style="color:green"><td><b>Total</td>
			<td></td></tr> 
			<tr align="center"><td><h5>Net Salary</td>
			<td><b>Rs.'.$total_net_salary.'</td></tr>	
			';	  		  	  
			echo'
			</tbody>
			</table>';
			}
			echo'				
			</div><!--  end widget-content -->
			</div><!-- widget  span12 clearfix-->
			</div><!-- row-fluid -->
			';
		
}

//function to called by calculatee salary hra ta da
function salary_calculator()//enter the measure details
{	
	/*//Author By:Manish Mishra					
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                           
	<div class="widget-header">
	<span><i class="icon-align-center"></i>calculate salary</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br/>
	';	
	$get_count_sal_calc=
	"SELECT * FROM salary_calculator";
	$exe_sal_calc=mysql_query($get_count_sal_calc);
	
	
	if(mysql_num_rows($exe_sal_calc))
	{
	
	echo '<form id="validation_demo" action="payroll/add_salary_calculator.php" method="get">              		 		
	<div class="section regexonly">
	<label>HRA<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] medium" name="hra" disabled="disabled">
	</div>
	</div>																		
	<div class="section regexonly">
	<label>TA<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] medium" name="ta" disabled="disabled">
	</div>
	</div>																			
	<div class="section regexonly">
	<label>DA<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] medium" name="da" disabled="disabled">
	</div>
	</div>																				 					
	<div class="section regexonly">
	<label>PF<small></small></label>   
	<div> 
	<input  type="text"  class="validate[required] medium" name="pf" disabled="disabled">
	</div>
	</div>					
	<div class="section regexonly">
	<label>CCA<small></small></label>   
	<div> 
	<input  type="text"  class="validate[required] medium" name="cca" disabled="disabled">
	</div>
	</div>					
	<div class="section regexonly">
	<label>TDS<small></small></label>   
	<div> 
	<input  type="text"  class="validate[required] medium" name="tds" disabled="disabled">
	</div>
	</div>		
	<div class="section last">
	
	<a href="payroll_view_salary_calculator.php"class="uibutton confirm" >View</a>
	</div>																					  
	
	</form>';	
	}
	
	else
	{
	
	echo'	
	<form id="validation_demo" action="payroll/add_salary_calculator.php" method="get">              		 		
	<div class="section regexonly">
	<label>HRA<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] medium" name="hra">
	</div>
	</div>																		
	<div class="section regexonly">
	<label>TA<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] medium" name="ta">
	</div>
	</div>																			
	<div class="section regexonly">
	<label>DA<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] medium" name="da">
	</div>
	</div>																				 					
	<div class="section regexonly">
	<label>PF<small></small></label>   
	<div> 
	<input  type="text"  class="validate[required] medium" name="pf">
	</div>
	</div>					
	<div class="section regexonly">
	<label>CCA<small></small></label>   
	<div> 
	<input  type="text"  class="validate[required] medium" name="cca">
	</div>
	</div>					
	<div class="section regexonly">
	<label>TDS<small></small></label>   
	<div> 
	<input  type="text"  class="validate[required] medium" name="tds">
	</div>
	</div>																							  
	<div class="section last">
	<div><button class="uibutton submit"  rel="1" type="submit" >submit</button>
	<button class="uibutton special"type="reset" >Reset</button> 
	
	
	</div>	
	</div>
	</form>
	';		
	}
	echo'				
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';*/											
	}

//function to called by  view to salary caculator hra ta da
function view_salary_calculator()//view measure details
{		
	//Author by:Manish Mishra					
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Calculate Salary</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	date_default_timezone_set('Asia/Kolkata');
            $date=date('d-m-Y');
            
             $flag_dates=0;
             $year=date('Y-m-d',strtotime($date));
	$year=$_GET['year'];
	echo' <h5 style="color:brown" align="center">'.$year.'</h5>';						
	if(isset($_GET['edited']))
	{
	echo "<h5 style=\"color:green\" align=\"center\">Edit Successfully</h5>";
	}
	echo' 		                                                                         	 
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>		   
	<th>DESIGNATION</th>
	<th>GRADE</th>
	<th>ACTION</th>						
	</tr>
	</thead>	 	
	<tbody align="center">  ';
	//view details of salary_calculate table 						
	$view_salary_calculate="
		SELECT * 
		FROM `salary_calculator`
		 ";
	$view_result=mysql_query($view_salary_calculate);
	while($fetch_result=mysql_fetch_array($view_result))    
		{
		echo' <tr id="'.$fetch_result[0].'">		
		<td>'.$fetch_result['measures'].'</td>
		<td>'.$fetch_result['value'].'</td>
		<td id="change_'.$fetch_result[0].'">
		<a href="#" class="table-icon edit" title="Edit"
		 onclick="edit_calc(\''.$fetch_result['measures'].'\','.$fetch_result[0].','.$fetch_result['value'].','.$year.')">
		<img src="images/icon/icon_edit.png"></a>
		<span class="tip">
		<a href="payroll/delete_salary_calculator.php?id='.$fetch_result[0].'&&year='.$year.'" 
		class="table-icon delete" title="Delete">
		<img src="images/icon/icon_delete.png"></a>
		</span>
		</td>
		</tr>						  
		';  
	   } //end of while                                                   			 				
	echo'
	</tbody>
	</table>	
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';											
}

//function to called by  edit salary_calculator
 function view_edit_salary_calculator()//edit salary details
{		
	/*//Author by:Manish Mishra					
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>calculate salary</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">
	
	';	
	//view details of salary_calculate table		
	$view_salary_calculate=" SELECT * 
	FROM `salary_calculator`";
	$view_result=mysql_query($view_salary_calculate);
	$fetch_result=mysql_fetch_array($view_result);			 	
	echo'	
	<form id="validation_demo" action="payroll/edit_salary_calculator.php" method="get">             
	
	<div class="section regexonly">
	<label>HRA<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] medium" name="hra" value="'.$fetch_result[2].'"> 
	</div>
	</div>
	<div class="section regexonly">
	<label>TA<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] medium" name="ta" value="'.$fetch_result[2].'">
	</div>
	</div>																							
	<div class="section regexonly">
	<label>DA<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] medium" name="da"value="'.$fetch_result[2].'">
	</div>
	</div>																				 
	<div class="section regexonly">
	<label>PF<small></small></label>   
	<div> 
	<input  type="text"  class="validate[required] medium" name="pf" value="'.$fetch_result[2].'">
	</div>
	</div>					
	<div class="section regexonly">
	<label>CCA<small></small></label>   
	<div> 
	<input  type="text"  class="validate[required] medium" name="cca"value="'.$fetch_result[2].'">
	</div>
	</div>
	
	<div class="section regexonly">
	<label>TDS<small></small></label>   
	<div> 
	<input  type="text"  class="validate[required] medium" name="tds"value="'.$fetch_result[2].'">
	</div>
	</div>																							 
	<div class="section last">			 	                 
	<div><button class="uibutton submit" rel="1" type="submit">submit</button>
	
	</div>
	</div>
	</form>
	';						
	echo'				
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';						*/						
}
	
//function to called by payroll_select_year_month.php
function  payroll_year_month()//select year
{    
	//Author by:Manish Mishra		
	echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Year & Month</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo' 
	<form action="payroll_salary_details.php" method="get" name="year">
	<div class="section ">
	<label>Select Year<small></small></label>           
	<div >
	<select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" name="year" >        
	<option value=""></option>'; 								
	//query to get year		
	$get_year="
		SELECT DISTINCT `year`
		FROM  `dates_d`
		";
	$execute_year=mysql_query($get_year);
	while($fetch_year=mysql_fetch_array($execute_year))//looping for getting year
		{   
		$year=$fetch_year['year'];
		echo'
		<option value="'.$fetch_year['year'].'">'. $year.'</option>';
		}
	echo'
	</select> 
	</div>
	</div> 
	';				
	echo' 
	<div class="section ">
	<label>Select Month<small></small></label>              
	<div >
	<select  data-placeholder="Choose  month..." class="chzn-select" tabindex="2" id="month" name=month>        
	<option value=""></option>'; 								
	//query to get month and month of year		
	$get_month="
		SELECT DISTINCT `month` ,`month_of_year`
		FROM  `dates_d`
		";
	$execute_month=mysql_query($get_month);
	while($fetch_month=mysql_fetch_array($execute_month))//looping for getting month
		{   
		$month=$fetch_month['month'];
		echo'
		<option value="'.$fetch_month[1].'">'. $month.'</option>';
		}
	echo'
	</select> 
	</div> 
	</div> 
	<div class="section last">
	<div><button class="uibutton submit_form" rel="1"  value="submit" >Go</button>
	</div></div>
	</form>  
	';			
	echo'				
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';															
	}	
//function to show salary details	
function salary_details()
{	
	//Author by:Manish Mishra										
	echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Calculate Salary</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';	
	if(($_GET['year']=="")&&($_GET['month']==""))
	{
	header('location:payroll_select_year_month.php');
	}
	if(($_GET['year']==""))
	{
	header('location:payroll_select_year_month.php');
	}
	if(($_GET['month']==""))
	{
	header('location:payroll_select_year_month.php');
	}
	if(isset($_GET['added']))
	{
	echo "<h5 style=\"color:green\" align=\"center\">Added Successfully</h5>";
	}
	if(isset($_GET['exit']))
	{
	echo "<h5 style=\"color:red\" align=\"center\">Employee Salary Already Exists</h5>";
	}
	$year=$_GET['year'];
	$month=$_GET['month'];	
        
        echo'<h5  align="center">    Month:'.$month.'     Year :'.$year.' </h5>';
//        echo' 		                                                                         	 
//	<table  class="table table-bordered table-striped" id="dataTable" >
//	<thead>
//	<tr><th>S.No.</th>		   
//	<th>Name</th>
//	<th>Post</th>
//	<th>Basic</th>	
//                         <th>Cheque No.</th>
//                       
//	</tr>
//	</thead>	 	
//	<tbody align="center">  ';
//           $get_salary="SELECT   employee_table.*    FROM employee_table  
//                                                                                        INNER JOIN employee_deleted_table
//                                                                                        ON employee_deleted_table.emp_id<>employee_table.e_id
//                                                                     ORDER BY employee_table.e_id+0  ";
//                                                             $exe_get_salary=mysql_query($get_salary);
//                                                            while( $fetch_salary=mysql_fetch_array($exe_get_salary))
//                                                            {   $emp_id=$fetch_salary[0];
//                                                             $name=$fetch_salary[1];
//                                                              $post=$fetch_salary[2];
//                                                               $qualification=$fetch_salary[3];
//                                                                $dob=$fetch_salary[4];
//                                                                 $doa=$fetch_salary[5];
//                                                                  $doc=$fetch_salary[6];
//                                                                 $noc=$fetch_salary[7];
//                                                                      $srno++;
//                                                                 echo'<tr><td>'.$srno.'</td>
//                                                                 <td>'.$name.'</td>
//                                                                     <td>'.$post.'</td>
//                                                                        ';
//     echo'<td width=10% rowspan=""><input type="text"  style="width:75px;color:green;" name="salary" id="salary"  onblur="add_salary('.$emp_id.','.$year.','.$month.');"></td>';
//                                                             
//     echo'<td width=10% rowspan=""><input type="text"  style="width:75px;color:green;" name="check_no"  id="check_no" onblur="add_salary('.$emp_id.','.$year.','.$month.');" ></td>';
//
//                                                            }
//        
//        echo'</tbody></table>';
//        
//        echo' <td><input type="text" width="5%"  name="salary" id="salary" style="width:100px;color:green;border-color:green;"  onblur="add_salary('.$id.','.$year.','.$month.') "></td>
//                                                                             <td><input type="text"  name="check_no"   id="check_no" style="width:100px;color:green;border-color:green;"  onblur=add_salary('.$id.','.$year.','.$month.')></td></tr>';
//        
        
	echo'	
	<form id="validation_demo" action="payroll/add_employee_salary_details.php " method="get"> 	
	<input type="hidden" name="year" value="'.$year.'"	/>
	<input type="hidden" name="month" value="'.$month.'"/> 
	<div class="section">
	<label>Employee Name<small></small></label>   
	<div>
	<select data-placeholder="Select a Employee..." class="chzn-select" tabindex="2" name="uId">
	<option value=""></option>
	';		  
	//get the employee name from users table
	$get_user_name_query="
		SELECT  DISTINCT *    FROM employee_table   WHERE  e_id NOT IN (SELECT emp_id FROM  employee_deleted_table)                                       
                                                                                      ORDER BY e_id+0  
		";
	$get_name_query_res=mysql_query($get_user_name_query);
	while($fetch_name=mysql_fetch_array($get_name_query_res))
		{
		$name=$fetch_name['e_name'];
		$uid=$fetch_name['e_id'];
		echo '
		<option value="'.$uid.'">'.$name.'</option>';
		}		
	echo'</select>       
	</div>
	</div>         	   
	<div class="section ">
	<label>Basic Salary(Rs.)<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] medium" name="basic_salary">
	</div>
	</div>	
          <div class="section ">
	<label>PF.<small></small></label>   
	<div> 
	<input type="text"  class=" medium" name="pf_emp">
	</div>
	</div>	
                            <div class="section ">
	<label>Check No.<small></small></label>   
	<div> 
	<input type="text"  class=" medium" name="check_no">
	</div>
	</div>	
        
	<div class="section last">
	<div><button class="uibutton submit" rel="1" type="submit"  >Save</button>
	<button class="uibutton special"type="reset"  >Reset</button> 
	</div>
	</div>											
	';					
	echo'				
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';											
}

//function to view net salary of employee		
function view_net_salary()
{ 
	//Author by:Manish Mishra		
	echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Net Salary Detailas</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';								
	$year=$_GET['year'];
	$month=$_GET['month'];
	echo' 	                                                                         	 
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th>Emp ID</th>
	';
	$get_mea="
		SELECT *
		FROM salary_calculator 
		WHERE year=".$year."
		ORDER BY id DESC";
	$exe_mea_name=mysql_query($get_mea);
	$k=0;
	while($fetch_mea_name=mysql_fetch_array($exe_mea_name))
		{
		echo '<th>'.$fetch_mea_name['measures'].'(%)</th>';	
		$k++;
		}
	echo '	
	<th>TAX</th>							
	</tr>
	</thead>	 	
	<tbody align="center"> 
	';		
	//view details of `employee_salary_details` table 						
	$view_salary_calculate="
		SELECT * 
		FROM `employee_salary_details`
		WHERE `emp_id`=".$_GET['id']." AND year=".$year."
		";
	$view_result=mysql_query($view_salary_calculate);
	$fetch_result=mysql_fetch_array($view_result) ;   
	echo' <tr>
	<td>'.$fetch_result['emp_id'].'</td>	
	';
	$q=3;
	for($i=0;$i<$k;$i++)//calculate measures
		{ 
		echo '
		<td>'.$fetch_result[$q].'</td>
		';
		$q++;
		}
	echo '<td>'.$fetch_result['TAX'].'</td>
	</tr>
	';	  		  	  
	echo'
	</tbody>
	</table>			
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';							
}
	
//function to called by call payroll_session_measure.php
function  payroll_session_measure()//select year
{  
	//Author by:Manish Mishra		
	echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Session</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';	
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo'
	<form action="payroll_add_salary_measure.php" method="get" name="session" >	
	<div class="section ">
	<label>Select Year<small></small></label>           
	<div >
	<select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" name="year" >        
	<option value=""></option>'; 								
	//query to get year		
	$get_year="
		SELECT DISTINCT `year`
		FROM  `dates_d`
		";
	$execute_year=mysql_query($get_year);
	while($fetch_year=mysql_fetch_array($execute_year))//looping for getting year
		{   
		$year=$fetch_year['year'];
		echo'
		<option value="'.$fetch_year['year'].'">'. $year.'</option>';
		}
	echo'
	</select> 
	</div>
	</div> ';
	echo'       								                           	       				
	<br>		   																
	<div class="section last" align="center">
	<button  class="uibutton submit_form" >Go</button>
	</div>
	</form>';
	echo'				
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';							
}		
//function to add_salary_measure
function add_salary_measure()
{
	//Author by:Manish Mishra				
	echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Add Salary Measure</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';	
	$year=2015;
	//$year=$_GET['year'];
	if($_GET['year']=="")
	{
	header('location:payroll_session_measure.php');
	}
	if(isset($_GET['error']))
	{
	echo "<h5 style=\"color:red\" align=\"center\"> Already Exists, Please try again</h5>";
	}
	if(isset($_GET['Measure_Inserted']))
	{
	echo "<h5 style=\"color:green\" align=\"center\"> Measure Inserted</h5>";
	}		
	echo'	
	<form id="validation_demo" action="payroll/add_salary_calculator.php" method="get"> 
	
	<div class="section ">
	<label>DESIGNATION<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] medium" name="measure">
	</div>
	</div>																		
	<div class="section regexonly">
	<label>GRADE<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] medium" name="value">
	</div>
	</div>																																																							
	<div class="section last">
	<div><button class="uibutton submit"  rel="1" type="submit" >submit</button>			 
	<button class="uibutton special"type="reset" >Reset</button> 				          		  
	<button class="uibutton confirm" type="button" ><a href="payroll_view_salary_calculator.php?year='.$_GET['year'].'">
	View</button></a>
	</div>	
	</div>
	</form>
	';										
	echo'
	</tbody>
	</table>			
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';							
}

//function to called by payroll_teacher_year_month.php
function  teacher_year_month()
{
//Author by:Manish Mishra		
	echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Year & Month</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo' 
	<form action="teacher_view_salary.php" method="get" name="year">
	<div class="section ">
	<label>Select Year<small></small></label>           
	<div >
	<select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" name="year" >        
	<option value=""></option>'; 								
	//query to get year		
	$get_year="
		SELECT DISTINCT `year`
		FROM  `dates_d`
		";
	$execute_year=mysql_query($get_year);
	while($fetch_year=mysql_fetch_array($execute_year))//looping for getting year
		{   
		$year=$fetch_year['year'];
		echo'
		<option value="'.$fetch_year['year'].'">'. $year.'</option>';
		}
	echo'
	</select> 
	</div>
	</div> 
	';				
	echo' 
	<div class="section ">
	<label>Select Month<small></small></label>              
	<div >
	<select  data-placeholder="Choose  month..." class="chzn-select" tabindex="2" id="month" name=month>        
	<option value=""></option>'; 								
	//query to get month and month of year		
	$get_month="
		SELECT DISTINCT `month` ,`month_of_year`
		FROM  `dates_d`
		";
	$execute_month=mysql_query($get_month);
	while($fetch_month=mysql_fetch_array($execute_month))//looping for getting month
		{   
		$month=$fetch_month['month'];
		echo'
		<option value="'.$fetch_month[1].'">'. $month.'</option>';
		}
	echo'
	</select> 
	</div> 
	</div> 
	<div class="section last">
	<div><button class="uibutton submit_form" rel="1"  value="submit" >Go</button>
	</div></div>
	</form>  
	';			
	echo'				
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';															
	
	
	
	
	
	
}
//teacher view own saLARY
function teacher_view_salary()
{  
	//Author by:Manish Mishra		
	echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Salary </span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';
	$year=$_GET['year'];
	$month=$_GET['month'];
	echo' 	                                                                         	 
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th>ID</th>
	<th>Name</th>
	<th>Date of Birth</th>
	
								
	</tr>
	</thead>	 	
	<tbody align="center"> 
	';	
	//select details from user table					
	 $view_employee="
		SELECT  DISTINCT *    FROM employee_table   WHERE  e_id NOT IN (SELECT emp_id FROM  employee_deleted_table)
                                                                                                                                                
                                                                                      ORDER BY  ord_post+0 
		";
	$view_result=mysql_query($view_employee);
	while($fetch_result=mysql_fetch_array($view_result))   
		{  
		 if($_SESSION['user_id']==$fetch_result['uId'])
			{
				echo' <tr>
			<td>'.$fetch_result['e_id'].'</td>
			<td><a href="payroll_employee_salary_details.php?id='.$fetch_result['uId'].'&year='.$year.'&month='.$month.'" style="cursor:pointer">
			'.$fetch_result['e_name'].'</a></td>
			<td>'.$fetch_result['DOB'].'</td>
			
			
			</tr>		
			';		                                                             
			}				 			
	    }//end of while
	echo'	
	</tbody>
	</table>			
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';							
}





//////salary structure 
function employee_yearly_salary_structure()
{
 //Author by:Manish Mishra		
	echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Year</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';	
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo'
	<form action="payroll_view_salary_structure.php" method="get" >	
	<div class="section ">
	<label>Select Year<small></small></label>           
	<div >
	<select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2  id="year"   name="year" >        
	<option value=""></option>'; 								
	//query to get year		
	$get_year="
		SELECT DISTINCT `year`
		FROM  `dates_d`
		";
	$execute_year=mysql_query($get_year);
	while($fetch_year=mysql_fetch_array($execute_year))//looping for getting year
		{   
		$year=$fetch_year['year'];
		echo'
		<option value="'.$fetch_year['year'].'">'. $year.'</option>';
		}
	echo'
	</select> 
	</div>
	</div> 
	';				
	echo' 
	<div class="section ">
	<label>Select Month<small></small></label>              
	<div >
	<select  data-placeholder="Choose  month..." class="chzn-select" tabindex="2" id="month" name=month>        
	<option value=""></option>'; 								
	//query to get month and month of year		
	$get_month="
		SELECT DISTINCT `month` ,`month_of_year`
		FROM  `dates_d`
		";
	$execute_month=mysql_query($get_month);
	while($fetch_month=mysql_fetch_array($execute_month))//looping for getting month
		{   
		$month=$fetch_month['month'];
		echo'
		<option value="'.$fetch_month[1].'">'. $month.'</option>';
		}
	echo'
	</select> 
	</div> 
	</div> 
	<div class="section last">
	<div><button class="uibutton submit_form" rel="1"  value="submit" >Go</button>
	</div></div>
	</form>  
	';			
	echo'				
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';   
   
}


function view_salary_structure()
{
    
  echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Salary Structure</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';	  
    
    $year=$_GET['year'];
    $month=$_GET['month'];
    //echo' <a href="payroll/payroll_employee_salary_structure.php?year='.$year.'&month='.$month.'"><button class="uibutton normal">Print before Sig.</button></a>';
    //  echo' <a href="payroll/payroll_employee_salary_after_structure.php?year='.$year.'&month='.$month.'"><button class="uibutton normal">Print After Sig.</button></a>';
       echo'<a href="payroll/print_employee_salary_structure.php?year='.$year.'&month='.$month.'"><button class="uibutton confirm" type="button">Print Simple</button></a>';
       
         echo'<a href="payroll/print_employee_salary_structure1.php?year='.$year.'&month='.$month.'"><button class="uibutton confirm" type="button">Print Signature</button></a>';
      
    echo'<h4 align="center" ">YEAR:'.$year.'</h4>';
           
//    if(isset($_GET['empupdate']))
//	{
//	echo '<h5 align="center" style="color:green">Edited Successfully</h5>';	
//		
//	}
           echo' 	                                                                         	 
	<table  class="table table-bordered table-striped" >
	<thead>
	<tr>
	<th>ID</th>
	<th>Name</th>
	<th>Designation</th>
	<th>DOB</th>
	
	<th>Address</th>
	<th>Phone</th>
	<th>Gender</th>
	<th>D.O.J.</th>
	<th>D.O.I.</th>
	<th>Qualification</th>
	<th>Profi. Qualification</th>
	<th>Nature of Job</th>
	<th>BASIC PAY</th>
	<th>GRADE PAY</th>

	
							
	</tr>
	</thead>	 	
	<tbody align="center"> 
	';	$sn=1;
		$data="SELECT * FROM payroll_emp ";
	$exe_data=mysql_query($data);
	while($fetch_data=mysql_fetch_array($exe_data))
	{
	
		$desi="SELECT * FROM `salary_calculator` WHERE id=".$fetch_data[14]."";
		$exe_desi=mysql_query($desi);
		$fetch_desi=mysql_fetch_array($exe_desi);
		$designation = $fetch_desi[1];
		echo '<tr>
		<th>'.$sn++.'</th>
		<th><a href="payroll/print_employee_salary_structure2.php?id='.$fetch_data[0].'">'.$fetch_data[1].'</a></th>
		<th>'.$designation.'</th>
		<th>'.$fetch_data[3].'</th>
		
		<th>'.$fetch_data[5].'</th>
		<th>'.$fetch_data[6].'</th>
		<th>'.$fetch_data[7].'</th>
		<th>'.$fetch_data[9].'</th>
		<th>'.$fetch_data[10].'</th>
		<th>'.$fetch_data[11].'</th>
		<th>'.$fetch_data[12].'</th>
		<th>'.$fetch_data[13].'</th>
		<th>'.$fetch_data[19].'</th>
		
		<th>'.$fetch_desi[2].'</th>
	
	
		
		</tr>';
	
	
	}
	
				 					                   
	echo'		
	</tbody>
	</table>			
					
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
    
}




//edit employee 
function edit_employee_salary()
{
    
    
  echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Employee Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';
  $uid=$_GET['id'];
  
  $month=$_GET['month'];
  $year=$_GET['year'];
          
    if(isset($_GET['editsalary']))
	{
	echo '<h5 align="center" style="color:green">Edited Successfully</h5>';	
		
	}
        
        $view_employee="
		SELECT   *    FROM employee_table  
                                                   WHERE e_id=$uid 
		";
	$view_result=mysql_query($view_employee);
	$fetch_result=mysql_fetch_array($view_result);
	   
                                                       $uid=$fetch_result['e_id'];
                                                        $name=$fetch_result['e_name'];
        
        
        
        
        echo'<h5 align="center">NAME:   '.$name.'</h5>';
        
        
             $get_salary="SELECT * FROM employee_salary_details WHERE  emp_id= $uid AND year=$year  AND month_of_year=$month   ";
                                                             $exe_get_salary=mysql_query($get_salary);
                                                             $fetch_salary=mysql_fetch_array($exe_get_salary);
                                                             $basic=$fetch_salary['basic_salary'];
                                                              $PF=$fetch_salary['pf_emp'];
                                                            $HRA=$fetch_salary['HRA'];
                                                             $DA=$fetch_salary['DA'];
                                                                   $net_salary=$fetch_salary['net_salary'];
                                                                 $check_no=$fetch_salary['check_no'];
                                                       $total=$basic+$HRA+$DA;
                                                                 
                                                                 $net_total=$total-$PF;
        
        
        
        
        
        
    echo' <form action="payroll/update_employee_salary.php" method="get" >
<input type="hidden" name=id      value='.$uid.' />
    <input type="hidden" name=month      value='.$month.' />
        <input type="hidden" name=year      value='.$year.' />
      <div class="section ">
    <label> Basic<small></small></label>   
    <div> 
   <input type="text" class="validate[required] medium" name="basic"   value="'.$basic.'"/>
     </div>
     </div>					
    <div class="section ">
    <label> PF<small></small></label>   
    <div> 
   <input type="text" class="validate[required] medium" name="pf"     value="'.$PF.'"/>
     </div>
     </div>	
     <div class="section ">
    <label>Check No.<small></small></label>   
    <div> 
   <input type="text" class="validate[required] medium" name="check_no"  value="'.$check_no.'"/>
     </div>
     </div>	
    
       <div class="section last">	
	<button class="uibutton icon  add " type="submit">Save </button> 
	
						
				</form>



       ';
    
    
    
    
    
    
    echo'				
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
    
    
    
    
    
    
    
    
    
    
    
}
//////addd employeee details
function add_employee_details()
{
    
    
  echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Employee Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';	  
    if(isset($_GET['empAdded']))
	{
	echo '<h5 align="center" style="color:green">Employee Added</h5>';	
		
	}
    echo' <form action="payroll/add_employee.php" method="get" >

      <div class="section ">
    <label> Name<small></small></label>   
    <div> 
   <input type="text" class="validate[required] medium" name="name"/>
     </div>
     </div>					
    <div class="section ">
    <label> Post<small></small></label>   
    <div> 
   <input type="text" class="validate[required] medium" name="post"/>
     </div>
     </div>	
     <div class="section ">
    <label> Qualification<small></small></label>   
    <div> 
   <input type="text" class="validate[required] medium" name="qualification"/>
     </div>
     </div>	
     <div class="section">
		   <label>Date of Birth<small></small></label>   
		  <div><input type="text"  id="birthday" class=" birthday  small " name="dob"  />
		  </div>
	                         </div>
		  <div class="section">
		   <label>Date of joining <small></small></label>   
		  <div><input type="text"  id="birthday1" class=" birthday  small " name="doa"  />
		  </div>
	                           </div>
                                                   <div class="section">
		   <label>DOC<small></small></label>   
		  <div><input type="text"  id="birthday2" class=" birthday  small " name="doc"  />
		  </div>
	                        </div>

 <div class="section ">
  <label>NOP<small></small></label>   
  <div>
  <select data-placeholder="Select ..." class="chzn-select" tabindex="2" name="nop">
          <option value=""></option> 	
         <option value="Probation">Probation</option>
          <option value="Confirm">Confirm</option>
          <option value="Adoc">Adoc</option>
          </select>
                                </div>
                                 </div>
                                 <br></br>
       <div class="section last">	
	<button class="uibutton icon  add " type="submit">Add </button> 
	<button class="uibutton icon special cancel " type="reset">Cancel</button> 	
						
				</form>



       ';
    
    
    
    
    
    
    echo'				
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
    
}


function view_employee_details()
{
    
    echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Salary Structure</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';	  
    
     if(isset($_GET['empupdate']))
	{
	echo '<h5 align="center" style="color:green">Edited successfully  !</h5>';	
		
	}
        
        
     if(isset($_GET['empDeleted']))
	{
	echo '<h5 align="center" style="color:red">Employee Deleted</h5>';	
		
	}
        
        
        
        echo'<a href="payroll/print_employee_details.php"><button class="uibutton confirm" type="button">Print</button></a>';
                  echo' 	                                                                         	 
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th>S.No.</th>
	<th>Name</th>
	<th>Post</th>
	<th>Qualification</th>
	<th>DOB</th>
	<th>DOA</th>
	<th>DOC</th>
	<th>NOP</th>	
                         <th>Action</th>
	</tr>
	</thead>	 	
	<tbody align="center"> 
	';  $srno=0;
         
                                             $get_salary="SELECT  DISTINCT *    FROM employee_table   WHERE  e_id NOT IN (SELECT emp_id FROM  employee_deleted_table)
                                                                                                                                                
                                                                                      ORDER BY  ord_post+0 
                                                                     ";
                                                             $exe_get_salary=mysql_query($get_salary);
                                                            while( $fetch_salary=mysql_fetch_array($exe_get_salary))
                                                            {   $id=$fetch_salary[0];
                                                             $name=$fetch_salary[1];
                                                              $post=$fetch_salary[2];
                                                               $qualification=$fetch_salary[3];
                                                                $dob=$fetch_salary[4];
                                                                 $doa=$fetch_salary[5];
                                                                  $doc=$fetch_salary[6];
                                                                 $noc=$fetch_salary[7];
                                                                      $srno++;
                                                                 echo'<tr><td>'.$srno.'</td>
                                                                 <td>'.$name.'</td>
                                                                     <td>'.$post.'</td>
                                                                         <td>'.$qualification.'</td>
                                                                             <td>'.$dob.'</td>
                                                                       
                                                                             <td>'.$doc.'</td>
                                                                                 <td>'.$doa.'</td>
                                                                                     <td>'.$noc.'</td>
                                                                      <td><span class="tip"><a href="payroll_edit_employee_details.php?id='.$id.'
			" class="table-icon edit" title="Edit">
			<img src="images/icon/icon_edit.png"/></a></span>
			<span class="tip" onclick="delete_emp_confirm('.$id.');">
			<a class="table-icon delete" title="Delete" >
			<img src="images/icon/icon_delete.png"/></a></span>	</td>
                                                                                 
                                                                         
                                                                      </tr>';     
                    
                                                            }
    
    echo'	</tbody></table>			
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
    
    
}




function employee_adit_details()
{
    
    
  echo '<div class="row-fluid">                           
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Employee Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';
  $id=$_GET['id'];
    if(isset($_GET['empAdded']))
	{
	echo '<h5 align="center" style="color:green">Employee Added</h5>';	
		
	}
        
        
                                                            $get_salary="SELECT * FROM employee_table   where e_id=$id  ";
                                                             $exe_get_salary=mysql_query($get_salary);
                                                         $fetch_salary=mysql_fetch_array($exe_get_salary);
                                                     $id=$fetch_salary[0];
                                                             $name=$fetch_salary[1];
                                                              $post=$fetch_salary[2];
                                                               $qualification=$fetch_salary[3];
                                                                $dob=$fetch_salary[4];
                                                                 $doa=$fetch_salary[5];
                                                                  $doc=$fetch_salary[6];
                                                                 $nop=$fetch_salary[7];
        
        
        
        
        
        
        
    echo' <form action="payroll/update_employee.php" method="get" >
<input type="hidden" name=id      value='.$id.' />
      <div class="section ">
    <label> Name<small></small></label>   
    <div> 
   <input type="text" class="validate[required] medium" name="name"   value="'.$name.'"/>
     </div>
     </div>					
    <div class="section ">
    <label> Post<small></small></label>   
    <div> 
   <input type="text" class="validate[required] medium" name="post"     value="'.$post.'"/>
     </div>
     </div>	
     <div class="section ">
    <label> Qualification<small></small></label>   
    <div> 
   <input type="text" class="validate[required] medium" name="qualification"  value="'.$qualification.'"/>
     </div>
     </div>	
     <div class="section">
		   <label>Date of Birth<small></small></label>   
		  <div><input type="text"  id="birthday" class=" birthday  small " name="dob" value="'.$dob.'" />
		  </div>
	                         </div>
		  <div class="section">
		   <label>Date of joining <small></small></label>   
		  <div><input type="text"  id="birthday1" class=" birthday  small " name="doa"  value="'.$doa.'"/>
		  </div>
	                           </div>
                                                   <div class="section">
		   <label>DOC<small></small></label>   
		  <div><input type="text"  id="birthday2" class=" birthday  small " name="doc"  value="'.$doc.'"/>
		  </div>
	                        </div>

 <div class="section ">
  <label>NOP<small></small></label>   
  <div>
  <select data-placeholder="Select ..." class="chzn-select" tabindex="2" name="nop">
          <option value="'.$nop.'">'.$nop.'</option> 	
         <option value="Probation">Probation</option>
          <option value="Confirm">Confirm</option>
          <option value="Adoc">Adoc</option>
          </select>
                                </div>
                                 </div>
                                 <br></br>
       <div class="section last">	
	<button class="uibutton icon  add " type="submit">Save </button> 
	<button class="uibutton icon special cancel " type="reset">Reset</button> 	
						
				</form>



       ';
    
    
    
    
    
    
    echo'				
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
    
    }
    
    
    
function add_new_emp()
{
	
	echo '	
<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Add Teacher</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
		
				';
				
				if(isset($_GET['usernameExists']))
				{
					echo "<h2 style=\"color:red\" align=\"center\">Username Already exists, please try again</h2>";
				}
				if(isset($_GET['error']))
				{
					echo "<h2 style=\"color:red\" align=\"center\">Error! Please try again</h2>";
				}
				if(isset($_GET['added']))
				{
					echo "<h2 style=\"color:green\" align=\"center\">Teacher Added</h2>" ;
				}
				
			echo'	
				
    <form  action="payroll/add_emp.php" method="post" >

	<div class="section ">
	<label> Name<small></small></label>   
	<div> 
	<input type="text" class="validate[required] medium" name="name"/>
	</div>
	</div>	
		
	<div class="section numericonly">
	<label>BASIC PAY<small></small></label>   
	<div> 
	<input type="text" class="validate[required] medium" name="basic"/>
	</div>
	</div>	
		
		
	<div class="section ">
	<label>Designation<small></small></label>   
	<div>   
	<select data-placeholder="Select a Designation..." class="chzn-select" tabindex="2" name="designation">
	<option value=""></option> 		  
	';		  
		//get the designationfrom designation table
		  $get_designation_query="SELECT * FROM `salary_calculator`";
		  $get_designation_query_res=mysql_query( $get_designation_query);
		  while($fetch_designation=mysql_fetch_array( $get_designation_query_res))
		  {
			  $designation=$fetch_designation[1];
			 
		  echo '
		   <option value="'.$fetch_designation[0].'">'. $designation.' - ( '.$fetch_designation[2].' )</option>';
		  }		
		  echo'</select>
             </div>
             </div>					
	<div class="section ">
	<label> Email<small></small></label>   
	<div> 
	<input type="text" class="validate[required,custom[email]] medium" name="email_id" id="e_required">
	</div>
	</div>				
	<div class="section  numericonly ">
	<label>Contact No<small></small></label>   
	<div> 
	<input type="text" class="validate[required] medium" name="phone_no"/>
	</div>
	</div>		
	<div class="section">
	<label>Address <small></small></label>   
	<div > <textarea name="local_address" id="Textareaelastic"  class="medium"  cols="" rows=""  ></textarea>
	</div>                                              
	</div>		   
	<div class="section">
	<label>Date of Birth<small></small></label>   
	<div><input type="text"  id="birthday" class=" birthday  small " name="dob"  />
	</div>
	</div>

	<div class="section">
	<label>Gender <small></small></label>   
	<div>
	<div class="radiorounded">
	<input type="radio" name="gender" id="radio-1" value="male" />
	<label for="radio-1" title="Male"></label>
	</div>
	<div class="radiorounded">
	<input type="radio" name="gender" id="radio-2" value="female"/>
	<label for="radio-2"  title="Female"></label>
	</div>
	</div>
	</div>
	


	<div class="section">
	<label>Date of Joining<small></small></label>   
	<div><input type="date"   name="doj"  />
	</div>
	</div>
	
	<div class="section">
	<label>Date of Increment<small></small></label>   
	<div><input type="date"   name="doi"  />
	</div>
	</div>



									  	  
	<div class="section ">
	<label>Qualification<small></small></label>   
	<div> 
	<input type="text" class="validate[required] medium" name="quali"/>
	</div>
	</div>	

		<div class="section ">
	<label> Professional Qualification<small></small></label>   
	<div> 
	<select name="pro_quali">
	<option value="N/A">N/A</option>
	<option value="M.Ed."> M.Ed.</option>
	<option value="B.Ed."> B.Ed.</option>
	<option value="B.Liv."> B.Liv.</option>
	<option value="DIET"> DIET</option>
	<option value="NTT"> NTT</option>
	<option value="JBT"> JBT</option>
	<option value="OTHER">OTHER</option>
	</select>
	</div>
	</div>	

		<div class="section ">
	<label>Nature of Job<small></small></label>   
	<div> 
	<select name="noj">
	<option value="ADOBE"> ADOBE</option>
	<option value="PROBATION"> PROBATION</option>
	<option value="PERMANENT"> PERMANENT</option>
	<option value="GUEST"> GUEST</option>
	<option value="OTHER">OTHER</option>
	</select>
	</div>
	</div>	


	
	<br></br>
	<div class="section last">	
	<div>
	<button class="uibutton icon  add " type="submit">Add Teacher</button> 
	<button class="uibutton icon special cancel " type="reset">Cancel</button> 
	</div>
	</div>	
	 
	</form>							
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
';
}
 
    
    
    
    
     
function add_new_emp1()
{

$priv=$_SESSION['priv'];
	echo '	
<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Add Teacher</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
		
				';
				
				if(isset($_GET['usernameExists']))
				{
					echo "<h2 style=\"color:red\" align=\"center\">Username Already exists, please try again</h2>";
				}
				if(isset($_GET['error']))
				{
					echo "<h2 style=\"color:red\" align=\"center\">Error! Please try again</h2>";
				}
				if(isset($_GET['added']))
				{
					echo "<h2 style=\"color:green\" align=\"center\">Teacher Added</h2>" ;
				}
				
				
				$data="SELECT * FROM payroll_emp WHERE uId=".$_GET['id']." ";
	$exe_data=mysql_query($data);
	$fetch_data=mysql_fetch_array($exe_data);
	
	
		$desi="SELECT * FROM `salary_calculator` WHERE id=".$fetch_data[14]."";
		$exe_desi=mysql_query($desi);
		$fetch_desi=mysql_fetch_array($exe_desi);
		$designation = $fetch_desi[1];
				
				
			echo'	

    <form  action="payroll/add_emp1.php" method="post" >
    
    <input type="hidden" name="id"  value="'.$fetch_data[0].'">

	<div class="section ">
	<label> Name<small></small></label>   

	<div> 
	<input type="text" class="validate[required] medium" name="name" value="'.$fetch_data[1].'"/>
	</div>
	</div>	';
	
	if($priv==11)
	{
		

	echo '<div class="section numericonly">
	<label>BASIC PAY<small></small></label>   
	<div> 
	<input type="text" class="validate[required] medium" name="basic" value="'.$fetch_data[19].'"/>

	</div>

	</div>	

		

		

	<div class="section ">

	<label>Designation<small></small></label>   

	<div>   

	<select data-placeholder="Select a Designation..." class="chzn-select" tabindex="2" name="designation">

	<option value="'.$fetch_desi[0].'">'.$fetch_desi[1].'</option> 		  

	';		  
		//get the designationfrom designation table
		  $get_designation_query="SELECT * FROM `salary_calculator`";
		  $get_designation_query_res=mysql_query( $get_designation_query);
		  while($fetch_designation=mysql_fetch_array( $get_designation_query_res))
		  {
			  $designation=$fetch_designation[1];
			 
		  echo '

		   <option value="'.$fetch_designation[0].'">'. $designation.' - ( '.$fetch_designation[2].' )</option>';
		  }		
		  echo'</select>

             </div>

             </div>';
             }
             
             echo '				

	<div class="section ">

	<label> Email<small></small></label>   

	<div> 

	<input type="text" class="validate[required,custom[email]] medium" name="email_id" id="e_required" value="'.$fetch_data[4].'">

	</div>

	</div>				

	<div class="section  numericonly ">

	<label>Contact No<small></small></label>   

	<div> 

	<input type="text" class="validate[required] medium" name="phone_no"  value="'.$fetch_data[6].'"/>

	</div>

	</div>		

	<div class="section">

	<label>Address <small></small></label>   

	<div > <textarea name="local_address" id="Textareaelastic"  class="medium"  cols="" rows=""  >'.$fetch_data[5].'</textarea>

	</div>                                              

	</div>		   

	<div class="section">

	<label>Date of Birth<small></small></label>   

	<div><input type="text"  id="birthday" class=" birthday  small " name="dob"   value="'.$fetch_data[3].'"/>

	</div>

	</div>
';




	

if($priv==11)
{


echo '
	<div class="section">

	<label>Date of Joining<small></small></label>   

	<div><input type="date"   name="doj"  value="'.$fetch_data[9].'" />

	</div>

	</div>

	

	<div class="section">

	<label>Date of Increment<small></small></label>   

	<div><input type="date"   name="doi"   value="'.$fetch_data[10].'"/>

	</div>

	</div>

';


}


	echo '								  	  

	<div class="section ">

	<label>Qualification<small></small></label>   

	<div> 

	<input type="text" class="validate[required] medium" name="quali"  value="'.$fetch_data[11].'"/>

	</div>

	</div>	';
	if($priv==11)
	{


	echo '
		<div class="section ">

	<label> Professional Qualification<small></small></label>   

	<div> 

	<select name="pro_quali">
	
	

	<option  value="'.$fetch_data[12].'">'.$fetch_data[12].'</option>

	<option value="M.Ed."> M.Ed.</option>

	<option value="B.Ed."> B.Ed.</option>

	<option value="B.Liv."> B.Liv.</option>

	<option value="DIET"> DIET</option>

	<option value="NTT"> NTT</option>

	<option value="JBT"> JBT</option>

	<option value="OTHER">OTHER</option>

	</select>

	</div>

	</div>	
	
	



		<div class="section ">

	<label>Nature of Job<small></small></label>   

	<div> 

	<select name="noj">
	
	<option  value="'.$fetch_data[13].'">'.$fetch_data[13].'</option>

	<option value="ADOBE"> ADOBE</option>

	<option value="PROBATION"> PROBATION</option>

	<option value="PERMANENT"> PERMANENT</option>

	<option value="GUEST"> GUEST</option>

	<option value="OTHER">OTHER</option>

	</select>

	</div>

	</div>	


';}
echo '


	

	<br></br>

	<div class="section last">	

	<div>

	<button class="uibutton icon  add " type="submit">Add Teacher</button> 

	<button class="uibutton icon special cancel " type="reset">Cancel</button> 

	</div>

	</div>	

	 

	</form>							

	</div><!-- row-fluid column-->

	</div><!--  end widget-content -->

	</div><!-- widget  span12 clearfix-->

	</div><!-- row-fluid -->

';
}
 
    
  
    


?>
