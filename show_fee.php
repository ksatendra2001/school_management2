<?php
require_once('../config/config.php');
require_once('../include/session.php');
require_once('../include/userdetail.php');
require_once('../include/check.php');
/*
$student_id=$_GET['id'];
   $duid=$_GET['did'];
	$current_session=$_SESSION['current_session_id'];
	$ge_id=$_GET['did'];
		//write query to get fee generated session id 
	$query_fee_ge_session_id="SELECT DISTINCT `duration`, `last_date`
							FROM `fee_generated_sessions`
							WHERE Id=".$ge_id."
							";
	$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
	$fetch_fee=mysql_fetch_array($execute_fee_ge_se_id);
	//Query to get the class of the user student
	$query_class_student = "SELECT class_index.class_name
							
							FROM class_index
							
							INNER JOIN class
							ON class.classId = class_index.cId
							
							WHERE class.sId = $student_id
							";
	$execute_class_student = mysql_query($query_class_student);
	$class_student = mysql_fetch_array($execute_class_student);
	//write query to get the session id from  the fee generated session
	$fetch_session_name="SELECT `session_name`
						FROM `session_table` 
						WHERE `sId`=".$current_session."";
	$execute_session_name=mysql_query($fetch_session_name);
	$fetch_session=mysql_fetch_array($execute_session_name);
	$session_name=$current_session['session_name'];
	$class_name = $class_student[0];
	$total_discount=0;
	$finee="fine";
	$fine=0;
	$camount=0;	
	$serial_no=1;
	//get name of the school
	$get_name_school=
					 "SELECT `name_school`
					 FROM `school_names`
					 WHERE `Id`= 3";
	$exe_name_school=mysql_query($get_name_school);
	$fetch_school_name=mysql_fetch_array($exe_name_school);
	$school_name=$fetch_school_name['name_school'];
	//QUERY get cost of transport from destination_rouite------
	$tranport_cost=0;
	$trans_amount="SELECT cost
	               FROM destination_route
				   
				   INNER JOIN user_vehicle_details
				   ON user_vehicle_details.did=destination_route.did
				   
				   WHERE user_vehicle_details.uId=$student_id
				   AND destination_route.session_id=".$_SESSION['current_session_id']."";
	$exe_amount_t=mysql_query($trans_amount);
	while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
		{
			$tranport_cost=$tranport_cost+$fetch_transport_cost[0];
		}
	//query get rent of hostel from room allocation --------
	$hostel_rent=0;
	$hostel_amount="SELECT rent 
	                FROM hostel_room_allocation
					WHERE sid=$student_id
					AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
	$exe_rent=mysql_query($hostel_amount);
	while($fetch_rent_cost=mysql_fetch_array($exe_rent))
		{
			$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
		}
	$trans_rent_cost=$hostel_rent+$tranport_cost;
	echo'
	<table  class="table table-bordered table-striped">';
	echo '     
	<thead>
	<tr>
	<th align="center" width="5%">S.No.</th>
	<th align="left" width="50%">Credential Type</th>
	<th align="left" width="45%">Amount</th>
	</tr>
	</thead>
	<tbody align="left">';
	//start query to fetch credential type
	$fetch_credentials_type="SELECT DISTINCT fee_credentials.type,fee_credentials.amount
							FROM fee_credentials
							INNER JOIN fee_details
							ON fee_details.fee_credential_id=fee_credentials.id
							WHERE student_id=".$student_id."
							AND fee_credentials.session_id=".$current_session."
							AND fee_details.fee_generated_session_id=".$duid."";
	$execute_credentials_type=mysql_query($fetch_credentials_type);
	while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
		{
			$camount=$camount+$fetch_credentials['amount'];
			//first latter of credential type convert small to capital
			$c=str_split($fetch_credentials['type']);
			$upper=strtoupper($c[0]);
			$count=count($c);
			echo '	<tr>
			<td align="center">'.$serial_no++.'</td>
			<td>';
			echo $upper;
			for($i=1;$i<$count;$i++)
				{
					echo $c[$i];
				}
			echo ' <td>'.$fetch_credentials['amount'].'</td></tr>';  
		}
	echo '<tr>
	<td align="center">'.$serial_no++.'</td>
	<td>Transport Fee</td>
	<td>'.$tranport_cost.'</td></tr>';
	echo '<tr>
	<td align="center">'.$serial_no++.'</td>
	<td>Hostel Fee</td>
	<td>'.$hostel_rent.'</td></tr>
	';
	echo ' <table  class="table table-bordered table-striped">
	<thead>
	<tr>
	<th align="center" width="5%">S.No.</th>
	<th align="left" width="50%">Discount Type</th>
	<th align="left" width="45%">Amount</th>
	</tr>
	</thead>
	<tbody align="left">';
	//query to fetch discountype of student
	$serial_no=1;
	$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
							FROM fee_discount_credentials
							INNER JOIN fee_details
							ON fee_details.fee_discount_id=fee_discount_credentials.id
							WHERE fee_details.student_id=".$student_id."
							AND fee_discount_credentials.session_id=".$current_session."
							AND fee_details.fee_generated_session_id=".$duid."";
	$execute_discount_type=mysql_query(  $fetch_discount_query);
	while($fetch_discount=mysql_fetch_array($execute_discount_type))
		{ 
			$total_discount=$total_discount+$fetch_discount['amount'];
			//first latter of discount type convert small to capital
			$d=str_split($fetch_discount['type']);
			
			$upper=strtoupper($d[0]);
			$c=count($d);
			echo '<tr>
			<td align="center">'.$serial_no++.'</td>
			<td>
			';
			echo $upper;
			for($i=2;$i<$c;$i++)
				{
					echo $d[$i];
				}
			echo '
			</td>
			
			<td>'.$fetch_discount['amount'].'</td>
			
			</tr>';
		}
	echo '
	<table  class="table table-bordered table-striped"  >
	<thead>
	</thead>
	<tbody align="left">';
	//write query to fetch fine amount
	$query_fine="SELECT `amount` 
				FROM `fee_credentials`
				WHERE  `type`='".$finee."'";
	$execute_fine=mysql_query($query_fine);
	$fetch_fine=mysql_fetch_array($execute_fine);
	$fine=$fetch_fine['amount'];
	//write query to get fee generated session id 
	$query_fee_ge_session_id="SELECT DISTINCT `type`, `last_date`
							  FROM `fee_generated_sessions`
							  WHERE Id=".$ge_id."
							   ";
	$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
	$fetch_fee=mysql_fetch_array($execute_fee_ge_se_id);
	$month=$fetch_fee['type'];
	$due_date=$fetch_fee['last_date'];
	//write query to get the paid on from the fee details
	$fetch_query_paid="SELECT DISTINCT `paid_on`
					  FROM `fee_details`
					  WHERE `fee_generated_session_id`=".$ge_id."
					  AND `fee_details`.`student_id`=".$student_id."";
	$execute_paid=mysql_query($fetch_query_paid);
	$fetch_paid=mysql_fetch_array($execute_paid);
	$paid_date=$fetch_paid['paid_on'];	
	//check condition due date < paid on date or not
	if($due_date<$paid_date)
		{
			$fine_days=$paid_date-$due_date;
				if($fine_days<=30)
				{
					$calculate_fine=100;
				}
				else if($fine_days<=60)
				{
					$calculate_fine=200;
				}
				else
				{
					$calculate_fine=500;
				}
		}
	else
		{
			$calculate_fine=0;  
		}
	//calculate fee amount according to duration
	$m=1;
	if($month=="Monthly")
		{
			$m=1; 
		}
	else if($month=="Quarterly")
		{
			$m=3; 
		}
	else if($month=="Half-Yearly")
		{
			$m=6;  
		}	
	else if($month=="Yearly")
		{
			$m=12;  
		}
	else
		{
			$m=1;  
		}
	$tfamount=$camount-$total_discount+$trans_rent_cost;
	$total_duration_amount= $tfamount*$m;
	$total_fine_amount=$total_duration_amount+$calculate_fine;
	$creden_cost=$camount+$trans_rent_cost;
	$fee=$creden_cost-$total_discount;
	echo '  <tr>
	<td width="55%"><b style="color:green">Total Credentials Amount</b></td><td><b style="color:green">'.$creden_cost.'</b></td>
	</tr>
	<tr>
	<td><b style="color:green">Total Discount</b></td><td><b style="color:green">'.$total_discount.'</b></td>
	</tr>
	<tr>
	<td><b style="color:green">Fee</b></td><td><b style="color:green">'.$fee.'</b></td>
	</tr>
	<tr>
	<td><b style="color:green">Late Fine</b></td><td><b style="color:green">'. $calculate_fine.'</b></td>
	</tr>
	<tr>
	<td><h4 style="color:red">Total Fee</h4></td><td><h4 style="color:red">'.$total_fine_amount.'</h4></td>
	</tr>
	</tbody>
	</table></tbody>
	</table>';
	//write query to get fee generated session id 
	$query_fee_ge_session_id="SELECT DISTINCT `duration`, `last_date`
							FROM `fee_generated_sessions`
							WHERE Id=".$ge_id."
							";
	$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
	$fetch_fee=mysql_fetch_array($execute_fee_ge_se_id);
	//query to get date on the id of date
	$get_date_of_id=
					"SELECT DISTINCT date
					FROM dates_d
					WHERE date_id = ".$fetch_fee['last_date']."";
	$exe_date=mysql_query($get_date_of_id);
	$fetch_date=mysql_fetch_array($exe_date);
	$due_date = $fetch_date[0];
	$last_due_dates=date('d-m-Y',strtotime($due_date));
	//query to get paid on date
	$paidondate="SELECT DISTINCT `paid_on`
			   FROM `fee_details`
			   WHERE `student_id`=".$student_id."
			   AND `fee_generated_session_id`=".$ge_id;
	$execute_paidondate=mysql_query($paidondate);
	$fetch_paidondate=mysql_fetch_array($execute_paidondate);
	//query to get date on the id of date
	
	$get_date_of_id=
					"SELECT DISTINCT date
					FROM dates_d
					WHERE date_id = ".$fetch_paidondate['paid_on']."";
	$exe_date=mysql_query($get_date_of_id);
	$fetch_date=mysql_fetch_array($exe_date);
	$payon_date = $fetch_date[0];
	$dates=date('d-m-Y',strtotime($payon_date));
	echo ' Last date of paid fee <b style="color:blue" >'.$fetch_fee['duration'].'</b> of session <b style="color:blue">
	'.$fetch_session['session_name'].'</b> is <b style="color:blue">'.$last_due_dates.'</b> and paid on date is<b style="color:blue">
	'.$dates.'</b><br><br>  ';          
	echo ' 
	</div>
	';
*/

$student_id=$_GET['id'];
  // $duid=$_GET['did'];
	//$current_session=$_SESSION['current_session_id'];
	//$ge_id=$_GET['did'];
 //Author : Anil Kumar*
 $flag=0;
	$duration=$_GET['did'];
	//$ad_no=$_GET['ad_no'];
	$session_id=$_SESSION['current_session_id'];
	$ABC_fee_paid=1;
	/*//query get student id from student_user
	$sid_query="SELECT sId, Name FROM student_user
				WHERE admission_no=".$ad_no;
	$exe_sid=mysql_query($sid_query);
	$fetch_sid=mysql_fetch_array($exe_sid);
	$student_id=$fetch_sid[0];
	$name=$fetch_sid[1];*/
	 $query_fee_a="SELECT * FROM fee_details
			     WHERE student_id=".$student_id."
			     AND fee_generated_session_id=".$_SESSION['current_session_id']."";
	$exe_fee_a=mysql_query($query_fee_a);
	$fetch_a=mysql_fetch_array($exe_fee_a);
	//query get student id and fee generation session id from fee details and check student fee generate or not
	$check_fee="SELECT student_id, fee_generated_session_id FROM fee_details";
	$exe_check_fee=mysql_query($check_fee);
	while($fetch_check_fee=mysql_fetch_array($exe_check_fee))
	{
		if(($fetch_check_fee[0]== $student_id)&&($fetch_check_fee[1] == $duration))
		{
			$flag=1;
			break;
		}
		else
		{
			$flag=0;
		}
		
	}
	if($flag==0)
	{
		echo '<h5 style="color:red" align="center">NO DATA AVAILABLE IN TABLE</h5> ';
	}
	else
	{
		//get date for calculating fine
		date_default_timezone_set('Asia/Kolkata');
		$date=date('Y-m-d');
		//paid date from fee details table and calculate fine
		$flag=0;
		$paidondate="SELECT paid_on, paid 
					 FROM fee_details
					 WHERE student_id=".$student_id."
					 AND fee_generated_session_id=".$duration."";
		$exepaidondate=mysql_query($paidondate);
		$fetchpaidondate=mysql_fetch_array($exepaidondate);
		//if $paidondate is not equal to zero then $format = paid on date otherwise $format = date default time zone
		if($fetchpaidondate[1]!=0)
		{
			$format=$fetchpaidondate[0];	
		}
		else
		{
			$format=date('Y-m-d',strtotime($date));
			$flag=1;	
		}
		//query get session id
		$session_id_ge="SELECT session_id, last_date 
						FROM fee_generated_sessions
						WHERE Id=".$duration."";
		$exe_se=mysql_query($session_id_ge);
		$fetch=mysql_fetch_array($exe_se);
		$current_session=$fetch[0];
		$due_date=$fetch[1];
		//querey get fee details from table name=>fee_detais
		 $query_fee="SELECT * FROM fee_details
				   WHERE student_id=".$student_id."
				   AND fee_generated_session_id=".$duration."";
		$exe_fee=mysql_query($query_fee);
		//get fine date
		if($flag==1)
			{
				//query get date id from dates_d
				$date_id="SELECT date_id
						  FROM dates_d
						  WHERE date='".$format."'";
				$execute_date=mysql_query($date_id);
				$fetch_date=mysql_fetch_array($execute_date);
				$paid_date=$fetch_date[0];
			}
		else
			{
				$paid_date=$format;	
			}
		//define fine
		$fine="fine";
		//query get fine amount/days
		$query_fine="SELECT amount
					 FROM fee_credentials
					 WHERE type='".$fine."'
					 AND session_id=".$_SESSION['current_session_id']." ";
		$exe_fine=mysql_query($query_fine);
		$fetch_fine=mysql_fetch_array($exe_fine);
		$fine_amount=$fetch_fine[0];
		//check condition due date < paid on date
		//and calculate fine
		if(($fetch_a['paid']==4)&&($fetch_a['paid_on']<3276))
		{
			$calculate_fine=500;
		}
		elseif($due_date<$paid_date)
			{
				$fine_days=$paid_date-$due_date;
					if($fine_days<=30)
						{
							$calculate_fine=100;
						}
						else if($fine_days<=60)
				{
					$calculate_fine=200;
				}
				else
				{
					$calculate_fine=500;
				}
			}
		else
			{
				$calculate_fine=0;  
			}
			
			
		echo'
		<table  class="table table-bordered table-striped" align="center">
		<thead>
		<tr>
		<th align="center" width="5%">S.No.</th>
		<th align="left" width="50%">Tution Type</th>
		<th align="left" width="45%">Amount</th>
		</tr>
		</thead>';
		$total_discount=0;
		$camount=0;	
		$serial_no=1;
		$fetch_fee=mysql_fetch_array($exe_fee);
		//QUERY get cost of transport from destination_rouite------
		$tranport_cost=0;
		 $trans_amount="SELECT cost
					   FROM destination_route
					   
					   INNER JOIN user_vehicle_details
					   ON user_vehicle_details.did=destination_route.did
					   
					   WHERE user_vehicle_details.uId=$student_id
					   AND destination_route.session_id=".$_SESSION['current_session_id']."";
		$exe_amount_t=mysql_query($trans_amount);
		while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
			{
				$tranport_cost=$tranport_cost+$fetch_transport_cost[0];
			}
		//query get rent of hostel from room allocation --------
		$hostel_rent=0;
		 $hostel_amount="SELECT rent 
						FROM hostel_room_allocation
						WHERE sid=$student_id
						AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
		$exe_rent=mysql_query($hostel_amount);
		while($fetch_rent_cost=mysql_fetch_array($exe_rent))
			{
				$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
			}
		$trans_rent_cost=$hostel_rent+$tranport_cost;
		echo '<tbody align="left">';
		//start query to fetch credential type and amount
		//and calculate total credential amount
		$security=1;
		$camoun=0;
		$fetch_credentials_type="SELECT DISTINCT fee_credentials.type,fee_credentials.amount
								FROM fee_credentials
								INNER JOIN fee_details
								ON fee_details.fee_credential_id=fee_credentials.id
								WHERE fee_details.student_id=".$student_id." 
								AND fee_credentials.session_id=".$current_session."
								AND fee_details.fee_generated_session_id=".$duration."";
		$execute_credentials_type=mysql_query($fetch_credentials_type);
		while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
			{
				$camount=$camount+$fetch_credentials['amount'];
				//first latter of credential type convert small to capital
				$c=str_split($fetch_credentials['type']);
				$upper=strtoupper($c[0]);
				$count=count($c);
				echo '	<tr>
				<td align="center">'.$serial_no++.'</td>
				<td>';
				echo $upper;
				for($i=1;$i<$count;$i++)
					{
						echo $c[$i];  
					}
				
						$quterlyfee = $fetch_credentials['amount']*3;
			        	echo '   <td>'.$fetch_credentials['amount'].'</td></tr>';  
				
			} 
		//calculate credentials with hostel fee and teransport fee
		$credential_amount=$camount+$trans_rent_cost;
		if($tranport_cost!=0)
		{
		echo '<tr>
		<td align="center">'.$serial_no++.'</td>
		<td>Transport Fee</td>
		<td>'.$tranport_cost.'</td></tr>';
		}
		if($hostel_rent!=0)
		{
		echo '<tr>
		<td align="center">'.$serial_no++.'</td>
		<td>Hostel Fee</td>
		<td>'.$hostel_rent.'</td></tr>
		';
		}
				// and calculate total discount amount
		$serial_no=1;
		$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
							  FROM fee_discount_credentials
							  INNER JOIN fee_details
							  ON fee_details.fee_discount_id=fee_discount_credentials.id
							  WHERE fee_details.student_id=".$student_id."
							  AND fee_discount_credentials.session_id=".$current_session."
							  AND fee_details.fee_generated_session_id=".$duration."";
		$execute_discount_type=mysql_query(  $fetch_discount_query);
		while($fetch_discount=mysql_fetch_array($execute_discount_type))
			{ 
				$total_discount=$total_discount+$fetch_discount['amount'];
			}
			if($total_discount!=0)
			{
		echo ' <table  class="table table-bordered table-striped">
		<thead>
		<tr>
		<th align="left" width="5%">S.No.</th>
		<th align="left" width="50%">Discount Type</th>
		<th align="left" width="45%">Amount</th>
		</tr>
		</thead>
		<tbody align="left">';
		//query to fetch discoun type of student and amount
		// and calculate total discount amount
		$serial_no=1;
		$total_discount=0;
		$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
							  FROM fee_discount_credentials
							  INNER JOIN fee_details
							  ON fee_details.fee_discount_id=fee_discount_credentials.id
							  WHERE fee_details.student_id=".$student_id."
							  AND fee_discount_credentials.session_id=".$current_session."
							  AND fee_details.fee_generated_session_id=".$duration."";
		$execute_discount_type=mysql_query(  $fetch_discount_query);
		while($fetch_discount=mysql_fetch_array($execute_discount_type))
			{ 
				 $total_discount=$total_discount+$fetch_discount['amount'];
				//first latter of discount type convert small to capital
				$d=str_split($fetch_discount['type']);
				$upper=strtoupper($d[0]);
				$c=count($d);
				echo '<tr>
				<td align="left">'.$serial_no++.'</td>
				<td>';
				echo $upper;
				for($i=1;$i<$c;$i++)
					{
						echo $d[$i];  
					}
				echo '  <td>'.$fetch_discount['amount'].'</td>
				</tr>';
			}
			}
		echo '
		<table  class="table table-bordered table-striped"  >
		<thead>
		</thead>
		<tbody align="left">';
		//write query to get id and type from fee generated session id 
		$query_fee_ge_session_id="SELECT DISTINCT `type`, last_date, duration
								  FROM `fee_generated_sessions`
								  WHERE Id=".$duration."";
		$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
		$fetch_fee_ge=mysql_fetch_array($execute_fee_ge_se_id);
		$month=$fetch_fee_ge['type'];	
		 $tfamount=$credential_amount-$total_discount;
		//calculate fee amount according to duration
		//monthly=1  quarterly-3, half early=6 and annually=12
		$m=1;
		if($month=="Monthly")
			{
				$m=1; 
			}
		else if($month=="Quarterly")
			{
				$m=3; 
			}
		else if($month=="Half-yearly")
			{
				 $m=6;  
			}	
		else if($month=="Yearly")
			{
				$m=12;  
			}
		//total fee according duration and with fine
		$abc_calculate_fee=$tfamount+$trans_rent_cost;
		 $fee_quterly=$tfamount*$m;
		$total_fee_amount_fine=$tfamount*$m + $calculate_fine;
	/*	//query get remaning fee from fee details
		$remaning_old=0;
		$query="SELECT remaning_fee
					FROM fee_details
					WHERE student_id=".$student_id."
					AND fee_generated_session_id<>".$duration."";
		$exe_fe=mysql_query($query);
		while($fetch_fe=mysql_fetch_array($exe_fe))
		{
		$remaning_old=$fetch_fe[0];
		}*/
		//calculate total remaning_fee in all duration in both query and add in second query by while loop
		
	/*	$que_remaning_duration="SELECT DISTINCT fee_generated_session_id
								FROM fee_details
								WHERE student_id=".$student_id."";
		$exe_remaning_duration=mysql_query($que_remaning_duration);
		while($fetch_remaning_duration=mysql_fetch_array($exe_remaning_duration))
			{*/
		//get duration
		//get total remaning fee in all duration
		$calculate_total_remaning_fee_of_stu_in_table=0;
		  $total_remaning_query="SELECT  remaning_fee
								FROM fee_details
								WHERE student_id=".$student_id."
								AND fee_generated_session_id<".$duration."
								AND remaning_fee <> 0";
		$exe_total_remaning_in_table=mysql_query($total_remaning_query);
		$fetch_total_remaning=mysql_fetch_array($exe_total_remaning_in_table);
		
		$calculate_total_remaning_fee_of_stu_in_table = $calculate_total_remaning_fee_of_stu_in_table+$fetch_total_remaning[0];
		
			/*}*/
		//query get remaning fee from fee details
		$remaning_fee=0;
		 $query_fee="SELECT remaning_fee, receiving_fee, paid
					FROM fee_details
					WHERE student_id=".$student_id."
					AND fee_generated_session_id = ".$duration."";
		$exe_fee=mysql_query($query_fee);
		$fetch_fee=mysql_fetch_array($exe_fee);
		//due fee
		 $remaning_fee=$fetch_fee[0];
		//paid fee
     	$receiving_fee = $fetch_fee[1];
		 $paid_status = $fetch_fee[2];
		//if fee paid then remaning fee show ==0
		$show_remaning_zero=0;
		//calculate remeaning fee after update query when remaning fee == 0 in table
		//dues fee in select duration in table
		if($fetch_fee[2]==1)
		{
		 $due_fee_in_se_du = $fee_quterly - $fetch_fee[1];
		 $duestd = $fetch_fee[0] - $due_fee_in_se_du;
		}
		else
		{
			$duestd=0;
		} 
		//previous duration remaning fees
		//calculate other duration fee if fee not pay in previous duration     =====previous duration remaning fee=====
		date_default_timezone_set('Asia/Kolkata');
		$today_date=date('Y-m-d');
		$format_today=date('Y-m-d',strtotime($today_date));
		//get date id from date _d
		 $pre_date="SELECT date_id FROM dates_d
		           WHERE date='".$format_today."'";
		$exe_pre_date=mysql_query($pre_date);
		$fetch_pre_date=mysql_fetch_array($exe_pre_date);
		$pre_ddate=$fetch_pre_date[0];
		$total_previous_remaning_fee=0;
		$fine_previoun =0;
		$other_duration_remaning_fee="SELECT * FROM fee_generated_sessions
		                              WHERE Id<>".$duration."
									  AND last_date<=".$due_date."";
									 
		$exe_other_duration_remaning_fee=mysql_query($other_duration_remaning_fee);
		while($fetch_other_duration_remaning_fee=mysql_fetch_array($exe_other_duration_remaning_fee))
			{
			   $duration_previous=$fetch_other_duration_remaning_fee[0];
			   //calculate fee
			   $previous_paid="SELECT paid, receiving_fee FROM fee_details
							   WHERE student_id=".$student_id."
							   AND fee_generated_session_id=".$duration_previous."";
			  $exe_pre_paid=mysql_query($previous_paid);
			  $fetch_previous_paid=mysql_fetch_array($exe_pre_paid);
			  $check_bounce_due_fee=$fetch_previous_paid[1];
	if(($fetch_previous_paid[0]==1)||($fetch_previous_paid[0]==2)||($fetch_previous_paid[0]==3)||($fetch_previous_paid[0]==5)
	   ||($fetch_previous_paid[0]==4))
		      {
			     $flag_paid=1  ;
				 $total_fee_amount_previous=0;
				 $fine_previoun=0;
		      }
		     else
		       {
					  //QUERY get cost of transport from destination_rouite------
					$tranport_cost_previous=0;
					$trans_amount="SELECT cost
								   FROM destination_route
								   
								   INNER JOIN user_vehicle_details
								   ON user_vehicle_details.did=destination_route.did
								   
								   WHERE user_vehicle_details.uId=$student_id
								   AND destination_route.session_id=".$_SESSION['current_session_id']."";
					$exe_amount_t=mysql_query($trans_amount);
					while($fetch_transport_cost_previous=mysql_fetch_array($exe_amount_t))
						{
							$tranport_cost_previous=$tranport_cost_previous+$fetch_transport_cost_previous[0];
						}
					//query get rent of hostel from room allocation --------
					$hostel_rent_previous=0;
					$hostel_amount="SELECT rent 
									FROM hostel_room_allocation
									WHERE sid=$student_id
									AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
					$exe_rent=mysql_query($hostel_amount);
					while($fetch_rent_cost_previous=mysql_fetch_array($exe_rent))
						{
							$hostel_rent_previous=$hostel_rent_previous+$fetch_rent_cost_previous[0];
						}
					$trans_rent_cost_previous=$hostel_rent_previous+$tranport_cost_previous;
					echo '<tbody align="left">';
					//start query to fetch credential type and amount
					//and calculate total credential amount
					$camount_previous=0;
					$fetch_credentials_type="SELECT DISTINCT fee_credentials.type,fee_credentials.amount
											FROM fee_credentials
											INNER JOIN fee_details
											ON fee_details.fee_credential_id=fee_credentials.Id
											WHERE fee_details.student_id=".$student_id." 
											AND fee_credentials.session_id=".$current_session."
											AND fee_details.fee_generated_session_id=".$duration_previous."";
					$execute_credentials_type=mysql_query($fetch_credentials_type);
					while( $fetch_credentials_previous=mysql_fetch_array($execute_credentials_type))
						{
							$camount_previous=$camount_previous+$fetch_credentials_previous['amount'];

						} 
					//calculate credentials with hostel fee and teransport fee
					$credential_amount_previous=$camount_previous+$trans_rent_cost_previous;
		
					//query to fetch discoun type of student and amount
					// and calculate total discount amount
					$serial_no=1;
					$total_discount_previous=0;
					$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
										  FROM fee_discount_credentials
										  INNER JOIN fee_details
										  ON fee_details.fee_discount_id=fee_discount_credentials.Id
										  WHERE fee_details.student_id=".$student_id."
										  AND fee_discount_credentials.session_id=".$current_session."
										  AND fee_details.fee_generated_session_id=".$duration_previous."";
					$execute_discount_type=mysql_query(  $fetch_discount_query);
					while($fetch_discount_previous=mysql_fetch_array($execute_discount_type))
						{ 
							$total_discount_previous=$total_discount_previous + $fetch_discount_previous['amount'];
							
						}
			/*		echo '
					<table  class="table table-bordered table-striped"  >
					<thead>
					</thead>
					<tbody>';*/
					//write query to get id and type from fee generated session id 
					$previoun_fine=0;
					 $query_fee_ge_session_id="SELECT DISTINCT `type`, last_date, duration
											  FROM `fee_generated_sessions`
											  WHERE Id=".$duration_previous."";
					$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
					$fetch_fee_pre=mysql_fetch_array($execute_fee_ge_se_id);
					$month_previous=$fetch_fee_pre['type'];
					$previous_duration_show = $fetch_fee_pre['duration'];
					//calculate previous duration remaning fee
					$tfamount_previous = $credential_amount_previous - $total_discount_previous;
					//calculate fine previous duration
					
					if($fetch_fee_pre[1]<$pre_ddate)
						{
						$fine_days = $pre_ddate- $fetch_fee_pre[1];
								
							if($fine_days<=30)
						{
							$previoun_fine=100;
						}
						else if($fine_days<=60)
						  {
							  $previoun_fine=200;
						  }
						  else 
						  {
							  $previoun_fine=500;
						  }
						 
						}
						$fine_previoun = $fine_previoun + $previoun_fine;
					//calculate fee amount according to duration
					//monthly=1  quarterly-3, half early=6 and annually=12
					$m=1;
					if($month_previous=="Monthly")
						{
							$m=1; 
						}
					else if($month_previous=="Quarterly")
						{
							$m=3; 
						}
					else if($month_previous=="Half-yearly")
						{
							 $m=6;  
						}	
					else if($month_previous=="Yearly")
						{
							$m=12;  
						}
					//total fee according duration and with fine
					$total_fee_amount_previous=$tfamount_previous  * $m ;
		      }
			  $total_previous_remaning_fee=$total_previous_remaning_fee + $total_fee_amount_previous;
		  }
		echo '  <tr>
		<td width="55%"><b>Total Tution Fee</b></td><td><b>'.$credential_amount.'
		</b></td>
		</tr>';
		if($total_discount>0)
		{
		echo '<tr>
		<td><b style="color:">Total Discount</b></td><td><b style="color:">'.$total_discount.'</b></td>
		</tr>
		<tr>';
		}
		 $type="SELECT * FROM fee_generated_sessions
	           WHERE Id=".$duration."";
    	$exe_type=mysql_query($type);
	    $fetch_type=mysql_fetch_array($exe_type);
		  echo'<td width="55%"><b style="color:green">Fee : (Monthly)</td><td><b style="color:green">'.$tfamount.'</td>
		</tr>
		<tr>';
	   echo'<td width="55%"><b style="color:green">Fee : ('.$fetch_type[4].')</td><td><b style="color:green">'.$fee_quterly.'</td>
		</tr>
		<tr>';
		//if condition show remaning fee == 0 if fee paid
	/*	if($paid_status==0)
		{*/
		//select query get previous remaning fee from fee details
		$query_pri_fee_store="SELECT pre_remaning_fee, previous_fine, paid
		                      FROM fee_details
							  WHERE student_id=".$student_id."
							  AND fee_generated_session_id=".$duration."";
		$exe_fee_store=mysql_query($query_pri_fee_store);
		$fetch_fee_store=mysql_fetch_array($exe_fee_store);
		$previous_fee_store_in_feedetails = $fetch_fee_store[0];
		$store_pre_fine = $fetch_fee_store[1] ;
		$paid_cheque=$fetch_fee_store[2];
		 $remaning_fee_pre_and_paid = $calculate_total_remaning_fee_of_stu_in_table + $total_previous_remaning_fee  
		                            + $previous_fee_store_in_feedetails - $fetch_fee_store[1] ;
			echo '<td><b>Remaning Fee</b></td><td><b>'.$remaning_fee_pre_and_paid.'</b></td></tr>';
			
			//calculate cheque bounce fee
			 $cheque_fine = "SELECT admission_no, paid_duration 
			                FROM fee_cheque_details
							where
							status=0 OR paid_duration=".$duration."";
			$exe_cheque_fine=mysql_query($cheque_fine);
			$fetch_cheque_fine=mysql_fetch_array($exe_cheque_fine);
			$paid_duration=$fetch_cheque_fine[1];

			$cheque_bounce_no = $fetch_cheque_fine[0];
			$cheque_bounce_amount = $cheque_bounce_no *200;
			if(($cheque_bounce_no>0)&&($paid_cheque==0))
			{
			echo '<tr><td><b>Cheque Bounce Charge</td><td><b>'.$cheque_bounce_amount.'</td>';
			}
			elseif($paid_duration==$duration)
			{
				echo '<tr><td><b>Cheque Bounce Charge</td><td><b>'.$cheque_bounce_amount.'</td>';
			}
/*		}
		else
		{
			echo '<td><h4 >Remaning Fee</h4></td><td><h4 >'.$show_remaning_zero.'</h4></td>';
		}*/
		
		 $TOTAL_FINE= $calculate_fine + $fine_previoun + $fetch_a['previous_fine'] ;
		
		echo '</tr>
		<tr>
		<td> <b>Late Fine </td><td><b>'.$TOTAL_FINE.'</td>
		</tr>';
		// get discount and fine
		$discount_dis=0;
		$fine_fi=0;
		$dis_fine="SELECT * FROM fee_cheque_cash_amount_details
		           WHERE student_id=".$student_id."
				   AND duration_id=".$duration."";
		$exe_dis_fine=mysql_query($dis_fine);
		while($fetch_dis_fine=mysql_fetch_array($exe_dis_fine))
		{
			$discount_dis=$discount_dis + $fetch_dis_fine[6];
		     $fine_fi=$fine_fi + $fetch_dis_fine[7];
		}
		 $xyz = $fine_fi - $discount_dis;
		//calculate total fee 
		//total fee =  total fee amount + remaning fee + due fee
		if(($fetch_a['paid']==1)&&($fetch_a['remaning_fee']==0))
		{
			$total_fee = $fee_quterly  + $duestd + $xyz ;
		}
		else
		{
			//add $duestd
	    $total_fee = $fee_quterly  + $xyz + $TOTAL_FINE +$remaning_fee_pre_and_paid + $cheque_bounce_amount;
		}
		echo '<tr>
		<td><b style="color:green">Total Fee</b></td><td><b style="color:green">'.$total_fee.'</b></td>
		</tr>
		<tr>
		<td><b>Paid Fee</td><td><b>'.$receiving_fee.'</td>
		</tr>
		<tr>
		<td><b>Due Fee</td><td><b>'.$remaning_fee.'</td>
		</tr>';
		if($receiving_fee==0)
		{
		echo '<tr>
		<td><b style="color:red">Payee Fee</b></td><td><b style="color:red">'.$total_fee.'</b></td>
		</tr>';
		}
		else
		{
		echo '<tr>
		<td><b style="color:red">Payee Fee</b></td><td><b style="color:red">'.$remaning_fee.'</b></td>
		</tr>';
		}
		echo '</tbody>
		</table></tbody>
		</table>';
		echo '<h5 Last date of fee submition of '.$fetch_fee_ge['duration'].' is: '.$fetch_fee_ge[1].'></h5>
		';
		echo '	   
		';	
	}
 
?>