<?php
require_once('../config/config.php');
require_once('../include/session.php');
require_once('../include/userdetail.php');
require_once('../include/check.php');

//get school test details data from the accounts school test details page admission function
$testname=$_POST['testname'];
$testtype=$_POST['type'];
$nque=$_POST['que'];
$testdu=$_POST['testdu'];
$testdate=$_POST['testdate'];
$max=$_POST['max'];
$pass=$_POST['pass'];

//if condition to check passing marks <= max marks
if($pass>$max)
{
  echo '<script type="text/javascript">
	
	alert("PASSING MARKS CAN`T BE MORE THAN MAXIMUM MARKS");
	
	window.location.href="../accounts_school_test_details.php";
	</script>
	
	';	
}
else
{

//write quiry to insert school test details into pre_school_test_details table
$insert_school_test_query="INSERT INTO `pre_school_test_details`
                           (`test_name`,`test_type`,`number_of_question`,`test_duration`,`test_date`,max_marks,`passing_marks`)
						    VALUES('$testname','$testtype',$nque,'$testdu','$testdate',$max,$pass)";
$execute_insert_query=mysql_query($insert_school_test_query); 

if(mysql_affected_rows())
{
header("location:../accounts_school_test_details.php?ssuccessfull");	
	
}
else
{
	header("location:../accounts_school_test_details.php?sUnsuccessfull");	
}
}
?>