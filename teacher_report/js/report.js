//function to get subject
 //Author;Manish
function select_sub()
{   
  class_id=$("#class").val();
  
		$.ajax({
		type: 'GET',
		url: 'teacher_report/show_report_subject.php',
		data: {class:class_id},
		success: function(data) {
				$('#data_sub').html(data);
		}
		});
	
}

//Author;Manish
function select_sub_plan()
{   
  class_id=$("#class").val();
  
		$.ajax({
		type: 'GET',
		url: 'teacher_report/show_report_subject_plan.php',
		data: {class:class_id},
		success: function(data) {
				$('#data_sub_plan').html(data);
		}
		});
	
}


//Author;Manish
function select_sub_plan_day()
{   
  class_id=$("#class").val();
  
		$.ajax({
		type: 'GET',
		url: 'teacher_report/show_report_subject_plan_day.php',
		data: {class:class_id},
		success: function(data) {
				$('#data_sub_plan_day').html(data);
		}
		});
	
}

//Author;Manish
function show_report_details()
{   
  class_id=$("#class").val();
  subject_id=$("#subject").val();
  from_date=$("#from").val();
  to_date=$("#to").val();
  
		$.ajax({
		type: 'GET',
		url: 'teacher_report/show_report_plan_details_day.php',
		data: {class:class_id,subject:subject_id,from:from_date,to:to_date},
		success: function(data) {
				$('#data_report_details').html(data);
				
		}
		});
	
}


//admin priv
//Author;Manish
function admin_select_sub_plan()
{   
  class_id=$("#class").val();
  tid_id=$("#tid").val();
   session=$("#session").val();
 
		$.ajax({
		type: 'GET',
		url: 'teacher_report/admin_show_report_subject_plan.php',
		data: {class:class_id,tid:tid_id,session_id:session},
		success: function(data) {
				$('#admin_data_sub_plan').html(data);
		}
		});
	
}

//Author;Manish
function admin_select_sub_plan_date()
{   
  class_id=$("#class").val();
  tid_id=$("#tid").val();
  from_id=$("#from").val();
  to_id=$("#to").val();
  
		$.ajax({
		type: 'GET',
		url: 'teacher_report/admin_show_report_subject_plan_date.php',
		data: {class:class_id,tid:tid_id,from:from_id,to:to_id},
		success: function(data) {
				$('#admin_data_sub_plan_date').html(data);
				
		}
		});
	
}
//admin daily report
function select_daily_sub_admin()
{   
  class_id=$("#class").val();
  tid_id=$("#tid").val();
  
		$.ajax({
		type: 'GET',
		url: 'teacher_report/show_admin_daily_report_subject.php',
		data: {class:class_id,tid:tid_id},
		success: function(data) {
				$('#data_daily_sub_admin').html(data);
		}
		});
	
}


//teacher daily report
function select_daily_sub_teacher()
{   
  class_id=$("#class").val();
  tid_id=$("#tid").val();
  month_id=$("#month").val();
   session_id_id=$("#session_id").val();
  
		$.ajax({
		type: 'GET',
		url: 'teacher_report/show_teacher_month_daily_report.php',
		data: {class:class_id,tid:tid_id,month:month_id,session_id:session_id_id},
		success: function(data) {
				$('#data_daily_sub_teacher').html(data);
		}
		});
	
}

//admin daily report
function date_daily_report_sub_admin()
{   
  class_id=$("#class").val();
  tid_id=$("#tid").val();
 
  
		$.ajax({
		type: 'GET',
		url: 'teacher_report/show_admin_daily_report_date.php',
		data: {class:class_id,tid:tid_id},
		success: function(data) {
				$('#date_daily_report_sub_admin').html(data);
		}
		});
	
}

//parent daily report
function daily_report_sub_parent()
{   
  
  tid_id=$("#tid").val();
  sid_id=$("#sid").val();
 
  
		$.ajax({
		type: 'GET',
		url: 'teacher_report/show_teacher_subject_parent.php',
		data: {tid:tid_id,sid:sid_id},
		success: function(data) {
				$('#date_daily_report_sub_parent').html(data);
		}
		});
	
}

//parent daily report date wise
function daily_report_sub_parent_date()
{   
  
  tid_id=$("#tid").val();
  sid_id=$("#sid").val();
   
 
  
		$.ajax({
		type: 'GET',
		url: 'teacher_report/show_teacher_subject_parent_daily_date.php',
		data: {tid:tid_id,sid:sid_id},
		success: function(data) {
				$('#date_daily_report_sub_parent_date').html(data);
		}
		});
	
}
//student daily report date wise
function daily_report_sub_student()
{   
  
  tid_id=$("#tid").val();
  sid_id=$("#sid").val();
 
  
		$.ajax({
		type: 'GET',
		url: 'teacher_report/show_teacher_subject_student.php',
		data: {tid:tid_id,sid:sid_id},
		success: function(data) {
				$('#date_daily_report_sub_student').html(data);
		}
		});
	
}

//student daily report date wise
function daily_report_sub_student_date()
{   
  
  tid_id=$("#tid").val();
  sid_id=$("#sid").val();
 
  
		$.ajax({
		type: 'GET',
		url: 'teacher_report/show_teacher_subject_student_date.php',
		data: {tid:tid_id,sid:sid_id},
		success: function(data) {
				$('#date_daily_report_sub_student_date').html(data);
		}
		});
	
}