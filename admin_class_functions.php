<?php


//function to view classes
function admin_view_classes()
{
	
	
	$sno = 0;
	
	
	
	$query_get_classes = "
		SELECT *
		FROM class_index
		ORDER BY class, section ASC
	";
	
	$execute_get_classes = mysql_query($query_get_classes);
	
	
	
	
	 echo '<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Classes </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead>
                                            <tr>
                                                <th>S. no.</th>
                                                <th>Class name</th>
                                                <th>Class</th>
												 <th>Section</th>
												  <th>Total no of students</th>
                                            </tr>
                                        </thead>
                                        <tbody align="center">';
										
										
										
										
										
										while($get_classes = mysql_fetch_array($execute_get_classes))
														{
															echo '<tr>';
															echo '<td align="center">'.++$sno.'</td>';
															echo '<td align="center">'.$get_classes[1].'</td>';
															echo '<td align="center">'.$get_classes[3].'</td>';
															echo '<td align="center">'.$get_classes[2].'</td>';
															
															$query_get_count_students = "
																SELECT COUNT(*) 
																FROM `class`
																WHERE `classId` = $get_classes[0] AND `session_id` = ".$_SESSION['current_session_id']."
															";
															$execute_get_count_students = mysql_query($query_get_count_students);
															$get_count_students = mysql_fetch_array($execute_get_count_students);
															
															echo '<td align="center">'.$get_count_students[0].'</td>';
															
															echo '</tr>';
														}
										
										
										
										
										
										
                                       echo ' </tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div><!-- row-fluid -->';

                                            

	
	
}






//function to add new class
function admin_add_class()
{
	
	
	
	echo '
	<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Classes </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
	
	
	
	
	
	
	 <div class="formEl_b">	
                                        <form id="validation_demo" method="get" action="users/add_new_class.php"> 
                                        <fieldset >
                                        <legend>Add new class</legend>
                                              <div class="section ">
                                              <label> Class</label>   
                                              <div> 
                                              <input type="text" class="validate[required] small" name="class" id="f_required">
                                              </div>
                                              
                                             </div>
                                              <div class="section ">
                                              <label> Section</label>   
                                              <div> 
                                              <input type="text" class="validate[required]  small" name="section" id="e_required">
                                              </div>
                                              </div>
                                             
                                         
                                              <div class="section last">
                                              <div>
											  
                                                <a class="uibutton submit_form  icon add " >Add class</a>
                                                <a class="uibutton" onClick="ResetForm()" title="Reset  Form">Clear Form</a>
                                             </div>
                                             </div>
                                        </fieldset>
                                        </form>
        
                                        </div>
        
                                      </div>
	
	';
	
	
	echo '</div>
	</div>
	</div>
	
	';
	
}








//function to delete a class
function admin_delete_class()
{
	
	
	
	
	
	$sno = 0;
	
	$query_get_classes = "
		SELECT *
		FROM class_index
		ORDER BY class, section ASC
	";
	
	$execute_get_classes = mysql_query($query_get_classes);
	
	
	
	
	 echo '<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Classes </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead>
                                            <tr>
                                                <th>S. no.</th>
                                                <th>Class name</th>
                                                <th>Class</th>
												 <th>Section</th>
												  <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody align="center">';
										
										
										
										
				
				while($get_classes = mysql_fetch_array($execute_get_classes))
								{
									echo '<tr>';
									echo '<td align="center">'.++$sno.'</td>';
									echo '<td align="center">'.$get_classes[1].'</td>';
									echo '<td align="center">'.$get_classes[3].'</td>';
									echo '<td align="center">'.$get_classes[2].'</td>';
									echo '<td><a class="btn btn-danger" 
									href="users/class_delete.php?class='.$get_classes[0].'"><i class="icon-trash icon-white"></i> Delete</a></td>';
									
								}
	
	
	
	
	
	
	
	 echo ' </tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div><!-- row-fluid -->';
}







//function to view subjects
function admin_view_subjects()
{
	
	
	
	
	
	$query_get_subject_details = "
		SELECT *
		FROM subject
		ORDER BY subject ASC
	";
	$execute_get_subject_details = mysql_query($query_get_subject_details);
	
			
				
				if(isset($_GET['subjectAdded']))
				{
					echo '<h2 align="center" style="color:Green">Subject Added Successfully</h2>';
				}
				if(isset($_GET['subjectDeleted']))
				{
					echo '<h2 align="center" style="color:Red">Subject Deleted Successfully</h2>';
				}
				
				
								 echo '<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Subjects </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <table  class="table table-bordered
									 table-striped" id="dataTable" >
									 
                                        <thead>
												  <th>Sno.</th>
												  <th>Subject Code</th>
												  <th>Subject Name</th>	                                         </thead>
											  </tr>
											  
											  <tbody align="center">
											  
											  ';
										$sno = 0;	  
										while($get_subject_details = mysql_fetch_array($execute_get_subject_details))
										{
											echo '<tr>';
											
											echo '<td>'.++$sno.'</td>';
											echo '<td>'.$get_subject_details['subject_code'].'</td>';
											echo '<td><a href="admin_view_subject_details.php?
											id='.$get_subject_details['subject_id'].'" >'.                                             $get_subject_details['subject'].'</a></td>';
											
											echo '</tr>';
										}
											  
					echo'	</tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div><!-- row-fluid -->';					  
										 
}








//function to view subject details
function admin_view_subject_details()
{
	
	
	
	
	
	//Function to show all the details of the subject
	
	$subject_id = $_GET['id'];
	
	//Query to get the details of the subject
	$query_get_subject_details = "
		SELECT `subject_code`, `subject`
		FROM `subject`
		WHERE `subject_id` = '$subject_id'
	";
	
	$execute_get_subject_details = mysql_query($query_get_subject_details);
	$get_subject_details = mysql_fetch_array($execute_get_subject_details);
	
	$subject_code = $get_subject_details['subject_code'];
	$subject_name = $get_subject_details['subject'];
	
	//Query to get the teachers and class details of the subject
	
	$query_get_class_teacher_subject = "
		SELECT users.Name, teachers.class
		FROM teachers
		
		INNER JOIN users
		ON teachers.tId = users.uId
		
		WHERE teachers.subject_id = $subject_id AND session_id = ".$_SESSION['current_session_id']."
	";
	
	$execute_get_class_teacher_subject = mysql_query($query_get_class_teacher_subject);
	

			echo '<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Subject details </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                   
											  
											 
					<h3>Subject Code : '.$subject_code.'</h3>
					<h3>Subject Name : '.$subject_name.'</h3>
				</table>
				
										  <table  class="table table-bordered
									 table-striped" id="dataTable" >
									  <thead>
                                        
											  
											 
											  <tr>
												  <th width="75px">Sno.</th>
												  <th width="175px">Teacher Name</th>
												  <th align="center" width="100px">Class</th>	  
											  </tr>
								     </thead>
											   <tbody align="center">';
									$sno = 0;		  
									while($get_class_teacher_subject = mysql_fetch_array($execute_get_class_teacher_subject))
									{
										echo '<tr>';
										echo '<td>'.++$sno.'</td>';
										echo '<td>'.$get_class_teacher_subject[0].'</td>';
										echo '<td>'.$get_class_teacher_subject[1].'</td>';
										echo '</tr>';
									}
											  
					echo '				</tbody>		  
										  </table>
										   </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div><!-- row-fluid -->
				';
	
}







//function to add new subject
function admin_add_subject()
{
	
	
	
	echo '
	<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Subjects </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
	
	
	
	
	
	
	 <div class="formEl_b">	
                                        <form id="validation_demo" method="get" action="users/add_new_subject.php"> 
                                        <fieldset >
                                        <legend>Add new subject</legend>
                                              <div class="section ">
                                              <label> Subject code</label>   
                                              <div> 
                                              <input type="text" class="validate[required] small" name="subject_code" id="f_required">
                                              </div>
                                              
                                             </div>
                                              <div class="section ">
                                              <label> Subject</label>   
                                              <div> 
                                              <input type="text" class="validate[required]  small" name="subject" id="e_required">
                                              </div>
                                              </div>
                                             
                                         
                                              <div class="section last">
                                              <div>
											  
                                                <a class="uibutton submit_form  icon add " >
												Add subject</a>
                                                <a class="uibutton" onClick="ResetForm()" title="Reset  Form">Clear Form</a>
                                             </div>
                                             </div>
                                        </fieldset>
                                        </form>
        
                                        </div>
        
                                      </div>
	
	';
	
	
	echo '</div>
	</div>
	</div>
	
	';
	
}







//function to delete subject
function admin_delete_subject()
{
	
	
	
	$query_get_subject_details = "
		SELECT *
		FROM subject
		ORDER BY subject ASC
	";
	$execute_get_subject_details = mysql_query($query_get_subject_details);
	
			
				
				
								 echo '<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Subjects </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <table  class="table table-bordered
									 table-striped" id="dataTable" >
									 
                                        <thead>
												  <th>Sno.</th>
												  <th>Subject Code</th>
												  <th>Subject Name</th>
												  <th>Action</th>	                                         </thead>
											  </tr>
											  
											  <tbody align="center">
											  
											  ';
										$sno = 0;	  
										while($get_subject_details = mysql_fetch_array($execute_get_subject_details))
										{
											echo '<tr>';
											
											echo '<td>'.++$sno.'</td>';
											echo '<td>'.$get_subject_details['subject_code'].'</td>';
											echo '<td>'.$get_subject_details['subject'].'</td>';
											
											echo '<td><a class="btn btn-danger" 
									href="users/subject_delete.php?subject_id='.$get_subject_details['subject_id'].'">
									<i class="icon-trash icon-white"></i> Delete</a></td>';
											
											echo '</tr>';
										}
											  
					echo'	</tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div><!-- row-fluid -->';
}
?>