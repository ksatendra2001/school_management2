                                           //for new item request
function change_button(val,val1,td_id)
{
	//called for changing request/cancel button along with change in database in status_code of user_request_status
	
	//Val stores the status of the button
	//val1 stores the item ID
	//td_id stores the ID of the TD
	
	var xmlhttp;    

	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
	 	if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById(td_id).innerHTML=xmlhttp.responseText;
			//document.write(xmlhttp.responseText);
		}
	}
	xmlhttp.open("GET","processing/change_button_onclick.php?val="+val+"&val1="+val1+"&td_id="+td_id,true);
	xmlhttp.send();
}

                                        //for service  requests
function request_service(val_service,val1_service,td_id_service)
{
	//called for changing request/cancel button along with change in database in status_code of user_request_status
	
	//Val stores the status of the button
	//val1 stores the item ID
	//td_id stores the ID of the TD

	
	var xmlhttp;    

	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
	 	if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById(td_id_service).innerHTML=xmlhttp.responseText;
			//document.write(xmlhttp.responseText);
		}
	}
	
	xmlhttp.open("GET","processing/service_request.php?val_service="+val_service+"&val1_service="+val1_service+"&td_id_service="+td_id_service,true);
	xmlhttp.send();
}
                                              //for dumping requests
function request_dumping(val_dumping,val1_dumping,td_id_dumping)
{
	//called for changing request/cancel button along with change in database in status_code of user_request_status
	
	//Val stores the status of the button
	//val1 stores the item ID
	//td_id stores the ID of the TD
	
	var xmlhttp;    

	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
	 	if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById(td_id_dumping).innerHTML=xmlhttp.responseText;
			//document.write(xmlhttp.responseText);
		}
	}
	xmlhttp.open("GET","processing/dumping_request.php?val_dumping="+val_dumping+"&val1_dumping="+val1_dumping+"&td_id_dumping="+td_id_dumping,true);
	xmlhttp.send();
}


                                           
function acknowledge_request(val,status_id)
{
	//acknowlege for admin approval
	
	var xmlhttp;    

	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
	 	if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById(val).innerHTML=xmlhttp.responseText;
			//document.write(xmlhttp.responseText);
		}
	}
	xmlhttp.open("GET","processing/acknowledge.php?val="+val+"&status_id="+status_id,true);
	xmlhttp.send();
}