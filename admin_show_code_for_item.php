<?php
require_once('include/function_admin.php');
require_once('../config/check.php');
require_once('../config/session.php');
require_once('../config/config.php');
require_once('include/admin_item_functions.php');

logged_in();
?>


<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <title>Stock Pile</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="../images/favicon.ico"/> 

        <!-- CSS Stylesheet-->
        <link type="text/css" rel="stylesheet" href="../components/bootstrap/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="../components/bootstrap/bootstrap-responsive.css" />
        <link type="text/css" rel="stylesheet" href="../css/zice.style.css"/>

		
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->  
		<script type="text/javascript" src="js/admin_user.js"></script>
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../components/ui/jquery.ui.min.js"></script> 
		<script type="text/javascript" src="../components/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../components/ui/timepicker.js"></script>
        <script type="text/javascript" src="../components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="../components/form/form.js"></script>
        <script type="text/javascript" src="../components/elfinder/js/elfinder.full.js"></script>
        <script type="text/javascript" src="../components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="../components/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="../components/jscrollpane/jscrollpane.min.js"></script>
        <script type="text/javascript" src="../components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="../components/chosen/chosen.js"></script>
        <script type="text/javascript" src="../components/validationEngine/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="../components/validationEngine/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="../components/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="../components/flot/flot.js"></script>
        <script type="text/javascript" src="../components/uploadify/uploadify.js"></script>       
		<script type="text/javascript" src="../components/Jcrop/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="../components/smartWizard/jquery.smartWizard.min.js"></script>
        <script type="text/javascript" src="../js/jquery.cookie.js"></script>
        <script type="text/javascript" src="../js/zice.custom.js"></script>
        
       
        	
		</head>        
        <body>        
        

<div id="header">
<?php

 top_header();//function for calling header in function_admin.php

?>
</div>


<div id="left_menu">
<?php

 left_menu();//function for calling left menu in function_admin.php

?>
 </div>

<div id="content" >
<div class="inner">
      
<?php
	top_menu();//function for calling top menu in function_admin.php
?>



<?php
   admin_show_code_for_item();//function for adding new item in admin_item_functions
?>




<?php
	footer(); //function for calling footer in function_admin.php
?>

</div> <!--// End inner -->
</div> <!--// End ID content --> 

         <script type="text/javascript">
			$('#item').addClass("select");
		</script>
        </body>
        </html>