<?php
require_once('include/functions_dashboard.php');
require_once('config/config.php');
require_once('include/session.php');
require_once('include/userdetail.php');
require_once('include/check.php');
require_once('include/grades_cce.php');

logged_in();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <title>School Management System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon.ico"/> 

        <!-- CSS Stylesheet-->
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap-responsive.css" />
        <link type="text/css" rel="stylesheet" href="css/zice.style.css"/>

		
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->  
		<script type="text/javascript" src="grades_cce/js/cce.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.ui.min.js"></script> 
		<script type="text/javascript" src="components/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="components/ui/timepicker.js"></script>
        <script type="text/javascript" src="components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="components/form/form.js"></script>
        <script type="text/javascript" src="components/elfinder/js/elfinder.full.js"></script>
        <script type="text/javascript" src="components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="components/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="components/jscrollpane/jscrollpane.min.js"></script>
        <script type="text/javascript" src="components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="components/chosen/chosen.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="components/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="components/flot/flot.js"></script>
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>       
		<script type="text/javascript" src="components/Jcrop/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="components/smartWizard/jquery.smartWizard.min.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/zice.custom.js"></script>
		 


<script type="text/javascript">
$(document).ready(function(){
  $("#flip").click(function(){
    $("#panel").slideToggle();
	$("#panel1").slideUp();
	$("#panel2").slideUp();
	$("#panel3").slideUp();
	
  });
});
</script>

<script type="text/javascript">
$(document).ready(function(){
  $("#flip1").click(function(){
    $("#panel1").slideToggle();
	$("#panel").slideUp();
	$("#panel2").slideUp();
	$("#panel3").slideUp();
  });
});
</script>

<script type="text/javascript">
$(document).ready(function(){
  $("#flip2").click(function(){
    $("#panel2").slideToggle();
	$("#panel1").slideUp();
	$("#panel3").slideUp();
	$("#panel").slideUp();
  });
});
</script>

<script type="text/javascript">
$(document).ready(function(){
  $("#flip3").click(function(){
    $("#panel3").slideToggle();
    $("#panel2").slideUp(); 
	$("#panel1").slideUp(); 
	$("#panel").slideUp(); 
   
	
  });
});
</script>


<style type="text/css"> 
#panel,#flip
{

text-align:center;

}
#panel
{
padding:30px;
display:none;
}
</style>

<style type="text/css"> 
#panel1,#flip1
{

text-align:center;

}
#panel1
{
padding:30px;
display:none;
}
</style>

<style type="text/css"> 
#panel2,#flip2
{

text-align:center;

}
#panel2
{
padding:30px;
display:none;
}
</style>

<style type="text/css"> 
#panel3,#flip3
{

text-align:center;

}
#panel3
{
padding:30px;
display:none;
}
</style>


		</head>        
        <body>        
<div id="header">
<?php

 top_header();//function for calling header in function_admin.php

?>
</div>


<div id="left_menu">
<?php

 left_menu();//function for calling left menu in function_admin.php

?>
 </div>

<div id="content">
<div class="inner">
      
<?php
 top_menu();//function for calling top menu in function_admin.php
?>



<?php
 award_list_term();//function for calling dashboard in function_admin.php
?>




<?php
 footer(); //function for calling footer in function_admin.php
?>       

 <script type="text/javascript">
$("#grades_cce").addClass("select");
</script>  
        </body>
      </html>
