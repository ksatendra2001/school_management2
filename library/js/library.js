function show(id_show)
{
	

  
    $("#panel"+id_show).slideToggle();
	
 


}


//function to prompt
function enter_book(book_id,copies_avai)
{
	
var show_prompt_for_book=window.prompt("Please enter number of books you wish to delete");	
if(show_prompt_for_book>copies_avai)
{
window.alert("Number of books cannot exceed copies available");	
window.location.href = "librarian_delete_book.php";
}
else
{
	var deleted=show_prompt_for_book;




	
var xmlhttp;    

if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
   			window.location.href = "librarian_delete_book.php?del_copies="+deleted;
    }
  }
xmlhttp.open("GET","library/update_copies_avai.php?deleted_copies="+deleted+"&book_id="+book_id,true);

xmlhttp.send();



}
}


//Function to return the book issued
function return_book(sr,id_book,stu_id)
{

	
	
var xmlhttp;    

if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("change_text"+sr).innerHTML=xmlhttp.responseText;
    }
  }

xmlhttp.open("GET","library/change_value_for_flag.php?student_id_ret="+stu_id+"&id_book="+id_book,true);
xmlhttp.send();
	
}

//reissue book



function reissue_book(sr,id)
{
 
var xmlhttp;    
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	var check = xmlhttp.responseText;

	if(check == "error")
	{
		alert("BOOK CAN NOT BE REISSUED!!");
	}

	else
	    	document.getElementById("change_text"+sr).innerHTML=check;
    }
  }

xmlhttp.open("GET","library/reissue_value_for_flag.php?id="+id,true);
xmlhttp.send();

}




//function to prompt to enter quantity limit in lib
function set_new_limit(limit_val)
{
	
	if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("new_time_limit").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","library/set_new_time_limit.php?new_limit="+limit_val,true);
xmlhttp.send();
}
//function for fine
function set_new_fine(fine)
{
	
	if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("new_fine").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","library/set_new_fine.php?fine="+fine,true);
xmlhttp.send();
}




function change_flag(id)
{
	var ele = document.getElementById(id);
	var temp = ele.src;
	ele.src = ele.name;
	ele.name = temp;
	var value = ele.alt;
	
var xmlhttp;    

if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
   			
    }
  }
xmlhttp.open("GET","library/value_for_flag.php?"+value,true);

xmlhttp.send();
}

