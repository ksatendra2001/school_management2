<?php
require_once('include/functions_dashboard.php');
require_once('config/config.php');
require_once('include/session.php');
require_once('include/check.php');
//require_once('users/manage_users_functions.php');
logged_in();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <title>School Management System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon.ico"/> 

        <!-- CSS Stylesheet-->
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap-responsive.css" />
        <link type="text/css" rel="stylesheet" href="css/zice.style.css"/>

		
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->  
		
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.ui.min.js"></script> 
	
        <script type="text/javascript" src="components/ui/timepicker.js"></script>
        <script type="text/javascript" src="components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="components/form/form.js"></script>
       
        <script type="text/javascript" src="components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="components/fancybox/jquery.fancybox.js"></script>
        
        <script type="text/javascript" src="components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="components/chosen/chosen.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js"></script>
        
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>       
		<script type="text/javascript" src="components/Jcrop/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="components/smartWizard/jquery.smartWizard.min.js"></script>

        <script type="text/javascript" src="js/zice.custom.js"></script>
        <script type="text/javascript" src="users/js/manage_users.js"></script>
			
	

		</head>        
        <body>        
<div id="header">

<?php

function manage_students_print_class()
{
    
    
  echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Print Students</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">';
	
	$q="SELECT `cId`,`class_name` FROM class_index ORDER BY class+0 ASC";

$q_res=mysql_query($q);
echo '<h2 align="center">';




echo '</h2>';
echo "<br>";
echo "<br>";
echo "<br>";
 echo '<ol class="rounded-list">';
while($res=mysql_fetch_array($q_res))
{
	
	
	echo '<tr class="row-fluid" id="added_rows">
      <td><div class="row-fluid">
          <div class="span6">							                                                              
      <li><a href="awardlist/new_award_list5.php?class_id='.$res[0].'">'.$res[1].'Class</a>                                                                    	                                 
	</div>
	</div><!-- end row-fluid  -->


</td>
</tr>';	 	 
}
echo '</div>
</div>
</div>';  
    
}

?>

<?php

 top_header();//function for calling header in function_admin.php

?>
</div>


<div id="left_menu">
<?php

 left_menu();//function for calling left menu in function_admin.php

?>
 </div>

<div id="content" >
<div class="inner">
      
<?php
 top_menu();//function for calling top menu in function_admin.php
?>



<?php
     manage_students_print_class();	//This is to generate the dashboard depending on the user privilege; filename=>include/function_menu.php
?>




<?php
 footer(); //function for calling footer in function_admin.php
?>       

<script type="text/javascript">
$("#manage_user").addClass("select");
</script>
        
        </body>
        </html>
