<?php
require_once('../config/config.php');
?>

<style>
table,th,td,tr
{
	
border:1px solid black;
font-size: 24px;
height:40px;
}
p
{
	
}
</style>
<div align="center">
<p align="center" style="font-size:40px;">
VIDYA BAL BHAWAN SR. SEC. SCHOOL<br/>
<span style="font-size:20px">XII CLASS CBSE RESULT ANALYSIS - 2013-14<br/>
RESULT ANALYSIS</span>
</p>
<br/>
<br/>

</div>

<p style="font-size:20px">
Name of the teacher : ............................................................................ <br/><br/>
Total No. of Students : ..........................................................................
</p>
<?php
/*

function calculate_subject_wise_90()
{
	//Method to calculate 90 above in each subject
	
	$array_subject_wise_90 = array();
	
	$ctr =0 ;
	
	$query_get_all = "SELECT * FROM `cgpa_report_class_12`";
	$execute_get_all = mysql_query($query_get_all);
	while($get_all = mysql_fetch_array($execute_get_all))
	{
		if($get_all['english'] >= 90)
		{
			$array_subject_wise_90[$ctr]['english']['name'] = $get_all['name'];
			$array_subject_wise_90[$ctr]['english']['roll_no'] = $get_all['roll_no'];
			$array_subject_wise_90[$ctr]['english']['marks'] = $get_all['english'];
			
			++$ctr;
		}
		
		
		if($get_all['maths'] >= 90)
		{
			$array_subject_wise_90[$ctr]['maths']['name'] = $get_all['name'];
			$array_subject_wise_90[$ctr]['maths']['roll_no'] = $get_all['roll_no'];
			$array_subject_wise_90[$ctr]['maths']['marks'] = $get_all['maths'];
			++$ctr;
		}
		if($get_all['accountancy'] >= 90)
		{
			$array_subject_wise_90[$ctr]['accountancy']['name'] = $get_all['name'];
			$array_subject_wise_90[$ctr]['accountancy']['roll_no'] = $get_all['roll_no'];
			$array_subject_wise_90[$ctr]['accountancy']['marks'] = $get_all['accountancy'];
			++$ctr;
		}
		if($get_all['biology'] >= 90)
		{
			$array_subject_wise_90[$ctr]['biology']['name'] = $get_all['name'];
			$array_subject_wise_90[$ctr]['biology']['roll_no'] = $get_all['roll_no'];
			$array_subject_wise_90[$ctr]['biology']['marks'] = $get_all['biology'];
			++$ctr;
		}
		if($get_all['business_studies'] >= 90)
		{
			$array_subject_wise_90[$ctr]['business_studies']['name'] = $get_all['name'];
			$array_subject_wise_90[$ctr]['business_studies']['roll_no'] = $get_all['roll_no'];
			$array_subject_wise_90[$ctr]['business_studies']['marks'] = $get_all['business_studies'];
			++$ctr;
		}
		if($get_all['chemistry'] >= 90)
		{
			$array_subject_wise_90[$ctr]['chemistry']['name'] = $get_all['name'];
			$array_subject_wise_90[$ctr]['chemistry']['roll_no'] = $get_all['roll_no'];
			$array_subject_wise_90[$ctr]['chemistry']['marks'] = $get_all['chemistry'];
			++$ctr;
		}
		if($get_all['computer_science'] >= 90)
		{
			$array_subject_wise_90[$ctr]['computer_science']['name'] = $get_all['name'];
			$array_subject_wise_90[$ctr]['computer_science']['roll_no'] = $get_all['roll_no'];
			$array_subject_wise_90[$ctr]['computer_science']['marks'] = $get_all['computer_science'];
			++$ctr;
		}
		if($get_all['economics'] >= 90)
		{
			$array_subject_wise_90[$ctr]['economics']['name'] = $get_all['name'];
			$array_subject_wise_90[$ctr]['economics']['roll_no'] = $get_all['roll_no'];
			$array_subject_wise_90[$ctr]['economics']['marks'] = $get_all['economics'];
			++$ctr;
		}
		if($get_all['physical_education'] >= 90)
		{
			$array_subject_wise_90[$ctr]['physical_education']['name'] = $get_all['name'];
			$array_subject_wise_90[$ctr]['physical_education']['roll_no'] = $get_all['roll_no'];
			$array_subject_wise_90[$ctr]['physical_education']['marks'] = $get_all['physical_education'];
			++$ctr;
		}
		if($get_all['physics'] >= 90)
		{
			$array_subject_wise_90[$ctr]['physics']['name'] = $get_all['name'];
			$array_subject_wise_90[$ctr]['physics']['roll_no'] = $get_all['roll_no'];
			$array_subject_wise_90[$ctr]['physics']['marks'] = $get_all['physics'];
			++$ctr;
		}
	}
	
	return $array_subject_wise_90;
}




$array_subject_wise_90 = array();
$array_subject_wise_90 = calculate_subject_wise_90();

echo '<table align="center">';
echo '<tr><th colspan="3">Above 90 in English </th></tr>';
foreach($array_subject_wise_90 as $subject_wise_90)
{
	if(isset($subject_wise_90['english']['name']))
	{
		echo '<tr><td>'.$subject_wise_90['english']['roll_no'].'</td><td>'.$subject_wise_90['english']['name'].'</td><td>'.$subject_wise_90['english']['marks'].'</td></tr>';
	}
	
}

echo '</table>';

echo '<table align="center">';
echo '<tr><th colspan="3">Above 90 in Physics </th></tr>';
foreach($array_subject_wise_90 as $subject_wise_90)
{
	
	if(isset($subject_wise_90['physics']['name']))
	{
		echo '<tr><td>'.$subject_wise_90['physics']['roll_no'].'</td><td>'.$subject_wise_90['physics']['name'].'</td><td>'.$subject_wise_90['physics']['marks'].'</td></tr>';
	}
	
}
echo '</table>';

echo '<br/>';

echo '<table align="center">';
echo '<tr><th colspan="3">Above 90 in Business Studies </th></tr>';
foreach($array_subject_wise_90 as $subject_wise_90)
{
	if(isset($subject_wise_90['business_studies']['name']))
	{
		echo '<tr><td>'.$subject_wise_90['business_studies']['roll_no'].'</td><td>'.$subject_wise_90['business_studies']['name'].'</td><td>'.$subject_wise_90['business_studies']['marks'].'</td></tr>';
	}
	
}
echo '</table>';

echo '<br/>';

echo '<table align="center">';
echo '<tr><th colspan="3">Above 90 in Chemistry </th></tr>';
foreach($array_subject_wise_90 as $subject_wise_90)
{
	
	if(isset($subject_wise_90['chemistry']['name']))
	{
		echo '<tr><td>'.$subject_wise_90['chemistry']['roll_no'].'</td><td>'.$subject_wise_90['chemistry']['name'].'</td><td>'.$subject_wise_90['chemistry']['marks'].'</td></tr>';
	}
	
}
echo '</table>';

echo '<br/>';

echo '<table align="center">';
echo '<tr><th colspan="3">Above 90 in Biology </th></tr>';
foreach($array_subject_wise_90 as $subject_wise_90)
{
	
	if(isset($subject_wise_90['biology']['name']))
	{
		echo '<tr><td>'.$subject_wise_90['biology']['roll_no'].'</td><td>'.$subject_wise_90['biology']['name'].'</td><td>'.$subject_wise_90['biology']['marks'].'</td></tr>';
	}
	
}
echo '</table>';

echo '<br/>';

echo '<table align="center">';
echo '<tr><th colspan="3">Above 90 in Maths </th></tr>';
foreach($array_subject_wise_90 as $subject_wise_90)
{
	
	if(isset($subject_wise_90['maths']['name']))
	{
		echo '<tr><td>'.$subject_wise_90['maths']['roll_no'].'</td><td>'.$subject_wise_90['maths']['name'].'</td><td>'.$subject_wise_90['maths']['marks'].'</td></tr>';
	}
	
}
echo '</table>';

echo '<br/>';

echo '<table align="center">';
echo '<tr><th colspan="3">Above 90 in Computer Science </th></tr>';
foreach($array_subject_wise_90 as $subject_wise_90)
{


	if(isset($subject_wise_90['computer_science']['name']))
	{
		echo '<tr><td>'.$subject_wise_90['computer_science']['roll_no'].'</td><td>'.$subject_wise_90['computer_science']['name'].'</td><td>'.$subject_wise_90['computer_science']['marks'].'</td></tr>';
	}
	
}
echo '</table>';

echo '<br/>';

echo '<table align="center">';
echo '<tr><th colspan="3">Above 90 in Economics </th></tr>';
foreach($array_subject_wise_90 as $subject_wise_90)
{
	if(isset($subject_wise_90['economics']['name']))
	{
		echo '<tr><td>'.$subject_wise_90['economics']['roll_no'].'</td><td>'.$subject_wise_90['economics']['name'].'</td><td>'.$subject_wise_90['economics']['marks'].'</td></tr>';
	}

}
echo '</table>';

echo '<br/>';

echo '<table align="center">';
echo '<tr><th colspan="3">Above 90 in Physical Education </th></tr>';
foreach($array_subject_wise_90 as $subject_wise_90)
{
	
	if(isset($subject_wise_90['physical_education']['name']))
	{
		echo '<tr><td>'.$subject_wise_90['physical_education']['roll_no'].'</td><td>'.$subject_wise_90['physical_education']['name'].'</td><td>'.$subject_wise_90['physical_education']['marks'].'</td></tr>';
	}
}
echo '</table>';

echo '<br/>';

echo '<table align="center">';
echo '<tr><th colspan="3">Above 90 in Accountancy </th></tr>';
foreach($array_subject_wise_90 as $subject_wise_90)
{

	if(isset($subject_wise_90['accountancy']['name']))
	{
		echo '<tr><td>'.$subject_wise_90['accountancy']['roll_no'].'</td><td>'.$subject_wise_90['accountancy']['name'].'</td><td>'.$subject_wise_90['accountancy']['marks'].'</td></tr>';
	}
}
echo '</table>';

*/


////////////////////////////////////////  BreakUp in slabs /////////////////////////////////////////////////////////

english_result_analysis();
physics_result_analysis();
business_studies_result_analysis();
chemistry_result_analysis();
biology_result_analysis();
maths_result_analysis();
computer_science_result_analysis();
economics_result_analysis();
physical_education_result_analysis();
accountancy_result_analysis();


//English
function english_result_analysis()
{
	echo '<table align="center" width="100%" style="border:1px solid black;border-collapse:collapse;">';
	echo '
	<tr><th colspan="9">ENGLISH</th></tr>
	<tr>
			<th>90 - 100 </th>
			<th>80 - 89 </th>
			<th>75 - 79 </th>
			<th>65 - 74 </th>
			<th>50 - 64 </th>
			<th>40 - 49 </th>
			<th>34 - 39 </th>
			<th>33 </th>
			<th>Below 33</th>
	</tr>';
	
	echo '<tr align="center" >';
	$dist = 0;
	$query90_100 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `english` BETWEEN 90 AND 100");
	$value = mysql_fetch_array($query90_100);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query80_89 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `english` BETWEEN 80 AND 89");
	$value = mysql_fetch_array($query80_89);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query75_79 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `english` BETWEEN 75 AND 79");
	$value = mysql_fetch_array($query75_79);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query65_74 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `english` BETWEEN 65 AND 74");
	$value = mysql_fetch_array($query65_74);
	echo '<td>'.$value[0].'</td>';
	
	$query50_64 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `english` BETWEEN 50 AND 64");
	$value = mysql_fetch_array($query50_64);
	echo '<td>'.$value[0].'</td>';
	
	$query40_49 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `english` BETWEEN 40 AND 49");
	$value = mysql_fetch_array($query40_49);
	echo '<td>'.$value[0].'</td>';
	
	$query34_39 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `english` BETWEEN 34 AND 39");
	$value = mysql_fetch_array($query34_39);
	echo '<td>'.$value[0].'</td>';
	
	$query33 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `english` = 33");
	$value = mysql_fetch_array($query33);
	echo '<td>'.$value[0].'</td>';
	
	$query_below_33 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `english` BETWEEN 1 AND 32");
	$value = mysql_fetch_array($query_below_33);
	echo '<td>'.$value[0].'</td>';
	
	echo '</tr>';
	echo '<tr><td colspan="9">Total Distinctions : '.$dist.'</td></tr>';
	$query_highest = mysql_query("SELECT max(english) FROM `cgpa_report_class_12`");
	$highest = mysql_fetch_array($query_highest);
	echo '<tr><td colspan="9">Highest Marks : '.$highest[0].'</td></tr>';
	
	$query_highest = mysql_query("SELECT min(english) FROM `cgpa_report_class_12`");
	$lowest = mysql_fetch_array($query_highest);
	echo '<tr><td colspan="9">Lowest Marks : '.$lowest[0].'</td></tr>';
	
	echo '<tr><td colspan="9">Compartment : </td></tr>';
	
	echo '</table>';
	
}

//Physics
function physics_result_analysis()
{
	echo '<table align="center" width="100%" style="border:1px solid black;border-collapse:collapse;">';
	echo '
	<tr><th colspan="9">Physics</th></tr>
	<tr>
			<th>90 - 100 </th>
			<th>80 - 89 </th>
			<th>75 - 79 </th>
			<th>65 - 74 </th>
			<th>50 - 64 </th>
			<th>40 - 49 </th>
			<th>34 - 39 </th>
			<th>33 </th>
			<th>Below 33</th>
	</tr>';
	
	echo '<tr align="center" >';
	$dist = 0;
	$query90_100 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `physics` BETWEEN 90 AND 100");
	$value = mysql_fetch_array($query90_100);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query80_89 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `physics` BETWEEN 80 AND 89");
	$value = mysql_fetch_array($query80_89);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query75_79 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `physics` BETWEEN 75 AND 79");
	$value = mysql_fetch_array($query75_79);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query65_74 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `physics` BETWEEN 65 AND 74");
	$value = mysql_fetch_array($query65_74);
	echo '<td>'.$value[0].'</td>';
	
	$query50_64 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `physics` BETWEEN 50 AND 64");
	$value = mysql_fetch_array($query50_64);
	echo '<td>'.$value[0].'</td>';
	
	$query40_49 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `physics` BETWEEN 40 AND 49");
	$value = mysql_fetch_array($query40_49);
	echo '<td>'.$value[0].'</td>';
	
	$query34_39 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `physics` BETWEEN 34 AND 39");
	$value = mysql_fetch_array($query34_39);
	echo '<td>'.$value[0].'</td>';
	
	$query33 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `physics` = 33");
	$value = mysql_fetch_array($query33);
	echo '<td>'.$value[0].'</td>';
	
	$query_below_33 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `physics` BETWEEN 1 AND 32");
	$value = mysql_fetch_array($query_below_33);
	echo '<td>'.$value[0].'</td>';
	
	echo '</tr>';
	echo '<tr><td colspan="9">Total Distinctions : '.$dist.'</td></tr>';
	$query_highest = mysql_query("SELECT max(physics) FROM `cgpa_report_class_12`");
	$highest = mysql_fetch_array($query_highest);
	echo '<tr><td colspan="9">Highest Marks : '.$highest[0].'</td></tr>';
	
	$query_highest = mysql_query("SELECT min(physics) FROM `cgpa_report_class_12` WHERE `physics` <> 0");
	$lowest = mysql_fetch_array($query_highest);
	echo '<tr><td colspan="9">Lowest Marks : '.$lowest[0].'</td></tr>';
	
	echo '<tr><td colspan="9">Compartment : </td></tr>';
	
	echo '</table>';
	

}

//Business Studies
function business_studies_result_analysis()
{
	echo '<table align="center" width="100%" style="border:1px solid black;border-collapse:collapse;">';
	echo '
	<tr><th colspan="9">Business studies</th></tr>
	<tr>
			<th>90 - 100 </th>
			<th>80 - 89 </th>
			<th>75 - 79 </th>
			<th>65 - 74 </th>
			<th>50 - 64 </th>
			<th>40 - 49 </th>
			<th>34 - 39 </th>
			<th>33 </th>
			<th>Below 33</th>
	</tr>';
	
	echo '<tr align="center" >';
	$dist = 0;
	$query90_100 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `business_studies` BETWEEN 90 AND 100");
	$value = mysql_fetch_array($query90_100);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query80_89 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `business_studies` BETWEEN 80 AND 89");
	$value = mysql_fetch_array($query80_89);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query75_79 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `business_studies` BETWEEN 75 AND 79");
	$value = mysql_fetch_array($query75_79);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query65_74 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `business_studies` BETWEEN 65 AND 74");
	$value = mysql_fetch_array($query65_74);
	echo '<td>'.$value[0].'</td>';
	
	$query50_64 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `business_studies` BETWEEN 50 AND 64");
	$value = mysql_fetch_array($query50_64);
	echo '<td>'.$value[0].'</td>';
	
	$query40_49 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `business_studies` BETWEEN 40 AND 49");
	$value = mysql_fetch_array($query40_49);
	echo '<td>'.$value[0].'</td>';
	
	$query34_39 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `business_studies` BETWEEN 34 AND 39");
	$value = mysql_fetch_array($query34_39);
	echo '<td>'.$value[0].'</td>';
	
	$query33 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `business_studies` = 33");
	$value = mysql_fetch_array($query33);
	echo '<td>'.$value[0].'</td>';
	
	$query_below_33 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `business_studies` BETWEEN 1 AND 32");
	$value = mysql_fetch_array($query_below_33);
	echo '<td>'.$value[0].'</td>';
	
	echo '</tr>';
	echo '<tr><td colspan="9">Total Distinctions : '.$dist.'</td></tr>';
	$query_highest = mysql_query("SELECT max(business_studies) FROM `cgpa_report_class_12`");
	$highest = mysql_fetch_array($query_highest);
	echo '<tr><td colspan="9">Highest Marks : '.$highest[0].'</td></tr>';
	
	$query_highest = mysql_query("SELECT min(business_studies) FROM `cgpa_report_class_12` WHERE `business_studies` <> 0");
	$lowest = mysql_fetch_array($query_highest);
	echo '<tr><td colspan="9">Lowest Marks : '.$lowest[0].'</td></tr>';
	
	echo '<tr><td colspan="9">Compartment : </td></tr>';
	
	echo '</table>';
	


}

//Chemistry
function chemistry_result_analysis()
{
	echo '<table align="center" width="100%" style="border:1px solid black;border-collapse:collapse;">';
	echo '
	<tr><th colspan="9">Chemistry</th></tr>
	<tr>
			<th>90 - 100 </th>
			<th>80 - 89 </th>
			<th>75 - 79 </th>
			<th>65 - 74 </th>
			<th>50 - 64 </th>
			<th>40 - 49 </th>
			<th>34 - 39 </th>
			<th>33 </th>
			<th>Below 33</th>
	</tr>';
	
	echo '<tr align="center" >';
	$dist = 0;
	$query90_100 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `chemistry` BETWEEN 90 AND 100");
	$value = mysql_fetch_array($query90_100);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query80_89 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `chemistry` BETWEEN 80 AND 89");
	$value = mysql_fetch_array($query80_89);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query75_79 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `chemistry` BETWEEN 75 AND 79");
	$value = mysql_fetch_array($query75_79);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query65_74 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `chemistry` BETWEEN 65 AND 74");
	$value = mysql_fetch_array($query65_74);
	echo '<td>'.$value[0].'</td>';
	
	$query50_64 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `chemistry` BETWEEN 50 AND 64");
	$value = mysql_fetch_array($query50_64);
	echo '<td>'.$value[0].'</td>';
	
	$query40_49 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `chemistry` BETWEEN 40 AND 49");
	$value = mysql_fetch_array($query40_49);
	echo '<td>'.$value[0].'</td>';
	
	$query34_39 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `chemistry` BETWEEN 34 AND 39");
	$value = mysql_fetch_array($query34_39);
	echo '<td>'.$value[0].'</td>';
	
	$query33 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `chemistry` = 33");
	$value = mysql_fetch_array($query33);
	echo '<td>'.$value[0].'</td>';
	
	$query_below_33 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `chemistry` BETWEEN 1 AND 32");
	$value = mysql_fetch_array($query_below_33);
	echo '<td>'.$value[0].'</td>';
	
	echo '</tr>';
	echo '<tr><td colspan="9">Total Distinctions : '.$dist.'</td></tr>';
	$query_highest = mysql_query("SELECT max(chemistry) FROM `cgpa_report_class_12`");
	$highest = mysql_fetch_array($query_highest);
	echo '<tr><td colspan="9">Highest Marks : '.$highest[0].'</td></tr>';
	
	$query_highest = mysql_query("SELECT min(chemistry) FROM `cgpa_report_class_12` WHERE `chemistry` <> 0");
	$lowest = mysql_fetch_array($query_highest);
	echo '<tr><td colspan="9">Lowest Marks : '.$lowest[0].'</td></tr>';
	
	echo '<tr><td colspan="9">Compartment : </td></tr>';
	
	echo '</table>';
	

}

//Biology
function biology_result_analysis()
{
	echo '<table align="center" width="100%" style="border:1px solid black;border-collapse:collapse;">';
	echo '
	<tr><th colspan="9">Biology</th></tr>
	<tr>
			<th>90 - 100 </th>
			<th>80 - 89 </th>
			<th>75 - 79 </th>
			<th>65 - 74 </th>
			<th>50 - 64 </th>
			<th>40 - 49 </th>
			<th>34 - 39 </th>
			<th>33 </th>
			<th>Below 33</th>
	</tr>';
	
	echo '<tr align="center" >';
	$dist = 0;
	$query90_100 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `biology` BETWEEN 90 AND 100");
	$value = mysql_fetch_array($query90_100);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query80_89 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `biology` BETWEEN 80 AND 89");
	$value = mysql_fetch_array($query80_89);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query75_79 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `biology` BETWEEN 75 AND 79");
	$value = mysql_fetch_array($query75_79);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query65_74 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `biology` BETWEEN 65 AND 74");
	$value = mysql_fetch_array($query65_74);
	echo '<td>'.$value[0].'</td>';
	
	$query50_64 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `biology` BETWEEN 50 AND 64");
	$value = mysql_fetch_array($query50_64);
	echo '<td>'.$value[0].'</td>';
	
	$query40_49 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `biology` BETWEEN 40 AND 49");
	$value = mysql_fetch_array($query40_49);
	echo '<td>'.$value[0].'</td>';
	
	$query34_39 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `biology` BETWEEN 34 AND 39");
	$value = mysql_fetch_array($query34_39);
	echo '<td>'.$value[0].'</td>';
	
	$query33 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `biology` = 33");
	$value = mysql_fetch_array($query33);
	echo '<td>'.$value[0].'</td>';
	
	$query_below_33 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `biology` BETWEEN 1 AND 32");
	$value = mysql_fetch_array($query_below_33);
	echo '<td>'.$value[0].'</td>';
	
	echo '</tr>';
	echo '<tr><td colspan="9">Total Distinctions : '.$dist.'</td></tr>';
	$query_highest = mysql_query("SELECT max(biology) FROM `cgpa_report_class_12`");
	$highest = mysql_fetch_array($query_highest);
	echo '<tr><td colspan="9">Highest Marks : '.$highest[0].'</td></tr>';
	
	$query_highest = mysql_query("SELECT min(biology) FROM `cgpa_report_class_12` WHERE `biology` <> 0");
	$lowest = mysql_fetch_array($query_highest);
	echo '<tr><td colspan="9">Lowest Marks : '.$lowest[0].'</td></tr>';
	
	echo '<tr><td colspan="9">Compartment : </td></tr>';
	
	echo '</table>';
	

}

//Maths
function maths_result_analysis()
{
	
	echo '<table align="center" width="100%" style="border:1px solid black;border-collapse:collapse;">';
	echo '
	<tr><th colspan="9">Maths</th></tr>
	<tr>
			<th>90 - 100 </th>
			<th>80 - 89 </th>
			<th>75 - 79 </th>
			<th>65 - 74 </th>
			<th>50 - 64 </th>
			<th>40 - 49 </th>
			<th>34 - 39 </th>
			<th>33 </th>
			<th>Below 33</th>
	</tr>';
	
	echo '<tr align="center" >';
	$dist = 0;
	$query90_100 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `maths` BETWEEN 90 AND 100");
	$value = mysql_fetch_array($query90_100);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query80_89 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `maths` BETWEEN 80 AND 89");
	$value = mysql_fetch_array($query80_89);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query75_79 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `maths` BETWEEN 75 AND 79");
	$value = mysql_fetch_array($query75_79);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query65_74 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `maths` BETWEEN 65 AND 74");
	$value = mysql_fetch_array($query65_74);
	echo '<td>'.$value[0].'</td>';
	
	$query50_64 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `maths` BETWEEN 50 AND 64");
	$value = mysql_fetch_array($query50_64);
	echo '<td>'.$value[0].'</td>';
	
	$query40_49 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `maths` BETWEEN 40 AND 49");
	$value = mysql_fetch_array($query40_49);
	echo '<td>'.$value[0].'</td>';
	
	$query34_39 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `maths` BETWEEN 34 AND 39");
	$value = mysql_fetch_array($query34_39);
	echo '<td>'.$value[0].'</td>';
	
	$query33 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `maths` = 33");
	$value = mysql_fetch_array($query33);
	echo '<td>'.$value[0].'</td>';
	
	$query_below_33 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `maths` BETWEEN 1 AND 32");
	$value = mysql_fetch_array($query_below_33);
	echo '<td>'.$value[0].'</td>';
	
	echo '</tr>';
	echo '<tr><td colspan="9">Total Distinctions : '.$dist.'</td></tr>';
	$query_highest = mysql_query("SELECT max(maths) FROM `cgpa_report_class_12`");
	$highest = mysql_fetch_array($query_highest);
	echo '<tr><td colspan="9">Highest Marks : '.$highest[0].'</td></tr>';
	
	$query_highest = mysql_query("SELECT min(maths) FROM `cgpa_report_class_12` WHERE `maths` <> 0");
	$lowest = mysql_fetch_array($query_highest);
	echo '<tr><td colspan="9">Lowest Marks : '.$lowest[0].'</td></tr>';
	
	echo '<tr><td colspan="9">Compartment : </td></tr>';
	
	echo '</table>';
	
}

//Computer Science
function computer_science_result_analysis()
{
	echo '<table align="center" width="100%" style="border:1px solid black;border-collapse:collapse;">';
	echo '
	<tr><th colspan="9">Computer science</th></tr>
	<tr>
			<th>90 - 100 </th>
			<th>80 - 89 </th>
			<th>75 - 79 </th>
			<th>65 - 74 </th>
			<th>50 - 64 </th>
			<th>40 - 49 </th>
			<th>34 - 39 </th>
			<th>33 </th>
			<th>Below 33</th>
	</tr>';
	
	echo '<tr align="center" >';
	$dist = 0;
	$query90_100 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `computer_science` BETWEEN 90 AND 100");
	$value = mysql_fetch_array($query90_100);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query80_89 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `computer_science` BETWEEN 80 AND 89");
	$value = mysql_fetch_array($query80_89);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query75_79 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `computer_science` BETWEEN 75 AND 79");
	$value = mysql_fetch_array($query75_79);
	echo '<td>'.$value[0].'</td>';
	$dist = $dist + $value[0];
	
	$query65_74 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `computer_science` BETWEEN 65 AND 74");
	$value = mysql_fetch_array($query65_74);
	echo '<td>'.$value[0].'</td>';
	
	$query50_64 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `computer_science` BETWEEN 50 AND 64");
	$value = mysql_fetch_array($query50_64);
	echo '<td>'.$value[0].'</td>';
	
	$query40_49 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `computer_science` BETWEEN 40 AND 49");
	$value = mysql_fetch_array($query40_49);
	echo '<td>'.$value[0].'</td>';
	
	$query34_39 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `computer_science` BETWEEN 34 AND 39");
	$value = mysql_fetch_array($query34_39);
	echo '<td>'.$value[0].'</td>';
	
	$query33 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `computer_science` = 33");
	$value = mysql_fetch_array($query33);
	echo '<td>'.$value[0].'</td>';
	
	$query_below_33 = mysql_query("SELECT count(*) FROM `cgpa_report_class_12` WHERE `computer_science` BETWEEN 1 AND 32");
	$value = mysql_fetch_array($query_below_33);
	echo '<td>'.$value[0].'</td>';
	
	echo '</tr>';
	echo '<tr><td colspan="9">Total Distinctions : '.$dist.'</td></tr>';
	$query_highest = mysql_query("SELECT max(computer_science) FROM `cgpa_report_class_12`");
	$highest = mysql_fetch_array($query_highest);
	echo '<tr><td colspan="9">Highest