<?php
require_once('../config/config.php');
$student_id = $_GET['roll_no'];

date_default_timezone_set ('Asia/Calcutta');
$date = date('d-F-Y');

/*$query_name_father_name = "
	SELECT `Name`,`Father's Name`,`gender`
	FROM `student_user`
	WHERE sId = $student_id
";
$execute_details = mysql_query($query_name_father_name);
$details = mysql_fetch_array($execute_details);
$name = $details[0];
$f_name = $details[1];
$gender = $details[2];*/

$query_get_12th_marks = "SELECT * FROM `cgpa_report_class_12` WHERE `roll_no` = $student_id";
$execute_get_12th_marks = mysql_query($query_get_12th_marks);
$get_12th_marks = mysql_fetch_array($execute_get_12th_marks);
$name = $get_12th_marks['name'];
$f_name = $get_12th_marks['father_name'];
$gender = $get_12th_marks['gender'];
$ctr = 0;


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CHARACTER CERTIFICATE</title>
</head>

<body style="font:'Times New Roman', Times, serif">

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width="100%" height="424">
<tr>
	<td height="73" align="right" valign="top"><p><strong>Date : <?php echo $date; ?></strong></p></td>
</tr>

<tr>
	<td align="center">
    <p style="font-size:24px"><u><strong>CHARACTER CERTIFICATE</strong></u></p>
    <p>&nbsp;</p></td>
</tr>

<tr>
	<td>
	  <p style="font-size:18px; text-align:justify; line-height:2; margin-bottom:0px">
            	Certified that <b><?php echo $name ?></b><?php 
					if($gender == 'MALE')
					{
						echo ' S/O ';
					}
					elseif($gender == 'FEMALE')
					{
						echo ' D/O ';
					}
				
				?><b> Mr. <?php echo $f_name ?></b> was a bonafide student of class XII. 
                <?php 
					if($gender == 'MALE')
					{
						echo 'He';
					}
					elseif($gender == 'FEMALE')
					{
						echo 'She';
					}
				
				?>
                
                 has appeared in All India Senior Secondary School Certificate Examination held in March 2016 and was declared passed.
      </p>
    </td>
</tr>

<tr>
	<td>
   	  <p style="font-size:18px; text-align:justify;">
        	<?php 
					if($gender == 'MALE')
					{
						echo 'He';
					}
					elseif($gender == 'FEMALE')
					{
						echo 'She';
					}
				
				?> bears a good moral character.
      </p>
    </td>
</tr>

<tr>
	<td height="100" valign="bottom">
    	<p style="text-align:justify;">&nbsp;
       	</p>
    	<p style="text-align:justify;">&nbsp;</p>
   	  <p style="text-align:justify;margin-top: 7%">Dr. S.V SHARMA <br/></p>
    </td>
</tr>
</table>

</body>
</html>