<?php
//header( 'Content-Type: text/html; charset=utf-8' ); 


require_once('../config/config.php');


//$class_id=$_GET['class_id'];
//$student_id=$_GET['student_id'];
?>
<style>
table,th,td,tr
{
border:1px solid black;
font-size: 16px;
}
</style>

<?php
 
function calculate_grade($m)
{
   $grade = '';
   if($m<= 10 && $m>= 9.1)
   {
     $grade = 'A1';	
   }
 else if($m<= 9 && $m>= 8.1)
    {
      $grade = 'A2';
    }
    else if($m<= 8 && $m>= 7.1)
    {
      $grade = 'B';
    }
    else if($m<= 7 && $m>= 61.1)
    {
      $grade = 'C';
    }
    else if($m<= 6 && $m>= 5.1)
    {
      $grade = 'D';
    }
    else if($m<= 5 && $m>= 4.1)
    {
      $grade = 'E';
    }
    else if($m<= 4 && $m>= 3)
    {
      $grade = 'F';
    }
 else 
    {
        $grade = 'G';
     }
    return $grade;
}

 ?>

<?php
//query to get the total student
  $get_total_student = "SELECT count(*) AS total FROM cgpa_report";
  $exe_total_student = mysql_query($get_total_student);
  $fetch_total_student = mysql_fetch_array($exe_total_student);
   //echo '<h2 style="color:#FF0000;text-decoration:underline;" align="center">VIDYA BAL BHAWAN SR. SEC. SCHOOL</h2>';
   //echo '<h3 style="color:orange;text-decoration:underline;" align="center">CLASS 10th BOARD RESULT</h3>';
  // echo '<h4 style="color:#79BAEC" align="center">STUDENTS APPEARED -: '.$fetch_total_student['total'].'</h4>';
   //query to get the total student who get cgpa 10
  $get_cgpa_10 = "SELECT count(*) AS total FROM cgpa_report WHERE cgpa=10";
  $exe_cgpa_10 = mysql_query($get_cgpa_10 );
  $fetch_cgpa_10 = mysql_fetch_array($exe_cgpa_10);
   //echo '<h3 style="color:#79BAEC" align="center">CGPA 10 = '.$fetch_cgpa_10['total'].'</h3>';
   //query to get the total student who get cgpa CGPA 9 & > 9
  $get_cgpa_9 = "SELECT count(*) AS total FROM cgpa_report WHERE cgpa>= 9";
  $exe_cgpa_9 = mysql_query($get_cgpa_9 );
  $fetch_cgpa_9 = mysql_fetch_array($exe_cgpa_9);
  
   //echo '<h3 style="color:#79BAEC" align="center">CGPA 9 & > 9 = '.$fetch_cgpa_9['total'].'</h3>';
   //echo '<h3 style="color:green;text-decoration:underline;" align="center">STUDENTS OF CLASS X WITH CGPA 9 & ABOVE</h3>';
   
    //query to get the total student who get cgpa CGPA 10
  $get_cgpa_10all = "SELECT DISTINCT cgpa FROM cgpa_report WHERE cgpa =10 ORDER BY cgpa DESC";
  $exe_cgpa_10all = mysql_query($get_cgpa_10all);
  $fetch_cgpa_10all = mysql_fetch_array($exe_cgpa_10all);
  
    //query to get the total student who get cgpa CGPA 9 & > 9
  $get_cgpa_9all = "SELECT DISTINCT cgpa FROM cgpa_report WHERE cgpa >= 9 AND cgpa < 10 ORDER BY cgpa DESC";
  $exe_cgpa_9all = mysql_query($get_cgpa_9all);
  echo '<table  class="table table-bordered table-striped"  width="60%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<thead ><tr>';
  $total = array();
  $i=1;
     $total[0] = $fetch_cgpa_10all['cgpa'];
  echo '<th>CGPA -: '.$fetch_cgpa_10all['cgpa'].'</th>';
  while($fetch_cgpa_9all = mysql_fetch_array($exe_cgpa_9all))
  {
    $evn = $fetch_cgpa_9all['cgpa']*10;
     if($evn%2==0)
     {
    echo '<th>CGPA -: '.$fetch_cgpa_9all['cgpa'].'</th>';
     $total[$i] = $fetch_cgpa_9all['cgpa'];
     $i++;
     }
  }
  
  echo'</tr></thead><tr align="center">';
  $array_marks = array();
  for($j=0;$j< $i;$j++)
  {
     //query to get the total student who get cgpa CGPA 9 & > 9
     $get_cgpa_total = "SELECT count(cgpa) AS total FROM cgpa_report WHERE cgpa ='".$total[$j]."'";
     $exe_cgpa_total = mysql_query($get_cgpa_total );
     $fetch_cgpa_total = mysql_fetch_array($exe_cgpa_total);
	 $array_marks[$j] = $fetch_cgpa_total['total'];
     echo '<td>'.$fetch_cgpa_total['total'].'</td>';
  }
   echo '</tr></table><br><br>';
   
 ?>



<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Highcharts Example</title>

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'STUDENTS OF CLASS X WITH CGPA 9 & ABOVE'
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.point.name +'</b>: '+ math.round(this.percentage) +' %';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '  ;
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Browser share',
                data: [
                    ['CGPA 9.2',   <?php echo $array_marks[4]; ?>],
                    ['CGPA 9.0',       <?php echo $array_marks[5]; ?>],
                    {
                        name: 'CGPA 10.0',
                        y: <?php echo $array_marks[0]; ?>,
                        sliced: true,
                        selected: true
                    },
                    ['CGPA 9.8',    <?php echo $array_marks[1]; ?>],
                    ['CGPA 9.6',     <?php echo $array_marks[2]; ?>],
                    ['CGPA 9.4',   <?php echo $array_marks[3]; ?>]
                ]
            }]
        });
    });
    
});
		</script>
	</head>
	<body>
<script src="../highcharts/js/highcharts.js"></script>
<script src="../highcharts/js/modules/exporting.js"></script>

<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>

	</body>
</html>
