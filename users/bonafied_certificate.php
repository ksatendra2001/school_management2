<?php
require_once('../config/config.php');
$student_id = $_GET['student_id'];

date_default_timezone_set ('Asia/Calcutta');
$date = date('d/m/Y');

$get_deatils = mysql_query("SELECT student_user.*, class_index.class_name FROM student_user
				INNER JOIN class ON student_user.sId = class.sId
				INNER JOIN class_index ON class_index.cId = class.classId
				WHERE student_user.sId = $student_id");
							
$fetch_details = mysql_fetch_array($get_deatils);

$dob = $fetch_details['DOB'];
$dob = date('d/m/Y', strtotime($dob));

$stu_name = ucwords(strtolower($fetch_details['Name']));
$father = ucwords(strtolower($fetch_details['Father\'s Name']));
$class = strtoupper($fetch_details['class_name']);
$adm_no = strtoupper($fetch_details['admission_no']);

?>



<div style="width: 100%;">

	<div align="right">
		<b>
			PH. No.: 22627876<br />
			: 22626299<br />
			Fax: 22617007
		</b>
	</div>

	<div align="center" width="100%">
		<table style="border:none; border-collapse: collapse; margin-top: -10px;" width="100%">
			<tr>
				<th width="15%"><img width="100%" src="../images/logo/School.jpg"></th>
				<th style="line-height: 25px;">
					<b style="font-size:24px;">VIDYA BAL BHAWAN SR. SEC. SCHOOL</b><br />
					<b style="font-size:16px;">
						(Recognised & Affiliated to C.B.S.E.)<br />
						KONDLI GHAROLI ROAD, MAYUR VIHAR-III, DELHI-110096<br />
						E-mail: vidyabalbhawan@gmail.com
					</b>
				</th>
			</tr>
		</table>
	</div>
	
	<div style="float:left">
		<b>Ref No./VBBSSS/15/BC</b>
	</div>
	
	<div style="display: inline-block;">
		&nbsp;
	</div>
	
	<div style="float:right">
		<b>Dated : <?php echo $date; ?></b>
	</div>
	
	<br /><br /><br /><br /><br />
	
	<div align="center" style="width: 100%;">
		<b style="font-size: 20px;"><u>TO WHOMSOEVER IT MAY CONCERN</u></b>
	</div>
	
	<br /><br />
	
	<div style="width: 100%; font-size: 18px; line-height: 25px;">
		<blockquote>
This is to certify that &nbsp; <b><?php echo $stu_name; ?></b> &nbsp; D/S/O &nbsp; <b><?php echo $father; ?></b> &nbsp; is/was a bonafied student of class  &nbsp; <b><?php echo $class; ?></b> &nbsp; vide admission number &nbsp; <b><?php echo $adm_no; ?>.</b> &nbsp; His/Her Date of Birth as per school record is &nbsp; <b><?php echo $dob; ?>.</b>
		</blockquote>
	</div>
