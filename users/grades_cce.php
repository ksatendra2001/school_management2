<?php
//function to show graph to student nso that he can compare students report to other students
function compare_stu_report()
{
	//get class of the folloeing student id
	$get_clas_id = 
	"SELECT classId , sId
	FROM class 
	WHERE sId = ".$_SESSION['user_id']."";
	$exe_get_class_id=mysql_query($get_clas_id);
    $fetch_clas_id=mysql_fetch_array($exe_get_class_id);
			
	$cls_id = $fetch_clas_id[0];
	$sid_all=$fetch_clas_id[1];
	
echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">                           
<div class="widget-header">
<span><i class="icon-home"></i>Graph showing your students marks</span>
</div><!-- End widget-header -->	
<div class="widget-content">';
			//get marks of the student of the folloeing parent by stu id as they are having same id
			$get_stu_marks=
			"SELECT SUM(marks)
			FROM scho_marks
			WHERE student_id = ".$_SESSION['user_id']."";
			$exe_sum_marks=mysql_query($get_stu_marks);
			$fetch_stu_marks=mysql_fetch_array($exe_sum_marks);
			$sum_stu_marks_fa_sa=$fetch_stu_marks[0];
			//get average marks of the class of the following student
			
			$get_avg_marks=
			"SELECT AVG(marks)
			FROM scho_marks
			INNER JOIN class
			ON class.sId = scho_marks.student_id
			WHERE class.classId = ".$cls_id."
			";
			$exe_avg_marks=mysql_query($get_avg_marks);
			$fetch_avg_marks=mysql_fetch_array($exe_avg_marks);
			$avg_marks = $fetch_avg_marks[0];
			//get all the studnet id's
			$get_all_stu_id=
			"SELECT sId
			FROM student_user";
			$exe_stu_all=mysql_query($get_all_stu_id);
			$i=0;
			$sum_all=array();
			while($fetch_stu_all=mysql_fetch_array($exe_stu_all))
			{
			//get maxium marks
				
			$get_max_marks=
			"SELECT SUM(marks)
			FROM scho_marks
			INNER JOIN class
			ON class.sId = scho_marks.student_id
			WHERE class.classId = ".$cls_id."
			AND scho_marks.student_id = ".$fetch_stu_all[0]."
			";
			$exe_max_marks=mysql_query($get_max_marks);
			$fetch_max_marks=mysql_fetch_array($exe_max_marks);
			$sum_all[$i]=$fetch_max_marks[0];
			$max_marks = $fetch_max_marks[0];
		
			$i++;
			}
			for($k=0;$k<$i;$k++)
			{
			$d=0;	
				if($i == $k + 1)
				{
					
					
				}
				else
				{
				$d=$k+1;	
				}
			//get max marks from that
			if($sum_all[$k]>$sum_all[$d])
			{
			$max = $sum_all[$k];	
			}
				
			}
			//get seprate fa and sa marks of the student
			 $get_fa_total_marks=
			"SELECT SUM(marks)
			FROM scho_marks
			WHERE student_id = ".$_SESSION['user_id']."
			AND scho_id <>3 AND scho_id <>6";
			$exe_fa_total=mysql_query($get_fa_total_marks);
			$fetch_fa_total=mysql_fetch_array($exe_fa_total);
			$total_fa_marks=$fetch_fa_total[0];
		
			//get sa total
			 $get_sa_total_marks=
			"SELECT SUM(marks)
			FROM scho_marks
			WHERE student_id = ".$_SESSION['user_id']."
			AND scho_id <> 1 AND scho_id <> 2 AND scho_id <> 4 AND scho_id <>5";
			$exe_sa_total=mysql_query($get_sa_total_marks);
			$fetch_sa_total=mysql_fetch_array($exe_sa_total);
			$total_sa_marks=$fetch_sa_total[0];
			//get avg fa and sa marks
		
			echo "<script type='text/javascript'>
$(function () {
    var chart;
    $(document).ready(function() {
    
        var colors = Highcharts.getOptions().colors,
            categories = ['Student marks', 'Average marks', 'Maximum marks'],
            name = 'Marks Comaprison',
            data = [{
                    y: ".$sum_stu_marks_fa_sa.",
                    color: colors[2],
                    drilldown: {
                        name: 'Student Marks',
                        categories: ['FA marks', 'SA marks'],
                        data: [".$total_fa_marks.", ".$total_sa_marks."],
                        color: colors[0]
                    }
                }, {
                    y: ".$avg_marks.",
                    color: colors[0],
                   
                }, {
                    y: ".$max.",
                    color: colors[1]
                  
                 
                }];
    
        function setChart(name, categories, data, color) {
            chart.xAxis[0].setCategories(categories);
            chart.series[0].remove();
            chart.addSeries({
                name: name,
                data: data,
                color: color || 'white'
            });
        }
    
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'column'
            },
            title: {
                text: 'Graph Comparing students cce FA and SA marks according to class'
            },
            
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Marks'
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var drilldown = this.drilldown;
                                if (drilldown) { // drill down
                                    setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                } else { // restore
                                    setChart(name, categories, data);
                                }
                            }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y;
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var point = this.point,
                        s = this.x +':<b>'+ this.y +'</b><br/>';
                    if (point.drilldown) {
                        s += 'Click to view '+ point.category +'';
                    } else {
                        s += 'Click to return ';
                    }
                    return s;
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }],
            exporting: {
                enabled: false
            }
        });
    });
    
});
		</script>";
			
			echo '
			<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
			</div>
			</div>
			</div>';
				
	
	
}

function show_students()
{

$id_check = 0;

//it is the class id which comes from previous page.
 $cid=$_GET['cid'];
echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Coscho table for activities </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
			';

  //query to get class details
         $q="SELECT `cId`,`class_name` FROM class_index
            WHERE `cId`=". $cid." ";
                 $q_res=mysql_query($q);
                         $res=mysql_fetch_array($q_res);
                         $class=$res['class_name'];

echo'  <h5  style="color:green"align="center">Class:'.     $class.'</h5>';
				echo '<table  class="table table-bordered table-striped" id="dataTable" >
				<thead>
				<tr>
					<th>Admission No.</th>
				    <th>Student Names</th>
		
	            </tr>
				</thead>
				<tbody align="center">
				';
                 
				$get_students = "
				SELECT student_user.Name , student_user.admission_no , student_user.sId
				FROM student_user
				INNER JOIN class
				ON class.sId = student_user.sId
				WHERE class.classId = ".$cid." AND class.session_id = ".$_SESSION['current_session_id']."
				 ";
				 $exe_students = mysql_query($get_students);
				 while($fetch_students = mysql_fetch_array($exe_students))
				 {
			      echo'
			    <tr>
			        <td>'.$fetch_students['admission_no'].'</td>
			        <td><a href="students_report_card.php?student_id='.$fetch_students['sId'].'&&class_id='.$cid.'">'.$fetch_students['Name'].'</a></td>
			    </tr>	
				'; 
				 }
		


echo '</tbody></table>
</div></div></div>';



}
//function to show report to student
function show_report_student()
{
	
	echo ' <div class="row-fluid">
                    
                    	     <!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-table"></i>Report card</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
	 <ol class="rounded-list" >';
	echo '<li><a href="grades_cce/new_grade_cce_view_student_full_report.php?student_id='.$_SESSION['user_id'].'">View Full Report</a></li>';
	echo '<li><a href="grades_cce/new_grade_cce_student_term_report.php?student_id='.$_SESSION['user_id'].'">View Term Report</a></li>
	</ol>
	</div>
	</div>
	</div>';
		
	
}
//select class for report card.
function select_class()
{
			if(isset($_GET['addedfield']))
				{
					echo '<h3 align="center">';
						echo 'Field added';
					echo '</h3>';
				}
				if(isset($_GET['editedfield']))
				{
					echo '<h3 align="center">';
						echo 'Field Edited and Saved';
					echo '</h3>';
				}
					if(isset($_GET['deletedfield']))
				{
					echo '<h3 align="center">';
						echo 'Field Deleted';
					echo '</h3>';
				}
			
		echo '<div class="row-fluid">
                    
                    
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Classes</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                   <h5 style="color:brown">Kindly Select class</h5>
						';
		

$disp='';
 $q="SELECT *
	FROM class_index
                           WHERE class <=10    AND level >=2  
                         ORDER BY  class +0 ASC,section ASC";
$q_res=mysql_query($q);
echo '

';
while($res=mysql_fetch_array($q_res))
{
$_SESSION["id"]=$res[0];
	$disp.='
    
<a href="show_students.php?cid='.$res['cId'].'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span><b style="color:green">'.$res[1].'   </div></a>			

			
	';


}



echo $disp;
				
echo '</div></div></div>';
			

	
	
}

//select claass teacher
function select_class_teacher()
{
    
    
    $priv = $_SESSION['priv'];


	
    
     
echo '             <div class="row-fluid">

        <!-- Table widget -->
    <div class="widget  span12 clearfix">

        <div class="widget-header">
            <span><i class="icon-home"></i>Select Class</span>
        </div><!-- End widget-header -->
	           <ol class="rounded-list"> ';
	  if($priv == 2)
                                    {
	                                  $tid=$_SESSION['user_id'];
                                                            //get classes on the id of the teacher
                                                            $get_classes_tea=
                                                            "SELECT DISTINCT teachers.class , class_index.cId
                                                            FROM teachers
                                                            INNER JOIN class_index
                                                            ON class_index.class_name = teachers.class
                                                            WHERE teachers.tId = ".$tid."  ORDER BY  class_index.class+0 ASC";
                                                            $exe_classes=mysql_query($get_classes_tea);
                                                            while($fetch_classes_teacher=mysql_fetch_array($exe_classes))
                                                            {
                  echo '<li><a href="show_students.php?cid='.$fetch_classes_teacher[1].'">'.$fetch_classes_teacher[0].'</a></li>'
                                                  ;  

                                                            }}

	echo '
                                                        </ol>
                                             
                                                        ';
//				
//echo '
//<tbody align="center">
//';
//while($res=mysql_fetch_array($q_res))
//{
//	//$_SESSION["id"]=$res[0];
//	$disp.='
//<tr>
//<td>    
//			
//<a href="show_students.php?cid='.$res['cId'].'"><div class="shoutcutBox" style="left: 0px;"> <span class="ico color chat-exclamation"></span>'.$res['class_name'].' </div></a>
//				</td>
//				
//	</tr>';
//
//
//}
//
//      }
//
//echo $disp;
echo'                        
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->

	   </div><!-- row-fluid -->';

	
	 
    
    
}
function show_fa_sa_teacher()
{
	

	//query to get name of all the classes
	$get_name_classes=
	"SELECT DISTINCT teachers.class,class_index.cId
	FROM teachers
	INNER JOIN class_index
	ON class_index.class_name = teachers.class
	WHERE tId = ".$_SESSION['user_id']."  ORDER BY  class_index.class+0 ASC";
	$exe_class_name=mysql_query($get_name_classes);
	
	//for sa
	$get_name_sa=
	"SELECT teachers.class,class_index.cId
	FROM teachers
	INNER JOIN class_index
	ON class_index.class_name = teachers.class
	WHERE tId = ".$_SESSION['user_id']."  ORDER BY  class_index.class+0 ASC";
	$exe_class=mysql_query($get_name_sa);
	
echo '   
                    <div class="row-fluid">
                            <div class="widget  span5 clearfix">
                                <div class="widget-header">
                                    <span><i class="icon-list-alt"></i>FA(Formative assesment)</span>
                                </div><!-- End widget-header -->	
                                <div class="widget-content">
								
								 <div class="span6">
								 
                                  <ol class="rectangle-list">
                                      <li><a href="#" onclick="show_fa();">Enter FA</a></li>
                                     </ol>
                                          <ol class="rounded-list" style="display:none" style="padding:30px" id="class_fa">
										  
										<div class="btn-group">
                                              <button class="btn btn-primary">Manage activities</button>
                                              <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></button>
                                              <ol class="dropdown-menu">
                                                <li class="rectangle-list"><a href="admin_edit_delete_fa_sa.php"><i class="icon-pencil"></i> Edit/Delete Activity</a></li>
                                               
                                                <li class="rectangle-list"><a href="admin_add_type_activity_scho.php"><i class="icon-pencil"></i>Add types of activities </a></li>
                                                <li class="divider"></li>
                                                <li class="rectangle-list"><a href="admin_add_fa.php"><i class="i"></i>Add activity</a></li>
					 <li class="rectangle-list"><a href="admin_add_max_marks_fa.php"><i class="i"></i>View max marks</a></li>
                                              </ul>
                                            </div>
<div id="show" style="display:none" style="padding:30px"></div>
										  <br>
										  <br>
										  
										  <li>Select class</li>
										  ';
										  while($fetch_name_class=mysql_fetch_array($exe_class_name))
										  {
										  
										  echo'
                                            <li><a href="select_exam_list.php?cid='.$fetch_name_class[1].'">'.$fetch_name_class[0].'</a></li>
                                           
											  ';
											  
										  }echo'
                                        </ol>
                                    </li>
                                  		
                                  </ol>
                              </div>
								
								
								
								
								
								
								
								
								
                                </div><!--  end widget-content -->
                            </div><!-- widget  span5 clearfix-->
                            ';
							
							
							//end of grid 1
							//start of nxt grid
							
							echo ' <div class="widget  span2 clearfix">
                                <div class="widget-header">
                                    <span><i class="icon-list-alt"></i>View report</span>
                                </div><!-- End widget-header -->	
                                <div class="widget-content">
								<a class="uibutton normal" href="admin_select_class_report.php">Show report</a>
								<a class="uibutton normal" href="grade_cce_select_teacher_class.php">Term report</a>
								<a class="uibutton special" href="grade_cce_select_class_retest_teacher.php">Retest Details</a>
								<a class="uibutton confirm" href="grade_cce_select_class_retest_report_teacher.php">Retest report</a>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span2 clearfix-->
                            ';
							
							//end of grid
							
							echo '
							
                            <div class="widget  span5 clearfix">
                                <div class="widget-header">
                                    <span><i class="icon-list-alt"></i>SA(Summative assesment)</span>
                                </div><!-- End widget-header -->	
                                <div class="widget-content">
								
								 <div class="span6">
                                 <ol class="rectangle-list">
                                      <li><a href="#" onclick="show_sa()">Enter SA</a></li>
                                     </ol>
                                          <ol class="rounded-list" style="display:none" style="padding:30px" id="class_sa">
										  
										<div class="btn-group">
                                              <button class="btn btn-primary">Manage activities</button>
                                              <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></button>
                                              <ol class="dropdown-menu">
                                              
                                                <li class="divider"></li>
                                                <li><a href="admin_add_max_marks_sa.php"><i class="i"></i>View Max Marks</a></li>
                                              </ul>
                                            </div>
<div id="show" style="display:none" style="padding:30px"></div>
										  <br>
										  <br>
										  
										  <li>Select class</li>
										  ';
										  while($fetch_name=mysql_fetch_array($exe_class))
										  {
										  
										  echo'
                                            <li><a href="select_student_list_for_class_sa.php?cid='.$fetch_name[1].'">'.$fetch_name[0].'</a></li>
                                           
											  ';
											  
										  }echo'
                                        </ol>
                                    </li>
                                  		
                                  </ol>
                              </div>

								
								
								
								
                                </div><!--  end widget-content -->
                            </div><!-- widget  span6 clearfix-->
                    </div><!-- row-fluid -->
';	
	
	
	
}
//function to select fa or sa
function show_fa_sa()
{
	//query to get name of all the classes
	$get_name_classes=
	"SELECT *
	FROM class_index
                        WHERE class_name  IN(SELECT class_name
	FROM class_index   WHERE class <=10 AND level >=2)        
                         ORDER BY  class+0 ASC ,section ASC";
	$exe_class_name=mysql_query($get_name_classes);
	
	//for sa
	$get_name_sa=
	"SELECT *
	FROM class_index WHERE class_name  IN(SELECT class_name
	FROM class_index   WHERE class <=10 AND level >=2)  
                           ORDER BY  class+0 ASC ,section ASC";
	$exe_class=mysql_query($get_name_sa);
	
echo '   
                    <div class="row-fluid">
                            <div class="widget  span5 clearfix">
                                <div class="widget-header">
                                    <span><i class="icon-list-alt"></i>FA(Formative assesment)</span>
                                </div><!-- End widget-header -->	
                                <div class="widget-content">
								
								 <div class="span6">
								 
                                  <ol class="rectangle-list">
                                      <li><a href="#" onclick="show_fa();">Enter FA</a></li>
                                     </ol>
                                          <ol class="rounded-list" style="display:none" style="padding:30px" id="class_fa">
										  
										<div class="btn-group">
                                              <button class="btn btn-primary">Manage activities</button>
                                              <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></button>
                                              <ol class="dropdown-menu">
                                                <li class="rectangle-list"><a href="admin_edit_delete_fa_sa.php"><i class="icon-pencil"></i> Edit/Delete Activity</a></li>
                                               
                                                <li class="rectangle-list"><a href="admin_add_type_activity_scho.php"><i class="icon-pencil"></i>Add types of activities </a></li>
                                                <li class="divider"></li>
                                                <li class="rectangle-list"><a href="admin_add_fa.php"><i class="i"></i>Add activity</a></li>
												 <li class="rectangle-list"><a href="admin_add_max_marks_fa.php"><i class="i"></i>View max marks</a></li>
                                              </ul>
                                            </div>
<div id="show" style="display:none" style="padding:30px"></div>
										  <br>
										  <br>
										  
										  <li>Select class</li>
										  ';
										  while($fetch_name_class=mysql_fetch_array($exe_class_name))
										  {
										  
										  echo'
                                            <li><a href="select_exam_list.php?cid='.$fetch_name_class[0].'">'.$fetch_name_class['class_name'].'</a></li>
                                           
											  ';
											  
										  }echo'
                                        </ol>
                                    </li>
                                  		
                                  </ol>
                              </div>
								
								
								
								
								
								
								
								
								
                                </div><!--  end widget-content -->
                            </div><!-- widget  span5 clearfix-->
                            ';
							
							
							//end of grid 1
							//start of nxt grid
							
							echo ' <div class="widget  span2 clearfix">
                                <div class="widget-header">
                                    <span><i class="icon-list-alt"></i>View report</span>
                                </div><!-- End widget-header -->	
                                <div class="widget-content">
                                                        <a class="uibutton normal" type="button" href="admin_select_class_report.php">Show report</a>
                                                        <a class="uibutton normal" href="grade_cce_scholastic_term_result.php">Term report</a>
                                                        <a class="uibutton  special" href="grade_cce_retest_scholastic.php">Retest Details</a>
                                                        <a class="uibutton  confirm" href="grade_cce_retest_view_report.php">Retest Report</a>
								
                             </div><!--  end widget-content -->
                            </div><!-- widget  span2 clearfix-->
                            ';
							
							//end of grid
							
							echo '
							
                            <div class="widget  span5 clearfix">
                                <div class="widget-header">
                                    <span><i class="icon-list-alt"></i>SA(Summative assesment)</span>
                                </div><!-- End widget-header -->	
                                <div class="widget-content">
								
								 <div class="span6">
                                 <ol class="rectangle-list">
                                      <li><a href="#" onclick="show_sa()">Enter SA</a></li>
                                     </ol>
                                          <ol class="rounded-list" style="display:none" style="padding:30px" id="class_sa">
										  
										<div class="btn-group">
                                              <button class="btn btn-primary">Manage activities</button>
                                              <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></button>
                                              <ol class="dropdown-menu">
                                              
                                                <li class="divider"></li>
                                                <li><a href="admin_add_max_marks_sa.php"><i class="i"></i>View Max Marks</a></li>
                                              </ul>
                                            </div>
<div id="show" style="display:none" style="padding:30px"></div>
										  <br>
										  <br>
										  
										  <li>Select class</li>
										  ';
										  while($fetch_name=mysql_fetch_array($exe_class))
										  {
										  
										  echo'
                                            <li><a href="select_student_list_for_class_sa.php?cid='.$fetch_name[0].'">'.$fetch_name['class_name'].'</a></li>
                                           
											  ';
											  
										  }echo'
                                        </ol>
                                    </li>
                                  		
                                  </ol>
                              </div>

								
								
								
								
                                </div><!--  end widget-content -->
                            </div><!-- widget  span6 clearfix-->
                    </div><!-- row-fluid -->
';	
	
}

//function to add max marks by admin
function admin_add_max_marks_fa()
{
echo '	       <div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Formative Assesment(Add/change max marks)</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
						
							
								 <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead>
                                            <tr>
											<th>Assesment</th>
											<th>Max marks</th>
											</tr>
											</thead>
											<tbody align="center">';
											//get max masrks of Fa's
											 $get_max_marks=
											"SELECT *
											FROM scho_fields
											WHERE scho_id = 1 OR scho_id =2 OR scho_id = 4 OR scho_id = 5";
											$exe_max_marks=mysql_query($get_max_marks);
											while($fetch_details_fa=mysql_fetch_array($exe_max_marks))
											{
											
											echo '
											<tr>
											<td>'.$fetch_details_fa[1].'
											</td>
											<td><input type="tel" value="'.$fetch_details_fa[2].'" style="width:30px" onchange="change_max_marks_fa(this.value,'.$fetch_details_fa[0].')" required="required"</td><span id="change_max_fa_'.$fetch_details_fa[0].'"></span>
											';
											}
											echo '
										
											</tbody>
											</table>
								
								</div>
								</div>
								</div>
								';	
	
	
}
//function show_lit of students for sa
function show_student_sa()
{
	$class_id=$_GET['cid'];
	echo '	       <div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Summative Assesment</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead>
                                            <tr>

								
				
				<table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead align="center">
										<tr>
								
									<th>Assesment</th>
									<th>Add marks</th>
										</tr>
										</thead>
										<tbody align="center">
';
				
//query to show students acc to cid

/*$get_names_students=
"SELECT student_user.Name , student_user.sId 
FROM student_user
INNER JOIN class
ON class.sId=student_user.sId
WHERE classId=".$class_id."
";
$exe_query_for_name=mysql_query($get_names_students);

$fetch_students_names=mysql_fetch_array($exe_query_for_name))
*/
echo '
<tr>
<form action="show_list_for_marks.php" method="get">



<input type="hidden" name="class_id" value="'.$class_id.'">


<td>
<select class="small" name="get_sa">
<option value="3">SA1</option>
<option value="6">SA2</option>

</select>


</td>
<td><button class="uibutton  icon forward normal" type="submit">Add marks obtained</button></td>
</form>
</tr>
';


echo '</tbody>
				</table>
				</div>
				</div>
				</div>';
	
	

}
//function to show list for entering marks for sa
function enter_marks_sa()
{
	
	
	$cid=$_GET['class_id'];
	$sa_type=$_GET['get_sa'];
	if($sa_type==3)
	{
		
		$sa= "SA1";
	}
	
	else
	{
		
	$sa = "SA2";	
	}
	//get name of the class for that id
	$get_class_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$cid."";
	$exe_clas_name=mysql_query($get_class_name);
	$fetch_class_name=mysql_fetch_array($exe_clas_name);
	$name_class=$fetch_class_name[0];
	//get student name for that class
	/*$name_student=
	"SELECT Name 
	FROM student_user
	WHERE sId=".$sid."";
 
 $exe=mysql_query($name_student);
 $fetch_name=mysql_fetch_array($exe);*/

	echo '	 <div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Summative Assesment--Enter marks('.$sa.')</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead align="center">
                                          

								
				
				
				
				
				';
				echo '
				<h5 style="color:grey" align="center">Class:'.$name_class.'</h5>
				
				'
			
				
				;
				echo '
				<tr>';
			//get name of the subjects for that class
			$get_sub_name=
			"SELECT subject.subject , subject.subject_id
			FROM subject
			INNER JOIN teachers
			ON teachers.subject_id=subject.subject_id
			WHERE class='".$name_class."'";	
			$exe_sub_name=mysql_query($get_sub_name);
			$count_sub=0;
			$id_sub=array();
			echo '<th>Student names</th>';
			while($fetch_sub_name=mysql_fetch_array($exe_sub_name))
			{
				$id_sub[$count_sub]=$fetch_sub_name[1];
				echo '<th>'.$fetch_sub_name[0].'</th>';
				++$count_sub;
				
			}
				
			 
			 echo '
				</tr>
				<tbody align="center">
				
				';
				//get student names on the class id
				 $get_stud_names=
				"SELECT student_user.Name , 
				student_user.sId
				FROM student_user
				
				INNER JOIN class
				ON class.sId = student_user.sId
				
				WHERE class.classId = ".$_GET['class_id']."  AND class.session_id=".$_SESSION['current_session_id']."";
				$exe_stu_names=mysql_query($get_stud_names);
				while($fetch_names_stu=mysql_fetch_array($exe_stu_names))
				{
				echo '<tr><td>'.$fetch_names_stu[0].'</td>';	
					
				
				
				for($i=0;$i<$count_sub;$i++)
				{
					$get=
					"SELECT * FROM scho_marks
					WHERE scho_id = ".$sa_type."
					AND student_id = ".$fetch_names_stu[1]."
					AND subject_id = ".$id_sub[$i]."";
					$exe_query=mysql_query($get);
					$fetch_marks=mysql_fetch_array($exe_query);
					
					//get values for id
					
				echo '
				
				<td id="'.$fetch_names_stu[1].'_'.$i.'">
				<input type="text" style="width:20px" id="'.$id_sub[$i].'" value="'.$fetch_marks['marks'].'" onblur="marks_enter_sa(this.value,'.$i.','.$id_sub[$i].','.$sa_type.','.$fetch_names_stu[1].');">
				<span id="add_'.$i.'_'.$fetch_names_stu[1].'"></span>
				</td>
			
				
				';}
				
				echo '</tr>';
				
				}
				echo '
				
				
				
				
				';
				
				
				
				
	echo '</tbody>
				</table>
				</div>
				</div>
				</div>';
	
}
function show_student_list_for_class()
{
	
$class_id=$_GET['cid'];	
	
	echo '	  <div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Select Student </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead>
 <div align="center"><a class="btn btn-large btn-info" href="select_subject_for_student_full_report.php?class_id='.$_GET['cid'].'"><i class="icon-info-sign icon-white"></i>Show Full Report</a>
</div>

				<tr align="center">
				<th>Students</th>
				<th>Action</th>		
					</tr>	
						
				

<tbody>

';
//query to get list i.e names of students for that class
$get_names_students=
"SELECT student_user.Name , student_user.sId 
FROM student_user
INNER JOIN class
ON class.sId=student_user.sId
WHERE classId=".$class_id."
";
$exe_query_for_name=mysql_query($get_names_students);

while($fetch_students_names=mysql_fetch_array($exe_query_for_name))
{
echo '<tr align="center">
<td>'.$fetch_students_names[0].'</td>



<td><a class="uibutton icon special add" href="select_exam_list.php?student_id='.$fetch_students_names['sId'].'&&cid='.$class_id.'">Add marks obtained</a><br>

<button class="uibutton icon confirm secure" onclick="select_list_for_fa('.$fetch_students_names['sId'].');">View marks obtained</button>
<div id="fa_list_'.$fetch_students_names['sId'].'" style="display:none" style="padding:50px">
<a href="select_subject_for_marks.php?student_id='.$fetch_students_names['sId'].'&&cid='.$class_id.'&&fa_id=1" class="uibutton icon prev normal ">FA1</button>
<a href="select_subject_for_marks.php?student_id='.$fetch_students_names['sId'].'&&cid='.$class_id.'&&fa_id=2" class="uibutton icon prev normal ">FA2</button>
<a href="select_subject_for_marks.php?student_id='.$fetch_students_names['sId'].'&&cid='.$class_id.'&&fa_id=4" class="uibutton icon next normal ">FA3</button>
<a href="select_subject_for_marks.php?student_id='.$fetch_students_names['sId'].'&&cid='.$class_id.'&&fa_id=5" class="uibutton icon next normal ">FA4</button>

</div></td>

';
//show_student_cce_marks.php?student_id='.$fetch_students_names['sId'].'&&cid='.$class_id.'

}
echo '
';
echo '</tbody></table></div></div></div>';
	
	
}
//function edit and delete show fa sa acctivity
function edit_delete_fa_sa()
{
$fetch_query_scho_activity="SELECT `id`, `scho_id`, `max_marks`, `name_of_activity`,`type_id`
                            FROM `scho_activity_table`";
$execute_scho_activity=mysql_query($fetch_query_scho_activity);
               if (isset($_GET['deletedactivity']))
				{
					echo "<h5 align=\"center\" style=\"color:cream\" align=\"center\"> DELETED SUCCESSFULLY!</h5>";
				}
				
if (isset($_GET['editscho']))
				{
					echo "<h5 align=\"center\" style=\"color:cream\" align=\"center\"> EDITTED SUCCESSFULLY!</h5>";
				}
if(isset($_GET['error']))
								{
								echo '<p style="color:red" align="center"><b>Error!</b></p>';	
									
								}
	echo'
			 <div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span>Scho Activity </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead align="center">
										<tr>
									
										<td>Scho Name</td>
										<td>Max Marks</td>
										<td>Name of Activity</td>
										<td>Type ID</td>
										<td>Action</td>
										</tr>
										</thead>
										';
										echo '<tbody align="center">';
								//fetch activity from the scho activity tabel
								while($fetch_activity=mysql_fetch_array($execute_scho_activity))
								{
									//get naem of the shco on that id
									$get_name_scho=
									"SELECT *
									FROM scho_fields
									WHERE scho_id = ".$fetch_activity['scho_id']."";
									$exe_scho_name=mysql_query($get_name_scho);
									$fetch_name_scho=mysql_fetch_array($exe_scho_name);
									//get type name
									$get_type_name=
									"SELECT *
									FROM types_of_activities
									WHERE Id = ".$fetch_activity['type_id']."";
									$exe_type_name=mysql_query($get_type_name);
									$fetch_name=mysql_fetch_array($exe_type_name);
									
								echo '<tr>
								      <td>'.$fetch_name_scho[1].'</td>
							
									  <td>'.$fetch_activity['max_marks'].'</td>
									  <td>'.$fetch_activity['name_of_activity'].'</td>
									  <td>'.$fetch_name[1].'</td>
									  <td>
									   <span class="" ><a href="admin_edit_fa_sa_activity.php?id='.$fetch_activity['id'].'
&&id_scho='.$fetch_name_scho[0].'&&name_scho='.$fetch_name_scho[1].'&&type='.$fetch_name[0].'"  title="Edit" ><img src="images/icon/icon_edit.png" ></a></span> 
						
						<span class="tip" ><a href="grades_cce/delete_scho_activity.php?id='.$fetch_activity['id'].'" class="Delete" data-name="delete name" title="Delete"  ><img src="images/icon/icon_delete.png" ></a></span>
									  </td>
									  </tr>
								  
									
									
								';	
								}
										
	
	
	
echo'</tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div><!-- row-fluid -->';
	
	
}
//function edit scho fa sa activity
function edit_fa_sa_activity()
{
	$id=$_GET['id'];
	$id_scho=$_GET['id_scho'];
	$name_scho=$_GET['name_scho'];
	$type=$_GET['type'];
	//get all the activities
	 $get_activities=
	"SELECT *
	FROM  types_of_activities
	WHERE `Id`='".$type."'";
	$exe_activities=mysql_query($get_activities);
	$fetch_activities=mysql_fetch_array($exe_activities);
	$activity_type=$fetch_activities[1];
	$activity_id=$fetch_activities[0];
	//get all the activities
	$get_all_fields=
	"SELECT *
	FROM scho_fields
	WHERE  scho_id=".$id_scho."
	";
	$exe_scho_fields=mysql_query($get_all_fields);
	$fetch_fields=mysql_fetch_array($exe_scho_fields);
	$scho_id=$fetch_fields[0];
	$scho_name=$fetch_fields[1];
	
	//if mysql query affected rows then get message enquiry successfull
								if(isset($_GET['editted']))
								{
								echo '<p style="color:brown" align="center"><b>EDITTED SUCCESSFULLY</b></p>';	
									
								}
									
	                       echo ' <div class="row-fluid">
                    
                    		    <!-- Table widget -->
                               <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span>Edit Registerd Student</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
								';
								//get details of the scho activity
								$get_id_scho_details=
								"SELECT * 
								FROM `scho_activity_table`
								WHERE id=".$id."";
								$exe_details_scho=mysql_query($get_id_scho_details);
								$fetch_scho_details=mysql_fetch_array($exe_details_scho);
								
						echo '	<form id="validation_demo" action="grades_cce/edit_scho_activity.php"  method="get">
						        
								 <div>
								 <input type="hidden" value="'.$fetch_scho_details['id'].'" name="id">
								 </div>
						         <div class="section">
								 <label>Scho Name<small></small></label>
								 <div>
								 <select name="scho" class="chzn-select" data-placeholder="Choose assesment">
								 <option value="'.$scho_id.'">'.$scho_name.'</option>
								 ';
								   //get all the activities
								  $get_fields=
								  "SELECT *
								  FROM scho_fields
								  WHERE scho_id=1 || scho_id=2 || scho_id = 4 || scho_id = 5
								  ";
								  $scho_fields=mysql_query($get_fields);
	
								 while($fetch_scho_fields=mysql_fetch_array($scho_fields))
								 {       
								      if($fetch_scho_fields[0]!=$scho_id)
								           {
									echo '<option value="'.$fetch_scho_fields[0].'">'.$fetch_scho_fields[1].'</option>'; 
										   }
								 }
								 
								 echo '
								</select>
								 </div></div>
								  <div class="section">
								 <label>Max Marks<small></small></label>
								 <div>
								 <input type="text" value="'.$fetch_scho_details['max_marks'].'" name="max">
								 </div></div>
								 	  <div class="section">
								 <label>Name of Activity<small></small></label>
								 <div>
								 <input type="text" value="'.$fetch_scho_details['Name_of_activity'].'" name="name">
								 </div></div>
								 	  <div class="section">
								 <label>Type name<small></small></label>
								 <div>
								 
								 ';
								  echo '<select name="type" id="type" class="chzn-select" data-placeholder="select">
								<option value="'.$activity_id.'">'.$activity_type.'</option>';
								
								//get all the activities
								$get_activities_type=
								"SELECT *
								FROM  types_of_activities
								";
								$exe_activities_type=mysql_query($get_activities_type);
								
								 while($fetch_activities_type=mysql_fetch_array($exe_activities_type))
								 {
								       if($activity_id!=$fetch_activities_type[0])
									   {
								echo '<option value="'.$fetch_activities_type[0].'">'.$fetch_activities_type[1].'</option>';
									   }
								
								 }
								 echo '
								 </select>
								 </div></div>
								  <div class="section last">
                                 <div>
                                <a class="btn submit_form" >Update</a>
								 <a href="admin_edit_delete_fa_sa.php" class="btn submit_form" >Cancel</a>
                                         
                                 </div>
                                  </div>
							
								';
							echo '	 </fieldset>
                                </form>
        
                          </div>								
                    </div>
								</div></div></div>';
		
	
	
}
//to show students report card
function students_report_card()
{
	echo ' <div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-table"></i>Report card</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">';
								echo'<div class="span4">
    
    <div class="breaks"><span></span></div>
	 <a href="grades_cce/new_grade_cce_teacher_view_full_report.php?class_id='.$_GET['class_id'].'&student_id='.$_GET['student_id'].'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	<b style="color:brown">Full Report</b> <em> </em> </div>
    <div class="breaks"><span></span></div>
	<a href="grades_cce/new_grade_cce_teacher_view_term_report.php?class_id='.$_GET['class_id'].'&student_id='.$_GET['student_id'].'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	<b style="color:brown">Term Report</b> <em> </em> </div>
    <div class="breaks"><span></span></div>
    
   </div><!-- span4 column--></div>
	</div>';
								/*echo'
								 <ol class="rounded-list" ';
	echo '<li><a href="grades_cce/result_front.php?sId='.$_GET['student_id'].'">Download front</a></li>';
	echo '<li><a href="grades_cce/report_back.php?student_id='.$_GET['student_id'].'">Download back</a></li>
	</ol>
	</div>
	</div>
	</div>';
	*/







}//end of function student report card.

//select class of the foloowing
function select_class_rep()
{
$priv = $_SESSION['priv'];

echo '  <div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Choose Class</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
								  <ol class="rounded-list" ';
								  if($priv == 2)
                                    {
	                                  $tid=$_SESSION['user_id'];
									  //get classes on the id of the teacher
									  $get_classes_tea=
									  "SELECT teachers.class , class_index.cId
									  FROM teachers
									  INNER JOIN class_index
									  ON class_index.class_name = teachers.class
									  WHERE teachers.tId = ".$tid."  ORDER BY  class_index.class+0 ASC";
									  $exe_classes=mysql_query($get_classes_tea);
									  while($fetch_classes_teacher=mysql_fetch_array($exe_classes))
									  {
										echo '<li><a href="select_subject_for_student_full_report.php?class_id='.$fetch_classes_teacher[1].'">'.$fetch_classes_teacher[0].'</a></li>'
								;  
										  
									  }
	
                                  }
								  else
								  {
								$get_classes=
								"SELECT *
                                                                                                                                                                                                    FROM class_index
                                                                                                                                                                                                                    WHERE class_name  IN(SELECT class_name
                                                                                                                                                                                                    FROM class_index   WHERE class <=10 AND class >=5)        
                                                                                                                                                                                                                     ORDER BY  class+0 ASC, section ASC";
								$exe_get_classes=mysql_query($get_classes);
								while($fetch_classes=mysql_fetch_array($exe_get_classes))
								{
								echo '<li><a href="select_subject_for_student_full_report.php?class_id='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>'
								;	
									
								}
								  }
								echo '
								</ol>
								</div>
								</div>
								</div>';
									
	
}
//function to select subject to view class full report
function select_subject_full_report()
{
    
             $class_id=$_GET['class_id'];  
             $term_id=$_GET['term'];   
              $test_id=$_GET['test'];  
              
        
echo '  <div class="row-fluid">

<!-- Table widget -->
<div class="widget  span12 clearfix">

<div class="widget-header">
<span><i class="icon-home"></i>Choose subject to view subject activities and marks</span>
</div><!-- End widget-header -->	

<div class="widget-content">';
								
	
//get subject id from teachers
$get_id_subject = 
"SELECT DISTINCT subject.subject , subject.subject_id
FROM subject
INNER JOIN cce_subject_type_skills_id_table

ON cce_subject_type_skills_id_table.subject_id = subject.subject_id

WHERE cce_subject_type_skills_id_table.class_id =$class_id";
$exe_subjects=mysql_query($get_id_subject);
echo '<div class="span4">
';
while($fetch_subjects=mysql_fetch_array($exe_subjects))
{

//echo ' <a href="class_full_report.php?class_id='.$_GET['class_id'].'&&subject_id='.$fetch_subjects['subject_id'].'"><div class="alertMessage inline info">'.$fetch_subjects[0].'</div></a> 
//';
}
echo '</div>';

echo '</div>

</div>
</div>';	
        
//echo '  <div class="row-fluid">
//                    
//                    		<!-- Table widget -->
//                            <div class="widget  span12 clearfix">
//                            
//                                <div class="widget-header">
//                                    <span><i class="icon-home"></i>Choose subject to view subject activities and marks</span>
//                                </div><!-- End widget-header -->	
//                                
//                                <div class="widget-content">';
//								
//		
// $get_name_subject=
//"SELECT class_name
//FROM class_index
//WHERE cId = ".$_GET['class_id']."
//
//
//";	
//$exe_name_subject=mysql_query($get_name_subject);
//while($fetch_name_subject=mysql_fetch_array($exe_name_subject))
//{
////get subject id from teachers
// $get_id_subject = 
//"SELECT DISTINCT subject.subject , subject.subject_id
//FROM subject
//INNER JOIN teachers
//
//ON teachers.subject_id = subject.subject_id
//
//WHERE teachers.class = '".$fetch_name_subject[0]."'";
//$exe_subjects=mysql_query($get_id_subject);
//echo '<div class="span4">
//';
//while($fetch_subjects=mysql_fetch_array($exe_subjects))
//{
//
//echo ' <a href="class_full_report.php?class_id='.$_GET['class_id'].'&&subject_id='.$fetch_subjects[1].'"><div class="alertMessage inline info">'.$fetch_subjects[0].'</div></a> 
//';
//}
//echo '</div>';
//}
//echo '</div>
//
//</div>
//</div>';	
}

//to show full class report of cce
function class_full_report()
{
	$session_id=$_SESSION['current_sesssion_id'];
	// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_name
	 FROM class_index 
	 WHERE cId = ".$_GET['class_id']."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 //function to bring name of the student of that class
     $get_stu_names=
	 "SELECT student_user.Name ,student_user.sId
	 FROM student_user
	 INNER JOIN class
	 ON class.sId = student_user.sId
	 
	 WHERE class.classId = ".$_GET['class_id']." AND class.session_id=".$_SESSION['current_session_id']."";
	 $exe_get_stu_names=mysql_query($get_stu_names);
	// $fetch_stu_names=mysql_fetch_array($exe_get_stu_names); // names of students
	  $get_subject = "
	  SELECT subject 
	  FROM subject
	  WHERE subject_id =".$_GET['subject_id']." ";
	  $exe_subject = mysql_query($get_subject);
	  $fetch_subject = mysql_fetch_array($exe_subject);
	  
	  //get max marks of all assesments
	  $get_max_marks=
	  "SELECT max_marks
	  FROM scho_fields";
	  $exe_max_marks=mysql_query($get_max_marks);
	  $ass_max_marks=array();
	  $q=0;
	  while($fetch_max_marks=mysql_fetch_array($exe_max_marks))
	  {
		 $ass_max_marks[$q]=$fetch_max_marks[0]; 
		  
		  $q++;
	  }
	  
	 
	
	  
//	echo '  
//	<a href="grades_cce/cce_download_xl.php?cid='.$_GET['class_id'].'&&subject_id='.$_GET['subject_id'].'"><button class="uibutton normal">Download excel sheet</button></a> 
//	<a href="grades_cce/term_1_report.php?cid='.$_GET['class_id'].'&&subject_id='.$_GET['subject_id'].'"><button class="uibutton normal">Term 1 Report</button></a>
//	<a href="grades_cce/term_2_report.php?cid='.$_GET['class_id'].'&&subject_id='.$_GET['subject_id'].'"><button class="uibutton normal">Term 2 Report</button></a> 
//	';
                echo'
	<div class="row-fluid" style="width:120%">
                       <div class="widget  span12 clearfix"  style="width:120%">
                            
                                <div class="widget-header" style="width:100%">
                                    <span><i class="icon-home"></i>Class Full Report </span>
                                </div><!-- End widget-header -->	
                                
                               
	
                    		<!-- Table widget -->
                            
			<table  class="table table-bordered table-striped"  width="100%">
                                        <thead align="center">
<tr align="center">
<th>Class</th>
<th><strong>'.$fetch_cls_name['class_name'].'</strong></th>
</tr>


<tr>


<th rowspan="2">Name Of Student</th>
<th>'.strtoupper($fetch_subject['subject']).'</th>
</tr>

<tr>
<th>FA1('.$ass_max_marks[0].')</th>
<th>FA1(10%)</th>
<th>FA2('.$ass_max_marks[1].')</th>
<th>FA2(10%)</th>
<th>SA1('.$ass_max_marks[2].')</th>
<th>SA1(30%)</th>
<th>Total<br/>(FA1+FA2+SA1)</th>
<th>FA3('.$ass_max_marks[3].')</th>
<th>FA3(10%)</th>
<th>FA4('.$ass_max_marks[4].')</th>
<th>FA4(10%)</th>
<th>SA2('.$ass_max_marks[5].')</th>
<th>SA(30%)</th>

<th>Total<br/>(FA3+FA4+SA2)</th>
<th>Total(100)</th>
<th>Grade Point</th>
</tr>
<tr> ';

$marks_fa=array();
$marks_sa=array();
$sid=array();
$marks;
$k=0;
$j=0;
while($fetch_stu_names=mysql_fetch_array($exe_get_stu_names))
{			

//get all the scho id
$sum1 =0;
$sum2 =0;
$total = 0;
echo '



<td>'.$fetch_stu_names['Name'].'</td>';

$k++;


$get_scho_id = 
"SELECT scho_id
FROM scho_fields";
$exe_scho_id=mysql_query($get_scho_id);
$i=0;
while($fetch_scho_id=mysql_fetch_array($exe_scho_id))
{
$scho_id = $fetch_scho_id[0];

//query to get student marks
$get_student_marks = "
SELECT SUM(marks),ID
FROM  cce_marks_scho_type_skills_table
WHERE student_id = ".$fetch_stu_names['sId']."
AND subject_id = ".$_GET['subject_id']."
AND session_id = ".$scho_id."

";

$exe_stu_marks=mysql_query($get_student_marks);
$fetch_stu_marks=mysql_fetch_array($exe_stu_marks);



echo '

<td id="'.$scho_id.'"><input type="text"  value="'.$fetch_stu_marks[0].'" style="width:20px" onblur="change_marks_rep('.$scho_id.','.$fetch_stu_names['sId'].','.$_GET['subject_id'].',this.value);"/><span id="done_marks_'.$scho_id.'_'.$fetch_stu_names['sId'].'"></span></td>
';
$i++;  
echo'
<td>
';
if(($scho_id == 1)||($scho_id == 2)||($scho_id == 4)||($scho_id == 5))
{
$marks = $fetch_stu_marks[0] * 0.1 ;
echo $marks ;


}
if(($scho_id == 3)||($scho_id == 6))
{
$marks = $fetch_stu_marks[0] * 0.3;
echo $marks;
}
echo '
</td>



';

if(($scho_id == 1)||($scho_id == 2)||($scho_id == 3))
{
$sum1 = $sum1+$marks; 


}
if(($scho_id == 4)||($scho_id == 5)||($scho_id == 6))
{
$sum2 = $sum2+$marks; 


}

++$j;
if($scho_id == 3)
{
echo'<td>'.$sum1.'</td>';
}

}
$total = $sum1 + $sum2;
$grade = 0;
if(($total>90) && ($total<=100))
{
$grade = 10;
}
if(($total>80) && ($total<=90))
{
$grade = 9;
}
if(($total>70) && ($total<=80))
{
$grade = 8;
}
if(($total>60) && ($total<=70))
{
$grade = 7;
}
if(($total>50) && ($total<=60))
{
$grade = 6;
}
if(($total>40) && ($total<=50))
{
$grade = 5;
}
if(($total>32) && ($total<=40))
{
$grade = 4;
}
if(($total>20) && ($total<=32))
{
$grade = 3;
}
if(($total>=0) && ($total<=20))
{
$grade = 2;
}


echo '

<td>'.$sum2.'</td>
<td>'.$total.'</td>
<td>'.$grade.'</td>

</tr>
';

}


echo '	
</thead>
<tbody align="center">';

echo ' </tbody>
</table>

';
//get name of the student and assesment of that class


echo '

</div>
</div>
</div>';
	
	
	
}
//function to view cce marks to student
function view_cce_marks_student()
{
	//get name of the student on that id
	$get_student_name=
	"SELECT Name
	FROM student_user
	WHERE sId = ".$_GET['student_id']."";
	$exe_stu_name=mysql_query($get_student_name);
	$fetch_name_student_on_id=mysql_fetch_array($exe_stu_name);
	$name = $fetch_name_student_on_id[0];
	//query to get name of the subject on the id
	$get_name_subject=
	"SELECT subject
	FROM subject
	WHERE subject_id = ".$_GET['subject_id']."";
	$exe_name_subject = mysql_query($get_name_subject);
	$fetch_name_subject = mysql_fetch_array($exe_name_subject);
	$sub_name=$fetch_name_subject[0];
	echo '  <div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Marks Obtained by '.$name
									.' </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <table  class="table table-bordered table-striped" id="dataTable" >
                                        
										
<thead>

<h5 align="center" style="color:grey">Subject: '.$sub_name.'</h5>
			';
			echo "	<tr>
				
				
				";
			$query="SELECT  types_of_activities.type_activities , scho_marks.marks
				 FROM types_of_activities
				 
				 INNER JOIN scho_marks
				 ON scho_marks.scho_act_id =  types_of_activities.Id
				 
				 WHERE scho_marks.student_id = ".$_GET['student_id']."
				 
				 AND scho_marks.scho_id = ".$_GET['fa_id']."
				 
				 AND scho_marks.subject_id = ".$_GET['subject_id']."";
		       $query_exe=mysql_query($query);
			   $k=0;
			   $marks=array();
			   
			   while($fetch_query_exe=mysql_fetch_array($query_exe))
			   {
				   $marks[$k] = $fetch_query_exe[1];
				   echo '<th>'.$fetch_query_exe[0].'</th>';
				   
				   $k++;
			   }
		echo '</tr>
		</thead>
		';
		echo '
		<tbody align="center">
		<tr>
		
		';
		for($i=0;$i<$k;$i++)
		{
			echo '<td>'.$marks[$i].'</td>';
			
		}
		echo '</td>';
	/* $count="SELECT COUNT(DISTINCT co_scho_name) As co_name FROM coscho_fields";
	 $count_exe=mysql_query($count);
	$fetch_count=mysql_fetch_array($count_exe);
	$count_val= $fetch_count[0];
	
		$ctr=0;
		$coscho_id = array();
		  while($name=mysql_fetch_array($query_exe))
		  {
			  
	 				 echo '<th>'.$name[0].'</th>';
					
					 ++$ctr;			 
		  }
		 
	
		  echo "
		  </tr>

</thead>		 <tbody align='center'> ";
		  
		 $q="SELECT  subject.subject
				 FROM subject
				 
				 INNER JOIN scho_marks
				 ON scho_marks.subject_id =  subject.subject_id
				 
				 WHERE scho_marks.student_id = ".$_GET['student_id']."
				 
				 AND scho_marks.scho_id = ".$_GET['fa_id']."";
		$execute=mysql_query($q);
		while($fetch=mysql_fetch_array($execute))
		{
		
		
		
			
		echo "<tr><td>".$fetch[0]."</td>";
	
		$ctr_text_box = $ctr;
		
		while($ctr_text_box != 0)
		{
			
			
			 for($i=0;$i<$ctr;$i++)
			{
			
		$sql_query_take="SELECT  scho_marks.marks
				 FROM scho_marks
				 
				 INNER JOIN subject
				 ON scho_marks.subject_id =  subject.subject_id
				 
				 WHERE scho_marks.student_id = ".$_GET['student_id']."
				 
				 AND scho_marks.scho_id = ".$_GET['fa_id']."";
		$exe_query_take=mysql_query($sql_query_take);
		$fetch_take=mysql_fetch_array($exe_query_take);
			
			echo '<td>'.$fetch_take[0].'</td>';
			--$ctr_text_box;
		}}
		// Query to get total
		
		echo "

	
	 </tr> ";
						
				


		}*/
//query to get deatils of the marks of the activity of the student
/*$get_details_student=
"SELECT scho_marks.marks , student_user.Name ,  scho_activity_table.Name_of_activity , 
 scho_fields.scho_name ,subject.subject
 FROM scho_marks
 
 INNER JOIN student_user
 ON student_user.sId = scho_marks.student_id
 
 INNER JOIN scho_activity_table
 ON scho_activity_table.id = scho_marks.scho_act_id
 
 INNER JOIN scho_fields
 ON scho_fields.scho_id = scho_marks.scho_id
 
 INNER JOIN subject
 ON subject.subject_id = scho_marks.subject_id
 
 WHERE scho_marks.student_id = ".$_GET['student_id']."
 
 AND scho_fields.scho_id = ".$_GET['fa_id']."";
	
$exe_details_student=mysql_query($get_details_student);
$k=0;
echo '<th>Subjects</th>';
$name_sub=array();
while($fetch_stu_details=mysql_fetch_array($exe_details_student))
{
	$name_sub[$k] = $fetch_stu_details[4];
echo '



			
							<th>'.$fetch_stu_details[2].'</th>
			  
			
					





';
$k++;

}
echo '</tr>
</thead>

<tbody>';

$get_details_stu=
"SELECT scho_marks.marks , student_user.Name ,  scho_activity_table.Name_of_activity , 
 scho_fields.scho_name ,subject.subject
 FROM scho_marks
 
 INNER JOIN student_user
 ON student_user.sId = scho_marks.student_id
 
 INNER JOIN scho_activity_table
 ON scho_activity_table.id = scho_marks.scho_act_id
 
 INNER JOIN scho_fields
 ON scho_fields.scho_id = scho_marks.scho_id
 
 INNER JOIN subject
 ON subject.subject_id = scho_marks.subject_id
 
 WHERE scho_marks.student_id = ".$_GET['student_id']."
 
 AND scho_fields.scho_id = ".$_GET['fa_id']."";
	
$exe_details_stu=mysql_query($get_details_stu);
echo '<tr>
<td></td>
';
while($fetch_stu = mysql_fetch_array($exe_details_stu))
{

echo '
<td>'.$fetch_stu[0].'</td>




';
}


for($i=0;$i<$k;$i++)
{
echo '<tr><td>'.$name_sub[$i].'</td></tr>';	
	
}*/


echo '
</tbody>
</table>
</div>
</div>
</div>';
}
//function to select subject
function select_subject_fa()
{
 echo '  <div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Choose subject to view subject activities and marks</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">';
 $get_name_subject=
"SELECT class_name
FROM class_index
WHERE cId = ".$_GET['cid']."


";	
$exe_name_subject=mysql_query($get_name_subject);
while($fetch_name_subject=mysql_fetch_array($exe_name_subject))
{
//get subject id from teachers
$get_id_subject = 
"SELECT subject.subject , subject.subject_id
FROM subject
INNER JOIN teachers

ON teachers.subject_id = subject.subject_id

WHERE teachers.class = '".$fetch_name_subject[0]."'";
$exe_subjects=mysql_query($get_id_subject);
echo '<div class="span4">
';
while($fetch_subjects=mysql_fetch_array($exe_subjects))
{

echo ' <a href="show_student_cce_marks.php?student_id='.$_GET['student_id'].'&&subject_id='.$fetch_subjects[1].'&&fa_id='.$_GET['fa_id'].'"><div class="alertMessage inline info">'.$fetch_subjects[0].'</div></a> 
';
}
echo '</div>';
}
echo '</div>

</div>
</div>';	
}
function show_students_for_grades()
{

$id_check = 0;

//it is the 
 $cid=$_GET['cid'];
echo '	 <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Coscho table for activities </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">

				
			
			';
				echo '<table  class="table table-bordered table-striped" id="dataTable" >';
echo "
				
				<thead>
				<tr>
				<th>Student Names</th>
				
				";
				$query="SELECT * FROM coscho_fields";
		$query_exe=mysql_query($query);
		
	 $count="SELECT COUNT(`group`) As co_name FROM coscho_fields";
	 $count_exe=mysql_query($count);
	$fetch_count=mysql_fetch_array($count_exe);
	$count_val= $fetch_count[0];
	
		$ctr=0;
		$coscho_id = array();
		  while($name=mysql_fetch_array($query_exe))
		  {
			 if(($name['coscho_id'] == 10)||($name['coscho_id'] == 11))
			 {
				$col = ""; 
			 }
			 else
			 {
				 $col=3;
			 }
			  
	 				 echo '<th id='.$name['coscho_id'].' colspan="'.$col.'">'.$name['group'].'</th>';
					 $coscho_id[$ctr] = $name['coscho_id'];
					 ++$ctr;			 
		  }
		 
		 echo "<th rowspan=2>Total</th>";
		  echo "<th rowspan=2>up grade</th>
		  </tr>
<tr>
<th></th>
";
$c=0;
 for($j=0;$j<$ctr;$j++)
			{
			
 //get act name 
 if(($coscho_id[$j]==10)||($coscho_id[$j]==11))
 {
	echo '<th></th>'; 
 }
 else
 {
			$get_name=
			  "SELECT name_activity
			  FROM types_activites_coscho
			  WHERE group_id = ".$coscho_id[$j]."";
			  $exe_name=mysql_query($get_name);
			  while($fetch_name_act=mysql_fetch_array($exe_name))
			  {
				 
			  
echo "

<th>".$fetch_name_act[0]."</th>";
$c++;
 }
}}
echo "
</tr>";
		  
echo "
		  
</thead>		 <tbody> ";
		  
		 $q="SELECT sId FROM class where classId =".$cid." AND session_id = ".$_SESSION['current_session_id']."";
		$execute=mysql_query($q);
		while($fetch=mysql_fetch_array($execute))
		{
		
		 $student_name="SELECT `Name` FROM student_user WHERE sId =".$fetch['sId']."";
		$exe_name=mysql_query($student_name);
		
	
		
		$fetch_name=mysql_fetch_array($exe_name);
		
			
		echo "<tr><td>".$fetch_name['Name']."</td>";
	
		$ctr_text_box = $ctr;
		
		while($ctr_text_box != 0)
		{
			
			
			 for($i=0;$i<$ctr;$i++)
			{
			
		$sql_query_take="SELECT marks FROM coscho_marks WHERE student_id=".$fetch['sId']." AND co_scho_id=".$coscho_id[$i]."";
		$exe_query_take=mysql_query($sql_query_take);
		$fetch_take=mysql_fetch_array($exe_query_take);
			
			echo '<td><input type="text" onblur="setValueUpdate(this.value,'. $coscho_id[$i].','.$fetch['sId'].',\'check'.++$id_check.'\')" style="width:20px" value="'.$fetch_take['marks'].'" /><span id="check'.$id_check.'"></span></td>
		
			';
			--$ctr_text_box;
		}}
		// Query to get total
		 $sql_get_total="SELECT total,upgrade_sub FROM total_coscho_marks WHERE s_id=".$fetch['sId']."";
		$exe_get_total=mysql_query($sql_get_total);
		$fetch_total_get=mysql_fetch_array($exe_get_total);
		echo '<td><input type="text" style="width:20px"></td><td><input type="text" style="width:20px"></td><td><input type="text" style="width:20px"></td><td><input type="text" style="width:20px"></td>';
		echo "<td>".$fetch_total_get['total']."</td>"; 
		echo "
	    <td>".$fetch_total_get['upgrade_sub']."</td>
	
	 </tr> 
		

";
 
}
echo '</tbody></table></div></div></div>';



}
//temp function to enter marks fa
function admin_enter_marks_fa()
{
	$class=$_GET['class_name'];
echo '     <div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Enter marks In FA(Class: ' .$class.')</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
<form id="demo">


								<div class="section">
								
								<label>Enter FA</label>
								<div>
								<select>
								<option>FA1</option>
								<option>FA2</option>
								<option>FA3</option>
								<option>FA4</option>
								</select>
								</div></div>
								<div class="section">
								<label>Select subject</label>
								<div>
								<select>
								<option>English</option>
								<option>Maths</option>
								<option>Physics</option>
								<option>Chemistry</option>
								</select>
								</div></div>
								<div class="section">
								<label>Select activity</label>
								<div>
								<select>
								<option>oral</option>
								<option>written</option>
								<option>comprehension</option>
								<option>class performance</option>
								</select>
								</div></div>
								<div class="section">
								<label>Max marks</label>
								<div>
							<input type="text" value="50" disabled="disabled"/>
								</div></div>
								<div class="section">
								<label>Max obtained</label>
								<div>
							<input type="text"/>
								</div></div>
								 <div class="section last">
                                                <div><a class="uibutton loading" title="Saving" rel="1" >submit</a> <a class="uibutton special"  >clear form</a> <a class="uibutton loading confirm" title="Checking" rel="0" >Check</a> </div>
                                           </div>

								</form>
								
							  </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->
                    </div><!-- row-fluid -->	
								';

}
//temp function to enter marks in sa
function admin_enter_marks_sa()
{
	$c_name=$_GET['c_name'];
	echo '<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Enter marks In SA(Class: ' .$c_name.')</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
<form id="demo">

<b>Max Marks:90</b>
<div class="section">
<label>Subject</label>
<div>
<label>Marks</label>
</div>
</div>
<div class="section">
<label>English</label>
<div>
<input type="text">
</div>
</div>
<div class="section">
<label>Science</label>
<div>
<input type="text">
</div>
</div>
<div class="section">
<label>Sanskrit</label>
<div>
<input type="text">
</div>
</div>
<div class="section">
<label>Socail science</label>
<div>
<input type="text">
</div>
</div>
<div class="section">
<label>Maths</label>
<div>
<input type="text">
</div>
</div>
<div class="section">
<label>Hindi</label>
<div>
<input type="text">
</div>
</div>

<div class="section last">
                                                <div><a class="uibutton loading" title="Saving" rel="1" >submit</a> <a class="uibutton special"  >clear</a> <a class="uibutton loading confirm" title="Checking" rel="0" >Check</a> </div>
                                           </div>

</form>
								
							  </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->
                    </div><!-- row-fluid -->



';
	
	
	
}
function co_scho_add()
{





if(isset($_GET['addedfield']))
        {
                echo '<h3 align="center">';
                        echo 'Field added';
                echo '</h3>';
        }
        if(isset($_GET['editedfield']))
        {
                echo '<h3 align="center">';
                        echo 'Field Edited and Saved';
                echo '</h3>';
        }
                if(isset($_GET['deletedfield']))
        {
                echo '<h3 align="center">';
                        echo 'Field Deleted';
                echo '</h3>';
        }

echo '	<div class="row-fluid">
     
<div class="widget  span12 clearfix">
    <div class="widget-header">
        <span><i class="icon-home"></i>Co_scholastic</span>
        </div><!-- End widget-header --><div class="widget-content"><br />	
    <a href="admin_add_coscho_name.php"><button class="uibutton icon add ">Add Co scholastic name</button></a>
<a href="admin_view_co_name.php"><button class="uibutton icon disable next ">View co-scho names</button></a>	
<a href="add_descriptive.php"><button class="uibutton icon add ">Add Descriptive Indicators</button></a>
<a href="delete_field.php"><button class="uibutton icon special edit">Delete Fields</button></a>
<span style="float:right"><a href="admin_generate_reports.php"><button class="uibutton ">Reports</button></a></span>
<br>';

      $disp='';
      $q="SELECT *
       FROM class_index  WHERE class <=10 AND level >=2  ORDER BY  class+0 ASC,section ASC ";
       $q_res=mysql_query($q);
   echo '
          <tbody align="center">
          ';
while($res=mysql_fetch_array($q_res))
{
    $_SESSION["id"]=$res[0];
    $disp.=' 

<a href="coscho_marks_insert.php?class_id='.$res['cId'].'"><div class="alertMessage inline info ">
'.$res['class_name'].'</div></a>
';
}
echo $disp;
//coscho_marks_insert.php?cid='.$res['cId'].'
				
echo '</div></div></div>';
				
	
}
//teacher coscho details
function  co_scho_add_teacher()
{
    

//
//if(isset($_GET['addedfield']))
//        {
//                echo '<h3 align="center">';
//                        echo 'Field added';
//                echo '</h3>';
//        }
//        if(isset($_GET['editedfield']))
//        {
//                echo '<h3 align="center">';
//                        echo 'Field Edited and Saved';
//                echo '</h3>';
//        }
//                if(isset($_GET['deletedfield']))
//        {
//                echo '<h3 align="center">';
//                        echo 'Field Deleted';
//                echo '</h3>';
//        }
$priv = $_SESSION['priv'];
echo '	<div class="row-fluid">

    <div class="widget  span12 clearfix">

        <div class="widget-header">
            <span><i class="icon-home"></i>Co_scholastic</span>
        </div><!-- End widget-header -->	

        <div class="widget-content">




    <a href="admin_add_coscho_name.php"><button class="uibutton icon add ">Add Co scholastic name</button></a>

<a href="admin_view_co_name.php"><button class="uibutton icon disable next ">View co-scho names</button></a>	
<a href="add_descriptive.php"><button class="uibutton icon add ">Add Descriptive Indicators</button></a>



<a href="delete_field.php"><button class="uibutton icon special edit">Delete Fields</button></a>


';
 
echo ' 
	<ol class="rounded-list" ';
	  if($priv == 2)
                                    {
	                                  $tid=$_SESSION['user_id'];
                                                            //get classes on the id of the teacher
                                                            $get_classes_tea=
                                                            "SELECT teachers.class , class_index.cId
                                                            FROM teachers
                                                            INNER JOIN class_index
                                                            ON class_index.class_name = teachers.class
                                                            WHERE teachers.tId = ".$tid."  ORDER BY  class_index.class+0 ASC";
                                                            $exe_classes=mysql_query($get_classes_tea);
                                                            while($fetch_classes_teacher=mysql_fetch_array($exe_classes))
                                                            {
                  echo '<li><a href="coscho_marks_insert.php?class_id='.$fetch_classes_teacher[1].'">'.$fetch_classes_teacher[0].'</a></li>'
                                                  ;  

                                                            }}

	echo '
                                                        </ol>
                                             
                                                        ';
									
	






//
//$disp='';
 
	
//  if($priv == 2)
//                                    {
//	                                  $tid=$_SESSION['user_id'];
//                                                            //get classes on the id of the teacher
//                                                        echo     $q=
//                                                            "SELECT teachers.class , class_index.cId,class_index.class_name 
//                                                            FROM teachers
//                                                            INNER JOIN class_index
//                                                            ON class_index.class_name = teachers.class
//                                                            WHERE teachers.tId = ".$tid."";
//$q_res=mysql_query($q);
//echo '
//<tbody align="center">
//';
//while($res=mysql_fetch_array($q_res))
//{
//	$_SESSION["id"]=$res[0];
//	$disp.='
//<tr>
//<td>
//<a href="coscho_marks_insert.php?class_id='.$res['cId'].'"><button class="uibutton loading confirm">'.$res['class_name'].'</button></a>
//	</td>			
//	</tr>';
//
//
//}
//
//                                    }
//
echo $disp;
//coscho_marks_insert.php?cid='.$res['cId'].'
				
echo '</div></div>';
			
				
	
	
		
	
	
    
    
}
//function to generate reports in grades cce
function admin_generate_reports()
{
//query to get all the classes	
$get_all_classes=
"SELECT *
	FROM class_index
                        WHERE class_name  IN(SELECT class_name
	FROM class_index   WHERE class <=10 AND level >=2)        
                         ORDER BY  class+0 ASC , section ASC";
$exe_all_classes=mysql_query($get_all_classes);
//session name
$session_name="SELECT `session_name` FROM session_table WHERE sId=".$_SESSION['current_session_id']."";
$exe_session=mysql_query($session_name);
$fetch_session=  mysql_fetch_array($exe_session);
$session=$fetch_session[0];
echo'  <div class="row-fluid">
                    
                    		<!-- Widget -->
                            <div class="widget  span12 clearfix" >
                                <div class="widget-header" >
                                    <span><i class="icon-warning-sign"></i>Add Co-scholastic name</span>
                                </div><!-- End widget-header -->	
                                <div class="widget-content">
								<form>
							<div class="section">
							<label>Select Class</label>
							<div>
							<select class="chzn-select " data-placeholder="Choose class" name="class_cce" id="class_cce">
								<option value=""></option>
								';
							while($fetch_all_class_name=mysql_fetch_array($exe_all_classes))
							{
								
							echo '<option value="'.$fetch_all_class_name[0].'">'.$fetch_all_class_name['class_name'].'</option>';
								
							}
							echo '</select>
							</div>
							</div>';	
								echo '
								<div class="section">
								<label>Co-Scholastic Part</label>
								<div>
								<select class="chzn-select" data-placeholder="Choose Co-Scho Part" name="area_cce" id="area_cce">
								<option value=""></option>';
								//get all the areas from cosho_areas
								$get_det_areas=
								"SELECT *
								FROM coscho_areas";
								$exe_co_areas=mysql_query($get_det_areas);
								while($fetch_co_areas=mysql_fetch_array($exe_co_areas))
								{
									
								echo '<option value="'.$fetch_co_areas[0].'">'.$fetch_co_areas[2].'('.$fetch_co_areas[1].')</option>';	
									
								}
								echo '
								</select>
								</div>
								</div>
								<div class="section">
								<label>Term</label>
								<div>
								<select class="chzn-select" name="term_cce" id="term_cce" data-placeholder="-select-">
								<option value=""></option>
								<option value="'.$_SESSION['current_session_id'].'">Full Year('.$session.')</option>
								</select>
								</div>
								</div>
								<div class="section last">
								<div>
								<button class="uibutton special" type="button" onclick="generate_report();">Show Report</button>
								</div>
								</div>
								</form>
								<div id="show_report"></div>
								</div>
								</div>
								</div>
								';	
	
	
}
//function to view co-scho-names added
function admin_view_co_names()
{
	echo'  <div class="row-fluid">
                    
                    		<!-- Widget -->
                            <div class="widget  span12 clearfix">
                                <div class="widget-header">
                                    <span><i class="icon-warning-sign"></i>Add Co-scholastic name</span>
                                </div><!-- End widget-header -->	
                                <div class="widget-content">
								
            <table  class="table table-bordered table-striped" id="dataTable" >
<thead>
<tr>';
                                  $id=array();
                                  $k=0;
                                  //get all the area name in the heading
                               $get_area_name=
                                  "SELECT *
                                  FROM coscho_areas";
                                  $exe_area_name=mysql_query($get_area_name);
                                  while($fetch_area_name=mysql_fetch_array($exe_area_name))
                                  {
                                          $id[$k]=$fetch_area_name[0];
                 $get_co_name_col=
                  "SELECT COUNT(coscho_id)
                  FROM coscho_fields
                  WHERE area_id = ".$id[$k]."
                  ";
                  $exe_co_name_col=mysql_query($get_co_name_col);
                  $fetch_col=mysql_fetch_array($exe_co_name_col);

                  if($fetch_col[0]==0)
                  {
                          $col =" ";

                  }
                  else
                  {
                  $col=$fetch_col[0];	
                  echo '
          <th id="'.$id[$k].'" colspan="'.$col.'">'.$fetch_area_name[1].'</th>
          ';
                  }

          $k++;
                                  }
          echo '


          </thead>
          <tbody align="center">';
          //get co-cho-name on that id
          for($i=0;$i<$k;$i++)
          {
          $get_co_name=
          "SELECT *
          FROM coscho_fields
          WHERE area_id = ".$id[$i]."
          ";
          $exe_co_name=mysql_query($get_co_name);
          while($fetch_co_name=mysql_fetch_array($exe_co_name))
          {
          echo '

          <td><span class="tip" title="click to edit"><a href="edit_field_grades.php?co_name_id='.$fetch_co_name[0].'&&co_area_id='.$id[$i].'"title="click to edit">'.$fetch_co_name[2].'</a></span></td>


          ';
          }
          }

          echo '</tr>
          </tbody>
          </table>
          </div></div></div>';

	
}
function add_coscho_name()
{
  echo'  <div class="row-fluid">            
                    		<!-- Widget -->
                            <div class="widget  span12 clearfix">
                                <div class="widget-header">
                                    <span><i class="icon-warning-sign"></i>Add Co-scholastic name</span>
                                </div><!-- End widget-header -->	
                                <div class="widget-content">

				
			<div style="float:center">
				<b style="color:green"></b>
				
				</div>
				
				<form id="validation_demo" action="grades_cce/admin_insert_co_name.php" method="post">
				<div class="section">
				<label>SELECT COSCHOLASTIC AREA</label>
				<div>
				<select name="fa_all" class="chzn-select" id="fa_all">
				
				<option value="0">--select--</option>
				<option value="1">2A</option>
				<option value="2">2B</option>
                                                                                                 <option value="3">3C</option>
				<option value="4">2D</option>
				<option value="5">3A</option>
				<option value="6">3B</option>
				
				</select></div>
				</div>';
				
		
	
	
	echo'
			<div class="section">
			<label>Co scho name</label>
			<div>
				<input type="tel" id="co_scho_name" name="co_scho_name" required="required" style="width:50%"/>
				</div>
				</div>
			
	<div class="section last">
	<div>
	<button type="submit" class="uibutton submit">Save</button>
	</div></div>
				</form>
				

</div>
</div></div>
	
	
	
	';
}
//function to generate students co_scholastic report
function coscho_marks_insert()
{
	$class_id=$_GET['class_id'];
	
	echo'  <div class="row-fluid">
                    
		<!-- Widget -->
		<div class="widget  span12 clearfix">
		<div class="widget-header">
		<span><i class="icon-home">Add marks</i></span>
		</div><!-- End widget-header -->	
		<div class="widget-content">	
		<div style="float:center">
		<b style="color:green"></b>
		</div>
	
	<form id="validation_demo">
	<input type="hidden" value="'.$class_id.'" id="class_id"/>
	<div class="section">
	<label>SELECT COSCHOLASTIC AREA</label>
	<div>
	<select name="fa_all" id="fa_all" onchange="show_activities_coscho(this.value,'.$class_id.');">
	<option value="0">--select--</option>
	';
	//get coscho area id
	 $get_co_area_id=
	"SELECT *
	FROM coscho_areas";
	$exe_coshco_areas=mysql_query($get_co_area_id);
	while($fetch_areas=mysql_fetch_array($exe_coshco_areas))
	{
	echo '<option value="'.$fetch_areas[0].'">'.$fetch_areas[2].'('.$fetch_areas[1].')</option>';	
		
	}
	echo '			
	</select></div>
	</div>';
	echo'
	<span id="type_act"></span>
	<span id="show_report"></span>
	</form>
	</div>
	</div></div>
	';
}
//function to show student list for coscho of thAT CLASS
function show_students_coscho()
{
	$class_id=$_GET['cid'];
	echo '	       <div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Summative Assesment</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead>
                                            <tr>

								
				
				<table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead align="center">
										<tr>
									<th>Name</th>
									
									<th>Add marks</th>
										</tr>
										</thead>
										<tbody align="center">
';
				
//query to show students acc to cid

$get_names_students=
"SELECT student_user.Name , student_user.sId 
FROM student_user
INNER JOIN class
ON class.sId=student_user.sId
WHERE classId=".$class_id."
";
$exe_query_for_name=mysql_query($get_names_students);

while($fetch_students_names=mysql_fetch_array($exe_query_for_name))
{
echo '
<tr>
<form action="coscho_marks_insert.php" method="get">


<input type="hidden" name="student_id" value="'.$fetch_students_names['sId'].'">
<input type="hidden" name="class_id" value="'.$class_id.'">

<td>'.$fetch_students_names[0].'</td>



</td>
<td><button class="uibutton  icon forward normal" type="submit">Add marks obtained</button></td>
</form>
</tr>
';
}

echo '</tbody>
				</table>
				</div>
				</div>
				</div>';
	
	
}

//add types under group cocho
function add_type()
{
	 echo' <div class="row-fluid">
                    
                    		<!-- Widget -->
                            <div class="widget  span12 clearfix">
                                <div class="widget-header">
                                    <span><i class="icon-warning-sign"></i>Select Area</span>
                                </div><!-- End widget-header -->	
                                <div class="widget-content">

				
			<div style="float:center">
				<b style="color:green"></b>
				<br>
				<b style="color:brown"></b>
				</div>
				
				<form action="grades_cce/admin_insert_activity.php" method="post" id="validation_demo">
				
				
				<div class="section">
				<label>SELECT COSCHOLASTIC AREA</label>
				<div>
				<select name="fa_all" onchange="show_activity_coscho(this.value);">
				
				<option value="0">--select--</option>
				<option value="1">LIFE SKILLS</option>
				<option value="2">WORK EDUCATION</option>
				<option value="3">VISUAL AND PERFORMING ARTS</option>
				<option value="4">ATTITUDE AND VALUES</option>
				<option value="5">CO.-SCHOLASTIC ACTIVITY</option>
				<option value="6">HEALTH AND PHYSICAL EDUCATION</option>
				
			</select></div>
				</div>
				
				
	

				
	
				<div class="section">
			<label>ENTER ACTIVITY </label>
				<div>
				<input type="text" name="activity"/>
			</div>
			</div>
				
				
			<div class="section">
			<label>Enter Max_Marks</label>
				<div>
				<input type="text" name="max_marks"/>
			</div>
			</div>
			<div class="section last">
				<div>
				<button class="btn submit_form">Save</button>
				<a class="btn" onClick="ResetForm()" title="Reset  Form">Clear Form</a>

				</div>
				</div>
				</form>
				
				
				

</div>
</div></div>
	
	';
	

}

//add descriptive under cocho activity
function add_descriptive()
{
	echo'  <div class="row-fluid">
                    
                    		<!-- Widget -->
                            <div class="widget  span12 clearfix">
                                <div class="widget-header">
                                    <span><i class="icon-warning-sign"></i>Select Area</span>
                                </div><!-- End widget-header -->	
                                <div class="widget-content">

				
			<div style="float:center">
				<b style="color:green"></b>
				
				</div>
				
				<form action="grades_cce/admin_insert_descriptive.php" method="post" id="validation_demo">
				
				<div class="section">
				<label>SELECT COSCHOLASTIC AREA</label>
				<div>
<select name="fa_all" id="fa_all" class="chzn-select" data-placeholder="Choose Co-scho part" onchange="show_co_scho_name();">
				
				<option value="0"></option>
				';
				//get all the areas id
				$get_areas_id=
				"SELECT *
				FROM coscho_areas";
				$exe_areas_id=mysql_query($get_areas_id);
				while($fetch_areas_id=mysql_fetch_array($exe_areas_id))
				{
				echo '
				<option value="'.$fetch_areas_id[0].'">'.$fetch_areas_id['area_id'].'('.$fetch_areas_id[1].')</option>
				';
				}
				echo '
				</select></div>
				</div>';
				
		
	
	
	echo'
			
		<span id="co_scho_name"></span>		
				
				
			<div class="section">
			<label>ENTER DESCRIPTIVE</label>
				<div>
				<input type="tel" name="desc"  style="width:50%" required="required"/>
			</div>
			</div>
			<div class="section last">
				<div>
				<button class="btn submit_form">Save</button>
				<a class="btn" onClick="ResetForm()" title="Reset  Form">Clear Form</a>

			</div>
				</div>
				</form>
				

</div>
</div></div>
	
	
	
	';
}
function field_add()


{

echo '	<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Add field</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
				<form action="grades_cce/add_fields.php" method="post" id="validation_demo">
				
				
				
				

<div class="section">
<label><b>Co scho Name:</b></label>
<div>
<input type="text" name="field_name" required="required">
</div></div>
<div class="section">
<label><b>Max.marks:</b></label>
<div>
<input type="text" name="max_marks" required="required">
</div>
</div>
<div class="section last">
<div>
<button class="uibutton special">Add</button>
</div>
</div>
</div>
</div>
</div>


	
	';
}

function field_edit_coscho()


{
	$area_id = $_GET['co_area_id'];
	$co_name_id = $_GET['co_name_id'];

echo '<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Edit field</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
				
				
				
				
';
$sql_edit="SELECT dsc_indicator_coscho.dsc_id,
dsc_indicator_coscho.desc_name , coscho_areas.co_area_name , coscho_fields.co_scho_name
FROM dsc_indicator_coscho

INNER JOIN coscho_areas
ON coscho_areas.Id = dsc_indicator_coscho.area_id

INNER JOIN coscho_fields
ON coscho_fields.coscho_id = dsc_indicator_coscho.co_scho_id
WHERE dsc_indicator_coscho.area_id = ".$area_id."
AND dsc_indicator_coscho.co_scho_id = ".$co_name_id."
";
$exe_sql_edit=mysql_query($sql_edit);
$s_no=1;
while($fetch_sql_edit=mysql_fetch_array($exe_sql_edit))
{
	
	
echo '
	<form action="grades_cce/edit_fields.php" method="get">


(<b>'.$s_no++.'</b>)
<div class="section">
<label><b>Co-Scholastic Part:</b></label>
<div>
<input type="text" id="coscho_id" name="coscho_part" value="'.$fetch_sql_edit[2].'">
<input type="hidden" name="part_id_co" value="'.$area_id.'"/>
</div>
</div>
<div class="section">
<label><b>Co scho Name:</b></label>
<div>
<input type="text" id="edit_field_name" name="co_name"  value="'.$fetch_sql_edit[3].'">
<input type="hidden" name="name_id_co" value="'.$co_name_id.'"/>
</div>
</div>
<div class="section">
<label><b>Descriptive Indicator</b></label>
<div>
<input type="text" id="edit_group" name="dsc_indicator" value="'.$fetch_sql_edit[1].'">
<input type="hidden" id="id_dsc" name="id_dsc" value="'.$fetch_sql_edit[0].'"/>
</div>
</div>
<div class="section last">
<div>
<button class="uibutton submit">Save</button>
</div>
</div>

</form>
';

}echo '</div></div></div>



	
	';
}
function field_delete_coscho()


{

echo '	<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Delete field</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">';
								echo '<ol class="rectangle-list">
								<li><a href="admin_delete_co_name.php">Delete co-scho-name</a></li>
								<li><a href="admin_delete_desc.php">Delete descriptive indicators</a></li>
								
								</ol>
								';
				
				echo '

</div></div></div>
	
	';
}

function delete_co_name()
{
	echo '	<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Delete field</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">';
								if(isset($_GET['deleted_field']))
								{
									
								echo '<h5 align="center" style="color:red">Co-name Deleted</h5>';	
								}
								echo '
								   <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead>
										<tr>
										<th>Area Name</th>
										<th>Co-Name</th>
										<th>Action</th>
										</tr>
										</thead>
										<tbody align="center">
										';
								//get all the co-scho-name
								$get_co_name=
								"SELECT coscho_fields.coscho_id,coscho_fields.co_scho_name
								,coscho_areas.co_area_name FROM coscho_fields
								INNER JOIN coscho_areas
								ON coscho_areas.Id = coscho_fields.area_id";
								$exe_co_name=mysql_query($get_co_name);
								while($fetch_co_name=mysql_fetch_array($exe_co_name))
								{
								echo '<tr>
								<td>'.$fetch_co_name[2].'</td>
								<td>'.$fetch_co_name[1].'</td>
								<td><span class="tip" ><a href="grades_cce/delete_fields.php?co_id='.$fetch_co_name[0].'" class="Delete" data-name="delete name" title="Delete"  ><img src="images/icon/icon_delete.png" ></a></span> 
</td>
								</tr>
								';	
									
									
								}
								
								
								echo '
								</tbody>
								</table>
								</div>
								</div>
								</div>';
	
	
}
function delete_desc_ind()
{
	echo '	<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Delete field</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">';
								if(isset($_GET['deleted_field']))
								{
									
								echo '<h5 align="center" style="color:red">Co-name Deleted</h5>';	
								}
								echo '
								   <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead>
										<tr>
										<th>Area Name</th>
										
										<th>Descriptive Name</th>
										<th>Action</th>
										</tr>
										</thead>
										<tbody align="center">
										';
								//get all the co-scho-name
$get_co_name=
"SELECT dsc_indicator_coscho.desc_name, coscho_areas.co_area_name, dsc_indicator_coscho.dsc_id
FROM dsc_indicator_coscho
INNER JOIN coscho_areas ON coscho_areas.Id = dsc_indicator_coscho.area_id

";
								$exe_co_name=mysql_query($get_co_name);
								while($fetch_co_name=mysql_fetch_array($exe_co_name))
								{
									
								echo '<tr>
								<td>'.$fetch_co_name[1].'</td>
							
								<td>'.$fetch_co_name[0].'</td>
								<td><span class="tip" ><a href="grades_cce/delete_fields_co.php?dsc_id='.$fetch_co_name[2].'" class="Delete" data-name="delete name" title="Delete"  ><img src="images/icon/icon_delete.png" ></a></span> 
</td>
								</tr>
								';	
									
									
								}
								//for 2 and 3 descriptives
							
								
								echo '
								</tbody>
								</table>
								</div>
								</div>
								</div>';	
	
}
// functions for scho_activities



function scho_add()

{
	
	

		
			
		echo '	<div class="full_w">
			<div class="h_title">Grades-Scholastic(FA & SA)</div>
				
				<table>
				
						
						
						
				



';


$disp='';
$q="SELECT `cId`,`class_name` FROM class_index ORDER BY class, section ASC";

$q_res=mysql_query($q);
echo '<tr><td>
<button class="add"><a href="admin_add_activity_scho.php">ADD ACTIVITY</a></button></td>
<td>
<button class="add"><a href="admin_add_type_activity_scho.php">ADD TYPES OF ACTIVITIES</a></button></td>
</tr>';
echo '<tr><td>';
echo '<h3>Kindly select class(for entering marks)</h3>';
echo '</td></tr>';
while($res=mysql_fetch_array($q_res))
{$_SESSION["id"]=$res[0];
	$disp.='<tr class="first" id="added_rows">
<td>
			
					<a href="select_exam_list.php?cid='.$res['cId'].'"><button id="btn" class="button approve" >'.$res[1].'</button></a>
				</td>
				
	</tr>';


}



echo $disp;
				
echo '</table></div>';
			
				
	
	
		
	
	
	
	
}
//function to add fa activities
function admin_add_activity()
{
		
		echo '	<div class="full_w">
				<div class="h_title">Grades-Scholastic(FA & SA)</div>
				
				<table>
				
						
						
						
				



';


$disp='';
$q="SELECT *
	FROM class_index
                        WHERE class_name  IN(SELECT class_name
	FROM class_index   WHERE class <=10)        
                         ORDER BY  class+0 ASC";

$q_res=mysql_query($q);

echo '<tr><td>';
echo '<h3>Kindly select class</h3>';
echo '</td></tr>';
while($res=mysql_fetch_array($q_res))
{$_SESSION["id"]=$res[0];
	$disp.='<tr class="first" id="added_rows">
<td>
			
					<a href="admin_add_fa.php?cid='.$res['cId'].'"><button id="btn" class="button approve" >'.$res[1].'</button></a>
				</td>
				
	</tr>';


}



echo $disp;
				
echo '</table></div>';
	
	
}

//function to select types of activities
function admin_add_type_activity()
{
	if(isset($_GET['added']))
	{
	echo '<h5 align="center" style="color:grey">Activity added</h5>';	
		
	}
	echo '	<div class="row-fluid">
                    
                    		<!-- Widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-align-left"></i> Grades-Scholastic(FA) </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">


				
			
				
						
						
						
				



';


$disp='';
$q="SELECT `cId`,`class_name` FROM class_index ORDER BY class, section ASC";

$q_res=mysql_query($q);



echo '<form method ="get" action ="grades_cce/admin_get_activity_type.php" id="demo_validation">'; 




echo '<div class="section">
<label>Enter the type of activity </label>
<div>
<input type="tel" name="activity_type" id="activity_type" required="required">
 </div>
 </div>';
 
echo '<div class="section last">
<div>
<button class="uibutton special">Submit</button>
</div>
</div>
</form>
';
echo '	<table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead align="center">
										<tr>
										<th>Names of activities(Added)</th>
										<th>Action</th>
										</tr></thead>
										<tbody align="center">'; 
//fetch all the activities added
 $get_all_types_activities=
"SELECT Id,type_activities
FROM types_of_activities";
$exe_types_activities=mysql_query($get_all_types_activities);

while($fetch_types_activities=mysql_fetch_array($exe_types_activities))
{
echo '<tr id="type_act_'.$fetch_types_activities[0].'">
<td>'.$fetch_types_activities[1].'</td>
<td id="act_'.$fetch_types_activities[0].'"><span><a original-title="Edit"><img src="images/icon/icon_edit.png" onclick="edit_type_activity('.$fetch_types_activities[0].',\''.$fetch_types_activities[1].'\');"></span>
<span class="tip"><a  data-name="delete name" original-title="Delete"><img src="images/icon/icon_delete.png" onclick="delete_type_act('.$fetch_types_activities[0].');"></a></span>
</td>
</tr>';	
	
	
}

echo '</tbody></table>


';				
echo '</div></div></div>';
	
}

function admin_add_fa()
{
	
	if(isset($_GET['error']))
	{
	echo '<p align="center" style="color:red"><b>Please choose Lesser value for marks</b></p>';	
		
	}
	echo '	<div class="row-fluid">
                    
                    		<!-- Widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-align-left"></i> Grades-Scholastic(FA)(SELECT FA) </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
							

						
';


$disp='';
                      $q="SELECT *
	FROM class_index
                        WHERE class_name  IN(SELECT class_name
	FROM class_index   WHERE class <=10 AND level >=2)        
                         ORDER BY  class+0 ASC , section ASC";

$q_res=mysql_query($q);




//FA 1 button form
echo '<form method ="get" action ="grades_cce/admin_add_activity.php" id="1">';
echo '<input type="hidden" name="fa" value="1">';
echo '
<a class="alertMessage inline info" id="flip">FA1</a>
';
echo '<div id="panel" align="left" style="background-color:#282828">
';

//to select class

echo '
<div class="section">

<label style="color:grey">Select Class</label>
<div>
<select onchange="show_class(this.value,1);" name="class">
';
//query to fetch classes from the database
$sql="SELECT *
	FROM class_index
                        WHERE class_name  IN(SELECT class_name
	FROM class_index   WHERE class <=10 AND level >=2)        
                         ORDER BY  class+0 ASC , section ASC";
$exe_class=mysql_query($sql);
echo '<option>--select class--</option>';
while($fetch_class=mysql_fetch_array($exe_class))
{
    echo '<option value="'.$fetch_class['class_name'].'">'.$fetch_class['class_name'].'</option>
	';
}
echo '</select>
</div></div>
<br>';

//to select subject according to class
echo '
<span id="subject1"></span>';

$sql="SELECT *
      FROM  types_of_activities
   
         ";

$result = mysql_query($sql);
echo '


<div class="section">
<label style="color:grey">Type of activities</label>
<div>
<select id="s1" name="type">
<option>--select</option>



';
while($row = mysql_fetch_array($result))
  {
    echo '<option value="'.$row[0].'">'.$row['type_activities'].'</option>'; 
 
  }

echo '</select>
</div>
</div>';	
	
//to enter name of activity
echo '
<div class="section">
<label style="color:grey">Name of activity</label>
<div>
<input type="text" name="name_of_activity"/>
</div>
</div>
';
//to select type of activities according to subject
echo '
<span id="s1"></span>
';
//to enter max marks
echo '
<div class="section">
<label style="color:grey">Max marks</label>
<div>
<input type="text" name="max_marks" style="width: 80px"/>
</div></div>
<div class="section last">
<div>
<button type ="submit" name ="submit_activity" class="uibutton submit">Submit</button>  
</div></div>
</div>

';
echo '</form>';

//FA 2 button form
echo '<form method ="get" action ="grades_cce/admin_add_activity.php" id="2">
       <input type="hidden" name="fa" value="2"';
echo '
<tr>
<a class="alertMessage inline info" id="flip1">FA2</a>

';
echo '<div id="panel1" align="left" style="background-color:#282828">
';
//to select class
echo '
<div class="section">
<label style="color:grey">Select Class</label>
<div>
<select onchange="show_class(this.value,2);" name="class">
';
//query to fetch classes from the database
$sql="SELECT *
	FROM class_index
                        WHERE class_name  IN(SELECT class_name
	FROM class_index   WHERE class <=10)        
                         ORDER BY  class+0 ASC";
$exe_class=mysql_query($sql);
echo '<option>--select class--</option>';
while($fetch_class=mysql_fetch_array($exe_class))
{
    echo '<option value="'.$fetch_class['class_name'].'">'.$fetch_class['class_name'].'</option>
	';
}
echo '</select>
</div>
</div>
<br>';
//to select subject according to class
echo '
<span id ="subject2"></span>
';

$sql="SELECT *
      FROM  types_of_activities
   
         ";

$result = mysql_query($sql);
echo '


<div class="section">
<label style="color:grey">Type of activities</label>
<div>
<select id="s1" name="type">
<option>--select</option>



';
while($row = mysql_fetch_array($result))
  {
    echo '<option value="'.$row[0].'">'.$row['type_activities'].'</option>'; 
 
  }

echo '</select>
</div>
</div>';	
	
//to enter name of activity
echo '
<div class="section">
<label style="color:grey">Name of activity</label>
<div>
<input type="text" name="name_of_activity"/>
</div>
</div>
';

//to select type of activities according to subject
echo '
<span id="s2"></span>

';
//to enter max marks
echo '
<div class="section">
<label style="color:grey">Max marks</label>
<div>
<input type="text" name="max_marks" style="width: 80px"/>
</div>
</div>
<div class="section last">
<div>
<button type ="submit" name ="submit_activity" class="uibutton submit">Submit</button>
</div></div>  

</div>

';
echo '</form>';

//FA 3 button form
echo '<form method ="get" action ="grades_cce/admin_add_activity.php" id="3">
      <input type="hidden" name="fa" value="3"';
echo '<tr>

<a class="alertMessage inline info" id="flip2">FA3</a>';
echo '<div id="panel2" align="left" style="background-color:#282828">
';
//to select class
echo '
<div class="section">
<label style="color:grey">Select Class</label>
<div>
<select onchange="show_class(this.value,3);" name="class">
';
//query to fetch classes from the database
$sql="SELECT *
	FROM class_index
                        WHERE class_name  IN(SELECT class_name
	FROM class_index   WHERE class <=10)        
                        ORDER BY  class+0 ASC";
$exe_class=mysql_query($sql);
echo '<option>--select class--</option>';
while($fetch_class=mysql_fetch_array($exe_class))
{
    echo '<option value="'.$fetch_class['class_name'].'">'.$fetch_class['class_name'].'</option>
	';
}
echo '</select>
</div>
</div><br>';
//to select subject according to class
echo '<span id ="subject3"></span>';

$sql="SELECT *
      FROM  types_of_activities
   
         ";

$result = mysql_query($sql);
echo '


<div class="section">
<label style="color:grey">Type of activities</label>
<div>
<select id="s1" name="type">
<option>--select</option>



';
while($row = mysql_fetch_array($result))
  {
    echo '<option value="'.$row[0].'">'.$row['type_activities'].'</option>'; 
 
  }

echo '</select>
</div>
</div>';	
	
//to enter name of activity
echo '
<div class="section">
<label style="color:grey">Name of activity</label>
<div>
<input type="text" name="name_of_activity"/>
</div>
</div>
';

//to select type of activities according to subject
echo '<span id="s3"></span>
';
//to enter max marks
echo '
<div class="section">
<label style="color:grey">Max marks</label>
<div>
<input type="text" name="max_marks" style="width: 80px"/>
</div>
</div>
<div class="section last">
<div>
<button type ="submit" name ="submit_activity" class="uibutton submit">Submit</button>  
</div>
</div>
</div>

';
echo '</form>';

//FA 4 button form
echo '<form method ="get" action ="grades_cce/admin_add_activity.php" id="4">
       <input type="hidden" name="fa" value="4"';
echo '<tr>

<a class="alertMessage inline info" id="flip3">FA4</a>';
echo '<div id="panel3" align="left" style="background-color:#282828">
';
//to select class
echo '
<div class="section">
<label style="color:grey">Select Class</label>
<div>
<select onchange="show_class(this.value,4);" name="class">
';
//query to fetch classes from the database
$sql= "SELECT *
	FROM class_index
                        WHERE class_name  IN(SELECT class_name
	FROM class_index   WHERE class <=10)        
                         ORDER BY  class+0 ASC";
$exe_class=mysql_query($sql);
echo '<option>--select class--</option>';
while($fetch_class=mysql_fetch_array($exe_class))
{
    echo '<option value="'.$fetch_class['class_name'].'">'.$fetch_class['class_name'].'</option>
	';
}
echo '</select>
</div></div>
<br>';
//to select subject according to class
echo '<span id ="subject4"></span>';

$sql="SELECT *
      FROM  types_of_activities
   
         ";

$result = mysql_query($sql);
echo '


<div class="section">
<label style="color:grey">Type of activities</label>
<div>
<select id="s1" name="type">
<option>--select</option>



';
while($row = mysql_fetch_array($result))
  {
    echo '<option value="'.$row[0].'">'.$row['type_activities'].'</option>'; 
 
  }

echo '</select>
</div>
</div>';	
	
//to enter name of activity

//to enter name of activity
echo '
<div class="section">
<label style="color:grey">Name of activity</label>
<div>
<input type="text" name="name_of_activity"/>
</div>
</div>
';
//to select type of activities according to subject
echo '<span id ="s4"></span>
';
//to enter max marks
echo '
<div class="section">
<label style="color:grey">Max marks</label>
<div>
<input type="text" name="max_marks" style="width: 80px"/>
</div>
</div>
<div class="section last">
<div>
<button type ="submit" name ="submit_activity" class="uibutton submit">Submit</button>  
</div>
</div>
</div>
';
echo '</form>';	



				
echo '</div>
</div>
</div>';
	
}



//function to add max marks for sa
function add_max_marks_sa()
{
echo '<div class="row-fluid">
                    
                    		<!-- Widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-align-left"></i> Grades-Scholastic(SA)(change max marks) </span>
                                </div><!-- End widget-header -->	
								   <div class="widget-content">
                                <form>
								
                            
							
							<table  class="table table-bordered table-striped" id="dataTable" >
							<thead>
							<tr>
							<th>Assesment Name</th>
							<th>Max Marks</th>
							
							</tr>
							</thead>
							 <tbody align="center">
							 ';
							 // get max marks assigned
							  $get_max_marks=
							 "SELECT *
							 FROM scho_fields
							 WHERE scho_id = 3 || scho_id = 6";
							 $exe_scho_marks=mysql_query($get_max_marks);
							 while($fetch_max_marks=mysql_fetch_array($exe_scho_marks))
							 {
							 
							 echo '
							 <tr>
							 <td><b>'.$fetch_max_marks[1].'</b></td>
							 <td><input type="tel" style="width:30px" value="'.$fetch_max_marks[2].'" onblur="change_max_marks_sa(this.value,'.$fetch_max_marks[0].')" name="sa_max_marks" id="sa_max_marks" required="required"/><span id="change_max_sa_'.$fetch_max_marks[0].'"></span></td>
						
							
							 </tr>
							 ';
							 }
							 echo '
							 </tbody>
							 </table>
							
								
								</div>
								</div>
								</div>';	
	
}

function show_students_for_scho()
{

$id_check = 0;

//it is the 
 $cid=$_GET['cid'];
echo '	<div class="full_w">
				<div class="h_title">List</div>
				
				
			
			';
				echo "<table style='text-align:center' cellpadding='0' cellspacing='0' width='100%'>
				
				<th>Student Names</th>
				
				";
				$query="SELECT * FROM coscho_fields";
		$query_exe=mysql_query($query);
		
	 $count="SELECT COUNT(DISTINCT co_scho_name) As co_name FROM coscho_fields";
	 $count_exe=mysql_query($count);
	$fetch_count=mysql_fetch_array($count_exe);
	$count_val= $fetch_count[0];
	
		$ctr=0;
		$coscho_id = array();
		  while($name=mysql_fetch_array($query_exe))
		  {
			  
	 				 echo '<th id='.$name['coscho_id'].'>'.$name['co_scho_name'].'</th>';
					 $coscho_id[$ctr] = $name['coscho_id'];
					 ++$ctr;			 
		  }
		 
		 
		  
		 $q="SELECT sId FROM class where classId =".$cid." AND session_id = ".$_SESSION['current_session_id']."";
		$execute=mysql_query($q);
		while($fetch=mysql_fetch_array($execute))
		{
		
		 $student_name="SELECT `Name` FROM student_user WHERE sId =".$fetch['sId']."";
		$exe_name=mysql_query($student_name);
		
	
		
		$fetch_name=mysql_fetch_array($exe_name);
		
			
		echo "<tr><td>".$fetch_name['Name']."</td>";
	
		$ctr_text_box = $ctr;
		
		while($ctr_text_box != 0)
		{
			
			
			 for($i=0;$i<$ctr;$i++)
			{
			
		$sql_query_take="SELECT marks FROM coscho_marks WHERE student_id=".$fetch['sId']." AND co_scho_id=".$coscho_id[$i]."";
		$exe_query_take=mysql_query($sql_query_take);
		$fetch_take=mysql_fetch_array($exe_query_take);
			
			echo '<td><input type="text" onblur="setValueUpdate(this.value,'. $coscho_id[$i].','.$fetch['sId'].',\'check'.++$id_check.'\')" style="width:20px" value="'.$fetch_take['marks'].'" /><span id="check'.$id_check.'"></span></td>';
			--$ctr_text_box;
		}}
		echo "<td><button id='btn'>Do Total</button></td>
	
	 </tr> 
		

";

}
echo '</table></div>';


}

function scho_select_list()
{
		
	$cid=$_GET['cid'];
	//$stu_id=$_GET['student_id'];
	

	//get class name from class_index
	$get_clas_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$cid."";
	$exe_cls_name=mysql_query($get_clas_name);
	$fetch_class_name=mysql_fetch_array($exe_cls_name);
	$class_name=$fetch_class_name[0];
	//query to get student name acc to id
	
	
	  echo ' 
	  <div class="row-fluid">
	  <!-- Widget -->
	  <div class="widget  span12 clearfix">
	  <div class="widget-header">
	  <span><i class="icon-warning-sign"></i>Select Area</span>
	  </div><!-- End widget-header -->	
	  <div class="widget-content">
	  ';
		
			    //query to get subjects acc to class id
				$get_subject_id=
				"SELECT DISTINCT subject_id
				FROM teachers
				WHERE class='".$class_name."'";
				$exe_subject_id=mysql_query($get_subject_id);
				echo '
				<form action="grades_cce/admin_insert_marks.php" method="post" id="validation_demo">
				
				
				
				<div class="section">
				<label>Assessment</label>
				<div>
				<select name="fa_all" id="id_fa" onchange="show_activity();"  >
				
				<option>--select--</option>
				
				';
				
			echo '	<option value="1">FA1</option> 
			<option value="2">FA2</option> 
			<option value="4">FA3</option> 
			<option value="5">FA4</option> ';
				
				
				echo '</select></div>
				</div>
				
				
	
			<div class="section">
			<label>Subject</label>
			<div>				
			
				';
				echo '<select name="sub_id" id="sub_id"   data-placeholder="Choose an subject..."  onchange="show_activity()">
				';
				while($fetch_subject_id=mysql_fetch_array($exe_subject_id))
				{
					
				//get subject name on that id
			    $get_subject_name=
				"SELECT DISTINCT subject
				FROM subject
				WHERE subject_id=".$fetch_subject_id['subject_id']."";
				$exe_name_sub=mysql_query($get_subject_name);
				$fetch_names=mysql_fetch_array($exe_name_sub);
				echo '<option value="'.$fetch_subject_id[0].'">'.$fetch_names['subject'].'</option>';	
					
					
					
				}
				echo '</select>
				</div>
				</div>
				
	<input type="hidden" value="'.$cid.'" id="class_id"/>
				
				<div id="type_act"></div>
				<div id="show_act_marks"></div>
				<div id="show_stu_fa"></div>
			
				
				
				
			
		
				</form>
				';
				
				echo '

</div>
</div></div>
	
	';
	
}

function show_student_list()


{
	 $subject_name=$_GET['sub_name'];
	
	
	//Query to get subject id via subject name
	 $sql_subject="SELECT subject_id FROM subject WHERE subject='".$subject_name."'";
	$exe_subject_name=mysql_query($sql_subject);
	$fetch_subject_id=mysql_fetch_array($exe_subject_name);
	$sub_id=$fetch_subject_id['subject_id'];
	
	
	$id_check = 0;
$exam_val=$_GET['exam_val'];
//it is the 
 $cid=$_GET['cid'];
 if($exam_val==1)
 {
echo '	<h2>Formative Assessment (FA) </h2>
				
				
			
			';
				echo "<table style='text-align:left' cellpadding='0' cellspacing='0' width='100%'>
				
				<th>Student Names</th>
				
				";
				$query="SELECT * FROM scho_fields WHERE scho_id<5";
		$query_exe=mysql_query($query);
		
	 $count="SELECT COUNT(DISTINCT scho_name) As co_name FROM scho_fields";
	 $count_exe=mysql_query($count);
	$fetch_count=mysql_fetch_array($count_exe);
	$count_val= $fetch_count[0];
	
		$ctr=0;
		$coscho_id = array();
		  while($name=mysql_fetch_array($query_exe))
		  {
			  
	 				 echo '<th id='.$name['scho_id'].'>'.$name['scho_name'].'</th>';
					 $scho_id[$ctr] = $name['scho_id'];
					 ++$ctr;			 
		  }
		
		 
		  
		 $q="SELECT sId FROM class where classId =".$cid." AND session_id = ".$_SESSION['current_session_id']."";
		$execute=mysql_query($q);
		while($fetch=mysql_fetch_array($execute))
		{
		
		 $student_name="SELECT `Name` FROM student_user WHERE sId =".$fetch['sId']."";
		$exe_name=mysql_query($student_name);
		
	
		
		$fetch_name=mysql_fetch_array($exe_name);
		
			
		echo "<tr><td>".$fetch_name['Name']."</td>";
	
		$ctr_text_box = $ctr;
		
		while($ctr_text_box != 0)
		{
			
			
			 for($i=0;$i<$ctr;$i++)
			{
			
		$sql_query_take="SELECT marks FROM scho_marks WHERE student_id=".$fetch['sId']." AND scho_id=".$scho_id[$i]." AND subject_id=$sub_id";
		$exe_query_take=mysql_query($sql_query_take);
		$fetch_take=mysql_fetch_array($exe_query_take);
			
			echo '<td><input type="text" onblur="setValue_scho('.$sub_id.',this.value,'. $scho_id[$i].','.$fetch['sId'].',\'check'.++$id_check.'\')" style="width:20px" value="'.$fetch_take['marks'].'"/><span id="check'.$id_check.'"></span></td>';
			--$ctr_text_box;
		}}
		echo "<td><button id='btn'>Do Total</button></td>
	
	 </tr> 
	

";

}
echo '</table></div>';
 }

 if($exam_val==2)
 {
	 
echo '	<h2>Summative Assessment (SA) Exams</h2>
				
				
			
			';
				echo "<table style='text-align:left' cellpadding='0' cellspacing='0' width='100%'>
				
				<th>Student Names</th>
				
				";
				$query="SELECT * FROM scho_fields WHERE scho_id>4;";
		$query_exe=mysql_query($query);
		
	 $count="SELECT COUNT(DISTINCT scho_name) As co_name FROM scho_fields";
	 $count_exe=mysql_query($count);
	$fetch_count=mysql_fetch_array($count_exe);
	$count_val= $fetch_count[0];
	
		$ctr=0;
		$coscho_id = array();
		  while($name=mysql_fetch_array($query_exe))
		  {
			  
	 				 echo '<th id='.$name['scho_id'].'>'.$name['scho_name'].'</th>';
					 $scho_id[$ctr] = $name['scho_id'];
					 ++$ctr;			 
		  }
		 
		 
		  
		 $q="SELECT sId FROM class where classId =".$cid." AND session_id = ".$_SESSION['current_session_id']."";
		$execute=mysql_query($q);
		while($fetch=mysql_fetch_array($execute))
		{
		
		 $student_name="SELECT `Name` FROM student_user WHERE sId =".$fetch['sId']."";
		$exe_name=mysql_query($student_name);
		
	
		
		$fetch_name=mysql_fetch_array($exe_name);
		
			
		echo "<tr><td>".$fetch_name['Name']."</td>";
	
		$ctr_text_box = $ctr;
		
		while($ctr_text_box != 0)
		{
			
			
			 for($i=0;$i<$ctr;$i++)
			{
			
		$sql_query_take="SELECT marks FROM scho_marks WHERE student_id=".$fetch['sId']." AND scho_id=".$scho_id[$i]." AND subject_id=$sub_id";
		$exe_query_take=mysql_query($sql_query_take);
		$fetch_take=mysql_fetch_array($exe_query_take);
			
			echo '<td><input type="text" onblur="setValue_scho('.$sub_id.',this.value,'. $scho_id[$i].','.$fetch['sId'].',\'check'.++$id_check.'\')" style="width:20px" value="'.$fetch_take['marks'].'"/><span id="check'.$id_check.'"></span></td>';
			--$ctr_text_box;
		}}
		echo "<td><button id='btn'>Do Total</button></td>
	
	 </tr> 
		

";

}
echo '</table></div>';
 }
 
 
 

	
	
}
//function to show classes of the school so that excel file can be downloaded accordingly
function admin_show_class_for_download()
{

	echo '	<div class="full_w">
				<div class="h_title">Download</div>
				
				<table>
				
						
						
						
				



';


$disp='';
$q_get_class_name="SELECT `cId`,`class_name` FROM class_index ORDER BY class+0 ASC";

$q_res=mysql_query($q_get_class_name);
echo '<tr><td>';
echo '<h3>Kindly select class(for entering marks)</h3>';
echo '</td></tr>';
while($res=mysql_fetch_array($q_res))
{
	$_SESSION["id"]=$res[0];
	$disp.='<tr class="first" id="added_rows">
<td>
			
					<a href="grades_cce/downloading_cbse.php?class_id='.$res[0].'"><button id="btn" class="button approve" >'.$res[1].'</button></a>
				</td>
				
	</tr>';


}



echo $disp;
				
echo '</table></div>';
			
}





//Manish Coding Start.........................................................................................................
//function to caled by grade_cce_scholastic_term_resxult.php
function grade_cce_view_scholastic_result()
{
	
echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View scholastic Report</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	
	';
	//query to get class name
	$get_class="SELECT *
	FROM class_index
                        WHERE class_name  IN(SELECT class_name
	FROM class_index   WHERE class <=10  AND level >=2)        
                        ORDER BY  class+0 ASC , section  ASC ";
	$exe_class=mysql_query($get_class);
	
	echo' <div class="span4"><h3 style="color:">Kindly select class</h3></div>';
	echo '</h2>';
	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo '<ol class="rounded-list">';
	
	while($res=mysql_fetch_array($exe_class))//looping for getting  class 
    	 {	 
		$class=$res['class_name'];
		echo '<tr class="row-fluid" id="added_rows">
		<td><div class="row-fluid">
		<div class="span6">							                                                              
		<li><a href="grade_cce_scholastic_student.php?&cid='.$res['cId'].'">'.$res['class_name'].'</a> 
		           </div>
		</div><!-- end row-fluid  -->
		</td>
		</tr>';	                                                          	                                 			 
    	 }		 
 echo'                        
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->

	   </div><!-- row-fluid -->';					
}
function grade_cce_student_detail()
{
$id_check = 0;

//it is the class id which comes from previous page.
 $cid=$_GET['cid'];
	echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	
	<div class="widget-header">
	<span><i class="icon-home"></i>Select Student </span>
	</div><!-- End widget-header -->	
	
	<div class="widget-content">
	';
	echo '<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th>Admission No.</th>
	<th>Student Names</th>
	
	</tr>
	</thead>
	<tbody align="center">
	';
	
	$get_students = "
	SELECT student_user.Name , student_user.admission_no , student_user.sId
	FROM student_user
	INNER JOIN class
	ON class.sId = student_user.sId
	WHERE class.classId = ".$cid." AND class.session_id=".$_SESSION['current_session_id']."
	";
	$exe_students = mysql_query($get_students);
	while($fetch_students = mysql_fetch_array($exe_students))
	{
	echo'
	<tr>
	<td>'.$fetch_students['admission_no'].'</td>
	<td><a href="grade_cce_scholastic_report_details.php?sid='.$fetch_students['sId'].'&&cid='.$cid.'">'.$fetch_students['Name'].'</a></td>
	</tr>	
	'; 
	}
	echo '</tbody></table>
	';
	echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';					
	}

function grade_cce_scholastic_term_detail()
{
	echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View scholastic Report</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	';
	
	
	$sid=$_GET['sid'];
	$cid=$_GET['cid'];
	
	echo'
	<div class="span4">
    <a href="grade_cce_scholastic_term1_report.php?cid='.$cid.'&sid='.$sid.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">Term1 Report</b> <em> With Marks & Grade</em> </div>
    <div class="breaks"><span></span></div>
	 <a href="grade_cce_scholastic_term1_report_with_grade.php?cid='.$cid.'&sid='.$sid.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">Term1 Report</b> <em> With Grade</em> </div>
    <div class="breaks"><span></span></div>
	
     <a href="grade_cce_scholastic_term2_report.php?cid='.$cid.'&sid='.$sid.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">Term2 Report</b><em>With Marks & Grade</em> </div>
    <div class="breaks"><span></span></div>
	
	 <a href="grade_cce_scholastic_term2_report_with_grade.php?cid='.$cid.'&sid='.$sid.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">Term2 Report</b> <em> With Grade</em> </div>
    <div class="breaks"><span></span></div>
	
   </div><!-- span4 column-->
                                          
';
      echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';			
}
//term 1 report
function grade_cce_scholastic_term1_report()

{
	$cid=$_GET['cid'];
	$sid=$_GET['sid'];

	echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View Term1 Report</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> ';
	
echo'<table class="table table-bordered table-striped" id="2" border="2" width="100%" >
<tr>
<td>
<table  width="80%" >
<tbody>
<tr>
<td align="left"></td>
</tr>
<tr>
<td align="left"><b>School Id:1002322</b><br></br><img src="../images/logo/School.jpg" width=35% height=35%></td>
<td align="center" ><h1>VIDYA BAL BHAWAN SR. SEC. SCHOOL </h1>
            <b>   (Recognised & Affiliated to C.B.S.E)</b><br >
               Kondli Gharoli Road ,Mayur vihar III, Delhi -110096<br>
               Phone No.: 22627876,22626299,22621494  Fax-22617007<br>
               Website: www.vidyabalbhawan.com,   Email-school.vidyabalbhawan@gmail.com
               </td>
</tr>
</tbody>
</table>
';
//get the name of the student on that id
$get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB
FROM student_user
WHERE sId = ".$_GET['sid']."";
$exe_s_name=mysql_query($get_s_name);
$fetch_s_name=mysql_fetch_array($exe_s_name);
$name_student=$fetch_s_name[0];
// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.class_name
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.sId = ".$sid."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 $class_name=$fetch_cls_name['class_name'];
echo '
<body>
<span> <h3 align="center"></h3><hr></hr> </span>                   
								
                                  
								
		<span><h4 align="left">&nbsp;Student Name&nbsp;:&nbsp;'.$name_student.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Admission no&nbsp;:&nbsp;'.$fetch_s_name[3].'</h4></span>
									<span><h4 align="left">&nbsp;Father\'s Name&nbsp;:&nbsp;'.$fetch_s_name[1].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Class Name&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;'.$class_name.'</h4></span>
									<span><h4 align="left">&nbsp;Mother\'s Name:&nbsp;'.$fetch_s_name[2].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date of Birth&nbsp;&nbsp;&nbsp;:&nbsp;'.date("jS F Y",strtotime($fetch_s_name[4])).'</h4></span><hr></hr>
 <span><h4 align="left">(Academic Performance:Scholastic Areas(Term1)</h4> </span>
 <!-- Table widget -->
		<table  class="table table-bordered table-striped" border="1">
                                        <thead align="center">
										  <tr>
                                               
												<th rowspan="2">SUBJECT</th>
										        <th colspan="2">FA1(10%)</th>
												<th colspan="2">FA2(10%)</th>
												<th colspan="2">SA1(30%)</th>
												<th colspan="2">TOTAL(50%)<br/>(FA1+FA2+SA1)</th>
											     </tr>
											     <tr>
												      <th>Marks</th>
												      <th>GR.</th>
													  <th>Marks</th>
													  <th>GR.</th>
													  <th>Marks</th>
													   <th>GR.</th>
													   <th>Marks</th>
												       <th>GR.</th>  
													   
												  </tr>
                                                  </thead>'; 
												  			
											 $marks_tot_grade="";
										    $marks_fa=array();
										    $marks_sa=array();
											
											$marks;
										  $get_subject = "
											SELECT DISTINCT subject.subject,subject.subject_id
											FROM subject
											INNER JOIN scho_marks
											ON subject.subject_id =scho_marks.subject_id
											INNER JOIN class
											ON class.sId=scho_marks.student_id
											INNER JOIN teachers
                                            ON teachers.subject_id = subject.subject_id
											WHERE scho_marks.student_id=".$sid." AND teachers.class='".$class_name."'";
											$exe_subject = mysql_query($get_subject);
											 $j=0;
                                                                                                                                                                                                                                                                         $tot=0;
											while($fetch_subject = mysql_fetch_array($exe_subject))
											{     
												//use of variable
												 $sum1 =0;
									  			 $sum2 =0;
									  			 $total = 0;
												 echo '
										         <tr>
											     <td>'.$fetch_subject['subject'].'</td> ';
												$marks_tot=0;
												 $get_scho_id = 
												  "SELECT scho_id
												  FROM scho_fields LIMIT 0,3";
												  $exe_scho_id=mysql_query($get_scho_id);
												   
												  while($fetch_scho_id=mysql_fetch_array($exe_scho_id))
												  {
												  $scho_id = $fetch_scho_id[0];
														
										
												 //query to get student marks
												    $get_student_marks = "
													SELECT  sum(marks),ID
													  FROM scho_marks
													  WHERE student_id = ".$sid."
													  AND subject_id=".$fetch_subject['subject_id']."
													  AND scho_id = ".$scho_id."  ";
													$exe_stu_marks=mysql_query($get_student_marks);
													$fetch_stu_marks=mysql_fetch_array($exe_stu_marks);
													
												 ';
												 						
												';
												 											 //query to get student marks
												 $get_student_marks_retest = "
												  SELECT  sum(marks),id
												  FROM scho_retest_marks
												  WHERE student_id = ".$sid."
												  AND subject_id=".$fetch_subject['subject_id']."
												  AND scho_id = ".$scho_id."
												  ";
												  
												  $exe_stu_marks_retest=mysql_query($get_student_marks_retest);
												  $fetch_stu_marks_retest=mysql_fetch_array($exe_stu_marks_retest);	
												  
												  //get max marks
												  if($fetch_stu_marks_retest[0]<$fetch_stu_marks[0]) 
												  {
													  $MAX=$fetch_stu_marks[0];
													  
												  }
												  else
												  {
													  $MAX=$fetch_stu_marks_retest[0];
													  
												  }        		 
														//calculate fa1 fa2	
												 if(($scho_id == 1)||($scho_id == 2))
																  {
																	$marks = $MAX * 0.1 ;
																	echo  ' <td align="center">'.$marks.'</td>';	 
																  }
																    if(($scho_id == 3))
																  { 
																
																	  $marks = $MAX * 0.3;
															echo  ' <td align="center">'.$marks.'</td>';
																  }
																  '
																  </td>';
																  
																  
																  
																  //grade for FA1,FA2,SA1
																  $grade1 = 0;
																   if(($MAX>90) && ($MAX<=100))
																   {
																	   $grade1 = 10;
																	   $g ='A1' ;

																   }
																   if(($MAX>80) && ($MAX<=90))
																   {
																	   $grade1 = 9;
																	   $g ='A2' ;
																   }
																   if(($MAX>70) && ($MAX<=80))
																   {
																	   $grade1 = 8;
																	   $g ='B1' ;
																   }
																   if(($MAX>60) && ($MAX<=70))
																   {
																	   $grade1= 7;
																	   $g ='B2' ;
																   }
																   if(($MAX>50) && ($MAX<=60))
																   {
																	   $grade1 = 6;
																	   $g ='C1' ;
																   }
																   if(($MAX>40) && ($MAX<=50))
																   {
																	   $grade1 = 5;
																	   $g ='C2' ;
																   }
																   if(($MAX>32) && ($MAX<=40))
																   {
																	  $grade1 = 4;
																	  $g ='D' ;
																   }
																	if(($MAX>20) && ($MAX<=32))
																   {
																	   $grade1 = 3;
																	   $g ='E1' ;
																   }
																	if(($MAX>=0) && ($MAX<=20))
																   {
																	   $grade1 = 2;
																	   $g ='E2' ;
																   }
																 echo  
																  '
																	<td  align="center"id="'.$scho_id.'">'.$g.'</td>  
																	
																';
																
															   //calculate grade for FA1,FA2,SA1
															   if(($scho_id == 1)||($scho_id == 2)||($scho_id == 3))
																  {  
																  // $marks_tot_grade =$marks_tot_grade+ $MAX*0.5;
																    $tot =$tot+ $MAX;
																        $marks_tot=$tot*0.5;
															                            $sum1 = $sum1+$marks; 
																  }	
																
																   $grade1_sum1 = 0;
																   
																   if((  $marks_tot>90) && (  $marks_tot<=100))
																   {
																	  $grade1_sum1 = 10;
																	  $grade_s1 = 'A1';
																   }
																   if((  $marks_tot>80) && (  $marks_tot<=90))
																   {
																	   $grade1_sum1 = 9;
																	   $grade_s1 = 'A2';
																   }
																   if((  $marks_tot>70) && (  $marks_tot<=80))
																   {
																	   $grade1_sum1 = 8;
																	   $grade_s1 = 'B1';
																   }
																   if((  $marks_tot>60) && (  $marks_tot<=70))
																   {
																	  $grade1_sum1= 7;
																	  $grade_s1 = 'B2';
																   }
																   if((  $marks_tot>50) && (  $marks_tot<=60))
																   {
																	   $grade1_sum1 = 6;
																	   $grade_s1 = 'C1';
																   }
																   if(( $marks_tot>40) && ( $marks_tot<=50))
																   {
																	   $grade1_sum1 = 5;
																	   $grade_s1 = 'C2';
																   }
																   if((  $marks_tot>30) && (  $marks_tot<=40))
																   {
																	 $grade1_sum1 = 4;
																	 $grade_s1 = 'D';
																   }
																	if((  $marks_tot>20) && (  $marks_tot<=30))
																   {
																	   $grade1_sum1 = 3;
																	   $grade_s1 = 'E1';
																   }
																	if((  $marks_tot>=0) && (  $marks_tot<=20))
																   {
																	   $grade1_sum1 = 2;
																	   $grade_s1 = 'E2';
																   }
																
																  
																if($scho_id == 3)
															      {
															echo 	'<td align="center">'.$sum1.'</td>';
															echo 	'<td align="center">'.$grade_s1.'</td>';
																  
															      }	   
																	   														
																		
													     }
													 
													   }
													   
													
									    	
								            
	echo'</tbody></table>
<br></br><br></br>
<div style="float:left"><b>Class Teacher\'s signature</b></div>

<div align="right"><b>Principal\'s signature</b></div>

 
 	     <br></br>                  
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid --> 	
';				
	
	
}	

//term 1 report with grade
function term1_report_with_grade()
{
	
	
	$cid=$_GET['cid'];
	$sid=$_GET['sid'];


	echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View Term1 Report</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  
		  	';
	

//get the name of the student on that id
$get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB
FROM student_user
WHERE sId = ".$_GET['sid']."";
$exe_s_name=mysql_query($get_s_name);
$fetch_s_name=mysql_fetch_array($exe_s_name);
$name_student=$fetch_s_name[0];
// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.class_name
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.sId = ".$sid."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 $class_name=$fetch_cls_name['class_name'];
echo '
<body>
<span> <h3 align="center"></h3><hr></hr> </span>                   
								
                                  
								
		<span><h4 align="left">&nbsp;Student Name&nbsp;:&nbsp;'.$name_student.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Admission no&nbsp;:&nbsp;'.$fetch_s_name[3].'</h4></span>
									<span><h4 align="left">&nbsp;Father\'s Name&nbsp;:&nbsp;'.$fetch_s_name[1].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Class Name&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;'.$class_name.'</h4></span>
									<span><h4 align="left">&nbsp;Mother\'s Name:&nbsp;'.$fetch_s_name[2].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date of Birth&nbsp;&nbsp;&nbsp;:&nbsp;'.date("jS F Y",strtotime($fetch_s_name[4])).'</h4></span><hr></hr>
 <span><h4 align="left">(Academic Performance:Scholastic Areas(Term1)</h4> </span>
 <!-- Table widget -->
		<table  class="table table-bordered table-striped" border="1">
                                        <thead align="center">
										  <tr>
                                               
												<th rowspan="2">SUBJECT</th>
										        <th colspan="1">FA1(10%)</th>
												<th colspan="1">FA2(10%)</th>
												<th colspan="1">SA1(30%)</th>
												<th colspan="1">TOTAL(50%)<br/>(FA1+FA2+SA1)</th>
											     </tr>
											     <tr>
												     
												      <th>GRADE</th>
													
													  <th>GRADE</th>
													 
													   <th>GRADE</th>
													  
												       <th>GRADE</th>  
													   
												  </tr>
                                                  </thead>'; 
												  			
											 $marks_tot_grade="";
										    $marks_fa=array();
										    $marks_sa=array();
											
											$marks;
										  $get_subject = "
											SELECT DISTINCT subject.subject,subject.subject_id
											FROM subject
											INNER JOIN scho_marks
											ON subject.subject_id =scho_marks.subject_id
											INNER JOIN class
											ON class.sId=scho_marks.student_id
											INNER JOIN teachers
                                            ON teachers.subject_id = subject.subject_id
											WHERE scho_marks.student_id=".$sid." AND teachers.class='".$class_name."'
										 ";
											$exe_subject = mysql_query($get_subject);
											 $j=0;
                                                                                         $tot=0;
											while($fetch_subject = mysql_fetch_array($exe_subject))
											{     
												//use of variable
												 $sum1 =0;
									  			 $sum2 =0;
									  			 $total = 0;
												 echo '
										         <tr>
											     <td>'.$fetch_subject['subject'].'</td> ';
												$marks_tot=0;
												 $get_scho_id = 
												  "SELECT scho_id
												  FROM scho_fields LIMIT 0,3";
												  $exe_scho_id=mysql_query($get_scho_id);
												   
												  while($fetch_scho_id=mysql_fetch_array($exe_scho_id))
												  {
												  $scho_id = $fetch_scho_id[0];
														
										
												 //query to get student marks
												    $get_student_marks = "
													SELECT  sum(marks),ID
													  FROM scho_marks
													  WHERE student_id = ".$sid."
													  AND subject_id=".$fetch_subject['subject_id']."
													  AND scho_id = ".$scho_id."  ";
													$exe_stu_marks=mysql_query($get_student_marks);
													$fetch_stu_marks=mysql_fetch_array($exe_stu_marks);
													
												 ';
												 						
												';
												 											 //query to get student marks
												 $get_student_marks_retest = "
												  SELECT  sum(marks),id
												  FROM scho_retest_marks
												  WHERE student_id = ".$sid."
												  AND subject_id=".$fetch_subject['subject_id']."
												  AND scho_id = ".$scho_id."
												  ";
												  
												  $exe_stu_marks_retest=mysql_query($get_student_marks_retest);
												  $fetch_stu_marks_retest=mysql_fetch_array($exe_stu_marks_retest);	
												  
												  //get max marks
												  if($fetch_stu_marks_retest[0]<$fetch_stu_marks[0]) 
												  {
													  $MAX=$fetch_stu_marks[0];
													  
												  }
												  else
												  {
													  $MAX=$fetch_stu_marks_retest[0];
													  
												  }        		 
														//calculate fa1 fa2	
												 if(($scho_id == 1)||($scho_id == 2))
																  {
																	$marks = $MAX * 0.1;
																	//echo  ' <td align="center">'.$marks.'</td>';	 
																  }
																    if(($scho_id == 3))
																  { 
																
																	  $marks = $MAX * 0.3;
														//	echo  ' <td align="center">'.$marks.'</td>';
																  }
																  '
																  </td>';
																  
																  
																  
																  //grade for FA1,FA2,SA1
																  $grade1 = 0;
																   if(($MAX>90) && ($MAX<=100))
																   {
																	   $grade1 = 10;
																	   $g ='A1' ;

																   }
																   if(($MAX>80) && ($MAX<=90))
																   {
																	   $grade1 = 9;
																	   $g ='A2' ;
																   }
																   if(($MAX>70) && ($MAX<=80))
																   {
																	   $grade1 = 8;
																	   $g ='B1' ;
																   }
																   if(($MAX>60) && ($MAX<=70))
																   {
																	   $grade1= 7;
																	   $g ='B2' ;
																   }
																   if(($MAX>50) && ($MAX<=60))
																   {
																	   $grade1 = 6;
																	   $g ='C1' ;
																   }
																   if(($MAX>40) && ($MAX<=50))
																   {
																	   $grade1 = 5;
																	   $g ='C2' ;
																   }
																   if(($MAX>32) && ($MAX<=40))
																   {
																	  $grade1 = 4;
																	  $g ='D' ;
																   }
																	if(($MAX>20) && ($MAX<=32))
																   {
																	   $grade1 = 3;
																	   $g ='E1' ;
																   }
																	if(($MAX>=0) && ($MAX<=20))
																   {
																	   $grade1 = 2;
																	   $g ='E2' ;
																   }
																 echo  
																  '
																	<td  align="center"id="'.$scho_id.'">'.$g.'</td>  
																	
																';
																
															   //calculate grade for FA1,FA2,SA1
															   if(($scho_id == 1)||($scho_id == 2)||($scho_id == 3))
																  {  
																  // $marks_tot_grade =$marks_tot_grade+ $MAX*0.5;
																     $tot =$tot+ $MAX  ;
																	  $marks_tot=$tot*0.5;
																	  $sum1 = $sum1+$marks; 
																  }	
																
																   $grade1_sum1 = 0;
																   
																   if(( $marks_tot>90) && ( $marks_tot<=100))
																   {
																	  $grade1_sum1 = 10;
																	  $grade_s1 = 'A1';
																   }
																   if(( $marks_tot>80) && ( $marks_tot<=90))
																   {
																	   $grade1_sum1 = 9;
																	   $grade_s1 = 'A2';
																   }
																   if(( $marks_tot>70) && ( $marks_tot<=80))
																   {
																	   $grade1_sum1 = 8;
																	   $grade_s1 = 'B1';
																   }
																   if(( $marks_tot>60) && ( $marks_tot<=70))
																   {
																	  $grade1_sum1= 7;
																	  $grade_s1 = 'B2';
																   }
																   if(( $marks_tot>50) && ( $marks_tot<=60))
																   {
																	   $grade1_sum1 = 6;
																	   $grade_s1 = 'C1';
																   }
																   if(( $marks_tot>40) && ( $marks_tot<=50))
																   {
																	   $grade1_sum1 = 5;
																	   $grade_s1 = 'C2';
																   }
																   if(( $marks_tot>30) && ( $marks_tot<=40))
																   {
																	 $grade1_sum1 = 4;
																	 $grade_s1 = 'D';
																   }
																	if(( $marks_tot>20) && ( $marks_tot<=30))
																   {
																	   $grade1_sum1 = 3;
																	   $grade_s1 = 'E1';
																   }
																	if(( $marks_tot>=0) && ( $marks_tot<=20))
																   {
																	   $grade1_sum1 = 2;
																	   $grade_s1 = 'E2';
																   }
																
																  
																if($scho_id == 3)
															      {
														//	echo 	'<td align="center">'.$sum1.'</td>';
															echo 	'<td align="center">'.$grade_s1.'</td>';
																  
															      }	   
																	   														
																		
													     }
													 
													   }
													   
													
									    	
								            
	echo'</tbody></table>
<br></br><br></br>
<div style="float:left"><b>Class Teacher\'s signature</b></div>

<div align="right"><b>Principal\'s signature</b></div>

 
 	     <br></br>                  
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid --> 	
';				
		
	
	
	
	
	
	
}




//term 2 report	
function grade_cce_scholastic_term2_report()
{
	
	$cid=$_GET['cid'];
	$sid=$_GET['sid'];


	echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View Term1 Report</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	';
	

//get the name of the student on that id
$get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB
FROM student_user
WHERE sId = ".$_GET['sid']."";
$exe_s_name=mysql_query($get_s_name);
$fetch_s_name=mysql_fetch_array($exe_s_name);
$name_student=$fetch_s_name[0];
// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.class_name
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.sId = ".$sid."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 $class_name=$fetch_cls_name['class_name'];
echo '
<span> <h3 align="center"></h3><hr></hr> </span>                   								
                      						
		<span><h4 align="left">&nbsp;Student Name&nbsp;:&nbsp;'.$name_student.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Admission no&nbsp;:&nbsp;'.$fetch_s_name[3].'</h4></span>
									<span><h4 align="left">&nbsp;Father\'s Name&nbsp;:&nbsp;'.$fetch_s_name[1].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Class Name&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;'.$class_name.'</h4></span>
									<span><h4 align="left">&nbsp;Mother\'s Name:&nbsp;'.$fetch_s_name[2].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date of Birth&nbsp;&nbsp;&nbsp;:&nbsp;'.date("jS F Y",strtotime($fetch_s_name[4])).'</h4></span><hr></hr>
 <span><h4 align="left">(Academic Performance:Scholastic Areas(Term2)</h4> </span>
		<table  class="table table-bordered table-striped" border="1">
	<thead align="center">
	<tr>
	<th rowspan="2">SUBJECT</th>
	<th colspan="2">FA3(10%)</th>
	<th colspan="2">FA4(10%)</th>
	<th colspan="2">SA2(30%)</th>
	<th colspan="2">TOTAL(50%)<br/>(FA3+FA4+SA2)</th>
	</tr>
	<tr>
	<th>Marks</th>
	<th>GR.</th>
	<th>Marks</th>
	<th>GR.</th>
	<th>Marks</th>
	<th>GR.</th>
	<th>Marks</th>
	<th>GR.</th>  
	</tr>
	</thead>'; 
												  			
	 $marks_tot_grade="";	  
	   $marks_fa=array();
	  $marks_sa=array();
	  
	  $marks;
	$get_subject = "
	  SELECT DISTINCT subject.subject,subject.subject_id
	  FROM subject
	  INNER JOIN scho_marks
	  ON subject.subject_id =scho_marks.subject_id
	  INNER JOIN class
	  ON class.sId=scho_marks.student_id
	 INNER JOIN teachers
      ON teachers.subject_id = subject.subject_id
	 WHERE scho_marks.student_id=".$sid." AND teachers.class='".$class_name."' ";
	  $exe_subject = mysql_query($get_subject);
	   $j=0;  $tot=0;
	  while($fetch_subject = mysql_fetch_array($exe_subject))
	  {     
		  //use of variable
		   $sum1 =0;
		   $sum2 =0;
		   $total = 0;
		   echo '
		   <tr>
		   <td>'.$fetch_subject['subject'].'</td> ';
		  $marks_tot=0;
		   $get_scho_id = 
			"SELECT scho_id
			FROM scho_fields LIMIT 3,6";
			$exe_scho_id=mysql_query($get_scho_id);
			 
			while($fetch_scho_id=mysql_fetch_array($exe_scho_id))
			{
			$scho_id = $fetch_scho_id[0];
				  
  
		   //query to get student marks
			  $get_student_marks = "
			  SELECT  sum(marks),ID
				FROM scho_marks
				WHERE student_id = ".$sid."
				AND subject_id=".$fetch_subject['subject_id']."
				AND scho_id = ".$scho_id."  ";
			  $exe_stu_marks=mysql_query($get_student_marks);
			  $fetch_stu_marks=mysql_fetch_array($exe_stu_marks);
			  
		   ';
								  
		  ';
		//query to get student retest marks
		   $get_student_marks_retest = "
			SELECT  sum(marks),id
			FROM scho_retest_marks
			WHERE student_id = ".$sid."
			AND subject_id=".$fetch_subject['subject_id']."
			AND scho_id = ".$scho_id."
			";
			
			$exe_stu_marks_retest=mysql_query($get_student_marks_retest);
			$fetch_stu_marks_retest=mysql_fetch_array($exe_stu_marks_retest);	
			
			//get max marks
			if($fetch_stu_marks_retest[0]<$fetch_stu_marks[0]) 
			{
				$MAX=$fetch_stu_marks[0];
				
			}
			else
			{
				$MAX=$fetch_stu_marks_retest[0];
				
			}        		 
				  //calculate fa1 fa2	
		   if(($scho_id == 4)||($scho_id == 5))
							{
							  $marks = $MAX * 0.1 ;
							  echo  ' <td align="center">'.$marks.'</td>';	 
							}
							  if(($scho_id == 6))
							{ 
						  
								$marks = $MAX * 0.3;
					  echo  ' <td align="center">'.$marks.'</td>';
							}
							'
							</td>';
							
							
							
							//grade for FA1,FA2,SA1
							$grade1 = 0;
							 if(($MAX>90) && ($MAX<=100))
							 {
								 $grade1 = 10;
								 $g ='A1' ;

							 }
							 if(($MAX>80) && ($MAX<=90))
							 {
								 $grade1 = 9;
								 $g ='A2' ;
							 }
							 if(($MAX>70) && ($MAX<=80))
							 {
								 $grade1 = 8;
								 $g ='B1' ;
							 }
							 if(($MAX>60) && ($MAX<=70))
							 {
								 $grade1= 7;
								 $g ='B2' ;
							 }
							 if(($MAX>50) && ($MAX<=60))
							 {
								 $grade1 = 6;
								 $g ='C1' ;
							 }
							 if(($MAX>40) && ($MAX<=50))
							 {
								 $grade1 = 5;
								 $g ='C2' ;
							 }
							 if(($MAX>32) && ($MAX<=40))
							 {
								$grade1 = 4;
								$g ='D' ;
							 }
							  if(($MAX>20) && ($MAX<=32))
							 {
								 $grade1 = 3;
								 $g ='E1' ;
							 }
							  if(($MAX>=0) && ($MAX<=20))
							 {
								 $grade1 = 2;
								 $g ='E2' ;
							 }
						   echo  
							'
							  <td align="center" id="'.$scho_id.'">'.$g.'</td>  
							  
						  ';
						
						 //calculate grade for FA1,FA2,SA1
						 if(($scho_id == 4)||($scho_id == 5)||($scho_id == 6))
							{  
							// $marks_tot_grade =$marks_tot_grade+ $MAX*0.5;
							   $tot=$tot+ $MAX ;
							   $marks_tot= $tot*0.5;
								$sum1 = $sum1+$marks; 
							}	
							 
							 $grade1_sum1 = 0;
							 
							 if(( $marks_tot>90) && ( $marks_tot<=100))
							 {
								$grade1_sum1 = 10;
								$grade_s1 = 'A1';
							 }
							 if(( $marks_tot>80) && ( $marks_tot<=90))
							 {
								 $grade1_sum1 = 9;
								 $grade_s1 = 'A2';
							 }
							 if(( $marks_tot>70) && ( $marks_tot<=80))
							 {
								 $grade1_sum1 = 8;
								 $grade_s1 = 'B1';
							 }
							 if(( $marks_tot>60) && ( $marks_tot<=70))
							 {
								$grade1_sum1= 7;
								$grade_s1 = 'B2';
							 }
							 if(( $marks_tot>50) && ( $marks_tot<=60))
							 {
								 $grade1_sum1 = 6;
								 $grade_s1 = 'C1';
							 }
							 if(( $marks_tot>40) && ( $marks_tot<=50))
							 {
								 $grade1_sum1 = 5;
								 $grade_s1 = 'C2';
							 }
							 if(( $marks_tot>30) && ( $marks_tot<=40))
							 {
							   $grade1_sum1 = 4;
							   $grade_s1 = 'D';
							 }
							  if(( $marks_tot>20) && ( $marks_tot<=30))
							 {
								 $grade1_sum1 = 3;
								 $grade_s1 = 'E1';
							 }
							  if(( $marks_tot>=0) && ( $marks_tot<=20))
							 {
								 $grade1_sum1 = 2;
								 $grade_s1 = 'E2';
							 }
						  
							
						  if($scho_id == 6)
							{
					  echo 	'<td align="center">'.$sum1.'</td>';
					  echo 	'<td align="center">'.$grade_s1.'</td>';
							
							}	   															  
								  
				   }
			   
				 }
				 
													
									    	
								            
	echo'</tbody></table>
<br></br><br></br>
<div style="float:left"><b>Class Teacher\'s signature</b></div>

<div align="right"><b>Principal\'s signature</b></div>

 	     <br></br>                  
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';					
}	


//term 2 report with grade

function term2_report_with_grade()
{
$cid=$_GET['cid'];
	$sid=$_GET['sid'];


	echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View Term2 Report</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	';
	

//get the name of the student on that id
$get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB
FROM student_user
WHERE sId = ".$_GET['sid']."";
$exe_s_name=mysql_query($get_s_name);
$fetch_s_name=mysql_fetch_array($exe_s_name);
$name_student=$fetch_s_name[0];
// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.class_name
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.sId = ".$sid."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 $class_name=$fetch_cls_name['class_name'];
echo '
<span> <h3 align="center"></h3><hr></hr> </span>                   								
                      						
                       <span><h4 align="left">&nbsp;Student Name&nbsp;:&nbsp;'.$name_student.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Admission no&nbsp;:&nbsp;'.$fetch_s_name[3].'</h4></span>
	<span><h4 align="left">&nbsp;Father\'s Name&nbsp;:&nbsp;'.$fetch_s_name[1].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Class Name&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;'.$class_name.'</h4></span>
	<span><h4 align="left">&nbsp;Mother\'s Name:&nbsp;'.$fetch_s_name[2].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date of Birth&nbsp;&nbsp;&nbsp;:&nbsp;'.date("jS F Y",strtotime($fetch_s_name[4])).'</h4></span><hr></hr>
 <span><h4 align="left">(Academic Performance:Scholastic Areas(Term2)</h4> </span>
		<table  class="table table-bordered table-striped" border="1">
	<thead align="center">
	<tr>
	<th rowspan="2">SUBJECT</th>
	<th colspan="1">FA3(10%)</th>
	<th colspan="1">FA4(10%)</th>
	<th colspan="1">SA2(30%)</th>
	<th colspan="1">TOTAL(50%)<br/>(FA3+FA4+SA2)</th>
	</tr>
	<tr>
	
	<th>GRADE</th>
	
	<th>GRADE</th>
	
	<th>GRADE</th>
	
	<th>GRADE</th>  
	
	</tr>
	</thead>'; 
												  			
				   $marks_tot_grade="";	    $marks_fa=array();
	  $marks_sa=array();
	  
	  $marks;
	$get_subject = "
	  SELECT DISTINCT subject.subject,subject.subject_id
	  FROM subject
	  INNER JOIN scho_marks
	  ON subject.subject_id =scho_marks.subject_id
	  INNER JOIN class
	  ON class.sId=scho_marks.student_id
	  INNER JOIN teachers
                                            ON teachers.subject_id = subject.subject_id
											WHERE scho_marks.student_id=".$sid." AND teachers.class='".$class_name."' ";
	  $exe_subject = mysql_query($get_subject);
	   $j=0;  $tot=0;
	  while($fetch_subject = mysql_fetch_array($exe_subject))
	  {     
		  //use of variable
		   $sum1 =0;
		   $sum2 =0;
		   $total = 0;
		   echo '
		   <tr>
		   <td>'.$fetch_subject['subject'].'</td> ';
		  $marks_tot=0;
		   $get_scho_id = 
			"SELECT scho_id
			FROM scho_fields LIMIT 3,6";
			$exe_scho_id=mysql_query($get_scho_id);
			 
			while($fetch_scho_id=mysql_fetch_array($exe_scho_id))
			{
			$scho_id = $fetch_scho_id[0];
				  
  
		   //query to get student marks
			  $get_student_marks = "
			  SELECT  sum(marks),ID
				FROM scho_marks
				WHERE student_id = ".$sid."
				AND subject_id=".$fetch_subject['subject_id']."
				AND scho_id = ".$scho_id."  ";
			  $exe_stu_marks=mysql_query($get_student_marks);
			  $fetch_stu_marks=mysql_fetch_array($exe_stu_marks);
			  
		   ';
								  
		  ';
													   //query to get student marks
		   $get_student_marks_retest = "
			SELECT  sum(marks),id
			FROM scho_retest_marks
			WHERE student_id = ".$sid."
			AND subject_id=".$fetch_subject['subject_id']."
			AND scho_id = ".$scho_id."
			";
			
			$exe_stu_marks_retest=mysql_query($get_student_marks_retest);
			$fetch_stu_marks_retest=mysql_fetch_array($exe_stu_marks_retest);	
			
			//get max marks
			if($fetch_stu_marks_retest[0]<$fetch_stu_marks[0]) 
			{
				$MAX=$fetch_stu_marks[0];
				
			}
			else
			{
				$MAX=$fetch_stu_marks_retest[0];
				
			}        		 
				  //calculate fa1 fa2	
		   if(($scho_id == 4)||($scho_id == 5))
							{
							  $marks = $MAX * 0.1 ;
							//  echo  ' <td align="center">'.$marks.'</td>';	 
							}
							  if(($scho_id == 6))
							{ 
						  
								$marks = $MAX * 0.3;
					//  echo  ' <td align="center">'.$marks.'</td>';
							}
							'
							</td>';
							
							
							
							//grade for FA1,FA2,SA1
							$grade1 = 0;
							 if(($MAX>90) && ($MAX<=100))
							 {
								 $grade1 = 10;
								 $g ='A1' ;

							 }
							 if(($MAX>80) && ($MAX<=90))
							 {
								 $grade1 = 9;
								 $g ='A2' ;
							 }
							 if(($MAX>70) && ($MAX<=80))
							 {
								 $grade1 = 8;
								 $g ='B1' ;
							 }
							 if(($MAX>60) && ($MAX<=70))
							 {
								 $grade1= 7;
								 $g ='B2' ;
							 }
							 if(($MAX>50) && ($MAX<=60))
							 {
								 $grade1 = 6;
								 $g ='C1' ;
							 }
							 if(($MAX>40) && ($MAX<=50))
							 {
								 $grade1 = 5;
								 $g ='C2' ;
							 }
							 if(($MAX>32) && ($MAX<=40))
							 {
								$grade1 = 4;
								$g ='D' ;
							 }
							  if(($MAX>20) && ($MAX<=32))
							 {
								 $grade1 = 3;
								 $g ='E1' ;
							 }
							  if(($MAX>=0) && ($MAX<=20))
							 {
								 $grade1 = 2;
								 $g ='E2' ;
							 }
						   echo  
							'
							  <td align="center" id="'.$scho_id.'">'.$g.'</td>  
							  
						  ';
						
						 //calculate grade for FA1,FA2,SA1
						 if(($scho_id == 4)||($scho_id == 5)||($scho_id == 6))
							{  
							// $marks_tot_grade =$marks_tot_grade+ $MAX*0.5;
							   $tot =$tot+ $MAX ;
							    $marks_tot= $tot*0.5;
								$sum1 = $sum1+$marks; 
							}	
							 
							 $grade1_sum1 = 0;
							 
							 if(( $marks_tot>90) && ( $marks_tot<=100))
							 {
								$grade1_sum1 = 10;
								$grade_s1 = 'A1';
							 }
							 if(( $marks_tot>80) && ( $marks_tot<=90))
							 {
								 $grade1_sum1 = 9;
								 $grade_s1 = 'A2';
							 }
							 if(( $marks_tot>70) && ( $marks_tot<=80))
							 {
								 $grade1_sum1 = 8;
								 $grade_s1 = 'B1';
							 }
							 if(( $marks_tot>60) && ( $marks_tot<=70))
							 {
								$grade1_sum1= 7;
								$grade_s1 = 'B2';
							 }
							 if(( $marks_tot>50) && ( $marks_tot<=60))
							 {
								 $grade1_sum1 = 6;
								 $grade_s1 = 'C1';
							 }
							 if(( $marks_tot>40) && ( $marks_tot<=50))
							 {
								 $grade1_sum1 = 5;
								 $grade_s1 = 'C2';
							 }
							 if(( $marks_tot>30) && ( $marks_tot<=40))
							 {
							   $grade1_sum1 = 4;
							   $grade_s1 = 'D';
							 }
							  if(( $marks_tot>20) && ( $marks_tot<=30))
							 {
								 $grade1_sum1 = 3;
								 $grade_s1 = 'E1';
							 }
							  if(( $marks_tot>=0) && ( $marks_tot<=20))
							 {
								 $grade1_sum1 = 2;
								 $grade_s1 = 'E2';
							 }
						  
							
						  if($scho_id == 6)
							{
					 // echo 	'<td align="center">'.$sum1.'</td>';
					  echo 	'<td align="center">'.$grade_s1.'</td>';
							
							}	   															  
								  
				   }
			   
				 }
				 
													
									    	
								            
	echo'</tbody></table>
<br></br><br></br>
<div style="float:left"><b>Class Teacher\'s signature</b></div>
<div align="right"><b>Principal\'s signature</b></div>

 	     <br></br>                  
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';		
	
}

	
//Retest details of student
function grade_cce_scholastic_retest()//view class details
{
	

echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View scholastic Report</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	
	';
	//query to get class name
	$get_class="SELECT *
	FROM class_index
                        WHERE class_name  IN(SELECT class_name
	FROM class_index   WHERE class <=10 AND level >=2)        
                         ORDER BY  class+0 ASC , section ASC";
	$exe_class=mysql_query($get_class);
	
	echo' <div class="span4"><h3 style="color:">Kindly select class</h3></div>';
	echo '</h2>';
	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo '<ol class="rounded-list">';
	
	while($res=mysql_fetch_array($exe_class))//looping for getting  class 
    	 {	 
		$class=$res['class_name'];
		echo '<tr class="row-fluid" id="added_rows">
		<td><div class="row-fluid">
		<div class="span6">							                                                              
		<li><a href="grade_cce_retest_student.php?&cid='.$res['cId'].'">'.$res['class_name'].'</a> 
		           </div>
		</div><!-- end row-fluid  -->
		</td>
		</tr>';	                                                          	                                 			 
    	 }		 
      echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';						
}
//
function grade_cce_scholastic_retest_student()//view student details
{
//it is the class id which comes from previous page.
 $cid=$_GET['cid'];
	echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	
	<div class="widget-header">
	<span><i class="icon-home"></i>Select Student </span>
	</div><!-- End widget-header -->	
	
	<div class="widget-content">
	';
	echo '<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th>Admission No.</th>
	<th>Student Names</th>
	
	</tr>
	</thead>
	<tbody align="center">
	';
	
	$get_students = "
	SELECT student_user.Name , student_user.admission_no , student_user.sId
	FROM student_user
	INNER JOIN class
	ON class.sId = student_user.sId
	WHERE class.classId = ".$cid." AND class.session_id=".$_SESSION['current_session_id']."
	";
	$exe_students = mysql_query($get_students);
	while($fetch_students = mysql_fetch_array($exe_students))
	{
	echo'
	<tr>
	<td>'.$fetch_students['admission_no'].'</td>
	<td><a href="grade_cce_retest_marks_scholastic.php?sid='.$fetch_students['sId'].'&&cid='.$cid.'">'.$fetch_students['Name'].'</a></td>
	</tr>	
	'; 
	}
	echo '</tbody></table>
	</div>';
	
	echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';						
}

function grade_cce_scholastic_retest_details()	
{
	$sid=$_GET['sid'];			
	$cid=$_GET['cid'];
	
	//get class name from class_index
	$get_clas_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$cid."";
	$exe_cls_name=mysql_query($get_clas_name);
	$fetch_class_name=mysql_fetch_array($exe_cls_name);
	$class_name=$fetch_class_name[0];
	//query to get student name acc to id
	  echo ' 
	  <div class="row-fluid">
	  <!-- Widget -->
	  <div class="widget  span12 clearfix">
	  <div class="widget-header">
	  <span><i class="icon-warning-sign"></i>Select Area</span>
	  </div><!-- End widget-header -->	
	  <div class="widget-content">
	  ';
		
	//query to get subjects acc to class id
	$get_subject_id=
		"SELECT DISTINCT subject_id
		FROM teachers
		WHERE class='".$class_name."'";
	$exe_subject_id=mysql_query($get_subject_id);
	
	//query to get assesment
  $get_details="
		SELECT `scho_id` ,`scho_name`
		FROM  `scho_fields`
		";
	$execute_details=mysql_query($get_details);
	echo' 
	<form id="validation_demo">
	<div class="section ">
	<label>Assessment</label>        
	<div>
	<select  data-placeholder="Choose Assessment..." class="chzn-select" tabindex="2" id="id_fa" name="fa_all" >        
	<option value=""></option>'; 								
	while($fetch_details=mysql_fetch_array($execute_details))
	{   
	$fetch_scho_name=$fetch_details['scho_name'];
	$fetch_scho_id=$fetch_details[0];	
	echo'
	<option value="'.$fetch_details[0].'">'. $fetch_scho_name.'</option>';
	}
	
	echo'</select>
	</div>
	</div>
	
	<div class="section">
	<label>Subject</label>
	<div>				
<select data-placeholder="Choose an subject..." class="chzn-select" name="sub_id" id="sub_id"  onchange="activity_details();">
	<option value=""></option>
	';
	while($fetch_subject_id=mysql_fetch_array($exe_subject_id))
	{
	
	//get subject name on that id
	$get_subject_name=
		"SELECT DISTINCT subject
		FROM subject
		WHERE subject_id=".$fetch_subject_id['subject_id']."";
	$exe_name_sub=mysql_query($get_subject_name);
	$fetch_names=mysql_fetch_array($exe_name_sub);
	echo '<option value="'.$fetch_subject_id[0].'">'.$fetch_names['subject'].'</option>';	
	}
	echo '
	</select>
	</div>
	</div>	
	
	<div id="type_activity"></div>
	<div id="show_stu_retest_fa"></div>
	<input type="hidden" value="'.$cid.'"  id="cid" name="cid"/>
	<input type="hidden" value="'.$sid.'"  id="sid" name="sid"/>
	</div>
	</form>
	';	
	echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';					
		
	}
	
	
	
//view report 
	
function retest_class_setails()
{
//query to get student name acc to id
	  echo ' 
	  <div class="row-fluid">
	  <!-- Widget -->
	  <div class="widget  span12 clearfix">
	  <div class="widget-header">
	  <span><i class="icon-warning-sign"></i>Select class</span>
	  </div><!-- End widget-header -->	
	  <div class="widget-content">
	  ';	
	
		//query to get class name
	$get_class="SELECT *
	FROM class_index
                        WHERE class_name  IN(SELECT class_name
	FROM class_index   WHERE class <=10 AND level >=2)        
                         ORDER BY  class+0 ASC , section ASC";
	$exe_class=mysql_query($get_class);
	
	echo' <div class="span4"><h3 style="color:">Kindly select class</h3></div>';
	echo '</h2>';
	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo '<ol class="rounded-list">';
	
	while($res=mysql_fetch_array($exe_class))//looping for getting  class 
    	 {	 
		$class=$res['class_name'];
		echo '<tr class="row-fluid" id="added_rows">
		<td><div class="row-fluid">
		<div class="span6">							                                                              
		<li><a href="grade_cce_retest_view_student.php?&cid='.$res['cId'].'">'.$res['class_name'].'</a> 
		           </div>
		</div><!-- end row-fluid  -->
		</td>
		</tr>';	                                                          	                                 			 
    	 }		 
     echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';					
			
	
}

function retest_student_setails()
{

//it is the class id which comes from previous page.
 $cid=$_GET['cid'];
	echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	
	<div class="widget-header">
	<span><i class="icon-home"></i>Select Student </span>
	</div><!-- End widget-header -->	
	
	<div class="widget-content">
	';
	echo '<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th>Admission No.</th>
	<th>Student Names</th>
	
	</tr>
	</thead>
	<tbody align="center">
	';
	
	$get_students = "
	SELECT student_user.Name , student_user.admission_no , student_user.sId
	FROM student_user
	INNER JOIN class
	ON class.sId = student_user.sId
	WHERE class.classId = ".$cid." AND class.session_id=".$_SESSION['current_session_id']."
	";
	$exe_students = mysql_query($get_students);
	while($fetch_students = mysql_fetch_array($exe_students))
	{
	echo'
	<tr>
	<td>'.$fetch_students['admission_no'].'</td>
	<td><a href="grade_cce_retest_view_all_scholastic_report.php?sid='.$fetch_students['sId'].'&cid='.$cid.'">'.$fetch_students['Name'].'</a></td>
	</tr>	
	'; 
	}
	echo '</tbody></table>
	</div>';
	
	echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';							

}


function  retest_all_report()
{
	
echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View scholastic Report</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	';
	
	
	$sid=$_GET['sid'];
	$cid=$_GET['cid'];
	
	echo'
	<div class="span4">
    <a href="grade_cce_retest_view_report_details.php?sid='.$sid.'&cid='.$cid.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <em><b style="color:green">Retest Report</b></em> </div>
    <div class="breaks"><span></span></div>
	
     
	 <a href="grade_cce_overall_scholastic_report.php?sid='.$sid.'&cid='.$cid.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <em><b style="color:green">Overall Retest Report</b></em> </div>
    <div class="breaks"><span></span></div>
	
   </div><!-- span4 column-->
                                          
';
      echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';				
	
	
}


function retest_student_report_details()
{
	
echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View Retest Details</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid">';
	$cid=$_GET['cid'];
    $sid=$_GET['sid'];	
	
	$sname="SELECT Name
	        FROM student_user
			WHERE sId=".$sid."";
	$exe_name=mysql_query($sname);
	$fetch_name=mysql_fetch_array($exe_name);
	$name=$fetch_name['Name'];		
	
	// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_name
	 FROM class_index 
	 WHERE cId = ".$_GET['cid']."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	
	$class_name=$fetch_cls_name['class_name'];
	echo'<h4 style="color:green" align="left">Name: '.$name.'</h4>';
	echo'<h4 style="color:green" align="left">Class: '.$class_name.'</h4>';
	 
	  echo'<table  class="table table-bordered table-striped" id="dataTable" >
	<thead style="color:brown">
	<th>Subject</th>
	<th>FA</th>
	<th>Activity Name</th>
	<th>Retest Marks</th>
	<th>Max Marks</th>
	<tbody align="center">';
	  
	  //query to get retest details
$get_details="SELECT *,scho_fields.scho_name,scho_activity_table.Name_of_activity,scho_activity_table.max_marks,subject.subject
	                FROM scho_retest_marks
					
					INNER JOIN subject 
					ON subject.subject_id=scho_retest_marks.subject_id
					
					
					INNER JOIN scho_fields
					ON scho_fields.scho_id=scho_retest_marks.scho_id
					
					 INNER JOIN scho_activity_table
					 ON scho_activity_table.id=scho_retest_marks.scho_act_id
					 
					WHERE scho_retest_marks.student_id=".$sid."";
					
		$exe_details=mysql_query($get_details);
		while($fetch_details=mysql_fetch_array($exe_details))			
		{
	    $scho_name=$fetch_details['scho_name'];
		$activity_scho=$fetch_details['Name_of_activity'];
		$subject_name=$fetch_details['subject'];
		$retest_marks=$fetch_details['max_marks'];
		$marks=$fetch_details['marks'];
	 echo '
	<tr>
	<td>'.$subject_name.'</td>
	<td>'.$scho_name.'</td>
	<td>'.$activity_scho.'</td>
	<td>'.$marks.'</td>
	<td>'.$retest_marks.'</td>
	</tr>
	';
	}
		echo'
	</tbody></table>
	';
echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	  </div><!-- row-fluid -->
';					
	
}

function grade_cce_scholastic_overall_report()
{
	
    echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix" style="width:115%" >
	      <div class="widget-header" style="width:100%">
	      <span><i class="icon-align-center"></i>View Retest Report</span>
	      </div><!-- End widget-header -->		  
	     <div class="widget-content"><br />';	
	
	$sid=$_GET['sid'];
	$cid=$_GET['cid'];
	// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_name
	 FROM class_index 
	 WHERE cId = ".$_GET['cid']."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 //function to bring name of the student of that class
     $get_stu_names=
	 "SELECT student_user.Name ,student_user.sId
	 FROM student_user
	 INNER JOIN class
	 ON class.sId = student_user.sId
	 
	 WHERE class.classId = ".$_GET['cid']." AND student_user.sId=".$sid." ";
	 $exe_get_stu_names=mysql_query($get_stu_names);
	 $stu_name=mysql_fetch_array($exe_get_stu_names);
	 $name=$stu_name['Name'];
	 echo'<h4 align="left" style="color:green">Name: '.$name.'</h4>
	
         <h4 align="left" style="color:green">Class: '.$fetch_cls_name['class_name'].'</h4>';
	 
	// $fetch_stu_names=mysql_fetch_array($exe_get_stu_names); // names of students
	  $get_subject = "
	  SELECT DISTINCT subject.subject,subject.subject_id
	  FROM subject
	  INNER JOIN scho_marks
	  ON subject.subject_id =scho_marks.subject_id
	  INNER JOIN class
	  ON class.sId=scho_marks.student_id
	 INNER JOIN teachers
                          ON teachers.subject_id = subject.subject_id
	WHERE scho_marks.student_id=".$sid." AND teachers.class='".$fetch_cls_name['class_name']."'
	 ";
	  $exe_subject = mysql_query($get_subject);
	 
	  
	  //get max marks of all assesments
	  $get_max_marks=
	  "SELECT max_marks
	  FROM scho_fields
	  ";
	  $exe_max_marks=mysql_query($get_max_marks);
	  $ass_max_marks=array();
	  $q=0;
	  while($fetch_max_marks=mysql_fetch_array($exe_max_marks))
	  {
		 $ass_max_marks[$q]=$fetch_max_marks[0]; 
		  
		  $q++;
	  }
	  
echo '     
<table  class="table table-bordered table-striped">
<thead >
 <tr>
<th>Name Of Subject</th>

 
	  <th>FA1('.$ass_max_marks[0].')</th>
	  <th>FA1(10%)</th>
	  <th>FA2('.$ass_max_marks[1].')</th>
	  <th>FA2(10%)</th>
	  <th>SA1('.$ass_max_marks[2].')</th>
	  <th>SA1(30%)</th>
	  <th>Total<br/>(FA1+FA2+SA1)</th>
	  <th>FA3('.$ass_max_marks[3].')</th>
	  <th>FA3(10%)</th>
	  <th>FA4('.$ass_max_marks[4].')</th>
	  <th>FA4(10%)</th>
	  <th>SA2('.$ass_max_marks[5].')</th>
	  <th>SA(30%)</th>
	  <th>Total<br/>(FA3+FA4+SA2)</th>
	  <th>Total(100)</th>
	  <th>Upgrade</th>
  </tr>
  <tr> ';

		$marks_fa=array();
		$marks_sa=array();
		
		$marks;
		$k=0;
		$j=0;
while($fetch_subject=mysql_fetch_array($exe_subject))
{			

//get all the scho id
$sum1 =0;
$sum2 =0;
$total = 0;
echo '
 <td>'.$fetch_subject['subject'].'</td>';
 $subject_id=$fetch_subject['subject_id'];
  $k++;
  
//get scho_id
  $get_scho_id = 
  "SELECT scho_id
  FROM scho_fields";
  $exe_scho_id=mysql_query($get_scho_id);
  $i=0;
  while($fetch_scho_id=mysql_fetch_array($exe_scho_id))
     {
  $scho_id = $fetch_scho_id[0];
  
		    //query to get student marks
			  $get_student_marks = "
				SELECT  sum(marks),ID
				FROM scho_marks
				WHERE student_id = ".$sid."
				AND subject_id=".$subject_id."
				AND scho_id = ".$scho_id."
				";
				
				$exe_stu_marks=mysql_query($get_student_marks);
				$fetch_stu_marks=mysql_fetch_array($exe_stu_marks);
				  //query to get student marks
			   $get_student_marks_retest = "
				SELECT  sum(marks),id
				FROM scho_retest_marks
				WHERE student_id = ".$sid."
				AND subject_id=".$subject_id."
				AND scho_id = ".$scho_id."
				";
				
				$exe_stu_marks_retest=mysql_query($get_student_marks_retest);
				$fetch_stu_marks_retest=mysql_fetch_array($exe_stu_marks_retest);	
				//get max marks
				if($fetch_stu_marks_retest[0]<$fetch_stu_marks[0]) 
				{
					$MAX=$fetch_stu_marks[0];
					$flag=0;
				}
				else
				{
					$MAX=$fetch_stu_marks_retest[0];
					$flag=1;
				}
				if($flag==1)
				{
				echo '<td style="color:red"> '.$MAX.'</td> ';
				}
				else
				{
              echo '<td> '.$MAX.'</td>';
				}
			   $i++;  
			echo'
			<td>
			';
if(($scho_id == 1)||($scho_id == 2)||($scho_id == 4)||($scho_id == 5))
{
  $marks = $fetch_stu_marks[0] * 0.1 ;
  echo $marks ;
  
   
}
  if(($scho_id == 3)||($scho_id == 6))
{
	$marks = $fetch_stu_marks[0] * 0.3;
  echo $marks;
}
echo '
</td>	
';
			
				if(($scho_id == 1)||($scho_id == 2)||($scho_id == 3))
				  {
					  $sum1 = $sum1+$marks; 
				
					 
				  }
					if(($scho_id == 4)||($scho_id == 5)||($scho_id == 6))
				  {
					  $sum2 = $sum2+$marks; 
				
					 
				  }
					  
					  ++$j;
				if($scho_id == 3)
			{
				echo'<td>'.$sum1.'</td>';
			}
							  
  }
	   $total = $sum1 + $sum2;
	   $grade = 0;
	   if(($total>90) && ($total<=100))
	   {
		   $grade = 10;
	   }
	   if(($total>80) && ($total<=90))
	   {
		   $grade = 9;
	   }
	   if(($total>70) && ($total<=80))
	   {
		   $grade = 8;
	   }
	   if(($total>60) && ($total<=70))
	   {
		   $grade = 7;
	   }
	   if(($total>50) && ($total<=60))
	   {
		   $grade = 6;
	   }
	   if(($total>40) && ($total<=50))
	   {
		   $grade = 5;
	   }
	   if(($total>32) && ($total<=40))
	   {
		   $grade = 4;
	   }
		if(($total>20) && ($total<=32))
	   {
		   $grade = 3;
	   }
		if(($total>=0) && ($total<=20))
	   {
		   $grade = 2;
	   }
	   
					
	 echo '
		  <td>'.$sum2.'</td>
		  <td>'.$total.'</td>
		  <td>'.$grade.'</td>
		  </tr>';		   
  }
echo ' 
</tbody>
</table>
';
echo'
 
</div><!--  end widget-content -->  	                       
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';			

}

//grade  cce final report select class
function all_student_report()
{
	
	echo '<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>Class Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';	

//query to get class name
	$get_class="SELECT `cId`,`class_name` 
	            FROM class_index 
                    WHERE class<=10 AND level >=2
	            ORDER BY class+0, section ASC";
	$exe_class=mysql_query($get_class);
	
	echo' <div class="span4"><h3 style="color:">Kindly select class</h3></div>';
	echo '</h2>';
	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo '<ol class="rounded-list">';
	
	while($res=mysql_fetch_array($exe_class))//looping for getting  class 
    	 {	 
		$class=$res['class_name'];
		echo '<tr class="row-fluid" id="added_rows">
		<td><div class="row-fluid">
		<div class="span6">							                                                              
		<li><a href="grade_cce_view_report_all_student.php?&cid='.$res['cId'].'">'.$res['class_name'].'</a>            </div>
		</div><!-- end row-fluid  -->
		</td>
		</tr>';	                                                          	                                 			 
    	 }		 	
								
echo'
</div><!--  end widget-content -->  	                       
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';				
}

//view all student report
function view_all_report_cce()
{
      $class_id=$_GET['cid'];
        
 if(($class_id==23)||($class_id==24)||($class_id==25)||($class_id==26)||($class_id==27)||($class_id==28)||($class_id==29)||($class_id==30)||($class_id==31)||($class_id==32)||($class_id==33)||($class_id==34)||($class_id==35)||($class_id==36)||($class_id==37)||($class_id==38)||($class_id==39)||($class_id==40)||($class_id==41)||($class_id==42)||($class_id==43)||($class_id==44)||($class_id==45))
{
   ?> 
    <style>
table,th,td,tr
{
border:1px solid black;
font-size: 19px;
}
</style>

    <?php
    
  
$sid=array();
$i=0;  
$calculate_total_grade=0;
// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.class_name,class.sId
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.classId = ".$_GET['cid']." AND class.session_id=".$_SESSION['current_session_id']."";
	 $exe_class_name=mysql_query($get_class_name);
	while( $fetch_cls_name=mysql_fetch_array($exe_class_name))
	{   
	 $class_name=$fetch_cls_name['class_name'];
	  $student_id=$fetch_cls_name['sId'];
	echo '<p style="z-index:1; position:absolute;  left:85px"><font style="font-size:38px;"><strong>VIDYA BAL BHAWAN SR. SEC. SCHOOL </strong></font></p>';
    echo'
<table  width="100%" style="border:0px">
<tbody ><tr><td>
<table  width="100%" style="border:0px">
<tbody style="border:0px">
<tr style="border:0px">
<td align="left" width="25%" style="border:0px"></td>
</tr>
<tr style="border:0px">';echo'<td align="left" style="border:0px"position:absolute; top:0px; ><img src="images/logo/School.jpg" width="32%"  ></td>';
echo'<br><div ><td  valign="top" width="150%" style="margin-left:-2px; width:100%;border:0px">
<font style="margin-left:0px; width:100%; font-size-size:12px;"" align="center"><br>
      Kondli Gharoli Road, Mayur Vihar III, Delhi -110096 </font>
     <br>
    <b style="margin-left:-22px; width:100%;"><font style="font-size:24px;">Record of Academic Performance(2014-2015)</b>  </font> </td>
</tr>
</tbody>
</table>';
       
	 //get the name of the student on that id
//$j=0;
$z=0; 
  $get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB,sId
FROM student_user
 WHERE sId=".$student_id."";
$exe_s_name=mysql_query($get_s_name);
while($fetch_s_name=mysql_fetch_array($exe_s_name))
  
  {
	  $sid=$fetch_s_name['sId'];
	/*$sid1[$z]=$sid;
	$i++;*/
	  $name_student=$fetch_s_name[0];
	
  

  $jke=1;
  
echo'       
<div class="row-fluid">
<div class="widget  span12 clearfix">
<div class="widget-header">
<table id=8   valign="top" class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<tbody  width="100%">
<tr width="40%"  style="border:0px solid black;border-collapse:collapse;">
<td align="left" width="40%"  style="border:0px solid black;border-collapse:collapse;">Student Name: <b style="font-size:16px"> '.$name_student.'</b></font></td>
<td align="left" width="25%"  style="border:0px solid black;border-collapse:collapse;">Admission No.:<b style="font-size:16px" > '.$fetch_s_name[3].'</b></font></td>
<td align="left"  style="border:0px solid black;border-collapse:collapse;">Examination Roll.No:<b style="font-size:16px"></b></td>
</tr>
<tr width="20%"  style="border:0px solid black;border-collapse:collapse;">
<td align="left" width="40%"  style="border:0px solid black;border-collapse:collapse;">Fathers Name:<b style="font-size:16px"> '.$fetch_s_name[1].'</b></font></td>
<td align="left" width="25%"  style="border:0px solid black;border-collapse:collapse;">Class & Section:<b style="font-size:16px" > '.$class_name.'       '.$section_name.' </b></font></td> 
<td align="left"   style="border:0px solid black;border-collapse:collapse;">D.O.B.- <b style="font-size:16px">'.date("jS F Y",strtotime($fetch_s_name[4])).' <b></b></b></font></td>
</tr>
<tr  width="40%"   style="border:0px solid black;border-collapse:collapse;">
<td align="left" width="40%"  style="border:0px solid black;border-collapse:collapse;">Mothers Name: <b style="font-size:16px"> '.$fetch_s_name[2].'</b></font></td >
<td align="left" width="25%"  style="border:0px solid black;border-collapse:collapse;">Blood Group:<b style="font-size:16px"> '.$blood.'</b></td> 
    <td  align="left"   style="border:0px solid black;border-collapse:collapse;"><font style="font-size:20px">Height: <b  width:100%;" style="font-size:14px"><font style="margin-left:40px; width:100%;" style="font-size:12px"></b><font size=""  style="font-size:20px">Weight:<b></td>
</tr>
</tbody></table>  							
       </div><!-- End widget-header -->	          							
<div class="widget-content" align="center">						
<!-- Table widget -->
    <table  class="table table-bordered table-striped" width="100%" valign="top" align="center" style="border:0px solid black;border-collapse:collapse;">
<thead >                                          
<tr">
   <th width="16%" style="font-size:16px" align="left">SCHOLASTIC AREA</th> 
   <th colspan="4" align="center"  style="font-size:16px;width:28%;">TERM-I</th>
   <th colspan="4" align="center"  style="font-size:16px;width:28%;">TERM-II</th>
   <th colspan="4" align="center"  style="font-size:16px;width:28%;">FINAL ASSESSMENT</th>
</tr>
<tr height="40" style="background-color:#CCCCCC;-webkit-print-color-adjust: exact;">
   <th style="font-size:20px;width:16%" >&nbsp;&nbsp;&nbsp;&nbsp;SUBJECT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
   <th align="center" style="font-size:18px;width:7%;">FA1<br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;">FA2<br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;">SA1<br>(30%)</th>
   <th align="center" style="font-size:18px;width:7%;"> Total<br>(50%)</th>
   <th align="center" style="font-size:18px;width:7%;">FA3<br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;"> FA4 <br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;">SA2<br>(30%)</th>
   <th align="center" style="font-size:18px;width:7%;"> Total<br>(50%)</th>
   <th align="center" style="font-size:18px;width:7%;">FA<br>(40%)</th>
   <th align="center" style="font-size:18px;width:7%;">SA<br>(60%)</th>
   <th align="center" style="font-size:18px;width:7%;">Overall<br>(FA+SA)<br  style="font-size:18px">100%</th>
   <th align="center" style="font-size:17px;width:7%;">Grade Point</th>
</tr> 															
  </thead> 
  
';

$total_max_sa_marks=0;
$total_max_fa_marks=0;
           $total_max_marks=0;
$marks_fa=array();
$marks_sa=array();
$sid=array();
$marks_total=0;
$get_marks=0;
$ctr=0; $sum_total_fa_get_marks=0;
    $final_grade=array(); 
    $sum_sa=0;

  $total_fas_marks=0;
   $get_subject = "
SELECT DISTINCT subject.subject, subject.subject_id,`cce_marks_scho_type_skills_table`.sts_id,cce_subject_type_skills_id_table.class_id
FROM subject
INNER JOIN `cce_subject_type_skills_id_table` 
ON subject.subject_id =`cce_subject_type_skills_id_table` .subject_id

INNER JOIN `cce_marks_scho_type_skills_table`
ON `cce_marks_scho_type_skills_table`.sts_id=cce_subject_type_skills_id_table.sts_id

INNER JOIN  cce_term_test_table
ON cce_term_test_table.id=cce_marks_scho_type_skills_table.term_test_id

 
INNER JOIN class
ON class.sId=`cce_marks_scho_type_skills_table` .student_id
WHERE `cce_marks_scho_type_skills_table` .student_id=$student_id  AND cce_term_test_table.class_id=$class_id 
   ";
$exe_subject = mysql_query($get_subject);
$j=0;
while($fetch_subject = mysql_fetch_array($exe_subject))
{         
 $sts_id=$fetch_subject['sts_id'];
$sub_id=$fetch_subject['subject_id'];
$subject=$fetch_subject['subject'];
////$term_dd=$fetch_subject['term_id'];
//echo $subject="SELECT  DISTINCT subject  from subject where subject_id=$sub_id ";
//
//$exe_subject=mysql_query($subject);
//while($fetch_subjects=  mysql_fetch_array($exe_subject))
//{
// $sub=$fetch_subjects['subject'];




 echo '
<tr>
<td style="font-size:16px"><b>'.$subject.'</td> ';
  
    $get_term_id="SELECT   term_id  FROM  cce_term_table   ";
    $exe_term=mysql_query($get_term_id);
    while($fetch_term_id=mysql_fetch_array($exe_term))
    {           $term=$fetch_term_id[0];
    
$get_marks="SELECT cce_marks_scho_type_skills_table.* ,class.classId 
          FROM  `cce_marks_scho_type_skills_table` 
          
 INNER JOIN cce_term_test_table
                  ON cce_marks_scho_type_skills_table.term_test_id=cce_term_test_table.id

 INNER JOIN cce_term_table
                  ON cce_term_table.term_id=cce_term_test_table.term_id
           INNER JOIN class 
         ON class.sId=cce_marks_scho_type_skills_table.student_id
       WHERE  cce_marks_scho_type_skills_table.sts_id=$sts_id  AND cce_marks_scho_type_skills_table.student_id=$student_id  AND cce_term_table.term_id=$term AND cce_term_test_table.class_id=$class_id"   ;
 $exe_marks=mysql_query($get_marks);
 while($fetch_marks=mysql_fetch_array($exe_marks))
 {
  $marks=$fetch_marks['marks'];
   $term_test_id=$fetch_marks[1];
  $cid=$fetch_marks['classId'];
   $get_test_id="SELECT cce_test_table.* FROM cce_test_table
                  INNER JOIN cce_term_test_table
                  ON cce_term_test_table.test_id=cce_test_table.test_id	
                  INNER JOIN cce_term_table
                  ON cce_term_table.term_id=cce_term_test_table.term_id
                
                  WHERE cce_term_test_table.id=$term_test_id  AND cce_term_test_table.class_id=$cid   AND cce_term_table.term_id=$term AND cce_term_test_table.class_id=$class_id";
              
 $exe_get_id=  mysql_query($get_test_id);
 while($fetch_test_id=mysql_fetch_array($exe_get_id))
 {   $test_id=$fetch_test_id['test_id'];
      $ctr++;
  $max_marks=$fetch_test_id['max_marks'];
$get_marks=$get_marks+$marks;
if(($test_id==5)||($test_id==6))
   {   
                                             
                                                            $total_max_fa_marks=$total_max_fa_marks+$max_marks;
                                                         
                                                             $marks_grade= ($marks *100)/$max_marks ;



                                  }
                                    if(($test_id == 7))
                                  {
                                   $total_max_sa_marks=$total_max_sa_marks+$max_marks;
                                       $marks_grade = $marks *100/$max_marks ;


                                  }     
 }


    //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($marks_grade>90) && ($marks_grade<=100))
                                   {
                                           $grade1 = 10;
                                           $g ='A1' ;
                                   }
                                   if(($marks_grade>80) && ($marks_grade<=90))
                                   {
                                           $grade1 = 9;
                                           $g ='A2' ;
                                   }
                                   if(($marks_grade>70) && ($marks_grade<=80))
                                   {
                                           $grade1 = 8;
                                           $g ='B1' ;
                                   }
                                   if(($marks_grade>60) && ($marks_grade<=70))
                                   {
                                           $grade1= 7;
                                           $g ='B2' ;
                                   }
                                   if(($marks_grade>50) && ($marks_grade<=60))
                                   {
                                           $grade1 = 6;
                                           $g ='C1' ;
                                   }
                                   if(($marks_grade>40) && ($marks_grade<=50))
                                   {
                                           $grade1 = 5;
                                           $g ='C2' ;
                                   }
                                   if(($marks_grade>32) && ($marks_grade<=40))
                                   {
                                          $grade1 = 4;
                                          $g ='D' ;
                                   }
                                        if(($marks_grade>20) && ($marks_grade<=32))
                                   {
                                           $grade1 = 3;
                                           $g ='E1' ;
                                   }
                                    if(($marks_grade>=0) && ($marks_grade<=32))
                                   {
                                           $grade1 = 2;
                                           $g ='E2' ;
                                   }


echo'<td align="center">'.$g.'</td>';
 $marks_total=$get_marks;
 if($jke<4)
{
 $total_max_marks=$total_max_marks+$max_marks;
}
$jke++;
$total_max_marks;
$total=($get_marks*100)/$total_max_marks;

$grade1_sum1 = 0;

                              

                                 //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($total>90) && ($total<=100))
                                   {
                                           $grade1 = 10;
                                           $gg ='A1' ;
                                   }
                                   if(($total>80) && ($total<=90))
                                   {
                                           $grade1 = 9;
                                           $gg ='A2' ;
                                   }
                                   if(($total>70) && ($total<=80))
                                   {
                                           $grade1 = 8;
                                           $gg ='B1' ;
                                   }
                                   if(($total>60) && ($total<=70))
                                   {
                                           $grade1= 7;
                                           $gg ='B2' ;
                                   }
                                   if(($total>50) && ($total<=60))
                                   {
                                           $grade1 = 6;
                                           $gg ='C1' ;
                                   }
                                   if(($total>40) && ($total<=50))
                                   {
                                           $grade1 = 5;
                                           $gg ='C2' ;
                                   }
                                   if(($total>32) && ($total<=40))
                                   {
                                          $grade1 = 4;
                                          $gg ='D' ;
                                   }
                                   if(($total>20) && ($total<=32))
                                   {
                                           $grade1 = 3;
                                           $gg ='E1' ;
                                   }
                                    if(($total>=0) && ($total<=20))
                                   {
                                           $grade1 = 2;
                                           $gg ='E2' ;
                                   }
                                   
         
                                   
 }   //echo   $final_grade=$gg;        
$sum_total_fa_get_marks=0;
$sum_total_sa_get_marks=0;
     echo'<td align="center">'.$gg.'</td>';
    }
         $get_test_id="SELECT * FROM cce_test_table ";
     $exe_test_id=mysql_query($get_test_id);
     while($fetch_test_id=mysql_fetch_array($exe_test_id))
     {  
            $fa_id=$fetch_test_id[0];
      if(($fa_id==5)||($fa_id==6)||($fa_id==8)||($fa_id==9))
   {    
                  $get_fa_marks="SELECT DISTINCT  SUM(marks) AS marks  FROM  `cce_marks_scho_type_skills_table` 
                                         
                                  INNER JOIN  `cce_term_test_table` 
                                  ON `cce_term_test_table` .id=cce_marks_scho_type_skills_table.term_test_id

                                   INNER  JOIN  `cce_test_table` 
                                   ON `cce_test_table`.test_id=cce_term_test_table.test_id 
                                   
                                  INNER JOIN cce_subject_type_skills_id_table
                                  ON cce_subject_type_skills_id_table.sts_id=cce_marks_scho_type_skills_table.sts_id
                                WHERE cce_test_table.test_id=$fa_id AND cce_marks_scho_type_skills_table.student_id=$student_id "
                         . "AND cce_subject_type_skills_id_table.subject_id=$sub_id AND cce_term_test_table.class_id=$class_id";
                          $fa_total_marks=mysql_query($get_fa_marks);
                         while($fetch_fa_marks=mysql_fetch_array($fa_total_marks))
                         {
                          
                             $sum_fa=$fetch_fa_marks['marks'];
                         '<br>'. $sum_total_fa_get_marks=$sum_total_fa_get_marks+$sum_fa.'';
                         }


   
     }
   

//     
    }
    
     //$get_fa=($sum_total_fa_get_marks*100)/100;
                                 //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($sum_total_fa_get_marks>90) && ($sum_total_fa_get_marks<=100))
                                   {
                                           $grade1 = 10;
                                           $gggfa ='A1' ;
                                   }
                                   if(($sum_total_fa_get_marks>80) && ($sum_total_fa_get_marks<=90))
                                   {
                                           $grade1 = 9;
                                           $gggfa ='A2' ;
                                   }
                                   if(($sum_total_fa_get_marks>70) && ($sum_total_fa_get_marks<=80))
                                   {
                                           $grade1 = 8;
                                           $gggfa ='B1' ;
                                   }
                                   if(($sum_total_fa_get_marks>60) && ($sum_total_fa_get_marks<=70))
                                   {
                                           $grade1= 7;
                                           $gggfa ='B2' ;
                                   }
                                   if(($sum_total_fa_get_marks>50) && ($sum_total_fa_get_marks<=60))
                                   {
                                           $grade1 = 6;
                                           $gggfa ='C1' ;
                                   }
                                   if(($sum_total_fa_get_marks>40) && ($sum_total_fa_get_marks<=50))
                                   {
                                           $grade1 = 5;
                                           $gggfa ='C2' ;
                                   }
                                   if(($sum_total_fa_get_marks>32) && ($sum_total_fa_get_marks<=40))
                                   {
                                          $grade1 = 4;
                                          $gggfa ='D' ;
                                   }
                                   if(($sum_total_fa_get_marks>20) && ($sum_total_fa_get_marks<=32))
                                   {
                                           $grade1 = 3;
                                           $gggfa ='E1' ;
                                   }
                                    if(($sum_total_fa_get_marks>=0) && ($sum_total_fa_get_marks<=20))
                                   {
                                           $grade1 = 2;
                                           $gggfa ='E2' ;
                                   }
    
    
   echo'<td align="center">'.$gggfa.'</td>';     
     
          
   /////////////////////total sa//////////////////////////////////////////////////
     $get_test_sa_id="SELECT * FROM cce_test_table ";
     $exe_test_sa_id=mysql_query($get_test_sa_id);
     while($fetch_test_sa_id=mysql_fetch_array($exe_test_sa_id))
     {  
            $sa_id=$fetch_test_sa_id[0];
      if(($sa_id==7)||($sa_id==10))
       {    
                   $get_sa_marks="SELECT DISTINCT  SUM(marks) AS marks  FROM  `cce_marks_scho_type_skills_table` 
                                         
                                  INNER JOIN  `cce_term_test_table` 
                                  ON `cce_term_test_table` .id=cce_marks_scho_type_skills_table.term_test_id

                                   INNER  JOIN  `cce_test_table` 
                                   ON `cce_test_table`.test_id=cce_term_test_table.test_id 
                                   
                                  INNER JOIN cce_subject_type_skills_id_table
                                  ON cce_subject_type_skills_id_table.sts_id=cce_marks_scho_type_skills_table.sts_id
                                WHERE cce_test_table.test_id=$sa_id AND cce_marks_scho_type_skills_table.student_id=$student_id "
                         . "AND cce_subject_type_skills_id_table.subject_id=$sub_id AND cce_term_test_table.class_id=$class_id";
                          $sa_total_marks=mysql_query($get_sa_marks);
                         while($fetch_sa_marks=mysql_fetch_array($sa_total_marks))
                         {
                          
                              $sum_sa=$fetch_sa_marks['marks'];
                         '<br>'. $sum_total_sa_get_marks=$sum_total_sa_get_marks+$sum_sa.'';
                         }


   
     }
   

//     
 
    }
        $get_sa=($sum_total_sa_get_marks*100)/200;
      //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($get_sa>90) && ($get_sa<=100))
                                   {
                                           $grade1 = 10;
                                           $gggsa  ='A1' ;
                                   }
                                   if(($get_sa>80) && ($get_sa<=90))
                                   {
                                           $grade1 = 9;
                                           $gggsa  ='A2' ;
                                   }
                                   if(($get_sa>70) && ($get_sa<=80))
                                   {
                                           $grade1 = 8;
                                           $gggsa  ='B1' ;
                                   }
                                   if(($get_sa>60) && ($get_sa<=70))
                                   {
                                           $grade1= 7;
                                           $gggsa  ='B2' ;
                                   }
                                   if(($get_sa>50) && ($get_sa<=60))
                                   {
                                           $grade1 = 6;
                                           $gggsa  ='C1' ;
                                   }
                                   if(($get_sa>40) && ($get_sa<=50))
                                   {
                                           $grade1 = 5;
                                           $gggsa  ='C2' ;
                                   }
                                   if(($get_sa>32) && ($get_sa<=40))
                                   {
                                          $grade1 = 4;
                                          $gggsa  ='D' ;
                                   }
                                   if(($get_sa>20) && ($get_sa<=32))
                                   {
                                           $grade1 = 3;
                                           $gggsa  ='E1' ;
                                   }
                                    if(($get_sa>=0) && ($get_sa<=20))
                                   {
                                           $grade1 = 2;
                                           $gggsa ='E2' ;
                                   }
    

    
    
   echo'<td align="center">'.$gggsa .'</td>';     
     $sum_fa_sa_total=$sum_total_sa_get_marks+$sum_total_fa_get_marks;
     
     $grand_total=($sum_fa_sa_total*100)/300;
      //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade_point = 0;
                                   if(($grand_total>90) && ($grand_total<=100))
                                   {
                                           $point = 10;
                                           $grand_total_grade ='A1' ;
                                   }
                                   if(($grand_total>80) && ($grand_total<=90))
                                   {
                                           $point = 9;
                                           $grand_total_grade ='A2' ;
                                   }
                                   if(($grand_total>70) && ($grand_total<=80))
                                   {
                                           $point = 8;
                                           $grand_total_grade ='B1' ;
                                   }
                                   if(($grand_total>60) && ($grand_total<=70))
                                   {
                                           $point= 7;
                                           $grand_total_grade ='B2' ;
                                   }
                                   if(($grand_total>50) && ($grand_total<=60))
                                   {
                                           $point = 6;
                                           $grand_total_grade ='C1' ;
                                   }
                                   if(($grand_total>40) && ($grand_total<=50))
                                   {
                                           $point = 5;
                                           $grand_total_grade ='C2' ;
                                   }
                                   if(($grand_total>32) && ($grand_total<=40))
                                   {
                                          $point = 4;
                                          $grand_total_grade ='D' ;
                                   }
                                   if(($grand_total>20) && ($grand_total<=32))
                                   {
                                           $point = 3;
                                           $grand_total_grade ='E1' ;
                                   }
                                    if(($grand_total>=0) && ($grand_total<=20))
                                   {
                                           $point = 2;
                                           $grand_total_grade ='E2' ;
                                   }
                                   
   echo'<td align="center">'.$grand_total_grade.'</td>';       
      
  $total_overall=$total_overall+$sum_fa_sa_total;
   
  $total_overall_per=($total_overall*100)/2100;
  
   $total_point=($sum_fa_sa_total*100)/300;
                                 //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade_point = 0;
                                   if(($total_point>90) && ($total_point<=100))
                                   {
                                           $grade_point = 10;
                                           $gg ='A1' ;
                                   }
                                   if(($total_point>80) && ($total_point<=90))
                                   {
                                           $grade_point = 9;
                                           $gg ='A2' ;
                                   }
                                   if(($total_point>70) && ($total_point<=80))
                                   {
                                           $grade_point = 8;
                                           $gg ='B1' ;
                                   }
                                   if(($total_point>60) && ($total_point<=70))
                                   {
                                           $grade_point= 7;
                                           $gg ='B2' ;
                                   }
                                   if(($total_point>50) && ($total_point<=60))
                                   {
                                           $grade_point = 6;
                                           $gg ='C1' ;
                                   }
                                   if(($total_point>40) && ($total_point<=50))
                                   {
                                           $grade_point = 5;
                                           $gg ='C2' ;
                                   }
                                   if(($total_point>32) && ($total_point<=40))
                                   {
                                          $grade_point = 4;
                                          $gg ='D' ;
                                   }
                                   if(($total_point>20) && ($total_point<=32))
                                   {
                                           $grade_point = 3;
                                           $gg ='E1' ;
                                   }
                                    if(($total_point>=0) && ($total_point<=20))
                                   {
                                           $grade_point = 2;
                                           $gg ='E2' ;
                                   }
                                   
   
   
   
   
        echo'<td align="center">'.$grade_point.'</td>';    
     $cgpa=$grade_point;
      $total_cgpa=$total_cgpa+$cgpa;
     $total_per=$total_cgpa/9;
     $calculata_cgpa=round($total_per,2);
     
        //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade_point = 0;
                                   if(($total_per>9) && ($total_per<=10))
                                   {
                                           $grade_point = 10;
                                           $gg_cpga ='A1' ;
                                   }
                                   if(($total_per>8) && ($total_per<=9))
                                   {
                                           $grade_point = 9;
                                           $gg_cpga ='A2' ;
                                   }
                                   if(($total_per>7) && ($total_per<=8))
                                   {
                                           $grade_point = 8;
                                           $gg_cpga ='B1' ;
                                   }
                                   if(($total_per>6) && ($total_per<=7))
                                   {
                                           $grade_point= 7;
                                           $gg_cpga ='B2' ;
                                   }
                                   if(($total_per>5) && ($total_per<=6))
                                   {
                                           $grade_point = 6;
                                           $gg_cpga ='C1' ;
                                   }
                                   if(($total_per>4) && ($total_per<=5))
                                   {
                                           $grade_point = 5;
                                           $gg_cpga ='C2' ;
                                   }
                                   if(($total_per>3) && ($total_per<=4))
                                   {
                                          $grade_point = 4;
                                          $gg_cpga ='D' ;
                                   }
                                   if(($total_per>2) && ($total_per<=3))
                                   {
                                           $grade_point = 3;
                                           $gg_cpga ='E1' ;
                                   }
                                    if(($total_per>=0) && ($total_per<=2))
                                   {
                                           $grade_point = 2;
                                           $gg_cpga ='E2' ;
                                   }
                                   
   
     
     
}}

echo'   </tbody></table>';
    

echo'<table  class="table table-bordered table-striped" width="100%" valign="top" align="center" style="border:0px solid black;border-collapse:collapse;"><tbody>
 <tr align="center">
            <td align="left" style="font-size:20px" colspan="5"><b>Attendance</b> : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;90/90</td>
            <td colspan="" align="center" style="font-size:19px">&nbsp;&nbsp;&nbsp;&nbsp;210/210&nbsp;&nbsp;</td>
                    <td colspan="5" align="center" style="font-size:12px"><b>Eetremely regular student. keep it up !</b></td>
            <td style="font-size:18px;">&nbsp;&nbsp;&nbsp;&nbsp;CGPA&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="font-size:18px">&nbsp;&nbsp;&nbsp;'.$calculata_cgpa.'&nbsp;&nbsp;</td>
          
          
        </tr>
 <tr align="center" style="border:0px solid black;border-collapse:collapse;">
           
            <td colspan="11" align="left"><font style="font-size:11px;"><b>Note:A1=91-100%;A2=81-90%;B1=71-80%;B2=61-70%;C1=51-60%;C2=41-50%;D=33-40%;E1=21-32%;E2=0-20% AND BELOW</b></td>
            
           
            <td colspan="" style="font-size:14px"><b>Overall Grade</b></td>
            <td colspan="" style="font-size:20px"><b>'.$gg_cpga.'</b></td>
                 
        </tr></tbody></table>';


// to ptint the scholastic area************************************************************************
 echo'
<table  class="table table-bordered table-striped"  width="100%" style="border:0px solid black;border-collapse:collapse;" cellpadding="2">
<thead >                            
<tr><th colspan="3" align="center" width="40%" style="background-color:#CCCCCC;-webkit-print-color-adjust: exact" ><font style="font-size:16px;">Co-SCHOLASTIC AREA</font></th>
</tr>
<tr><th align="center" width=""><font style="font-size:17px;">ACTIVITY</font></th><th align="center" style="width:5%;"><font style="font-size:16px;">GRADE</th></font><th align="center" ><font style="font-size:17px;">INDICATOR NAME</font></th></tr>
  </thead>
<tbody>';
 $ctr=0;
 $coscho_area="SELECT * FROM cce_coscho_fields ";
$exe_coscho=mysql_query($coscho_area);
while($coscho_area_name=mysql_fetch_array($exe_coscho))
{   $coscho_name=$coscho_area_name[2];
       $coscho_id=$coscho_area_name[0];
       $cosho=$coscho_area_name['cosho_area_id'];
       $ctr++;
    echo'<tr><td width="28%" height="30"> <b size="4"style="font-size:16px">'.$coscho_name.'</b></font></td>';
    $get_grade="SELECT * FROM cce_coscho_indicator_marks_table WHERE student_id=$student_id  AND coscho_id=  $coscho_id ";
    $exe_grade= mysql_query($get_grade);
    while($fetch_grade=  mysql_fetch_array($exe_grade))
    {             $grade=$fetch_grade['grade'];
    
   $coscho_indicator_name="SELECT *
            FROM cce_coscho_indicator_table  WHERE coscho_id=$coscho_id  AND  grade='$grade '";
    $exe_indicator=mysql_query($coscho_indicator_name);
    while($fetch_indicator=  mysql_fetch_array($exe_indicator))
    {             $indicato_name=$fetch_indicator[2];

       echo'<td align="center" width="3%" ><b style="font-size:18px">'.$fetch_indicator['grade'].'</b></td><td width="70%"><b style="font-size:12px" >'.$name_student.'</b><font  style="font-size:16px">   '. $indicato_name.'</font></td></tr>';
    }
}}
echo'</tbody></table>';




// to print the bottom part signature and self awerness........................
echo'<table width="100%" height="" style="border:0px solid black;border-collapse:collapse;"><tbody>    
    <tr align="center" style="border:1px solid black; border-collapse:collapse;">
            <td colspan="13" style="background-color:#CCCCCC;-webkit-print-color-adjust: exact"><font  style="font-size:16px">
<b>SELF AWARENESS</b></font></td>
        </tr>
         <tr align="center" style="border:0px solid black;border-collapse:collapse;">
            <td colspan="2" celpadding="10%"><font size="4"style="font-size:17px" ><b>My Goals</b></td>
             <td colspan="11" ><font size="4" style="font-size:17px"><b>My Interests and Hobbies</b></td>
        </tr>
         <tr align="center" style="border:0px solid black;border-collapse:collapse;">
            <td colspan="2" align="left"><font size="3"  style="font-size:16px">I will become an electrical engineer some day.</td>
             <td colspan="11" align="left"><font size="3"  style="font-size:16px">My favourite hobby is playing snooker. I enjoy working on the computer</td>
        </tr>
        <tr align="center" style="border:0px;" style="border:0px solid black;border-collapse:collapse;">
         <td colspan="13" align="left" width="100%" ><font size="3"><b  style="font-size:18px" width="100%">CONGRATULATIONS !</b> <b  style="font-size:16px" > Your ward is promoted to </b><b  style="margin-left:20px; width:100%;"  style="font-size:20px"><font size="4">Class:</font></b><b  style="margin-left:40px; width:100%;"  style="font-size:22px"><font size="4">New Session beings on .......</b></td>

            </tbody></table>';

echo'<table  width="100%"  style="border:0px;" width=""></tbody>
       </tr>
       <tr align="center" height="110px;" >
         <td  valign="bottom"  width="50%"  style="border:0px;" style="font-size:25px" align="left"><b>DATE:</b></td>
           <td  valign="bottom"  width="50%"  style="border:0px;" style="font-size:25px" ><b style="margin-left:-460px; width:100%;">Class Teacher\'s signature</b></td>
           <td valign="bottom"  width="0%"  style="border:0px;" style="font-size:25px"><b style="margin-left:-150px; width:100%;">Principal\'s signature</b></td>
       </tr>
      
         ';
    echo' </tbody></table> ';
    

echo'</tbody></table>';
        $z++;    }
  echo '
   </tbody>
  </table>
<br></br><hr>';
        }

        elseif (($class_id==19)||($class_id==20)||($class_id==21)||($class_id==22))
            {        $class_id=$_GET['cid'];
            
            
            
             $get_class_name = 
	 "SELECT class_index.class_name,class.sId
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.classId = ".$_GET['cid']." AND class.session_id=".$_SESSION['current_session_id']."";
	 $exe_class_name=mysql_query($get_class_name);
	while( $fetch_cls_name=mysql_fetch_array($exe_class_name))
	{   
	 $class_name=$fetch_cls_name['class_name'];
	  $student_id=$fetch_cls_name['sId'];
//******************************* Query to get the marks and store in array format*******************************
$array_report = array();
$i=0;
// query to get the subject on the basis of class.
$get_subject = "SELECT DISTINCT subject_id FROM cce_subject_type_skills_id_table WHERE class_id=".$class_id."";
$exe_subject = mysql_query($get_subject);
while($fetch_subject= mysql_fetch_array($exe_subject))
{
   // query to get the subject name
   $get_subject_name = "SELECT `subject` FROM subject WHERE subject_id=".$fetch_subject['subject_id']."";
   $exe_subject_name = mysql_query($get_subject_name);
   $fetch_subject_name = mysql_fetch_array($exe_subject_name);
   $report[$i]['subject'] = $fetch_subject_name['subject'];
   
     //query to get the type_id
         $get_type_id = "SELECT DISTINCT type_id FROM cce_subject_type_skills_id_table
         WHERE cce_subject_type_skills_id_table.subject_id = ".$fetch_subject['subject_id']."
         AND cce_subject_type_skills_id_table.class_id = ".$class_id."";
   
        $type =array();
        
        $j=0;
        $exe_type_id = mysql_query($get_type_id);
        while($fetch_type_id = mysql_fetch_array($exe_type_id))
         {
            //query to get the type_id
            $get_type_name = "SELECT * 
            FROM cce_types_activity_table
            WHERE type_id = ".$fetch_type_id['type_id']."";
            $exe_type_name = mysql_query($get_type_name);
            $fetch_type_name =mysql_fetch_array($exe_type_name);
            
           // $type[$j]['type_name'] = $fetch_type['type_name'];

            // query to get the skill name
           $get_skill_id = "SELECT skill_id,sts_id FROM cce_subject_type_skills_id_table 
            WHERE type_id = ".$fetch_type_id['type_id']." 
            AND subject_id = ".$fetch_subject['subject_id']."
            AND class_id = ".$class_id."" ;
            
            $exe_skill_id = mysql_query($get_skill_id);
            while($fetch_skill_id = mysql_fetch_array($exe_skill_id))
            {
            $sts_id = $fetch_skill_id['sts_id'];
            $skill = array();
            $k = 0;
            //query to get the get the marks of first_term c.t-1
            $get_marks_t1_ct1 = "SELECT marks FROM cce_marks_scho_type_skills_table
             WHERE student_id = ".$student_id." AND sts_id = ".$sts_id." AND term_test_id = 7";
             $exe_marks_t1_ct1 = mysql_query($get_marks_t1_ct1);
             $fetch_markd_t1_ct1 = mysql_fetch_array($exe_marks_t1_ct1);
             $marks_t1_ct1 = $fetch_markd_t1_ct1['marks'];
             
             // query to get the get the marks of first_term c.t-2
             $get_marks_t1_ct2 = "SELECT marks FROM cce_marks_scho_type_skills_table
             WHERE student_id = ".$student_id." AND sts_id = ".$sts_id." AND term_test_id = 8";
             $exe_marks_t1_ct2 = mysql_query($get_marks_t1_ct2);
             $fetch_markd_t1_ct2 = mysql_fetch_array($exe_marks_t1_ct2);
             $marks_t1_ct2 = $fetch_markd_t1_ct2['marks'];
             
             // query to get the get the marks of term2 c.t-111
             $get_marks_t2_ct1 = "SELECT marks FROM cce_marks_scho_type_skills_table
             WHERE student_id = ".$student_id." AND sts_id = ".$sts_id." AND term_test_id = 9";
             $exe_marks_t2_ct1 = mysql_query($get_marks_t2_ct1);
             $fetch_markd_t2_ct1 = mysql_fetch_array($exe_marks_t2_ct1);
             $marks_t2_ct1 = $fetch_markd_t2_ct1['marks'];
             
             
              
             // query to get the get the marks of term2 c.t-4
             $get_marks_t2_ct2 = "SELECT marks FROM cce_marks_scho_type_skills_table
             WHERE student_id = ".$student_id." AND sts_id = ".$sts_id." AND term_test_id = 10";
             $exe_marks_t2_ct2 = mysql_query($get_marks_t2_ct2);
             $fetch_markd_t2_ct2 = mysql_fetch_array($exe_marks_t2_ct2);
             $marks_t2_ct2 = $fetch_markd_t2_ct2['marks'];
             
             
             $t11_total = $marks_t1_ct2 + $marks_t1_ct1;
             $t22_total = $marks_t2_ct2 + $marks_t2_ct1;
             
            
             $total = ($t11_total+$t22_total);
            // $grade_total = 0;
             $t1_marks='';
             $t2_marks='';
             $marks1_t1_ct1 = '';
             $marks1_t1_ct2 = '';
             $marks1_t2_ct1 = '';
             $marks1_t2_ct2 = '';
             $sub_total='';
             $grade = '';
             //$grade = calculate_grade_point($total); // function to calculate and return the grade 
            if(!($fetch_skill_id['skill_id']))
            {
               
                if($fetch_type_id['type_id'] == 1)  
                   {
                     $t1_total = ($t11_total);
                     $t2_total = ($t22_total);
                     $marks1_t1_ct1 = $marks_t1_ct1;
                     $marks1_t1_ct2 = $marks_t1_ct2;
                     $marks1_t2_ct1 = $marks_t2_ct1;
                     $marks1_t2_ct2 = $marks_t2_ct2;
                     $sub_total = $total;
                     $grade = calculate_total_grade($sub_total);
                   }
                   else
                   {
                     $t1_marks = (($t11_total*100)/10);
                     $t2_marks = (($t22_total*100)/10);
                     $total1 = (($total*100)/20);
                     $sub_total = calculate_total_grade($total1);
                     $t1_total = calculate_total_grade($t1_marks);
                     $t2_total= calculate_total_grade($t2_marks);
                     
                     $marks1_t1_ct1 = calculate_grade($marks_t1_ct1);
                     $marks1_t1_ct2 = calculate_grade($marks_t1_ct2);
                     $marks1_t2_ct1 = calculate_grade($marks_t2_ct1);
                     $marks1_t2_ct2 = calculate_grade($marks_t2_ct2);
                     
                     $grade = calculate_total_grade($total1);
                   }
                   
               $subject_name = $fetch_subject_name['subject'];
               $type_name = $fetch_type_name['type_name'];
               $skill_name = $fetch_skill_details['skill_name'];
               $array_report[$subject_name][$type_name][$type_name]['t1']['ct1']['marks'] = $marks1_t1_ct1;
               $array_report[$subject_name][$type_name][$type_name]['t1']['ct2']['marks'] = $marks1_t1_ct2;
               $array_report[$subject_name][$type_name][$type_name]['t1']['total'] = $t1_total;
               $array_report[$subject_name][$type_name][$type_name]['t2']['ct1']['marks'] = $marks1_t2_ct1;
               $array_report[$subject_name][$type_name][$type_name]['t2']['ct2']['marks'] = $marks1_t2_ct2;
               $array_report[$subject_name][$type_name][$type_name]['t2']['total'] = $t2_total;
               $array_report[$subject_name][$type_name][$type_name]['t1+t2']= $sub_total;
               $array_report[$subject_name][$type_name][$type_name]['grade'] = $grade;
               //$skill_name = "Written assessment";
               //$array_report[$subject_name][$type_name][$type_name][$marks_t1_ct1][$marks_t1_ct2][$total][$grade] = 0;
            }
            else
            {
               $t1_total = (($t11_total*100)/10);
               $t2_total = (($t22_total*100)/10);
               $total1 = (($total*100)/20);
               $sub_total = calculate_total_grade($total1);
            // query to get the skill details
            $get_skill_details ="SELECT * FROM cce_name_skills_table
            WHERE skill_id = ".$fetch_skill_id['skill_id']." ";
            $exe_skill_details = mysql_query($get_skill_details);
            while($fetch_skill_details = mysql_fetch_array($exe_skill_details))
            { 
               //$skill[$k]['skill'] = $fetch_skill_details['skill_name'];
               $subject_name = $fetch_subject_name['subject'];
               $type_name = $fetch_type_name['type_name'];
               $skill_name = $fetch_skill_details['skill_name'];
               $array_report[$subject_name][$type_name][$skill_name]['t1']['ct1']['marks'] = calculate_grade($marks_t1_ct1);
               $array_report[$subject_name][$type_name][$skill_name]['t1']['ct2']['marks'] = calculate_grade($marks_t1_ct2);
               $array_report[$subject_name][$type_name][$skill_name]['t1']['total'] = calculate_total_grade($t1_total);
               $array_report[$subject_name][$type_name][$skill_name]['t2']['ct1']['marks'] = calculate_grade($marks_t2_ct1);
               $array_report[$subject_name][$type_name][$skill_name]['t2']['ct2']['marks'] =calculate_grade($marks_t2_ct2);
               $array_report[$subject_name][$type_name][$skill_name]['t2']['total'] = calculate_total_grade($t2_total);
              // echo $t1_total; echo'<br>';
               //echo $t2_total; echo'<br>'; 
              
               $array_report[$subject_name][$type_name][$skill_name]['t1+t2']=  $sub_total;
               $array_report[$subject_name][$type_name][$skill_name]['grade'] = $sub_total;
               //$array_report[$subject_name][$type_name][$skill_name][$marks_t1_ct1][$marks_t1_ct2][$total][$grade] = 0;
              
               $k++;
            }
             //$type[$j]['skill_name'] = $skill;
             $j++;
            }
            }
         }
       //$report[$i]['type'] = $type;
    //print_r($report);
}
//print_r($array_report);
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['ct1']['marks']; echo'<br>';
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'];echo'<br>';
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['total'];echo'<br>';
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['ct1']['marks']; echo'<br>';
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'];echo'<br>';
// echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['total'];echo'<br>';
// echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['grade'];echo'<br>';
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1+t2'];

// query to part that is use to access the co-corricular activity parts from cce_co_scho_marks_table table of database on the basic of term_id, class_id and activity_id and skill_id.
 $co_curricular = array();
 
//query to get the activity
   $get_co_curricular_act = "SELECT id,co_activity_name FROM cce_co_scho_activity_table WHERE level=2";
   $exe_co_curricular_act = mysql_query($get_co_curricular_act);
   while($fetch_co_curricular_act = mysql_fetch_array($exe_co_curricular_act))
   {
      $activity = $fetch_co_curricular_act['co_activity_name'];
      //query to get the skill from cce_co_scho_area_table table
       $get_co_curricular_skill = "SELECT id,co_skill FROM cce_co_scho_area_table WHERE co_activity_id=".$fetch_co_curricular_act['id']." ";
       $exe_co_curricular_skill = mysql_query($get_co_curricular_skill);
       $num_rows = mysql_num_rows($exe_co_curricular_skill);
       if($num_rows>0)
       {
       while($fetch_co_curricular_skill = mysql_fetch_array($exe_co_curricular_skill))
             {
              $skill = $fetch_co_curricular_skill['co_skill'];
              //query to get the marks from cce_co_scho_marks_table table
               $get_co_curricular_marks = "SELECT grade FROM cce_co_scho_marks_table WHERE student_id = $student_id AND class_id = $class_id AND term_id = 2 AND activity_id = ".$fetch_co_curricular_act['id']." AND skill_id =".$fetch_co_curricular_skill['id']." ";
               
               $exe_co_curricular_marks = mysql_query($get_co_curricular_marks);
               $fetch_co_curricular_marks = mysql_fetch_array($exe_co_curricular_marks);
               $fetch_co_curricular_marks['grade'];
               $co_curricular[$activity][$skill]['g'] = $fetch_co_curricular_marks['grade'];
               
             }
       }
       else
       {
            //query to get the marks from cce_co_scho_marks_table table
            $get_co_curricular_marks0 = "SELECT grade FROM cce_co_scho_marks_table WHERE student_id = $student_id AND class_id = $class_id AND term_id = 2 AND activity_id = ".$fetch_co_curricular_act['id']." AND skill_id = 0 ";
            
             $exe_co_curricular_marks0 = mysql_query($get_co_curricular_marks0);
             $fetch_co_curricular_marks0 = mysql_fetch_array($exe_co_curricular_marks0);
             
             $co_curricular[$activity][$activity]['g'] = $fetch_co_curricular_marks0['grade'];
       }
       
       
   }
    }
  // print_r($co_curricular);

         ?>
<style>
table,th,td,tr
{
border:1px solid black;
font-size: 11px;
}
</style>
<?php 
     
       $get_class_name = 
	 "SELECT class_index.class_name,class.sId
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.classId = ".$_GET['cid']." AND class.session_id=".$_SESSION['current_session_id']."";
	 $exe_class_name=mysql_query($get_class_name);
	while( $fetch_cls_name=mysql_fetch_array($exe_class_name))
	{   
	 $class_name=$fetch_cls_name['class_name'];
	  $student_id=$fetch_cls_name['sId'];

   // To generate the term report of class-id 19 *************************************************************************************************************************************************************************************

echo '<p style="z-index:1; position:absolute;  left:85px"><font size="6%" ><strong>VIDYA BAL BHAWAN SR. SEC. SCHOOL </strong></font></p>';
    echo'
<table  width="100%" style="border:0px">
<tbody ><tr><td>
<table  width="100%" style="border:0px">
<tbody style="border:0px">
<tr style="border:0px">
<td align="left" width="25%" style="border:0px"></td>
</tr>
<tr style="border:0px">';echo'<td align="left" style="border:0px"position:absolute; top:0px; ><img src="images/logo/School.jpg" width=40% ></td>';
echo'<br><div ><td  valign="top" width="150%" style="margin-left:-2px; width:100%;border:0px">
<font size="4" style="margin-left:0px; width:100%;" style="font-size:20px"><br>
      Kondli Gharoli Road, Mayur vihar III, Delhi -110096 <br>Phone No.: 22627876,22626299,22621494  Fax-22617007 </font>
      <font size="4" style="margin-left:20px; width:100%;" style="font-size:20px"></font><br>
    <b style="margin-left:-30px; width:100%;"><font size="5">Record of Acadmic Performance(2014-2015)</b>  </font> </td>
</tr>
</tbody>
</table>';

$z=0; 
  $get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB,sId
FROM student_user
 WHERE sId=".$student_id."";
$exe_s_name=mysql_query($get_s_name);
while($fetch_s_name=mysql_fetch_array($exe_s_name))
  
  {
	  $sid=$fetch_s_name['sId'];
	/*$sid1[$z]=$sid;
	$i++;*/
	  $name_student=$fetch_s_name[0];
	

echo '
         
<div class="row-fluid">
<div class="widget  span12 clearfix">
<div class="widget-header">
<table  width="100%" id=8   valign="top" class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<tr width="30%" style="border:0px solid black;">
<td align="left" style="border:0px solid black;"><b style="font-size:12px">Student Name:  '.$name_student.'</b></font></td>
<td align="left" style="border:0px solid black;"><b style="font-size:12px">Class : '.$class_name.'       '.$section_name.' </b><b style="margin-left:10px; width:100%;" ><font size="2" style="font-size:12px">Admission No.: '.$fetch_s_name[3].'    </b></font></td>
<td align="left" style="border:0px solid black;"><b style="font-size:12px">Examination Roll.No:</b></td>
</tr>
<tr width="30%" style="border:0px solid black;">
<td align="left" style="border:0px solid black;"><b style="font-size:12px">Fathers Name: '.$fetch_s_name[1].'</b></font></td>
<td align="left"width="30" style="border:0px solid black;"> <b style="font-size:12px">Mothers Name:  '.$fetch_s_name[2].' </b></font></td> 
<td align="left" style="border:0px solid black;"><b style="font-size:12px" >D.O.B.- '.date("jS F Y",strtotime($fetch_s_name[4])).' <b></b></b></font></td>
</tr>
</tbody></table>  							
</div><!-- End widget-header -->	          							
<div class="widget-content" align="center">						
<!-- Table widget -->

<table  class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<thead >                                          
<tr">
<th rowspan="2" colspan="2" width="27%"><font size="3">SUBJECT</font></th> <th colspan="3" align="center">Term-I</th><th colspan="3" align="center" style="border:1px;">Term-II</th><th colspan="2" align="cener">Grand Total</font></th></tr>
<tr><th  align="center" style="border:1px solid black;">C.T.-I</font></th><th  align="center" style="border:1px solid black;">C.T.-II</font></th><th  align="center"  width="5%">TOTAL</font></th><th  align="center" style="border:1px solid black;">C.T.-III</font></th><th  align="center" style="border:1px solid black;">C.T.-IV</font></th><th  align="center"  width="5%">TOTAL</font></th><th  align="center" width="10%">TOTAL</font></th>
<th style="border:1px solid black;" width="8%"> Grade</font></th>										
 </tr>    
 
  </thead> 
  <tbody align="center" style="border:1px solid black;">
        
              
        <tr align="center">
              <td valign="left" width="10%"><b>ENGLISH</b></td> 
              <td valign="" width="10%" colspan="" align="left"><b  style="font-size:12px">Written Assessment</b></td> 
              <td width="8%" style="border:1px solid black;">'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['grade'].'</b></td
          </tr>
         
         <tr align="center">
         <td valign="" width="17%" rowspan="2" align="left"><b  style="font-size:14px">Reading Skill</b></td>
            <td valign="" width="17%" colspan="" align="left">Pronunciation</td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  colspan="" align="left">Fluency</td>
             <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Reading Skills']['Fluency']['grade'].'</b></td>
         </tr>
           
        <tr align="center">
            <td valign="" width="17%" style="font-size:12;" rowspan="4" align="left"><b  style="font-size:14px">Writing Skill</b></td> 
            <td valign="" width="17%" align="left">Creative writing</td>
            
             <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%" align="left">Hand Writing</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  align="left">Grammar</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['grammar']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['grammar']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['grammar']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['grammar']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['grammar']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['grammar']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['grammar']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Writing Skills']['grammar']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  align="left"  style="font-size:12px">Spelling</td>
             <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Writing Skills']['Spelling']['grade'].'</b></td>
         </tr>
      <tr align="center">
            <td valign="" width="17%" rowspan="2" align="left"><b  style="font-size:14px">Speaking Skill</b></td>
            <td valign="top" width="17%"  align="left">Conversation</td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['grade'].'</b></td>
        </tr>
       <tr align="center">
           <td valign="" width="17%"  align="left"> Recitation</td>
           <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['grade'].'</b></td>
        </tr>
       <tr align="center">
          <td valign="" width="17%" colspan="2" align="left" style="font-size:14px"><b>Dictation</b></td>
          <td width="8%">'.$array_report['ENGLISH']['Dictation']['Dictation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Dictation']['Dictation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Dictation']['Dictation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Dictation']['Dictation']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Dictation']['Dictation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Dictation']['Dictation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Dictation']['Dictation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Dictation']['Dictation']['grade'].'</b></td>
       </tr>
       
        <tr align="center">
              <td valign="center" width="10%"><b>HINDI</b></td> <td valign="left" width="10%" align="left"><b  style="font-size:12px">Written Assessment</b></td> 
                <td width="8%" style="border:1px solid black;">'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['HINDI']['Written Assesment']['Written Assesment']['grade'].'</b></td
          </tr>
         
          <tr align="center">
         <td valign="" width="17%" rowspan="2" align="left"><b  style="font-size:14px">Reading Skill</b></td>
            <td valign="" width="17%" colspan="" align="left">Pronunciation</td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Reading Skills']['Pronunciation']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  colspan="" align="left">Fluency</td>
             <td width="8%">'.$array_report['HINDI']['Reading Skills']['Fluency']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Fluency']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Fluency']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Fluency']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Fluency']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Fluency']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Fluency']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Reading Skills']['Fluency']['grade'].'</b></td>
         </tr>
           
        <tr align="center">
            <td valign="" width="17%" style="font-size:12;" rowspan="4" align="left"><b  style="font-size:14px">Writing Skill</b></td> 
            <td valign="" width="17%" align="left">Creative writing</td>
            
             <td width="8%">'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Writing Skills']['Creative Writing']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%" align="left">Hand Writing</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Hand writing']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Hand writing']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Hand writing']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Hand writing']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Hand writing']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Hand writing']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Hand writing']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Writing Skills']['Hand writing']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  align="left">Grammar</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['grammar']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['grammar']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['grammar']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['grammar']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['grammar']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['grammar']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['grammar']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Writing Skills']['grammar']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  align="left"  style="font-size:12px">Spelling</td>
             <td width="8%">'.$array_report['HINDI']['Writing Skills']['Spelling']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Spelling']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Spelling']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Spelling']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Spelling']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Spelling']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Spelling']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Writing Skills']['Spelling']['grade'].'</b></td>
         </tr>
      <tr align="center">
            <td valign="" width="17%" rowspan="2" align="left"><b  style="font-size:14px">Speaking Skill</b></td>
            <td valign="top" width="17%"  align="left">Conversation</td>
            <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Conversation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Conversation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Conversation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Conversation']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Conversation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Conversation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Conversation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Speaking Skills']['Conversation']['grade'].'</b></td>
        </tr>
       <tr align="center">
           <td valign="" width="17%"  align="left"> Recitation</td>
           <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Recitation']['t1']['ct1']['marks'].'</td>
           <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Recitation']['t1']['ct2']['marks'].'</td>
           <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Recitation']['t1']['total'].'</b></td>
           <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Recitation']['t2']['ct1']['marks'].'</td>
           <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Recitation']['t2']['ct2']['marks'].'</b></td>
          <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Recitation']['t2']['total'].'</b></td>
           <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Recitation']['t1+t2'].'</b></td>
           <td><b>'.$array_report['HINDI']['Speaking Skills']['Recitation']['grade'].'</b></td>
        </tr>
       <tr align="center">
          <td valign="" width="17%" colspan="2" align="left" style="font-size:14px"><b>Dictation</b></td>
          <td width="8%">'.$array_report['HINDI']['Dictation']['Dictation']['t1']['ct1']['marks'].'</td>
          <td width="8%">'.$array_report['HINDI']['Dictation']['Dictation']['t1']['ct2']['marks'].'</td>
          <td width="8%"><b>'.$array_report['HINDI']['Dictation']['Dictation']['t1']['total'].'</b></td>
          <td width="8%">'.$array_report['HINDI']['Dictation']['Dictation']['t2']['ct1']['marks'].'</td>
          <td width="8%">'.$array_report['HINDI']['Dictation']['Dictation']['t2']['ct2']['marks'].'</b></td>
          <td width="8%"><b>'.$array_report['HINDI']['Dictation']['Dictation']['t2']['total'].'</b></td>
          <td width="8%"><b>'.$array_report['HINDI']['Dictation']['Dictation']['t1+t2'].'</b></td>
          <td><b>'.$array_report['HINDI']['Dictation']['Dictation']['grade'].'</b></td>
       </tr>
     
  <tr align="center">
          <td valign="center" width="10%"><b>MATHS</b></td> 
          <td valign="top" width="17%" align="left"><b  style="font-size:12px">Written Assessment</b></td>
          <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
          <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
          <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['grade'].'</b></td>
   </tr>
  <tr align="center">
          <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Concept</b></td>
          <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Concept']['Concept']['t1']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Concept']['Concept']['t1']['ct2']['marks'].'</td>
         <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Concept']['Concept']['t1']['total'].'</b></td>
         <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Concept']['Concept']['t2']['ct1']['marks'].'</td>
         <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Concept']['Concept']['t2']['ct2']['marks'].'</b></td>
         <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Concept']['Concept']['t2']['total'].'</b></td>
         <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Concept']['Concept']['t1+t2'].'</b></td>
        <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Concept']['Concept']['grade'].'</b></td>
     </tr>
   <tr align="center">
       <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Tables</b></td>
       <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Concept']['Concept']['t1']['ct1']['marks'].'</td>
       <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Tables']['Tables']['t1']['ct2']['marks'].'</td>
      <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Tables']['Tables']['t1']['total'].'</b></td>
     <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Tables']['Tables']['t2']['ct1']['marks'].'</td>
     <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Tables']['Tables']['t2']['ct2']['marks'].'</b></td>
     <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Tables']['Tables']['t2']['total'].'</b></td>
     <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Tables']['Tables']['t1+t2'].'</b></td>
    <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Tables']['Tables']['grade'].'</b></td>
    </tr>
   <tr align="center">
        <td width="17%" colspan="2" align="left" style="font-size:14px"><b>Mental Ability</b></td>
        <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t1']['ct1']['marks'].'</td>
       <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t1']['ct2']['marks'].'</td>
       <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t1']['total'].'</b></td>
       <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Mental Ability']['Mental Ability']['grade'].'</b></td>
     </tr>
  <tr align="center">
           <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Activity</b></td>
           <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Activity']['Activity']['t1']['ct1']['marks'].'</td>
           <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Activity']['Activity']['t1']['ct2']['marks'].'</td>
           <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Activity']['Activity']['t1']['total'].'</b></td>
           <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Activity']['Activity']['t2']['ct1']['marks'].'</td>
           <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Activity']['Activity']['t2']['ct2']['marks'].'</b></td>
           <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Activity']['Activity']['t2']['total'].'</b></td>
           <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Activity']['Activity']['t1+t2'].'</b></td>
          <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Activity']['Activity']['grade'].'</b></td>
 </tr>  
            

<tr align="center">
       
         <td width="10%"><b>SCIENCE</b></td>
         <td valign="top" width="17%" align="left"><b style="font-size:12px">Written Assessment</b></td> 
               <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['grade'].'</b></td
</tr>
<tr align="center">
                 <td valign="top" width="17%" colspan="2" align="left"  style="font-size:14px"><b>Group Discussion</b></td> 
             <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['grade'].'</b></td>
</tr>
<tr align="center">
           <td valign="top" width="17%" colspan="2" align="left"  style="font-size:14px"><b>Activity</b></td>
                             
            <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Activity']['Activity']['t1']['ct1']['marks'].'</td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Activity']['Activity']['t1']['ct2']['marks'].'</td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Activity']['Activity']['t1']['total'].'</b></td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Activity']['Activity']['t2']['ct1']['marks'].'</td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Activity']['Activity']['t2']['ct2']['marks'].'</b></td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Activity']['Activity']['t2']['total'].'</b></td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Activity']['Activity']['t1+t2'].'</b></td>
            <td style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Activity']['Activity']['grade'].'</b></td>
</tr>
     <tr align="center">
        <td valign="top" width="17%" colspan="2" align="left"  style="font-size:14px"><b>Project</b>
              <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Project']['Project']['t1']['ct1']['marks'].'</td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Project']['Project']['t1']['ct2']['marks'].'</td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Project']['Project']['t1']['total'].'</b></td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Project']['Project']['t2']['ct1']['marks'].'</td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Project']['Project']['t2']['ct2']['marks'].'</b></td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Project']['Project']['t2']['total'].'</b></td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Project']['Project']['t1+t2'].'</b></td>
            <td style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Project']['Project']['grade'].'</b></td>
     </tr>  
       <tr align="center">
        <td valign="center" width="10%"><b>SST</b></td> <td valign="top" width="17%" align="left" style="font-size:12px"><b>Written Assessment<b></td> 
         <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
          <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
          <td style="border:1px solid black;"><b>'.$array_report['SST']['Written Assesment']['Written Assesment']['grade'].'</b></td>
   </tr>
 <tr align="center">
                  <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Environmental Sensitivity</b></td> 
                <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['t1']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['t1']['ct2']['marks'].'</td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['t1']['total'].'</b></td>
          <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['t2']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['t2']['ct2']['marks'].'</b></td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['t2']['total'].'</b></td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['t1+t2'].'</b></td>
          <td style="border:1px solid black;"><b>'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['grade'].'</b></td>
      </tr>
                
 <tr align="center">
         <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Group discussion</b></td>
             <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Group Discussion']['Group Discussion']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Group Discussion']['Group Discussion']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Group Discussion']['Group Discussion']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Group Discussion']['Group Discussion']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Group Discussion']['Group Discussion']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Group Discussion']['Group Discussion']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Group Discussion']['Group Discussion']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['SST']['Group Discussion']['Group Discussion']['grade'].'</b></td>
       </tr>
                  
   <tr align="center">
                <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Activity</b></td> 
                <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Activity']['Activity']['t1']['ct1']['marks'].'</td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Activity']['Activity']['t1']['ct2']['marks'].'</td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Activity']['Activity']['t1']['total'].'</b></td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Activity']['Activity']['t2']['ct1']['marks'].'</td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Activity']['Activity']['t2']['ct2']['marks'].'</b></td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Activity']['Activity']['t2']['total'].'</b></td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Activity']['Activity']['t1+t2'].'</b></td>
            <td style="border:1px solid black;"><b>'.$array_report['SST']['Activity']['Activity']['grade'].'</b></td>
       </tr>                       


  <tr align="center">
         <td valign="center" width="10%"><b>COMPUTER</b></td> 
         <td valign="top" width="17%" align="left"><b  style="font-size:12px">Written Assessment</b></td> 
          <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['grade'].'</b></td
   </tr>
                
  <tr align="center">
              <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Practical</b></td> 
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Practical']['Practical']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Practical']['Practical']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Practical']['Practical']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Practical']['Practical']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Practical']['Practical']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Practical']['Practical']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Practical']['Practical']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Practical']['Practical']['grade'].'</b></td>
</tr>
   <tr align="center">
            <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Viva</b></td> 
           <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Viva']['Viva']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Viva']['Viva']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Viva']['Viva']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Viva']['Viva']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Viva']['Viva']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Viva']['Viva']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Viva']['Viva']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Viva']['Viva']['grade'].'</b></td>
    </tr>
   <tr align="center">
            <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Project</b></td> 
           <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Project']['Project']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Project']['Project']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Project']['Project']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Project']['Project']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Project']['Project']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Project']['Project']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Project']['Project']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Project']['Project']['grade'].'</b></td>
   </tr>  
               

   <tr align="center">
        <td valign="center" width="10%"><b>G.K</b></td> <td valign="top" width="17%" align="left"><b  style="font-size:12px">Written Assessment<b></td> 
          <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['GK']['Written Assesment']['Written Assesment']['grade'].'</b></td
   </tr>
<tr align="center">
                  <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Current Knowledge</b></td>
                  <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t2']['ct2']['marks'].'</b></td>
               <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['GK']['Current Knowledge']['Current Knowledge']['grade'].'</b></td>
</tr>
        

';
echo'<tr  width="100%">
<td align="left"  colspan="10"><b  style="font-size:18px"><font size="3">Attendance:</font></b><b style="margin-left:160px; width:100%;"  style="font-size:18px"><font size="3">Blood Group:</b> '.$blood.'<b style="margin-left:100px; width:100%;" style="font-size:18px"><font size="3">Height:</b><b style="margin-left:100px; width:100%;" style="font-size:18px"><font size="3">Weight:</b></td> 

</tr>
</tbody></table>
';                      
      $get_subject = "
      SELECT DISTINCT subject.subject,subject.subject_id,scho_activity_table.*,types_of_activities.*
      FROM subject
      INNER JOIN scho_marks
      ON subject.subject_id =scho_marks.subject_id
      INNER JOIN scho_activity_table
      ON scho_activity_table.id =scho_marks.scho_act_id
      INNER JOIN types_of_activities
      ON types_of_activities.Id=scho_activity_table.type_id
      INNER JOIN class
      ON class.sId=scho_marks.student_id
      WHERE scho_marks.student_id=".$_GET['student_id']." ";

        echo'<table class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
        <thead>
              <tr style="border:1px solid black;">
                <th align="center" colspan="6" style="border:1px solid black;"><b  style="font-size:14px">Co-Curricular Activities</b></th><th align="center" style="border:1px solid black;" colspan="4"><b  style="font-size:14px">Personality  Development</b></th></tr>
              <tr> <th align="center"  style="border:1px solid black;" colspan="2"><b> Activities</b></th>
                <th align="center" style="border:1px solid black;"><b>GRADE</b></font></th>
                <th align="center"  style="border:1px solid black;" colspan="2"><b>Activities</b></th>
                <th align="center" style="border:1px solid black;"><b>GRADE</b></font></th>
                <th align="center" style="border:1px solid black;" ><b>Activities</b></th>
                 <th align="center"><b>GRADE</b></th>
                 <th align="center" style="border:1px solid black;" ><b>Activities</b></th>
                 <th align="center"><b>GRADE</b></th>
                </tr>  </thead>';
      
     echo'<tr >
       <th rowspan="3" align="center" width="10%">GAMES<td>Team Spirit</td>
       <td align="center"><b>'.$co_curricular['GAMES']['Team spirit']['g'].'</b></td>
       <th rowspan="3" align="left" width="10%">ART & CRAFT<td>Neatness</td><td align="center"><b>'.$co_curricular['ART & CRAFT']['Neatness']['g'].'</b></td>
       <td>Attitude</td></td><td align="center"><b>'.$co_curricular['Attitude']['Attitude']['g'].'</b></td>
       <td>Confidence</td><td align="center"><b>'.$co_curricular['Confidence']['Confidence']['g'].'</b></td>
       </tr>
       <tr>
       <td>Discipline</td><td align="center"><b>'.$co_curricular['GAMES']['Discipline']['g'].'</b></td>
       <td>Colouring</td><td align="center"><b>'.$co_curricular['ART & CRAFT']['Coloring']['g'].'</b></td>
       <td>Care of Belongings</td><td align="center"><b>'.$co_curricular['Care of belongings']['Care of belongings']['g'].'</b></td>
       <td>Discipline</td><td align="center"><b>'.$co_curricular['Discipline']['Discipline']['g'].'</b></td></th>
       <tr>
         <td>Enthusiasm</td>
         <td align="center"><b>'.$co_curricular['GAMES']['Enthusiasm']['g'].'</b></td>
         <td>Book Work</td><td align="center"><b>'.$co_curricular['ART & CRAFT']['Book Work']['g'].'</b></td>
         <td>Regularity & Punctuality</td><td align="center"><b>'.$co_curricular['Regularity & Punctuality']['Regularity & Punctuality']['g'].'</b></td><td>Respect for School Property</td><td align="center"><b>'.$co_curricular['Respect for school property']['Respect for school property']['g'].'</b></td></th>
       </tr>
       <th align="center">MUSIC & DANCE</th><td>Rhythm</td> <td align="center"><b>'.$co_curricular['MUSIC & DANCE']['Rhythm']['g'].'</b></td>
       <td colspan="2">Interest</td><td align="center"><b>'.$co_curricular['MUSIC & DANCE']['INTEREST']['g'].'</b></td>
       <td>Sharing & Caring</td><td align="center"><b>'.$co_curricular['Sharing & Caring']['Sharing & Caring']['g'].'</b></td>
       <td>Imaginative Skills</td><td align="center"><b>'.$co_curricular['Imaginative skills']['Imaginative skills']['g'].'</b></td></tr></tr>';
    echo'  <table width="100%" align="left" id="77" height=40 style="border:0px"><tbody>
<td  style="float:left;border:0px" valign="bottom"><b><font size="2"><b >Parent\'s signature</b><font size="2"><b  style="margin-left:140px; width:100%;">Class Teacher\'s signature</font></b><b  style="margin-left:160px; width:100%;"><font size="2">Principal\'s signature</b></font></td>
</tbody></table>';
    
echo'</tbody></table>';
        $z++;    }}
  echo '
   </tbody>
  </table>
<br></br>';
	
            }
 }
	
	

//priv:teacher show report
function show_class_teacher()
{
	
	$priv = $_SESSION['priv'];

echo '  <div class="row-fluid">
                    
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>Choose Class</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<ol class="rounded-list" ';
								  if($priv == 2)
                                    {
	                                  $tid=$_SESSION['user_id'];
									  //get classes on the id of the teacher
									  $get_classes_tea=
									  "SELECT teachers.class , class_index.cId
									  FROM teachers
									  INNER JOIN class_index
									  ON class_index.class_name = teachers.class
									  WHERE teachers.tId = ".$tid."   ORDER BY  class_index.class+0 ASC";
									  $exe_classes=mysql_query($get_classes_tea);
									  while($fetch_classes_teacher=mysql_fetch_array($exe_classes))
									  {
				echo '<li><a href="grade_cce_select_stu_teacher.php?cid='.$fetch_classes_teacher[1].'">'.$fetch_classes_teacher[0].'</a></li>'
								;  
										  
									  }
	
                                  }
								  else
								  {
								$get_classes=
								"SELECT *
								FROM class_index";
								$exe_get_classes=mysql_query($get_classes);
								while($fetch_classes=mysql_fetch_array($exe_get_classes))
								{
								echo '<li><a href="grade_cce_select_stu_teacher.php?cid='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>'
								;	
									
								}
								  }
								echo '
								</ol>
								</div>
								</div>
								</div>';
									
	
}
//show student details
function show_stu_teacher()
{
//it is the class id which comes from previous page.
 $cid=$_GET['cid'];
	echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	
	<div class="widget-header">
	<span><i class="icon-home"></i>Select Student </span>
	</div><!-- End widget-header -->	
	
	<div class="widget-content">
	';
	echo '<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th>Admission No.</th>
	<th>Student Names</th>
	
	</tr>
	</thead>
	<tbody align="center">
	';
	
	$get_students = "
	SELECT student_user.Name , student_user.admission_no , student_user.sId
	FROM student_user
	INNER JOIN class
	ON class.sId = student_user.sId
	WHERE class.classId = ".$cid." AND class.session_id=".$_SESSION['current_session_id']."
	";
	$exe_students = mysql_query($get_students);
	while($fetch_students = mysql_fetch_array($exe_students))
	{
	echo'
	<tr>
	<td>'.$fetch_students['admission_no'].'</td>
	<td><a href="grade_cce_scholastic_report_details.php?sid='.$fetch_students['sId'].'&cid='.$cid.'">'.$fetch_students['Name'].'</a></td>
	</tr>	
	'; 
	}
	echo '</tbody></table>
	</div>';
	
	echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';								
}

//show retest details
function  retest_teacher_class_details()
{

	$priv = $_SESSION['priv'];

echo '  <div class="row-fluid">
                    
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>Choose Class</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<ol class="rounded-list" ';
								  if($priv == 2)
                                    {
	                                  $tid=$_SESSION['user_id'];
									  //get classes on the id of the teacher
									  $get_classes_tea=
									  "SELECT teachers.class , class_index.cId
									  FROM teachers
									  INNER JOIN class_index
									  ON class_index.class_name = teachers.class
									  WHERE teachers.tId = ".$tid."   ORDER BY  class_index.class+0 ASC";
									  $exe_classes=mysql_query($get_classes_tea);
									  while($fetch_classes_teacher=mysql_fetch_array($exe_classes))
									  {
				echo '<li><a href="grade_cce_retest_student.php?cid='.$fetch_classes_teacher[1].'">'.$fetch_classes_teacher[0].'</a></li>'
								;  
										  
									  }}
		
	echo '
								</ol>
								</div>
								';
									
	echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';			
}


function retest_report_teacher_class_details()
{
	
$priv = $_SESSION['priv'];

echo '  <div class="row-fluid">
                    
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>Choose Class</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<ol class="rounded-list" ';
								  if($priv == 2)
                                    {
	                                  $tid=$_SESSION['user_id'];
									  //get classes on the id of the teacher
									  $get_classes_tea=
									  "SELECT teachers.class , class_index.cId
									  FROM teachers
									  INNER JOIN class_index
									  ON class_index.class_name = teachers.class
									  WHERE teachers.tId = ".$tid."  ORDER BY  class_index.class+0 ASC";
									  $exe_classes=mysql_query($get_classes_tea);
									  while($fetch_classes_teacher=mysql_fetch_array($exe_classes))
									  {
				echo '<li><a href="grade_cce_retest_view_student.php?cid='.$fetch_classes_teacher[1].'">'.$fetch_classes_teacher[0].'</a></li>'
								;  
										  
									  }}
		
	echo '
								</ol>
								</div>
								';
									
	echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';		

}

function show_final_report_teacher()
{
	
$priv = $_SESSION['priv'];

echo '  <div class="row-fluid">
                    
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>Choose Class</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<ol class="rounded-list" ';
	  if($priv == 2)
                                    {
	                                  $tid=$_SESSION['user_id'];
                                                            //get classes on the id of the teacher
                                                            $get_classes_tea=
                                                            "SELECT teachers.class , class_index.cId
                                                            FROM teachers
                                                            INNER JOIN class_index
                                                            ON class_index.class_name = teachers.class
                                                            WHERE teachers.tId = ".$tid."";
                                                            $exe_classes=mysql_query($get_classes_tea);
                                                            while($fetch_classes_teacher=mysql_fetch_array($exe_classes))
                                                            {
                  echo '<li><a href="show_students.php?cid='.$fetch_classes_teacher[1].'">'.$fetch_classes_teacher[0].'</a></li>'
                                                  ;  

                                                            }}

	echo '
                                                        </ol>
                                                        </div>
                                                        ';
									
	echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';			
}



function show_all_report_student_teacher()
{
$priv = $_SESSION['priv'];

echo '  <div class="row-fluid">
                    
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>Choose Class</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<ol class="rounded-list" ';
								  if($priv == 2)
                                    {
	                                  $tid=$_SESSION['user_id'];
									  //get classes on the id of the teacher
									  $get_classes_tea=
									  "SELECT teachers.class , class_index.cId
									  FROM teachers
									  INNER JOIN class_index
									  ON class_index.class_name = teachers.class
									  WHERE teachers.tId = ".$tid."";
									  $exe_classes=mysql_query($get_classes_tea);
									  while($fetch_classes_teacher=mysql_fetch_array($exe_classes))
									  {
				echo '<li><a href="grade_cce_view_report_all_student.php?cid='.$fetch_classes_teacher[1].'">'.$fetch_classes_teacher[0].'</a></li>'
								;  
										  
									  }}
		
	                        echo '
								</ol>
								</div>
								';
									
	  echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';					
}



///for nuraery..........grade cce..........................................................................................................................................................................................................................................................////////////////////////////////////////////////////////////


//add activity
function add_nursery_activity_cce()
{
    
$priv = $_SESSION['priv'];

  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Activity Details</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';  

if($priv != 99)
{
  echo' 	                   
       <a class="uibutton btn-btn confirm" type="button" href="grade_cce_add_activity_details_nursery.php" align="center">Add Activity</a>
       <a class="uibutton denger" href="grade_cce_assign_subject_activity.php">Assign to subject</a>    ';
}
//          echo'<a class="uibutton btn-btn confirm" type="button" href="grade_cce_add_activity_indicator_details_nursery.php" align="center">Add Indicator</a>
//                  
//       <a class="uibutton denger" href="grade_cce_add_marks_activity_classwise.php">Classwise Add Marks</a>   
//       ';
    echo'      <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          WHERE level <=1
          ORDER BY class ASC
         ";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grade_cce_add_marks_nur_to_pre_students.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }
          echo '

       </div>
          </div>
          </div>         ';	
}

function  grade_cce_add_marks_nur_pre_student()
{
    
     echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Activity Details</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';  
     
     $id_class=$_GET['id_class'];
     
  echo' 	                   
       <a class="uibutton btn-btn " type="button" href="grade_cce_add_marks_nursery_assesment.php?id_class='.$id_class.'" align="center">Add Marks Scholastic Area</a>
     
       ';
//          echo'<a class="uibutton btn-btn confirm" type="button" href="grade_cce_add_activity_indicator_details_nursery.php" align="center">Add Indicator</a>
//                    <a class="uibutton denger" href="#">Add Marks Co-Scholastic Area</a>
//       <a class="uibutton denger" href="grade_cce_add_marks_activity_classwise.php">Classwise Add Marks</a>   
//       ';
    
//    echo' 	                   
//   <a class="uibutton btn-btn confirm" align="center"  type="button" href="grades_cce/grade_cce_final_report_activity_details_nursery.php?student_id='.$student_id.'&&class_id='.$class_id.'" >View Report</a>         '
//            . '                    ';

    
     echo '

       </div>
          </div>
          </div>         ';
    
}

function   grade_cce_add_marks_nur_pre_assesment()
{
    
     echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Activity Details</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';  
    $class_id=$_GET['id_class'];
    
    
   echo'  <form  action="grade_cce_view_nursery_student_details.php" method="get"><div class="section "> <label>Select Term</label>   
         <div> 
          <select  data-placeholder="Choose  Term..." class="chzn-select" tabindex="2" name="term">
        <option value=""></option>  ';
                        
$get_term="SELECT *
               FROM  `cce_term_table` 
               
                   ";
$execute_get_term=mysql_query($get_term);
while($fetch_term=mysql_fetch_array($execute_get_term))//looping for getting year
{   
         $term_name=$fetch_term['term_name'];

echo'
     <option value="'.$fetch_term[0].'">'. $term_name.'</option>';
  }
 echo'    
</select>
</div>
</div>';
  $get_cycle_test="SELECT cce_test_table.*
               FROM  `cce_test_table` 
               INNER JOIN  cce_term_test_table
               ON cce_term_test_table.test_id=cce_test_table.test_id
               WHERE cce_term_test_table.class_id=$class_id
                   ";
  echo'<div class="section "> <label>Select Cycle Test</label>   
         <div> 
          <select  data-placeholder="Choose  cycle Test..." class="chzn-select" tabindex="2" name="test">
        <option value=""></option>  ';
                        

$execute_get_cycle_test=mysql_query($get_cycle_test);
while($fetch_cycle_test=mysql_fetch_array($execute_get_cycle_test))//looping for getting year
{   
         $cycle_name=$fetch_cycle_test['test_name'];

echo'
     <option value="'.$fetch_cycle_test['test_id'].'">'.$cycle_name.'</option>';
  }
 echo'    
</select>
</div>
</div>';  
 echo'    <input type="hidden" name="id_class"  value="'.$class_id.'">
</div><div class="section last">
<div>
<button type ="submit"  class="uibutton submit">Submit</button>  
</div>
</div></form>';
   
   
    echo '

       </div>
          </div>
          </div>         ';
    
}








function grade_cce_add_activity_indicator()
{
       $current_session=$_SESSION['current_session_id'];
      echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Add Activity</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';     
          if(isset($_GET['added']))
            {
	echo "<h3 style=\"color:green\" align=\"center\">Successfully Added</h3>" ;
            }

echo '
                        <div class="section">	
                        <form id="validation_demo" method="post" action="grades_cce/add_grade_cce_activity_indicator_name.php"> 
                        <div class="section ">
                        <label>Level</label>   
                        <div> 
                        <select  name="level" data-placeholder="Choose  LEVEL..." class="chzn-select" tabindex="2" >
                        <option value=""></option>
                        <option value="0">Nursery</option>
                        <option value="1">Pre Primary</option>
                       <option value="2"> Primary</option>
                       <option value="3">Secondary</option>
                       </select>
                        </div>
                        </div>
	  
                        <div class="section "><label>Select Activity</label>   
                <div> 
          <select  data-placeholder="Choose  Activity..." class="chzn-select" tabindex="2" name="activity">
        <option value=""></option>  ';
                        
$get_visitor_year="SELECT *
               FROM  `grade_cce_activity_name`
               WHERE session_id=".$_SESSION['current_session_id']."
               ORDER BY activity_name  ASC
                   ";
$execute_visitor_year=mysql_query($get_visitor_year);
while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
{   
         $subject=$fetch_visitor_year[1];

echo'
     <option value="'.$fetch_visitor_year[0].'">'. $subject.'</option>';
  }
 echo'    
</select>
</div>
</div>
   <input type="hidden" name="session_id" value="'.$current_session.'"/>
                        <div class="section ">
	  <label>Indicator Name</label>   
	  <div> 
	  <input type="text" class="validate[required] mediam" autocomplete="off" name="indicator_name" id="f_required" >
	  </div> 
	  </div>
                         <div class="section ">
	  <label>Grade</label>   
	  <div> 
	  <input type="text" class="mediam" autocomplete="off" name="grade"  >
	  </div> 
	  </div>
                           <div class="section last">
	  <div>
	  <a class="uibutton submit_form  icon add " > Add Indicator</a>
                         <a href="#" class="uibutton btn-btn confirm" >View Activity</a>
	  </div>
	  </div>                         
               ';
         
         echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';	
           
    
    
    
    
    
    
    
    
    
    
    
    
    
}

//add activity
     function  add_activity_nursery()
     {
     $current_session=$_SESSION['current_session_id'];
      echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Add Activity</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';     
          if(isset($_GET['added']))
            {
	echo "<h3 style=\"color:green\" align=\"center\">Successfully Added</h3>" ;
            }

echo '
                        <div class="section">	
                        <form id="validation_demo" method="post" action="grades_cce/grade_cce_activity_name.php"> 
                        <div class="section ">
                        <label>Level</label>   
                        <div> 
                        <select name="level">
                        <option value="">---SELECT LEVEL---</option>
                        <option value="0">Nursery</option>
                        <option value="1">Pre Primary</option>
                       <option value="2"> Primary</option>
                       <option value="3">Secondary</option>
                       </select>
                        </div>
                        </div>
	  <div class="section ">
	  <label> Activity Name</label>   
	  <div> 
	  <input type="text" class="validate[required] small" autocomplete="off" name="activity" id="f_required" >
	  </div> 
	  </div>
                        <div class="section last">
	  <div>
	  <a class="uibutton submit_form  icon add " > Add  Activity</a>
                         <a href="grade_cce_view_activity_details.php" class="uibutton btn-btn confirm" >View Activity</a>
	  </div>
	  </div>          
                       <input type="hidden" name="session_id" value="'.$current_session.'"/>

               ';
         
         echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';	
         
         
         
         
     }
     //view activity fo nursery pp     
  function view_activity_details()
          {
              
       $current_session=$_SESSION['current_session_id'];
      echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>View Activity</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';      
             
              echo '
                        <div class="section">	
                        <form id="validation_demo"> 
                        <div class="section ">
                        <label>Level</label>   
                        <div> 
                        <select name="level"   onchange="show_activity_grade_cce(this.value);">
                        <option value="">---SELECT LEVEL---</option>
                        <option value="0">Nursery</option>
                        <option value="1">Pre Primary</option>
                       <option value="2"> Primary</option>
                       <option value="3">Secondary</option>
                       </select>
                        </div>
                        </div>
	  </form>
                              ';
              
               echo'  <span id="type_act_cce">	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';	
         
              
              
          }
          
          
          
          
 function  grade_cce_view_nursery_students()
 {
              
                $current_session=$_SESSION['current_session_id'];     
       //it is the class id which comes from previous page.
 $cid=$_GET['id_class'];
 $term_id=$_GET['term'];
  $test_id=$_GET['test'];
 
 
echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>view students </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
			';

                   //query to get class details
                    $q="SELECT `cId`,`class` ,`section`  FROM class_index
                               WHERE `cId`=". $cid." ";
                        $q_res=mysql_query($q);
                         $res=mysql_fetch_array($q_res);
                         $class=$res['class'];
                          $section=$res['section'];

                    echo'  <h5  style="color:green"align="center">Class:   '.$class.'   <br>    Section:     '.$section.'</h5>';
                    echo '<table  class="table table-bordered table-striped" id="dataTable" >
                    <thead>
                    <tr>
                     <th>Admission No.</th>
                     <th>Student Names</th>
                  </tr>
                    </thead>
                    <tbody align="center">
                    ';

                    $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$_GET['id_class']."'
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td><a href="grade_cce_nursery_add_marks_activity.php?student_id='.$fetch_students['sId'].'&class_id='.$cid.'&term_id='.$term_id.'&test_id='.$test_id.'">'.$fetch_students['Name'].'</a></td>
                </tr>	
                    '; 
                     }



echo '</tbody></table>
</div></div></div>';                    
          }
 
          //assign subject to activity
 function  nursery_assign_activity_subject_cce()
  {
          $current_session=$_SESSION['current_session_id'];
           $n=0;   
         echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">                    
<div class="widget-header">
 <span><i class="icon-home"></i>view students </span>
</div><!-- End widget-header -->	
<div class="widget-content">';   
         if(isset($_GET['assign']))
            {
	echo "<h4 style=\"color:green\" align=\"center\">Successfully Assign</h4>" ;
            }
    //query to get activity name from grade activity table     
 $get_activy="SELECT *
                              FROM  grade_cce_activity_name
                              ";
$execute_get_activy=mysql_query($get_activy);	                
echo '
          <div class="section">	
          <form id="validation_demo" method="post" action="grades_cce/grade_cce_assign_activity_name.php"> 
          <div class="section ">
                        <label>Level</label>   
                        <div> 
                        <select   data-placeholder="Choose  subject..." name="level">
                        <option value="">---SELECT LEVEL---</option>
                        <option value="0">Nursery</option>
                        <option value="1">Pre Primary</option>
                       <option value="2"> Primary</option>
                       <option value="3">Secondary</option>
                       </select>
                        </div>
                        </div>
<div class="section "> <label>Select Subject</label>   
    <div> 
          <select  data-placeholder="Choose  subject..." class="chzn-select" tabindex="2" name="subject">
        <option value=""></option>  ';
                        
$get_visitor_year="SELECT *
               FROM  `subject`
               ORDER BY subject ASC
                   ";
$execute_visitor_year=mysql_query($get_visitor_year);
while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
{   
         $subject=$fetch_visitor_year['subject'];

echo'
     <option value="'.$fetch_visitor_year[0].'">'. $subject.'</option>';
  }
 echo'    
</select>
</div>
</div>
<div class="section">
<label> Mullti select <small>Text custom</small></label>   
 <div> 
  <select  class="chzn-select " multiple tabindex="10" name="activity[]">
    <option value=""></option> ';
                                                              
while($fetch_execute_get_activy=mysql_fetch_array($execute_get_activy))//looping for getting year
{             
         $name=$fetch_execute_get_activy['1'];

echo'
     <option value="'.$fetch_execute_get_activy[0].'" >'. $name.'</option>';
    $n++;
  }
  
      echo' 
</select>
</div>
</div>
<div class="section last">
<div>
<a class="uibutton submit_form " >save</a>      
</div>
</div>          
<input type="hidden" name="max" value="'.$n.'"/>
<input type="hidden" name="session_id" value="'.$current_session.'"/>
 </form>                                          
            ';
               
            echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';	
         
                
        }
           
          
 //add marks          
function add_nursery_marks_activity_cce()
 {
 $class_id=$_GET['class_id'];
            $student_id=$_GET['student_id'];
            $term_id=$_GET['term_id'];
           $test_id=$_GET['test_id'];
            // qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.class,class_index.section
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.sId = ".$_GET['student_id']."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 $class_name=$fetch_cls_name['class'];
         	 $section=$fetch_cls_name['section'];
                 
                 $test_name="SELECT * FROM cce_test_table WHERE test_id=$test_id";
                 $exe_test=  mysql_query($test_name);
                 $fetch_test=mysql_fetch_array($exe_test);
                 $test_name=$fetch_test[1];
                                           
$get_term="SELECT *
               FROM  `cce_term_table` 
               WHERE term_id=$term_id
                   ";
$execute_get_term=mysql_query($get_term);
   $fetch_term=mysql_fetch_array($execute_get_term);
   $term_name=$fetch_term[1];
       
       echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Class:'.$class_name.'        /          Section:'. $section.'       '.$term_name.'     (  '.$test_name.')</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';       
  
       
       
//get the name of the student on that id
// echo' 	                   
//       <a class="uibutton btn-btn confirm" align="center"  type="button" href="grades_cce/grade_cce_final_report_activity_details_nursery.php?student_id='.$student_id.'&&class_id='.$class_id.'" >View Report</a>                      
//       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_add_marks_personal_development_indicators.php?student_id='.$student_id.'" >Personal development marks</a>    ';
$get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB,`Blood Group`
FROM student_user
WHERE sId = ".$_GET['student_id']."";
$exe_s_name=mysql_query($get_s_name);
$fetch_s_name=mysql_fetch_array($exe_s_name);
$name_student=$fetch_s_name[0];
$admission=$fetch_s_name['admission_no'];

  echo'<h4 align="left" style="color:brown">Name:'.$name_student.'  <br>Admission No.:'.$admission.'</h4>';  


 echo '
<table class="table table-bordered table-striped"  border="2" width="100%" id="dataTable">
<thead >
<tr>
<th rowspan="3">SUBJECT</th>
</tr>
<tr >
<th  rowspan="5">Activity Name</th>
<th>Select Marks</th>

</tr>
</thead>';
 
$get_visitor_year="SELECT DISTINCT subject.subject , subject.subject_id
               FROM  `subject`
                INNER JOIN grade_cce_assign_subject_activity
               ON subject.subject_id=grade_cce_assign_subject_activity.subject_id
                   ";
$execute_visitor_year=mysql_query($get_visitor_year);
while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
{      
         $subject=$fetch_visitor_year['subject'];
                $id=$fetch_visitor_year['subject_id'];
             echo' <tr><td width="20%"><b>'.$subject.'</b></td><td></td><td></td><td></td></tr>';
    

       $get_subject="SELECT  DISTINCT subject. *,grade_cce_activity_name.*
               FROM  `subject`
               INNER JOIN grade_cce_assign_subject_activity
               ON subject.subject_id=grade_cce_assign_subject_activity.subject_id
               INNER JOIN grade_cce_activity_name
               ON grade_cce_activity_name.id=grade_cce_assign_subject_activity.activity_id
               INNER JOIN class_index
               ON class_index.level=grade_cce_assign_subject_activity.level
              WHERE class_index.cId=$class_id  AND grade_cce_assign_subject_activity.subject_id=$id
                   ";
$execute_subject=mysql_query($get_subject);
while($fetch_subject=mysql_fetch_array($execute_subject))//looping for getting year
{   
          $subject=$fetch_subject['subject'];
          $activity=$fetch_subject['activity_name'];
          $activity_id=$fetch_subject['id'];
          $subject_id=$fetch_subject['subject_id'];       
$get_sum=
"SELECT marks
FROM grade_cce_marks_activity
WHERE subject_id =". $id."
AND activity_id = ".$activity_id."
AND student_id = ".$student_id."    AND session_id = ".$_SESSION['current_session_id']." AND test_id=$test_id";
$exe_get_sum_count=mysql_query($get_sum);
$fetch_nur_marks=mysql_fetch_array($exe_get_sum_count);
$nur_marks=$fetch_nur_marks['marks'];
if(($id==1)||($id==2)||( $id==3))     
{  $id;
 echo' <tr><td></td>
<td>'.$activity.'</td><td align="center">
<select  name="activity" id="activity_'.$activity_id.'_'.$subject_id.'_'.$student_id.'"  style="width:60px;" onchange="insert_nur_activity_marks('.$subject_id.','.$_GET['student_id'].', '.$activity_id.','.$term_id.','.$test_id.','.$class_id.');">
 <option value='.$nur_marks.'>'.$nur_marks.'</option>
<option value="10">10</option>
<option value="9.5">9.5</option>
<option value="9">9</option>
<option value="8.5">8.5</option>
<option value="8">8</option>
<option value="7.5">7.5</option>
<option value="7">7</option>
<option value="6.5">6.5</option>
<option value="6">6</option>
<option value="5.5">5.5</option>
<option value="5">5</option>
<option value="4">4</option>
<option value="3">3</option>
<option value="2">2</option>
<option value="1">1</option>
<option value="0">0</option>
</select>
</td>';
}
 elseif(($test_id==18)) {             
$get_sum=
"SELECT marks
FROM grade_cce_marks_activity
WHERE subject_id =".$subject_id."
AND activity_id = ".$activity_id."
AND student_id = ".$student_id."    AND session_id = ".$_SESSION['current_session_id']." AND test_id=18 ";
$exe_get_sum_count=mysql_query($get_sum);
$fetch_nur_marks=mysql_fetch_array($exe_get_sum_count);
echo$nur_markss=$fetch_nur_marks['marks'];
$gg="";
if($nur_markss==10)
{   $gg='A+';

}
elseif($nur_markss==9)
{   $gg='A';

}
elseif($nur_markss==8)
{   $gg='B';

}
elseif($nur_markss==7)
{   $gg='C';

}



$ttest_id=18;
echo'<tr><td></td>
<td>'.$activity.'</td><td align="center">
<select  name="activity" id="activity_'.$activity_id.'_'.$subject_id.'_'.$student_id.'"  style="width:60px;" onchange="insert_nur_activity_marks('.$subject_id.','.$_GET['student_id'].', '.$activity_id.','.$term_id.','.$ttest_id.','.$class_id.');">
 <option value='.$nur_markss.'>'.$gg.'</option>
<option value="10">A+</option>
<option value="9">A</option>
<option value="8">B</option>
<option value="7">C</option>
</select>
</td>';
}

}
}
            echo'  </tbody></table>	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';	
           
				
            
        }
        
        
        
        function grade_cce_add_marks_classwise()
        {
            
            
        echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Activity Details</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';  
  
    echo'      <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          WHERE level <=1
         ";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grade_cce_add_marks_student_classwise.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }
          echo '

       </div>
          </div>
          </div>         ';	
            
            
            
            
        }
        
        function  grade_cce_add_marks_student_classwise()
        {
            
        
	$class_id=$_GET['id_class'];
	
	//get name of the class for that id
	$get_class_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$class_id."";
	$exe_clas_name=mysql_query($get_class_name);
	$fetch_class_name=mysql_fetch_array($exe_clas_name);
	$name_class=$fetch_class_name[0];	
	echo '	 <div class="row-fluid">              
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">                           
                                <div class="widget-header">
                                    <span><i class="icon-home"></i></span>
                                </div><!-- End widget-header -->	                            
                                <div class="widget-content">';
               echo '
                            <h5 style="color:grey" align="center">Class:'.$name_class.'</h5>   ' ;
                              
              echo '
<table class="table table-bordered table-striped"  border="2" width="100%" id="dataTable">
<thead >
<tr>
<th rowspan="2" width=10%>Name</th>
</tr>
<tr><th  colspan="6">Subject  Name</th></tr>

</thead>';

    echo '
           <tbody align="left"><th>';
           //get student names on the class id
    
     $get_visitor_year="SELECT DISTINCT subject.subject , subject.subject_id
               FROM  `subject`
                INNER JOIN grade_cce_assign_subject_activity
               ON subject.subject_id=grade_cce_assign_subject_activity.subject_id
                   ";
$execute_visitor_year=mysql_query($get_visitor_year);
while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
{      
         $subject=$fetch_visitor_year['subject'];
                $id=$fetch_visitor_year['subject_id'];
             echo' <td ><b>'.$subject.'</b></td></th>';          
             
             
}   

            $get_stud_names=
           "SELECT student_user.Name , 
           student_user.sId
           FROM student_user

           INNER JOIN class
           ON class.sId = student_user.sId

           WHERE class.classId = ".$_GET['id_class']."  AND class.session_id=".$_SESSION['current_session_id']."";
           $exe_stu_names=mysql_query($get_stud_names);
           while($fetch_names_stu=mysql_fetch_array($exe_stu_names))
           { 
               $student_id=$fetch_names_stu['sId'];
            
           echo '<tr><td width="15%">'.$fetch_names_stu[0].'</td></tr>';

         }    
          
 
          
    echo '</tbody>
           </table>
           </div>
           </div>
           </div>';

            
            
            
//            
//        $get_subject="SELECT  subject. *,grade_cce_activity_name.*,class.sId
//               FROM  `subject`
//               INNER JOIN grade_cce_assign_subject_activity
//               ON subject.subject_id=grade_cce_assign_subject_activity.subject_id
//               INNER JOIN grade_cce_activity_name
//               ON grade_cce_activity_name.id=grade_cce_assign_subject_activity.activity_id
//               INNER JOIN class_index
//               ON class_index.level=grade_cce_assign_subject_activity.level
//               INNER JOIN class
//           ON class_index.cId = class.classId
//              WHERE class_index.cId=$class_id  AND grade_cce_assign_subject_activity.subject_id=$id
//                   ";
//$execute_subject=mysql_query($get_subject);
//while($fetch_subject=mysql_fetch_array($execute_subject))//looping for getting year
//{   
//     $student_id=$fetch_subject['sId'];
//          $subject=$fetch_subject['subject'];
//          $activity=$fetch_subject['activity_name'];
//         $activity_id=$fetch_subject['id'];
//           $subject_id=$fetch_subject['subject_id'];
//
//                        echo' <td width="15%" ><b>'.$activity.'</b></td></th>';      
//           
//}        
        }        
          //assign to indicator  
  function grade_cce_add_marks_development_indicator()
  {
            
            
       $class_id=$_GET['class_id'];
            $student_id=$_GET['student_id'];
            
            // qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.class,class_index.section
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.sId = ".$_GET['student_id']."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 $class_name=$fetch_cls_name['class'];
         	 $section=$fetch_cls_name['section'];
       echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Class:'.$class_name.'        /          Section:'. $section.'       </span>
      </div><!-- End widget-header -->
      <div class="widget-content">';       
//get the name of the student on that id
	                   
      
$get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB,`Blood Group`
FROM student_user
WHERE sId = ".$_GET['student_id']."";
$exe_s_name=mysql_query($get_s_name);
$fetch_s_name=mysql_fetch_array($exe_s_name);
$name_student=$fetch_s_name[0];
$admission=$fetch_s_name['admission_no'];

  echo'<h4 align="left" style="color:brown">Name:'.$name_student.'  <br>Admission No.:'.$admission.'</h4>';  

echo '
                        <div class="section">	
                        <form id="validation_demo" method="post" action="grades_cce/add_marks_grade_cce_nursery_activity_indicator.php"> 
                        <div class="section ">
                        <label>Level</label>   
                        <div> 
                        <select  name="level" data-placeholder="Choose  LEVEL..." class="chzn-select" tabindex="2" >
                        <option value=""></option>
                        <option value="0">Nursery</option>
                        <option value="1">Pre Primary</option>
                       <option value="2"> Primary</option>
                       <option value="3">Secondary</option>
                       </select>
                        </div>
                        </div>
	  
                        <div class="section "><label>Select Activity</label>   
                <div> 
          <select  data-placeholder="Choose  Activity..." class="chzn-select" tabindex="2" name="activity" id="activity" onchange=" insert_nur_activity_indicator_marks(this.value);">
        <option value=""></option>  ';
                        
$get_visitor_year="SELECT *
               FROM  `grade_cce_activity_name`
               WHERE session_id=".$_SESSION['current_session_id']."
               ORDER BY activity_name  ASC
                   ";
$execute_visitor_year=mysql_query($get_visitor_year);
while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
{   
         $subject=$fetch_visitor_year[1];

echo'
     <option value="'.$fetch_visitor_year[0].'">'. $subject.'</option>';
  }
 echo'    
</select>
</div>
</div>

   <input type="hidden" name="session_id" value="'.$current_session.'"/>
         <input type="hidden" name="student_id" value="'.$student_id.'"/>
                  <span id="show_indicators">
                                                
                     ';
         
            
      echo'  </tbody></table>	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';	
         
                   
        }
        
        
        
        
        
        function grade_cce_activity_dashboard()
        {
            
               echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Activity details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';       
            
           echo' 	                   
       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_view_report_nursery_pre_primary_class.php">Nursery To Pre-Primary</a>          
       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_add_primary_activity.php">I To IV</a>
       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_v_to_viii_dashboard.php">V To X</a>
          <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_xi_to_xii_class_details.php">XI To XII</a>
       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_v_to_x_award_list.php">(VI To VIII)Award List</a>
         <a class="uibutton btn-btn " align="center"  type="button" href="new_grade_cce_a3_report_class.php">A3 Report </a>
         <a class="uibutton btn-btn " align="center"  type="button" href="new_grade_cce_green_sheet_report_class.php">I Term Green Sheet  Report </a> <a class="uibutton btn-btn " align="center"  type="button" href="new_grade_cce_green_sheet_report_class_term_2.php">II Term Green Sheet  Report </a>

<a class="uibutton btn-btn " align="center"  type="button" href="new_grade_cce_green_sheet_full_report_class.php">Full Green Sheet (I-IV)</a>

<a class="uibutton btn-btn " align="center"  type="button" href="preboard_green_sheet_report_class.php">Pre-Board Green Sheet  Report </a>        
<a class="uibutton btn-btn confirm " align="center"  type="button" href="new_grade_cce_blank_award_list_class.php">All class Blank award list</a> 
            <a class="uibutton btn-btn confirm " align="center"  type="button" href="new_grade_cce_loose_sheet_class.php">loose Sheet</a>
            <a class="uibutton btn-btn confirm " align="center"  type="button" href="grade_cce_co_acholastic_award_list_class.php">CO-SCHOLASTIC Awardlist</a>
<a class="uibutton btn-btn confirm " align="center"  type="button" href="grade_cce_xii_class_practical_list_class.php">XII Prctical Exam List</a>';
      
echo'<a class="uibutton btn-confirm " align="center"  type="button" href="grade_cce_test_all_report_class.php">All Student Test Report(V to X)</a>
  <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_test_all_report_class_1_4.php">All Student Test Report(I to IV)</a>
 <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_full_green_sheet_report_v_x.php">FULL Green Sheet Report(V-X)</a> ';


          echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';	
        }
                
       
function grade_cce_test_report_class()
{


 echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
           echo' <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          WHERE  class >=5 AND class <=10
       ORDER BY  order_by
";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grade_cce_view_test_details.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';     
}

function grade_cce_test_details()
{

 $class_id=$_GET['id_class'];

        
echo '  <div class="row-fluid">
<!-- Table widget -->
<div class="widget  span12 clearfix">
<div class="widget-header">
<span><i class="icon-home"></i>Choose subject to view subject activities and marks</span>
</div><!-- End widget-header -->	
<div class="widget-content">';
								
	 //echo'<a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_test_sheet_report_class.php?class_id='.$class_id.'">Test Sheet Report </a><br>'; 
//get subject id from teachers

 $get_id_subject = 
"SELECT *
FROM cce_test_table
INNER JOIN cce_term_test_table
ON cce_term_test_table.test_id=cce_test_table.test_id
WHERE cce_term_test_table.class_id=$class_id
";
$exe_subjects=mysql_query($get_id_subject);
echo '<div class="span4">
';
while($fetch_subjects=mysql_fetch_array($exe_subjects))
{
$test_id=$fetch_subjects[0];
echo ' <a href="grades_cce/grade_cce_test_report_all_students.php?id_class='.$class_id.'& test_id='.$test_id.'&test_name='.$fetch_subjects['test_name'].'"><div class="alertMessage inline info">'.$fetch_subjects['test_name'].'</div></a> 
';
}
echo '</div>';

echo '</div>

</div>
</div>';











}
/////sheet////////
function cce_test_details_all_stu()
{
      
 $class_id=$_GET['class_id'];

      
echo '  <div class="row-fluid">
<!-- Table widget -->
<div class="widget  span12 clearfix">
<div class="widget-header">
<span><i class="icon-home"></i>Choose subject to view subject activities and marks</span>
</div><!-- End widget-header -->	
<div class="widget-content">';
  //get subject id from teachers
 $get_id_subject = 
"SELECT *
FROM cce_test_table
";
$exe_subjects=mysql_query($get_id_subject);
echo '<div class="span4">
';
while($fetch_subjects=mysql_fetch_array($exe_subjects))
{
$test_id=$fetch_subjects[0];
echo ' <a href="grades_cce/grade_cce_test_sheet_report_all_students.php?cid='.$class_id.'& test_id='.$test_id.'&test_name='.$fetch_subjects['test_name'].'"><div class="alertMessage inline info">'.$fetch_subjects['test_name'].'</div></a> 
';
}
echo '</div>';

echo '</div>

</div>
</div>';   
      
      
      
}
function class_test_stu_report()
{
      
 

 echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
           echo' <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          WHERE  class >=5 AND class <=10
       ORDER BY  order_by
";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grade_cce_view_student_test_details.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';          
      
      
}
function student_test_details()
{
      
      $class_id=$_GET['id_class'];
            $get_class="SELECT * FROM class_index  WHERE cId=$class_id";
            $class_iid=mysql_query($get_class);
            $fetch_class=mysql_fetch_array($class_iid);
            $class=$fetch_class['class'];
            $section=$fetch_class['section'];
            
      echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Report details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
               echo'  <h5  style="color:green"align="center">Class:   '.$class.'   <br>    Section:     '.$section.'</h5>';
                    echo '<table  class="table table-bordered table-striped" >
                    <thead>
                    <tr>
                     <th>Admission No.</th>
                     <th>Student Names</th>
                     </tr>
                    </thead>
                    <tbody align="center">
                    ';

              $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId=$class_id
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td><a href="grade_cce_test_details.php?student_id='.$fetch_students['sId'].'&&class_id='.$class_id.'">'.$fetch_students['Name'].'</a></td>
                </tr>	
                    '; 
                     }

echo '</tbody></table>
';          
            
            
                         echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';            
            
                
      
}
function cce_test_details()
{
      
  $class_id=$_GET['class_id'];

     $student_id=$_GET['student_id'];    
echo '  <div class="row-fluid">
<!-- Table widget -->
<div class="widget  span12 clearfix">
<div class="widget-header">
<span><i class="icon-home"></i>Choose subject to view subject activities and marks</span>
</div><!-- End widget-header -->	
<div class="widget-content">';
								
	
//get subject id from teachers
 $get_id_subject = 
"SELECT *
FROM cce_test_table
INNER JOIN cce_term_test_table
ON cce_term_test_table.test_id=cce_test_table.test_id
WHERE cce_term_test_table.class_id=$class_id
";
$exe_subjects=mysql_query($get_id_subject);
echo '<div class="span4">
';
while($fetch_subjects=mysql_fetch_array($exe_subjects))
{
$test_id=$fetch_subjects[0];
echo ' <a href="grades_cce/grade_cce_test_report_students.php?class_id='.$class_id.'& test_id='.$test_id.'&test_name='.$fetch_subjects['test_name'].'&student_id='.$student_id.'"><div class="alertMessage inline info">'.$fetch_subjects['test_name'].'</div></a> 
';
}
echo '</div>';

echo '</div>

</div>
</div>';     
      
      
      
      
      
      
      
}






///////////////////////////////// end All student test  Report   //////////////////////////////////////////////////////////////////////////////
 
        
        function grade_cce_co_scholastic_award_list_class()
        {
            
           echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
           echo' <ol class="rounded-list">
          ';
      $get_class=
          "SELECT *
          FROM class_index
       ORDER BY order_by  ASC 
";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grade_cce_co_scholastic_activity_class.php?class_id='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';            
                 
            
        }
        
        function grade_cce_co_scholastic_award_list_activity()
        {
            $class_id=$_GET['class_id'];
    echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Activity</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
                    
              echo'        <div class="span4">
    <a href="grades_cce/grade_cce_view_co_scho_activity_award_list.php?class_id='.$class_id.'& activity_id=1 && subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">Life Skills</b> <em> </em> </div>
    <div class="breaks"><span></span></div>
	 <a href="grades_cce/grade_cce_view_co_scho_activity_award_list.php?class_id='.$class_id.'& activity_id=2&& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">Work Education & Visual &performing Arts</b> <em> </em> </div>
    <div class="breaks"><span></span></div>
	
     <a href="grades_cce/grade_cce_view_co_scho_activity_award_list.php?class_id='.$class_id.'& activity_id=3&& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">Attitudes & Values  Towards</b><em></em> </div>
    <div class="breaks"><span></span></div>
 <a href="grades_cce/grade_cce_view_co_scho_activity_award_list.php?class_id='.$class_id.'& activity_id=4 && subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
  <b style="color:brown">Co-curricular Activities & Health & phy. Education</b><em></em> </div> '; 
      
         echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';              
            
        }
        
        
        
        
        
        
        
        
        ////loos sheet class
        function grade_cce_loose_sheet_class()
        {
             echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
           echo' <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
       ORDER BY order_by  ASC 
";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grades_cce/grade_cce_loose_sheet_class.php?class_id='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';            
        }
        function grade_cce_blank_award_list_class()
        {
          echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
           echo' <ol class="rounded-list">
          ';
      $get_class=
          "SELECT *
          FROM class_index
       ORDER BY  level ,class,section  ASC 
";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grades_cce/grade_cce_blank_award_list_all_class.php?class_id='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';            
            
        }
        
        
        ///nursery to preprimary
        function view_nurser_pre_primary_class()
        {
        echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
           echo' <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          WHERE  class >=0 AND class <1
       ORDER BY  class  ASC
";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grade_cce_nuresery_to_pre_primary_view_report.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';            
            
        }
        
         function view_nursery_pre_primary_report()
        {
            
      echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Report details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            $class_id=$_GET['id_class'];
         $get_cycle_test="SELECT cce_test_table.*
               FROM  `cce_test_table` 
               INNER JOIN  cce_term_test_table
               ON cce_term_test_table.test_id=cce_test_table.test_id
               WHERE cce_term_test_table.class_id=$class_id
                   ";
         $exe_ass=  mysql_query($get_cycle_test);
         while($fetch_ass=mysql_fetch_array($exe_ass))
         {
             $ass_id=$fetch_ass['test_id'];
         $ass_name=$fetch_ass['test_name'];
         
         echo' <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_view_assesment_report_nursery_pre_primary.php?class_id='.$class_id.'&test_id='.$ass_id.'">'.$ass_name.'</a>';
         }
         
         
              echo' <br>	                   
   
        <a class="uibutton btn-btn confirm " align="center"  type="button" href="grade_cce_view_term_nursery_pre_primary.php?class_id='.$class_id.'">Term Report</a>    
       <a class="uibutton btn-btn confirm " align="center"  type="button" href="grade_cce_view_full_report_nursery_pre_primary.php?class_id='.$class_id.'">Full Report</a>'
                      . ' <a class="uibutton btn-btn confirm " align="center"  type="button" href="grades_cce/grade_cce_view_term_green_sheet.php?class_id='.$class_id.'">Green sheet of Term 1</a>    
       <a class="uibutton btn-btn confirm " align="center"  type="button" href="grades_cce/grade_cce_view_full_green_sheet.php?class_id='.$class_id.'">Green sheet of Term 2</a> 
 <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_view_new_green_sheet_assessement.php?class_id='.$class_id.'">Green sheet Report</a>  '; 
            
                         echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
          
        }

        function grade_cce_term_report_primary()
        {
               $class_id=$_GET['class_id'];
            
            $get_class="SELECT * FROM class_index  WHERE cId=$class_id";
            $class_iid=mysql_query($get_class);
            $fetch_class=mysql_fetch_array($class_iid);
            $class=$fetch_class['class'];
            $section=$fetch_class['section'];
            
      echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Report details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
               echo'  <h5  style="color:green"align="center">Class:   '.$class.'   <br>    Section:     '.$section.'</h5>';
                    echo '<table  class="table table-bordered table-striped" id="dataTable" >
                    <thead>
                    <tr>
                     <th>Admission No.</th>
                     <th>Student Names</th>
                     </tr>
                    </thead>
                    <tbody align="center">
                    ';

              $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId=$class_id
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td><a href="grades_cce/grade_cce_assessment_term_report_details.php?student_id='.$fetch_students['sId'].'&&class_id='.$class_id.'">'.$fetch_students['Name'].'</a></td>
                </tr>	
                    '; 
                     }

echo '</tbody></table>
';          
            
            
                         echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';            
             
            
            
        }
        
        
        
        
        
        function view_nursery_pre_primary_assesment_report()
        {
            $test_id=$_GET['test_id'];
             $class_id=$_GET['class_id'];
            
            $get_class="SELECT * FROM class_index  WHERE cId=$class_id";
            $class_iid=mysql_query($get_class);
            $fetch_class=mysql_fetch_array($class_iid);
            $class=$fetch_class['class'];
            $section=$fetch_class['section'];
            
      echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Report details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
               echo'  <h5  style="color:green"align="center">Class:   '.$class.'   <br>    Section:     '.$section.'</h5>';
                    echo '<table  class="table table-bordered table-striped" id="dataTable" >
                    <thead>
                    <tr>
                     <th>Admission No.</th>
                     <th>Student Names</th>
                     </tr>
                    </thead>
                    <tbody align="center">
                    ';

              $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId=$class_id
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td><a href="grades_cce/grade_cce_assesment_report_activity_details.php?student_id='.$fetch_students['sId'].'&&class_id='.$class_id.'&test_id='.$test_id.'">'.$fetch_students['Name'].'</a></td>
                </tr>	
                    '; 
                     }

echo '</tbody></table>
';          
            
            
                         echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';            
            
            
        }
        
        
        
        
        
        
        
        
        
        
       function view_nursery_pre_primary_full_report()
       {
            $class_id=$_GET['class_id'];
                           // $current_session=$_SESSION['current_session_id'];     
       //it is the class id which comes from previous page.
//$class_id=$_GET['id_class'];
echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>view students </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
			';

                   //query to get class details
                  $q="SELECT `cId`,`class` ,`section`  FROM class_index
                               WHERE `cId`=". $class_id." ";
                        $q_res=mysql_query($q);
                         $res=mysql_fetch_array($q_res);
                         $class=$res['class'];
                          $section=$res['section'];

                    echo'  <h5  style="color:green"align="center">Class:   '.$class.'   <br>    Section:     '.$section.'</h5>';
                    echo '<table  class="table table-bordered table-striped" id="dataTable" >
                    <thead>
                    <tr>
                     <th>Admission No.</th>
                     <th>Student Names</th>
                  </tr>
                    </thead>
                    <tbody align="center">
                    ';

                    $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$class_id."'
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td><a href="grades_cce/grade_cce_assessment_new_full_report.php?student_id='.$fetch_students['sId'].'&&class_id='.$class_id.'">'.$fetch_students['Name'].'</a></td>
                </tr>	
                    '; 
                     }

echo '</tbody></table>
';          
          echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
          
       }
        
        
        
        ////XI  to XII class rep[ort
        function grade_cce_class_xi_to_xii()
        {
            
            echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
           echo'      <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          WHERE  class >=11 AND class <=12
         ";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grade_cce_class_xi_to_xii_view_report.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
                  
        }
        
        
        function grade_cce_view_report_xi_to_xii()
        {
            echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Report details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            $class_id=$_GET['id_class'];
            
              echo' 	                   
       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_view_term_report_xi_class.php?class_id='.$class_id.'" >Term Report</a>                      
       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_view_report_xi_xii.php?class_id='.$class_id.'">Full Report</a>  '; 
            
                         echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
                  
            
        }
        
        
        function grade_cce_view_term_report_xi_to_xii()
        {
               echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Report details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
           // $class_id=$_GET['id_class'];
            
                 $class_id=$_GET['class_id'];
    //get name of the class for that id
	$get_class_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$class_id."";
	$exe_clas_name=mysql_query($get_class_name);
	$fetch_class_name=mysql_fetch_array($exe_clas_name);
	$name_class=$fetch_class_name[0];	

               echo '
                            <h5 style="color:grey" align="center">Class:'.$name_class.'</h5>   ' ;
                              
              echo '
<table class="table table-bordered table-striped"  border="2" width="100%" id="dataTable">
<thead >
<th  width=10%>Admission No.</th><th  colspan="6">  Name</th>
</thead>';

    echo '
           <tbody align="center">';
           //get student names on the class id
   
   $query="SELECT student_user.Name,student_user.admission_no,student_user.sId, class_index.* from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
		INNER JOIN class_index ON class_index.cId = class.classId
                WHERE class.classId='".$_GET['class_id']."'
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>


	<td>';

if($fetch_students['class_2'] == "XII")
echo'<a href="grades_cce/grade_cce_view_student_term_report_xi_xii.php?student_id='.$fetch_students['sId'].'&&class_id='.$_GET['class_id'].'">';


else
{
if(stripos($fetch_students['section'], 'sci'))
echo'<a href="grades_cce/grade_cce_view_student_term_report_xi_science.php?student_id='.$fetch_students['sId'].'&&class_id='.$_GET['class_id'].'">';

else
echo'<a href="grades_cce/grade_cce_view_student_term_report_xi_commerce.php?student_id='.$fetch_students['sId'].'&&class_id='.$_GET['class_id'].'">';

}
echo$fetch_students['Name'].'
</a>
	</td>

                </tr>	
                    '; 
                     }
          
    echo '</tbody>
           </table>
          ';           
                 
            
                echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
            
            
        
            
            
            
            
                echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
            
        }
        
        
        
        
        
        
        
        function grade_cce_view_student_xi_to_xii()
        {
            
           $class_id=$_GET['class_id'];
    //get name of the class for that id
	$get_class_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$class_id."";
	$exe_clas_name=mysql_query($get_class_name);
	$fetch_class_name=mysql_fetch_array($exe_clas_name);
	$name_class=$fetch_class_name[0];	
echo '	 <div class="row-fluid">              
                        <!-- Table widget -->
                        <div class="widget  span12 clearfix">                           
                            <div class="widget-header">
                                <span><i class="icon-home"></i></span>
                            </div><!-- End widget-header -->	                            
                            <div class="widget-content">';
               echo '
                            <h5 style="color:grey" align="center">Class:'.$name_class.'</h5>   ' ;
                              
              echo '
<table class="table table-bordered table-striped"  border="2" width="100%" id="dataTable">
<thead >
<th  width=10%>Admission No.</th><th  colspan="6">  Name</th>
</thead>';

    echo '
           <tbody align="center">';
           //get student names on the class id
   
   $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$_GET['class_id']."'
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
if($_GET['class_id']==58||$_GET['class_id']==59)
{
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td><a href="grades_cce/grade_cce_view_student_full_report_xi_xii_test1.php?student_id='.$fetch_students['sId'].'&&class_id='.$_GET['class_id'].'">'.$fetch_students['Name'].'</a></td>
                </tr>	
                    '; 
}

else
{
 echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td><a href="grades_cce/grade_cce_view_student_full_report_xi_xii_test.php?student_id='.$fetch_students['sId'].'&&class_id='.$_GET['class_id'].'">'.$fetch_students['Name'].'</a></td>
                </tr>	
                    '; 
}
                     }
          
    echo '</tbody>
           </table>
          ';           
                 
          echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
                      
            
            
            
        }
        
        
        
        
        
        
         //award list for V to X...award list...................................................
        
        function grade_cce_award_list_v_to_x()
        {
            
            echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Activity details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';       
            
              
    echo'      <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          WHERE  class >5 AND class <10
         ";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grades_cce/FA_test_award_list.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
                  
            
            
            
            
            
        }
           
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        ///v....to...vIII clas Report.....................................................................................................
        
        
        function class_v_to_viii_dashboard()
        {
            
       echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Activity details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';       
         echo' 	                                   
       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_v_to_viii_class_details.php">View Full Report</a>
       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_v_to_viii_term_class.php">View Term Report</a>'; 
            
  echo'  	                       
  </div><!-- row-fluid column-->
  </div><!--  end widget-content -->
  </div><!-- widget  span12 clearfix-->
   </div><!-- row-fluid -->';	
            
        }
  
        
        ///term wise V to  viii................................................
        function   term_view_class_v_to_viii()
        {
            
            echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Activity details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';       
            
              
    echo'      <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          WHERE  class >=5 AND class <=10
         ";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grade_cce_term_view_student_v_viii_class.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
                  
        }
        
        function  term_view_student_v_to_viii()
        {
             $class_id=$_GET['id_class'];
    //get name of the class for that id
	$get_class_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$class_id."";
	$exe_clas_name=mysql_query($get_class_name);
	$fetch_class_name=mysql_fetch_array($exe_clas_name);
	$name_class=$fetch_class_name[0];	
echo '	 <div class="row-fluid">              
                        <!-- Table widget -->
                        <div class="widget  span12 clearfix">                           
                            <div class="widget-header">
                                <span><i class="icon-home"></i></span>
                            </div><!-- End widget-header -->	                            
                            <div class="widget-content">';


echo' 	                   
                            
       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_view_term_report_class_markswise.php?id_class='.$class_id.'" >View  Markswise Term Report</a>
        ';



               echo '
                            <h5 style="color:grey" align="center">Class:'.$name_class.'</h5>   ' ;
                              
              echo '
<table class="table table-bordered table-striped"  border="2" width="100%" id="dataTable">
<thead >
<tr>
<th rowspan="2" width=10%>Admission No.</th>
</tr>
<tr><th  colspan="6">  Name</th></tr>

</thead>';

    echo '
           <tbody align="center">';
           //get student names on the class id
   
   $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$_GET['id_class']."'
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td><a href="grades_cce/grade_cce_view_student_term_report_v_to_viii.php?student_id='.$fetch_students['sId'].'&&class_id='.$_GET['id_class'].'">'.$fetch_students['Name'].'</a></td>
                </tr>	
                    '; 
                     }
          
    echo '</tbody>
           </table>
           </div>
           </div>
           </div>';           
            
            
            
            
            
        }
                ////view term report
        function term_view_markswise_student_v_to_viii()
        {
            
             $class_id=$_GET['id_class'];
    //get name of the class for that id
	$get_class_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$class_id."";
	$exe_clas_name=mysql_query($get_class_name);
	$fetch_class_name=mysql_fetch_array($exe_clas_name);
	$name_class=$fetch_class_name[0];	
echo '	 <div class="row-fluid">              
                        <!-- Table widget -->
                        <div class="widget  span12 clearfix">                           
                            <div class="widget-header">
                                <span><i class="icon-home"></i></span>
                            </div><!-- End widget-header -->	                            
                            <div class="widget-content">';




               echo '
                            <h5 style="color:grey" align="center">Class:'.$name_class.'</h5>   ' ;
                              
              echo '
<table class="table table-bordered table-striped"  border="2" width="100%" id="dataTable">
<thead >
<tr>
<th rowspan="2" width=10%>Admission No.</th>
</tr>
<tr><th  colspan="6">  Name</th></tr>

</thead>';

    echo '
           <tbody align="center">';
           //get student names on the class id
   
   $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$_GET['id_class']."'
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td><a href="grades_cce/grade_cce_view_markswise_student_term_report_v_to_viii.php?student_id='.$fetch_students['sId'].'&&class_id='.$_GET['id_class'].'">'.$fetch_students['Name'].'</a></td>
                </tr>	
                    '; 
                     }
          
    echo '</tbody>
           </table>
           </div>
           </div>
           </div>';           
            
            
            
              
            
            
            
            
            
            
            
            
        }

        
        //term wise V to  Viii.................................................
        
        
        function  view_class_v_to_viii()
        {
            echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Activity details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';       
            
              
    echo'      <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          WHERE  class >=5 AND class <=10
         ";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grade_cce_view_student_v_viii_class.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }
 
            
            
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';    
            
        }
        
        function  view_student_v_to_vIII()
        {
            
               $class_id=$_GET['id_class'];
    //get name of the class for that id
               
	$get_class_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$class_id."";
	$exe_clas_name=mysql_query($get_class_name);
	$fetch_class_name=mysql_fetch_array($exe_clas_name);
	$name_class=$fetch_class_name[0];	
echo '	 <div class="row-fluid">              
                        <!-- Table widget -->
                        <div class="widget  span12 clearfix">                           
                            <div class="widget-header">
                                <span><i class="icon-home"></i></span>
                            </div><!-- End widget-header -->	                            
                            <div class="widget-content">';
 echo' 	                   
                            
       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_view_report_class_markswise.php?id_class='.$class_id.'" >View  Markswise Report</a>
        ';

               echo '
                            <h5 style="color:grey" align="center">Class:'.$name_class.'</h5>   ' ;
                              
              echo '
<table class="table table-bordered table-striped"  border="2" width="100%"  id="dataTable">
<thead >
<tr>
<th rowspan="2" width=10%>Admission No.</th>
</tr>
<tr><th  colspan="6">  Name</th></tr>

</thead>';

    echo '
           <tbody align="center">';
           //get student names on the class id
   
   $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$_GET['id_class']."'
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td><a href="grades_cce/grade_cce_view_student_full_report_v_to_viii.php?student_id='.$fetch_students['sId'].'&&class_id='.$_GET['id_class'].'">'.$fetch_students['Name'].'</a></td>
                </tr>	
                    '; 
                     }
          
    echo '</tbody>
           </table>
           </div>
           </div>
           </div>';         
        }
        
        /////marks wise student
        function view_student_v_to_vIII_markswise()
        {
            
               $class_id=$_GET['id_class'];
    //get name of the class for that id
               
	$get_class_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$class_id."";
	$exe_clas_name=mysql_query($get_class_name);
	$fetch_class_name=mysql_fetch_array($exe_clas_name);
	$name_class=$fetch_class_name[0];	
echo '	 <div class="row-fluid">              
                        <!-- Table widget -->
                        <div class="widget  span12 clearfix">                           
                            <div class="widget-header">
                                <span><i class="icon-home"></i></span>
                            </div><!-- End widget-header -->	                            
                            <div class="widget-content">';
// echo' 	                   
//                            
//       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_view_report_class_markswise.php?class_id='.$class_id.'" >View  Markswise Report</a>
//        ';

               echo '
                            <h5 style="color:grey" align="center">Class:'.$name_class.'</h5>   ' ;
                              
              echo '
<table class="table table-bordered table-striped"  border="2" width="100%"  id="dataTable">
<thead >
<tr>
<th rowspan="2" width=10%>Admission No.</th>
</tr>
<tr><th  colspan="6">  Name</th></tr>

</thead>';

    echo '
           <tbody align="center">';
           //get student names on the class id
   
   $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$_GET['id_class']."'
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td><a href="grades_cce/grade_cce_view_student_markswise_full_report_v_to_viii.php?student_id='.$fetch_students['sId'].'&&class_id='.$_GET['id_class'].'">'.$fetch_students['Name'].'</a></td>
                </tr>	
                    '; 
                     }
          
    echo '</tbody>
           </table>
           </div>
           </div>
           </div>';          
            
            
            
            
            
        }
        
        
        
        
        
        
        function add_activity_primary()
        {
            
            echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Activity details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';       
        //get the name of the student on that id
 echo' 	                   
                            
       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_view_report_class_primary.php" >View Report</a>
       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_view_term_report_class_primary.php" >View Term Report</a> ';


//<a class="uibutton btn-btn confirm" align="center"  type="button" href="grade_cce_report_assign_activity_details_primary.php" >View Assign  Activity</a> 
//
////FA 1 button form
//echo '<form method ="get" action ="grades_cce/grade_cce_add_activity_details_primary.php" id="1">';
//echo '<input type="hidden" name="fa" value="1">';
//echo '
//<a class="alertMessage inline info" id="flip">C.T.-I</a>
//';
//echo '<div id="panel" align="left" style="background-color:#282828">
//';
//
////to select class
//
//echo '
//<div class="section">
//
//<label style="color:grey">Select Class</label>
//<div>
//<select onchange="show_class(this.value,1);" name="class">
//';
////query to fetch classes from the database
//$sql="SELECT *
//	FROM class_index
//                        WHERE class_name  IN(SELECT class_name
//	FROM class_index   WHERE class <=5 AND level >=2)        
//                         ORDER BY  class+0 ASC , section ASC";
//$exe_class=mysql_query($sql);
//echo '<option>--select class--</option>';
//while($fetch_class=mysql_fetch_array($exe_class))
//{
//    echo '<option value="'.$fetch_class['class_name'].'">'.$fetch_class['class_name'].'</option>
//	';
//}
//echo '</select>
//</div></div>
//<br>';
//
////to select subject according to class
//echo '
//<span id="subject1"></span>';
//
//$sql="SELECT *
//      FROM  types_of_activities
//   
//         ";
//
//$result = mysql_query($sql);
//echo '
//
//
//<div class="section">
//<label style="color:grey">Type of activities</label>
//<div>
//<select id="s1" name="type">
//<option>--select</option>
//
//
//
//';
//while($row = mysql_fetch_array($result))
//  {
//    echo '<option value="'.$row[0].'">'.$row['type_activities'].'</option>'; 
// 
//  }
//
//echo '</select>
//</div>
//</div>';	
//	
////to enter name of activity
//echo '
//<div class="section">
//<label style="color:grey">Name of activity</label>
//<div>
//<input type="text" name="name_of_activity"/>
//</div>
//</div>
//';
////to select type of activities according to subject
//echo '
//<span id="s1"></span>
//';
////to enter max marks
//echo '
//<div class="section">
//<label style="color:grey">Max marks</label>
//<div>
//<input type="text" name="max_marks" style="width: 80px"/>
//</div></div>
//<div class="section last">
//<div>
//<button type ="submit" name ="submit_activity" class="uibutton submit">Submit</button>  
//</div></div>
//</div>
//
//';
//echo '</form>';
//
////FA 2 button form
//echo '<form method ="get" action ="grades_cce/grade_cce_add_activity_details_primary.php" id="2">
//       <input type="hidden" name="fa" value="2"';
//echo '
//<tr>
//<a class="alertMessage inline info" id="flip1">C.T.-II</a>
//
//';
//echo '<div id="panel1" align="left" style="background-color:#282828">
//';
////to select class
//echo '
//<div class="section">
//<label style="color:grey">Select Class</label>
//<div>
//<select onchange="show_class(this.value,2);" name="class">
//';
////query to fetch classes from the database
//$sql="SELECT *
//	FROM class_index
//                        WHERE class_name  IN(SELECT class_name
//	FROM class_index    WHERE class <=5 AND level >=2)        
//                         ORDER BY  class+0 ASC";
//$exe_class=mysql_query($sql);
//echo '<option>--select class--</option>';
//while($fetch_class=mysql_fetch_array($exe_class))
//{
//    echo '<option value="'.$fetch_class['class_name'].'">'.$fetch_class['class_name'].'</option>
//	';
//}
//echo '</select>
//</div>
//</div>
//<br>';
////to select subject according to class
//echo '
//<span id ="subject2"></span>
//';
//
//$sql="SELECT *
//      FROM  types_of_activities
//   
//         ";
//
//$result = mysql_query($sql);
//echo '
//
//
//<div class="section">
//<label style="color:grey">Type of activities</label>
//<div>
//<select id="s1" name="type">
//<option>--select</option>
//
//
//
//';
//while($row = mysql_fetch_array($result))
//  {
//    echo '<option value="'.$row[0].'">'.$row['type_activities'].'</option>'; 
// 
//  }
//
//echo '</select>
//</div>
//</div>';	
//	
////to enter name of activity
//echo '
//<div class="section">
//<label style="color:grey">Name of activity</label>
//<div>
//<input type="text" name="name_of_activity"/>
//</div>
//</div>
//';
//
////to select type of activities according to subject
//echo '
//<span id="s2"></span>
//
//';
////to enter max marks
//echo '
//<div class="section">
//<label style="color:grey">Max marks</label>
//<div>
//<input type="text" name="max_marks" style="width: 80px"/>
//</div>
//</div>
//<div class="section last">
//<div>
//<button type ="submit" name ="submit_activity" class="uibutton submit">Submit</button>
//</div></div>  
//
//</div>
//
//';
//echo '</form>';
//
////FA 3 button form
//echo '<form method ="get" action ="grades_cce/grade_cce_add_activity_details_primary.php" id="3">
//      <input type="hidden" name="fa" value="3"';
//echo '<tr>
//
//<a class="alertMessage inline info" id="flip2">C.T.-III</a>';
//echo '<div id="panel2" align="left" style="background-color:#282828">
//';
////to select class
//echo '
//<div class="section">
//<label style="color:grey">Select Class</label>
//<div>
//<select onchange="show_class(this.value,3);" name="class">
//';
////query to fetch classes from the database
//$sql="SELECT *
//	FROM class_index
//                        WHERE class_name  IN(SELECT class_name
//	FROM class_index    WHERE class <=5 AND level >=2)        
//                        ORDER BY  class+0 ASC";
//$exe_class=mysql_query($sql);
//echo '<option>--select class--</option>';
//while($fetch_class=mysql_fetch_array($exe_class))
//{
//    echo '<option value="'.$fetch_class['class_name'].'">'.$fetch_class['class_name'].'</option>
//	';
//}
//echo '</select>
//</div>
//</div><br>';
////to select subject according to class
//echo '<span id ="subject3"></span>';
//
//$sql="SELECT *
//      FROM  types_of_activities
//   
//         ";
//
//$result = mysql_query($sql);
//echo '
//
//
//<div class="section">
//<label style="color:grey">Type of activities</label>
//<div>
//<select id="s1" name="type">
//<option>--select</option>
//
//
//
//';
//while($row = mysql_fetch_array($result))
//  {
//    echo '<option value="'.$row[0].'">'.$row['type_activities'].'</option>'; 
// 
//  }
//
//echo '</select>
//</div>
//</div>';	
//	
////to enter name of activity
//echo '
//<div class="section">
//<label style="color:grey">Name of activity</label>
//<div>
//<input type="text" name="name_of_activity"/>
//</div>
//</div>
//';
//
////to select type of activities according to subject
//echo '<span id="s3"></span>
//';
////to enter max marks
//echo '
//<div class="section">
//<label style="color:grey">Max marks</label>
//<div>
//<input type="text" name="max_marks" style="width: 80px"/>
//</div>
//</div>
//<div class="section last">
//<div>
//<button type ="submit" name ="submit_activity" class="uibutton submit">Submit</button>  
//</div>
//</div>
//</div>
//
//';
//echo '</form>';
//
////FA 4 button form
//echo '<form method ="get" action ="grades_cce/grade_cce_add_activity_details_primary.php" id="4">
//       <input type="hidden" name="fa" value="4"';
//echo '<tr>
//
//<a class="alertMessage inline info" id="flip3">C.T.-IV</a>';
//echo '<div id="panel3" align="left" style="background-color:#282828">
//';
////to select class
//echo '
//<div class="section">
//<label style="color:grey">Select Class</label>
//<div>
//<select onchange="show_class(this.value,4);" name="class">
//';
////query to fetch classes from the database
//$sql= "SELECT *
//	FROM class_index
//                        WHERE class_name  IN(SELECT class_name
//	FROM class_index    WHERE class <=5 AND level >=2)        
//                         ORDER BY  class+0 ASC";
//$exe_class=mysql_query($sql);
//echo '<option>--select class--</option>';
//while($fetch_class=mysql_fetch_array($exe_class))
//{
//    echo '<option value="'.$fetch_class['class_name'].'">'.$fetch_class['class_name'].'</option>
//	';
//}
//echo '</select>
//</div></div>
//<br>';
////to select subject according to class
//echo '<span id ="subject4"></span>';
//
//$sql="SELECT *
//      FROM  types_of_activities
//   
//         ";
//
//$result = mysql_query($sql);
//echo '
//
//
//<div class="section">
//<label style="color:grey">Type of activities</label>
//<div>
//<select id="s1" name="type">
//<option>--select</option>
//
//
//
//';
//while($row = mysql_fetch_array($result))
//  {
//    echo '<option value="'.$row[0].'">'.$row['type_activities'].'</option>'; 
// 
//  }
//
//echo '</select>
//</div>
//</div>';	
//	
////to enter name of activity
//
////to enter name of activity
//echo '
//<div class="section">
//<label style="color:grey">Name of activity</label>
//<div>
//<input type="text" name="name_of_activity"/>
//</div>
//</div>
//';
////to select type of activities according to subject
//echo '<span id ="s4"></span>
//';
////to enter max marks
//echo '
//<div class="section">
//<label style="color:grey">Max marks</label>
//<div>
//<input type="text" name="max_marks" style="width: 80px"/>
//</div>
//</div>
//<div class="section last">
//<div>
//<button type ="submit" name ="submit_activity" class="uibutton submit">Submit</button>  
//</div>
//</div>
//</div>
//';
//echo '</form>';	




            
            
            
            
            echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';	  
            
        }
        
        function grade_cce_view_primary_report()
        {
            
    echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Activity details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';       
            
           echo '
<table  class="table table-bordered table-striped" border="2" width="100%" height="60" >
<thead >                                          
<tr>
<th row span="6"><font size="4">SUBJECT</font></th>
<th rowspan="4" align="center"><font size="4"><i>Scholastic Area</i></font></th>
<th width="35%"><font size="4"> Marks</font></th>	

 </tr>     																	
  </thead>
';



            
       $select_activity_id="SELECT  *  FROM    types_of_activities ";
            $exe=mysql_query($select_activity_id);
           while( $fetch_activity=mysql_fetch_array($exe))
           {
               
               $act_id=$fetch_activity[0];
               
                  $act_name=$fetch_activity[1];
      
            
            
      $get="SELECT   subject.subject , types_of_activities.type_activities ,scho_activity_table.*,SUM(scho_activity_table.max_marks)
                    FROM  types_of_activities 
                INNER JOIN  scho_activity_table                    
               ON    scho_activity_table.type_id =types_of_activities.Id
               INNER JOIN subject
               ON subject.subject_id=scho_activity_table.subject_id
                 WHERE   scho_activity_table.type_id=$act_id";
            
            $exe_get=mysql_query($get);
            while($fetch_get=mysql_fetch_array($exe_get))
            {      
                 $max_marks=$fetch_get['max_marks']; 
                 $subject=$fetch_get['subject']; 
                  $type_activity=$fetch_get['type_activities'];  
                  $type_activity_id=$fetch_get['Id']; 
                  $activity_name=$fetch_get['Name_of_activity'];
                        echo'<tr><td><b>'.$subject.'</b></td>';  
   echo'<tr><td>'.$type_activity.'</td><td>'.$activity_name.'</td><td>'. $max_marks.'</td></tr></tr>';     
            }      
}     
    echo '</tbody>
           </table>
           ';
    
            
            
            
            
        echo'  	                       
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';

            
            
        }
        
      //////////////////////////////////////////////////////////////////////////////////////////////////////////term report/////////////////////////////////////////
        
        function grade_cce_view_term_report_class_primary()
        {
            
             echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';   
       
    echo'      <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          WHERE class >=1 AND class <=4
         ";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grade_cce_view_student_term_report_primary_class.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }
 
       echo'  	                       
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';
  
        }
        
        function grade_cce_student_view_term_report_primary()
        {
            
            $class_id=$_GET['id_class'];
    //get name of the class for that id
	$get_class_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$class_id."";
	$exe_clas_name=mysql_query($get_class_name);
	$fetch_class_name=mysql_fetch_array($exe_clas_name);
	$name_class=$fetch_class_name[0];	
echo '	 <div class="row-fluid">              
                        <!-- Table widget -->
                        <div class="widget  span12 clearfix">                           
                            <div class="widget-header">
                                <span><i class="icon-home"></i></span>
                            </div><!-- End widget-header -->	                            
                            <div class="widget-content">';
               echo '
                            <h5 style="color:grey" align="center">Class:'.$name_class.'</h5>   ' ;
                              
              echo '
<table class="table table-bordered table-striped"  border="2" width="100%" id="dataTable">
<thead >
<tr>
<th rowspan="2" width=10%>Admission No.</th>
</tr>
<tr><th  colspan="6">  Name</th></tr>

</thead>';

    echo '
           <tbody align="center">';
           //get student names on the class id
   
   $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$_GET['id_class']."'
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td><a href="grades_cce/grade_cce_view_student_term_report_primary.php?student_id='.$fetch_students['sId'].'&&class_id='.$_GET['id_class'].'">'.$fetch_students['Name'].'</a></td>
                </tr>	
                    '; 
                     }

          
    echo '</tbody>
           </table>
           </div>
           </div>
           </div>';    
        }
        
        ////////////////////////////////////////////////////////
        
  function  grade_cce_view_class_primary_report()
  {
        
        echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';   
       
    echo'      <ol class="rounded-list">';
      $get_class=
          "SELECT *
          FROM class_index
          WHERE class  >=1 AND class<=4
         ";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grade_cce_view_student_primary_class.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';
          } 
       echo'  	                       
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';
     
  }
  
  function  grade_cce_view_class_primary_student()
  {
        $class_id=$_GET['id_class'];
    //get name of the class for that id
	$get_class_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$class_id."";
	$exe_clas_name=mysql_query($get_class_name);
	$fetch_class_name=mysql_fetch_array($exe_clas_name);
	$name_class=$fetch_class_name[0];	
echo '	 <div class="row-fluid">              
                        <!-- Table widget -->
                        <div class="widget  span12 clearfix">                           
                            <div class="widget-header">
                                <span><i class="icon-home"></i></span>
                            </div><!-- End widget-header -->	                            
                            <div class="widget-content">';
               echo '
                            <h5 style="color:grey" align="center">Class:'.$name_class.'</h5>   ' ;
                              
              echo '
<table class="table table-bordered table-striped"  border="2" width="100%" id="dataTable">
<thead >
<tr>
<th rowspan="2" width=10%>Admission No.</th>
</tr>
<tr><th  colspan="6">  Name</th></tr>

</thead>';

    echo '
           <tbody align="center">';
           //get student names on the class id
   
   $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$_GET['id_class']."'
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td><a href="grades_cce/grade_cce_view_student_report_primary.php?student_id='.$fetch_students['sId'].'&&class_id='.$_GET['id_class'].'">'.$fetch_students['Name'].'</a></td>
                </tr>	
                    '; 
                     }

          
    echo '</tbody>
           </table>
           </div>
           </div>
           </div>';  
  }
        
  //grade_cce award list
  function award_list_term()
  {
        
     echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Award List</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';        
        
        echo'<form action="cce_award_list_show_subject.php" method="get">
              <div class="section "> <label>Select Class</label>   
               <div> 
          <select  data-placeholder="Choose  Class..." class="chzn-select" tabindex="2" name="class">
        <option value=""></option>  ';

$priv = $_SESSION['priv'];
                        

if($priv == 99)
{
$get_class="SELECT *
               FROM  `class_index`
		WHERE level = 1 OR level = 2
               ORDER BY level ASC, class+0 ASC, section ASC
                   ";
}

else
{
$get_class="SELECT *
               FROM  `class_index`
               ORDER BY class+0 ASC ,section ASC
                   ";
}
$execute_class=mysql_query($get_class);
while($fetch_class=mysql_fetch_array($execute_class))//looping for getting year
{   
         $class=$fetch_class['class_name'];

echo'
     <option value="'.$fetch_class[0].'">'. $class.'</option>';
  }
 echo'    
</select>
</div>
</div>';
 echo'<div class="section "> <label>Select Term</label>   
    <div> 
          <select  data-placeholder="Choose  Term..." class="chzn-select" tabindex="2" name="term">
        <option value=""></option>  ';
                        
$get_term="SELECT *
               FROM  `cce_term_table` 
               
                   ";
$execute_get_term=mysql_query($get_term);
while($fetch_term=mysql_fetch_array($execute_get_term))//looping for getting year
{   
         $term_name=$fetch_term['term_name'];

echo'
     <option value="'.$fetch_term[0].'">'. $term_name.'</option>';
  }
 echo'    
</select>
</div>
</div>';
   $get_cycle_test="SELECT *
               FROM  `cce_test_table` 
               
                   ";
  echo'<div class="section "> <label>Select Cycle Test</label>   
    <div> 
          <select  data-placeholder="Choose  cycle Test..." class="chzn-select" tabindex="2" name="test">
        <option value=""></option>  ';
                        

$execute_get_cycle_test=mysql_query($get_cycle_test);
while($fetch_cycle_test=mysql_fetch_array($execute_get_cycle_test))//looping for getting year
{   
         $cycle_name=$fetch_cycle_test['test_name'];

echo'
     <option value="'.$fetch_cycle_test['test_id'].'">'.$cycle_name.'</option>';
  }
 echo'    
</select>
</div>
</div><div class="section last">
<div>
<button type ="submit"  class="uibutton submit">Submit</button>  
</div>
</div></form>';
       echo'  	                       
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';
      
        
        
        
        
  }
  
  
  function award_list_subject()
  {
echo '  <div class="row-fluid">

         <!-- Table widget -->
         <div class="widget  span12 clearfix">

        <div class="widget-header">
            <span><i class="icon-home"></i>Choose subject to view subject activities and marks</span>
        </div><!-- End widget-header -->	

        <div class="widget-content">';

$class_id=$_GET['class'];
$test_id=$_GET['test'];
$term_id=$_GET['term'];

 $get_name_subject=
"SELECT class_name
FROM class_index
WHERE cId = ".$_GET['class']."


";	
$exe_name_subject=mysql_query($get_name_subject);
while($fetch_name_subject=mysql_fetch_array($exe_name_subject))
{
//get subject id from teachers
 $get_id_subject = 
"SELECT DISTINCT subject.subject , subject.subject_id
FROM subject
INNER JOIN teachers

ON teachers.subject_id = subject.subject_id

WHERE teachers.class = '".$fetch_name_subject[0]."'";
$exe_subjects=mysql_query($get_id_subject);
echo '<div class="span4">
';
while($fetch_subjects=mysql_fetch_array($exe_subjects))
{

echo ' <a href="grade_cce_award_list_details_type.php?class_id='.$_GET['class'].'&subject_id='.$fetch_subjects[1].'&test_id='.$_GET['test'].'&term_id='.$term_id.'"><div class="alertMessage inline info">'.$fetch_subjects[0].'</div></a> 
';
}
echo '</div>';
}
echo '</div>

</div>
</div>';	     
        
        
        
            
  }
  
  function grade_cce_award_list_type()
  {
        
     echo '  <div class="row-fluid">

         <!-- Table widget -->
         <div class="widget  span12 clearfix">

        <div class="widget-header">
            <span><i class="icon-home"></i>Choose subject to view subject activities and marks</span>
        </div><!-- End widget-header -->	

        <div class="widget-content">';

 $class_id=$_GET['class_id'];
 $test_id=$_GET['test_id'];
 $term_id=$_GET['term_id'];        
 $subject_id=$_GET['subject_id'];      
        
    echo' 	                   
 <a class="uibutton btn-btn confirm" align="center"  type="button" href="new_grade_cce_activity_award_list.php?class_id='.$class_id.'&subject_id='.$subject_id.'&test_id='.$test_id.'&term_id='.$term_id.'"" >
 Activity Award List </a>                      
 <a class="uibutton btn-btn " align="center"  type="button" href="grades_cce/cce_cycle_test_award_list.php?class_id='.$class_id.'&subject_id='.$subject_id.'&test_id='.$test_id.'&term_id='.$term_id.'" >
Cycle Test Award List</a> 
<a class="uibutton  danger " align="center"  type="button" href="grades_cce/cce_award_list_details.php?class_id='.$class_id.'&subject_id='.$subject_id.'&test_id='.$test_id.'&term_id='.$term_id.'" >Full Award List</a>
      <a class="uibutton  danger " align="center"  type="button" href="grades_cce/cce_speaking_assess_mark_sheet_details.php?class_id='.$class_id.'&subject_id='.$subject_id.'&test_id='.$test_id.'&term_id='.$term_id.'" >Speaking Assessment Marksheet</a>  
    <a class="uibutton  danger " align="center"  type="button" href="grades_cce/cce_listening_details.php?class_id='.$class_id.'&subject_id='.$subject_id.'&test_id='.$test_id.'&term_id='.$term_id.'" >Listening Assessment </a> 
 <a class="uibutton  danger " align="center"  type="button" href="grades_cce/download_excel_award_list.php?class_id='.$class_id.'&subject_id='.$subject_id.'&test_id='.$test_id.'&term_id='.$term_id.'" >Download Excel </a>  ';    
        
        
 echo '</div>

</div>
</div>';	     
                 
  }
  
  function grade_cce_activity_award_list()
  {
      
      $class_id=$_GET['class_id'];
         $subject_id=$_GET['subject_id'];
        echo '  <div class="row-fluid">

         <!-- Table widget -->
         <div class="widget  span12 clearfix">
        <div class="widget-header">
            <span><i class="icon-home"></i>Activity Details</span>
        </div><!-- End widget-header -->	
        <div class="widget-content">';
        if(($class_id ==7)||($class_id==8)||($class_id==9)||($class_id==10)||($class_id==11)||($class_id==12)||($class_id==13)||($class_id==14)||($class_id==15)||($class_id==16)||($class_id==17)||($class_id==18)||($class_id==19)||($class_id==20)||($class_id==21)||($class_id==22)||($class_id==64))
        {
            if(($subject_id==1)||($subject_id==2))//ENGLISH HINDI
        {
           echo'        <div class="span4">
    <a href="grades_cce/grade_cce_view_activity_award_list.php?class_id='.$class_id.'& activity_id=1 && subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">Reading Skills</b> <em> </em> </div>
    <div class="breaks"><span></span></div>
	 <a href="grades_cce/grade_cce_view_activity_award_list.php?class_id='.$class_id.'& activity_id=2&& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">Writing Skills</b> <em> </em> </div>
    <div class="breaks"><span></span></div>
	
     <a href="grades_cce/grade_cce_view_activity_award_list.php?class_id='.$class_id.'& activity_id=3&& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">Speaking Skills</b><em></em> </div>
    <div class="breaks"><span></span></div>
	 <a href="grades_cce/grade_cce_view_activity_award_list.php?class_id='.$class_id.'& activity_id=4 && subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">Dictation</b><em></em> </div>
    <div class="breaks"><span></span></div>   '; 
            
        }
        elseif($subject_id==3)////MATHS
        {
           echo'   <div class="breaks"><span></span></div>
	 <a href="grades_cce/grade_cce_view_activity_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">Maths-Activity</b><em></em> </div>
    <div class="breaks"><span></span></div>	 '; 
            
            
            
            
        }
                
 elseif($subject_id==4)////EVS
        {
           echo'   <div class="breaks"><span></span></div>
	 <a href="grades_cce/grade_cce_view_activity_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">EVS-Activity</b><em></em> </div>
    <div class="breaks"><span></span></div>	 '; 
            
            
            
            
        }
         elseif($subject_id==6)////COMPUTER
        {
           echo'   <div class="breaks"><span></span></div>
	 <a href="grades_cce/grade_cce_view_activity_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">Computer-Activity</b><em></em> </div>
    <div class="breaks"><span></span></div>	 '; 
            
            
            
            
        }
        
        
           elseif($subject_id==5)////GK
        {
           echo'   <div class="breaks"><span></span></div>
	 <a href="grades_cce/grade_cce_view_activity_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">GK-Activity</b><em></em> </div>
    <div class="breaks"><span></span></div>	 '; 
            
            
            
            
        }
            elseif($subject_id==7)////DRAWING
        {
           echo'   <div class="breaks"><span></span></div>
	 <a href="grades_cce/grade_cce_view_activity_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">Drawing-Activity</b><em></em> </div>
    <div class="breaks"><span></span></div>	 '; 
            
            
            
            
        }
            elseif($subject_id==8)////SOCIAL SCIENCE
        {
           echo'   <div class="breaks"><span></span></div>
	 <a href="grades_cce/grade_cce_view_activity_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">Social Science-Activity</b><em></em> </div>
    <div class="breaks"><span></span></div>	 '; 
            
            
            
            
        }
            elseif($subject_id==9)////SCIENCE
        {
           echo'   <div class="breaks"><span></span></div>
	 <a href="grades_cce/grade_cce_view_activity_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">Science-Activity</b><em></em> </div>
    <div class="breaks"><span></span></div>	 '; 
            
            
            
            
        }

           elseif($subject_id==23)////SST
        {
           echo'   <div class="breaks"><span></span></div>
	 <a href="grades_cce/grade_cce_view_activity_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>

	   <b style="color:brown">SST-Activity</b><em></em> </div>
    <div class="breaks"><span></span></div>	 '; 
            
            
            
            
        }
  
        
        }
        else if(($class_id ==36)||($class_id==37)||($class_id==38)||($class_id==39)||($class_id==40)||($class_id==41)||($class_id==42)||($class_id==43)||($class_id==44)||($class_id==45))
        {
            
           echo'        <div class="span4">
    <a href="grades_cce/grade_cce_view_activity_class_work_9_10_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">Class Work</b> <em> </em> </div>
    <div class="breaks"><span></span></div>
	 <a href="grades_cce/grade_cce_view_activity_home_work_9_10_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">Home Work</b> <em> </em> </div>
    <div class="breaks"><span></span></div>
	
     <a href="grades_cce/grade_cce_view_activity_assignment_9_10_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">Assignment</b><em></em> </div>
     '; 
           
           if($subject_id==1)///english
           {
            echo'     <div class="breaks"><span></span></div>
    <a href="grades_cce/grade_cce_view_activity_speech_9_10_fa_1_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">FA-I (speech-Cum-Poster)</b> <em> </em> </div>
    <div class="breaks"><span></span></div>
	 <a href="grades_cce/grade_cce_view_activity_book_9_10_fa_2_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">FA-II (Book Review)</b> <em> </em> </div>
    <div class="breaks"><span></span></div>
	
     <a href="grades_cce/grade_cce_view_activity_poem_9_10_fa_3_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">FA-III (Poem Waiting)</b><em></em> </div>
           <div class="breaks"><span></span></div>
	
     <a href="grades_cce/grade_cce_view_activity_role_9_10_fa_4_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">FA-IV (Role Play)</b><em></em> </div>
        ';         }   
        else if(($class_id ==36)||($class_id==37)||($class_id==38)||($class_id==39)||($class_id==40)||($class_id ==41)||($class_id==42)||($class_id==43)||($class_id==44)||($class_id==45))///maths
        {
            
             echo'     <div class="breaks"><span></span></div>
    <a href="grades_cce/grade_cce_view_activity_speech_9_10_fa_1_activity_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">FA-I (Activity)</b> <em> </em> </div>
    <div class="breaks"><span></span></div>
	 <a href="grades_cce/grade_cce_view_activity_book_9_10_fa_2_activity_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">FA-II (Activity)</b> <em> </em> </div>
    <div class="breaks"><span></span></div>
	
     <a href="grades_cce/grade_cce_view_activity_poem_9_10_fa_3_activity_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">FA-III (Activity)</b><em></em> </div>
           <div class="breaks"><span></span></div>
	
     <a href="grades_cce/grade_cce_view_activity_role_9_10_fa_4_activity_award_list.php?class_id='.$class_id.'& subject_id='.$subject_id.'"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">FA-IV (Activity)</b><em></em> </div>
        ';  
      }
      //elseif ((($class_id ==41)||($class_id==42)||($class_id==43)||($class_id==44)||($class_id==45))) {
//            
//        }
            
        }
        else if(($class_id ==1)||($class_id==2)||($class_id==3)||($class_id==4)||($class_id==5)||($class_id==6))
        {
            if($subject_id==1)
            {
           echo'        <div class="span4">
    <a href="grades_cce/grade_cce_view_activity_class_nur_english_award_list.php?class_id='.$class_id.'&subject_id=1"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">English Activity</b> <em> </em> </div>
    <div class="breaks"><span></span></div>';
    
            }
            elseif ($subject_id==2) {
            
        

	echo' <a href="grades_cce/grade_cce_view_activity_class_nur_hindi_award_list.php?class_id='.$class_id.'&subject_id=2"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	 <b style="color:brown">Hindi Activity</b> <em> </em> </div>
    <div class="breaks"><span></span></div>';
            }
            elseif ($subject_id==3) {
            
        
    echo' <a href="grades_cce/grade_cce_view_activity_class_nur_math_award_list.php?class_id='.$class_id.'&subject_id=3"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">Maths Activity</b><em></em> </div>
      <div class="breaks"><span></span></div>';
            }
            elseif ($subject_id==24) {
            
        
    echo' <a href="grades_cce/grade_cce_view_activity_class_nur_art_award_list.php?class_id='.$class_id.'&subject_id=24"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	   <b style="color:brown">Art & CRAFT</b><em></em> </div>
   '; 
            }     
            
        }
      
 
      
 
           
 echo '</div>
</div>
</div>';	
      
      
      
      
  }
  
  
  
  
  
  
  
  
  
  
  
  /////////////////////////////////////////////////grade ccce ..........new..........................................................................................................................................................................................
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  function  grade_cce_new_add_activity()
  {
      
$priv = $_SESSION['priv'];

echo '  <div class="row-fluid">
          <!-- Table widget -->
         <div class="widget  span12 clearfix">
        <div class="widget-header">
       <span><i class="icon-home"></i>Add  activities and marks</span>
        </div><!-- End widget-header -->	
        <div class="widget-content">';
        echo' 	                   
 <a class="uibutton btn-btn confirm" align="center"  type="button" href="grade_cce_nursery_add_activity.php" >Nursery To Pre-Primary</a>  
  <a class="uibutton btn-btn " align="center"  type="button" href="new_grade_cce_class_co_scholastic_area.php" >
 Co-scholastic Area</a>';

if($priv != 99)
{
echo'
 <a class="uibutton btn-btn " align="center"  type="button" href="new_grade_cce_allocate_subject_activity_skill.php" >
 Add Scholastic Area</a>';
}
echo'
  <a class="uibutton btn-btn " align="center"  type="button" href="new_grade_cce_add_marks_subject_activity_skill.php" >
 Add Scholastic Marks</a>';  



if($priv != 99)
{ 

echo '
   <a class="uibutton btn-btn confirm " align="center"  type="button" href="new_grade_cce_xi_xii_dashboard.php" >
Class XI to XII </a>'; 
        
     if(isset($_GET['error']))
	{
	echo '<p align="center" style="color:red"><b>Error!!</b></p>';	
		
	}
        else if(isset($_GET['allocated']))
	{
	echo '<p align="center" style="color:red"><b>Error! Already Allocated!</b></p>';	
		
	}
        else if(isset($_GET['add']))
        {
           echo '<p align="center" style="color:green"><b>Added Successfully!!</b></p>';	
        }


$disp='';
                      $q="SELECT *
	FROM class_index
                        WHERE class_name  IN(SELECT class_name
	FROM class_index   WHERE class <=10 AND level >=2)        
                         ORDER BY  class+0 ASC , section ASC";

$q_res=mysql_query($q);




//BUTTON TO ADD THE TERM
echo '<form method ="get" action ="grades_cce/admin_add_term.php" id="1">';

echo '
<a class="alertMessage inline info" id="flip">ADD TERM</a>
';
echo '<div id="panel" align="left" style="background-color:#282828">
';
//to enter name of activity
echo '
<div class="section">
<label style="color:grey;">NAME OF TERM</label>
<div>
<input type="text" name="name_of_term"/><br>
<input type ="submit" name ="add_term" class="uibutton submit">ADD</button> 
</div>

</div>
 <h6 style="background-color:grey; font-family:arial;color:white;font-size:12px;">LIST OF TERM EXIST</h6>
 <table width="100%" id="ajax_delete_term">
 <thead>
 <tr style="background-color:grey; font-family:arial;color:white;font-size:12px;">
 <th>Term Name </th><th>Action</th>
 </tr>
 </thead><tbody>';
// query to get the term available in database fro cce_term_table
$get_term = "SELECT * FROM cce_term_table";
$exe_term = mysql_query($get_term);
while($fetch_term = mysql_fetch_array($exe_term))
{
   echo ' <tr style="background-color:grey; font-family:arial;color:white;font-size:12px;" id="term_'.$fetch_term['term_id'].'">
            <td>'.$fetch_term['term_name'].'</td><td id="edit_term_'.$fetch_term['term_id'].'" class=" "><span><a original-title="Edit"><img src="images/icon/icon_edit.png" onclick="edit_term('.$fetch_term[0].',\''.$fetch_term[1].'\');" title="Edit"></a></span><a original-title="Edit">
<span class="tip"></span></a><a data-name="delete name" original-title="Delete"><img src="images/icon/icon_delete.png" onclick="delete_term('.$fetch_term[0].');" title="Delete"></a>
</td>

         </tr>';
}


echo' 
 <tbody></table>
';

echo '


</div>

';
echo '</form>';

//BUTTON TO AD THE TEST
echo '<form method ="get" action ="grades_cce/admin_add_test.php" id="2">
      

<a class="alertMessage inline info" id="flip1">ADD TEST</a>

';
echo '<div id="panel1" align="left" style="background-color:#282828">
';

//to enter name of test
echo '
<div class="section">
<label style="color:grey">Name of Test</label>
<div>
<input type="text" name="name_of_test"/>
</div>
</div>
';


//to enter max marks
echo '
<div class="section">
<label style="color:grey">Max marks</label>
<div>
<input type="text" name="max_marks" style="width: 80px"/>
</div>
</div>
<div class="section last">
<div>
<button type ="submit" name ="submit_test" class="uibutton submit">Submit</button>
</div></div>  


<h6 style="background-color:grey; font-family:arial;color:white;font-size:12px;">LIST OF TEST AVAILABLE</h6>
 <table width="100%" id="ajax_delete_test" border="2">
 <thead>
 <tr style="background-color:grey; font-family:arial;color:white;font-size:12px;">
 <th>Test Name </th><th>Max Marks </th><th>Action</th>
 </tr>
 </thead><tbody>';
// query to get the test available in database from cce_test_table
$get_test = "SELECT * FROM cce_test_table";
$exe_test = mysql_query($get_test);
while($fetch_test = mysql_fetch_array($exe_test))
{
   echo ' <tr style="background-color:grey; font-family:arial;color:white;font-size:12px;" id="test_'.$fetch_test['test_id'].'">
            <td>'.$fetch_test['test_name'].'</td><td>'.$fetch_test['max_marks'].'</td><td id="edit_term_'.$fetch_test['test_id'].'" class=" "><span><a original-title="Edit"><img src="images/icon/icon_edit.png" onclick="edit_test('.$fetch_test[0].',\''.$fetch_test[1].'\','.$fetch_test['max_marks'].');" title="Edit"></a></span><a original-title="Edit">
<span class="tip"></span></a><a data-name="delete name" original-title="Delete"><img src="images/icon/icon_delete.png" onclick="delete_test('.$fetch_test[0].');" title="Delete"></a>
</td>

         </tr>';
}


echo' <tbody></table>';

echo '</div></form>';



//BUTTON FOR ALLOCATION OF THE TERM TO TEST ON CLASS
echo '<form method ="post" action ="grades_cce/admin_allocate_term_test.php" id="3">
      <input type="hidden" name="fa" value="3"';
echo '<tr>

<a class="alertMessage inline info" id="flip2">ALLOCATE TEST TO TERM</a>';
echo '<div id="panel2" align="left" style="background-color:#282828">
';
//to select class
echo '
<div class="section">
<label style="color:grey">Select Class</label>
<div>
<select name="class" id="class">
';
//query to fetch classes from the database
$sql="SELECT *
               FROM  class_index";
$exe_class=mysql_query($sql);
echo '<option value="">--select class--</option>';
while($fetch_class=mysql_fetch_array($exe_class))
{
    echo '<option value="'.$fetch_class['cId'].'">'.$fetch_class['class_name'].'</option>
	';
}
echo '</select>
</div>
</div><br>';
//to select the term from cce_term_table
echo '<span id ="subject3"></span>';

$sql_term="SELECT *
      FROM  cce_term_table
   
         ";

$result_term = mysql_query($sql_term);
echo '


<div class="section">
<label style="color:grey">Select Term</label>
<div>
<select id="term" name="term">
<option>--select Term--</option>

';
while($row = mysql_fetch_array($result_term))
  {
    echo '<option value="'.$row[0].'">'.$row['term_name'].'</option>'; 
 
  }

echo '</select>
</div>
</div>';	
	

//to select the test from cce_test_table
echo '<span id ="subject3"></span>';

$sql_test="SELECT *
      FROM  cce_test_table
   
         ";

$result_test = mysql_query($sql_test);
echo '
<div class="section">
<label style="color:grey">Select Test</label>
<div>
<select id="test" name="test">
<option>--select Test--</option>



';
while($row1 = mysql_fetch_array($result_test))
  {
    echo '<option value="'.$row1[0].'">'.$row1['test_name'].'</option>'; 
 
  }

echo '</select>
</div>
</div>
';


//to enter max marks
echo '

<div class="section last">
<div>
<button type ="submit" name ="submit_allocate_test_term" class="uibutton submit">Allocate</button>  
</div>
</div>


<h6 style="background-color:grey; font-family:arial;color:white;font-size:12px;">LIST OF ALLOCATED CLASS TERM & TEST</h6>
 <table class="table table-bordered" id="dataTable" width="100%" border="2">

 <thead>
 <tr style="background-color:grey; font-family:arial;font-size:12px;">
<th>Class Name </th><th>Term Name </th> <th>Test Name </th><th>Action</th>
 </tr>
 </thead><tbody>';
// query to get the allocated class test & term
$get_allocated = "SELECT * FROM cce_term_test_table";
$exe_allocated = mysql_query($get_allocated);
while($fetch_allocated = mysql_fetch_array($exe_allocated))
{
   // query to get the name of class
   $get_cls = "SELECT class_name FROM class_index WHERE cId = ".$fetch_allocated['class_id']."";
   $exe_cls = mysql_query($get_cls);
   $fetch_cls_name = mysql_fetch_array($exe_cls);
   
   // query to get the name of term
   $get_term_name = "SELECT term_name FROM cce_term_table WHERE term_id=".$fetch_allocated['term_id']."";
   $exe_term_name = mysql_query($get_term_name);
   $fetch_term_name = mysql_fetch_array($exe_term_name);
   
   //query to get the test name fron cce_test_table
   $get_test_name = "SELECT test_name FROM cce_test_table WHERE test_id=".$fetch_allocated['test_id']."";
   $exe_test_name = mysql_query($get_test_name);
   $fetch_test_name = mysql_fetch_array($exe_test_name);
   
   echo ' <tr style="background-color:grey; font-family:arial;font-size:12px;" id="allovated_'.$fetch_allocated['term_test_id'].'">
            <td>'.$fetch_cls_name['class_name'].'</td><td>'.$fetch_term_name['term_name'].'</td><td>'.$fetch_test_name['test_name'].'</td><td id="edit_term_'.$fetch_test['test_id'].'" class=" "><a data-name="delete name" original-title="Delete"><img src="images/icon/icon_delete.png" onclick="delete_allocated('.$fetch_allocated[0].');" title="Delete"></a>
</td>

         </tr>';
}


echo' <tbody></table>';



echo '</div></form>';

}
	
echo '</div>
</div>
</div>';  
      
  }
  
  //add marks co-scholastic area teacher
  
  function grade_cce_new_class_co_scholastic_teacher()
  {
      echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">                    
<div class="widget-header">
 <span><i class="icon-home"></i>Assign Co-scholastic</span>
</div><!-- End widget-header -->	
<div class="widget-content">';       
        
         echo' 	                   

 <a class="uibutton btn-btn " align="center"  type="button" href="new_grade_cce_add_co_scholastic_area_classwise_teacher.php" >
 Nursery-IV Class</a>
  <a class="uibutton btn-btn  " align="center"  type="button" href="new_grade_cce_add_co_scholastic_area_teacher.php" >
V-X Class</a>';    

                      
        echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';        
      
      
      
      
      
      
      
      
  }
  
  function grade_cce_new_class_Nur_Iv_co_scholastic_teacher()/////nur.....IV class
  {
      echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">                    
<div class="widget-header">
 <span><i class="icon-home"></i>Add C0-scholastic Marks</span>
</div><!-- End widget-header -->	
<div class="widget-content">';   
      echo'<form action="new_grade_cce_add_marks_co_scholastic_term_wise_i_iv.php" method="get">';
          
 echo'<div class="section "> <label>Select Term</label>   
    <div> 
          <select  data-placeholder="Choose  Term..." class="chzn-select" tabindex="2" name="term">
        <option value=""></option>  ';
                        
$get_term="SELECT *
               FROM  `cce_term_table` 
               
                   ";
$execute_get_term=mysql_query($get_term);
while($fetch_term=mysql_fetch_array($execute_get_term))//looping for getting year
{   
         $term_name=$fetch_term['term_name'];

echo'
     <option value="'.$fetch_term[0].'">'. $term_name.'</option>';
  }
 echo'    
</select>
</div>
</div>';
   $get_cycle_test="SELECT *
               FROM  `cce_test_table` 
               
                   ";
  echo'<div class="section "> <label>Select Cycle Test</label>   
    <div> 
          <select  data-placeholder="Choose  cycle Test..." class="chzn-select" tabindex="2" name="test">
        <option value=""></option>  ';
                        

$execute_get_cycle_test=mysql_query($get_cycle_test);
while($fetch_cycle_test=mysql_fetch_array($execute_get_cycle_test))//looping for getting year
{   
         $cycle_name=$fetch_cycle_test['test_name'];

echo'
     <option value="'.$fetch_cycle_test['test_id'].'">'.$cycle_name.'</option>';
  }
 echo'    
</select>
</div>
</div><div class="section last">
<div>
<button type ="submit"  class="uibutton submit">Submit</button>  
</div>
</div></form>';  
      
      
      
      
//      
//   echo'      <ol class="rounded-list">';
//      $get_class=
//          "SELECT *
//          FROM class_index
//          WHERE class  >=1 AND class<=4
//          ORDER BY class+0 ASC ,section ASC  ";
//          $exe_get_class=mysql_query($get_class);
//          while($fetch_classes=mysql_fetch_array($exe_get_class))
//          {
//                echo '<li><a href="new_grade_cce_add_marks_co_scholastic_term_wise_i_iv?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';
//          }      
//    
     echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';         
                 
        
  }
   
      ///
  function grade_cce_new_class_v_X_co_scholastic_teacher()//v-X
  {
      
      
     echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';   
         echo'<form action="new_grade_cce_view_student_v_viii_class.php" method="get">';
          
 echo'<div class="section "> <label>Select Term</label>   
    <div> 
          <select  data-placeholder="Choose  Term..." class="chzn-select" tabindex="2" name="term">
        <option value=""></option>  ';
                        
$get_term="SELECT *
               FROM  `cce_term_table` 
               
                   ";
$execute_get_term=mysql_query($get_term);
while($fetch_term=mysql_fetch_array($execute_get_term))//looping for getting year
{   
         $term_name=$fetch_term['term_name'];

echo'
     <option value="'.$fetch_term[0].'">'. $term_name.'</option>';
  }
 echo'    
</select>
</div>
</div>';
   $get_cycle_test="SELECT *
               FROM  `cce_test_table`  WHERE  test_id>=5 AND test_id<=10
               
                   ";
  echo'<div class="section "> <label>Select  Test</label>   
    <div> 
          <select  data-placeholder="Choose  cycle Test..." class="chzn-select" tabindex="2" name="test">
        <option value=""></option>  ';
                        

$execute_get_cycle_test=mysql_query($get_cycle_test);
while($fetch_cycle_test=mysql_fetch_array($execute_get_cycle_test))//looping for getting year
{   
         $cycle_name=$fetch_cycle_test['test_name'];

echo'
     <option value="'.$fetch_cycle_test['test_id'].'">'.$cycle_name.'</option>';
  }
 echo'    
</select>
</div>
</div>';  
   
    echo'  <div class="section "> <label>Select Class</label>   
               <div> 
          <select  data-placeholder="Choose  Class..." class="chzn-select" tabindex="2" name="class" id="class">
        <option value=""></option>  ';
                        
$get_class="SELECT *
               FROM  `class_index` Where class >=5  AND class <=10
               ORDER BY class+0 ASC ,section ASC
                   ";
$execute_class=mysql_query($get_class);
while($fetch_class=mysql_fetch_array($execute_class))//looping for getting year
{   
         $class=$fetch_class['class_name'];

echo'
     <option value="'.$fetch_class[0].'">'. $class.'</option>';
  }
 echo'    
</select>
</div>
</div><div class="section last">
<div>
<button type ="submit"  class="uibutton submit">Submit</button>  
</div>
</div></form>';
   
   
   
   
   
       echo'  	                       
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';
       
  }
      
      
      
      
      
      
  
  
  
  
  
  
  
  
  
  //////add co-scholastic area 1-Iv class...................
  
  function grade_cce_new_class_co_scholastic()
  {
        
     echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">                    
<div class="widget-header">
 <span><i class="icon-home"></i>Assign Co-scholastic</span>
</div><!-- End widget-header -->	
<div class="widget-content">';       
        
         echo' 	                   

 <a class="uibutton btn-btn " align="center"  type="button" href="new_grade_cce_add_co_scholastic_area_classwise.php" >
 Nursery-IV Class</a>';

$priv = $_SESSION['priv'];
if($priv != 99)
{
echo'
  <a class="uibutton btn-btn  " align="center"  type="button" href="new_grade_cce_add_co_scholastic_area_teacher.php" >
V-X Class</a>';    
}
                      
        echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';         
             
  }
  
  function grade_cce_new_class_co_scholastic_1_4()
  {
        
    echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">                    
<div class="widget-header">
 <span><i class="icon-home"></i>Add C0-scholastic</span>
</div><!-- End widget-header -->	
<div class="widget-content">';      
       
      echo' 	                   
 <a class="uibutton btn-btn " align="center"  type="button" href="new_grade_cce_add_marks_co_scholastic_i_to_iv.php" >
Add Marks</a>'; 
      
$priv = $_SESSION['priv'];

if($priv != 99)
{
      
      if(isset($_GET['error']))
	{
	echo '<p align="center" style="color:red"><b>Please filled All Details</b></p>';	
		
	}
    echo '
                        <div class="section">	
                        <form id="validation_demo" method="post" action="grades_cce/add_marks_grade_cce_nursery_activity_indicator.php"> 
                        <div class="section ">
                        <label>Level</label>   
                        <div> 
                        <select  name="level" data-placeholder="Choose  LEVEL..." class="chzn-select" tabindex="2" id="level">
                        <option value=""></option>
                        <option value="0">Nursery</option>
                        <option value="1">Pre Primary</option>
                       <option value="2"> Primary</option>
                       <option value="3">Secondary</option>
                       </select>
                        </div>
                        </div>
	  
                        <div class="section "><label>Select Term</label>   
                <div> 
          <select  data-placeholder="Choose  Term..." class="chzn-select" tabindex="2" name="term" id="term" onchange="add_co_scholastic(this.value);">
        <option value=""></option>  ';
                        
$get_visitor_year="SELECT *
               FROM  `cce_term_table`
              
                   ";
$execute_visitor_year=mysql_query($get_visitor_year);
while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
{   
         $subject=$fetch_visitor_year[1];

echo'
     <option value="'.$fetch_visitor_year[0].'">'. $subject.'</option>';
  }
 echo'    
</select>
</div>
</div><span id="show_details">';

}    
    
   echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';         
        
        
        
  }
  
 ////////////////////////////////////////////////////addd marks co-scholastic 1--4 class
  function grade_cce_new_add_marks_co_scholastic_1_4_class()
  {
      echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">                    
<div class="widget-header">
 <span><i class="icon-home"></i>Add C0-scholastic Marks</span>
</div><!-- End widget-header -->	
<div class="widget-content">';   
      echo'<form action="new_grade_cce_add_marks_co_scholastic_term_wise_i_iv.php" method="get">';
          
 echo'<div class="section "> <label>Select Term</label>   
    <div> 
          <select  data-placeholder="Choose  Term..." class="chzn-select" tabindex="2" name="term">
        <option value=""></option>  ';
                        
$get_term="SELECT *
               FROM  `cce_term_table` 
               
                   ";
$execute_get_term=mysql_query($get_term);
while($fetch_term=mysql_fetch_array($execute_get_term))//looping for getting year
{   
         $term_name=$fetch_term['term_name'];

echo'
     <option value="'.$fetch_term[0].'">'. $term_name.'</option>';
  }
 echo'    
</select>
</div>
</div>';
   $get_cycle_test="SELECT *
               FROM  `cce_test_table` 
               
                   ";
  echo'<div class="section "> <label>Select Cycle Test</label>   
    <div> 
          <select  data-placeholder="Choose  cycle Test..." class="chzn-select" tabindex="2" name="test">
        <option value=""></option>  ';
                        

$execute_get_cycle_test=mysql_query($get_cycle_test);
while($fetch_cycle_test=mysql_fetch_array($execute_get_cycle_test))//looping for getting year
{   
         $cycle_name=$fetch_cycle_test['test_name'];

echo'
     <option value="'.$fetch_cycle_test['test_id'].'">'.$cycle_name.'</option>';
  }
 echo'    
</select>
</div>
</div><div class="section last">
<div>
<button type ="submit"  class="uibutton submit">Submit</button>  
</div>
</div></form>';  
      
      
      
      
//      
//   echo'      <ol class="rounded-list">';
//      $get_class=
//          "SELECT *
//          FROM class_index
//          WHERE class  >=1 AND class<=4
//          ORDER BY class+0 ASC ,section ASC  ";
//          $exe_get_class=mysql_query($get_class);
//          while($fetch_classes=mysql_fetch_array($exe_get_class))
//          {
//                echo '<li><a href="new_grade_cce_add_marks_co_scholastic_term_wise_i_iv?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';
//          }      
//    
     echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';         
                 
  }
  
  function grade_cce_new_add_marks_co_scholastic_term_wise_1_4()
  {
        
 echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">                    
<div class="widget-header">
<span><i class="icon-home"></i>Add Marks</span>
</div><!-- End widget-header -->	
<div class="widget-content">';      
        
        $class_id=$_GET['class'];
        $term_id=$_GET['term'];
        $test_id=$_GET['test'];
         echo'<form action="grades_cce/new_cce_insert_co_coscho_marks_i_iv.php" method="get">';
          echo'  <div class="section "> <label>Select Class</label>   
               <div> 
          <select  data-placeholder="Choose  Class..." class="chzn-select" tabindex="2" name="class" id="class" onchange="select_class_co_scho_activity('.$term_id.','.$test_id.');">
        <option value=""></option>  ';
                        
$get_class="SELECT *
               FROM  `class_index` Where  class <=4
               ORDER BY class+0 ASC ,section ASC
                   ";
$execute_class=mysql_query($get_class);
while($fetch_class=mysql_fetch_array($execute_class))//looping for getting year
{   
         $class=$fetch_class['class_name'];

echo'
     <option value="'.$fetch_class[0].'">'. $class.'</option>';
  }
 echo'    
</select>
</div>
</div>';
        echo'<span id="show_co_act"></span>'
 . '<span id="show_co_activity"></span> '
                . '<span id="show_student"></span> ';
        
        
        
     
     echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';    
        
        
  }
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  
  
  
  
  function new_grade_cce_allocat_activity_skill()
  {
        
       echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">                    
<div class="widget-header">
 <span><i class="icon-home"></i>Assign Activity</span>
</div><!-- End widget-header -->	
<div class="widget-content">';      
      if(isset($_GET['assign']))
            {
	echo "<h4 style=\"color:green\" align=\"center\">Successfully Assign</h4>" ;
            }
                  
        //term 3 button form
echo '    <form  id="validation_demo" method="post" action="grades_cce/cce_insert_subject_activity_skills.php" id="3"> 
      <input type="hidden" name="fa" value="3">';
echo '<tr>

<a class="alertMessage inline info" id="flip2">Assign Activity</a>';
echo '<div id="panel2" align="left" style="background-color:#CEF6F5">

<div class="section "> <label>Select Subject</label>   
    <div> 
          <select  data-placeholder="Choose  subject..." class="chzn-select" tabindex="2" name="subject">
        <option value=""></option>  ';
                        
$get_subject="SELECT *
               FROM  `subject`
               ORDER BY subject ASC
                   ";
$execute_subject=mysql_query($get_subject);
while($fetch_subject=mysql_fetch_array($execute_subject))//looping for getting year
{   
         $subject=$fetch_subject['subject'];

echo'
     <option value="'.$fetch_subject[0].'">'. $subject.'</option>';
  }
 echo'    
</select>
</div>
</div>
<div class="section">
<label>Select Class</label>   
 <div> 
  <select  class="chzn-select " multiple tabindex="18" name="class[]">
    <option value=""></option> ';
                        
$get_activity="SELECT *
               FROM  class_index ";
$execute_activity=mysql_query($get_activity);
while($fetch_activity=mysql_fetch_array($execute_activity))//looping for getting year
{   
         $activity_name=$fetch_activity['class_name'];

echo'
     <option value="'.$fetch_activity[0].'">'. $activity_name.'</option>';
  }
 echo'    
</select>
</div>
</div>
<div class="section">
<label>Select Activity</label>   
 <div> 
  <select  class="chzn-select " multiple tabindex="10" name="activity[]">
    <option value=""></option> ';
                        
$get_activity="SELECT *
               FROM  cce_types_activity_table ";
$execute_activity=mysql_query($get_activity);
while($fetch_activity=mysql_fetch_array($execute_activity))//looping for getting year
{   
         $activity_name=$fetch_activity['type_name'];

echo'
     <option value="'.$fetch_activity[0].'">'. $activity_name.'</option>';
  }
 echo'    
</select>
</div>
</div>
<div class="section">
<label>Select Skills</label>   
 <div> 
  <select  class="chzn-select " multiple tabindex="10" name="skill[]">
    <option value=""></option> ';
     
 $get_skill="SELECT *
               FROM   `cce_name_skills_table`  ";
$execute_skill=mysql_query($get_skill);
 
 
while($fetch_execute_skill=mysql_fetch_array($execute_skill))//looping for getting year
{             
         $skill_name=$fetch_execute_skill['1'];

echo'
     <option value="'.$fetch_execute_skill[0].'" >'. $skill_name.'</option>';
    $n++;
  }
  
      echo' 
</select>
</div>
</div>
<div class="section last">
<div>
<a class="uibutton submit_form " >save</a>      
</div>
</div>          
<input type="hidden" name="max" value="'.$n.'"/>
 </form>                                          
            ';  
//to select type of activities according to subject
echo '<span id="s3"></span>
';
//to enter max marks
echo '
</div>

';
echo '</form>';
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//test 4 button form
echo '<form method ="get" action ="grades_cce/new_cce_add_scho_remarks.php" id="4">
       <input type="hidden" name="fa" value="4">';
echo '<tr>
<a class="alertMessage inline info" id="flip3">Add Remarks</a>';
echo '<div id="panel3" align="left" style="background-color:#CEF6F5">';
//to enter name of activity

//to enter name of activity
echo '
<div class="section">
<label style="color:grey">Remarks</label>
<div>
<input type="text" name="remarks"/>
</div>
</div>
';
//to select type of activities according to subject
echo '<span id ="s4"></span>
';
//to enter max marks
echo '
<div class="section">
<label style="color:grey">Grade</label>
<div>
<input type="text" name="grade" style="width: 80px"/>
</div>
</div>
<div class="section last">
<div>
<button type ="submit" name ="submit" class="uibutton submit">Submit</button>  
</div>
</div>
</div>
';
echo '</form>';    
     //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////       
    //test 4 button form
echo '<form method ="get" action ="grades_cce/new_cce_insert_activity_skill_remarks.php"  id="2">
       <input type="hidden" name="fa" value="4">';
echo '<tr>
<a class="alertMessage inline info" id="flip1">Assign Remarks</a>';
echo '<div id="panel1" align="left" style="background-color:#CEF6F5">';
echo'<div class="section "> <label>Select Subject</label>   
    <div> 
          <select  data-placeholder="Choose  subject..." class="chzn-select" tabindex="2" name="subject" id=subject>
        <option value=""></option>  ';
                        
$get_subject="SELECT *
               FROM  `subject`
               ORDER BY subject ASC
                   ";
$execute_subject=mysql_query($get_subject);
while($fetch_subject=mysql_fetch_array($execute_subject))//looping for getting year
{   
         $subject=$fetch_subject['subject'];

echo'
     <option value="'.$fetch_subject[0].'">'. $subject.'</option>';
  }
 echo'    
</select>
</div>
</div>';
echo'<div class="section">
<label>Select Class</label>   
 <div> 
  <select  class="chzn-select " multiple tabindex="18" name="class[]"  id="class"  onchange="select_remarks_activity(this.value)">
    <option value=""></option> ';
                        
$get_activity="SELECT *
               FROM  class_index ";
$execute_activity=mysql_query($get_activity);
while($fetch_activity=mysql_fetch_array($execute_activity))//looping for getting year
{   
         $activity_name=$fetch_activity['class_name'];

echo'
     <option value="'.$fetch_activity[0].'">'. $activity_name.'</option>';
  }
 echo'    
</select>
</div>
</div><span id="show_activity"></span>';

echo '</form>';        

       
        echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';    
        
        
  }
  
  
  
  //add marks 
  function new_grade_cce_add_marks_scholatic_area()
  {

$priv = $_SESSION['priv'];
        
  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Add Marks</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';        
       // echo'   <a class="uibutton normal" type="button" href="admin_select_class_report.php">V to X</a>';
        echo'<form id="validation_demo" method ="get" action ="new_grade_cce_view_subject_class.php">';
             
 echo'<div class="section "> <label>Select Term</label>   
    <div> 
          <select  data-placeholder="Choose  Term..." class="chzn-select" tabindex="2" name="term" id="term">
        <option value=""></option>  ';
                        
$get_term="SELECT *
               FROM  `cce_term_table` 
               
                   ";
$execute_get_term=mysql_query($get_term);
while($fetch_term=mysql_fetch_array($execute_get_term))//looping for getting year
{   
         $term_name=$fetch_term['term_name'];

echo'
     <option value="'.$fetch_term[0].'">'. $term_name.'</option>';
  }
 echo'    
</select>
</div>
</div>
  <div class="section "> <label>Select Class</label>   
               <div> 
          <select  data-placeholder="Choose  Class..." class="chzn-select" tabindex="2" name="class" id="class" onchange="select_test(this.value '.$fetch_term[0].')">
        <option value=""></option>  ';
            
if($priv == 99)
{            
$get_class="SELECT *
               FROM  `class_index`
		WHERE level=2
               ORDER BY class+0 ASC ,section ASC
                   ";
}

else
{
$get_class="SELECT *
               FROM  `class_index` Where class>=1
               ORDER BY class+0 ASC ,section ASC
                   ";
}
$execute_class=mysql_query($get_class);
while($fetch_class=mysql_fetch_array($execute_class))//looping for getting year
{   
         $class=$fetch_class['class_name'];

echo'
     <option value="'.$fetch_class[0].'">'. $class.'</option>';
  }
 echo'    
</select>
</div>
</div><span id="show_test"></span>
  <div class="section last">
<div>
 <a class="uibutton submit_form "  type="submit"  >
GO</a>
</div>
</div> 

</form>';
       echo'  	                       
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';
                 
  }
  
  
  function new_grade_cce_view_subject_class()
  {
        
             $class_id=$_GET['class'];  
             $term_id=$_GET['term'];   
              $test_id=$_GET['test'];  
              
        
echo '  <div class="row-fluid">

<!-- Table widget -->
<div class="widget  span12 clearfix">

<div class="widget-header">
<span><i class="icon-home"></i>Choose subject to view subject activities and marks</span>
</div><!-- End widget-header -->	

<div class="widget-content">';
								
	
//get subject id from teachers
 $get_id_subject = 
"SELECT DISTINCT subject.subject , subject.subject_id
FROM subject
INNER JOIN cce_subject_type_skills_id_table

ON cce_subject_type_skills_id_table.subject_id = subject.subject_id

WHERE cce_subject_type_skills_id_table.class_id =$class_id";
$exe_subjects=mysql_query($get_id_subject);
echo '<div class="span4">
';
while($fetch_subjects=mysql_fetch_array($exe_subjects))
{

echo ' <a href="new_grade_cce_add_marks_all_students_class.php?class_id='.$_GET['class'].'&subject_id='.$fetch_subjects[1].'&term_id='.$term_id.'& test_id='.$test_id.'"><div class="alertMessage inline info">'.$fetch_subjects[0].'</div></a> 
';
}
echo '</div>';

echo '</div>

</div>
</div>';	
        
        
        
  }
  
  
  function new_grade_cce_add_marks_all_student()
  {
        
             $class_id=$_GET['class_id'];  
             $term_id=$_GET['term_id'];   
             $test_id=$_GET['test_id'];    
             $subject_id=$_GET['subject_id'];    
             
             $test_name="SELECT * FROM cce_test_table WHERE test_id=$test_id";
             $exe_test_name=mysql_query($test_name);
             $fetch_test_name=mysql_fetch_array($exe_test_name);
             $test=$fetch_test_name['test_name'];
             
             
             
             
        //query to get the term_test_id from test term table
             $get_term_test_id="SELECT * FROM  `cce_term_test_table` WHERE test_id=$test_id AND term_id=$term_id AND class_id=$class_id";
$exe_term_test=mysql_query($get_term_test_id);
$fetch_term_test=  mysql_fetch_array($exe_term_test);
//$test_name=$fetch_term_test['test_name'];
$term_test_id=$fetch_term_test[0];
       





        //get name of the class for that id
	$get_class_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$class_id."";
	$exe_clas_name=mysql_query($get_class_name);
	$fetch_class_name=mysql_fetch_array($exe_clas_name);
	$name_class=$fetch_class_name[0];	
       	
//get subject id from teachers
 $get_id_subject = 
"SELECT *
FROM subject WHERE subject_id=$subject_id
";
$exe_subjects=mysql_query($get_id_subject);            
 $fetch_subject=  mysql_fetch_array($exe_subjects);
 $subject=$fetch_subject['subject'];
 
               
               
echo '<div class="row-fluid" style="width:111%;">              
      <!-- Table widget -->
      <div class="widget  span12 clearfix"style="width:100%;" >                           
          <div class="widget-header" style="width:100%;">
              <span><i class="icon-home"></i>Class->'.$name_class.', Subject->'.$subject.',Term->'.$term_id.',Test->'.$test.'</span>
          </div><!-- End widget-header -->	                            
          <div class="widget-content" >';

echo' 	                   
                            
       <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_insert_asl_marks.php?class_id='.$class_id.' &term_id='.$term_id.'&test_id='.$test_id.'" >ASL Marks</a>
        ';



// query to get the activity on the basic of subject
$get_type_id = "SELECT DISTINCT type_id FROM cce_subject_type_skills_id_table WHERE subject_id =$subject_id AND class_id=$class_id";
$exe_type_id = mysql_query($get_type_id);


              echo '
<table class="table table-bordered table-striped"  border="2" width="100%"  id="dataTable"><thead >
<tr>
<th width=4% rowspan="2"><b>Admission No.</th>

<th  width=10% rowspan="2"><b>Name</th>';
       while($fetch_type_id= mysql_fetch_array($exe_type_id))
       {
          //query to get the type name
         $get_type_name = "SELECT * FROM cce_types_activity_table where type_id = ".$fetch_type_id['type_id']."";
          $exe_type_name = mysql_query($get_type_name);
          $fetch_type_name = mysql_fetch_array($exe_type_name);
          
         
          //query to get the skill_id
        $get_skill_id = "SELECT DISTINCT skill_id FROM cce_subject_type_skills_id_table where type_id =".$fetch_type_id['type_id']."";
        $exe_skill_id = mysql_query($get_skill_id);
        $num_rows = mysql_num_rows($exe_skill_id); 
        if($num_rows == 1)
        {
         echo' <th  width=8% rowspan="2"><b>'.$fetch_type_name['type_name'].'</th> ';
        }
        else
        {
           echo' <th  width=8% rowspan="" colspan="'.$num_rows.'"><b>'.$fetch_type_name['type_name'].'</th> ';
        }
        
       }
       
 
 echo'</tr><tr>';
 // coding to print 2nd rows
 // query to get the activity on the basic of subject
$get_type_id1 = "SELECT DISTINCT type_id FROM cce_subject_type_skills_id_table WHERE subject_id =$subject_id AND class_id=$class_id";
$exe_type_id1 = mysql_query($get_type_id1);
  while($fetch_type_id1= mysql_fetch_array($exe_type_id1))
       {
                   
          //query to get the skill_id
        $get_skill_id1 = "SELECT DISTINCT skill_id FROM cce_subject_type_skills_id_table where type_id =".$fetch_type_id1['type_id']."";
        $exe_skill_id1 = mysql_query($get_skill_id1);
        $num_rows1 = mysql_num_rows($exe_skill_id1); 
        while($fetch_skill_name = mysql_fetch_array($exe_skill_id1))
        if($fetch_skill_name['skill_id'] == 0)
        {
         echo'';
        }
        else
        {
           //query to get the skill name
           $get_skill_name= "SELECT * FROM cce_name_skills_table WHERE skill_id=".$fetch_skill_name['skill_id']."";
           $exe_skill_name = mysql_query($get_skill_name);
           $fetch_skill_name = mysql_fetch_array($exe_skill_name);
           echo' <th  width=10% rowspan=""><b>'.$fetch_skill_name['skill_name'].'</th> ';
        }
        
       }
 
echo '</tr>';


echo'</thead>';
 
  
 
 $col=$j;
    echo '
           <tbody align="center">';
           //get student names on the class id
   
   $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$_GET['class_id']."'
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
                  echo'
                <tr>
                    <td width=""align="left">'.$fetch_students['admission_no'].'</td>
                    <td width="" align="left">'.$fetch_students['Name'].'</a></td>';
                  
                  $sid = $fetch_students['sId'];
                  // coding to print the main table body
          // query to get the activity on the basic of subject
         $get_type_id2 = "SELECT DISTINCT type_id FROM cce_subject_type_skills_id_table WHERE subject_id =$subject_id AND class_id=$class_id";
         $exe_type_id2 = mysql_query($get_type_id2);
           while($fetch_type_id2= mysql_fetch_array($exe_type_id2))
                {
                  $type_id = $fetch_type_id2['type_id'];
                  //query to get the skill_id
                 $get_skill_id2 = "SELECT DISTINCT skill_id FROM cce_subject_type_skills_id_table where type_id =".$fetch_type_id2['type_id']."";
                 $exe_skill_id2 = mysql_query($get_skill_id2);
                 //$num_rows2 = mysql_num_rows($exe_skill_id2); 
                 while($fetch_skill_id2 = mysql_fetch_array($exe_skill_id2))
                 
                 if($fetch_skill_id2['skill_id'] == 0)
                 {
                    $skill_id = 0;
                     if($type_id==1)
                     {
                                         $marks = '';

                                        // query to get the sts_id from cce_subject_type_skills_id_table
                                        $get_sts_id = "SELECT sts_id FROM cce_subject_type_skills_id_table WHERE subject_id=".$subject_id." AND type_id=".$type_id." AND skill_id=".$skill_id." AND class_id=".$class_id."";
                                        $exe_sts_id = mysql_query($get_sts_id);
                                        $fetch_sts_id = mysql_fetch_array($exe_sts_id);
                                        $sts_id = $fetch_sts_id['sts_id'];
                                        if($sts_id)
                                        {
                                        //query to check the already enserted marks in cce_marks_scho_type_skill_table in database
                                         $get_marks = "SELECT marks FROM cce_marks_scho_type_skills_table
                                         WHERE term_test_id = $term_test_id AND student_id=".$sid." AND sts_id=".$sts_id."";
                                         $exe_marks = mysql_query($get_marks);
                                         $fetch_marks = mysql_fetch_array($exe_marks);
                                         $marks=$fetch_marks['marks'];

                                        }


                                        
                                       echo'<td width=10% rowspan=""><input type="text" value="'.$marks.'" style="width:25px;color:green;" onblur="save_data('.$class_id.','.$subject_id.','.$term_id.','.$test_id.','.$sid.','.$type_id.','.$skill_id.')" id="td_'.$class_id.''.$subject_id.''.$term_id.''.$test_id.''.$sid.''.$type_id.''.$skill_id.'"><span id="mark_'.$class_id.''.$subject_id.''.$term_id.''.$test_id.''.$sid.''.$type_id.''.$skill_id.'" min="1" max="5"></span></td>';
                                        
                     }
                  else 
                      {   
                    
                $get_marks = "SELECT  cce_marks_scho_type_skills_table.marks  FROM cce_marks_scho_type_skills_table
 INNER JOIN  cce_subject_type_skills_id_table 
 ON cce_subject_type_skills_id_table.sts_id=cce_marks_scho_type_skills_table.sts_id
WHERE cce_marks_scho_type_skills_table.term_test_id = $term_test_id AND cce_marks_scho_type_skills_table.student_id=".$sid." AND cce_subject_type_skills_id_table.skill_id=0   AND  cce_subject_type_skills_id_table.type_id=$type_id AND cce_subject_type_skills_id_table.subject_id=$subject_id  ";
                                         $exe_marks = mysql_query($get_marks);
                                        $fetch_marks = mysql_fetch_array($exe_marks);
                                        $markss=$fetch_marks['marks'];
                      
//                                  // query to get the remark id from cce_scho_activity_remarks_table
//                            $get_remark_id = "SELECT DISTINCT remarks_id FROM cce_scho_activity_remarks_table WHERE activity_id=".$type_id."";
//                               $exe_remark_id = mysql_query($get_remark_id);

                               echo '<td width=10% rowspan=""><select name="test_id" style="width:60px;" onchange="save_data('.$class_id.','.$subject_id.','.$term_id.','.$test_id.','.$sid.','.$type_id.','.$skill_id.')" id="td_'.$class_id.''.$subject_id.''.$term_id.''.$test_id.''.$sid.''.$type_id.''.$skill_id.'"><option value="0">'.$markss.'</option>
                                  ';

//                               while($fetch_remark_id = mysql_fetch_array($exe_remark_id))
//                               {

                                  // query to get remark in select box from cce_scho_remarks_table
                                   $get_remark = "SELECT DISTINCT(grade),marks  FROM cce_scho_remarks_table WHERE marks!='' ORDER BY marks DESC";
                                  $exe_remark = mysql_query($get_remark);
                                 while( $fetch_remark = mysql_fetch_array($exe_remark))
                                 {
                                     // query to get the marks of grade from cce_scho_remarks_table table
                                         $fetch_remark['grade'];
                                        echo '<option value="'.$fetch_remark['marks'].'">'.$fetch_remark['marks'].'</option>'; 

                                 }
                              // }
                                  echo '</select></td>';

                      }
                 }
                 else
                 {
                    $skill_id = $fetch_skill_id2['skill_id'];
                    $marks = '';

                   // query to get the sts_id from cce_subject_type_skills_id_table
                   $get_sts_id = "SELECT sts_id FROM cce_subject_type_skills_id_table WHERE subject_id=".$subject_id." AND type_id=".$type_id." AND skill_id=".$skill_id." AND class_id=".$class_id."";
                   $exe_sts_id = mysql_query($get_sts_id);
                   $fetch_sts_id = mysql_fetch_array($exe_sts_id);
                   $sts_id = $fetch_sts_id['sts_id'];
                   if($sts_id)
                   {
                   //query to check the already enserted marks in cce_marks_scho_type_skill_table in database
                    $get_marks = "SELECT marks FROM cce_marks_scho_type_skills_table
                    WHERE term_test_id = $term_test_id AND student_id=".$sid." AND sts_id=".$sts_id."";
                    $exe_marks = mysql_query($get_marks);
                    $fetch_marks = mysql_fetch_array($exe_marks);
                    $marks=$fetch_marks['marks'];

                   }
                    $get_marks = "SELECT  cce_marks_scho_type_skills_table.marks  FROM cce_marks_scho_type_skills_table
 INNER JOIN  cce_subject_type_skills_id_table 
 ON cce_subject_type_skills_id_table.sts_id=cce_marks_scho_type_skills_table.sts_id
WHERE cce_marks_scho_type_skills_table.term_test_id = $term_test_id AND cce_marks_scho_type_skills_table.student_id=".$sid." AND cce_marks_scho_type_skills_table.sts_id=".$sts_id." AND cce_subject_type_skills_id_table.skill_id=$skill_id  AND cce_subject_type_skills_id_table.subject_id=$subject_id AND cce_subject_type_skills_id_table.type_id!=1  GROUP BY cce_subject_type_skills_id_table.type_id ";
                                         $exe_marks = mysql_query($get_marks);
                                        $fetch_marks = mysql_fetch_array($exe_marks);
                                      // $ttid=$fetch_marks[0];
                                           $marksssss=$fetch_marks['marks'];
                   
                   echo '<td width=10% rowspan=""><select name="test_id" style="width:60px;" onchange="save_data('.$class_id.','.$subject_id.','.$term_id.','.$test_id.','.$sid.','.$type_id.','.$skill_id.')" id="td_'.$class_id.''.$subject_id.''.$term_id.''.$test_id.''.$sid.''.$type_id.''.$skill_id.'"><option value="0">'.$marksssss.'</option>
                      ';
                  
                 
                      // query to get remark in select box from cce_scho_remarks_table
                      $get_remark = "SELECT DISTINCT(grade),marks  FROM cce_scho_remarks_table WHERE marks!= ''";
                      $exe_remark = mysql_query($get_remark);
                      while($fetch_remark = mysql_fetch_array($exe_remark))
                      {
                      
                            echo '<option value="'.$fetch_remark['marks'].'">'.$fetch_remark['marks'].'</option>'; 

                      }
                 
                      echo '</select></td>';

                 }

                }
 
      echo '</tr>';
 }
    echo '</tbody>
           </table>
           </div>
           </div>
           </div>';  
              
  }
  
  
  
 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
  
   //////////asl marks//////
  
  function term_asl_marks()
  {
      $class_id=$_GET['class_id'];
      $term=$_GET['term_id'];
      echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">                    
<div class="widget-header">
 <span><i class="icon-home"></i>view students </span>
</div><!-- End widget-header -->	
<div class="widget-content">';  
                    echo '
<table class="table table-bordered table-striped"  border="2" width="100%"   >
<thead >
<tr>
<th rowspan="2" width=10%>Admission No.</th>
</tr>
<tr><th  align="center">Name</th><th>Grade</th></tr>

</thead>';

    echo '
           <tbody align="center">';
           //get student names on the class id
   
   $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$class_id."'
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {         $student_id=$fetch_students['sId'];
                     
                      $get_details_for_update=
"SELECT *
FROM cce_asl_marks_table
WHERE 
student_id = ".$student_id."
    AND class_id = ".$class_id."
AND session_id = ".$_SESSION['current_session_id']." AND term_id = ".$term."";
$exe_details=mysql_query($get_details_for_update);
$fetch_details=mysql_fetch_array($exe_details);
    $grades=$fetch_details['grade'];               
                     
                     
                     
                     
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td align="left" width="30%">'.$fetch_students['Name'].'</td>
             <td align="center">
<select  name="grade" id="grade"  style="width:100px;" onchange="insert_asl_marks('.$student_id.','.$class_id.',this.value,'.$term.');">
 <option value="0">'.$grades.'</option>
<option value="A1">A1</option>
<option value="A2">A2</option>
<option value="B1">B1</option>
<option value="B2">B2</option>
<option value="C1">C1</option>
<option value="C2">C2</option>
<option value="D">D</option>
<option value="E1">E1</option>
<option value="E2">E2</option>
<option value="AB">AB</option>
</select>     </td></tr>	  '; 
                     }     

                     
                     echo'</tbody></table>';  
      
      
      
      
                
            echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';
      
      
      
  }
 
 /////////new award list......................................................
  function grade_cce_new_award_list_details()
  {
   $current_session=$_SESSION['current_session_id'];
           $n=0;   
         echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">                    
<div class="widget-header">
 <span><i class="icon-home"></i>view students </span>
</div><!-- End widget-header -->	
<div class="widget-content">';   
         if(isset($_GET['assign']))
            {
	echo "<h4 style=\"color:green\" align=\"center\">Successfully Assign</h4>" ;
            }
                   
echo '
          <div class="section">	
          <form id="validation_demo" method="post" action="grades_cce/grade_cce_assign_activity_name.php"> 
        <div class="section">
<label> Mullti select <small>Text custom</small></label>   
 <div> 
  <select  class="chzn-select " multiple tabindex="10" name="activity1[]">
    <option value=""></option> ';
 $get_visitor_year="SELECT *
               FROM  `subject`
               ORDER BY subject ASC
                   ";
$execute_visitor_year=mysql_query($get_visitor_year);
while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
{   
         $subject=$fetch_visitor_year['subject'];

echo'
     <option value="'.$fetch_visitor_year[0].'">'. $subject.'</option>';
  }
  
      echo' 
</select>
</div>
</div>  
<div class="section">
<label> Mullti select <small>Text custom</small></label>   
 <div> 
  <select  class="chzn-select " multiple tabindex="10" name="activity2[]">
    <option value=""></option> ';

 echo'    
</select>
</div>
</div>
<div class="section">
<label> Mullti select <small>Text custom</small></label>   
 <div> 
  <select  class="chzn-select " multiple tabindex="10" name="activity3[]">
    <option value=""></option> ';
                                                              
while($fetch_execute_get_activy=mysql_fetch_array($execute_get_activy))//looping for getting year
{             
         $name=$fetch_execute_get_activy['1'];

echo'
     <option value="'.$fetch_execute_get_activy[0].'" >'. $name.'</option>';
    $n++;
  }
  
      echo' 
</select>
</div>
</div>
<div class="section last">
<div>
<a class="uibutton submit_form " >save</a>      
</div>
</div>          
<input type="hidden" name="max" value="'.$n.'"/>
<input type="hidden" name="session_id" value="'.$current_session.'"/>
 </form>                                          
            ';
               
            echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';	
         
  }

  /////////////////////////////////////////////////co-scholastic area////////////////////////////////////////////////////////////////////////////////////////
  
  
  function new_grade_cce_new_add_co_scholastic()
  {
      
    $current_session=$_SESSION['current_session_id'];
      echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Add Activity</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';     
      
           echo' 	                   
 <a class="uibutton btn-btn confirm" align="center"  type="button" href="new_grade_cce_coscho_add_indicator.php" >
 Add Indicator</a>
 <a class="uibutton btn-btn " align="center"  type="button" href="new_grade_cce_coscho_add_marks.php" >
 Add Marks</a>';    
        
      
      
          if(isset($_GET['added']))
            {
	echo "<h3 style=\"color:green\" align=\"center\">Successfully Added</h3>" ;
            }

echo '
                        <div class="section">	
                        <form id="validation_demo" method="get" action="grades_cce/new_grade_cce_add_cosho_fields.php"> 
                        <div class="section ">
                        <label>Level</label>   
                        <div> 
                        <select  name="level" data-placeholder="Choose  LEVEL..." class="chzn-select" tabindex="2" >
                        <option value=""></option>
                        <option value="0">Nursery</option>
                        <option value="1">Pre Primary</option>
                       <option value="2"> Primary</option>
                       <option value="3">Secondary</option>
                       <option value="4">Sr.Secondary</option>
                       </select>
                        </div>
                        </div>
	  
                        <div class="section "><label>Select Activity</label>   
                <div> 
          <select  data-placeholder="Choose  Area..." class="chzn-select" tabindex="2" name="area_id">
      <option value=""></option>
                        <option value="0">2(A)Life Skills</option>
                        <option value="1">2(B)Work Education </option>
                       <option value="2"> 2(C)Visual & Performing Arts</option>
                       <option value="3">2(D)Attitudes and Values</option>
                       <option value="4">3(A)Activity</option>
                         <option value="5">3(B)Health & Physical Education</option>
                           
                       </select>
                        </div>
                        </div>
   <input type="hidden" name="session_id" value="'.$current_session.'"/>
                        <div class="section ">
	  <label> Area of Assesment</label>   
	  <div> 
	  <input type="text" class="validate[required] mediam" autocomplete="off" name="coscho_name" id="f_required" >
	  </div> 
	  </div>
                           <div class="section last">
	  <div>
	  <a class="uibutton submit_form  icon add " > Add</a>
                         <a href="#" class="uibutton btn-btn confirm" >View </a>
	  </div>
	  </div>                         
               ';
         
         echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';	
           
  
  }
  
  ///add indicator
  function new_grade_cce_add_indicator()
  {
      
       $current_session=$_SESSION['current_session_id'];
      echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Add Activity</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';     
          if(isset($_GET['added']))
            {
	echo "<h3 style=\"color:green\" align=\"center\">Successfully Added</h3>" ;
            }

echo '
                        <div class="section">	
                        <form id="validation_demo" method="get" action="grades_cce/new_grade_cce_add_indicator_name.php"> 
                        <div class="section ">
                        <label>Level</label>   
                        <div> 
                        <select  name="level" data-placeholder="Choose  LEVEL..." class="chzn-select" tabindex="2" >
                        <option value=""></option>
                        <option value="0">Nursery</option>
                        <option value="1">Pre Primary</option>
                       <option value="2"> Primary</option>
                       <option value="3">Secondary</option>
                       </select>
                        </div>
                        </div>
	  
                        <div class="section "><label>Select Coscho Name</label>   
                <div> 
          <select  data-placeholder="Choose  Name..." class="chzn-select" tabindex="2" name="coscho_id">
        <option value=""></option>  ';
                        
$get_visitor_year="SELECT *
               FROM  `cce_coscho_fields`
               WHERE session_id=".$_SESSION['current_session_id']."
               ORDER BY cosho_name  ASC
                   ";
$execute_visitor_year=mysql_query($get_visitor_year);
while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
{   
         $subject=$fetch_visitor_year[2];

echo'
     <option value="'.$fetch_visitor_year[0].'">'. $subject.'</option>';
  }
 echo'    
</select>
</div>
</div>
   <input type="hidden" name="session_id" value="'.$current_session.'"/>
                        <div class="section ">
	  <label>Indicator Name</label>   
	  <div> 
	  <input type="text" class="validate[required] mediam" autocomplete="off" name="indicator_name" id="f_required" >
	  </div> 
	  </div>
                         <div class="section ">
	  <label>Grade</label>   
	  <div> 
	  <input type="text" class="mediam" autocomplete="off" name="grade"  >
	  </div> 
	  </div>
                           <div class="section last">
	  <div>
	  <a class="uibutton submit_form  icon add " > Add Indicator</a>
                         <a href="#" class="uibutton btn-btn confirm" >View Activity</a>
	  </div>
	  </div>                         
               ';
         
         echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';	
           

  }
  
  
  function new_grade_cce_add_marks_indicator()
  {
      
   echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';   
         echo'<form action="new_grade_cce_view_student_v_viii_class.php" method="get">';
          
 echo'<div class="section "> <label>Select Term</label>   
    <div> 
          <select  data-placeholder="Choose  Term..." class="chzn-select" tabindex="2" name="term">
        <option value=""></option>  ';
                        
$get_term="SELECT *
               FROM  `cce_term_table` 
               
                   ";
$execute_get_term=mysql_query($get_term);
while($fetch_term=mysql_fetch_array($execute_get_term))//looping for getting year
{   
         $term_name=$fetch_term['term_name'];

echo'
     <option value="'.$fetch_term[0].'">'. $term_name.'</option>';
  }
 echo'    
</select>
</div>
</div>';
   $get_cycle_test="SELECT *
               FROM  `cce_test_table`  WHERE  test_id>=5 AND test_id<=10
               
                   ";
  echo'<div class="section "> <label>Select  Test</label>   
    <div> 
          <select  data-placeholder="Choose  cycle Test..." class="chzn-select" tabindex="2" name="test">
        <option value=""></option>  ';
                        

$execute_get_cycle_test=mysql_query($get_cycle_test);
while($fetch_cycle_test=mysql_fetch_array($execute_get_cycle_test))//looping for getting year
{   
         $cycle_name=$fetch_cycle_test['test_name'];

echo'
     <option value="'.$fetch_cycle_test['test_id'].'">'.$cycle_name.'</option>';
  }
 echo'    
</select>
</div>
</div>';  
   
    echo'  <div class="section "> <label>Select Class</label>   
               <div> 
          <select  data-placeholder="Choose  Class..." class="chzn-select" tabindex="2" name="class" id="class">
        <option value=""></option>  ';
                        
$get_class="SELECT *
               FROM  `class_index` Where class >=5  AND class <=10
               ORDER BY class+0 ASC ,section ASC
                   ";
$execute_class=mysql_query($get_class);
while($fetch_class=mysql_fetch_array($execute_class))//looping for getting year
{   
         $class=$fetch_class['class_name'];

echo'
     <option value="'.$fetch_class[0].'">'. $class.'</option>';
  }
 echo'    
</select>
</div>
</div><div class="section last">
<div>
<button type ="submit"  class="uibutton submit">Submit</button>  
</div>
</div></form>';
   
   
   
   
   
   
   
//   
//    echo'      <ol class="rounded-list">';
//      $get_class=
//          "SELECT *
//          FROM class_index
//          WHERE class  >=5 AND class<=10
//          ORDER BY class+0 ASC ,section ASC  ";
//          $exe_get_class=mysql_query($get_class);
//          while($fetch_classes=mysql_fetch_array($exe_get_class))
//          {
//                echo '<li><a href="new_grade_cce_view_student_v_viii_class.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';
//          } 
       echo'  	                       
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';
       
  }
  
  function new_grade_cce_view_student_marks_indicator()
  {
      
       $class_id=$_GET['class'];
         $term_id=$_GET['term'];
           $test_id=$_GET['test'];
    //get name of the class for that id
	$get_class_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$class_id."";
	$exe_clas_name=mysql_query($get_class_name);
	$fetch_class_name=mysql_fetch_array($exe_clas_name);
	$name_class=$fetch_class_name[0];	
echo '	 <div class="row-fluid">              
                        <!-- Table widget -->
                        <div class="widget  span12 clearfix">                           
                            <div class="widget-header">
                                <span><i class="icon-home"></i></span>
                            </div><!-- End widget-header -->	                            
                            <div class="widget-content">';
               echo '
                            <h5 style="color:grey" align="center">Class:'.$name_class.'</h5>   ' ;
                           
               echo '
                        <div class="section">	
                        <form id="validation_demo"> 
                        <div class="section "><label>Select Co-scholastic Area</label>   
                <div> 
          <select  data-placeholder="Choose  Area..." class="chzn-select" tabindex="2" name="area_id" onchange="new_show_coscho(this.value,'.$class_id.','.$term_id.','.$test_id.');">
      <option value=""></option>
                        <option value="0">2(A)Life Skills</option>
                        <option value="1">2(B)Work Education </option>
                       <option value="2"> 2(C)Visual & Performing Arts</option>
                       <option value="3">2(D)Attitudes and Values</option>
                       <option value="4">3(A)Activity</option>
                         <option value="5">3(B)Health & Physical Education</option> 
                       </select>
                        </div>
                        </div><span id="type_coscho"></span>
                        <span id="student_coscho"></span>';
              
 

    echo '</tbody>
           </table>
           </div>
           </div>
           </div>';  
      
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  /////////////////////PRIVILEGE TEACHER//////////////////////////////////////////TEACHER////////////////////////////
  
  function new_grade_cce_add_marks_sholastic_teacher()
  {
        
     echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Add Marks</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';        
           
  echo' 	                   
       <a class="uibutton btn-btn confirm" align="center"  type="button" href="grade_cce_nursery_add_activity.php" >Nursery To Pre-Primary</a>  
     
       ';
       echo'<form id="validation_demo" method ="get" action ="new_grade_cce_teacher_subject_class.php">';
             
 echo'<div class="section "> <label>Select Term</label>   
    <div> 
          <select  data-placeholder="Choose  Term..." class="chzn-select" tabindex="2" name="term" id="term">
        <option value=""></option>  ';
                        
$get_term="SELECT *
               FROM  `cce_term_table` 
               
                   ";
$execute_get_term=mysql_query($get_term);
while($fetch_term=mysql_fetch_array($execute_get_term))//looping for getting year
{   
         $term_name=$fetch_term['term_name'];

echo'
     <option value="'.$fetch_term[0].'">'. $term_name.'</option>';
  }
 echo'    
</select>
</div>
</div>
  <div class="section "> <label>Select Class</label>   
               <div> 
          <select  data-placeholder="Choose  Class..." class="chzn-select" tabindex="2" name="class" id="class" onchange="select_test(this.value '.$fetch_term[0].')">
        <option value=""></option>  ';
     
//	//query to get name of all the classes
//	$get_name_classes=
//	"SELECT DISTINCT teachers.class,class_index.cId
//	FROM teachers
//	INNER JOIN class_index
//	ON class_index.class_name = teachers.class
//	WHERE tId = ".$_SESSION['user_id']."  ORDER BY  class_index.class+0 ASC";
//	$exe_class_name=mysql_query($get_name_classes);
	                   
$get_class="SELECT DISTINCT teachers.class,class_index.*
	FROM teachers
	INNER JOIN class_index
	ON class_index.class_name = teachers.class
	WHERE tId = ".$_SESSION['user_id']."  ORDER BY  class_index.class+0 ASC";
$execute_class=mysql_query($get_class);
while($fetch_class=mysql_fetch_array($execute_class))//looping for getting year
{   
         $class=$fetch_class['class_name'];

echo'
     <option value="'.$fetch_class['cId'].'">'. $class.'</option>';
  }
 echo'    
</select>
</div>
</div><span id="show_test"></span>
  <div class="section last">
<div>
 <a class="uibutton submit_form "  type="submit"  >
GO</a>
</div>
</div> 

</form>';
       echo'  	                       
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';
                     
        
        
  }
  
  
  function new_grade_cce_sholastic_subject_teacher()
  {
   
        $class_id=$_GET['class'];  
             $term_id=$_GET['term'];   
              $test_id=$_GET['test'];  
                $teacher_id = $_SESSION['user_id'];
    
echo '  <div class="row-fluid">

<!-- Table widget -->
<div class="widget  span12 clearfix">

<div class="widget-header">
<span><i class="icon-home"></i>Choose subject to view subject activities and marks</span>
</div><!-- End widget-header -->	

<div class="widget-content">';
								
	
//get subject id from teachers
 $get_id_subject = 
"SELECT DISTINCT subject.subject , subject.subject_id
FROM subject
INNER JOIN cce_subject_type_skills_id_table

ON cce_subject_type_skills_id_table.subject_id = subject.subject_id

INNER JOIN  teachers
ON  teachers.subject_id=cce_subject_type_skills_id_table.subject_id
INNER JOIN class_index
	ON class_index.class_name = teachers.class
WHERE cce_subject_type_skills_id_table.class_id =$class_id AND teachers.tId = $teacher_id";
$exe_subjects=mysql_query($get_id_subject);
echo '<div class="span4">
';
while($fetch_subjects=mysql_fetch_array($exe_subjects))
{

echo ' <a href="new_grade_cce_add_marks_all_students_class.php?class_id='.$_GET['class'].'&subject_id='.$fetch_subjects[1].'&term_id='.$term_id.'& test_id='.$test_id.'"><div class="alertMessage inline info">'.$fetch_subjects[0].'</div></a> 
';
}
	
        
       
      echo'  	                       
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';
                        
        
        
  }
  
  
  
  
  
  
  
  function  new_grade_cce_add_marks_co_sholastic_teacher()
  {
   echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Add Marks</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';        
        
        echo'<form id="validation_demo" method ="get" action ="new_grade_cce_add_marks_co_scholastic_teacher.php">';
             
 echo'<div class="section "> <label>Select Term</label>   
    <div> 
          <select  data-placeholder="Choose  Term..." class="chzn-select" tabindex="2" name="term" id="term">
        <option value=""></option>  ';
                        
$get_term="SELECT *
               FROM  `cce_term_table` 
               
                   ";
$execute_get_term=mysql_query($get_term);
while($fetch_term=mysql_fetch_array($execute_get_term))//looping for getting year
{   
         $term_name=$fetch_term['term_name'];

echo'
     <option value="'.$fetch_term[0].'">'. $term_name.'</option>';
  }
 echo'    
</select>
</div>
</div>
  <div class="section "> <label>Select Class</label>   
               <div> 
          <select  data-placeholder="Choose  Class..." class="chzn-select" tabindex="2" name="class" id="class" onchange="select_test(this.value '.$fetch_term[0].')">
        <option value=""></option>  ';
     
//	//query to get name of all the classes
//	$get_name_classes=
//	"SELECT DISTINCT teachers.class,class_index.cId
//	FROM teachers
//	INNER JOIN class_index
//	ON class_index.class_name = teachers.class
//	WHERE tId = ".$_SESSION['user_id']."  ORDER BY  class_index.class+0 ASC";
//	$exe_class_name=mysql_query($get_name_classes);
	                   
$get_class="SELECT DISTINCT teachers.class,class_index.*
	FROM teachers
	INNER JOIN class_index
	ON class_index.class_name = teachers.class
	WHERE tId = ".$_SESSION['user_id']."  ORDER BY  class_index.class+0 ASC";
$execute_class=mysql_query($get_class);
while($fetch_class=mysql_fetch_array($execute_class))//looping for getting year
{   
         $class=$fetch_class['class_name'];

echo'
     <option value="'.$fetch_class['cId'].'">'. $class.'</option>';
  }
 echo'    
</select>
</div>
</div><span id="show_test"></span>
  <div class="section last">
<div>
 <a class="uibutton submit_form "  type="submit"  >
GO</a>
</div>
</div> 

</form>';
       echo'  	                       
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';
                     
        
             
        
        
    
        
  }
  
  
  function add_marks_co_scholastic_teacher()
  {
        
   
        $flag=1;
        
         $class_id=$_GET['class'];
        $term_id=$_GET['term'];
        $test_id=$_GET['test'];
        
  
   //get name of the class for that id
	$get_class_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$class_id."";
	$exe_clas_name=mysql_query($get_class_name);
	$fetch_class_name=mysql_fetch_array($exe_clas_name);
	$name_class=$fetch_class_name[0];	
echo '	 <div class="row-fluid">              
                        <!-- Table widget -->
                        <div class="widget  span12 clearfix">                           
                            <div class="widget-header">
                                <span><i class="icon-home"></i></span>
                            </div><!-- End widget-header -->	                            
                            <div class="widget-content">';
               echo '
                            <h5 style="color:grey" align="center">Class:'.$name_class.'</h5>   ' ;
                           
            
  
  
        
        $get_class="SELECT *
	FROM class_index
	
	WHERE class >=5 ";
$execute_class=mysql_query($get_class);
while($fetch_class=mysql_fetch_array($execute_class))
{
      $level=$fetch_class['level'];
	//$nurs_level=$level-2;
	  
  $cid=$fetch_class[0];
  
  if($cid==$class_id)
  {
        
        $flag=0;
        
  }
    
}
        

if($flag==0)
{
     
        echo '
                        <div class="section">	
                        <form id="validation_demo"> 
                        <div class="section "><label>Select Co-scholastic Area</label>   
                <div> 
          <select  data-placeholder="Choose  Area..." class="chzn-select" tabindex="2" name="area_id" onchange="new_show_coscho(this.value,'.$class_id.','.$term_id.','.$test_id.');">
      <option value=""></option>
                        <option value="0">2(A)Life Skills</option>
                        <option value="1">2(B)Work Education </option>
                       <option value="2"> 2(C)Visual & Performing Arts</option>
                       <option value="3">2(D)Attitudes and Values</option>
                       <option value="4">3(A)Activity</option>
                         <option value="5">3(B)Health & Physical Education</option> 
                       </select>
                        </div>
                        </div><span id="type_coscho"></span>
                        <span id="student_coscho"></span>';
              
 

    echo '</tbody>
           </table>
           </div>
           </div>
           </div>';  
  
}


//////////class..........1  to v class......................................................
     else if($flag==1)
       {
       //  $flag=3;
        $get_class="SELECT DISTINCT level
	FROM class_index WHERE cId=$class_id
	
	 ";
$execute_class=mysql_query($get_class);
while($fetch_level=mysql_fetch_array($execute_class))
{
$leve_c=$fetch_level['level'];
if($leve_c<2)
{
 $flag=2;
}
else{
$flag=3;
}
}
if($flag==3)
       {


         echo'<form action="grades_cce/new_cce_insert_co_coscho_marks_i_iv.php" method="get"><input type="hidden" name="class" id="class" value="'.$class_id.'"/>';			 
$get_class="SELECT * 
               FROM `cce_co_scho_activity_table` WHERE level=2
                   ";
          echo'  <div class="section "> <label>Select Acivity</label>   
               <div> 
          <select  data-placeholder="Choose  Activity..." class="chzn-select" tabindex="2" name="co_activity"  id="co_activity"  onchange="select_coscho_skill('.$term_id.','.$test_id.');">
        <option value=""></option>  ';
 
$execute_class=mysql_query($get_class);
while($fetch_class=mysql_fetch_array($execute_class))//looping for getting year
{   
         $class=$fetch_class['co_activity_name'];

echo'
     <option value="'.$fetch_class[0].'">'. $class.'</option>';
  }
 echo'    
</select>
</div>
</div><span id="show_co_activity"></span> <span id="show_student"></span> ';
        
    
                     
            
  
  }
  elseif(($flag==2))
  {      echo'<form action="grades_cce/new_cce_insert_co_coscho_marks_i_iv.php" method="get"><input type="hidden" name="class" id="class" value="'.$class_id.'"/>';
   echo'  <div class="section "> <label>Select Acivity</label>   
               <div> 
          <select  data-placeholder="Choose  Activity..." class="chzn-select" tabindex="2" name="co_activity"  id="co_activity"  onchange="select_coscho_skill('.$term_id.','.$test_id.');">
        <option value=""></option>  ';
                        
$get_class="SELECT * 
               FROM `cce_co_scho_activity_table` WHERE level<2
                   ";
$execute_class=mysql_query($get_class);
while($fetch_class=mysql_fetch_array($execute_class))//looping for getting year
{   
         $class=$fetch_class['co_activity_name'];

echo'
     <option value="'.$fetch_class[0].'">'. $class.'</option>';
  }
 echo'    
</select>
</div>
</div><span id="show_co_activity"></span> <span id="show_student"></span> ';
        
    
                     
    }   
}	
   echo'  	                       
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';
  }
  
  
  
  ///////////////a3 report//////////////////////////////////////////////
  
  function grade_cce_a3_report_class()
  {
      
       echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Activity details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';       
            
              
    echo'      <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
        
         ";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grade_cce_a3_report_view_student_class.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
                  
  
  }
  function   grade_cce_a3_report_student_class()
  {
      
        $class_id=$_GET['id_class'];
    //get name of the class for that id
	$get_class_name=
	"SELECT class_name
	FROM class_index
	WHERE cId=".$class_id."";
	$exe_clas_name=mysql_query($get_class_name);
	$fetch_class_name=mysql_fetch_array($exe_clas_name);
	$name_class=$fetch_class_name[0];	
echo '	 <div class="row-fluid">              
                        <!-- Table widget -->
                        <div class="widget  span12 clearfix">                           
                            <div class="widget-header">
                                <span><i class="icon-home"></i></span>
                            </div><!-- End widget-header -->	                            
                            <div class="widget-content">';
               echo '
                            <h5 style="color:grey" align="center">Class:'.$name_class.'</h5>   ' ;
                              
              echo '
<table class="table table-bordered table-striped"  border="2" width="100%"  id="dataTable">
<thead >
<tr>
<th rowspan="2" width=10%>Admission No.</th>
</tr>
<tr><th  colspan="6">  Name</th></tr>

</thead>';

    echo '
           <tbody align="center">';
           //get student names on the class id
   
   $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$_GET['id_class']."'
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td><a href="grades_cce/grade_cce_view_student_full_a3_report_i_iv.php?student_id='.$fetch_students['sId'].'&&class_id='.$_GET['id_class'].'">'.$fetch_students['Name'].'</a></td>
                </tr>	
                    '; 
                     }
          
    echo '</tbody>
           </table>
           </div>
           </div>
           </div>';         
  }
  
  
  
  ////green shee report deatkkl///////////////////////////////////////////////////////
  
  function grade_cce_green_sheet_class()
  {
      
  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
           echo'      <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          WHERE  class >=1 AND class <=12
         ";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grades_cce/grade_cce_view_green_sheet_marks_xi.php?class_id='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
                      
      
  }
  
  
  
  function grade_cce_full_green_sheet_class()
  {
      
  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
           echo'      <ol class="rounded-list">
          ';

  
      $get_class = "SELECT * FROM class_index WHERE  level = 2";

      $exe_get_class=mysql_query($get_class);

          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grades_cce/grade_cce_green_sheet_view_full_report.php?class_id='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
                      
      
  }
  
  
  
  //////////////////////////////////////////////////////////////////////////////////////////////classs xi   xii    ///////////////////////////////////////////////////
  
  
  function grade_cce_xi_xii_dashboard()
  {
      
           
  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">(XI XII)details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
         if(isset($_GET['add']))
            {
	echo "<h3 style=\"color:green\" align=\"center\">Successfully Added</h3>" ;
            }
                if(isset($_GET['err']))
            {
	echo "<h3 style=\"color:red\" align=\"center\">Successfully Added</h3>" ;
            }
            if(isset($_GET['stream_assigned']))
            {
	echo "<h3 style=\"color:red\" align=\"center\">Successfully Added</h3>" ;
            }
            if(isset($_GET['error_stream_assigned']))
            {
	echo "<h3 style=\"color:red\" align=\"center\">ERROR</h3>" ;
            }
    
            if(isset($_GET['test_added']))
            {
	echo "<h3 style=\"color:red\" align=\"center\">Successfully Added</h3>" ;
            }
            if(isset($_GET['error_in_test_adding']))
            {
	echo "<h3 style=\"color:red\" align=\"center\">ERROR</h3>" ;
            }
            if(isset($_GET['test_assigned']))
            {
	echo "<h3 style=\"color:red\" align=\"center\">Successfully Added</h3>" ;
            }
            if(isset($_GET['error_test_assigning']))
            {
	echo "<h3 style=\"color:red\" align=\"center\">ERROR</h3>" ;
            }
             if(isset($_GET['add_exam']))
            {
	echo "<h3 style=\"color:red\" align=\"center\">ERROR</h3>" ;
            }
            
 //button to add marks 
            
            echo '<a class="uibutton btn-btn " align="center"  type="button" href="new_grade_cce_select_class_subject.php" >
 Add Marks</a>';

            
//BUTTON TO ADD THE STREAM
echo '<form method ="get" action ="grades_cce/cce_add_stream_xi_xii_class.php" id="1">';

echo '
<a class="alertMessage inline info" id="flip">ADD STREAM</a>
';
echo '<div id="panel" align="left" style="background-color:aliceblue">
';
//to enter name of activity
echo '
<div class="section">
<label style="color:grey;">NAME OF STREAM</label>
<div>
<input type="text" name="stream"/><br>
<input type ="submit" name ="add_stream" class="uibutton submit"></button> 
</div>
</div>
 <h6 style="background-color:chocolate; font-family:arial;color:white;font-size:12px;">LIST OF STREAM EXIST</h6>
 <table width="100%" id="ajax_delete_term">
 <thead>
 <tr style="background-color:gold; font-family:arial;color:black;font-size:12px;">
 <th>Stream Name </th><th>Action</th>
 </tr>
 </thead><tbody>';
// query to get the term available in database fro cce_term_table
$get_term = "SELECT * FROM cce_stream_table";
$exe_term = mysql_query($get_term);
while($fetch_term = mysql_fetch_array($exe_term))
{
   echo ' <tr style="background-color:gold; font-family:arial;color:black;font-size:12px;" id="term_'.$fetch_term['id'].'">
            <td>'.$fetch_term['stream_name'].'</td><td id="edit_term_'.$fetch_term['_id'].'" class=" "><span><a original-title="Edit"><img src="images/icon/icon_edit.png" onclick="edit_term('.$fetch_term[0].',\''.$fetch_term[1].'\');" title="Edit"></a></span><a original-title="Edit">
<span class="tip"></span></a><a data-name="delete name" original-title="Delete"><img src="images/icon/icon_delete.png" onclick="delete_term('.$fetch_term[0].');" title="Delete"></a>
</td>

         </tr>';
}


echo'
 <tbody></table>

</div>';



echo '</form>';





//BUTTON TO ASSIGN THE STREAM TO CLASSES
//query to select class xii-xi
$class=
        "
           SELECT * 
           FROM `class_index`
           WHERE `class` BETWEEN '11' AND '12'
           
           

";
$exe_class=  mysql_query($class);

// query to get the streams available in database 

$get_stream = "SELECT * FROM cce_stream_table";
$exe_stream = mysql_query($get_stream);

echo '<form method ="get" action ="grades_cce/assign_stream_xi_xii.php" id="2">
      

<a class="alertMessage inline info" id="flip1">ASSIGN STREAM</a>

';
echo '<div id="panel1" align="left" style="background-color:aliceblue">
';
echo '
<div class="section">
<label style="color:grey;">NAME OF CLASS</label>
<div>
<select name="class_name[]" class="chzn-select " multiple tabindex="4">';

while($fetch_class=  mysql_fetch_array($exe_class))
{
//   $query="
//      SELECT class_id
//      FROM `cce_stream_assignment_xi_xii`
//      ";
//   $exe_query=  mysql_query($query);
//   while($fetch_query=  mysql_fetch_array($exe_query))
//   {  if(mysql_num_rows($fetch_query==0))
//      //if($fetch_query[0]!=$fetch_class['cId'])
//      {
   echo'<option  value="'.$fetch_class['class_name'].'">'.$fetch_class['class_name'].'
      </option>
      
      
      ';
   
      
   
}
echo'
   </select>
   </div>
<br><br>
<label style="color:grey;">STREAM NAME</label>
<div>
 <select name="stream_name" data-placeholder="Choose a Stream..." class="chzn-select" tabindex="2" >
 <option></option>';
 while($fetch_stream = mysql_fetch_array($exe_stream))
 {
    echo'
       <option name="stream_name" value="'.$fetch_stream['stream_name'].'">'.$fetch_stream['stream_name'].'
       </option>
       
';
 }
echo'
   </select>
   </div><br><br>
<input type ="submit" name ="assign_stream" class="uibutton submit"></button> 
</div>
';
 echo '<h6 style="background-color:chocolate; font-family:arial;color:white;font-size:12px;"> CLASSES WITH THEIR STREAMS </h6>
 <table width="100%" id="ajax_delete_term">
 <thead>
 <tr style="background-color:gold; font-family:arial;color:black;font-size:12px;">
 <th>Class Name </th>
 <th>Stream Name </th>
 <th>Action</th>
 </tr>
 </thead><tbody>';
// query to get the term available in database fro cce_term_table
$get_details = "SELECT * FROM cce_stream_assignment_xi_xii";
$exe_details = mysql_query($get_details);
while($fetch_details = mysql_fetch_array($exe_details))
{
   //query to get stream name
   $query_stream_name=
           "
             SELECT `stream_name`
             FROM `cce_stream_table`
             WHERE `id`=".$fetch_details['stream_id']."
";
   $exe_query_stream_name=  mysql_query($query_stream_name);
   $fetch_stream_name=  mysql_fetch_array($exe_query_stream_name);
   
   //query to get class name
   $query_class_name=
           "
            SELECT `class_name`
            FROM `class_index`
            WHERE `cId`=".$fetch_details['class_id']."
";
   $exe_query_class_name=  mysql_query($query_class_name);
   $fetch_class_name=  mysql_fetch_array($exe_query_class_name);

   echo ' <tr style="background-color:gold; font-family:arial;color:black;font-size:12px;" id="assign_'.$fetch_details['id'].'">
            <td>'.$fetch_class_name['class_name'].'</td><td>'.$fetch_stream_name['stream_name'].'</td><td id="edit_stream_'.$fetch_details['id'].'" class=" "><span><a original-title="Edit"><img src="images/icon/icon_edit.png" onclick="edit_stream_assignment(\''.$fetch_class_name[0].'\',\''.$fetch_stream_name[0].'\','.$fetch_details['id'].');" title="Edit"></a></span><a original-title="Edit">
<span class="tip"></span></a><a data-name="delete name" original-title="Delete"><img src="images/icon/icon_delete.png" onclick="delete_stream_assignment('.$fetch_details[0].');" title="Delete"></a>
</td>

         </tr>';
}


echo'
 <tbody></table>



</div></form>';




//BUTTON FOR ALLOCATION OF THE TERM TO TERM ON CLASS
echo '<form method ="get" action ="grades_cce/add_term_xi_xii.php" id="6">
      <input type="hidden" name="fa" value="6"';
echo '<tr>

<a class="alertMessage inline info" id="flip6">Add Term </a>';
echo '<div id="panel6" align="left" style="background-color:aliceblue">


<div class="section">
<label style="color:grey">Name of term</label>
<div>
<input type="text" name="name_of_term" required="true"/>
</div>
</div>
';


echo'<div class="section last">
<div>
<button type ="submit" name ="submit_test" class="uibutton submit">Submit</button>
</div></div>  


<h6 style="background-color:chocolate; font-family:arial;color:white;font-size:12px;">LIST OF TERM AVAILABLE</h6>
 <table width="100%" id="ajax_delete_test" border="2">
 <thead>
 <tr style="background-color:gold; font-family:arial;color:black;font-size:12px;">
 <th>Term Name </th><th>Action</th>
 </tr>
 </thead><tbody>';
// query to get the test available in database from cce_test_table
$get_term = "SELECT * FROM add_term_xi_xii_table";
$exe_term = mysql_query($get_term);
while($fetch_term = mysql_fetch_array($exe_term))
{
   $term_id=$fetch_term['term_id'];
   echo ' <tr style="background-color:gold; font-family:arial;color:black;font-size:12px;" id="test_xi_xii_'.$fetch_term['term_id'].'">
            <td>'.$fetch_term['term_name'].'</td><td id="edit_term_'.$fetch_term['term_id'].'" class=" "><span><a original-title="Edit"><img src="images/icon/icon_edit.png" onclick="edit_term_xi_xii('.$term_id.',\''.$fetch_term[1].'\');" title="Edit"></a></span><a original-title="Edit">
<span class="tip"></span></a><a data-name="delete name" original-title="Delete"><img src="images/icon/icon_delete.png" onclick="delete_test_xi_xii('.$term_id.');" title="Delete"></a>
</td>

         </tr>';
}


echo' <tbody></table>';

echo '</div></form>';

//BUTTON FOR ALLOCATION OF THE TERM TO TEST ON CLASS
echo '<form method ="get" action ="grades_cce/add_test_xi_xii.php" id="3">
      <input type="hidden" name="fa" value="3">';
echo '<tr>

<a class="alertMessage inline info" id="flip2">Add Test </a>';
echo '<div id="panel2" align="left" style="background-color:aliceblue">


<div class="section">
<label style="color:grey">Name of Test</label>
<div>
<input type="text" name="name_of_test" required="true"/>
</div>
</div>
';


//to enter max marks
echo '
<div class="section">
<label style="color:grey">Max marks</label>
<div>
<input type="text" name="max_marks" style="width: 80px" required="true"/>
</div>
</div>
<div class="section last">
<div>
<button type ="submit" name ="submit_test" class="uibutton submit">Submit</button>
</div></div>  


<h6 style="background-color:chocolate; font-family:arial;color:white;font-size:12px;">LIST OF TEST AVAILABLE</h6>
 <table width="100%" id="ajax_delete_test" border="2">
 <thead>
 <tr style="background-color:gold; font-family:arial;color:black;font-size:12px;">
 <th>Test Name </th><th>Max Marks </th><th>Action</th>
 </tr>
 </thead><tbody>';
// query to get the test available in database from cce_test_table
$get_test = "SELECT * FROM add_test_xi_xii";
$exe_test = mysql_query($get_test);
while($fetch_test = mysql_fetch_array($exe_test))
{
   echo ' <tr style="background-color:gold; font-family:arial;color:black;font-size:12px;" id="test_xi_xii_'.$fetch_test['id'].'">
            <td>'.$fetch_test['test_name'].'</td><td>'.$fetch_test['max_marks'].'</td><td id="edit_term_'.$fetch_test['id'].'" class=" "><span><a original-title="Edit"><img src="images/icon/icon_edit.png" onclick="edit_test_xi_xii('.$fetch_test[0].',\''.$fetch_test[1].'\','.$fetch_test['max_marks'].');" title="Edit"></a></span><a original-title="Edit">
<span class="tip"></span></a><a data-name="delete name" original-title="Delete"><img src="images/icon/icon_delete.png" onclick="delete_test_xi_xii('.$fetch_test[0].');" title="Delete"></a>
</td>

         </tr>';
}


echo' <tbody></table>';

echo '</div></form>';
	  



//BUTTON FOR ALLOCATION OF THE TERM TO TEST ON CLASS


$get_stream = "SELECT * FROM cce_stream_table";
$exe_stream = mysql_query($get_stream);

echo '<form method ="get" action ="grades_cce/allocate_tests_to_classes.php" id="4">
      ';
echo '<tr>

<a class="alertMessage inline info" id="flip4">Assign Test Type</a>';
echo '<div id="panel4" align="left" style="background-color:aliceblue">
';
//to select class

echo '<div class="section">
<label style="color:grey">Select Class</label>
<div>
<select name="class" data-placeholder="Choose a Class..." class="chzn-select" tabindex="2" >


';
//query to fetch classes from the database
$sql="SELECT *
               FROM  class_index
               WHERE class BETWEEN 11 AND 12               
";
$exe_class=mysql_query($sql);
echo '<option value=""></option>';
while($fetch_class=mysql_fetch_array($exe_class))
{
    echo '<option value="'.$fetch_class['cId'].'">'.$fetch_class['class_name'].'</option>
	';
}
echo '</select>
</div>
</div><br>';
//to select the term from cce_term_table

 $get_term="SELECT * FROM add_term_xi_xii_table";
$execute_term=mysql_query($get_term);
//$result_term=mysql_fetch_array($execute_term);



echo '<div class="section">
<label style="color:grey">Select Term</label>
<div>
<select name="term" data-placeholder="Choose a Term..." class="chzn-select" tabindex="2" >
<option value=""></option>';
while($term=mysql_fetch_array($execute_term))
{ 
    echo '<option value="'.$term[0].'">'.$term['term_name'].'</option>'; 


}


echo '</select>
</div>
</div><br>';





$sql_test="SELECT *
      FROM  add_test_xi_xii
   
         ";

$result_test = mysql_query($sql_test);
echo '
<div class="section">
<label style="color:grey">Select Test</label>
<div>
<select name="test" data-placeholder="Choose test..." class="chzn-select" tabindex="2" >


<option value=""></option>



';
while($row1 = mysql_fetch_array($result_test))
  {
    echo '<option value="'.$row1[0].'">'.$row1['test_name'].'</option>'; 
 
  }

echo '</select>
</div>
</div>
';


//to enter max marks
echo '

<div class="section last">
<div>
<button type ="submit" name ="submit_allocate_test_term" class="uibutton submit">Allocate</button>  
</div>
</div>


<h6 style="background-color:chocolate; font-family:arial;color:white;font-size:12px;">LIST OF ALLOCATED CLASS TESTS</h6>
 <table class="table table-bordered" id="dataTable" width="100%" border="2">

 <thead>
 <tr style="background-color:gold; font-family:arial;font-size:12px;">
<th>Class Name </th><th>Term Name </th> <th>Test Name </th><th>Action</th>
 </tr>
 </thead><tbody>';
// query to get the allocated class test & term
$get_allocated = "SELECT * FROM assign_test_xi_xii";
$exe_allocated = mysql_query($get_allocated);
while($fetch_allocated = mysql_fetch_array($exe_allocated))
{   $term_idd=$fetch_allocated['term_id'];
   // query to get the name of class
   $get_cls = "SELECT class_name FROM class_index WHERE cId = ".$fetch_allocated['class_id']."";
   $exe_cls = mysql_query($get_cls);
   $fetch_cls_name = mysql_fetch_array($exe_cls);
   
   //query to get term name from cce_term_table
    $get_term_name="SELECT * FROM add_term_xi_xii_table WHERE term_id=$term_idd";
   $exe_term_name=  mysql_query($get_term_name);
   $fetch_term_name=  mysql_fetch_array($exe_term_name);
      $term_name=$fetch_term_name['term_name'];
   //query to get the test name fron cce_test_table
   $get_test_name = "SELECT * FROM add_test_xi_xii WHERE id=".$fetch_allocated['test_id']."";
   $exe_test_name = mysql_query($get_test_name);
   $fetch_test_name = mysql_fetch_array($exe_test_name);
   
   echo ' <tr style="background-color:gold; font-family:arial;font-size:12px;" id="allocated_xi_xii_'.$fetch_allocated['id'].'">
            <td>'.$fetch_cls_name['class_name'].'</td><td>'.$term_name.'</td><td>'.$fetch_test_name['test_name'].'</td><td id="edit_term_'.$fetch_test_name['id'].'" class=" "><a data-name="delete name" original-title="Delete"><img src="images/icon/icon_delete.png" onclick="delete_allocated_xi_xii('.$fetch_allocated[0].');" title="Delete"></a>
</td>

         </tr>';
}


echo' <tbody></table>';



echo '</div></form>';
	  
     	//BUTTON FOR ALLOCATION OF THE TERM TO TEST ON CLASS
echo '<form method ="get" action ="grades_cce/add_exam_type_xi_xii.php" id="5">
      <input type="hidden" name="fa" value="5"> ';

echo '<tr>

<a class="alertMessage inline info" id="flip5">Add Exam Type</a>';
echo '<div id="panel5" align="left" style="background-color:aliceblue">';

echo '<div class="section">
<label style="color:grey">Select Class</label>
<div>
<select name="class_id" data-placeholder="Choose a Class..." class="chzn-select" tabindex="2" >


';
//query to fetch classes from the database
$sql="SELECT *
               FROM  class_index
               WHERE class BETWEEN 11 AND 12               
";
$exe_class=mysql_query($sql);
echo '<option value=""></option>';
while($fetch_class=mysql_fetch_array($exe_class))
{
    echo '<option value="'.$fetch_class['cId'].'">'.$fetch_class['class_name'].'</option>
	';
}
echo '</select>
</div>
</div><br>';
echo '<div class="section">
<label style="color:grey">Select Subject</label>
<div>
<select name="subject_id" data-placeholder="Choose a Subject..." class="chzn-select" tabindex="2" >


';
//query to fetch classes from the database
$sql="SELECT *
               FROM  subject
                            
";
$exe_class=mysql_query($sql);
echo '<option value=""></option>';
while($fetch_class=mysql_fetch_array($exe_class))
{
    echo '<option value="'.$fetch_class[0].'">'.$fetch_class['subject'].'</option>
	';
}
echo '</select>
</div>
</div><br>

<div class="section">
<label style="color:grey">Exam Type</label>
<div>
<input type="text" name="exam_type" required="true"/>
</div>
</div>
';


//to enter max marks
echo '
<div class="section">
<label style="color:grey">Max marks</label>
<div>
<input type="text" name="max_marks" style="width: 80px" required="true"/>
</div>
</div>
<div class="section last">
<div>
<button type ="submit"  class="uibutton submit">Submit</button>
</div></div>  


<h6 style="background-color:chocolate; font-family:arial;color:white;font-size:12px;">LIST OF TEST AVAILABLE</h6>
 <table width="100%" id="ajax_delete_test" border="2">
 <thead>
 <tr style="background-color:gold; font-family:arial;color:black;font-size:12px;">
 <th>Test Name </th><th>Max Marks </th><th>Class</th><th>Subject</th><th>Action</th>
 </tr>
 </thead><tbody>';
// query to get the test available in database from cce_test_table
 $get_test = "SELECT cce_exam_type_xi_xii.*    ,class_index.*,subject.*   FROM cce_exam_type_xi_xii 
        INNER JOIN class_index ON class_index.cId=cce_exam_type_xi_xii.class_id
        INNER JOIN subject ON subject.subject_id=cce_exam_type_xi_xii.subject_id        


";
$exe_test = mysql_query($get_test);
while($fetch_test = mysql_fetch_array($exe_test))
{
   echo ' <tr style="background-color:gold; font-family:arial;color:black;font-size:12px;" id="test_xi_xii_'.$fetch_test[0].'">
            <td>'.$fetch_test['exam_type'].'</td><td>'.$fetch_test['max_marks'].'</td><td>'.$fetch_test['class_name'].'</td><td>'.$fetch_test['subject'].'</td><td id="edit_term_'.$fetch_test[0].'" class=" "><span><a original-title="Edit"><img src="images/icon/icon_edit.png" onclick="edit_exam_type_xi_xii('.$fetch_test[0].',\''.$fetch_test[1].'\','.$fetch_test['max_marks'].');" title="Edit"></a></span><a original-title="Edit">
<span class="tip"></span></a><a data-name="delete name" original-title="Delete"><img src="images/icon/icon_delete.png" onclick="delete_exam_type_xi_xii('.$fetch_test[0].');" title="Delete"></a>
</td>

         </tr>';
}


echo' <tbody></table>';

echo '</div></form>';
  
      
      
      
       echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
  
  }
  
 
  function excel_award_list_class()
  {
        
    echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
           echo'      <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
           ORDER BY order_by
         ";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grades_cce/grade_cce_excel_award_list.php?class_id='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
                      
          
        
        
        
        
        
  }
  



 //select class and subject for xi_xii marks 
  function new_grade_cce_select_class_subject()
  {
        
  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Add Marks</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';        
 echo ' <form action="insert_marks_xi_xii.php"  method="get">    
             
 <div class="section "> <label>Select Term</label>   
    <div> 
          <select  data-placeholder="Choose  Term..." class="chzn-select" tabindex="2" name="term" id="term">
        <option value=""></option>  ';
                        
$get_term="SELECT *
               FROM  `cce_term_table` 
               
                   ";
$execute_get_term=mysql_query($get_term);
while($fetch_term=mysql_fetch_array($execute_get_term))//looping for getting year
{   
         $term_name=$fetch_term['term_name'];

echo'
     <option value="'.$fetch_term[0].'">'. $term_name.'</option>';
  }
  
  
 echo'    
</select>
</div>
</div>';


echo'<div class="section "> <label>Select Test</label>   
    <div> 
          <select  data-placeholder="Choose  Test..." class="chzn-select" tabindex="2" name="test" id="test">
        <option value=""></option>  ';
 
        //Query to display details of the tests
        
        $queryTestName = "SELECT * FROM  add_test_xi_xii";
	
        $getTestName = mysql_query($queryTestName);
        while($fetch_test=mysql_fetch_array($getTestName))
        {
           $test_name=$fetch_test['test_name'];
           echo'
              <option value="'.$fetch_test[0].'">'.$test_name.'</option>';
        }

 echo'    
</select>
</div>
</div>


  <div class="section "> <label>Select Class</label>   
               <div> 
          <select  data-placeholder="Choose  Class..." class="chzn-select" tabindex="2" name="class" id="class" 
        <option value=""></option>  ';
                        
$get_class="SELECT *
               FROM  `class_index` Where class>=11
               ORDER BY order_by
                   ";
$execute_class=mysql_query($get_class);
while($fetch_class=mysql_fetch_array($execute_class))//looping for getting year
{   
         $class=$fetch_class['class_name'];

echo'
     <option value="'.$fetch_class[0].'">'. $class.'</option>';
  }
 echo'    
</select>
</div>
</div>



  
  <div>
 <button type ="submit"  class="uibutton submit">Submit</button>

</div>

</form>';
       echo'  	                       
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';
                 
  }

function insert_marks_xi_xii()
{
   $class_id = $_GET['class'];
	//$testId = $_GET['test_id'];
	$test_id = $_GET['test'];
	$queryTestName = "SELECT * FROM  add_test_xi_xii WHERE id=$test_id";
	
        $getTestName = mysql_query($queryTestName);
        $fetch_test=mysql_fetch_array($getTestName);
        $test_name=$fetch_test['test_name'];
	$queryClassName = "SELECT `class_name` FROM `class_index` WHERE `cId` = '$class_id'";
	$getClassName = mysql_query($queryClassName);
	$className = mysql_fetch_array($getClassName);
	$className = $className[0];
	
	echo '
	<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i><b>Class :'.$className.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Test Name : '.$test_name.'</b></span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
';
																																		                                   
        $get_test = "SELECT DISTINCT subject.*   FROM cce_exam_type_xi_xii 
       
        INNER JOIN subject ON subject.subject_id=cce_exam_type_xi_xii.subject_id        
WHERE cce_exam_type_xi_xii.class_id=$class_id

";
$exe_test = mysql_query($get_test);
echo '<div class="span4">
';
while($fetch_test = mysql_fetch_array($exe_test))
{
      
      $subject=$fetch_test['subject'];
      $subject_id=$fetch_test['subject_id'];
     echo ' <a href="new_grade_cce_add_marks_Xi_xii_students_class.php?class_id='.$class_id.'&subject_id='.$subject_id.'&test_id='.$test_id.'&testName='.$test_name.'"><div class="alertMessage inline info">'.$subject.'</div></a> 
';
      
}
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
         
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                                                             
																		//------------End of the section to display the subject and teacher---------
/*
Now to insert/update data into the marks table, we have to first get the current marks table and then enter 
the following details into the table
sId - Id of the student which will come from student_user table, this is to identify the student
tId - Id of the teacher which will come from users table, this is to tel which teacher is associated with it
cId - Id of the class which will come from class_index table, this will tell the class of the student
test_id - id of the test which will come from test_index table, this will tell the test Id
subject_id - id of the subject which will come from subject table, this will tell for which subject the marks ar 
			given
			
There will be 2 javascript function calls:
one will be to update the marks when the above details are given and to do that we will make a select query if it returns a value we will put it in the textbox and will associate a update function call with the text box

The other one will be when the data will be entered for the first time to do this again we will make a select query and if it doesnot return anything then we will associate a insert function call with the text box

*/
							
							
//							
//							
//							//Query to get the students id and names for marks
//							$queryStudent = "
//							SELECT student_user.sId, `Name`
//							FROM student_user
//							INNER JOIN class
//							ON student_user.sId = class.sId
//                            
//							WHERE class.classId = '$classId'
//							ORDER BY `Name` ASC
//							";
//							
//							$getStudents = mysql_query($queryStudent);
//							$uniqueId = 0;					//use to generate unique ID for each <td>
//							
//											echo'	<table class="table table-bordered table-striped">
//														
//									<!--Header of the table-->
//									 <thead>
//                                            <tr style="text-align: center">
//											<th style="width:90px">Student Name</th>';
//											for($i=0;$i<$result_count;$i++)
//											{
//												echo '<th style="width:70px; ">'.$subjectNameMarks[$i].'</th>';
//											}
//								echo   '</tr>
//								 </thead>
//								 <tbody align="center">';
//								while($students = mysql_fetch_array($getStudents))
//								{
//									/*Now use a for loop to generate TD containing a text 
//										with Test Id, Teacher Id, subject Id, classId
//									*/
//									$studentId = $students[0];
//									$studentName = $students[1];
//									
//									echo '<tr>';
//										echo '<td>'.$studentName.'</td>';
//										for($i=0;$i<$result_count;$i++)
//										{
//											/*
//											$uniqueId : generates a unique Id for a <td>
//											$studentId : contains the student Id and it changes every <tr>
//											$teacherIdMarks : contains the teacher Id and changes every<td>
//											$classId : contains the class Id and is constant throughout
//											$testId : contains the test Id and is constant
//											$subjectIdMarks : contains the subject Id and changes every<td>
//											*/
//											$queryMarks = "SELECT `mId`,`marks` FROM marks WHERE sId = '$studentId' AND tId = '$teacherIdMarks[$i]' AND cId = '$classId' AND test_id = '$testId' AND subject_id = '$subjectIdMarks[$i]'";
//											$getMarks = mysql_query($queryMarks);
//											$count_no_rows = mysql_num_rows($getMarks);
//											
//											if($count_no_rows == 0)
//											{
//												++$uniqueId;
//												//For the insert operation
//												echo '<td><img id="'.$uniqueId.'IMG" style="visibility:hidden" /><input id="'.$uniqueId.'" style="width :60px" type="text" name="studentId='.$studentId.'&teacherId='.$teacherIdMarks[$i].'&classId='.$classId.'&testId='.$testId.'&subjectId='.$subjectIdMarks[$i].'" onBlur="getValueInsert('.$uniqueId.')" /></td>';
//											}
//											else
//											{
//												//For the update operation
//												$marks = mysql_fetch_array($getMarks);
//												$mId = $marks[0];
//												$marksValue = $marks[1];
//												$uniqueId++;
//												echo '<td><img id="'.$uniqueId.'IMG" style="visibility:hidden" /><input id="'.$uniqueId.'" style="width :60px" type="text" name="studentId='.$studentId.'&teacherId='.$teacherIdMarks[$i].'&classId='.$classId.'&testId='.$testId.'&subjectId='.$subjectIdMarks[$i].'&mId='.$mId.'" value="'.$marksValue.'" onfocus="getValueUpdate('.$uniqueId.')" onblur="setValueUpdate('.$uniqueId.')" /></td>';
//											}
//										}
//									echo '</tr>';
//								}
//											
//											echo '
//											</tbody>
//											</table>';
											echo'
						
	
	</div>
												</div>
												</div>
												</div>
												';

}
 
  
 




function new_grade_cce_add_marks_xi_xii_student()
{
      
      
      
   $class_id = $_GET['class_id'];
	$testId = $_GET['test_id'];
	$testName = $_GET['testName'];
        $subject_id=$_GET['subject_id'];
        
       
                
	
	 $queryClassName = "SELECT `class_name` FROM `class_index` WHERE `cId` = '$class_id'";
	$getClassName = mysql_query($queryClassName);
	$className = mysql_fetch_array($getClassName);
	$className = $className[0];
        $get_test = "SELECT DISTINCT subject.*   FROM cce_exam_type_xi_xii   
 INNER JOIN subject ON subject.subject_id=cce_exam_type_xi_xii.subject_id        
WHERE cce_exam_type_xi_xii.class_id=$class_id AND cce_exam_type_xi_xii.subject_id=$subject_id
";
$exe_test = mysql_query($get_test);
$fetch_test = mysql_fetch_array($exe_test);
      $subject=$fetch_test['subject'];
      $subject_id=$fetch_test['subject_id'];
      echo '
	<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i><b>Class :'.$className.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Test Name : '.$testName.'&nbsp;&nbsp;&nbsp;&nbsp; Subject:'.$subject.'</b></span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
';
   $cy=0;
	
   echo'<form>
  <div class="section "> <label>Select Exam Type</label>   
               <div> 
          <select  data-placeholder="Choose  ..." class="chzn-select" tabindex="2" name="exam_type_id" id="exam_type_id" onchange="select_stu('.$class_id.','.$testId.','.$subject_id.')">
        <option value=""></option>  ';
       $exam_type="SELECT * FROM `cce_exam_type_xi_xii`
           WHERE class_id=$class_id AND subject_id=$subject_id ";
           $exe_exam_type=mysql_query($exam_type);
while($fetch_exam=mysql_fetch_array($exe_exam_type))
   {
        $exam_type = $fetch_exam['exam_type'];
        $exam_type_id=$fetch_exam[0];
        $max_marks = $fetch_exam['max_marks'];      
echo'
     <option value="'.$exam_type_id.'">'. $exam_type.'</option>';
  }
 echo'    
</select>
</div>
</div></form><span id="show_stu"></span>';
   
   
   
   
   
   
   
}



   ////xi xii teacher add marks
        
        function xi_xii_dashboard_teacher()
        {
           echo '	 <div class="row-fluid">              
                       
                        <div class="widget  span12 clearfix">                           
                            <div class="widget-header">
                                <span><i class="icon-home"></i></span>
                            </div><!-- End widget-header -->	                            
                            <div class="widget-content">';
         echo ' <form action="cce_add_marks_xi_xii_teacher.php"  method="get">    
             
 <div class="section "> <label>Select Term</label>   
    <div> 
          <select  data-placeholder="Choose  Term..." class="chzn-select" tabindex="2" name="term" id="term">
        <option value=""></option>  ';
                        
$get_term="SELECT *
               FROM  `cce_term_table` 
               
                   ";
$execute_get_term=mysql_query($get_term);
while($fetch_term=mysql_fetch_array($execute_get_term))//looping for getting year
{   
         $term_name=$fetch_term['term_name'];

echo'
     <option value="'.$fetch_term[0].'">'. $term_name.'</option>';
  }
  
  
 echo'    
</select>
</div>
</div>';


echo'<div class="section "> <label>Select Test</label>   
    <div> 
          <select  data-placeholder="Choose  Test..." class="chzn-select" tabindex="2" name="test" id="test">
        <option value=""></option>  ';
 
        //Query to display details of the tests
        
        $queryTestName = "SELECT * FROM  add_test_xi_xii";
	
        $getTestName = mysql_query($queryTestName);
        while($fetch_test=mysql_fetch_array($getTestName))
        {
           $test_name=$fetch_test['test_name'];
           echo'
              <option value="'.$fetch_test[0].'">'.$test_name.'</option>';
        }

 echo'    
</select>
</div>
</div>';
	                   
  $get_class="SELECT DISTINCT teachers.class,class_index.*
	FROM teachers
	INNER JOIN class_index
	ON class_index.class_name = teachers.class
	WHERE tId = ".$_SESSION['user_id']."  ORDER BY  class_index.class+0 ASC";

  echo'<div class="section "> <label>Select Class</label>   
               <div> 
          <select  data-placeholder="Choose  Class..." class="chzn-select" tabindex="2" name="class" id="class" >
        <option value=""></option>  ';
 
$execute_class=mysql_query($get_class);
while($fetch_class=mysql_fetch_array($execute_class))//looping for getting year
{   
         $class=$fetch_class['class_name'];

echo'
     <option value="'.$fetch_class['cId'].'">'. $class.'</option>';
  }
 
 echo'    
</select>
</div>
</div>
  <div>
 <button type ="submit"  class="uibutton submit">Submit</button>

</div>

</form>';
       echo'  	                       
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';
                 
  }

function xi_xii_add_marks_subject_teacher()
{
   
   
   
   
   
    $class_id = $_GET['class'];
	//$testId = $_GET['test_id'];
	$test_id = $_GET['test'];
	 $queryTestName = "SELECT * FROM  add_test_xi_xii WHERE id=$test_id";
	
        $getTestName = mysql_query($queryTestName);
        $fetch_test=mysql_fetch_array($getTestName);
        $test_name=$fetch_test['test_name'];
	$queryClassName = "SELECT `class_name` FROM `class_index` WHERE `cId` = '$class_id'";
	$getClassName = mysql_query($queryClassName);
	$className = mysql_fetch_array($getClassName);
	$className = $className[0];
	
	echo '
	<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i><b>Class :'.$className.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Test Name : '.$test_name.'</b></span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
';
																																		                                   
        $get_test = "SELECT DISTINCT subject.* FROM cce_exam_type_xi_xii
 INNER JOIN subject
 ON subject.subject_id=cce_exam_type_xi_xii.subject_id 

 INNER JOIN teachers 
ON teachers.subject_id = subject.subject_id
 INNER JOIN class_index 
ON class_index.class_name = teachers.class
 WHERE  cce_exam_type_xi_xii.class_id =$class_id  AND  teachers.tId = ".$_SESSION['user_id']."";

$exe_test = mysql_query($get_test);
echo '<div class="span4">
';
while($fetch_test = mysql_fetch_array($exe_test))
{
      
      $subject=$fetch_test['subject'];
      $subject_id=$fetch_test['subject_id'];
     echo ' <a href="new_grade_cce_add_marks_Xi_xii_students_class.php?class_id='.$class_id.'&subject_id='.$subject_id.'&test_id='.$test_id.'&testName='.$test_name.'"><div class="alertMessage inline info">'.$subject.'</div></a> 
';
      
}
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
         
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                                                             

											echo'
						
	
	</div>
												</div>
												</div>
												</div>
';}




///////////////////////co-area........................marks........................


function grade_cce_xi_xii_co_area_dashboard()
{
   echo '	 <div class="row-fluid">              
                       
                        <div class="widget  span12 clearfix">                           
                            <div class="widget-header">
                                <span><i class="icon-home"></i></span>
                            </div><!-- End widget-header -->	                            
                            <div class="widget-content">';
         echo ' <form action="cce_add_co_area_marks_xi_xii_teacher.php"  method="get">    
             
 <div class="section "> <label>Select Term</label>   
    <div> 
          <select  data-placeholder="Choose  Term..." class="chzn-select" tabindex="2" name="term" id="term">
        <option value=""></option>  ';
                        
$get_term="SELECT *
               FROM  `cce_term_table` 
               
                   ";
$execute_get_term=mysql_query($get_term);
while($fetch_term=mysql_fetch_array($execute_get_term))//looping for getting year
{   
         $term_name=$fetch_term['term_name'];
        
echo'
     <option value="'.$fetch_term[0].'">'. $term_name.'</option>';
  }
  
  
 echo'    
</select>
</div>
</div>';


	                   
  $get_class="SELECT DISTINCT teachers.class,class_index.*
	FROM teachers
	INNER JOIN class_index
	ON class_index.class_name = teachers.class
	WHERE tId = ".$_SESSION['user_id']."  ORDER BY  class_index.class+0 ASC";

  echo'<div class="section "> <label>Select Class</label>   
               <div> 
          <select  data-placeholder="Choose  Class..." class="chzn-select" tabindex="2" name="class" id="class" >
        <option value=""></option>  ';
 
$execute_class=mysql_query($get_class);
while($fetch_class=mysql_fetch_array($execute_class))//looping for getting year
{   
         $class=$fetch_class['class_name'];

echo'
     <option value="'.$fetch_class['cId'].'">'. $class.'</option>';
  }
 
 echo'    
</select>
</div>
</div>
  <div>
 <button type ="submit"  class="uibutton submit">Submit</button>

</div>

</form>';
       echo'  	                       
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';
            
    
    
    
    
    
    
    
    
}
//////
function grade_cce_xi_xii_co_area_details()
{
    
   
                  $class_id = $_GET['class'];
     $term_id = $_GET['term'];
	//$test_id = $_GET['test'];
	
	$queryClassName = "SELECT `class_name` FROM `class_index` WHERE `cId` = '$class_id'";
	$getClassName = mysql_query($queryClassName);
	$className = mysql_fetch_array($getClassName);
	$className = $className[0];
	
	echo '
	<div class="row-fluid">                            
                        <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i><b>Class :'.$className.'</b></span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
';
																																		                                   
   $get_test = "SELECT  * FROM  cce_xi_xii_co_area_table";

$exe_test = mysql_query($get_test);
echo '<div class="span4">
';
while($fetch_test = mysql_fetch_array($exe_test))
{
      $area_name=$fetch_test['area_name'];
      $area_id=$fetch_test['area_id'];
     echo ' <a href="new_grade_cce_add_co_area_marks_Xi_xii_students_class.php?class_id='.$class_id.'&area_id='.$area_id.'&term_id='.$term_id.'"><div class="alertMessage inline info">'.$area_name.'</div></a> 
';
      
}
                                            
                                            
                                                                          

echo'
</div>
</div>
</div>
</div>
';
        
}


function grade_cce_xi_xii_co_area_student_details()
{
     $class_id=$_GET['class_id'];
     $term_id=$_GET['term_id'];
     $area_id=$_GET['area_id'];
      echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">                    
<div class="widget-header">
 <span><i class="icon-home"></i>view students </span>
</div><!-- End widget-header -->	
<div class="widget-content">';  
      
        $get_test = "SELECT  * FROM  cce_xi_xii_co_area_table where area_id=$area_id";

$exe_test = mysql_query($get_test);

$fetch_test = mysql_fetch_array($exe_test);
    $area_name=$fetch_test[1];  
      
      
      
      
      echo'<h5 align="center">'.$area_name.'</h5>';
      
                    echo '
<table class="table table-bordered table-striped"  border="2"    width="100%"   >
<thead >
<tr>
<th rowspan="2" width=10%>Admission No.</th>
</tr>
<tr><th  align="center">Name</th><th>Grade</th></tr>

</thead>';

    echo '
           <tbody align="center">';
           //get student names on the class id
   
   $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                INNER JOIN roll_no
                ON roll_no.sId=student_user.sId

                WHERE class.classId='".$class_id."'
                AND class.session_id='".$_SESSION['current_session_id']."' ORDER BY  roll_no.roll_no";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {         $student_id=$fetch_students['sId'];
                     
                      $get_details_for_update=
"SELECT *
FROM    cce_xi_xii_co_area_marks_table
WHERE 
student_id = ".$student_id."
    AND class_id = ".$class_id."
AND session_id = ".$_SESSION['current_session_id']." AND term_id = ".$term_id." AND area_id=$area_id";
$exe_details=mysql_query($get_details_for_update);
$fetch_details=mysql_fetch_array($exe_details);
    $grades=$fetch_details['grade'];               
                     
                     
                     
                     
                  echo'
                <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td align="left" width="30%">'.$fetch_students['Name'].'</td>
             <td align="center">
<select  name="grade" id="grade"  style="width:100px;" onchange="insert_xi_xii_co_area_marks('.$student_id.','.$class_id.',this.value,'.$term_id.','.$area_id.');">
 <option value="0">'.$grades.'</option>
<option value="A">A</option>
<option value="B">B</option>
<option value="C">C</option>
<option value="D">D</option>
<option value="E">E</option>
<option value="AB">AB</option>
</select>     </td></tr>	  '; 
                     }     

                     
                     echo'</tbody></table>';  
      
      
      
      
                
            echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';
       
    
    
    
    
    
    

    
}


function preboard_green_sheet_class()
  {
      
  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
           echo'      <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          WHERE  class >=11 AND class <=12
         ";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grades_cce/preboad_green_sheet_view_report.php?class_id='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
                      
      
  }


       
function grade_cce_class_xii_practical()
{


 echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
           echo' <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          WHERE  class >=11 
       ORDER BY  order_by
";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grades_cce/grade_cce_xii_practical_list.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';     
}


///////////////////////////////////////////////////////////////////TEST REPORT////////////////////////////////////////


function   term_view_class_all()
        {
            
            echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';       
            
              
    echo'      <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          
         ";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grades_cce/grade_cce_view_green_sheet.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
                  
        }
        

        
 function   term_view_class_all_()
        {
            
            echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';       
            
              
    echo'      <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index 
          
         ";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grades_cce/grade_cce_view_green_sheet_marks.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
                  
        }


function grade_cce_test_1_4_class()
{


 echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
     <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
         echo' <ol class="rounded-list">
         ';

  
      $get_class=
          "SELECT *
         FROM class_index
         WHERE  class >=1 AND class <=4
       ORDER BY  order_by
";
         $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
               echo '<li><a href="grade_cce_all_test_details_1_4.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
          echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';     
}






///////////////////////////////// end All student test  Report   //////////////////////////////////////////////////////////////////////////////

function test_all_test_details_1_4()
{


 $class_id=$_GET['id_class'];

        
echo '  <div class="row-fluid">
<!-- Table widget -->
<div class="widget  span12 clearfix">
<div class="widget-header">
<span><i class="icon-home"></i>Choose subject to view subject activities and marks</span>
</div><!-- End widget-header -->	
<div class="widget-content">';
								
	 //echo'<a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_test_sheet_report_class.php?class_id='.$class_id.'">Test Sheet Report </a><br>'; 
//get subject id from teachers

 $get_id_subject = 
"SELECT *,cce_term_test_table.*
FROM cce_test_table
INNER JOIN cce_term_test_table
ON cce_term_test_table.test_id=cce_test_table.test_id
WHERE cce_term_test_table.class_id=$class_id
";
$exe_subjects=mysql_query($get_id_subject);
echo '<div class="span4">
';
while($fetch_subjects=mysql_fetch_array($exe_subjects))
{
$test_id=$fetch_subjects[0];
$term_id=$fetch_subjects['term_id'];
echo'<a href="grade_cce_test_report_all_subject_1_4.php?class='.$class_id.'&test_id='.$test_id.'&term_id='.$fetch_subjects['term_id'].'"><div class="alertMessage inline info">'.$fetch_subjects['test_name'].'</div></a> 
';
}
echo '</div>';

echo '</div>

</div>
</div>';

}





function cce_test_all_subject_1_4()
{

   $class_id=$_GET['class'];  
             $term_id=$_GET['term_id'];   
              $test_id=$_GET['test_id'];  
              
        
echo '  <div class="row-fluid">

<!-- Table widget -->
<div class="widget  span12 clearfix">

<div class="widget-header">
<span><i class="icon-home"></i>Choose subject to view subject activities and marks</span>
</div><!-- End widget-header -->	

<div class="widget-content">';
								
	
//get subject id from teachers
 $get_id_subject = 
"SELECT DISTINCT subject.subject , subject.subject_id
FROM subject
INNER JOIN cce_subject_type_skills_id_table

ON cce_subject_type_skills_id_table.subject_id = subject.subject_id

WHERE cce_subject_type_skills_id_table.class_id =$class_id";
$exe_subjects=mysql_query($get_id_subject);
echo '<div class="span4">
';
while($fetch_subjects=mysql_fetch_array($exe_subjects))
{

echo ' <a href="grades_cce/grade_cce_view_test_all_report_1_4.php?class_id='.$_GET['class'].'&subject_id='.$fetch_subjects[1].'&term_id='.$term_id.'&test_id='.$test_id.'"><div class="alertMessage inline info">'.$fetch_subjects[0].'</div></a> 
';
}
echo '</div>';

echo '</div>

</div>
</div>';	


}


//////////////////////////////////////////////////////////TERM-II Green sheet  /////////////////////////////////////////////////

function grade_cce_view_green_sheet_term_2()
{

 echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
     <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
         echo' <ol class="rounded-list"> ';
      $get_class=
          "SELECT *
         FROM class_index
         WHERE  class >=1 
       ORDER BY  order_by
";
         $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
	if($fetch_classes[0]==52||$fetch_classes[0]==53)
		{
		 echo '<li><a href="grades_cce/grade_cce_view_green_sheet_marks_xi.php?class_id='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';
}

elseif($fetch_classes[0]==58||$fetch_classes[0]==59)
		{
		 echo '<li><a href="grades_cce/grade_cce_view_green_sheet_marks_xi1.php?class_id='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';
}
	else
     echo '<li><a href="grades_cce/grade_cce_view_green_sheet_term_2.php?class_id='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
          echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';     

}







//////////////////////////////////VIEW GREEN SHEET NURSERY PP???
function   grade_cce_assessement()
{
  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Report details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            $class_id=$_GET['class_id'];
            $get_cycle_test="SELECT cce_test_table.*
               FROM  `cce_test_table` 
               INNER JOIN  cce_term_test_table
               ON cce_term_test_table.test_id=cce_test_table.test_id
               WHERE cce_term_test_table.class_id=$class_id
                   ";
         $exe_ass=  mysql_query($get_cycle_test);
         while($fetch_ass=mysql_fetch_array($exe_ass))
         {
             $ass_id=$fetch_ass['test_id'];
         $ass_name=$fetch_ass['test_name'];
         
         echo' <a class="uibutton btn-btn " align="center"  type="button" href="grade_cce_view_green_sheet_subject_nursery_pre_primary.php?class_id='.$class_id.'&test_id='.$ass_id.'">'.$ass_name.'</a>';
         }
         
         
             
              
              
              
              
              
              
                         echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';      
           
   
   
   
   
   
   
}
function view_nursery_pre_primary_green_sheet_sub()
{
   
         
             $class_id=$_GET['class_id'];  
            
              $test_id=$_GET['test_id'];  
              
        
echo '  <div class="row-fluid">

<!-- Table widget -->
<div class="widget  span12 clearfix">

<div class="widget-header">
<span><i class="icon-home"></i>Choose subject to view subject activities and marks</span>
</div><!-- End widget-header -->	

<div class="widget-content">';
								
	
//get subject id from teachers
 $get_id_subject = 
"SELECT DISTINCT subject.subject , subject.subject_id
FROM subject
INNER JOIN grade_cce_assign_subject_activity

ON grade_cce_assign_subject_activity.subject_id = subject.subject_id

WHERE subject.subject_id<=3";
$exe_subjects=mysql_query($get_id_subject);
echo '<div class="span4">
';
while($fetch_subjects=mysql_fetch_array($exe_subjects))
{

echo ' <a href="grades_cce/grade_cce_green_sheet_nur_pp_report.php?class_id='.$_GET['class_id'].'&subject_id='.$fetch_subjects[1].'&test_id='.$test_id.'"><div class="alertMessage inline info">'.$fetch_subjects[0].'</div></a> 
';
}
echo '</div>';

echo '</div>

</div>
</div>';	
          
   
   
   
   
   
   
}

//////////////////////////////FULL REPORT CARD V _X CLASSS///////////////////

      
function full_green_sheet_class_v_x()
{


 echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center">Class details</i></span>
      </div><!-- End widget-header -->
      <div class="widget-content">';    
            
           echo' <ol class="rounded-list">
          ';

  
      $get_class=
          "SELECT *
          FROM class_index
          WHERE  class >=5 AND class <=10
       ORDER BY  order_by
";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="grades_cce/grade_cce_view_full_green_sheet_v_x.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }   
           echo'  	                       
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->';     
}


?>
