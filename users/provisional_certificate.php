<?php
require_once('../config/config.php');
$student_id = $_GET['student_id'];


date_default_timezone_set ('Asia/Calcutta');
$date = date('d-M-Y');

$query_name_father_name = "
	SELECT `Name`,`Father's Name`,`gender`,`DOB`
	FROM `student_user`
	WHERE sId = $student_id
";
$execute_details = mysql_query($query_name_father_name);
$details = mysql_fetch_array($execute_details);
$name = $details[0];
$f_name = $details[1];
$gender = $details[2];
$dob = $details[3];

$query_get_12th_marks = "SELECT * FROM `cgpa_report_class_12` WHERE `sid` = $student_id";
$execute_get_12th_marks = mysql_query($query_get_12th_marks);
$get_12th_marks = mysql_fetch_array($execute_get_12th_marks);

$ctr = 0;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PROVISIONAL CERTIFICATE</title>
</head>

<body style="font:'Times New Roman', Times, serif;">
<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
<table  align="right" >
<tr >
<td  align="right" valign="top" style="margin-right:550px;width:100%;font-size:18px"><strong >Date : <?php echo $date; ?></strong>
</td>
</tr>
<table cellspacing="5" width="100%">


<p>&nbsp;</p><p>&nbsp;</p>


<tr>
	<td align="center" colspan="5"><p style="font-size:24px"><strong><u>PROVISIONAL CERTIFICATE</u></strong></p></td>
</tr>

<tr>
	<td colspan="5"><p style="font-size:18px; text-align:justify; ">Certified that 
    
    <b><?php echo $name; ?></b> <?php 
	
			if($gender == 'Male')
			{
				echo ' S/O ';
			}
			elseif($gender == 'Female')
			{
				echo ' D/O ';
			}
	
	?> <b>Mr.</b> <b><?php echo $f_name; ?></b> was a bonafide student of this institute. 
    
    
    <?php 
	
			if($gender == 'Male')
			{
				echo ' He ';
			}
			elseif($gender == 'Female')
			{
				echo ' She ';
			}
	
	?>
    
    
     has appeared in All India Senior Secondary School Certificate Examination held in March 2014 and passed the same with the following marks:</p></td>
</tr>

<tr>
	<th align="left">S. No.</th>
    <td><b>Subject</b></td>
    <th align="center">Maximum Marks</th>
    <th align="center">Marks Obtained</th>
</tr>
<?php
if($get_12th_marks['english'] != 0)
{
	echo '
			<tr>
				<td align="center">'.++$ctr.'.</td>
				<td >English</td>
				<td align="center">100</td>
				<td align="center">'.$get_12th_marks['english'].'</td>
			</tr>
	';
}

?>

<?php
if($get_12th_marks['maths'] != 0)
{
	echo '
			<tr>
				<td align="center">'.++$ctr.'.</td>
				<td>Mathematics</td>
				<td align="center">100</td>
				<td align="center">'.$get_12th_marks['maths'].'</td>
			</tr>
	';
}

?>

<?php
if($get_12th_marks['physics'] != 0)
{
	echo '
			<tr>
				<td align="center">'.++$ctr.'.</td>
				<td >Physics</td>
				<td align="center">100</td>
				<td align="center">'.$get_12th_marks['physics'].'</td>
			</tr>
	';
}

?>

<?php
if($get_12th_marks['chemistry'] != 0)
{
	echo '
			<tr>
				<td align="center">'.++$ctr.'.</td>
				<td >Chemistry</td>
				<td align="center">100</td>
				<td align="center">'.$get_12th_marks['chemistry'].'</td>
			</tr>
	';
}

?>

<?php
if($get_12th_marks['biology'] != 0)
{
	echo '
			<tr>
				<td align="center">'.++$ctr.'.</td>
				<td >Biology</td>
				<td align="center">100</td>
				<td align="center">'.$get_12th_marks['biology'].'</td>
			</tr>
	';
}

?>

<?php
if($get_12th_marks['accountancy'] != 0)
{
	echo '
			<tr>
				<td align="center">'.++$ctr.'.</td>
				<td >Accountancy</td>
				<td align="center">100</td>
				<td align="center">'.$get_12th_marks['accountancy'].'</td>
			</tr>
	';
}

?>

<?php
if($get_12th_marks['economics'] != 0)
{
	echo '
			<tr>
				<td align="center">'.++$ctr.'.</td>
				<td >Economics</td>
				<td align="center">100</td>
				<td align="center">'.$get_12th_marks['economics'].'</td>
			</tr>
	';
}

?>

<?php
if($get_12th_marks['business_studies'] != 0)
{
	echo '
			<tr>
				<td align="center">'.++$ctr.'.</td>
				<td >Business Studies</td>
				<td align="center">100</td>
				<td align="center">'.$get_12th_marks['business_studies'].'</td>
			</tr>
	';
}

?>

<?php
if($get_12th_marks['computer_science'] != 0)
{
	echo '
			<tr>
				<td align="center">'.++$ctr.'.</td>
				<td >Computer Science</td>
				<td align="center">100</td>
				<td align="center">'.$get_12th_marks['computer_science'].'</td>
			</tr>
	';
}

?>

<?php
if($get_12th_marks['physical_education'] != 0)
{
	echo '
			<tr>
				<td align="center">'.++$ctr.'.</td>
				<td >Physical Education</td>
				<td align="center">100</td>
				<td align="center">'.$get_12th_marks['physical_education'].'</td>
			</tr>
	';
}

?>


<tr>
	<td colspan="5">
   	  <p style="font-size:18px; text-align:justify; ">
   	  We wish 
      
      <?php 
	
			if($gender == 'Male')
			{
				echo ' him ';
			}
			elseif($gender == 'Female')
			{
				echo ' her ';
			}
	
	?>
      
      <b> ALL THE BEST</b> for all 
       
       <?php 
	
			if($gender == 'Male')
			{
				echo ' his ';
			}
			elseif($gender == 'Female')
			{
				echo ' her ';
			}
	
	?>
       
      future endeavours</strong>.</p>
    </td>
</tr>

<tr>
	<td colspan="5" align="left">
    	<p>&nbsp;
       	</p>
    	<p>&nbsp;</p>
    	<p>Dr. S. V.  SHARMA  
        </p>
    </td>
</tr>

</table>
</body>
</html>

