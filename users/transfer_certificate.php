<?php
require_once('../include/functions_dashboard.php');
require_once('../config/config.php');
require_once('../include/session.php');
require_once('../include/check.php');
require_once('manage_users_functions.php');


//$student_id = $_GET['student_id'];

$studentSid = $_GET['student_id'];
	//This function is used to view detials of the student
	
	$privilege = $_SESSION['priv'];
/*	if(($privilege!=0)&&($privilege!=1))
	{
		header("Location: error.php");
	}
	*/
	//Query to fetch the login details of the user
	
       $query_login_details = mysql_query("SELECT `username`,`privilege` FROM `login` WHERE `lId`='$studentSid' ");
	$login_details = mysql_fetch_array($query_login_details);
	$username = $login_details[0];
	$privilege = $login_details[1];
	
	//Query to get the class of the student users
                                 $query_get_class = "
		SELECT class_index.class_name
		FROM class_index
		INNER JOIN class
		ON class.classId = class_index.cId
		WHERE class.sId = $studentSid AND class.session_id = ".$_SESSION['current_session_id']."
	";
	$execute_get_class = mysql_query($query_get_class);
	$class = mysql_fetch_array($execute_get_class);
	$class = $class[0];
	
	
	//echo $username.' '.$privilege;
	
	//Query to fetch personal details of the user
	 $query_personal_details = mysql_query("SELECT `Name`,`DOB`,`Email`,`Local Address`,`Phone No`,`gender`,`image`, `Father's Name`, `Mother's Name`, `Guardian's Contact`,`Guardian's Email`,`admission_no`,`Blood Group`,`sId`,`category`,`religion`,`admission_date`  FROM `student_user` WHERE `sId` = '$studentSid' ");

	$personal_details = mysql_fetch_array($query_personal_details);
	$name = $personal_details[0];
	$dob = $personal_details[1];
	$email = $personal_details[2];
	$local_address = $personal_details[3];
	$phone_no = $personal_details[4];
	$sex = $personal_details[5];
	$image = $personal_details[6];
	$father_name = $personal_details[7];
	$mother_name = $personal_details[8];
	$guardian_contact = $personal_details[9];
	$guardian_email = $personal_details[10];
	$admission_no = $personal_details[11];
	$blood_group = $personal_details[12];
	$sId = $personal_details[13];
	$category = $personal_details[14];
	$religion = $personal_details[15];
	$admission_date = $personal_details[16];
	

	$get_stu_class = "SELECT `class` FROM `struck_off_student`
				INNER JOIN `class_index` ON struck_off_student.class_id = class_index.cId
				WHERE struck_off_student.student_id = $studentSid";

	$exe_stu_class = mysql_query($get_stu_class);

	$fetch_stu_class = mysql_fetch_array($exe_stu_class);

	$stu_class = $fetch_stu_class[0];

	$get_tc = "SELECT max(transfer_id) FROM `transfer_certificate`";
	$exe_tc = mysql_query($get_tc);

	$fetch_tc = mysql_fetch_array($exe_tc);

	$tc_no = $fetch_tc[0]+1;



	$get_tc = "SELECT `transfer_id` FROM `transfer_certificate` WHERE `sid` = $studentSid";
	$exe_tc = mysql_query($get_tc);

	$fetch_tc = mysql_fetch_array($exe_tc);

	if($fetch_tc)
		$tc_no = $fetch_tc[0];


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TRANSFER CERTIFICATE</title>
   <script>
	function change_date()
	{
		$('#date_leaving_school').html('<input type="date" name="date_of_leaving_school"/>');
	}
   </script>

   <script src="js/manage_users.js"></script>

</head>

<body>
Transfer Certificate details
<form action="print_transfer_certificate.php?sid='.$studentSid.'" method="post" enctype="multipart/form-data">
   <input type="hidden" name="sid" value="<?php echo $studentSid ?>" />
<table>

<tr>
	<td></td>
    <td>TC No.</td>
    <td><?php echo '<input type="text" name="tc_num" value="'.$tc_no.'"" onkeyup="check_tc_no(this.value)"/> &nbsp; <b><div style="display:inline-block;" id="tc_no"></div></b>';?></td>
</tr>

<tr>
	<td>1.</td>
    <td>Admission No.</td>
    <td><input type="text" name="admission_no" value="<?php echo $admission_no; ?>" /></td>
</tr>

<tr>
	<td>2.</td>
    <td>Student's Name</td>
    <td><input type="text" name="student_name" value="<?php echo $name; ?>" /></td>
</tr>

<tr>
	<td>3.</td>
    <td>Father's Name</td>
    <td><input type="text" name="father_name" value="<?php echo $father_name; ?>" /></td>
</tr>

<tr>
	<td>4.</td>
    <td>Mother's Name</td>
    <td><input type="text" name="mother_name" value="<?php echo $mother_name; ?>" /></td>
</tr>

<tr>
	<td>5.</td>
    <td>Date of Birth</td>
    <td><input type="text" name="dob" value="<?php echo $dob; ?>" /></td>
</tr>

<tr>
	<td>6.</td>
    <td>Date of Admission</td>
    <td><input type="text" name="doa" value="<?php echo $admission_date; ?>" /></td>
</tr>

<tr>
	<td>7.</td>
    <td>Date of leaving the School</td>
    <td id="date_leaving_school"><input type="text" name="date_of_leaving_school" value="29/05/2014" /></td>
</tr>

<tr>
	<td>8.</td>
    <td>Reason for leaving the School</td>
    <td><input type="text" name="reason_leaving_school" value="XII PASS" /></td>
</tr>

<tr>
	<td>9.</td>
    <td>Attendance Actual</td>
    <td><input type="text" name="attendance" value="" /></td>
</tr>

<tr>
	<td>10.</td>
    <td>Total Working Days</td>
    <td><input type="text" name="working_days" value="216" /></td>
</tr>

<tr>
    <td>11.</td>
    <td>Last Class Attended</td>
    <td><input type="text" name="last_class" value="<?php echo $stu_class; ?>" /></td>
</tr>

<tr>
	<td>12.</td>
    <td>Result</td>
    <td><select name="result" id="result" style="width:175px;"><option value="PASS">PASS</option><option value="FAIL">FAIL</option><option value="Reading in XI class">Reading in XI Class</option><option value="Reading in IV class">Reading in IV Class</option></select></td>
</tr>

<tr>
	<td>13.</td>
    <td>Promoted to class</td>
    <td><input type="text" name="promoted" value="----" /></td>
</tr>

<tr>
	<td>14.</td>
    <td>General Conduct</td>
    <td><input type="text" name="general_conduct" value="GOOD" /></td>
</tr>

<tr>
	<td>15.</td>
    <td>Performance in Co-Curricular Activities</td>
    <td><input type="text" name="co_curricular" value="GOOD" /></td>
</tr>

<tr>
    <td>16.</td>
    <td>All dues & other dues paid upto</td>
    <td><input type="text" name="dues_paid_upto" value="March 2014" /></td>
</tr>

<tr>
	<td>17.</td>
    <td>Issue Date</td>
    <td><input type="text" name="issue_date" value="<?php echo date('d/m/y'); ?>" /></td>
</tr>

<tr>
	<td>18.</td>
    <td>Remarks</td>
    <td><input type="text" name="remarks" value="---" /></td>
</tr>
 
   <tr align="centre">
	
   
      <td width=""></td><td></td><td colspan="" align="right;"><button class="uibutton submit " rel="1" type="submit">Transfer Certificate</button></td>
</tr> 
</table>
</form>
</body>
</html>
