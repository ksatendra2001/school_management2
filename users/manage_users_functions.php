<?php
//function to assign class teacher
function assign_class_teacher()
{
              echo '
	<div class="row-fluid">                            
                         <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Assign class teacher</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">';
	if(isset($_GET['already_assigned']))
	{
	echo '<h5 align="center" style="color:red">Teacher already assigned</h5>';	
		
	}
	if(isset($_GET['assigned']))
	{
	echo '<h5 align="center" style="color:green">Teacher assigned</h5>';	
		
	}
	echo '
	<form action="users/assign_class_teacher.php" method="post">
	';
	//show list of all the teachers where priv = 2 
                   $get_list_teachers_from_users=
	"SELECT users.uId,users.Name
	FROM users
	INNER JOIN login
	ON login.lId = users.uId
	WHERE login.privilege=2";
	$exe_list_teachers=mysql_query($get_list_teachers_from_users);
	echo '
	<div class="section">
	<label>Teachers Name</label>
	<div>
	<select class="chzn-select" name="tid" data-placeholder="Select name">
	<option value=""></option>';
	while($fetch_list=mysql_fetch_array($exe_list_teachers))
	{
		echo '<option value="'.$fetch_list[0].'">'.$fetch_list[1].'</option>';
		
	}
	
	echo '
	</select>
	</div>
	</div>';
	//get list of all the classes
	 $get_list_classes=
	"SELECT cId,class_name
	FROM class_index
                       ORDER BY class+0 ASC ,section ASC";
	$exe_list_class=mysql_query($get_list_classes);
	echo '
	<div class="section">
	<label>Select class</label>
	<div>
	<select class="chzn-select" name="cid" tabindex="1" data-placeholder="Select class">
	<option value=""></option>';
	while($fetch_list_classes=mysql_fetch_array($exe_list_class))
	{
		echo '<option value="'.$fetch_list_classes[0].'">'.$fetch_list_classes[1].'</option>';
		
	}
	
	echo '
	</select>
	</div>
	</div>
	<div class="section last">
	<div>
	<button class="uibutton normal">Assign</button>
	</div>
	</div>
	</form>
	<h5 style="color:grey" align="center">List of class teachers assigned for classes</h5>
	';
	//below is a list of all assigned class teachers
//fetch all the list from class teachers
$get_list_of_c_teachers=
"SELECT users.Name,class_index.class_name
FROM users
INNER JOIN class_teachers
ON class_teachers.tId = users.uId
INNER JOIN class_index
ON class_index.cId = class_teachers.cId";
$exe_class_teachers=mysql_query($get_list_of_c_teachers);

	echo '
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
    <tr>
	<th>Class Teacher name</th>
	<th>Class </th>
	</tr>
	<tbody align="center">
	';
	while($fetch_list_c_teachers=mysql_fetch_array($exe_class_teachers))
	{
		
	echo '<tr>
	<td>'.$fetch_list_c_teachers[0].'</td>
	<td>'.$fetch_list_c_teachers[1].'</td>
	</tr>
	';	
		
	}
	
	
	echo '
	</tbody>
	</table>
	</div>
	</div>
	</div>
	';
		
	
	
}

 // function to update password
 function reset_password()
{	
	 $uid=$_GET['student_id'];
	echo '
	<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Update Password</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	 <form id="validation_demo" action="users/update_password.php?student_id='.$uid.'" method="post" enctype="multipart/form-data">
	 <input type="hidden" value="'.$uid.'" name="student_id"/>
	 
	 <fieldset >
										 <div class="section ">
                                              <label>Old Password</label> 
											  <div>
											 <input type="password"  name="old_password" id="pass_old" value="" required="required"><span id="check_old"></span>
											  </div> 
                                             </div>
											 <div class="section ">
                                              <label>New Password</label> 
											  <div>
											 <input type="password"  name="new_password" id="password" value="" class="validate[required,minSize[3]] medium">
											  </div> 
                                             </div>
											 <div class="section ">
                                              <label>Confirm Password</label> 
											  <div>
											 <input type="password"  name="passwordCon" id="passwordCon" value="" class="validate[required,equals[password]] medium">											  </div> 
                                             </div>
											 
											 
        
                                       ';



echo' <div class="section last">
                                              <div>
                                                <input type="submit" value="Update" class="btn submit_form" ></a>
                                                <a class="btn" onClick="ResetForm()" title="Reset  Form">Clear</a>
                                             </div>
                                             </div> </form>
</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->

 ';
 }

function manage_students_dashboard()
{
	//This function will display the dashboard of managing student users.
	
	 $query_get_students_details = "
	SELECT   student_user.admission_no, student_user.Name, class_index.class_name, student_user.sId
	FROM student_user
	INNER JOIN class
	ON class.sId = student_user.sId
	INNER JOIN class_index
	ON class_index.cId = class.classId
	WHERE class.session_id = ".$_SESSION['current_session_id']."
	 ORDER BY student_user.admission_no+0 ASC 
	";
	$execute_get_students_details = mysql_query($query_get_students_details);
	echo '	
	
               	<div class="row-fluid">	
	           <!-- Table widget -->
	 <div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i> Classes </span>
	</div><!-- End widget-header -->	
	
	<div class="widget-content">';
	if(isset($_GET['studentDeleted']))
	{
	echo '<h5 align="center" style="color:red">Student Deleted Successfully!!</h5>';
		
	}
    if(isset($_GET['studentStruckOff']))
	{
	echo '<h5 align="center" style="color:red">Student StruckOff Successfully!!</h5>';
		
	}
    if(isset($_GET['rollback']))
	{
	echo '<h5 align="center" style="color:red">Student Roll Back Successfully!!</h5>';
		
	}
	echo'
	<h2> Welcome</h2>
	
 <a href="manage_user_add_new_student.php">
    <button class="uibutton icon special add "  rel="1" type="submit" >Add New Student</button></a>
	<a href="select_class_for_xls.php"><button class="uibutton normal "  rel="1" type="submit" >Download Students Info(xls)</button></a>
	
	
	 <a href="manage_user_promote_student.php">
    <button class="uibutton submit "  rel="1" type="submit" >Promote student</button></a>
	
	<a href="manage_user_student_filter.php">
    <button class="uibutton submit "  rel="1" type="submit" >Filters</button></a>

<a href="manage_users_allot_roll_no.php"><button class="uibutton normal "  rel="1" type="submit" >Roll No.</button></a>
<a href="manage_users_datewise_students.php"><button class="uibutton normal"  rel="1" type="submit" >All AdmissionDatewise</button></a>
     <a href="select_class_for_admission_dates.php"><button class="uibutton normal">View classwise Admission Date</button></a>    
     <a href="select_class_for_print_student.php"><button class="uibutton normal">Print students details</button></a><a href="view_ews_students_dashbard.php"><button class="uibutton normal">EWS Student</button></a>
     <a href="show_class_list.php"><button class="uibutton icon special edit "">Edit Master</button></a>
     <a href="manage_download_students.php"><button class="uibutton icon special edit "">Download students</button></a>
	<a href="view_struck_off_student.php"><button class="uibutton normal">View struck off Student</button></a>
<a href="select_class_for_print_admit_card.php"><button class="uibutton normal">Print Admit Card</button></a>
<a href="select_student_for_view_tc.php"><button class="uibutton normal">View Transfer Certificate</button></a>
<br>
<a href="select_class_for_answer_sheet_performa.php"><button class="uibutton normal">Answer Sheets Receiving Performa</button></a>
<a href="student_id_card.php"><button class="uibutton normal">Student Id Card</button></a>
';
	
	echo'		
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
         <tr>		   
	<th>Sno.</th>
	<th>Admission No.</th>
	<th>Student Name</th>
	<th>Class</th>
	<th>Bonafied</th>
	<th>Action</th>
       <th>Action</th>
	
					
    </tr>
    </thead>
						
<tbody align="center">';
$sno = 0;
while($students_details = mysql_fetch_array($execute_get_students_details))
{
        ++$sno;
                        echo '
                       <tr id="txtboxes">
                        <td>'.$sno.'</td>
                        <td>'.$students_details[0].'</td>
                        <td><a href="manage_users_student_details.php?student_id='.$students_details[3].'" >'.$students_details[1].'</a></td>
                        <td>'.$students_details[2].'</td>
                         <td><a href="users/bonafied_certificate.php?student_id='.$students_details[3].'">Bonafied</a></td>
                        <td><a class="btn btn- " onclick="stuck_stu_confirm('.$students_details[3].');">
<i class="icon-trash icon-white"></i> Stuck off</a></td>
                              
<td><a class="btn btn-danger" onclick="delete_stu_confirm('.$students_details[3].');">
<i class="icon-trash icon-white"></i> Delete</a></td>
  </tr> ';
}

echo	
'</tbody>
</table>
<div class="entry">		
<div id="txtHint"></div>
<div id="Hint"></div>				
</div>';
}

function manage_students_print_class()
{
    
    
  echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Print Students</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">';
	
	$q="SELECT `cId`,`class_name` FROM class_index ORDER BY class+0 ASC";

$q_res=mysql_query($q);
echo '<h2 align="center">';




echo '</h2>';
echo "<br>";
echo "<br>";
echo "<br>";
 echo '<ol class="rounded-list">';
while($res=mysql_fetch_array($q_res))
{
	
	
	echo '<tr class="row-fluid" id="added_rows">
      <td><div class="row-fluid">
          <div class="span6">							                                                              
      <li><a href="users/stu_print_details.php?class_id='.$res[0].'">'.$res[1].'Class</a>                                                                    	                                 
	</div>
	</div><!-- end row-fluid  -->


</td>
</tr>';	 	 
}
echo '</div>
</div>
</div>';  
    
}



// FUNCTION FOR SELECT CLASS FOR ADMIT CARD

function manage_students_print_admit_card()
{
    
    
  echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Print Admit Card</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">';
	
	$q="SELECT `cId`,`class_name` FROM class_index ORDER BY class+0 ASC";

$q_res=mysql_query($q);
echo '<h2 align="center">';




echo '</h2>';
echo "<br>";
echo "<br>";
echo "<br>";
 echo '<ol class="rounded-list">';
while($res=mysql_fetch_array($q_res))
{
	
	
	echo '<tr class="row-fluid" id="added_rows">
      <td><div class="row-fluid">
          <div class="span6">							                                                              
      <li><a href="users/stu_print_admit_card.php?class_id='.$res[0].'">'.$res[1].'Class</a>                                                                    	                                 
	</div>
	</div><!-- end row-fluid  -->


</td>
</tr>';	 	 
}
echo '</div>
</div>
</div>';  
    
}

//student id card
function manage_students_print_id_card()
{
    
  echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Print Id Card</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">';
	
	$q="SELECT `cId`,`class_name` FROM class_index ORDER BY class+0 ASC";

$q_res=mysql_query($q);
echo '<h2 align="center">';

echo '</h2>';
echo "<br>";
echo "<br>";
echo "<br>";
 echo '<ol class="rounded-list">';
while($res=mysql_fetch_array($q_res))
{
	
	
	echo '<tr class="row-fluid" id="added_rows">
      <td><div class="row-fluid">
          <div class="span6">							                                                              
      <li><a href="users/stu_print_id_card.php?class_id='.$res[0].'">'.$res[1].'Class</a>                                                                    	                                 
	</div>
	</div><!-- end row-fluid  -->


</td>
</tr>';	 	 
}
echo '</div>
</div>
</div>';
}

//datewise student
function manage_students_datewise()
{
    
echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select dates</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
      
<br>
<br>
<br>
        '; 
        echo '
<form>
<div class="section">
<label>FROM</label>
<div>
<input type="tel" class="birthday" id="from_date"/>
</div>
</div>
<div class="section">
<label>To</label>
<div>
<input type="tel" class="birthday" id="to_date"/>
</div>
</div>

<div class="section last">
<div>
<button class="uibutton submit" type="button" onclick="show_student_dates()">Go</button>
</div>
</div>
</form>	
<div id="show_students_for_dates"></div>

        ';

        echo '</div>
        </div>
        </div>';	

 
}

function manage_students_class_datewise()
{
  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select dates</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
      
<br>
<br>
<br>
        '; 
        echo '
<form      action="users/print_student_class_admission_date.php"  method=get>';
//get list of all the classes
	 $get_list_classes=
	"SELECT cId,class_name
	FROM class_index
                       ORDER BY class+0 ASC ,section ASC";
	$exe_list_class=mysql_query($get_list_classes);
	echo '
	<div class="section">
	<label>Select class</label>
	<div>
	<select class="chzn-select" name="cid" tabindex="1" data-placeholder="Select class">
	<option value=""></option>';
	while($fetch_list_classes=mysql_fetch_array($exe_list_class))
	{
		echo '<option value="'.$fetch_list_classes[0].'">'.$fetch_list_classes[1].'</option>';
		
	}
	
	echo '
	</select>
	</div>
	</div>
<div class="section">
<label>FROM</label>
<div>
<input type="tel" class="birthday" id="from_date" name="from_date"/>
</div>
</div>
<div class="section">
<label>To</label>
<div>
<input type="tel" class="birthday"  name="to_date"   id="to_date"/>
</div>
</div>
<div class="section last">
<div>
<button class="uibutton submit" type="submit" >Go</button>
</div>
</div>
</form>	
<div id="show_students_for_dates"></div>

        ';

        echo '</div>
        </div>
        </div>';	
  

}




//function which shows a wizard which promotes a student
function students_promote()
{
echo '  <div class="row-fluid">
                    
<!-- Widget -->
<div class="widget  span12 clearfix">

<div class="widget-header">
    <span><i class="icon-wrench"></i>Follow steps to promote student in next session</span>
</div><!-- End widget-header -->	

<div class="widget-content">';
                if(isset($_GET['student_promoted']))
                {
                echo '<h5 align="center" style="color:grey">Students promoted</h5>';	

                }
                 if(isset($_GET['already_promoted']))
                {
                echo '<h5 align="center" style="color:grey">Students Already promoted</h5>';	

                }
                echo '
<form action="users/promote_student_insert.php">
<!-- Smart Wizard -->
<div id="wizard" class="swMain">
    <ul>
        <li><a href="#step-1">
        <label class="stepNumber">1</label>
        <span class="stepDesc">Step 1<br />
           <small>(Select Class)</small>
        </span>
    </a></li>
        <li><a href="#step-2">
        <label class="stepNumber">2</label>
        <span class="stepDesc">Step 2<br />
           <small>Uncheck students</small>
        </span>
    </a></li>
        <li><a href="#step-3">
        <label class="stepNumber">3</label>
        <span class="stepDesc">Step 3<br />
           <small>Choose session</small>
        </span>                   
     </a></li>
<li><a href="#step-4">
        <label class="stepNumber">4</label>
        <span class="stepDesc">Step 4<br />
           <small>Select class to promote</small>
        </span>                   
     </a></li>
    </ul>
    <div id="step-1" class="section">
    <label>Select class</label>
    <div>
';
    //get all the classes
    $get_classes=
    "SELECT *
    FROM class_index
    ORDER BY class+0 ASC,section ASC";
    $exe_classes=mysql_query($get_classes);
    echo '<select name="class_selected" id="class-selected" tabindex="2" onchange="populate_next_data(this.value)">
    <option value="">-select-</option>';

    while($fetch_classes=mysql_fetch_array($exe_classes))
    {
    echo '<option value="'.$fetch_classes[0].'">'.$fetch_classes['class_name'].'</option>';	
    }

echo '</select>
    
       </div>
       </div>
     <div id="step-2">
<h5 style="color:red" style="display:block" id="no_sel">Please select values from last step</h5>
<div id="show_data"></div>
    </div>                      
    <div id="step-3" class="section">
<label>Select Session</label>
<div>';
 //get all the session from the session table
 $get_sessions=
 "SELECT *
 FROM session_table";
 $exe_session=mysql_query($get_sessions);
echo '<select name="session_promotion">';
 while($fetch_sessions=mysql_fetch_array($exe_session))
 {
        echo '<option value="'.$fetch_sessions[0].'">'.$fetch_sessions[1].'</option>'; 


 }

 echo '
</select>
</div>		   
 </div>
  <div id="step-4" class="section">
<label>Select Class</label>
<div>
';
 //get all the session from the session table
 $get_sessions=
 "SELECT *
 FROM class_index
 ORDER BY class+0 ASC,section ASC";
 $exe_session=mysql_query($get_sessions);
echo '<select name="class_promote">';
 while($fetch_sessions=mysql_fetch_array($exe_session))
 {
        echo '<option value="'.$fetch_sessions[0].'">'.$fetch_sessions[1].'</option>'; 


 }

echo '
</select>
</div>		   
   </div>
</div><!-- End SmartWizard Content -->  
<div class="clearfix"></div>
</form>
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';	
	
}
//function to show class for selecting to downloadind xlx format info of student
function select_class_stu_for_xls()
{
	echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Manage Students</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">';
	
	$q="SELECT `cId`,`class_name` FROM class_index ORDER BY class+0 ASC";

$q_res=mysql_query($q);
echo '<h2 align="center">';




echo '</h2>';
echo "<br>";
echo "<br>";
echo "<br>";
 echo '<ol class="rounded-list">';
while($res=mysql_fetch_array($q_res))
{
	
	
	echo '<tr class="row-fluid" id="added_rows">
      <td><div class="row-fluid">
          <div class="span6">							                                                              
      <li><a href="users/stu_download_xls.php?class_id='.$res[0].'">'.$res[1].'Class</a>                                                                    	                                 
	</div>
	</div><!-- end row-fluid  -->


</td>
</tr>';	 	 
}
echo '</div>
</div>
</div>';
}
function add_new_student()
{
	
	//Code to retrieve all the classes from class_index
	
	$query_get_classes = "
	SELECT `cId`, `class_name`
	FROM `class_index`
	ORDER BY class+0 ASC
	";
	
	$execute_get_classes = mysql_query($query_get_classes);
	
	echo '
	<div class="row-fluid">                            
                       <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Add Student</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
                        ';
                        if(isset($_GET['usernameExists']))
                        {
                                echo "<h2 style=\"color:red\" align=\"center\">Username Already exists, please try again</h2>";
                        }
                        if(isset($_GET['addmissionexist']))
                        {
                                echo "<h4 style=\"color:red\" align=\"center\">Admission no. Already exists, please try again</h4>";
                        }
                        if(isset($_GET['passwordMismatch']))
                        {
                                echo "<h2 style=\"color:red\" align=\"center\">Password Mismatch, please try again</h2>";
                        }
                        if(isset($_GET['studentAdded']))
                        {
                                echo "<h2 style=\"color:green\" align=\"center\">Student added successfully</h2>";
                        }
                        if(isset($_GET['ERROR']))
                        {
                                echo "<h2 style=\"color:red\" align=\"center\">Error, Please try again</h2>";
                        }
	echo '
<form action="users/add_student.php" method="post" enctype="multipart/form-data">
<div class="section ">
  <label> Admission No.</label>   
  <div> 
 <input id="admission_no" name="admission_no" type="text" onKeyUp="check_admission_no();" onblur="check_admission_no();"  /> <label id="show_availability1"></label></td>
 </div>
 </div>
      <div class="section ">
    <label> Name<small></small></label>   
    <div> 
   <input type="text" class="validate[required] medium" name="name"/>
     </div>
     </div>					
    <div class="section">
   <label>Username</label> 
   <div>
<input type="text" onKeyUp="check_username_student();" onblur="check_username_student();"  name="username" id="username"  class="validate[required,minSize[3],maxSize[20],] medium"  /> <label id="show_availability"></label></td> </div>
  <label>Password</label>
   <div><input type="password"  class="validate[required,minSize[3]] medium"  name="password" id="password"  />
  </div>
 <label>Confirm Password</label> 
   <div> <input type="password" class="validate[required,equals[password]] medium"  name="confirm_password" id="passwordCon"  />
  </div>
  </div>				
	
     <div class="section">
		<label>Gender <small></small></label>   
	    <div>
		<div class="radiorounded">
		<input type="radio" name="gender" id="radio-1" value="male" />
		<label for="radio-1" title="Male"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="gender" id="radio-2" value="female"/>
		<label for="radio-2"  title="Female"></label>
	   </div>
       </div>
	   </div>
		 <div class="section">
		<label>EWS <small></small></label>   
	    <div>
		<div class="radiorounded">
		<input type="radio" name="ews" id="radio-3" value="yes" />
		<label for="radio-3" title="YES"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="ews" id="radio-4" value="no"/>
		<label for="radio-4"  title="NO"></label>
	   </div>
       </div>
	   </div>

        <div class="section">
		   <label>Date of Birth<small></small></label>   
		  <div><input type="text"  id="birthday" class=" birthday  small " name="dob"  />
		  </div>
	      </div>
		  <div class="section">
		   <label>Date of Admission<small></small></label>   
		  <div><input type="text"  id="birthday1" class=" birthday  small " name="doa"  />
		  </div>
	      </div>
		
            <div class="section ">
			<label> Email ID<small></small></label>   
			<div> 
			<input type="text" class="validate[required,custom[email]] medium" name="email_id" id="e_required" onblur="checkemail(this.value)">
		   </div>
		   </div>
           				
			<div class="section  numericonly ">
		   <label>Contact No<small></small></label>   
		   <div> 
		  <input type="text" class="validate[required] medium" name="phone_no"/>
		  </div>
		  </div>
		
         <div class="section ">
  <label>Class<small></small></label>   
  <div>   
  <select data-placeholder="Select a Class..." class="chzn-select" tabindex="2" name="students_class">
  <option value=""></option> 		  
  ';		  
	
		  while($get_classes = mysql_fetch_array($execute_get_classes))
		  {
		 echo '
		  <option value="'.$get_classes[0].'">'.$get_classes[1].'</option>';
		  }		
		  echo'</select>
             </div>
             </div>	
             
             <div class="section ">
			  <label>Father\'s Name<small></small></label>   
			  <div> 
			 <input type="text" class="validate[required] medium" name="father_name" input id="father_name"/>
             </div>
             </div>	
			 
			 <div class="section ">
			  <label>Father\'s occupation<small></small></label>   
			  <div> 
			 <input type="text" class="validate[required] medium" name="father_occupation" input id="father_ocupation"/>
             </div>
             </div>	
			 
			 <div class="section ">
			  <label>Father\'s Annual Income<small></small></label>   
			  <div> 
			 <input type="text" class="validate[required] medium" name="father_annual_income" input id="father_annual_income"/>
             </div>
             </div>	
             
             <div class="section ">
			  <label>Mother\'s Name<small></small></label>   
			  <div> 
			 <input type="text" class="validate[required] medium" name="mothers_name" input id="mothers_name"/>
             </div>
             </div>
			 
			 <div class="section ">
			  <label>Mother\'s Occupation<small></small></label>   
			  <div> 
			 <input type="text" class="validate[required] medium" name="mothers_occupation" input id="mothers_occupation"/>
             </div>
             </div>
			 
			 <div class="section ">
			  <label>Mother\'s Annual Income<small></small></label>   
			  <div> 
			 <input type="text" class="validate[required] medium" name="mothers_annual_income" input id="mothers_annual_income"/>
             </div>
             </div>
             
              <div class="section ">
			<label>Parent\'s Email ID<small></small></label>   
			<div> 
			<input type="text" class="validate[required,custom[email]] medium" name="guardians_email" id="guardians_email" onblur="checkemail(this.value)">
		   </div>
		   </div>
           
           <div class="section">
		  <label>Local Address <small></small></label>   
		  <div > <textarea name="local_address" id="Textareaelastic"  class="medium"  cols="" rows=""  ></textarea>
		   </div>                                              
		    </div>
		  <div class="section ">
  <label>Religion<small></small></label>   
  <div>
  <select data-placeholder="Select Religion..." class="chzn-select" tabindex="2" name="religion">
								  <option value=""></option> 	
								 <option value="Hindu">Hindu</option>
								  <option value="Muslim">Muslim</option>
								  <option value="Christian">Christian</option>
								  <option value="Sikh">Sikh</option>
								  <option value="Jain">Jain</option>
								  <option value="Buddhist">Buddhist</option>
								  <option value="Others">Others</option>
								 
								</select>
                                </div>
                                 </div>
			
         <div class="section">
		<label>Category <small></small></label>   
	    <div>
		<div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="general" />
		<label for="radio-1" title="General"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-2" value="obc"/>
		<label for="radio-2"  title="OBC"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="st" />
		<label for="radio-1" title="ST"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="sc" />
		<label for="radio-1" title="SC"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="other" />
		<label for="radio-1" title="Other"></label>
	   </div>
       </div>
	   </div>
      <div class="section ">
  <label>Blood Group<small></small></label>   
  <div>
  <select data-placeholder="Select Your blood Group..." class="chzn-select" tabindex="2" name="blood_group">
							 <option value=""></option> 	
								 <option value="O +VE">O +VE</option>
								  <option value="A +VE">A +VE</option>
								  <option value="B +VE">B +VE</option>
								  <option value="O -VE">O -VE</option>
								  <option value="A -VE">A -VE</option>
								  <option value="B -VE">B -VE</option>
								  <option value="AB +VE">AB +VE</option>
								  <option value="AB -VE">AB -VE</option>
								</select>
                                </div>
                                 </div>
			
		<div class="section ">
       <label> Picture<small></small></label>   
       <div> 
	   <input type="file" name="image" class="fileupload" />
       </div>
       </div>
						
						<br></br>
       <tr><div class="section last">	
	<button class="uibutton icon  add " type="submit">Add Student</button> 
	<button class="uibutton icon special cancel " type="reset">Cancel</button> 	
						
				</form>
				</table>
				
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>
			 </div><!-- row-fluid column-->
     </div><!--  end widget-content -->
     </div><!-- widget  span12 clearfix-->
     </div><!-- row-fluid -->
	 ';
}

function view_student_details()
{
	$studentSid = $_GET['student_id'];
	//This function is used to view detials of the student
	
	$privilege = $_SESSION['priv'];
/*	if(($privilege!=0)&&($privilege!=1))
	{
		header("Location: error.php");
	}
	*/
	//Query to fetch the login details of the user
	
	$query_login_details = mysql_query("SELECT `username`,`privilege` FROM `login` WHERE `lId`='$studentSid' ");
	$login_details = mysql_fetch_array($query_login_details);
	$username = $login_details[0];
	$privilege = $login_details[1];
	
	//Query to get the class of the student users
                                 $query_get_class = "
		SELECT class_index.class_name, class_index.class
		FROM class_index
		INNER JOIN class
		ON class.classId = class_index.cId
		WHERE class.sId = $studentSid AND class.session_id = ".$_SESSION['current_session_id']."
	";
	$execute_get_class = mysql_query($query_get_class);
	$class = mysql_fetch_array($execute_get_class);
	$classs= $class[1];
	$class = $class[0];
	
	
	//echo $username.' '.$privilege;
	
	//Query to fetch personal details of the user
	 $query_personal_details = mysql_query("SELECT `Name`,`DOB`,`Email`,`Local Address`,`Phone No`,`gender`,`image`, `Father's Name`, `Mother's Name`, `Guardian's Contact`,`Guardian's Email`,`admission_no`,`Blood Group`,`sId`,`category`,`religion`,`father_occupation`,`father_annual_income`,`mothers_occupation`,`mothers_annual_income`,`admission_date`,`ews`  FROM `student_user` WHERE `sId` = '$studentSid' ");

	$personal_details = mysql_fetch_array($query_personal_details);
	$name = $personal_details[0];
	$dob = $personal_details[1];
	$email = $personal_details[2];
	$local_address = $personal_details[3];
	$phone_no = $personal_details[4];
	$sex = $personal_details[5];
	$image = $personal_details[6];
	$father_name = $personal_details[7];
	$mother_name = $personal_details[8];
	$guardian_contact = $personal_details[9];
	$guardian_email = $personal_details[10];
	$admission_no = $personal_details[11];
	$blood_group = $personal_details[12];
	$sId = $personal_details[13];
	$category = $personal_details[14];
	$religion = $personal_details[15];
	$father_occupation = $personal_details[16];
	$father_annual_income = $personal_details[17];
	$mothers_occupation = $personal_details[18];
	$mothers_annual_income = $personal_details[19];
	$doa=$personal_details['admission_date'];
        $ews=$personal_details['ews'];
	
	//echo '<br/>'.$name.' '.$dob.' '.$email.' '.$local_address.' '.$phone_no.' '.$sex.' '.$image;
					
					
		echo '	
	
		<div class="row-fluid">

					  <!-- Table widget -->
					  <div class="widget  span12 clearfix">
					  
					  <div class="widget-header">
						<span class="small s_color"> Manage Student </span>
					   </div> <!-- End widget-header -->	
						
						<div class="widget-content">';
						
						if(isset($_GET['detailsUpdateSuccess']))
				           {
					echo "<h4 style=\"color:green\" align=\"center\">Details updated successfully</h4>";
				          }
						echo'
						  <table class="table table-bordered student_details">
						  <tr>
						  <td><b/>PICTURE<br><br><br><br><br><br></td>
						  <td><image src="user_image/'.$image.'" width="200px" height="200px" /></td>
						  </tr>
						   <tr>
						  <td><b/>Admission Number</td>
						  <td>'.strtoupper($admission_no).'</td>
						  </tr>
						   <tr>
						  <td><b/>User Name</td>
						  <td>'.strtoupper($username).'</td>
						  </tr>
						  <tr>
						  <td><b/>Student Name</td>
						  <td>'.strtoupper($name).'</td>
						  </tr>
						  <tr>
						  <td><b/>Class</td>
						  <td>'.strtoupper($class).'</td>
						  </tr>
						  <tr>
						  <td><b/>Gender</td>
						  <td>'.strtoupper($sex).'</td>
						  </tr>
						  <tr>
						  <td><b/>Date of Birth</td>
						  <td>'.$dob.'</td>
						  </tr>
                           <tr>
						  <td><b/>Email address</td>
						  <td>'.strtoupper($email).'</td>
						  </tr>
                           <tr>
						  <td><b/>Local Address</td>
						  <td>'.strtoupper($local_address).'</td>
						  </tr>
                          <tr>
						  <td><b/>Phone Number</td>
						  <td>'.$phone_no.'</td>
						  </tr>
						   <tr>
						  <td><b/>Religion</td>
						  <td>'.strtoupper($religion).'</td>
						  </tr>
						  <tr>
						  <td><b/>Category</td>
						  <td>'.strtoupper($category).'</td>
						  </tr>
                          <tr>
						  <td><b/>Blood Group</td>
						  <td>'.strtoupper($blood_group).'</td>
						  </tr>
						  <tr>
						  <td><b/>Father Name</td>
						  <td>'.strtoupper($father_name).'</td>
						  </tr>
						  <tr>
						  <td><b/>Father Occupation</td>
						  <td>'.strtoupper($father_occupation).'</td>
						  </tr>
						  <tr>
						  <td><b/>Father Annual Income</td>
						  <td>'.strtoupper($father_annual_income).'</td>
						  </tr>
						  <tr>
						  <td><b/>Mother Name</td>
						  <td>'.strtoupper($mother_name).'</td>
						  </tr>
						  <tr>
						  <td><b/>Mother Occupation</td>
						  <td>'.strtoupper($mothers_occupation).'</td>
						  </tr>
						  <tr>
						  <td><b/>Mother Annual Income</td>
						  <td>'.strtoupper($mothers_annual_income).'</td>
						  </tr>
						  <tr>
						  <td><b/>Guardian\'s Email.</td>
						  <td>'.strtoupper($guardian_email).'</td>
						  </tr>
                                                  <tr>
						  <td><b/>Date of Admission</td>
						  <td>'.$doa.'</td>
						  </tr>
                                                  <tr>
						  <td><b/>EWS</td>
						  <td>'.strtoupper($ews).'</td>
						  </tr>

						  
	
	</table>
	<div class="entry">
	<div class="sep"></div>			
	<a href="manage_user_edit_student_details.php?student_id='.$sId.'"><button type="submit" class="add">Edit Details</button></a>
	<a href="manage_users_view_students.php"><button type="button" >Back</button></a>
</div>
<br/>';

if($classs == 10 || $classs == 12)
{
echo'
<a href="users/provisional_certificate.php?student_id='.$studentSid.'">
    <button class="uibutton special " rel="1" type="submit">Provisional Certificate</button>
</a>

<a href="users/character_certificate.php?student_id='.$studentSid.'">
	<button class="uibutton normal " rel="1" type="submit">Character Certificate</button>
</a>';
}
	
echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	
	</div>
	<div class="entry">
	
	<div id="txtHint"></div>
	<div id="Hint"></div>	
	
	</div>
	
	';							
}

function edit_students_details()
{
	//Code to retrieve all the classes from class_index
	
  $query_get_classes = "
	SELECT `cId`, `class_name`
	FROM `class_index`
	INNER JOIN class
	ON class.classId= class_index.cId
	WHERE class.sId=".$_GET['student_id']."
	ORDER BY class+0 ASC
	";
	
	$execute_get_classes = mysql_query($query_get_classes);
	  $get_classes = mysql_fetch_array($execute_get_classes);
	   $class_name=$get_classes['class_name'];	
          $class=$get_classes['cId'];
	$sId = $_GET['student_id'];
  $queryDetailsStudent = "
	SELECT `Name`,`Email`,`DOB`,`Phone No`,`Father's Name`, `Mother's Name`,`Local Address`,`Guardian's Contact`,`Guardian's Email`,`admission_no`,`gender`,`Blood Group`,student_user.category,student_user.religion ,`admission_date`,`ews`
	FROM student_user
        INNER JOIN class
        ON class.sId=student_user.sId
	
	WHERE student_user.sId = '$sId'
	
	";
	$getDetailsStudent = mysql_query($queryDetailsStudent);
	$detailsStudent = mysql_fetch_array($getDetailsStudent);
	
	$name = $detailsStudent['Name'];
	$email = $detailsStudent['Email'];
	$dob = $detailsStudent['DOB'];
	$phone_no = $detailsStudent['Phone No'];
	$father_name = $detailsStudent[4];
	$mother_name = $detailsStudent[5];
	$local_address = $detailsStudent['Local Address'];
	$guardian_contact = $detailsStudent[7];
	$guardian_email = $detailsStudent[8];
	$admission_no = $detailsStudent['admission_no'];
	$gender = $detailsStudent['gender'];
	$blood_group = $detailsStudent['Blood Group'];
	//$username = $detailsStudent['username'];
	//$privilege = $detailsStudent['privilege'];
	$category = $detailsStudent['category'];
	$religion = $detailsStudent['religion'];
        $doa = $detailsStudent['admission_date'];
        $ews = $detailsStudent['ews'];
	
	echo '
			<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Edit Student</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
				';
				
				
	echo '
			<form action="users/update_student.php" method="post" enctype="multipart/form-data">
				<fieldset >
				<input type="hidden" name="student_id" value="'.$sId.'" />
						  <div class="section ">
						  <label>Admission Number</label>   
						  <div> 
						 <input id="admission_no" name="admission_no" type="text" value="'.$admission_no.'" >
						  </div>
						 </div>
						 
						 <div class="section ">
						  <label>Name</label>   
						  <div> 
						<input id="name" name="name" type="text" value="'.$name.'" required="required" >
						  </div>
						 </div>
						 
						
						 
						<div class="section ">
						<label>Gender</label>   
						<div> 
						<input id="gender" name="gender" type="text" value="'.$gender.'" >
						</div>
					   </div>				
					
					<div class="section ">
					<label>Date Of Birth</label>   
					<div> 
					<input id="dob" name="dob" type="text"  class="birthday" value="'.$dob.'" >
					</div>
				   </div>
                                   <div class="section ">
						<label>EWS</label>   
						<div> 
						<input id="ews" name="ews" type="text" value="'.$ews.'" >
						</div>
					   </div>		
					
				   <div class="section ">
					<label>Date Of Admission</label>   
					<div> 
					<input id="doa" name="doa" type="text"  class="birthday" value="'.$doa.'" >
					</div>
				   </div>
				   <div class="section ">
					<label>Email Id</label>   
					<div> 
					<input id="email_id" name="email_id" type="text" onblur="checkemail(this.value)" value="'.$email.'" >
					</div>
				   </div>
					
				<div class="section ">
				<label>Phone Number</label>   
				<div> 
				<input id="phone_no" name="phone_no" type="text" onblur="phonenumber_validate(this.value);" value="'.$phone_no.'" >
				</div>
			   </div>
			   
			   <div class="section ">
				<label>Class</label>   
				<div> 
				<select data-placeholder="Select Class.." class="chzn-select" tabindex="2" 
				  name="students_class">';
								
					echo '<option value="'.$class.'">'.$class_name.'</option>';				 			
				                 $get_class_details="SELECT  cId,class_name
                                                                                              FROM `class_index`
                                                                                             WHERE cId <> ".$get_classes[0]."
                                                                                             ORDER BY class, section ASC";
                                                                                             $exe_class=mysql_query($get_class_details);
                                                                                            while($fetch_class_details=mysql_fetch_array($exe_class))

                                                                                                     {

                                                    echo '<option value="'.$fetch_class_details[0].'">'.$fetch_class_details[1].'</option>';				 


								
				 }
					echo '
								</select>
				</div>
			   </div>
			   
			   <div class="section ">
			  <label>Father\'s Name</label>   
			  <div> 
			  <input type="text"  name="father_name" id="father_name" value="'.$father_name.'">
			  </div>
			 </div>
			 
			  <div class="section ">
			<label>Mother\'s Name</label>   
			<div> 
			<input type="text"  name="mother_name" id="mother_name" value="'.$mother_name.'">
			</div>
		   </div>
		   
		  <div class="section ">
		  <label>Parent\'s Email ID</label>   
		  <div> 
		  <input id="guardians_email" name="guardians_email" type="text" onblur="checkemail(this.value)" value="'.$guardian_email.'" >
		  </div>
		 </div>
		 
		  <div class="section ">
		  <label>Local Address</label>   
		  <div> 
		  <textarea name="local_address">'.$local_address.'</textarea>
		  </div>
		 </div>
		 	  <div class="section ">
  <label>Religion<small></small></label>   
  <div>
  <select data-placeholder="Select Your Religion..." class="chzn-select" tabindex="2" name="religion">
							<option value="'.$religion.'" >'.$religion.'</option>';
							    if($religion!="Hindu")
								{
									echo ' <option value="Hindu">Hindu</option>';
								}
								  if($religion!="Muslim")
								{
									echo '<option value="Muslim">Muslim</option>';
								}
								  if($religion!="Christian")
								{
									echo ' <option value="Christian">Christian</option>';
								}
								  if($religion!="Sikh")
								{
									echo ' <option value="Sikh">Sikh</option>';
								}
								  if($religion!="Jain")
								{
									echo '<option value="Jain">Jain</option>';
								}
								  if($religion!="Buddhis")
								{
									echo '<option value="Buddhist">Buddhist</option>';
								}
								  if($religion!="Others")
								{
									 echo '<option value="Others">Others</option>';
								}
								
							/*	echo ' <option value="Hindu">Hindu</option>';
								 echo '<option value="Muslim">Muslim</option>';
								  echo ' <option value="Christian">Christian</option>';
								echo ' <option value="Sikh">Sikh</option>';
								 echo '<option value="Jain">Jain</option>';
								 echo '<option value="Buddhist">Buddhist</option>';
								 echo '<option value="Others">Others</option>';*/
								 
								echo '</select>
                                </div>
                                 </div>
		
		 
		 
		  <div class="section">
		<label>Category <small></small></label>   
	    <div>';
		if($category == 'general')
		{
		echo'
		<div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="general" checked="checked"/>
		<label for="radio-1" title="General"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-2" value="obc"/>
		<label for="radio-2"  title="OBC"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="st" />
		<label for="radio-1" title="ST"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="sc" />
		<label for="radio-1" title="SC"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="other" />
		<label for="radio-1" title="Other"></label>
	   </div>';
		}
		else if($category=='obc')
		{
		echo'
		<div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="general" />
		<label for="radio-1" title="General"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-2" value="obc" checked="checked"/>
		<label for="radio-2"  title="OBC"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="st" />
		<label for="radio-1" title="ST"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="sc" />
		<label for="radio-1" title="SC"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="other" />
		<label for="radio-1" title="Other"></label>
	   </div>';	
			}
		else if($category == 'st')
		{
		echo'
		<div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="general" />
		<label for="radio-1" title="General"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-2" value="obc"/>
		<label for="radio-2"  title="OBC"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="st" checked="checked" />
		<label for="radio-1" title="ST"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="sc" />
		<label for="radio-1" title="SC"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="other" />
		<label for="radio-1" title="Other"></label>
	   </div>';
		}
		else if($category=='sc')
		{
		echo'
		<div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="general" />
		<label for="radio-1" title="General"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-2" value="obc"/>
		<label for="radio-2"  title="OBC"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="st" />
		<label for="radio-1" title="ST"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="sc"  checked="checked"/>
		<label for="radio-1" title="SC"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="other" />
		<label for="radio-1" title="Other"></label>
	   </div>';	
			}
			else if($category=='other')
		{
		echo'
		<div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="general" />
		<label for="radio-1" title="General"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-2" value="obc"/>
		<label for="radio-2"  title="OBC"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="st" />
		<label for="radio-1" title="ST"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="sc" />
		<label for="radio-1" title="SC"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="other"   checked="checked" />
		<label for="radio-1" title="Other"></label>
	   
	   </div>';	
			}
			else if($category=="")
		{
		echo'
		<div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="general" />
		<label for="radio-1" title="General"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-2" value="obc"/>
		<label for="radio-2"  title="OBC"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="st" />
		<label for="radio-1" title="ST"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="sc" />
		<label for="radio-1" title="SC"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="category" id="radio-1" value="other"    />
		<label for="radio-1" title="Other"></label>
	   
	   </div>';	
			}
	   echo'
       
	   </div>
	   </div>
		  <div class="section ">
		  <label>Blood Group</label>   
		  <div> 
		  <select data-placeholder="Select Your blood_group..." class="chzn-select" tabindex="2" 
		 name="blood_group" >
		<option value="'.$blood_group.'">'.$blood_group.'</option>';
		                        if($blood_group!="A +VE")
								{
									 echo '<option value="A +VE">A +VE</option>';
								}
								  if($blood_group!="B +VE")
								{
									echo '  <option value="B +VE">B +VE</option>';
								} 
								 if($blood_group!="O -VE")
								{
									 echo ' <option value="O -VE">O -VE</option>';
								}  if($blood_group!=" -VE")
								{
									 echo '  <option value="A -VE">A -VE</option>';
								}
								  if($blood_group!="B -VE")
								{
									echo '  <option value="B -VE">B -VE</option>';
								}
								  if($blood_group!="AB +VE")
								{
									echo ' <option value="AB +VE">AB +VE</option>';
								}
								  if($blood_group!="AB -VE")
								{
									echo ' <option value="AB -VE">AB -VE</option>';
								}
							
								
								 
								 
								
								 
								  
								  
								 echo '</select>
		  </div>
		 </div>
		 
		 <div class="section ">
				<label>Picture</label>   
				<div> 
			  <a href="manage_users_student_change_picture.php?student_id='.$sId.'"><strong>Click Here to Change the Picture</strong></a>
				</div>
			   </div>
			 
			  <div class="section last">
				<div>
				  <button type="submit" class="add">Update Student</button>				  
				  <button type="reset" class="cancel">Cancel</button>
			   </div>
			   </div>

				</fieldset>
				</form>
				
				
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>
				</div><!-- row-fluid column-->
     </div><!--  end widget-content -->
     </div><!-- widget  span12 clearfix-->
     </div><!-- row-fluid -->';
	
	
	
}

function view_teachers()
{
		//This function will display the dashboard of managing Teacher users.
	
	$query_get_teachers_details = "
	SELECT users.Name, users.uId
	FROM users
	
	INNER JOIN `login`
	ON login.lId = users.uId
	
	WHERE login.privilege = 2
	
	ORDER BY users.Name ASC
	";
	
	$execute_teachers_details = mysql_query($query_get_teachers_details);
	echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Manage Teachers</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	<h2> Welcome</h2>
';	
		if(isset($_GET['delete_teacher']))
				{
					echo '<p style="color:red; font-weight:BOLD" align="center">Deletion successful !</p>';
				}
				
				if(isset($_GET['classSubjectRelationDeleted']))
				{
					echo '<p style="color:red; font-weight:BOLD" align="center">Deletion successful !</p>';
				}
				if(isset($_GET['done']))
				{
					
				echo '<h5 align="center">Added</h5>';	
				}
				
		echo'		
					
    <a href="manage_user_add_new_teacher.php">
    <button class="uibutton icon normal add "  rel="1" type="submit" >Add New Teacher</button></a>
		
    <a href="manage_add_designation.php">
    <button class="uibutton icon special add "  rel="1" type="submit" >Add Designation</button></a>
	
	<a href="manage_assign_class_teachers.php">
    <button class="uibutton normal "  rel="1" type="submit" >Assign Class Teachers</button></a>
	
	</div>';
	
		
	
	echo' 		                                                                         	 
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
    <tr>		   
	<th>Sno.</th>
	<th>Name</th>
	<th>Classes</th>
	<th>Subjects</th>
	<th>(Add/delete) class/subject(s)</th>
					
    </tr>
    </thead>	 	
	<tbody align="center">  ';
						   
					//Loop to generate the table
					
					$sno = 0;
					while($get_name_teachers = mysql_fetch_array($execute_teachers_details))
					{
						echo '<tr>';
						
						//Query to get the classes and the subjects details
						$query_class_subject_details = "
							SELECT subject.subject, teachers.class ,teachers.Id
							FROM teachers
							
							INNER JOIN subject
							ON teachers.subject_id = subject.subject_id
							
							WHERE teachers.tId = $get_name_teachers[1] AND session_id = ".$_SESSION['current_session_id']."
						";
						$execute_class_subject_details = mysql_query($query_class_subject_details);
						
						//Arrays to store the class and subjects
						$class = array();
						$subject = array();
						$ctr = 0;
						
						while($class_subject_details = mysql_fetch_array($execute_class_subject_details))
						{
							//Loop to get the class and subjects of a particular teacher
							
							$subject[$ctr] = $class_subject_details[0]; 	//Stores the subject details
							$class[$ctr] = $class_subject_details[1];		//Stores the class details
							$ctr++;
						}
						
						echo '<td>'.++$sno.'</td>
						
						';
						
						//Display the name of the teacher
						echo '<td align="center"><a href="manage_users_view_teachers_details.php?id='.$get_name_teachers[1].'">'.$get_name_teachers[0].'</td>';
						
						//Display the classes
						echo '<td>';
						$check = 0;
						for($i=0;$i<$ctr;$i++)
						{
							if($check == 0)
							{
								echo $class[$i].'<br>';
								++$check;
							}
							else
							{
								echo ','.$class[$i].'<br>';
							}
						}
						echo '
							<a href="#"><img src="img/edit.gif" id="'.$sno.'classes" style="visibility:hidden" align="right" onmouseover="change_edit_symbol_over(\''.$sno.'classes\')" onmouseout="change_edit_symbol_out(\''.$sno.'classes\')" onclick="redirect_sybmbol(\''.$sno.'subjects\',\''.$get_name_teachers[0].'\',\''.$get_name_teachers[1].'\')" /></a>
						</td>';
						
						
						//Display the Subjects
						echo '<td>';
						$check = 0;
						for($i=0;$i<$ctr;$i++)
						{
							if($check == 0)
							{
								echo $subject[$i].'<br>';
								++$check;
							}
							else
							{
								echo ','.$subject[$i].'<br>';
							}
						}
						echo '
						<a href="#"><img src="img/edit.gif" id="'.$sno.'subjects" style="visibility:hidden" align="right" onmouseover="change_edit_symbol_over(\''.$sno.'subjects\')" onmouseout="change_edit_symbol_out(\''.$sno.'subjects\')" onclick="redirect_sybmbol(\''.$sno.'subjects\',\''.$get_name_teachers[0].'\',\''.$get_name_teachers[1].'\')" /></a>
						</td>
						<td><span class="tip"><a href="show_class_subject.php?id_tea='.$get_name_teachers[1].'" class="pop_box" original-title="Add"><img src="images/icon/icon_edit.png"></a></span>
						<span class="tip"><a href="admin_delete_class_sub_tea.php?id_tea='.$get_name_teachers[1].'"data-name="delete name" original-title="Delete"><img src="images/icon/icon_delete.png"></a></span></td>
						
						';
						
						echo '</tr>
						
						
						
						';
					}
					
					
				
				
	echo'		

					</tbody>
				</table>				
		  </div><!-- row-fluid column-->
		  </div><!--  end widget-content -->
		  </div><!-- widget  span12 clearfix-->
		  </div><!-- row-fluid -->
';													
}
//function to show popup of class and subjects
/*function show_class_sub()
{
	$tid=$_POST['tid'];
	$class_name=$_POST['class'];
	$sub_id =$_POST['sid'];
	echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Manage Teachers</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">';
	
	
	echo '</div>
	</div>
	</div>';		
	
}*/
//function to delete class or subjects
function delete_class_sub()
{
	$teacher_id=$_GET['id_tea'];
	
	echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Manage Teachers</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">';
	//get all the contents of the id
	$get_details_id_tea=
"SELECT subject.subject,teachers.class,subject.subject_id,class_index.cId
FROM teachers
INNER JOIN subject 
ON subject.subject_id = teachers.subject_id
INNER JOIN class_index
ON class_index.class_name = teachers.class
WHERE tId = ".$teacher_id."";
$exe_details=mysql_query($get_details_id_tea);
$details=array();
$subjects = array();
$count=0;

	
echo '<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
    <tr>
	<th>Classes</th>
	<th>Subjects</th>
	<th>Action</th>	
	</thead>
	<tbody align="center">';
 while($details_fetch=mysql_fetch_array($exe_details))
		  {
        $subject[$count] = $details_fetch[0];
		$classes[$count] = $details_fetch[1];
		echo '<tr>
		<td>'.$details_fetch[1].'</td>
		<td>'.$details_fetch[0].'</td>
		<td id="del_'.$details_fetch[3].''.$details_fetch[2].'"><span><a onclick="delete_this(\''.$details_fetch[1].'\','.$details_fetch[3].','.$details_fetch[2].','.$teacher_id.');" "data-name="delete name" original-title="Delete"><img src="images/icon/icon_delete.png"></a></span></td></td>
		</tr>';
		
		$count++;
		
		  }
	echo '
	</tbody>
	</table>
	</div>
	</div>
	</div>
	';	
}
function class_subject_relationship()
{
	//Function to who and edit class subject relationship
	
	$teacher_id = $_GET['teacher_id'];
	$teacher_name = $_GET['teacher_name'];
	//Query to get the details of class and subject
	
	$query_class_subject_details = "
							SELECT subject.subject, teachers.class, teachers.Id
							FROM teachers
							
							INNER JOIN subject
							ON teachers.subject_id = subject.subject_id
							
							WHERE teachers.tId = $teacher_id AND session_id = ".$_SESSION['current_session_id']."
						";
	$execute_class_subject_details = mysql_query($query_class_subject_details);
	
	echo '	
	
			<div class="full_w">
				<div class="h_title">Manage Teachers</div>
				<h2>Class Subject relationship of '.$teacher_name.'</h2>';
				
				if(isset($_GET['valueAdded']))
				{
					echo '<p align="center"></p>';
				}
				
	echo'			
				<div class="entry">
					<div class="sep"></div>			
					<a href="manage_user_add_class_subject_teacher.php?teacher_id='.$teacher_id.'&teacher_name='.$teacher_name.'"><button type="submit" class="add">Add Class Teacher Relationship</button></a>
					
					<a href="#"><button type="submit" class="cancel" onclick="delete_teacher('.$teacher_id.');">Delete Teacher</button></a>
					
				</div>
				<table border="1">
					<thead>
						<tr>
							<th scope="col">Sno.</th>
							<th scope="col">Class</th>
							<th scope="col">Subject</th>
							
						</tr>
					</thead>
						
					<tbody>';
					//Loop to generate the table
					$sno = 0;
					while($class_subject_details = mysql_fetch_array($execute_class_subject_details))
					{
						echo '<tr>';
						
						echo '<td>'.++$sno.'</td>';
						echo '<td onmouseover="show_edit(\''.$sno.'subjects\')" onmouseout="hide_edit(\''.$sno.'subjects\')">'.$class_subject_details[1].'</td>';
						echo '<td onmouseover="show_edit(\''.$sno.'subjects\')" onmouseout="hide_edit(\''.$sno.'subjects\')">'.$class_subject_details[0].'
						<a href="#"><img src="img/edit.gif" id="'.$sno.'subjects" style="visibility:hidden" align="right" onmouseover="change_edit_symbol_over(\''.$sno.'subjects\')" onmouseout="change_edit_symbol_out(\''.$sno.'subjects\')" onclick="redirect_edit_class_teacher(\''.$class_subject_details[2].'\');" /></a>
						</td>';
						
						echo '</tr>';
					}
					
					echo	
					'</tbody>
				</table>
				
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>';
}

function edit_class_subject_relationship()
{
	
	$id = $_GET['id'];
	
	//Query to get the current subject class relation
	$query_get_class_subject = "
		SELECT subject.subject, teachers.class
		FROM teachers
		
		INNER JOIN subject
		ON subject.subject_id = teachers.subject_id
		
		WHERE teachers.Id = $id
	";
	
	$execute_get_class_subject = mysql_query($query_get_class_subject);
	
	$get_class_subject = mysql_fetch_array($execute_get_class_subject);
	
	$subject = $get_class_subject[0];
	$class = $get_class_subject[1];
	
	
	//Query to get all the subjects
	$query_get_subjects = "
		SELECT subject_id,subject
		FROM subject
		ORDER BY subject ASC
	";
	$execute_get_subjects = mysql_query($query_get_subjects);
	
	//Query to get all the classes
	$query_get_classes = "
		SELECT cId, class_name
		FROM class_index
		ORDER BY class+0 ASC
	";
	$execute_get_classes = mysql_query($query_get_classes);
	
	echo '	
	
			<div class="full_w">
				<div class="h_title">Edit Class/Subject </div>
				<h2 align="center">';
				
				if(isset($_GET['UpdateSuccess']))
				{
					//Succes Update
					echo '<p style="color:Green">Update successful</p>';
				}
				if(isset($_GET['UpdateError']))
				{
					//Error Update
					echo '<p style="color:RED">Update Error</p>';
				}
				
			echo'	
				</h2>
				
				<div class="entry">
					<div class="sep"></div>			
					
					<table style="width:250px">
						<tr>
							<td ><strong>Current Class</strong></td><td>'.$class.'</td>
						</tr>
						<tr>
							<td><strong>Current Subject</strong></td><td>'.$subject.'</td>
						</tr>
						
						<form action="users/update_class_subject_teacher.php" method="post" >
						
						<input type="hidden" name="id" value="'.$id.'" />
						
						<tr>
							<td><strong>Select Class</strong></td><td>';
							echo '<select name="class" >';
							while($get_classes = mysql_fetch_array($execute_get_classes))
							{
								//Display all the classes in the select list
								echo '
									<option value="'.$get_classes[1].'">'.$get_classes[1].'</option>
								';
							}
							echo '</select>';
							
					echo'	</td>
						</tr>
						<tr>
							<td><strong>Select Subject</strong></td><td>';
							echo '<select name="subject" >';
							while($get_subjects = mysql_fetch_array($execute_get_subjects))
							{
								//Display all the subjects in the select list
								echo '
									<option value="'.$get_subjects[0].'">'.$get_subjects[1].'</option>
								';
							}
							echo '</select>';
					echo'	</td>
						</tr>
						
						<tr>
						<td><input type="submit" value="Update" />
						</form></td>
						
						<td>
							<a href="users/delete_class_subject_realtionship.php?id='.$id.'">Delete</a>
						</td>
						</tr>
						
						
					</table>
					
				</div>
				
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>';
}

function add_class_subject_relationship()
{
	//Function to add new Class Subject Relationship
	
	$teacher_id = $_GET['teacher_id'];
	$teacher_name = $_GET['teacher_name'];
	
	//Query to get all the subjects
	$query_get_subjects = "
		SELECT subject_id,subject
		FROM subject
		ORDER BY subject ASC
	";
	$execute_get_subjects = mysql_query($query_get_subjects);
	
	//Query to get all the classes
	$query_get_classes = "
		SELECT cId, class_name
		FROM class_index
		ORDER BY class+0 ASC
	";
	$execute_get_classes = mysql_query($query_get_classes);
	
	echo '	
	
			<div class="full_w">
				<div class="h_title">Add Class/Subject </div>
				<h2 align="center">';
				
				if(isset($_GET['UpdateSuccess']))
				{
					//Succes Update
					echo '<p style="color:Green">Update successful</p>';
				}
				if(isset($_GET['UpdateError']))
				{
					//Error Update
					echo '<p style="color:RED">Update Error</p>';
				}
				
			echo'	
				</h2>
				
				<div class="entry">
					<div class="sep"></div>			
					
					<table style="width:250px">
											
						<form action="users/add_class_subject_teacher.php" method="post" >
						
						<input type="hidden" name="teacher_id" value="'.$teacher_id.'" />
						<input type="hidden" name="teacher_name" value="'.$teacher_name.'" />
						
						<tr>
							<td><strong>Select Class</strong></td><td>';
							echo '<select name="class" >';
							while($get_classes = mysql_fetch_array($execute_get_classes))
							{
								//Display all the classes in the select list
								echo '
									<option value="'.$get_classes[1].'">'.$get_classes[1].'</option>
								';
							}
							echo '</select>';
							
					echo'	</td>
						</tr>
						<tr>
							<td><strong>Select Subject</strong></td><td>';
							echo '<select name="subject" >';
							while($get_subjects = mysql_fetch_array($execute_get_subjects))
							{
								//Display all the subjects in the select list
								echo '
									<option value="'.$get_subjects[0].'">'.$get_subjects[1].'</option>
								';
							}
							echo '</select>';
					echo'	</td>
						</tr>
						
						<tr>
						<td><input type="submit" value="Add" /></td><td></td>
						</tr>
						
						</form>
					</table>
					
				</div>
				
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>';
}

function view_classes()
{
	$query_get_classes = "
		SELECT *
		FROM class_index
		ORDER BY class+0 ASC
	";
	
	$execute_get_classes = mysql_query($query_get_classes);
	
	echo '	
			<div class="full_w">
				<div class="h_title">View Classes</div>';
				
				if(isset($_GET['classAdded']))
				{
					echo '<h2 align="center" style="color:Green">Class Added Successfully</h2>';
				}
				if(isset($_GET['classDeleted']))
				{
					echo '<h2 align="center" style="color:Red">Class Deleted Successfully</h2>';
				}
				
				echo'
															<table >
																<tr>
																	<th width="75px">Sno.</th>
																	<th align="center">Class Name</th>
																	<th align="center">Class </th>
																	<th align="center">Section</th>
																	<th align="center">Total Number of Students</th>
																</tr>';
														
														$sno = 0;
														while($get_classes = mysql_fetch_array($execute_get_classes))
														{
															echo '<tr>';
															echo '<td align="center">'.++$sno.'</td>';
															echo '<td align="center">'.$get_classes[1].'</td>';
															echo '<td align="center">'.$get_classes[3].'</td>';
															echo '<td align="center">'.$get_classes[2].'</td>';
															
															$query_get_count_students = "
																SELECT COUNT(*) 
																FROM `class`
																WHERE `classId` = $get_classes[0] AND `session_id` = ".$_SESSION['current_session_id']."
															";
															$execute_get_count_students = mysql_query($query_get_count_students);
															$get_count_students = mysql_fetch_array($execute_get_count_students);
															
															echo '<td align="center">'.$get_count_students[0].'</td>';
															
															echo '</tr>';
														}
												echo '
															</table>
														 
				
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>';
}

function add_classes()
{
	echo '	
			<div class="full_w">
				<div class="h_title">Add New Class</div>
				<h2></h2>
				<form action="users/add_new_class.php" method="post" >
					  <table >
						  <tr>
							  <td>Class</td><td><input type="text" name="class" /></td>
						  </tr>
						  <tr>  
							  <td>Section</td><td><input type="text" name="section" /></td>
						  </tr>
						  <tr>
							  <td><button type="submit" class="add" >ADD</button></td><td></td>
						  </tr>
						  </tr>
				  	  </table>
					  
				</form>
				
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>';
}

function delete_class()
{
	$query_get_classes = "
		SELECT *
		FROM class_index
		ORDER BY class+0 ASC
	";
	
	$execute_get_classes = mysql_query($query_get_classes);
	
	echo '	
			<div class="full_w">
				<div class="h_title">View Classes</div>
				
										  <table style="width:300px">
										  <form method="post" action="users/class_deleted.php">
											  <tr>
												  <th width="75px">Sno.</th>
												  <th width="5px"></th>
												  <th align="center" width="100px">Class Name</th>
												  
												  
											  </tr>';
									  
									  $sno = 0;
									  while($get_classes = mysql_fetch_array($execute_get_classes))
									  {
										  echo '<tr>';
										  echo '<td align="center">'.++$sno.'</td>';
										  echo '<td align="center"><input type="radio" name="class" value="'.$get_classes['cId'].'" /></td>';
										  echo '<td align="center">'.$get_classes['class_name'].'</td>';
										  
										 
										  
										  echo '</tr>';
									  }
							  echo '
							  
							  <tr>
							  	<td></td>
								<td><button type="submit" class="cancel">Delete</button></td>
								<td></td>
							  </tr>
							  </form>
										  </table>
										  
						
														 
				
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>';
}

function view_subjects()
{
	
	$query_get_subject_details = "
		SELECT *
		FROM subject
		ORDER BY subject ASC
	";
	$execute_get_subject_details = mysql_query($query_get_subject_details);
	echo '	
			<div class="full_w">
				<div class="h_title">View Subjects</div>';
				
				if(isset($_GET['subjectAdded']))
				{
					echo '<h2 align="center" style="color:Green">Subject Added Successfully</h2>';
				}
				if(isset($_GET['subjectDeleted']))
				{
					echo '<h2 align="center" style="color:Red">Subject Deleted Successfully</h2>';
				}
				
				
				echo'					  <table style="width:300px">
											  <tr>
												  <th width="75px">Sno.</th>
												  <th width="175px">Subject Code</th>
												  <th align="center" width="100px">Subject Name</th>	  
											  </tr>';
										$sno = 0;	  
										while($get_subject_details = mysql_fetch_array($execute_get_subject_details))
										{
											echo '<tr>';
											
											echo '<td>'.++$sno.'</td>';
											echo '<td>'.$get_subject_details['subject_code'].'</td>';
											echo '<td><a href="manage_users_view_subject_details.php?id='.$get_subject_details['subject_id'].'" >'.$get_subject_details['subject'].'</a></td>';
											
											echo '</tr>';
										}
											  
					echo'						  
										  </table>
										  
						
														 
				
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>';
}

function view_subjects_details()
{
	//Function to show all the details of the subject
	
	$subject_id = $_GET['id'];
	
	//Query to get the details of the subject
	$query_get_subject_details = "
		SELECT `subject_code`, `subject`
		FROM `subject`
		WHERE `subject_id` = '$subject_id'
	";
	
	$execute_get_subject_details = mysql_query($query_get_subject_details);
	$get_subject_details = mysql_fetch_array($execute_get_subject_details);
	
	$subject_code = $get_subject_details['subject_code'];
	$subject_name = $get_subject_details['subject'];
	
	//Query to get the teachers and class details of the subject
	
	$query_get_class_teacher_subject = "
		SELECT users.Name, teachers.class
		FROM teachers
		
		INNER JOIN users
		ON teachers.tId = users.uId
		
		WHERE teachers.subject_id = $subject_id AND session_id = ".$_SESSION['current_session_id']."
	";
	
	$execute_get_class_teacher_subject = mysql_query($query_get_class_teacher_subject);
	
	echo '	
			<div class="full_w">
				<div class="h_title">View Subjects details</div>
				
				<table style="width:300px" >
					<tr><th width="100px">Subject Code</th><td>'.$subject_code.'</td></tr>
					<tr><th width="100px">Subject Name</th><td>'.$subject_name.'</td></tr>
				</table>
				
										  <table style="width:300px">
											  <tr>
												  <th width="75px">Sno.</th>
												  <th width="175px">Teacher Name</th>
												  <th align="center" width="100px">Class</th>	  
											  </tr>';
									$sno = 0;		  
									while($get_class_teacher_subject = mysql_fetch_array($execute_get_class_teacher_subject))
									{
										echo '<tr>';
										echo '<td>'.++$sno.'</td>';
										echo '<td>'.$get_class_teacher_subject[0].'</td>';
										echo '<td>'.$get_class_teacher_subject[1].'</td>';
										echo '</tr>';
									}
											  
					echo '						  
										  </table>
						
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>';
	
}

function add_subject()
{
	//Function to add a subject
	
	echo '	
			     <div class="full_w">
				<div class="h_title">Add New Class</div>
				<h2></h2>
				<form action="users/add_new_subject.php" method="post" >
					      <table >
						  <tr>
							  <td>Subject Code</td><td><input type="text" name="subject_code" /> </td>
						  </tr>
						  <tr>  
							  <td>Subject </td><td><input type="text" name="subject" /></td>
						  </tr>
						  <tr>
							  <td><button type="submit" class="add" >ADD</button></td><td></td>
						  </tr>
						  </tr>
				  	      </table>
					  
				</form>
				
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>';
	
}
//function to delete subject
function delete_subject()
{
	$query_get_subject_details = "
		SELECT *
		FROM subject
		ORDER BY subject ASC
	";
	$execute_get_subject_details = mysql_query($query_get_subject_details);
	echo '	
			<div class="full_w">
				<div class="h_title">Delete Subjects</div>';
				
				
				echo'					  <table style="width:400px">
						  <form method="post" action="users/delete_subject.php" >
											  <tr>
												  <th width="75px">Sno.</th>
												  <th></th>
												  <th width="175px">Subject Code</th>
												  <th align="center" width="200px">Subject Name</th>	  
											  </tr>';
										$sno = 0;	  
										while($get_subject_details = mysql_fetch_array($execute_get_subject_details))
										{
											echo '<tr>';
											
											echo '<td>'.++$sno.'</td>';
											echo '<td><input type="radio" name="subject_id" value="'.$get_subject_details['subject_id'].'" /></td>';
											echo '<td>'.$get_subject_details['subject_code'].'</td>';
											echo '<td><a href="manage_users_view_subject_details.php?id='.$get_subject_details['subject_id'].'" >'.$get_subject_details['subject'].'</a></td>';
											
											echo '</tr>';
										}
											  
					echo'			
							<tr>
								<td><button type="submit" class="cancel">Delete </button></td>
							 </tr>
							</form>			  
						  </table>													 				
				<div class="entry">		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>';
}
//function to add new teacher details
function add_new_teacher()
{
	
	echo '	
<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Add Teacher</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
		
				';
				
				if(isset($_GET['usernameExists']))
				{
					echo "<h2 style=\"color:red\" align=\"center\">Username Already exists, please try again</h2>";
				}
				if(isset($_GET['error']))
				{
					echo "<h2 style=\"color:red\" align=\"center\">Error! Please try again</h2>";
				}
				if(isset($_GET['added']))
				{
					echo "<h2 style=\"color:green\" align=\"center\">Teacher Added</h2>" ;
				}
				
			echo'	
				
    <form id="validation_demo" action="users/add_teacher.php" method="post" enctype="multipart/form-data">

<div class="section ">
<label> Name<small></small></label>   
<div> 
<input type="text" class="validate[required] medium" name="name"/>
</div>
</div>	
<div class="section">
<label> Login  Account  <small></small></label>
<div>
<input type="text" onKeyUp="check_username_teacher();" onblur="check_username_teacher();"  name="username" id="username"  class="validate[required,minSize[3],maxSize[20],] medium"  /> <label id="show_availability"></label></td>    <label>Username</label>                                             
  </div>
  <div>
  <input type="password"  class="validate[required,minSize[3]] medium"  name="password" id="password"  />
  <label>Password</label>
  </div>
  <div>
  <input type="password" class="validate[required,equals[password]] medium"  name="confirm_password" id="passwordCon"  />
  <label>Confirm Password</label>                                                    
  </div>
  </div>						
 <div class="section ">
  <label>Designation<small></small></label>   
  <div>   
  <select data-placeholder="Select a Designation..." class="chzn-select" tabindex="2" name="designation">
  <option value=""></option> 		  
  ';		  
		//get the designationfrom designation table
		  $get_designation_query="SELECT * FROM `designation`";
		  $get_designation_query_res=mysql_query( $get_designation_query);
		  while($fetch_designation=mysql_fetch_array( $get_designation_query_res))
		  {
			  $designation=$fetch_designation[1];
			 
		  echo '
		   <option value="'.$fetch_designation[0].'">'. $designation.'</option>';
		  }		
		  echo'</select>
             </div>
             </div>					
			  <div class="section ">
			<label> Email<small></small></label>   
			<div> 
			<input type="text" class="validate[required,custom[email]] medium" name="email_id" id="e_required">
		   </div>
		   </div>				
			<div class="section  numericonly ">
		   <label>Contact No<small></small></label>   
		   <div> 
		  <input type="text" class="validate[required] medium" name="phone_no"/>
		  </div>
		  </div>		
		  <div class="section">
		  <label>Address <small></small></label>   
		  <div > <textarea name="local_address" id="Textareaelastic"  class="medium"  cols="" rows=""  ></textarea>
		   </div>                                              
		    </div>		   
		   <div class="section">
		   <label>Date of Birth<small></small></label>   
		  <div><input type="text"  id="birthday" class=" birthday  small " name="dob"  />
		  </div>
	      </div>
	   
	    <div class="section">
		<label>Gender <small></small></label>   
	    <div>
		<div class="radiorounded">
		<input type="radio" name="gender" id="radio-1" value="male" />
		<label for="radio-1" title="Male"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="gender" id="radio-2" value="female"/>
		<label for="radio-2"  title="Female"></label>
	   </div>
       </div>
	   </div>
       <div class="section ">
       <label> Image <small></small></label>   
       <div> 
	   <input type="file" name="image" class="fileupload" />
       </div>
       </div>
       <br></br>
       <tr><div class="section last">	
	<button class="uibutton icon  add " type="submit">Add Teacher</button> 
	<button class="uibutton icon special cancel " type="reset">Cancel</button> 							  	  	  
     </form>							
	 </div><!-- row-fluid column-->
     </div><!--  end widget-content -->
     </div><!-- widget  span12 clearfix-->
     </div><!-- row-fluid -->
';
}
//function to view teacher details
function view_teacher_details()
{
	$user_id = $_GET['id'];
	//get designation from designation table
	//get designation on that id
	 $get_designation=
	"SELECT des_id 
	FROM users_designation_mapping
	WHERE uid=".$user_id."";
	$exe_get_des_id=mysql_query($get_designation);
	$fetch_des_id=mysql_fetch_array($exe_get_des_id);
	//get name of the designation acc to des id
	$get_des_name=
	"SELECT designation
	FROM designation 
	WHERE id=".$fetch_des_id[0]."";
	$exe_des_name=mysql_query($get_des_name);
	$fetch_name=mysql_fetch_array($exe_des_name);
	$designation=$fetch_name[0];
	                 
	
	//This function is used to view detials of the student
	
	$privilege = $_SESSION['priv'];
	if(($privilege!=0)&&($privilege!=1))
	{
		header("Location: error.php");
	}
	
	//Query to fetch the login details of the user
	
	$query_login_details = mysql_query("SELECT `username`,`privilege` FROM `login` WHERE `lId`='$user_id' ");
	$login_details = mysql_fetch_array($query_login_details);
	$username = $login_details[0];
	$privilege = $login_details[1];
	
	
	//Query to fetch personal details of the user
	$query_personal_details = mysql_query("SELECT `Name`,`DOB`,`Email`,`Local Address`,`Phone No`,`sex`,`image`,`uId`  FROM `users` WHERE `uId` = '$user_id' ");

	$personal_details = mysql_fetch_array($query_personal_details);
	$name = $personal_details[0];
	$dob = $personal_details[1];
	$email = $personal_details[2];
	$local_address = $personal_details[3];
	$phone_no = $personal_details[4];
	$sex = $personal_details[5];
	$image = $personal_details[6];
	$sId = $personal_details[7];
	
	
	//echo '<br/>'.$name.' '.$dob.' '.$email.' '.$local_address.' '.$phone_no.' '.$sex.' '.$image;
					
					
		echo '	
	<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Manage Teacher</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">';
				
				if(isset($_GET['update']))
				{
					echo "<h3 style=\"color:green\" align=\"center\">Update Successful</h3>";
				}
				
			echo '	
				<h2>Welcome</h2>
				
    	
	
				</div>
				<image src="user_image/'.$image.'"  width="200px" height="200px" align="left" />
				
				<table  class="table table-bordered table-striped" id="dataTable" >
						
						
						<tr>
							<td><strong>Name</strong></td><td>'.$name.'</td>
						</tr>
						
						<tr>
							<td><strong>Gender</strong></td><td>'.$sex.'</td>
						</tr>
						<tr>
							<td><strong>Username</strong></td><td>'.$username.'</td>
						</tr>
						<tr>
							<td><strong>Type of user</strong></td><td>Teacher</td>
						</tr>
						<tr>
							<td><strong>Date of Birth</strong></td><td>'.$dob.'</td>
						</tr>
						<td><strong>Designation</strong></td><td>'.$designation.'</td>
						</tr>
						<tr>
							<td><strong>Email address</strong></td><td>'.$email.'</td>
						</tr>
						<tr>
							<td><strong>Local Address</strong></td><td>'.$local_address.'</td>
						</tr>
						<tr>
							<td><strong>Phone Number</strong></td><td>'.$phone_no.'</td>
						</tr>
						
					</table>
					<a class="btn btn-small btn-info" href="manage_user_edit_teacher_details.php?user_id='.$user_id.'"><i class="icon-info-sign icon-white"></i>Edit Details</a>';
				/*	<a href="manage_user_edit_teacher_details.php?user_id='.$user_id.'"><button type="submit" class="uibutton icon edit special">Edit Details</button></a>*/
					echo '<a class="btn btn-danger" href="users/manage_user_delete_teacher_details.php?user_id='.$user_id.'"><i class="icon-trash icon-white"></i> Delete</a>';
						/*<a href="manage_user_edit_teacher_details.php?user_id='.$user_id.'"><button type="submit" class="btn btn-danger">Delete Details</button></a>	*/							 
				echo ' </div><!-- row-fluid column-->
		  </div><!--  end widget-content -->
		  </div><!-- widget  span12 clearfix-->
		  </div><!-- row-fluid -->
			
';
}
//function to edi teacher details
function edit_teachers()
{
	$user_id = $_GET['user_id'];
	//get designation on that id
	 $get_designation=
	"SELECT des_id 
	FROM users_designation_mapping
	WHERE uid=".$user_id."";
	$exe_get_des_id=mysql_query($get_designation);
	$fetch_des_id=mysql_fetch_array($exe_get_des_id);
	//get name of the designation acc to des id
	$get_des_name=
	"SELECT designation
	FROM designation 
	WHERE id=".$fetch_des_id[0]."";
	$exe_des_name=mysql_query($get_des_name);
	$fetch_name=mysql_fetch_array($exe_des_name);
	$designation=$fetch_name[0];
	
	//Query to get the username of the teacher
	$query_username = "
		SELECT username
		FROM login
		WHERE lId = $user_id AND privilege = 2
	";
	$execute_username = mysql_query($query_username);
	$username = mysql_fetch_array($execute_username);
	$username = $username[0];
	
	//Query to get the details of the user
	$query_get_details_user = "
		SELECT `Name`, `DOB`, `Email`, `Local Address`, `Phone No`, `sex`, `image`
		FROM users
		WHERE uId = $user_id
	";
	$execute_get_details_user = mysql_query($query_get_details_user);
	$get_details_user = mysql_fetch_array($execute_get_details_user);	
	$name = $get_details_user[0];
	$dob = $get_details_user[1];
	$email = $get_details_user['Email'];
	$local_address = $get_details_user[3];
	$phone_no = $get_details_user[4];
    $gender = $get_details_user[5];
	$image = $get_details_user[6];
	
	echo '
	<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Edit Teacher</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
				
				<form action="users/update_teacher.php" method="post" enctype="multipart/form-data">				
				 <div class="section ">			
			  <div> 
			 <input type="hidden" value="'.$user_id.'" name="user_id"/>
             </div>
             </div>					
			 <div class="section ">
			  <label> Name<small></small></label>   
			  <div> 
			 <input input id="name" name="name" type="text" value="'.$name.'"class="validate[required] medium"/>
             </div>
             </div>	
				 
	
	 <div class="section">
     <label>birthday<small></small></label>   
     <div><input type="text" value="'.$dob.'" id="birthday" class=" birthday  small " name="dob"  />
     </div>
     </div>
	 <div class="section">
		<label>Gender <small></small></label>   
	    <div>
		<div class="radiorounded">
		';
		if($gender == 'male')
		{
		echo '
		<input type="radio" name="gender" id="radio-1" value="male" checked="checked"/>
		<label for="radio-1" title="Male"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="gender" id="radio-2" value="female" />
		<label for="radio-2"  title="Female"></label>
		';
		}
		else
		{
			
		echo '
		<input type="radio" name="gender" id="radio-1" value="male" />
		<label for="radio-1" title="Male"></label>
	   </div>
	   <div class="radiorounded">
		<input type="radio" name="gender" id="radio-2" value="female" checked="checked"/>
		<label for="radio-2"  title="Female"></label>
		';	
			
		}
		
		echo '
	   </div>
       </div>
	   </div>
	 <div class="section ">
	<label>Designation<small></small></label>   
		<div> <select data-placeholder="'.$designation.'" class="chzn-select" tabindex="2" name="designation">
  <option value="'.$designation.'"></option>'; 
  
  //get the designationfrom designation table
		  $get_designation_query="SELECT * FROM  `designation`";
		  $get_designation_query_res=mysql_query( $get_designation_query);
		  while($fetch_designation=mysql_fetch_array( $get_designation_query_res))
		  {
			  $designation=$fetch_designation[1];
			  $designation_id=$fetch_designation['Id'];		  			 
		  echo'
		   <option value="'.$designation_id.'">'. $designation.'(id='.$designation_id.')</option>';
		  }		
  		echo'  
	   </select>
	   </div>
	   </div>			
	     <div class="section ">
		<label> Email<small></small></label>   
		<div> 
	   <input type="text" id="e_required " name="email_id" onblur="checkemail(this.value)"
	   value="'.$email.'"class="validate[required,custom[email]] medium ">
		   </div>
		  </div>
		   <div class="section numericonly">
		<label>Contact NO.<small></small></label>   
		<div> 
		 <input name="phone_no" type="text" value="'.$phone_no.'" class="medium">
		</div>
	   </div>

	
	 <div class="section">
     <label>Address <small></small></label>   
     <div > <textarea name="local_address" id="Textareaelastic"  class="medium"  cols="" rows=""  >'.$local_address.'</textarea>
     </div>                                              
     </div>
	 
	<div class="section">
     <label>Picture <small></small></label>   
     <div > <input type="file" name="image" class="fileupload" />
     </div>                                              
     </div>
	 
    <tr><div class="section last">	
	<button class="uibutton icon special add " type="submit">Update Teacher</button> 
			
     </form>															
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
';
}
// function to add designation

 function add_new_designation()
{	
	echo '
	<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Add Designation</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />';			   
	if(isset($_GET['error']))
	  {
	echo "<h5 style='color:red' align='center'> Already Exists, Please try again</h5>";
	  }
	
	if(isset($_GET['successfuly_added']))
	  {
	echo "<h5 style='color:green' align='center'> Successfully Added</h5>";
	  }		
	echo'				
	<form id="validation_demo" action="users/add_designation.php" method="post" enctype="multipart/form-data">	
	         <div class="section ">
			  <label>Designation<small></small></label>   
			  <div> 
			 <input input id="name" name="designation" onkeyup="check_designation();" type="text" class="validate[required]  medium"/><label id="show_availability"></label>
             </div>
             </div>	
			 <div class="section last">		
	<button class="uibutton icon special add "  type="submit" >Add</button>
	</div>
	</form> 
	';		
	echo '	
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
';
			
	}
	
// attendance of school whether it is open or not

function working_closed()
{
echo '
	<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Working/Closed</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />';
                                                                                                      if(isset($_GET['day_working']))
				{
					echo '<h3 align="center">';
						echo 'Day Saved';
					echo '</h3>';
				}
                                
                                 if(isset($_GET['working']))
				{
					echo '<h3 align="center">';
						echo 'Already Day Saved !';
					echo '</h3>';
				}
                                
                                
				echo '
			<a href="attendance/working_date_school.php"><button class="uibutton icon"  type="submit" >Working Day ?</button></a>
			
				
				
				<table  class="table table-bordered table-striped" id="dataTable">
				<thead>
				<th>Working dates</th>
				
				</thead>
				<tbody align="center">
				';
				//get all the working days which are saved 
				$get_days=
				"SELECT date , date_id
				FROM dates_d
				INNER JOIN working_days
				ON working_days.Date_working_id = dates_d.date_id";
				$exe_working_days=mysql_query($get_days);
				while($fetch_days=mysql_fetch_array($exe_working_days))
				{
				echo '<tr id="w_'.$fetch_days[1].'">
				<td>'.$fetch_days[0].'</td>';
				/*echo'
				<td><span onclick="edit_working_closed(\''.$fetch_days[0].'\','.$fetch_days[1].')"><a original-title="Edit"><img src="images/icon/icon_edit.png"></a></span>
				<span class="tip"><a href="javascript:void(0)" class="Delete" data-name="delete name" title="Delete"><img src="images/icon/icon_delete.png"></a></span></td>
				
				</tr>';	*/
					}
				echo '	
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
';
	
	
}

// function to update picture

 function edit_image()
{	
	 $uid=$_GET['student_id'];
	echo '
	<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Update Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	 <form id="demovalidation" action="users/update_image.php?student_id='.$uid.'" method="post" enctype="multipart/form-data"> 
                                        <fieldset >
										 <div class="section ">
                                              <label>Picture</label> 
											  <div><input type="file"  name="picture" id="picture" value=" "> 
											  <input type="hidden"  name="student_id" id="" value="'.$uid.'">
											  </div> 
                                             </div>
											
											<div class="section last">
                                              <div>
                                                <input type="submit" value="Update" class="btn submit_form" ></a>
                                                <a class="btn" onClick="ResetForm()" title="Reset  Form">Clear</a>
                                             </div>
                                             </div>
                                        </fieldset>
                                        </form>
										</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->

 ';
 }
 //function to show profile to the user as requested
function show_profile_user()
{
$uid=$_GET['uid'];
$priv=$_GET['priv'];	
echo '
	<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>User Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">
	<h5 align="center">Welcome</h5>

	<table  class="table table-bordered table-striped">
	<tbody>';
	//get all the details
	if(($priv == 3)||($priv == 4))
	{
		 $get_details_user=
	"SELECT *
	FROM student_user
	WHERE sId = ".$uid."";
	
	$exe_details=mysql_query($get_details_user);
	$fetch_details=mysql_fetch_array($exe_details);	
		
	}
	
	else
	{
	$get_details_user=
	"SELECT *
	FROM users
	WHERE uId = ".$uid."";
	
	$exe_details=mysql_query($get_details_user);
	$fetch_details=mysql_fetch_array($exe_details);
	}
	echo '<tbody>
	<div align="left"><img src="user_image/'.$fetch_details[8].'" style="width:200px" style="height:200px"/></div>';
	echo '<tr>
	<td>Name</td><td>'.$fetch_details[1].'</td></tr>
	<tr><td>Username</td><td>'.$fetch_details[2].'</td></tr>
	<tr><td>DOB</td><td>'.$fetch_details[3].'</td></tr>
	<tr><td>Email</td><td>'.$fetch_details[4].'</td></tr>
	<tr><td>Local Address</td><td>'.$fetch_details[5].'</td></tr>
	<tr><td>Phone No</td><td>'.$fetch_details[6].'</td></tr>
	<tr><td>Gender</td><td>'.$fetch_details[7].'</td></tr>';
	
	
	echo '
	</tbody>
	</table>

	<br /><br />';

	if($priv==2)
	{
	echo'<a class="btn btn-primary" href="edit_profile_teacher.php?id='.$fetch_details[0].'"><b>&nbsp; EDIT PROFILE &nbsp;</b></a>';
	}

	echo'

	</div>
	</div>
	</div>';
	
}
//function to reset password of the user
function reset_pass_user()
{
$priv=$_GET['priv'];
$uid=$_GET['uid'];	
echo '
	<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Reset password</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">
	
';
if(isset($_GET['done']))
{
echo '<h5 align="center" style="color:green">Password changed</h5>';	
	
}
if(isset($_GET['nt_done']))
{
echo '<h5 align="center" style="color:red">Error!</h5>';	
	
}
if(isset($_GET['old_wrong']))
{
echo '<h5 align="center" style="color:red">old password is wrong</h5>';	
	
}
echo '
<form id="validation_demo" action="users/reset_password.php" method="post">
<input type="hidden" value="'.$uid.'" name="user_id"/>
<input type="hidden" value="'.$priv.'" name="priv"/>
<div class="section">
<label>Old password</label>
<div>
<input type="password" name="old_pass" id="old_pass" class="validate[required]" />
</div>
</div>
<div class="section">
<label>New password</label>
<div>
<input type="password" name="new_pass" id="new_pass" class="validate[required]"/>
</div>
</div>
<div class="section">
<label>Confirm password</label>
<div>
<input type="password" class="validate[required,equals[new_pass]] name="confirm_pass" id="confirm_pass" />
</div>
</div>
<div class="section last">
<div>
<button class="uibutton normal">Save</button>
</div>
</div>
';


echo '</div>
</div>
</div>';	
	
	
}

function filter_students()
{
	//Method to filter the students on various basis
	
	//Query to get the class name from class_index table
	$query_get_class = "SELECT `class_name` FROM `class_index`";
	$execute_get_class = mysql_query($query_get_class);
	
	//This function will display the dashboard of managing student users.
	
	 $query_get_students_details = "
	SELECT   student_user.admission_no, student_user.Name, class_index.class_name, student_user.sId
	FROM student_user
	INNER JOIN class
	ON class.sId = student_user.sId
	INNER JOIN class_index
	ON class_index.cId = class.classId
	WHERE class.session_id = ".$_SESSION['current_session_id']."
	 ORDER BY student_user.admission_no+0 ASC 
	";
	$execute_get_students_details = mysql_query($query_get_students_details);
	echo '	
	
                  <div class="row-fluid">                            
                      <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Filter Students</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />';
	if(isset($_GET['studentDeleted']))
	{
	echo '<h5 align="center" style="color:red">Student Deleted Successfully!!</h5>';
		
	}


	echo'
<form action="users_apply_filters.php" method="POST">
	
	<div>
  <select id="category" data-placeholder="Select Category..." class="chzn-select" tabindex="2" name="category" onchange="filter(\'category\',this.value)">
								  <option value=""></option> 	
								 <option value="general">General</option>
								  <option value="OBC">OBC</option>
								  <option value="SC">SC</option>
								  <option value="ST">ST</option>
								  <option value="other">others</option>
								 
  </select>
 
  <select id="religion" data-placeholder="Select Religion..." class="chzn-select" tabindex="2" name="religion" onchange="filter(\'religion\',this.value)">
								  <option value=""></option> 	
								 <option value="Hindu">Hindu</option>
								  <option value="Muslim">Muslim</option>
								  <option value="Christian">Christian</option>
								  <option value="Sikh">Sikh</option>
								  <option value="Jain">Jain</option>
								  <option value="Buddhist">Buddhist</option>
								  <option value="Others">Others</option>
								 
  </select>
  
  <select id="gender" data-placeholder="Select Gender..." class="chzn-select" tabindex="2" name="gender" onchange="filter(\'gender\',this.value)">
								  <option value=""></option> 	
								 <option value="Male">Male</option>
								  <option value="Female">Female</option>
								  
								 
  </select>
  
  <select id="class" data-placeholder="Class..." class="chzn-select" tabindex="2" name="class" onchange="filter(\'class\',this.value)">
								  <option value=""></option> 	';
								  
								  while($get_class = mysql_fetch_array($execute_get_class))
								  {
									  echo '<option value="'.$get_class[0].'">'.$get_class[0].'</option>';
								  }
								  
								 echo '
  </select>
  </div>
								
								
	';
	
echo ' <br />
<div width="100%">
	<input type="checkbox" value="s_name" name="s_name" checked /><b> &nbsp; Student Name</b>
	<span style="margin-left: 50px;"><input type="checkbox" value="adm" name="adm" checked /><b> &nbsp;Admission No.</b></span>
	<span style="margin-left: 50px;"><input type="checkbox" value="name" name="doa" checked /><b> &nbsp;Admission Date</b></span>
	<span style="margin-left: 50px;"><input type="checkbox" value="father" name="father" checked /><b> &nbsp;Father\'s Name</b></span>
	<span style="margin-left: 50px;"><input type="checkbox" value="mother" name="mother" checked /><b> &nbsp;Mother\'s Name</b></span>
	<span style="margin-left: 50px;"><input type="checkbox" value="contact" name="contact" checked /><b> &nbsp;Contact</b></span>
	<span style="margin-left: 50px;"><input type="checkbox" value="class" name="classs" checked /><b> &nbsp;Class</b></span> <br />
	<span style=""><input type="checkbox" value="age" name="age" checked /><b> &nbsp;Age</b></span>
	<span style="margin-left: 112px;"><input type="checkbox" value="gender" name="genderr" checked /><b> &nbsp;Gender</b></span>
	<span style="margin-left: 91px;"><input type="checkbox" value="religion" name="religionn" checked /><b> &nbsp;Religion</b></span>
	<span style="margin-left: 94px;"><input type="checkbox" value="address" name="address" checked /><b> &nbsp;Address</b></span>

<br /><br /></div>';
	
echo '<button type="submit" class="btn btn-primary"><b> APPLY FILTERS </b></button><br /><br /></form>';

	echo'			
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
                        <tr>		   
	<th>Sno.</th>
	<th>Admission No.</th>
	
	<th>Student Name</th>
	<th>Contact</th>
	<th>Father\'s Name</th>
	<th>Mother\'s Name</th>
	<th>Class</th>
	<th>Age</th>
	<th>Gender</th>
	<th>Religion</th>
	<th>Caste</th>
	
					
    </tr>
    </thead>
						
<tbody align="center">';
$sno = 0;
/*while($students_details = mysql_fetch_array($execute_get_students_details))
{
        ++$sno;
                        echo '
                       <tr id="txtboxes">
                        <td>'.$sno.'</td>
                        <td>'.$students_details[0].'</td>
                        <td><a href="manage_users_student_details.php?student_id='.$students_details[3].'" >'.$students_details[1].'</a></td>
                        <td>'.$students_details[2].'</td>
<td><a class="btn btn-danger" onclick="delete_stu_confirm('.$students_details[3].');">
<i class="icon-trash icon-white"></i> Delete</a></td>
  </tr> ';
}*/

echo	
'</tbody>
</table>
<div class="entry">		
<div id="txtHint"></div>
<div id="Hint"></div>				
</div>';
}

function allot_roll_no_select_class()
{
    //Method to get the list of classes for allotment of Roll No
    
    echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Manage Students</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">';
	
	$q="SELECT `cId`,`class_name` FROM class_index ORDER BY class+0 ASC";

$q_res=mysql_query($q);
echo '<h2 align="center">';




echo '</h2>';
echo "<br>";
echo "<br>";
echo "<br>";
 echo '<ol class="rounded-list">';
while($res=mysql_fetch_array($q_res))
{
	
	
	echo '<tr class="row-fluid" id="added_rows">
      <td><div class="row-fluid">
          <div class="span6">							                                                              
      <li><a href="manage_user_allot_roll_no_view_class_details.php?class_id='.$res[0].'">Class - '.$res[1].'</a>                                                                    	                                 
	</div>
	</div><!-- end row-fluid  -->


</td>
</tr>';	 	 
}
echo '</div>
</div>
</div>';
    
    
}

function allot_roll_no_class_student_list()
{
    //Function to show the details of the class including Roll No.
    $class_id = $_GET['class_id'];
    
echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Manage Students</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">';

$query_get_student_details = "
            SELECT roll_no.roll_no, student_user.Name, student_user.admission_no
            FROM `roll_no`
            
            INNER JOIN `student_user`
            ON roll_no.sId = student_user.sId
            
            WHERE roll_no.cId = $class_id
               
            ORDER BY roll_no.roll_no ASC 
";
$execute_get_student_details = mysql_query($query_get_student_details);
$ctr = mysql_num_rows($execute_get_student_details);

if($ctr == 0)
{
    //If the query returns no value then roll_no of the students are not yet generated
    echo '<h5 style="color:green" align="center">ROLL NO. NOT GENERATED, <a href="users/generate_roll_no.php?class_id='.$class_id.'" >GENERATE NOW</a></h5>';
}
else
{
    //Show the student details with Roll No
    
    echo '
    <a href="manage_users_edit_allot_roll_no.php?class_id='.$class_id.'">
    <button class="uibutton submit "  rel="1" type="submit" >Edit Roll No.</button></a>
    
    <a href="users/manage_users_edit_allot_roll_no_print.php?class_id='.$class_id.'">
    <button class="uibutton submit "  rel="1" type="submit" >Print</button></a>

 <a href="users/regenerate_roll_no.php?class_id='.$class_id.'&id=1">
    <button class="uibutton submit "  rel="1" type="submit" >Regenerate Now</button></a>
';
    
    
    echo '<table class="table table-bordered table-striped dataTable" id="dataTable" >';
        echo '<tr>';
            echo'<th>ROLL NO</th><th>Admission No</th><th>NAME</th>';
        echo '</tr>';
        while($get_student_details = mysql_fetch_array($execute_get_student_details))
        {
            //Get the student details
            
            echo '<tr align="center">';
                echo '<td>'.$get_student_details[0].'</td>';
                echo '<td>'.$get_student_details[2].'</td>';
                echo '<td>'.$get_student_details[1].'</td>';
            echo '</tr>';
        }
    echo '</table>';
}
echo '</div>
</div>
</div>';
    
    
}

function allot_roll_no_edit_roll_no()
{
    //Method to Edit the Roll No
    
    
    $class_id = $_GET['class_id'];
    
echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Manage Students</span>
	</div><!-- End widget-header -->		
	<div class="widget-content">';

$query_get_student_details = "
            SELECT roll_no.roll_no, student_user.Name, roll_no.Id
            FROM `roll_no`
            
            INNER JOIN `student_user`
            ON roll_no.sId = student_user.sId
            
            WHERE roll_no.cId = $class_id
               
            ORDER BY roll_no.roll_no ASC 
";
$execute_get_student_details = mysql_query($query_get_student_details);
$ctr = mysql_num_rows($execute_get_student_details);

if($ctr == 0)
{
    //If the query returns no value then roll_no of the students are not yet generated
    echo '<h5 style="color:green" align="center">ROLL NO. NOT GENERATED, <a href="users/generate_roll_no.php?class_id='.$class_id.'" >GENERATE NOW</a></h5>';
}
else
{
    //Show the student details with Roll No
    
     echo '
    <a href="manage_user_allot_roll_no_view_class_details.php?class_id='.$class_id.'">
    <button class="uibutton submit "  rel="1" type="submit" >SAVE</button></a>';
    
    echo '<table class="table table-bordered table-striped dataTable" id="dataTable" >';
        echo '<tr>';
            echo'<th>ROLL NO</th><th>NAME</th>';
        echo '</tr>';
        while($get_student_details = mysql_fetch_array($execute_get_student_details))
        {
            //Get the student details
            
            echo '<tr>';
                echo '<td><input type="text" value ="'.$get_student_details[0].'" style="width:20px" id="roll_no'.$get_student_details[2].'" onchange="edit_roll_no('.$get_student_details[2].');" /></td>';
                echo '<td>'.$get_student_details[1].'</td>';
            echo '</tr>';
        }
    echo '</table>';
}
echo '</div>
</div>
</div>';
    
}



function  list_details()
{
  echo '<div class="row-fluid">
			  
 <div class="span12  widget clearfix">
<div><!--  end widget-content -->	
 <div class="widget-header">
  <span><i class="icon-align-center"></i>Select class to view students</span>
  </div><!-- End widget-header -->	

	'; 
	
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");
	echo "<h2>"; 
	echo '<h5 style="color:grey" align="center"> '.$today.'</h5>';

	
	echo "</h2>";

$disp='';
 $q="SELECT `cId`,`class_name` FROM class_index ORDER BY class+0 ASC ,section ASC";

$q_res=mysql_query($q);


echo "<br>";
echo '<ol class="rounded-list">';
while($res=mysql_fetch_array($q_res))
{
	
	
	echo '<tr class="row-fluid" id="added_rows">
       <td><div class="row-fluid">
       <div class="span6">							                                                              
      <li><a href="edit_students_record.php?class_id='.$res['cId'].'">'.$res[1].'</a>                                                                    	                                 
	</div>
	</div><!-- end row-fluid  -->


</td>
</tr>';	 	 
}
echo'
</ol>	                            
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 
}
function edit_students_record()
{
  $class_id=$_GET['class_id'];
  $session_id=$_SESSION['current_session_id'];
  $q="SELECT * 
                              FROM class_index 
                WHERE cId='".$_GET['class_id']."'";
                $execute=mysql_query($q);
                $fetch=mysql_fetch_array($execute);
                $class_name_stu=$fetch['class_name'];

        echo '<h5 style="color:green" align="center">Class: '.$class_name_stu.'</h5>';
        
        
        echo'	<table class="table table-bordered table-striped" id="dataTable">
														
									<!--Header of the table-->
									 <thead>
                                            <tr style="text-align: center">
											<th>Student Name</th>
                                                                                        
                                                                                        <th>Date of Admission</th>
                                                                                        <th>Phone No</th>
                                                                                        <th>DOB</th>
                                                                                        <th>Admission No</th>
                                                                                        <th>Address</th>
                                                                                        <th>Father\'s Name</th>
                                                                                        <th>Mother\'s Name</th>
                                                                                        <th>Gender</th>
                                                                                        <th>Blood Group</th>
                                                                                        <th>Religion</th>
                                                                                        <th>Email</th>
                                                                                        <th>Mother\'s Occupation</th>
                                                                                        <th>Father\'s  Occupation</th>
                                                                                        <th>ews</th>
                                                                                        <th>Guardian\'s Contact</th>
                                                                                        <th>Category</th>';
								echo   '</tr>
								 </thead>
								 <tbody align="center">';
                                                                $unique_id=0;
   
 //query to get student ids studyin in  same class
 $get_sid=
         "
             SELECT `sId` 
             FROM `class`
             WHERE classId='$class_id' AND session_id=$session_id
             
";
$exe_get_sid=mysql_query($get_sid);
while($fetch_sid=  mysql_fetch_array($exe_get_sid))
        //query to get students details
{ //echo $fetch_sid[0];
   $get_details="SELECT * FROM `student_user` WHERE `sId`='$fetch_sid[0]'
       
";
  $run_details=  mysql_query($get_details);
  
  
   
                                                                //$father='Father''sName;
                                                                   

                                                                while($fetch_details= mysql_fetch_array($run_details))
                                                                {     
                                                                   //$sId = $fetch_details['sId'];
                                                                   echo '<tr>';
										echo '<td> <input  id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-Name" value="'.$fetch_details['Name'].'"style="width:170px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>';
                                                                                   
                                                                                  
                                                                                   
                                                                                     echo' <td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-admission_date" value="'.$fetch_details['admission_date'].'" style="width:70px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
                                                                                         
                                                                                         
                                                                                         
                                                                                      <td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-Phone No" value="'.$fetch_details['Phone No'].'" style="width:70px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
                                                                                         
                                                                                         
                                                                                         
                                                                                      <td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-DOB" value="'.$fetch_details['DOB'].'" style="width:70px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
                                                                                         
                                                                                         

                                                                                      <td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-admission_no" value="'.$fetch_details['admission_no'].'" style="width:60px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
                                                                                         
                                                                                         

                                                                                      <td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-Local Address" value="'.$fetch_details['Local Address'].'" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
                                                                                         
                                                                                         

                                                                                      <td> <input id="'.++$unique_id.'"  type="text" name="'.$fetch_details['sId'].'-Father\'s Name" value="'.$fetch_details['Father\'s Name'].'" style="width:170px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
                                                                                         

                                                                                         
                                                                                      <td><input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-Mother\'s Name" value="'.$fetch_details['Mother\'s Name'].'" style="width:170px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
                                                                                          

                                                                                          
<td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-gender" value="'.$fetch_details['gender'].'" style="width:70px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
    

<td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-Blood Group" value="'.$fetch_details['Blood Group'].'" style="width:70px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
    

<td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-religion" value="'.$fetch_details['religion'].'" style="width:100px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
    

<td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-Email" value="'.$fetch_details['Email'].'" style="width:170px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
    

<td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-mothers_occupation" value="'.$fetch_details['mothers_occupation'].'" style="width:100px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
    

<td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-father_occupation" value="'.$fetch_details['father_occupation'].'" style="width:100px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
    

<td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-ews" value="'.$fetch_details['ews'].'" style="width:100px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
    

    
<td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-Guardian\'s Contact" value="'.$fetch_details['Guardian\'s Contact'].'" style="width:100px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
                                                                                   
   
<td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-category" value="'.$fetch_details['category'].'" style="width:100px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
       

                                                                             ';
}}
								
											
											echo '
											</tbody>
											</table>';

}




function download_excel_class()
{
    
    echo '<div class="row-fluid">
			  
 <div class="span12  widget clearfix">
<div><!--  end widget-content -->	
 <div class="widget-header">
  <span><i class="icon-align-center"></i>Select class to view students</span>
  </div><!-- End widget-header -->	

	'; 
	
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");
	echo "<h2>"; 
	echo '<h5 style="color:grey" align="center"> '.$today.'</h5>';

	
	echo "</h2>";

$disp='';
 $q="SELECT `cId`,`class_name` FROM class_index ORDER BY class+0 ASC ,section ASC";

$q_res=mysql_query($q);


echo "<br>";
echo '<ol class="rounded-list">';
while($res=mysql_fetch_array($q_res))
{
	
	
	echo '<tr class="row-fluid" id="added_rows">
       <td><div class="row-fluid">
       <div class="span6">							                                                              
      <li><a href="users/students_download_record.php?class_id='.$res['cId'].'">'.$res[1].'</a>                                                                    	                                 
	</div>
	</div><!-- end row-fluid  -->


</td>
</tr>';	 	 
}
echo'
</ol>	                            
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
';  
    
    
    
    
}

//////add height weight
function add_height_weight()
{
    echo '<div class="row-fluid">
			  
 <div class="span12  widget clearfix">
<div><!--  end widget-content -->	
 <div class="widget-header">
  <span><i class="icon-align-center"></i>Select class to view students</span>
  </div><!-- End widget-header -->	
'; 
    																																		                                   
        $get_test = "SELECT DISTINCT class_index.*  FROM class_index
 INNER JOIN teachers 
ON class_index.class_name = teachers.class
 WHERE    teachers.tId = ".$_SESSION['user_id']."";

$exe_test = mysql_query($get_test);
echo '<div class="span4">
';
while($fetch_test = mysql_fetch_array($exe_test))
{
      
      $class_name=$fetch_test['class_name'];
      $class_id=$fetch_test['cId'];
     echo ' <a href="new_grade_cce_add_height_students_class.php?class_id='.$class_id.'"><div class="alertMessage inline info">'.$class_name.'</div></a> ';
      
}
                                            
                                            

    
    
    echo'
</ol>	                            
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
';  
    
}


function add_height_weight_student()
{
    $class_id=$_GET['class_id'];
    $session_id=$_SESSION['current_session_id'];
      echo '
<table class="table table-bordered table-striped"  border="2"    width="100%"   >
<thead >
<tr>
<th rowspan="2" width=10%>Admission No.</th>
</tr>
<tr><th  align="center">Name</th><th>Height</th><th>Weight</th><th>Blood Group</th><th>Roll No.</th><th>No. of Worhing Day</th><th>No. of Present Day</th></tr>

</thead>';

    echo '
           <tbody align="center">';
           //get student names on the class id
               $unique_id=0;
   
 //query to get student ids studyin in  same class
 //echo $fetch_sid[0];
   $get_details="SELECT student_user . * 
FROM  `student_user` 
INNER JOIN class ON student_user.sId = class.sId
WHERE class.classId =$class_id";

  $run_details=  mysql_query($get_details);
  
  
   
                                                                //$father='Father''sName;
                                                                   

                                                                while($fetch_details= mysql_fetch_array($run_details))
                                                                {     
                                                                   //$sId = $fetch_details['sId'];
                                                                   echo '<tr>';
                                                                   
                                                                     echo'<td> '.$fetch_details['admission_no'].'</td>';      
			echo '<td> '.$fetch_details['Name'].'</td>';
                                                                             
                                                                                    
                                                                                  
                                                                                   
                                                                                 echo' <td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-height" value="'.$fetch_details['height'].'" style="width:70px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
                                                                                         
                                                                                         
                                                                                         
                                                                                      <td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-weight" value="'.$fetch_details['weight'].'" style="width:70px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
                                                                                         
                                                                                         
                                                                                         
                                                                                      <td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-Blood Group" value="'.$fetch_details['Blood Group'].'" style="width:70px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
                                                                                         
                                                                                         

                                                                                         
                                                                                         
    <td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-roll_no_stu" value="'.$fetch_details['roll_no_stu'].'"  style="width:70px"  onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>
                                                                                       <td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-working_day" value="'.$fetch_details['working_day'].'"  style="width:70px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>    
                                                                                       <td> <input id="'.++$unique_id.'" type="text" name="'.$fetch_details['sId'].'-present_day" value="'.$fetch_details['present_day'].'" style="width:70px" onfocus="getValueUpdate('.$unique_id.')" onblur="setValueUpdate('.$unique_id.')"/></td>    
                                                                   
                                                                                         

                                                                             ';
}
						
                     
                     echo'</tbody></table>';  
        
    
    
    
    
    
}


function view_stuck_off_student()
{


  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Issued Transfer Certificate</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';
   echo'			
	<table  class="table table-bordered table-striped" id="dataTable" >
		<thead>											
			<tr>
				<th>S.No.</th>
				<th>Admission No.</th>
				<th>Student Name</th>
				<th>Class</th>
				<th>Stuck off Date</th>
				<th>Action</th>
			</tr>
		</thead>
		
		<tbody align="center">';
   $sn=1;
  $get_datail="SELECT * FROM struck_off_student WHERE flag=0";
  $exe_detail=mysql_query($get_datail);
 while($fetch_detail=mysql_fetch_array($exe_detail))
 {
     $student_id=$fetch_detail['student_id'];
     $stuck_date=$fetch_detail['struck_off_date'];
    
	$class_id = $fetch_detail['class_id'];
	
    $get_class = "SELECT * FROM class_index WHERE cId = $class_id";
     $exe_class=mysql_query($get_class);
     $fetch_class=mysql_fetch_array($exe_class);
     $class_=$fetch_class[1];
     
	 $class_name  = $fetch_class['class_name'];
	 
     $get_name="SELECT * FROM student_user WHERE `sId` = $student_id";
     $exe_name=mysql_query($get_name);
     $fetch_name=mysql_fetch_array($exe_name);
     $name=$fetch_name['Name'];
     $admission_no=$fetch_name['admission_no'];
     
     
//     $get_class="SELECT * FROM class_index INNER JOIN class ON class_index.cId=class.classId WHERE class.sId=$student_id";
//     $exe_class=mysql_query($get_class);
//     $fetch_class=mysql_fetch_array($exe_class);
//     $class=$fetch_class['class_name'];
     echo'<tr>
           <td>'.$sn.'</td> 
           <td>'.$admission_no.'</td>
           <td>'.$name.'</td>   
           <td>'.$class_name.'</td>   ';
      echo'     <td>'.$stuck_date.'</td>';
         echo'<td><a class="btn btn- " onclick="roll_back('.$student_id.');">
<i class="icon-trash icon-white"></i> roll back</a></td>';
         // echo'<td><a href="users/assign_new_class.php?sid='.$student_id.'">Roll Back </a></td>';                   

	echo' <td><a class="btn btn- " href="users/transfer_certificate.php?student_id='.$student_id.'"><i class="icon-green"></i>Transfer Certificate</a></td>';           
     
  echo'   </tr>';
$sn++;
     
 }

         echo' </tbody>
		 </table>';
  
      

        echo '</div>
        </div>
        </div>';	
  

}






function select_stu_view_tc()
{
  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Issued Transfer Certificate</span>
      </div><!-- End widget-header -->
      <div class="widget-content">

	<a href="select_class_for_print_tc.php" class="btn btn-primary" ><b>PRINT</b></a> <br /><br />

   	<table class="table table-bordered table-striped" id="dataTable">
														
	<!--Header of the table-->

		<thead>
			<tr style="text-align: center">
				<th>S.No.</th>
				<th>Transfer Id.</th>
				<th>Admission No.</th>
				<th>Class</th>
				<th>Student Name</th>
				<th>Issue Date</th>
			</tr>
		</thead>

		<tbody align="center">';
   $sn=1;
  $get_datail = "SELECT * FROM `transfer_certificate`
		INNER JOIN `student_user` ON student_user.sId = transfer_certificate.sid
		ORDER BY transfer_certificate.issue_date DESC";
  $exe_detail = mysql_query($get_datail);
 while($fetch_detail=mysql_fetch_array($exe_detail))
 {
     $student_id=$fetch_detail['sid'];
     $transfer_id = $fetch_detail[0];
     $name=$fetch_detail['Name'];
     $admission_no=$fetch_detail[2];
     $issue_date = $fetch_detail['issue_date'];
     

  $get_class = mysql_query("SELECT class_name FROM `struck_off_student`
		INNER JOIN `class_index` ON class_index.cId = struck_off_student.class_id
		WHERE student_id = $student_id");

  $fetch_class= mysql_fetch_array($get_class);

	$class_name = $fetch_class[0];


     echo'<tr align="center">
		   <td>'.$sn++.'.</td> 
		   <td>'.$transfer_id.'</td> 
		   <td>'.$admission_no.'</td>
		   <td>'.$class_name.'</td>
		   <td><a href="view_tc_details.php?transfer_id='.$transfer_id.'">'.$name.'</a></td>
		   <td>'.$issue_date.'</td>           
     	  </tr>';
     
 }

  echo' 	</tbody>
	</table>';
  
      

        echo '</div>
        </div>
        </div>';
}





function select_class_print_tc()
{
  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select Class for Transfer Certificate</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';

echo' <ol class="rounded-list">';

  $get_datail = "SELECT * FROM `class_index`  WHERE class_name NOT LIKE '%TEST%'
		ORDER BY level ASC, class_name ASC";
  $exe_detail = mysql_query($get_datail);
 while($fetch_detail=mysql_fetch_array($exe_detail))
 {
	echo'
<li>
	<a href="users/print_tc_classwise.php?class_id='.$fetch_detail['cId'].'&class='.$fetch_detail['class_name'].'">
		'.$fetch_detail['class_name'].'
	</a>
</li>';
 }

echo'</ol>';

        echo '</div>
        </div>
        </div>';
}





function view_tc_details()
{

	$transfer_id = $_GET['transfer_id'];

  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Transfer Certificate Details</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
<br>

<a class="btn btn- " href="users/re_print_transfer_certificate.php?transfer_id='.$transfer_id.'">PRINT TC</a><br><br>

   	<table class="table table-bordered table-striped" id="">
														
	<!--Header of the table-->

		<thead>
			<tr style="text-align: center">
				<th>S.No.</th>
				<th>Fields</th>
				<th>Details</th>
			</tr>
		</thead>

		<tbody align="center">';
   $sn=1;
  $get_datail = "SELECT * FROM `transfer_certificate`
		INNER JOIN `student_user` ON student_user.sId = transfer_certificate.sid
		WHERE `transfer_id` = $transfer_id";
  $exe_detail = mysql_query($get_datail);

	$fetch_detail=mysql_fetch_array($exe_detail);

	     $student_id=$fetch_detail['sid'];
	     $transfer_id = $fetch_detail[0];
	     $name=$fetch_detail['Name'];
	     $admission_no=$fetch_detail[2];
	     $issue_date = $fetch_detail['issue_date'];
	     
	     echo'<tr align="center">
			   <td>'.$sn++.'.</td> 
			   <td>Name</td>
			   <td>'.$name.'</td>          
	     	  </tr>


		<tr align="center">
			   <td>'.$sn++.'.</td> 
			   <td>Admission No.</td>
			   <td>'.$admission_no.'</td>          
     		</tr>


		<tr align="center">
			   <td>'.$sn++.'.</td> 
			   <td>Date of Leaving School</td>
<td><input type="text" id="date_of_leaving_school_'.$transfer_id.'" value="'.$fetch_detail['date_of_leaving_school'].'" onblur="save_tc_data('.$transfer_id.',this.value,\'date_of_leaving_school\')"></td>          
     		</tr>


		<tr align="center">
			   <td>'.$sn++.'.</td> 
			   <td>Reason Leaving School</td>
<td><input type="text" id="reason_leaving_school_'.$transfer_id.'" value="'.$fetch_detail['reason_leaving_school'].'" onblur="save_tc_data('.$transfer_id.',this.value,\'reason_leaving_school\')"></td>          
     		</tr>


		<tr align="center">
			   <td>'.$sn++.'.</td> 
			   <td>Attendance</td>
<td><input type="text" id="attendance_'.$transfer_id.'" value="'.$fetch_detail['attendance'].'" onblur="save_tc_data('.$transfer_id.',this.value,\'attendance\')"></td>          
     		</tr>


		<tr align="center">
			   <td>'.$sn++.'.</td> 
			   <td>Working Days</td>
<td><input type="text" id="working_days_'.$transfer_id.'" value="'.$fetch_detail['working_days'].'" onblur="save_tc_data('.$transfer_id.',this.value,\'working_days\')"></td>          
     		</tr>


		<tr align="center">
			   <td>'.$sn++.'.</td> 
			   <td>Last Class</td>
<td><input type="text" id="last_class_'.$transfer_id.'" value="'.$fetch_detail['last_class'].'" onblur="save_tc_data('.$transfer_id.',this.value,\'last_class\')"></td>          
     		</tr>


		<tr align="center">
			   <td>'.$sn++.'.</td> 
			   <td>Result</td>
<td><input type="text" id="result_'.$transfer_id.'" value="'.$fetch_detail['result'].'" onblur="save_tc_data('.$transfer_id.',this.value,\'result\')"></td>          
     		</tr>


		<tr align="center">
			   <td>'.$sn++.'.</td> 
			   <td>Promoted</td>
<td><input type="text" id="promoted_'.$transfer_id.'" value="'.$fetch_detail['promoted'].'" onblur="save_tc_data('.$transfer_id.',this.value,\'promoted\')"></td>          
     		</tr>


		<tr align="center">
			   <td>'.$sn++.'.</td> 
			   <td>General Conduct</td>
<td><input type="text" id="general_conduct_'.$transfer_id.'" value="'.$fetch_detail['general_conduct'].'" onblur="save_tc_data('.$transfer_id.',this.value,\'general_conduct\')"></td>          
     		</tr>


		<tr align="center">
			   <td>'.$sn++.'.</td> 
			   <td>Co-Curricular</td>
<td><input type="text" id="co_curricular_'.$transfer_id.'" value="'.$fetch_detail['co_curricular'].'" onblur="save_tc_data('.$transfer_id.',this.value,\'co_curricular\')"></td>          
     		</tr>


		<tr align="center">
			   <td>'.$sn++.'.</td> 
			   <td>Dues Paid Upto</td>
<td><input type="text" id="dues_paid_upto_'.$transfer_id.'" value="'.$fetch_detail['dues_paid_upto'].'" onblur="save_tc_data('.$transfer_id.',this.value,\'dues_paid_upto\')"></td>          
     		</tr>

		<tr align="center">
			   <td>'.$sn++.'.</td> 
			   <td>Issue Date</td>
<td><input type="text" id="issue_date_'.$transfer_id.'" value="'.$fetch_detail['issue_date'].'" onblur="save_tc_data('.$transfer_id.',this.value,\'issue_date\')"></td>          
     		</tr>

		<tr align="center">
			   <td>'.$sn++.'.</td> 
			   <td>Remarks</td>
<td><input type="text" id="remarks_'.$transfer_id.'" value="'.$fetch_detail['remarks'].'" onblur="save_tc_data('.$transfer_id.',this.value,\'remarks\')"></td>          
     		</tr>


 	</tbody>
</table>';
  
      

        echo '</div>
        </div>
        </div>';
}





function select_class_answer_sheet_performa()
{

echo '
	<div class="row-fluid">                            
     		<div class="span12  widget clearfix">                            

			<div class="widget-header">
			<span><i class="icon-align-center"></i>Manage Students</span>
			</div><!-- End widget-header -->		

			<div class="widget-content">';
	
	$get_classes = mysql_query("SELECT `cId`,`class_name` FROM class_index 
					ORDER BY level ASC, class*1 ASC, section ASC");

	echo '<h2 align="center"></h2>';
 
	echo '<ol class="rounded-list">';

while($fetch_classes = mysql_fetch_array($get_classes))
{
	echo '<li><a href="users/answer_sheet_receiving_performa.php?class_id='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';
}


echo '		</ol>
			</div>
		</div>
	</div>';

}





function apply_custom_filters()
{
echo '
	<div class="row-fluid">                            
     		<div class="widget  span12 clearfix">                            

			<div class="widget-header">
			<span><i class="icon-align-center"></i>Filtered Students List</span>
			</div><!-- End widget-header -->		

			<div class="widget-content">';

$fields = array();


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////// WHERE CLAUSE /////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$class = $_POST['class'];
$gender = $_POST['gender'];
$religion = $_POST['religion'];
$category = $_POST['category'];

$where = " WHERE";

if($class != "" && ($gender != "" || $religion != "" || $category != ""))
	$where .= " class_name LIKE '$class' AND";

else
	if($class != "")
		$where .= " class_name LIKE '$class'";

if($gender != "" && ($religion != "" || $category != ""))
	$where .= " gender LIKE '$gender' AND";

else
	if($gender != "")
		$where .= " gender LIKE '$gender'";

if($religion != "" && $category != "")
	$where .= " religion LIKE '$religion' AND";

else
	if($religion != "")
		$where .= " religion LIKE '$religion'";

if($category != "")
	$where .= " category LIKE '%$category%'";



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////// FIELDS LIST //////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if(isset($_POST['adm']))
	array_push($fields, 'admission_no');

if(isset($_POST['doa']))
	array_push($fields, 'admission_date');

if(isset($_POST['s_name']))
	array_push($fields, 'Name');

if(isset($_POST['father']))
	array_push($fields, 'Father\'s Name');

if(isset($_POST['mother']))
	array_push($fields, 'Mother\'s Name');

if(isset($_POST['classs']))
{
	array_push($fields, 'class_name');
	$join1 = " INNER JOIN class ON class.sId = student_user.sId INNER JOIN class_index ON class_index.cId = class.classId";
}

if(isset($_POST['contact']))
	array_push($fields, 'Phone No');

if(isset($_POST['genderr']))
	array_push($fields, 'gender');

if(isset($_POST['religionn']))
	array_push($fields, 'religion');

if(isset($_POST['address']))
	array_push($fields, 'Local Address');

if(isset($_POST['age']))
	array_push($fields, 'DOB');


$get_data = "SELECT student_user.sId, ";

foreach($fields as $val)
{
	$get_data .= "`".$val."`, ";
}

$get_data = substr_replace($get_data, "", "-2", "2");

$get_data .= " FROM student_user";

if(isset($_POST['class']))
{
	$get_data .= $join1;
}

$query = $get_data.$where;

$execute = mysql_query($query);


echo'
<div style="width: 100px; display: inline-block;">

	<form action="users/print_filtered_students_list.php" method="POST">';

	foreach($fields as $field)
	{
		echo'	<input type="hidden" name="fields[]" value="'.$field.'" />';
	}

		echo'	<input type="hidden" name="where" value="'.$where.'" />';

	echo'
		<button class="btn btn-primary" type="submit"><b> PRINT </b></button>
	</form>
</div>

<div style="width: 100px; display: inline-block;">
	<form action="users/send_sms_filtered_students.php" method="POST">

		<input type="hidden" name="sid" id="sid" value="" />
		<button class="btn btn-primary" type="submit"><b> SEND SMS </b></button>
	</form>
</div>
	';

echo'
<table class="table table-bordered table-striped" id="dataTable" >
	<thead>
		<tr>
			<th>S.No.</th>';
		
			foreach($fields as $head)
			{
				if($head == "Local Address")
					$head = "Address";

				if($head == "Phone No")
					$head = "Contact";

				if($head == "admission_date")
					$head = "Adm. Date";

				if($head == "admission_no")
					$head = "Adm. No.";

				if($head == "class_name")
					$head = "Class";

				echo '<th>'.$head.'</th>';
			}	
echo'
		</tr>
	</thead>';


$sn = 1;
$student_ids = '';

while($fetch_data = mysql_fetch_array($execute))
{
	$student_ids .= $fetch_data[0].'__';

	echo'
	<tr>
		<td align="center">'.$sn++.'</td>';
		
	foreach($fields as $field)
	{
		if($field == "admission_no" || $field == "class_name" || $field == "gender" || $field == "religion")
		{
			echo '<td align="center">'.$fetch_data[$field].'</td>';
		}

		else
		{
			echo '<td>'.$fetch_data[$field].'</td>';
		}
	}

echo'	
	</tr>';
}


echo'
	<span style="visibility: hidden;" id="stu_ids">'.$student_ids.'</span>
	
	<script>
		var val = document.getElementById("stu_ids").innerHTML;
		document.getElementById("sid").value = val;
	</script>
';

echo '	</table>	
			</div>
		</div>
	</div>';
}








function manage_users_ews_dashboard()
{
  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>EWS Dashboard</span>
      </div><!-- End widget-header -->
      <div class="widget-content">

<a class="btn btn-success" href="view_ews_students_select_class.php"><b>CLASSWISE</b></a>
<span style="margin-left: 5px;">
	<a class="btn btn-success" href="users/view_ews_students_all.php"><b>ALL</b></a>
</span>

</div>

</div>

</div>';
}



function manage_users_ews_select_class()
{
$ss=1;

$class_id = $_GET['class_id'];

  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i> Select Class</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';

$get_class = mysql_query("SELECT * FROM class_index WHERE class_name NOT LIKE '%test%' ORDER BY level ASC, class*1 ASC, section ASC");

echo' <ol class="rounded-list">';

	while($fetch_class = mysql_fetch_array($get_class))
	{
		echo'
		<li><a href="users/view_ews_students_classwise.php?class_id='.$fetch_class['cId'].'">'.$fetch_class['class_name'].'</a><li>';
	}

echo'</ol>';

echo'
</div>

</div>

</div>';
}





function edit_profile_teacher()
{
$uid = $_GET['id'];
$priv=2;

  echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i> Edit Profile</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';

$get_details = mysql_query("SELECT * FROM users WHERE uId = $uid");

$fetch_details = mysql_fetch_array($get_details);

echo'
<form id="validation_demo" action="users/save_profile_teacher.php" method="post" enctype="multipart/form-data">

<input type="hidden" name="uId" value="'.$uid.'" />
 
<div class="section ">
	<label> Profile Picture<small></small></label>
	<div>
		<img src="user_image/'.$fetch_details['image'].'" width="200px" height="200px" alt="image_error" /><br /><br />
		<span><b>Change Picture ..</b> &nbsp; &nbsp; <input type="file" class="validate[required] medium" name="image" /></span>
	</div>
</div>

<div class="section ">
	<label> Name<small></small></label>
	<div> 
		<input type="text" class="validate[required] medium" name="name" value="'.$fetch_details['Name'].'"/>
	</div>
</div>

<div class="section ">
	<label> D.O.B.<small>Date Format ( yyyy-mm-dd )</small></label>
	<div> 
		<input type="text"  id="birthday" class=" birthday  validate[required] medium" name="dob"   value="'.$fetch_details['DOB'].'"/>
	</div>
</div>

<div class="section ">
	<label> Gender<small>(Male / Female)</small></label>
	<div> 
		<input type="text" class="validate[required] medium" name="gender" value="'.$fetch_details['sex'].'"/>
	</div>
</div>

<div class="section numericonly">
	<label> Phone No.<small></small></label>
	<div> 
		<input type="text" class="validate[required] medium" name="phone" value="'.$fetch_details['Phone No'].'"/>
	</div>
</div>

<div class="section ">
	<label> Local Address<small></small></label>
	<div> 
		<textarea name="local_address" id="Textareaelastic"  class="medium"  cols="" rows=""  >'.$fetch_details['Local Address'].'</textarea>
	</div>
</div>

<div class="section ">
	<label> E-mail Address<small> enter valid email address (eg. john@mail.com)</small></label>
	<div> 
		<input type="text" class="validate[required] medium" name="email"  value="'.$fetch_details['Email'].'"/>
	</div>
</div>

<div class="section last">
<br />
	<div> 
		<button type="submit" class="btn btn-primary"> <b> &nbsp; SAVE PROFILE &nbsp; </b> </button>
	</div>
</div>


</form>

</div>

</div>

</div>';
}



?>
