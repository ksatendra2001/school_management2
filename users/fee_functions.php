<?php
require_once('sendsms/sendsms.php');



function fee_register_select_class()
{
  echo '<div class="row-fluid">			  

      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i> Select class</span>
      </div><!-- End widget-header -->

      <div class="widget-content">';
	


$q = "SELECT `cId`, `class_name` FROM class_index
	WHERE class_name NOT LIKE '%ALUM%' AND class_name NOT LIKE '%test%'
	ORDER BY class+0 ASC, section ASC";

$q_res=mysql_query($q);


echo "<br>";
echo '<ol class="rounded-list">';
while($res=mysql_fetch_array($q_res))
{
	echo'<li><a href="fee/print_fee_reg.php?cid='.$res['cId'].'">'.$res[1].'</a></li>';
}


echo'
</ol>
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 
}
//in this function select level of collection fee 
function accounts_collect_today_fee()
{
	//Author : Anil Kumar*
	
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Select Date and Level</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';	
	if (isset($_GET['no']))
		{
			echo '<h4 align="center"><span style="color:red">SELECT LEVEL AND DATE PROPERLY!</h4></span>';
		}
	echo'  
	<div class="load_page">
	<div class="formEl_b">	
	<form id="validation_demo" action="accounts_collect_today_fee_show.php" method="get"> 
	<div class="section">
	<label>Select Date</label>   
	<div><input type="text"  id="datepick" class="datepicker" readonly="readonly" name="date"  /></div>
	</div>
	<div class="section ">
	<label>Select Level <small></small></label>   
	<div> 
	<select name="level">
	<option value="0">---SELECT LEVEL---</option>
	<option value="1">Pre Primary</option>
	<option value="2"> Primary</option>
	<option value="3">Secondary</option>
	<option value="4">Senior Secondary</option>
	<option value="5">Class wise</option>
	<option value="6">School</option>
	</select>
	</div>
	</div>
	<div class="section last">
	<div>
	<button class="btn submit_form" >Submit</button>
	</div>
	</div>
	</form>
	</div>								
	</div>';
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';	
}

//function show fee collection details in select date
function accounts_collect_today_fee_show()
{
	//Author: Anil Kumar *
	$current_session=$_SESSION['current_session_id'];	
	//get date and level from accounts_collect_today_fee	
	$date_today=$_GET['date'];
	$level=$_GET['level'];
	//change format of date
	$date_format=date('Y-m-d',strtotime($date_today));
	
	//select date id fro dates_d table
	$date_query="SELECT date_id
				FROM dates_d
				WHERE date='".$date_format."'";
	$execute_date=mysql_query($date_query);
	$fetch_date=mysql_fetch_array($execute_date);
	$date=$fetch_date[0];
	//if level blanks then return previous page
	if(($level==0)||($level=="")||($date_today==""))
	  {
	  		header("location:accounts_collect_today_fee.php?no");	
	  }
	
	//show fee according level 
	// if level is school or class
	//in this level show fee classwise 
	else if($level==5)
	{
		echo'
		<div class="row-fluid">
		<!-- Table widget -->
		<div class="widget  span12 clearfix">
		<div class="widget-header">
		<span>Fee Not Pay Students</span>
		</div><!-- End widget-header -->	
		<div class="widget-content">
		';	
		//query to get all class
		$query_class="SELECT *
		             FROM class_index
		              ";
		$execute_class=mysql_query($query_class);
		echo '
		<div class="section last">
		<label>SELECT CLASS</label>
		<div > <ol class="rounded-list">';				  
		while($fetch_class=mysql_fetch_array($execute_class))
		{ 
			echo' <li><a href="accounts_fee_show_class_level.php?cId='.$fetch_class['cId'].'
			&&date='.$date_today.'">'.$fetch_class['class_name'].'</a></li> ';	
		}
		echo'
		</ol>
		</div>
		</div>
		';	
		echo'
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	  </div><!-- row-fluid -->';	 
	}
	
	//in this level all school fee show
	else  if($level==6)
	{
		//query to get students details whose fee paid students details
		$stu_fee_details="SELECT DISTINCT student_user.`Name`, student_user.admission_no, student_user.`Father's Name`,
						   class.classId, fee_generated_sessions.duration, fee_generated_sessions.last_date,                            fee_details.mode_of_payment,
							fee_details.student_id, fee_details.fee_generated_session_id, fee_generated_sessions.type,
							student_user.sId
							   
							FROM fee_details 
							
							INNER JOIN student_user 
							ON student_user.sId=fee_details.student_id
							
							INNER JOIN fee_generated_sessions 
							ON fee_generated_sessions.id=fee_details.fee_generated_session_id
							
							INNER JOIN class
							ON class.sId=fee_details.student_id 
							
							INNER JOIN class_index
							ON class_index.cId=class.classId
							
							WHERE fee_details.paid_on=".$date."
							AND fee_details.paid=1 
							";
		$execute_fee_paid=mysql_query($stu_fee_details);
		
		$i=1;
		$today_collection=0;
		echo'
		<div class="row-fluid">
		<!-- Table widget -->
		<div class="widget  span12 clearfix">
		<div class="widget-header">
		<span>FEE PAID DETAILS OF SELECT DATE</span>
		</div><!-- End widget-header -->	
		<div class="widget-content">
		<table  class="table table-bordered table-striped" id="dataTable" >
		<thead>
		<tr>
		<th>S. no.</th>
		<th>Admission no.</th>
		<th>Student Name</th>
		<th>Father`s Name</th>
		<th>Class</th>
		<th>Duration</th>
		<th>Last Date</th>
		<th>Payment Mode</th>
		<th> Fee </th>
		<th>Hostel Fee </th>
		<th>Total Fee </th>
		</tr>
		</thead>
		<tbody align="center">
		';	
		while($fetch_fee_paid=mysql_fetch_array($execute_fee_paid))
			{
				$date_id_last=$fetch_fee_paid[5];
				$month=$fetch_fee_paid[9];
				//select class name from class index
				$class="SELECT class_name
					   FROM class_index
					   WHERE cId=".$fetch_fee_paid[3]."";
				$exe_class=mysql_query($class);
				$fetch_class=mysql_fetch_array($exe_class);
				echo '
				<tr>
				<td>'.$i.'</td>
				<td>'.$fetch_fee_paid[1].'</td>
				<td>'.$fetch_fee_paid[0].'</td>
				<td>'.$fetch_fee_paid[2].'</td>
				<td>'.$fetch_class['0'].'</td>
				<td>'.$fetch_fee_paid[4].'</td>
				';
				//query to get date from dates _d
				$last_date="SELECT date
						   FROM dates_d
						   WHERE date_id=".$date_id_last."";
				$exe_last_d=mysql_query($last_date);
				$fetch_last_d=mysql_fetch_array($exe_last_d);
				//change date format
				$date_last=date('Y-m-d',strtotime($_GET['date']));
				echo '
				<td>'.$fetch_last_d[0].'</td>
				
				<td>'.$fetch_fee_paid[6].'</td>
				';
			    //QUERY get cost of transport from destination_rouite------
				$transport_cost=0;
				$trans_amount="SELECT cost
							   FROM destination_route
							   
							   INNER JOIN user_vehicle_details
							   ON user_vehicle_details.did=destination_route.did
							   
							   WHERE user_vehicle_details.uId=".$fetch_fee_paid[7]."
							   AND destination_route.session_id=".$_SESSION['current_session_id']."";
				$exe_amount_t=mysql_query($trans_amount);
				while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
					{
						$transport_cost=$transport_cost+$fetch_transport_cost[0];
					}
				//query get rent of hostel from room allocation --------
				$hostel_rent=0;
				$hostel_amount="SELECT rent 
								FROM hostel_room_allocation
								WHERE sid=".$fetch_fee_paid[7]."
								AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
				$exe_rent=mysql_query($hostel_amount);
				while($fetch_rent_cost=mysql_fetch_array($exe_rent))
					{
						$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
					}
				$trans_rent_cost=$hostel_rent+$transport_cost;
				$i++;
				// query to calculate total fee amount
				//query calculate total credentials amount
				$camount=0;
				$fetch_credentials_type="SELECT DISTINCT fee_credentials.type,fee_credentials.amount
										FROM fee_credentials
										INNER JOIN fee_details
										ON fee_details.fee_credential_id=fee_credentials.id
										WHERE student_id=".$fetch_fee_paid[7]."
										AND fee_credentials.session_id=".$current_session."
										AND fee_details.fee_generated_session_id=".$fetch_fee_paid[8]."";
				$execute_credentials_type=mysql_query($fetch_credentials_type);
				
				//loop get total credentials amount
				while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
					{ 
					    $camount=$camount+$fetch_credentials['amount'];
					}
				//query calculate total amount of discount 
				$total_discount=0;
				$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
								FROM fee_discount_credentials
								INNER JOIN fee_details
								ON fee_details.fee_discount_id=fee_discount_credentials.id
								WHERE student_id=".$fetch_fee_paid[7]."
								AND fee_discount_credentials.session_id=".$current_session."
								AND fee_details.fee_generated_session_id=".$fetch_fee_paid[8]."";
				$execute_discount_type=mysql_query(  $fetch_discount_query);
				
				//LOOP get total discount amounts
				while($fetch_discount=mysql_fetch_array($execute_discount_type))
					{ 
					    $total_discount=$total_discount+$fetch_discount['amount'];
					}
				//calculate total fee (credentials amount - discount amount)
				$total_fee_amount=$camount-$total_discount+$trans_rent_cost;
				$fee=$total_fee_amount-$hostel_rent;
				//calculate fee amount according to duration
				//Monthly=1  Quarterly-3, half early=6 and annually=12
				$m=1;
				if($month=="Monthly")
					{
					 	$m=1; 
					}
				else if($month=="Quarterly")
					{
				   	 	$m=3; 
					}
				else if($month=="Half-Yearly")
					{
						 $m=6;  
					}	
				else if($month=="Yearly")
					{
						$m=12;  
					}
				//calculate total fee according duration and total today collection
				$total_fee=$total_fee_amount*$m;
				$today_collection=$today_collection+$total_fee;
				echo '
				<td>'.$fee.'</td>
				<td>'.$hostel_rent.'</td>
				<td>'.$total_fee.'</td>
				</tr>';
		   }
			 
		  //select date from dates_d table
		  $date_d="SELECT date 
				  From dates_d
				  WHERE date_id=".$date."";
		  $exe_date=mysql_query($date_d);
		  $fetch_date=mysql_fetch_array($exe_date);
		  //change date format
		  $fetch_date_format=date('jS F Y',strtotime($fetch_date[0]));
		  echo '<h5 align="center" style="color:green">  Total Fee Collection of '.$fetch_date_format.' is: 
		  <b style="color:red">'.$today_collection.'</b></h5>';
		  echo'
		  </tbody>
		  </table>
		  </div><!--  end widget-content -->
		  </div><!-- widget  span12 clearfix-->
		  </div><!-- row-fluid -->'; 
	 }
	 
	 //show fee primry or secondary or seneior secondary or pre primary 
	 else
		{
			//query to get students details whose fee paid
			$stu_fee_details="SELECT DISTINCT student_user.`Name`, student_user.admission_no, student_user.`Father's Name`,
							 class.classId, fee_generated_sessions.duration, 
							 fee_generated_sessions.last_date, fee_details.mode_of_payment,
							 fee_details.student_id, fee_details.fee_generated_session_id, fee_generated_sessions.type
								 
							FROM fee_details 
							
							INNER JOIN student_user 
							ON student_user.sId=fee_details.student_id
							
							INNER JOIN fee_generated_sessions 
							ON fee_generated_sessions.id=fee_details.fee_generated_session_id
							
							INNER JOIN class
							ON class.sId=fee_details.student_id 
							
							INNER JOIN class_index
							ON class_index.cId=class.classId
							
							WHERE fee_details.paid_on=".$date."
							AND fee_details.paid=1 
							AND class_index.level=".$level."";
			
			 $execute_fee_paid=mysql_query($stu_fee_details);
			 $i=1;
			 $today_collection=0;
			 echo'
			<div class="row-fluid">
			<!-- Table widget -->
			<div class="widget  span12 clearfix">
			<div class="widget-header">
			<span>FEE PAID DETAILS OF SELECT DATE</span>
			</div><!-- End widget-header -->	
			<div class="widget-content">
			<table  class="table table-bordered table-striped" id="dataTable" >
			<thead>
			<tr>
			<th>S. no.</th>
			<th>Admission no.</th>
			<th>Student Name</th>
			<th>Father`s Name</th>
			<th>Class</th>
			<th>Duration</th>
			<th>Last Date</th>
			<th>Payment Mode</th>
			<th>Fee</th>
			<th>Hostel Fee</th>
			<th>Total Fee</th>
			</tr>
			</thead>
			<tbody align="center">
			';	
			while($fetch_fee_paid=mysql_fetch_array($execute_fee_paid))
			  {
				$month=$fetch_fee_paid[9];
				$date_id_last=$fetch_fee_paid[5];
				//select class name from class index
				$class="SELECT class_name
						FROM class_index
						WHERE cId=".$fetch_fee_paid[3]."";
				$exe_class=mysql_query($class);
				$fetch_class=mysql_fetch_array($exe_class);
				
				echo '<tr>
				<td>'.$i.'</td>
				<td>'.$fetch_fee_paid[1].'</td>
				<td>'.$fetch_fee_paid[0].'</td>
				<td>'.$fetch_fee_paid[2].'</td>
				<td>'.$fetch_class['0'].'</td>
				<td>'.$fetch_fee_paid[4].'</td>
				';
				//query to get date from dates _d
				$last_date="SELECT date
				FROM dates_d
				WHERE date_id=".$date_id_last."";
				$exe_last_d=mysql_query($last_date);
				$fetch_last_d=mysql_fetch_array($exe_last_d);
				//change date format
				$date_last=date('Y-m-d',strtotime($_GET['date']));
				echo '
				<td>'.$fetch_last_d[0].'</td>
				
				<td>'.$fetch_fee_paid[6].'</td>
				';
				$i++;
				//QUERY get cost of transport from destination_rouite------
				$transport_cost=0;
				$trans_amount="SELECT cost
							   FROM destination_route
							   
							   INNER JOIN user_vehicle_details
							   ON user_vehicle_details.did=destination_route.did
							   
							   WHERE user_vehicle_details.uId=".$fetch_fee_paid[7]."
							   AND destination_route.session_id=".$_SESSION['current_session_id']."";
				$exe_amount_t=mysql_query($trans_amount);
				while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
					{
						$transport_cost=$transport_cost+$fetch_transport_cost[0];
					}
				//query get rent of hostel from room allocation --------
				$hostel_rent=0;
				$hostel_amount="SELECT rent 
								FROM hostel_room_allocation
								WHERE sid=".$fetch_fee_paid[7]."
								AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
				$exe_rent=mysql_query($hostel_amount);
				while($fetch_rent_cost=mysql_fetch_array($exe_rent))
					{
						$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
					}
				$trans_rent_cost=$hostel_rent+$transport_cost;
				// query to calculate total fee amount
				//query calculate total credentials amount
				$camount=0;
				$fetch_credentials_type="SELECT DISTINCT fee_credentials.type,fee_credentials.amount
										FROM fee_credentials
										INNER JOIN fee_details
										ON fee_details.fee_credential_id=fee_credentials.id
										WHERE student_id=".$fetch_fee_paid[7]."
										AND fee_credentials.session_id=".$current_session."
										AND fee_details.fee_generated_session_id=".$fetch_fee_paid[8]."";
				$execute_credentials_type=mysql_query($fetch_credentials_type);
				while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
					{
						$camount=$camount+$fetch_credentials['amount'];
					}
				//query calculate total amount of discount 
				$total_discount=0;
				$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
									  FROM fee_discount_credentials
									  INNER JOIN fee_details
									  ON fee_details.fee_discount_id=fee_discount_credentials.id
									  WHERE student_id=".$fetch_fee_paid[7]."
									  AND fee_discount_credentials.session_id=".$current_session."
									  AND fee_details.fee_generated_session_id=".$fetch_fee_paid[8]."";
				$execute_discount_type=mysql_query(  $fetch_discount_query);
				while($fetch_discount=mysql_fetch_array($execute_discount_type))
					{ 
						$total_discount=$total_discount+$fetch_discount['amount'];
					}
				//calculate total fee (credentials amount - discount amount)
				$total_fee_amount=$camount-$total_discount+$trans_rent_cost;
				$fee=$total_fee_amount-$hostel_rent;
				//calculate fee amount according to duration
				//Monthly=1  Quarterly-3, half early=6 and annually=12
				$m=1;
				if($month=="Monthly")
					{
						$m=1; 
					}
				else if($month=="Quarterly")
					{
						$m=3; 
					}
				else if($month=="Half-Yearly")
					{
						$m=6;  
					}	
				else if($month=="Yearly")
					{
						$m=12;  
					}
				//calculate total fee according duration and total today collection
				$total_fee=$total_fee_amount*$m;
				$today_collection=$today_collection+$total_fee;
				echo '
				<td>'.$fee.'</td>
				<td>'.$hostel_rent.'</td>
				<td>'.$total_fee.'</td>
				</tr>';	
			}
		//get date from dates_d		 
		$date_d="SELECT date 
		From dates_d
		WHERE date_id=".$date."";
		$exe_date=mysql_query($date_d);
		$fetch_date=mysql_fetch_array($exe_date);
		$fetch_date_format=date('jS F Y',strtotime($fetch_date[0]));
		//show total amount of all students
		echo '<h5 align="center" style="color:green">  Total Fee Collection of '.$fetch_date_format.' is:
		<b style="color:red">'.$today_collection.'</b></h5>';
		echo'
		</tbody>
		</table>
		</div><!--  end widget-content -->
		</div><!-- widget  span12 clearfix-->
		</div><!-- row-fluid -->';
	}
}

//show fee collection according to class
function accounts_fee_show_class_level()
{
	//Author By: Anil Kumar *
	
	$current_session=$_SESSION['current_session_id'];
	//get class id and date from page name =>accounts collect today fee show
	$cid=$_GET['cId'];
	$date_today=$_GET['date'];
	$date_format=date('Y-m-d',strtotime($_GET['date']));
	
	//select date id fro dates_d table
	$date_query="SELECT date_id
				FROM dates_d
				WHERE date='".$date_format."'";
	$execute_date=mysql_query($date_query);
	$fetch_date=mysql_fetch_array($execute_date);
	$date=$fetch_date[0];
	
	//query to get students details whose fee paid
	$stu_fee_details="SELECT DISTINCT student_user.`Name`, student_user.admission_no, student_user.`Father's Name`,
					class.classId, fee_generated_sessions.duration, 
					fee_generated_sessions.last_date, fee_details.mode_of_payment,
					fee_details.student_id, fee_details.fee_generated_session_id, fee_generated_sessions.type
					 
					FROM fee_details 
					
					INNER JOIN student_user 
					ON student_user.sId=fee_details.student_id
					
					INNER JOIN fee_generated_sessions 
					ON fee_generated_sessions.id=fee_details.fee_generated_session_id
					
					INNER JOIN class
					ON class.sId=fee_details.student_id 
					
					INNER JOIN class_index
					ON class_index.cId=class.classId
					
					WHERE fee_details.paid_on=".$date."
					AND fee_details.paid=1 
					AND class_index.cId=".$cid." ";
	$execute_fee_paid=mysql_query($stu_fee_details);
	$i=1;
	$today_collection=0;
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>FEE PAID DETAILS OF SELECT DATE</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th>S. no.</th>
	<th>Admission no.</th>
	<th>Student Name</th>
	<th>Father`s Name</th>
	<th>Class</th>
	<th>Duration</th>
	<th>Last Date</th>
	<th>Payment Mode</th>
	<th>Fee</th>
	<th>Hostel Fee</th>
	<th> Fee Amount</th>
	</tr>
	</thead>
	<tbody align="center">
	';	
	while($fetch_fee_paid=mysql_fetch_array($execute_fee_paid))
		{
			$month=$fetch_fee_paid[9];
			$date_id_last=$fetch_fee_paid[5];
			//select class name from class index
			$class="SELECT class_name
			FROM class_index
			WHERE cId=".$fetch_fee_paid[3]."";
			$exe_class=mysql_query($class);
			$fetch_class=mysql_fetch_array($exe_class);
			
			echo '<tr>
			<td>'.$i.'</td>
			<td>'.$fetch_fee_paid[1].'</td>
			<td>'.$fetch_fee_paid[0].'</td>
			<td>'.$fetch_fee_paid[2].'</td>
			<td>'.$fetch_class['0'].'</td>
			<td>'.$fetch_fee_paid[4].'</td>';
			//query to get date from dates _d
			$last_date="SELECT date
					   FROM dates_d
					   WHERE date_id=".$date_id_last."";
			$exe_last_d=mysql_query($last_date);
			$fetch_last_d=mysql_fetch_array($exe_last_d);
			//change date format
			$date_last=date('Y-m-d',strtotime($_GET['date']));
			echo '
			<td>'.$fetch_last_d[0].'</td>
			<td>'.$fetch_fee_paid[6].'</td>
			';
			//QUERY get cost of transport from destination_rouite------
				$transport_cost=0;
				$trans_amount="SELECT cost
							   FROM destination_route
							   
							   INNER JOIN user_vehicle_details
							   ON user_vehicle_details.did=destination_route.did
							   
							   WHERE user_vehicle_details.uId=".$fetch_fee_paid[7]."
							   AND destination_route.session_id=".$_SESSION['current_session_id']."";
				$exe_amount_t=mysql_query($trans_amount);
				while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
					{
						$transport_cost=$transport_cost+$fetch_transport_cost[0];
					}
				//query get rent of hostel from room allocation --------
				$hostel_rent=0;
				$hostel_amount="SELECT rent 
								FROM hostel_room_allocation
								WHERE sid=".$fetch_fee_paid[7]."
								AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
				$exe_rent=mysql_query($hostel_amount);
				while($fetch_rent_cost=mysql_fetch_array($exe_rent))
					{
						$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
					}
				$trans_rent_cost=$hostel_rent+$transport_cost;
			$i++;
			// query to calculate total fee amount
			//query calculate total credentials amount
			$camount=0;
			$fetch_credentials_type="SELECT DISTINCT fee_credentials.type,fee_credentials.amount
									FROM fee_credentials
									INNER JOIN fee_details
									ON fee_details.fee_credential_id=fee_credentials.id
									WHERE student_id=".$fetch_fee_paid[7]."
									AND fee_credentials.session_id=".$current_session."
									AND fee_details.fee_generated_session_id=".$fetch_fee_paid[8]."";
			$execute_credentials_type=mysql_query($fetch_credentials_type);
			while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
				{
					$camount=$camount+$fetch_credentials['amount'];
				}
			//query calculate total amount of discount 
			$total_discount=0;
			$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
									FROM fee_discount_credentials
									INNER JOIN fee_details
									ON fee_details.fee_discount_id=fee_discount_credentials.id
									WHERE student_id=".$fetch_fee_paid[7]."
									AND fee_discount_credentials.session_id=".$current_session."
									AND fee_details.fee_generated_session_id=".$fetch_fee_paid[8]."";
			$execute_discount_type=mysql_query(  $fetch_discount_query);
			while($fetch_discount=mysql_fetch_array($execute_discount_type))
				{ 
					$total_discount=$total_discount+$fetch_discount['amount'];
				}
			//calculate total fee amount (credentials amount - discount amount)
			$total_fee_amount=$camount-$total_discount+$trans_rent_cost;
			$fee=$total_fee_amount-$hostel_rent;
			//calculate fee amount according to duration
			//Monthly=1  Quarterly-3, half early=6 and annually=12
			$m=1;
			if($month=="Monthly")
				{
					$m=1; 
				}
			else if($month=="Quarterly")
				{
					$m=3; 
				}
			else if($month=="Half-Yearly")
				{
					$m=6;  
				}	
			else if($month=="Yearly")
				{
					$m=12;  
				}
			//calculate total fee according duration and total today collection
			$total_fee=$total_fee_amount*$m;
			$today_collection=$today_collection+$total_fee;
			echo '
			<td>'.$fee.'</td>
			<td>'.$hostel_rent.'</td>
			<td>'.$total_fee.'</td>
			</tr>';
		}
	//select date from date_d
	$date_d="SELECT date 
			From dates_d
			WHERE date_id=".$date."";
	$exe_date=mysql_query($date_d);
	$fetch_date=mysql_fetch_array($exe_date);
	$fetch_date_format=date('jS F Y',strtotime($fetch_date[0]));
	echo '<h5 align="center" style="color:green">  Total Fee Collection of '.$fetch_date_format.' is: 
	<b style="color:red">'.
	$today_collection.'</b></h5>';
	echo'
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';	
}
//function to generate the student fee---         
function students_fee_student_generated()
{
	//Author By: Anil Kumar *
	$student_id = $_SESSION['user_id'];
	$current_session=$_SESSION['current_session_id'];
	//Query to get the class of the user student
	$query_class_student = "SELECT class_index.class_name
							FROM class_index
								
							INNER JOIN class
							ON class.classId = class_index.cId
							
							WHERE class.sId = $student_id
							";
	$execute_class_student = mysql_query($query_class_student);
	$class_student = mysql_fetch_array($execute_class_student);
	$class_name = $class_student[0];
	//write query to get the session id from  the fee generated session
	$fetch_session_name="SELECT `session_name`
						FROM `session_table` 
						WHERE `sId`=".$current_session."";
	$execute_session_name=mysql_query($fetch_session_name);
	$fetch_session=mysql_fetch_array($execute_session_name);
	$session_name=$current_session['session_name'];
	//QUERY get cost of transport from destination_rouite------
	$transport_cost=0;
	$trans_amount="SELECT cost
	               FROM destination_route
				   
				   INNER JOIN user_vehicle_details
				   ON user_vehicle_details.did=destination_route.did
				   
				   WHERE user_vehicle_details.uId=$student_id
				   AND destination_route.session_id=".$_SESSION['current_session_id']."";
	$exe_amount_t=mysql_query($trans_amount);
	while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
		{
			$transport_cost=$transport_cost+$fetch_transport_cost[0];
		}
	//query get rent of hostel from room allocation --------
	$hostel_rent=0;
	$hostel_amount="SELECT rent 
	                FROM hostel_room_allocation
					WHERE sid=$student_id
					AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
	$exe_rent=mysql_query($hostel_amount);
	while($fetch_rent_cost=mysql_fetch_array($exe_rent))
		{
			$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
		}
	$trans_rent_cost=$hostel_rent+$transport_cost;
	echo'  <div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span> Name: '.$_SESSION['name'].'</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- Table UITab -->
	<div id="UITab" style="position:relative;">
	 <h5 style="color:grey">Class : '.$class_name.'</h5>  <h5 align="center"> Current Session : 
	 '.$fetch_session['session_name'].'</h5>	 
	<table  class="table table-bordered table-striped">';
	 echo ' <thead>
	<tr>
	<th>Duration</th>
	<th>Due/Paid Date</th>
	<th>Late Fine/in Rs.</th>
	<th>Due/Paid Amount</th>
	<th>Status</th>
	<th>Fee Receipt</th>
	<th width="20%">Remarks</th>
	</tr>
	</thead>
	<tbody align="center">';
	$finee="fine";
	$fine=0;
	//write query to fetch fine amount
	$query_fine="SELECT `amount` 
				FROM `fee_credentials`
				WHERE  `type`='".$finee."'
				AND fee_credentials.session_id=".$current_session."";
	$execute_fine=mysql_query($query_fine);
	$fetch_fine=mysql_fetch_array($execute_fine);
	$fine=$fetch_fine['amount'];
	$total_discount=0;
	$camount=0;
	$flag=1;
	$aflag=0;
	$ai=1;
	//write query to get fee generated session id 
	$query_fee_ge_session_id="SELECT `Id` , `type`
							  FROM `fee_generated_sessions`
							  WHERE `session_id`=".$_SESSION['current_session_id']."";
	$execute_fee=mysql_query($query_fee_ge_session_id);
	while($fetch_fee=mysql_fetch_array($execute_fee))
		{
			$month=$fetch_fee['type'];
			// query to calculate total fee amount
			//query calculate total credentials amount
			$camount=0;
			$fetch_credentials_type="SELECT DISTINCT fee_credentials.type,fee_credentials.amount
			FROM fee_credentials
			INNER JOIN fee_details
			ON fee_details.fee_credential_id=fee_credentials.id
			WHERE student_id=".$_SESSION['user_id']."
			AND fee_credentials.session_id=".$current_session."
			AND fee_details.fee_generated_session_id=".$fetch_fee[0]."";
			$execute_credentials_type=mysql_query($fetch_credentials_type);
			while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
				{
					$camount=$camount+$fetch_credentials['amount'];
				}
			//query calculate total amount of discount 
			$total_discount=0;
			$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
									FROM fee_discount_credentials
									INNER JOIN fee_details
									ON fee_details.fee_discount_id=fee_discount_credentials.id
									WHERE fee_details.student_id=".$_SESSION['user_id']."
									AND fee_discount_credentials.session_id=".$current_session."
									AND fee_details.fee_generated_session_id=".$fetch_fee[0]."";
			$execute_discount_type=mysql_query(  $fetch_discount_query);
			while($fetch_discount=mysql_fetch_array($execute_discount_type))
				{ 
					$total_discount=$total_discount+$fetch_discount['amount'];
				}
			$total_fee_amount=$camount+$trans_rent_cost-$total_discount;
			//write query to get the paid details from the fee details
			$fetch_query_paid="SELECT `paid`
								FROM `fee_details`
								WHERE `fee_generated_session_id`=".$fetch_fee['Id']."
								AND `student_id`=".$_SESSION['user_id']."";
			$execute_paid=mysql_query($fetch_query_paid);
			$fetch_paid=mysql_fetch_array($execute_paid);
			$paid_value=$fetch_paid['paid'];
			//if fee details table is empty then if condition show no data available and break
			if($paid_value=="")
				{
					echo  "";
				}
			//check fee paid or not if $paid_value==0 not paid else paid
			else if($paid_value==0)
				{
					$ai++;
					//write query to select all fee details if fee not paid		
				    $get_detail="SELECT DISTINCT fee_generated_sessions.duration ,
								fee_generated_sessions.last_date, fee_generated_sessions.Id,
								fee_details.remarks              
								
								FROM fee_generated_sessions
								
								INNER JOIN fee_details
								ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
								 
								WHERE fee_details.student_id = ".$_SESSION['user_id']."
								AND fee_generated_sessions.session_id=".$current_session."
								AND fee_generated_sessions.Id = ".$fetch_fee[0]."
								AND fee_details.fee_generated_session_id=".$fetch_fee[0]."";		
					$exe_details=mysql_query($get_detail);
					$ch=array();
					$paid=array();
					$duration=array();
					$i=0;
					echo '<tr>';
					$fetch_details=mysql_fetch_array($exe_details);
					//calculate fee amount according to duration
					//Monthly=1  Quarterly-3, half early=6 and annually=12
					$m=1;
					if($month=="Monthly")
						{
							$m=1; 
						}
					else if($month=="Quarterly")
						{
							$m=3; 
						}
					else if($month=="Half-Yearly")
						{
							$m=6;  
						}	
					else if($month=="Yearly")
						{
							$m=12;  
						}
					$total_fee=$total_fee_amount*$m;
					//query to get date on the id of date
					$get_date_of_id=
									"SELECT DISTINCT date
									FROM dates_d
									WHERE date_id = ".$fetch_details['last_date']."";
					$exe_date=mysql_query($get_date_of_id);
					$fetch_date=mysql_fetch_array($exe_date);
					$due_date = $fetch_date[0];
					//get current date and show fine
					date_default_timezone_set('Asia/Kolkata');
		             $current_date=date('Y-m-d');
					  $current_id="SELECT date_id FROM dates_d
					             WHERE date='".$current_date."'";
					 $exe_current=mysql_query($current_id);
					 $fetch_current_date=mysql_fetch_array($exe_current);
					 if($fetch_details['last_date']<$fetch_current_date[0])
					  {
						$fine_days=$fetch_current_date[0] - $fetch_details['last_date'];
						if($fine_days<=30)
						{
							$calculate_fine=100;
						}
						else if($fine_days<=60)
						{
							$calculate_fine=200;
						}
						else
						{
							$calculate_fine=500;
						}
						/*$calculate_fine=$fine_days*$fine; */
					}
				else
					{
						$calculate_fine=0;  
					}
					//calculate total fee with fine amounts
					$tota_fee_with_fine = $total_fee + $calculate_fine;
					echo '<td><a href="students_fee_not_paid_details.php?id='.$fetch_details[2].'">'.$fetch_details[0].'</a>
					</td>
					<td>'.$due_date.'</td>
					<td>'.$calculate_fine.'</td>
					<td>'.$tota_fee_with_fine.'</td>
					<td><h5 style="color:red">Fee Not paid</h5></td>
					<td>NA</td> 
					<td>'.$fetch_details['remarks'].'</td> 
					</tr>';
				}
			else
			{
			 	$ai++;	 
				echo '	<tbody align="center">';
				$get_detail="SELECT DISTINCT fee_generated_sessions.duration,
							fee_generated_sessions.last_date , fee_details.paid , fee_generated_sessions.Id
							 ,fee_details.paid_on
							, fee_details.remarks
							FROM fee_details
							
							INNER JOIN fee_generated_sessions
							
							ON fee_generated_sessions.id = fee_details.fee_generated_session_id 
							
							WHERE fee_details.student_id = ".$_SESSION['user_id']."
							AND fee_generated_sessions.session_id=".$current_session."
							AND fee_generated_sessions.Id = ".$fetch_fee[0]."
							AND fee_details.fee_generated_session_id=".$fetch_fee[0]."";
				$exe_details=mysql_query($get_detail);
				$ch=array();
				$paid=array();
				$duration=array();
				$i=0;
				echo '<tr>';
				$fetch_details=mysql_fetch_array($exe_details);
				$due_date =$fetch_details['last_date'];
				//query to get date on the id of date
				$get_date_of_id=
								"SELECT DISTINCT date
								FROM dates_d
								WHERE date_id = ".$fetch_details['paid_on']."";
				$exe_date=mysql_query($get_date_of_id);
				$fetch_date=mysql_fetch_array($exe_date);
				$paid_on_date = $fetch_date[0];
				$paid_date=$fetch_details['paid_on'];
				//check condition due date < paid on date
				//and calculate fine
				if($due_date<$paid_date)
					{
						$fine_days=$paid_date-$due_date;
						if($fine_days<=30)
						{
							$calculate_fine=100;
						}
						else if($fine_days<=60)
						{
							$calculate_fine=200;
						}
						else
						{
							$calculate_fine=500;
						}
						/*$calculate_fine=$fine_days*$fine; */
					}
				else
					{
						$calculate_fine=0;  
					}
				//calculate fee amount according to duration
				//Monthly=1  Quarterly-3, half early=6 and annually=12
				$m=1;
				if($month=="Monthly")
					{
						$m=1; 
					}
				else if($month=="Quarterly")
					{
						$m=3; 
					}
				else if($month=="Half-Yearly")
					{
						$m=6;  
					}	
				else if($month=="Yearly")
					{
						$m=12;  
					}
				else
					{
						$m=1; 
					}
				//calculate total fee amount with duration
				$t_fee=$total_fee_amount*$m;
				//calculate total fee amount with fine
				$pay_amount=$t_fee+ $calculate_fine;
				echo '<td><a href="students_fee_not_paid_details.php?id='.$fetch_details[3].'">'.$fetch_details[0].'</a></td>
				<td>'.$paid_on_date.'</td>
				<td>'.$calculate_fine.'</td>
				<td>'.$pay_amount.'</td>';
				if($paid_value==3)
				{
					echo '
				<td><h5 style="color:green">Cheque Pending</h5></td>
				<td><a href="students_fee_paid_details.php?id='.$fetch_fee[0].'"> Show</a></td>
				<td>'.$fetch_details['remarks'].'</td></tr>';	
				}
				else
				{
				echo '
				<td><h5 style="color:green">Fee Paid</h5></td>
				<td><a href="students_fee_paid_details.php?id='.$fetch_fee[0].'"> Show</a></td>
				<td>'.$fetch_details['remarks'].'</td></tr>';
				}
				echo '';
			}
		}
	echo ' 
	</tbody>';
	if($ai==1)
		{
			echo '<h5 style="color:green" align="center">NO DATA AVAILABLE</h5>';
		}
	echo '</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div>
	</div><!-- row-fluid -->';
}

//function to show the student fee credentials details discount and  paid fee
function students_fee_not_paid_details()
{
	//Author By: Anil Kumar *
	
	$duid=$_GET['id'];
	$student_id = $_SESSION['user_id'];
	$current_session=$_SESSION['current_session_id'];
	//Query to get the class of the user student
	$query_class_student = "SELECT class_index.class_name
							FROM class_index
							
							INNER JOIN class
							ON class.classId = class_index.cId
							
							WHERE class.sId = $student_id
							";
	$execute_class_student = mysql_query($query_class_student);
	$class_student = mysql_fetch_array($execute_class_student);
	//write query to get the session id from  the fee generated session
	$fetch_session_name="SELECT `session_name`
						FROM `session_table` 
						WHERE `sId`=".$current_session."";
	$execute_session_name=mysql_query($fetch_session_name);
	$fetch_session=mysql_fetch_array($execute_session_name);
	$session_name=$current_session['session_name'];
	$class_name = $class_student[0];
	$total_discount=0;
	$camount=0;	
	$serial_no=1;
	//QUERY get cost of transport from destination_rouite------
	$tranport_cost=0;
	$trans_amount="SELECT cost
	               FROM destination_route
				   
				   INNER JOIN user_vehicle_details
				   ON user_vehicle_details.did=destination_route.did
				   
				   WHERE user_vehicle_details.uId=$student_id
				   AND destination_route.session_id=".$_SESSION['current_session_id']."";
	$exe_amount_t=mysql_query($trans_amount);
	while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
		{
			$tranport_cost=$tranport_cost+$fetch_transport_cost[0];
		}
	//query get rent of hostel from room allocation --------
	$hostel_rent=0;
	$hostel_amount="SELECT rent 
	                FROM hostel_room_allocation
					WHERE sid=$student_id
					AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
	$exe_rent=mysql_query($hostel_amount);
	while($fetch_rent_cost=mysql_fetch_array($exe_rent))
		{
			$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
		}
	$trans_rent_cost=$hostel_rent+$tranport_cost;
	echo'
	
	<span>Student : '.$_SESSION['name'].'</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<h6>Class : '.$class_name.'</h6>
	<h5 align="center"> Current Session : '.$fetch_session['session_name'].'</h5>
	<table  class="table table-bordered table-striped" align="center">
	<thead>
	<tr>
	<th align="center" width="5%">S.No.</th>
	<th align="left" width="50%">Credential Type</th>
	<th align="left" width="45%">Amount</th>
	</tr>
	</thead>
	<tbody>';
	//start query to fetch credential type and amount
	//and calculate total credential amount
	$fetch_credentials_type="SELECT DISTINCT fee_credentials.type,fee_credentials.amount
							FROM fee_credentials
							INNER JOIN fee_details
							ON fee_details.fee_credential_id=fee_credentials.id
							WHERE fee_details.student_id=".$_SESSION['user_id']." 
							AND fee_credentials.session_id=".$current_session."
							AND fee_details.fee_generated_session_id=".$duid."";
	$execute_credentials_type=mysql_query($fetch_credentials_type);
	while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
		{
			$camount=$camount+$fetch_credentials['amount'];
			//first latter of credential type convert small to capital
			$c=str_split($fetch_credentials['type']);
			$upper=strtoupper($c[0]);
			$count=count($c);
			echo '	<tr>
			<td align="center">'.$serial_no++.'</td>
			<td>';
			echo $upper;
			for($i=1;$i<$count;$i++)
				{
					echo $c[$i];  
				}
			echo '   <td>'.$fetch_credentials['amount'].'</td></tr>';  
		}
		 
	//calculate credentials with hostel fee and teransport fee
	$credential_amount=$camount+$trans_rent_cost;
	echo '<tr>
	<td align="center">'.$serial_no++.'</td>
	<td>Transport Fee</td>
	<td>'.$tranport_cost.'</td></tr>';
	echo '<tr>
	<td align="center">'.$serial_no++.'</td>
	<td>Hostel Fee</td>
	<td>'.$hostel_rent.'</td></tr>
	';
	echo ' <table  class="table table-bordered table-striped">
	<thead>
	<tr>
	<th align="center" width="5%">S.No.</th>
	<th align="left" width="50%">Discount Type</th>
	<th align="left" width="45%">Amount</th>
	</tr>
	</thead>
	<tbody>';
	//query to fetch discoun type of student and amount
	// and calculate total discount amount
	$serial_no=1;
	$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
						  FROM fee_discount_credentials
						  INNER JOIN fee_details
						  ON fee_details.fee_discount_id=fee_discount_credentials.id
						  WHERE fee_details.student_id=".$_SESSION['user_id']."
						  AND fee_discount_credentials.session_id=".$current_session."
						  AND fee_details.fee_generated_session_id=".$duid."";
	$execute_discount_type=mysql_query(  $fetch_discount_query);
	while($fetch_discount=mysql_fetch_array($execute_discount_type))
		{ 
			$total_discount=$total_discount+$fetch_discount['amount'];
			//first latter of discount type convert small to capital
			$d=str_split($fetch_discount['type']);
			$upper=strtoupper($d[0]);
			$c=count($d);
			echo '<tr>
			<td align="center">'.$serial_no++.'</td>
			<td>';
			echo $upper;
			for($i=1;$i<$c;$i++)
				{
					echo $d[$i];  
				}
			echo '  <td>'.$fetch_discount['amount'].'</td>
			</tr>';
		}
	echo '
	<table  class="table table-bordered table-striped"  >
	<thead>
	</thead>
	<tbody>';
	//write query to get id and type from fee generated session id 
	$query_fee_ge_session_id="SELECT DISTINCT `Id`, `type`
							  FROM `fee_generated_sessions`
							  WHERE session_id=".$_SESSION['current_session_id']."";
	$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
	while($fetch_fee=mysql_fetch_array($execute_fee_ge_se_id))
		{
			$month=$fetch_fee['type'];
		}
	$tfamount=$credential_amount-$total_discount;
	//calculate fee Monthly ,quartrely.........
	/*	   $m=1;
	if($month=="Monthly")
		{
			$m=1; 
		}
	else if($month=="Quarterly")
		{
			$m=3; 
		}
	else
		{
			$m=12;  
		}
	$total_fee_amount=$tfamount*$m;*/
	echo '  <tr>
	<td width="55%"><b style="color:green">Total Credentials Amount</b></td><td><b style="color:green">'.$credential_amount.'
	</b></td>
	</tr>
	<tr>
	<td><b style="color:green">Total Discount</b></td><td><b style="color:green">'.$total_discount.'</b></td>
	</tr>
	<tr>
	<td><h4 style="color:red">Total Fee</h4></td><td><h4 style="color:red">'.$tfamount.'</h4></td>
	</tr>
	</tbody>
	</table></tbody>
	</table>
	<a href="students_fee_student_generated.php" class="uibutton icon answer ">Back</a>
	';
	echo '	   
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}                                            //    ========================

//function to show the student fee details if  paid
function students_fee_paid_details()
{
	//Author By: Anil Kumar *
	
	$duid=$_GET['id'];
	$student_id = $_SESSION['user_id'];
	$current_session=$_SESSION['current_session_id'];
	$ge_id=$_GET['id'];
		//write query to get fee generated session id 
	$query_fee_ge_session_id="SELECT DISTINCT `duration`, `last_date`
							FROM `fee_generated_sessions`
							WHERE Id=".$ge_id."
							";
	$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
	$fetch_fee=mysql_fetch_array($execute_fee_ge_se_id);
	//Query to get the class of the user student
	$query_class_student = "SELECT class_index.class_name
							
							FROM class_index
							
							INNER JOIN class
							ON class.classId = class_index.cId
							
							WHERE class.sId = $student_id
							";
	$execute_class_student = mysql_query($query_class_student);
	$class_student = mysql_fetch_array($execute_class_student);
	//write query to get the session id from  the fee generated session
	$fetch_session_name="SELECT `session_name`
						FROM `session_table` 
						WHERE `sId`=".$current_session."";
	$execute_session_name=mysql_query($fetch_session_name);
	$fetch_session=mysql_fetch_array($execute_session_name);
	$session_name=$current_session['session_name'];
	$class_name = $class_student[0];
	$total_discount=0;
	$finee="fine";
	$fine=0;
	$camount=0;	
	$serial_no=1;
	//get name of the school
	$get_name_school=
					 "SELECT `name_school`
					 FROM `school_names`
					 WHERE `Id`= 3";
	$exe_name_school=mysql_query($get_name_school);
	$fetch_school_name=mysql_fetch_array($exe_name_school);
	$school_name=$fetch_school_name['name_school'];
	//QUERY get cost of transport from destination_rouite------
	$tranport_cost=0;
	$trans_amount="SELECT cost
	               FROM destination_route
				   
				   INNER JOIN user_vehicle_details
				   ON user_vehicle_details.did=destination_route.did
				   
				   WHERE user_vehicle_details.uId=$student_id
				   AND destination_route.session_id=".$_SESSION['current_session_id']."";
	$exe_amount_t=mysql_query($trans_amount);
	while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
		{
			$tranport_cost=$tranport_cost+$fetch_transport_cost[0];
		}
	//query get rent of hostel from room allocation --------
	$hostel_rent=0;
	$hostel_amount="SELECT rent 
	                FROM hostel_room_allocation
					WHERE sid=$student_id
					AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
	$exe_rent=mysql_query($hostel_amount);
	while($fetch_rent_cost=mysql_fetch_array($exe_rent))
		{
			$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
		}
	$trans_rent_cost=$hostel_rent+$tranport_cost;
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>FEE RECEIPT</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<div  id="c_'.$student_id.'">
	<h3 align="center" style="color:GREEN">'.$school_name.'</h3>
	<h5>Student : '.$_SESSION['name'].'</h5>
	<h6>Class : '.$class_name.'</h6>
	<h5 align="center"> Current Session : '.$fetch_session['session_name'].'</h5>
	<h5 align="center"> Duration : '.$fetch_fee['duration'].'</h5>
	<table  class="table table-bordered table-striped">';
	echo '     
	<thead>
	<tr>
	<th align="center" width="5%">S.No.</th>
	<th align="left" width="50%">Credential Type</th>
	<th align="left" width="45%">Amount</th>
	</tr>
	</thead>
	<tbody>';
	//start query to fetch credential type
	$fetch_credentials_type="SELECT DISTINCT fee_credentials.type,fee_credentials.amount
							FROM fee_credentials
							INNER JOIN fee_details
							ON fee_details.fee_credential_id=fee_credentials.id
							WHERE student_id=".$_SESSION['user_id']."
							AND fee_credentials.session_id=".$current_session."
							AND fee_details.fee_generated_session_id=".$duid."";
	$execute_credentials_type=mysql_query($fetch_credentials_type);
	while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
		{
			$camount=$camount+$fetch_credentials['amount'];
			//first latter of credential type convert small to capital
			$c=str_split($fetch_credentials['type']);
			$upper=strtoupper($c[0]);
			$count=count($c);
			echo '	<tr>
			<td align="center">'.$serial_no++.'</td>
			<td>';
			echo $upper;
			for($i=1;$i<$count;$i++)
				{
					echo $c[$i];
				}
			echo ' <td>'.$fetch_credentials['amount'].'</td></tr>';  
		}
	echo '<tr>
	<td align="center">'.$serial_no++.'</td>
	<td>Transport Fee</td>
	<td>'.$tranport_cost.'</td></tr>';
	echo '<tr>
	<td align="center">'.$serial_no++.'</td>
	<td>Hostel Fee</td>
	<td>'.$hostel_rent.'</td></tr>
	';
	echo ' <table  class="table table-bordered table-striped">
	<thead>
	<tr>
	<th align="center" width="5%">S.No.</th>
	<th align="left" width="50%">Discount Type</th>
	<th align="left" width="45%">Amount</th>
	</tr>
	</thead>
	<tbody>';
	//query to fetch discountype of student
	$serial_no=1;
	$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
							FROM fee_discount_credentials
							INNER JOIN fee_details
							ON fee_details.fee_discount_id=fee_discount_credentials.id
							WHERE fee_details.student_id=".$_SESSION['user_id']."
							AND fee_discount_credentials.session_id=".$current_session."
							AND fee_details.fee_generated_session_id=".$duid."";
	$execute_discount_type=mysql_query(  $fetch_discount_query);
	while($fetch_discount=mysql_fetch_array($execute_discount_type))
		{ 
			$total_discount=$total_discount+$fetch_discount['amount'];
			//first latter of discount type convert small to capital
			$d=str_split($fetch_discount['type']);
			
			$upper=strtoupper($d[0]);
			$c=count($d);
			echo '<tr>
			<td align="center">'.$serial_no++.'</td>
			<td>
			';
			echo $upper;
			for($i=2;$i<$c;$i++)
				{
					echo $d[$i];
				}
			echo '
			</td>
			
			<td>'.$fetch_discount['amount'].'</td>
			
			</tr>';
		}
	echo '
	<table  class="table table-bordered table-striped"  >
	<thead>
	</thead>
	<tbody>';
	//write query to fetch fine amount
	$query_fine="SELECT `amount` 
				FROM `fee_credentials`
				WHERE  `type`='".$finee."'";
	$execute_fine=mysql_query($query_fine);
	$fetch_fine=mysql_fetch_array($execute_fine);
	$fine=$fetch_fine['amount'];
	//write query to get fee generated session id 
	$query_fee_ge_session_id="SELECT DISTINCT `type`, `last_date`
							  FROM `fee_generated_sessions`
							  WHERE Id=".$ge_id."
							   ";
	$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
	$fetch_fee=mysql_fetch_array($execute_fee_ge_se_id);
	$month=$fetch_fee['type'];
	$due_date=$fetch_fee['last_date'];
	//write query to get the paid on from the fee details
	$fetch_query_paid="SELECT DISTINCT `paid_on`
					  FROM `fee_details`
					  WHERE `fee_generated_session_id`=".$ge_id."
					  AND `fee_details`.`student_id`=".$_SESSION['user_id']."";
	$execute_paid=mysql_query($fetch_query_paid);
	$fetch_paid=mysql_fetch_array($execute_paid);
	$paid_date=$fetch_paid['paid_on'];	
	//check condition due date < paid on date or not
	if($due_date<$paid_date)
		{
			$fine_days=$paid_date-$due_date;
				if($fine_days<=30)
				{
					$calculate_fine=100;
				}
				else if($fine_days<=60)
				{
					$calculate_fine=200;
				}
				else
				{
					$calculate_fine=500;
				}
		}
	else
		{
			$calculate_fine=0;  
		}
	//calculate fee amount according to duration
	$m=1;
	if($month=="Monthly")
		{
			$m=1; 
		}
	else if($month=="Quarterly")
		{
			$m=3; 
		}
	else if($month=="Half-Yearly")
		{
			$m=6;  
		}	
	else if($month=="Yearly")
		{
			$m=12;  
		}
	else
		{
			$m=1;  
		}
	$tfamount=$camount-$total_discount+$trans_rent_cost;
	$total_duration_amount= $tfamount*$m;
	$total_fine_amount=$total_duration_amount+$calculate_fine;
	$creden_cost=$camount+$trans_rent_cost;
	$fee=$creden_cost-$total_discount;
	echo '  <tr>
	<td width="55%"><b style="color:green">Total Credentials Amount</b></td><td><b style="color:green">'.$creden_cost.'</b></td>
	</tr>
	<tr>
	<td><b style="color:green">Total Discount</b></td><td><b style="color:green">'.$total_discount.'</b></td>
	</tr>
	<tr>
	<td><b style="color:green">Fee</b></td><td><b style="color:green">'.$fee.'</b></td>
	</tr>
	<tr>
	<td><b style="color:green">Late Fine</b></td><td><b style="color:green">'. $calculate_fine.'</b></td>
	</tr>
	<tr>
	<td><h4 style="color:red">Total Fee</h4></td><td><h4 style="color:red">'.$total_fine_amount.'</h4></td>
	</tr>
	</tbody>
	</table></tbody>
	</table>';
	//write query to get fee generated session id 
	$query_fee_ge_session_id="SELECT DISTINCT `duration`, `last_date`
							FROM `fee_generated_sessions`
							WHERE Id=".$ge_id."
							";
	$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
	$fetch_fee=mysql_fetch_array($execute_fee_ge_se_id);
	//query to get date on the id of date
	$get_date_of_id=
					"SELECT DISTINCT date
					FROM dates_d
					WHERE date_id = ".$fetch_fee['last_date']."";
	$exe_date=mysql_query($get_date_of_id);
	$fetch_date=mysql_fetch_array($exe_date);
	$due_date = $fetch_date[0];
	$last_due_dates=date('d-m-Y',strtotime($due_date));
	//query to get paid on date
	$paidondate="SELECT DISTINCT `paid_on`
			   FROM `fee_details`
			   WHERE `student_id`=".$_SESSION['user_id']."
			   AND `fee_generated_session_id`=".$ge_id;
	$execute_paidondate=mysql_query($paidondate);
	$fetch_paidondate=mysql_fetch_array($execute_paidondate);
	//query to get date on the id of date
	
	$get_date_of_id=
					"SELECT DISTINCT date
					FROM dates_d
					WHERE date_id = ".$fetch_paidondate['paid_on']."";
	$exe_date=mysql_query($get_date_of_id);
	$fetch_date=mysql_fetch_array($exe_date);
	$payon_date = $fetch_date[0];
	$dates=date('d-m-Y',strtotime($payon_date));
	echo ' Last date of paid fee <b style="color:blue" >'.$fetch_fee['duration'].'</b> of session <b style="color:blue">
	'.$fetch_session['session_name'].'</b> is <b style="color:blue">'.$last_due_dates.'</b> and paid on date is<b style="color:blue">
	'.$dates.'</b><br><br>  ';          
	echo ' DATE<br><br> <br><br> 
	SIGNATURE	 
	</div>
	</div><!--  end widget-content -->';
	echo '		
	<div align="right" style="cursor:pointer"><img src="images/print.png" onclick="print_this('.$student_id.')"
	style="width:40px" style="height:30px" /></div>
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}


//sagun sir code`s
//function to total fee details
/*function  fee_total_details()
{	
echo  '<div class="row-fluid">                           
<div class="span12  widget clearfix">
<div class="widget-header">
<span>Total Fee Details</span>
</div><!-- End widget-header -->		  
<div class="widget-content"><br />
<div class="row-fluid"> 			
';	
$time_offset ="525"; // Change this to your time zone
$time_a = ($time_offset * 120);
$today = date("jS F Y");	 
echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
$disp='';
$q="SELECT `cId`,`class_name` FROM class_index ORDER BY class, section ASC";
$q_res=mysql_query($q);
echo '<h2 align="center">';
echo' <div class="span4"><h3 style="color:">Kindly select class</h3></div>';
echo '</h2>';
echo "<br>";
echo "<br>";
echo "<br>";
echo '<ol class="rounded-list">';
while($res=mysql_fetch_array($q_res))//looping for getting  class id
{	
echo '<tr class="row-fluid" id="added_rows">
<td><div class="row-fluid">
<div class="span6">							                                                              
<li><a href="fee_manager_total_calculation.php?class_id='.$res['cId'].'&&session_id='.$_GET['session'].'">'.$res[1].'Class</a>                                                                    	                                 
</div>
</div><!-- end row-fluid  -->
</td>
</tr>';	 	 
}
echo'                                       
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';										

}*/
//function to show fee paid details
function fee_paid_details_accounts()
{ 
    //Author By: Sagun sir
	echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span> Fee Reciept print</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"> Kindly Enter  <span class="netip"><a  class="red" > Student\'s Admission Number  </a></span>
	</div>
	';		  
	echo'       
	<div class="section">
	<label>Admission No.</label>   
	<div> 
	<input type="text" name="admission_no" id="admission_no"  class=" medium" onkeyup="student_fee_detail(this.value)" />
	</div>
	</div>
	<div id="myfeeDiv">
	</div>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';	
}

//sagun's functions
//function to add fee credaentials
function add_fee_credentials()
{	
    //Author By: Sagun sir	
	echo '
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ADD Fee Credentials </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"> Kindly add   <span class="netip"><a  class="red" > basic fee credentials  </a></span>
	</div>
	';		  
	if (isset($_GET['added']))
		{
			echo '<h4 align="center"><span style="color:green">FEE CREDENTIAL ADDED SUCCESSFULLY!</h4></span>';
		}
	else if	(isset($_GET['error']))
		{
			echo '<h4 align="center"><span style="color:red">ERROR!</span></h4>';
		}	
	else if (isset($_GET['deleted']))
		{
			echo '<h4 align="center"><span style="color:green">FEE CREDENTIAL DELETED SUCCESSFULLY!</h4></span>';
		}
	else if (isset($_GET['editted']))
		{
			echo '<h4 align="center"><span style="color:green">FEE CREDENTIAL EDITED SUCCESSFULLY!</h4></span>';
		}
	echo'      
	<form id="demo"  action="fee/fee_credentials.php" method="post"> 
	<div class="section">
	<label>Credential Type</label>   
	<div> 
	<input type="text" name="type" class=" medium" required="required"/>
	</div>
	</div>
	<div class="section numericonly">
	<label> Amount (Rupees) <small> In Number Only</small></label>   
	<div> 
	<input name="amount" type="text" class=" medium" required="required">
	</div>
	</div>
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form" >
	<a class="btn" onclick="ResetForm()" title="Reset  Form">Clear Form</a>
	
	</div>
	</div>	 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
	$sno=0;
	echo' 
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>View Fee Credentials </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<table class="table table-bordered table-striped" id="dataTable">
	<thead>';
	echo'   
	<tr>
	<th width="5%">S.No.</th>
	<th width="28%">TYPE</th>
	<th width="28%">AMOUNT(RUPEES)</th>
	<th width="28%">SESSION</th>
	<th width="9%">ACTIONS</th>
	</tr>
	</thead>
	<tbody align="center">';	  
	$query = "SELECT * 
			FROM fee_credentials
			ORDER BY type ASC
			";
			$execute = mysql_query($query) ;
	while($fetch = mysql_fetch_array($execute))
		{            
			++$sno;    
			$query_session="SELECT *
							FROM session_table
							WHERE sId=".$fetch['session_id']
							;
			$execute_session=mysql_query($query_session);
			while($fetch_session=mysql_fetch_array($execute_session))						       
				{  
					echo'     <tr class="odd gradeX">
					<td>'.$sno.'</td>
					<td>'.$fetch['type'].'</td>
					<td>'.$fetch['amount'].'</td>
					<td>'.$fetch_session['session_name'].'</td>
					<td class=" ">
					<span class="tip"><a href="accounts_edit_fee_credentials.php?id='.$fetch['Id'].'" original-title="Edit">
					<img src="images/icon/icon_edit.png" ></a></span> 
					<span class="tip"><a href="fee/delete_fee_credentials.php?id='.$fetch['Id'].'" >
					<img src="images/icon/icon_delete.png" >
					</a></span> 
					</td>
					</tr>';
				}		
		}
	
	echo' </tbody>
	<a class="btn btn-success" href="accounts_fee_credentials_class_select.php">View Fee Credentials Allocation</a>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}

//edit fee credentials details in this function
function edit_fee_credentials()
{
	//Author By: Sagun sir	
	echo '
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Edit Fee Credentials </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"> EDIT   <span class="netip"><a  class="red" > basic fee credentials  </a></span>
	</div> ';
	$sql="SELECT *
		  FROM fee_credentials
		  WHERE id = ".$_GET['id']."
		  ";
	$execute=mysql_query($sql);
	$fetch=mysql_fetch_array($execute);	 			  
	echo'							       
	<form id="demo"  action="fee/edit_fee_credentials.php?id='.$_GET['id'].'" method="post"> 
	<div class="section">
	<label>Credential Type</label>   
	<div> 
	<input type="text" name="type" value="'.$fetch['type'].'" class=" medium" required="required"/>
	</div>
	</div>
	<div class="section numericonly">
	<label> Amount (Rupees) <small> In Number Only</small></label>   
	<div> 
	<input name="amount" type="text" value="'.$fetch['amount'].'" class=" medium" required="required">
	</div>
	</div>
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form" >
	<a class="btn" onclick="ResetForm()" title="Reset Form">Clear Form</a>
	</div>
	</div>	 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';	
}

//function to add fee discount credentials
function add_fee_discount_credentials()
{	//Author By: Sagun sir		
    echo '
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Add Fee Discount Credentials </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"> Kindly add   <span class="netip"><a  class="red" >fee  discount credentials  </a></span>
	</div>
	';		  
	if (isset($_GET['added']))
		{
			echo '<h4 align="center"><span style="color:green">FEE DISCOUNT CREDENTIAL ADDED SUCCESSFULLY!</h4></span>';
		}
	else if	(isset($_GET['error']))
		{
			echo '<h4 align="center"><span style="color:red">ERROR!</span></h4>';
		}
	else if (isset($_GET['deleted']))
		{
			echo '<h4 align="center"><span style="color:green">FEE DISCOUNT CREDENTIAL DELETED SUCCESSFULLY!</h4></span>';
		}	
	else if (isset($_GET['editted']))
		{
			echo '<h4 align="center"><span style="color:green">FEE DISCOUNT CREDENTIAL EDITED SUCCESSFULLY!</h4></span>';
		}										
	echo'        
	<form id="demo"  action="fee/fee_discount_credentials.php" method="post"> 
	<div class="section">
	<label>Discount Type</label>   
	<div> 
	<input type="text" name="type" class=" medium" required="required" autofocus />
	</div>
	</div>
	<div class="section numericonly">
	<label> Amount (Rs. / %) <small> In Number Only</small></label>   
	<div> 
	<input name="amount" type="text" class="medium" required="required">
        <input type="radio" name="radio" id="0" value="0"  checked="checked"/>In Rupees
        <input type="radio" name="radio" id="1" value="1" >In Percentage
	</div>
	</div>
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form">
	<a class="btn" onclick="ResetForm()" title="Reset  Form">Clear Form</a>
	</div>
	</div>	 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
	$sno=0;
	echo' 
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>View Fee Discount Credentials </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<table class="table table-bordered table-striped" id="dataTable">
	<thead>';
	echo'    <tr>
	<th width="5%">S.No.</th>
	<th width="28%">TYPE</th>
	<th width="28%">DISCOUNT</th>
	<th width="28%">SESSION</th>
	<th width="9%">ACTIONS</th>
	</tr>
	</thead>
	<tbody align="center">';
	$query = "SELECT * 
			  FROM fee_discount_credentials
			  ORDER BY type ASC
			  ";
	$execute = mysql_query($query) ;
	while($fetch = mysql_fetch_array($execute))
		{            
			++$sno;  
			$query_session="SELECT *
			FROM session_table
			WHERE sId=".$fetch['session_id']
			;
			$execute_session=mysql_query($query_session);
			while($fetch_session=mysql_fetch_array($execute_session))						       
				{          
					echo'     <tr class="odd gradeX">
					<td>'.$sno.'</td>
					<td>'.$fetch['type'].'</td>';
                                        if($fetch[4]==0)
                                        {
                                            echo '<td>'.$fetch['amount'].' in Rs.</td>';
                                        }
                                        else if($fetch[4]==1)
                                        {
                                            echo '<td>'.$fetch['amount'].' in %</td>';
                                        }
					echo '<td>'.$fetch_session['session_name'].'</td>
					<td class=" ">
					<span class="tip"><a href="accounts_edit_fee_discount_credentials.php?id=
					'.$fetch['Id'].'" original-title="Edit">
					<img src="images/icon/icon_edit.png"></a></span> 
					
					<span class="tip"><a href="fee/delete_fee_discount_credentials.php?id='.$fetch['Id'].'" >
					<img src="images/icon/icon_delete.png" >
					</a></span>  
					</td>
					</tr>';
				}
		}
	echo'
    </tbody>
	<a class="btn btn-success" href="accounts_fee_credentials_class_select.php">View Fee Discounts Allocation</a>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}

//fee discount s credentials  edit and update in this function
function edit_fee_discount_credentials()
{
	//Author By: Sagun sir	
	echo '
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Edit Fee Discount Credentials </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"> EDIT   <span class="netip"><a  class="red" >  fee discount credentials  </a></span>
	</div> ';
	$sql="SELECT *
		  FROM fee_discount_credentials
		  WHERE id = ".$_GET['id']."
		  ";
	$execute=mysql_query($sql);
	$fetch=mysql_fetch_array($execute);	 			  
	echo'							       
	<form id="demo"  action="fee/edit_fee_discount_credentials.php?id='.$_GET['id'].'" method="post"> 
	<div class="section">
	<label>Credential Type</label>   
	<div> 
	<input type="text" name="type" value="'.$fetch['type'].'" class=" medium" required="required"/>
	</div>
	</div>
	<div class="section numericonly">
	<label> Amount (Rupees) <small> In Number Only</small></label>   
	<div> 
	<input name="amount" type="text" value="'.$fetch['amount'].'" class=" medium" required="required">
	</div>
	</div>
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form" >
	<a class="btn" onclick="ResetForm()" title="Reset  Form">Clear Form</a>
	</div>
	</div>	 		 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';	
}

//fee generation details add 
function add_fee_generation_details()
{
	//Author By: Sagun sir	
    echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span> Fee Generation Details </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly add   <span class="netip"><a  class="red" > fee generation 
	details  </a></span>
	</div>
	';	
/*	if (isset($_GET['select']))
		{
			echo '<h4 align="center"><span style="color:red">FILL ALL FIELDS PROPERLY!</h4></span>';
		}*/	  
	if (isset($_GET['added']))
		{
			echo '<h4 align="center"><span style="color:green">FEE GENERATION DETAILS ADDED SUCCESSFULLY!</h4></span>';
		}
	else if	(isset($_GET['error']))
		{
			echo "<h4 align=\"center\">ERROR!</h4>";
		}	
	echo'       
	<form id="demo"  action="fee/fee_generation_details.php" method="post"> 
	
	<div class="section">
	<label>Duration<small>Monthwise</small></label>   
	<div> 
	<input type="text" name="duration" class=" small" required="required"/>
	</div>
	</div>';
	/*		<div class="section">
	<label>Last DATE<small>Fee Submission</small></label>   
	<div><input type="text" id="datepick" class="datepicker hasDatepicker"  name="last_date">
	</div>
	</div>*/
	echo '		 <div class="section">
	<label>Last DATE<small>Fee Submission</small></label>    
	<div><input type="text"  id="datepick" class="datepicker"  name="last_date" required="required" /></div>
	</div>';
	echo '		
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form">
	<a class="btn" onclick="ResetForm()" title="Reset  Form">Clear Form</a>
	</div>
	</div>	 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
	$sno=0;
	echo' 
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>View Fee Discount Credentials </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	if (isset($_GET['deleted']))
		{
			echo '<h4 align="center"><span style="color:green">FEE GENERATION DETAIL DELETED SUCCESSFULLY!</h4></span>';
		}	
	else if (isset($_GET['editted']))
		{
			echo '<h4 align="center"><span style="color:green">FEE GENERATION DETAIL EDITED SUCCESSFULLY!</h4></span>';
		}	
	else if (isset($_GET['error']))
		{
			echo '<h4 align="center"><span style="color:red">ERROR!</h4></span>';
		}						
	echo'         
	<table class="table table-bordered table-striped" id="dataTable">
	<thead>';
	echo'   
	<tr>
	<th width="5%">S.No.</th>
	<th width="22%">TYPE</th>
	<th width="22%">DURATION</th>
	<th width="22%">LAST DATE<br/><small>(Fee submission)</small></th>
	<th width="22%">SESSION</th>
	<th width="7%">ACTION</th>
	</tr>
	</thead>
	<tbody align="center">';
	$query = "SELECT * , session_name
			FROM fee_generated_sessions
			INNER JOIN session_table
			ON fee_generated_sessions.session_id = session_table.sId
			ORDER BY last_date ASC
			";
	$execute = mysql_query($query) ;
	while($fetch = mysql_fetch_array($execute))
		{            
			++$sno;           
			echo'     <tr class="odd gradeX">
			<td>'.$sno.'</td>
			<td>'.$fetch['type'].'</td>
			<td>'.$fetch['duration'].'</td>';
			$sql_date="SELECT date
			FROM dates_d
			WHERE date_id = ".$fetch['last_date']."
			";
			$execute_date = mysql_query($sql_date);
			$fetch_date = mysql_fetch_array($execute_date);	
			
		
			echo'		
			<td>'.$fetch_date['date'].'</td>
			<td>'.$fetch['session_name'].'</td>
			<td class=" ">
			<span class="tip"><a href="accounts_edit_fee_generation_details.php?id='.$fetch['Id'].'" original-title="Edit">
			<img src="images/icon/icon_edit.png"></a></span> ';
//		echo '	<span class="tip"><a href="fee/delete_fee_generation_details.php?id='.$fetch['Id'].'" original-title="Delete">
//			<img src="images/icon/icon_delete.png"></a></span> ';
		echo	'</td>
			</tr>';
		}
	echo' </tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}
	
	
	
/*function view_fee_generation_details() 
{
	//Author By: Sagun sir	
	$sno=0;
	echo' 
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>View Fee Discount Credentials </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	if (isset($_GET['deleted']))
	{
	echo '<h4 align="center"><span style="color:green">FEE GENERATION DETAIL DELETED SUCCESSFULLY!</h4></span>';
	}	
	else if (isset($_GET['editted']))
	{
	echo '<h4 align="center"><span style="color:green">FEE GENERATION DETAIL EDITED SUCCESSFULLY!</h4></span>';
	}	
	else if (isset($_GET['error']))
	{
	echo '<h4 align="center"><span style="color:red">ERROR!</h4></span>';
	}						
	echo'         <table class="table table-bordered table-striped" id="dataTable">
	<thead>';
	echo'    <tr>
	<th width="5%">S.No.</th>
	<th width="22%">TYPE</th>
	<th width="22%">DURATION</th>
	<th width="22%">LAST DATE<br/><small>(Fee submission)</small></th>
	<th width="22%">SESSION</th>
	<th width="7%">ACTION</th>
	</tr>
	</thead>
	<tbody align="center">';
	$query = "SELECT * , session_name
	FROM fee_generated_sessions
	INNER JOIN session_table
	ON fee_generated_sessions.session_id = session_table.sId
	ORDER BY last_date ASC
	";
	$execute = mysql_query($query) ;
	while($fetch = mysql_fetch_array($execute))
	{            
	++$sno;           
	echo'     <tr class="odd gradeX">
	<td>'.$sno.'</td>
	<td>'.$fetch['type'].'</td>
	<td>'.$fetch['duration'].'</td>';
	$sql_date="SELECT date
	FROM dates_d
	WHERE date_id = ".$fetch['last_date']."
	";
	$execute_date = mysql_query($sql_date);
	$fetch_date = mysql_fetch_array($execute_date);		  
	echo'		<td>'.$fetch_date['date'].'</td>
	<td>'.$fetch['session_name'].'</td>
	<td class=" ">
	<span class="tip"><a href="accounts_edit_fee_generation_details.php?id='.$fetch['Id'].'" original-title="Edit">
	<img src="images/icon/icon_edit.png"></a></span> 
	<span class="tip"><a href="#" onclick="deleted_view('.$fetch['Id'].');" original-title="Delete">
	<img src="images/icon/icon_delete.png"></a></span> 
	</td>
	</tr>';
	}
	echo' </tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
	}*/

function edit_fee_generation_details()
{
	//Author By: Sagun sir	
	echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span> Fee Generation Details </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly add   <span class="netip"><a  class="red" > fee generation 
	details  </a></span>
	</div>
	';		  
	$sql="SELECT *
		FROM fee_generated_sessions
		WHERE id = ".$_GET['id']."
		";
	$execute=mysql_query($sql);
	$fetch=mysql_fetch_array($execute);	 	
	$date_id = $fetch['last_date'];
	$sql_date="SELECT date
				FROM dates_d
				WHERE date_id = ".$fetch['last_date']."
				";
	$execute_date=mysql_query($sql_date);
	$fetch_date=mysql_fetch_array($execute_date);	
	echo'        
	<form id="demo"  action="fee/edit_fee_generation_details.php?id='.$_GET['id'].'" method="post"> 
	<div class="section">
	<label>Type</label>   
	<div> ';
	/*<input type="text" name="type" value="'.$fetch['type'].'" class=" medium" />*/
	echo '<select  name="type"  class="chzn-select medium" data-placeholder="'.$fetch['type'].'">';
	echo '<option value="'.$fetch['type'].'">'.$fetch['type'].'</option>';
	if($fetch['type']!="Monthly")
	{
	   echo '<option value="Monthly">Monthly</option>';
	}
	if($fetch['type']!="Quarterly")
	{
		echo '<option value="Quarterly">Quarterly</option>';
	}
	if($fetch['type']!="Half-Yearly")
	{
		echo '<option value="Half-Yearly">Half-Yearly</option>';
	}
	if($fetch['type']!="Yearly")
	{
	   echo '<option value="Yearly">Yearly</option>';
	}
	echo '</select>
	</div>
	</div>
	<div class="section">
	<label>Duration<small>Monthwise</small></label>   
	<div> 
	<input type="text" name="duration" value="'.$fetch['duration'].'" class=" medium" />
	</div>
	</div>
	<div class="section">
	<label>Last Date<small>Fee Submission</small></label>   
	<div><input type="text" id="datepick" value="'.$fetch_date['date'].'" class="datepicker"  name="last_date" />
	</div>
	</div>
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form">
	<a class="btn" onclick="ResetForm()" title="Reset  Form">Clear Form</a>
	</div>
	</div>	 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}

//function to show all classes and accountant select class and view credentials allocation
function fee_credentials_class_select()
{
	//Author By :Anil Kumar *
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Not Pay Students</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';
	//query to get all class
	$query_class="SELECT *
				 FROM class_index
				  ";
	$execute_class=mysql_query($query_class);
	echo '
	<div class="section last">
	<label>SELECT CLASS</label>
	<div > <ol class="rounded-list">';				  
	while($fetch_class=mysql_fetch_array($execute_class))
	{ 
		echo' <li><a href="accounts_fee_credentials_allocation.php?cId='.$fetch_class['cId'].'
		">'.$fetch_class['class_name'].'</a></li> ';	
	}
	echo'
	</ol>
	</div>
	</div>
	';
	echo '
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';		
}

//function to show all classes and accountant select class and view fee discount allocation
function fee_discount_class_select()
{
	//Author By :Anil Kumar *
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Not Pay Students</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';

	//query to get all class
	$query_class="SELECT *
				 FROM class_index
				  ";
	$execute_class=mysql_query($query_class);
	echo '
	<div class="section last">
	<label>SELECT CLASS</label>
	<div > <ol class="rounded-list">';				  
	while($fetch_class=mysql_fetch_array($execute_class))
	{ 
		echo' <li><a href="accounts_fee_discount_allocation.php?cId='.$fetch_class['cId'].'
		">'.$fetch_class['class_name'].'</a></li> ';	
	}
	echo'
	</ol>
	</div>
	</div>
	';	
	echo '
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';		
}

//function to show fee credentials allocation
function fee_credentials_allocation()
{
	//Author By: Anil Kumar *
	//get class id from function fee credentials select class ()
	$cid=$_GET['cId'];
	$sno=0;
	echo' 
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>View Allocated Fee Credentials </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	//show message delete credentials successfully
	if(isset($_GET['deleted']))
		{
			echo '<h4 align="center"><span style="color:green">Credential Deleted Successfully!</span></h4>';
		}
	else if(isset($_GET['error']))
		{
			echo '<h4 align="center"><span style="color:green">ERROR!</span></h4>';
		}					
	echo'
    <table class="table table-bordered table-striped" id="dataTable">
	<thead>';
	echo'    <tr>
	<th width="5%">S.No.</th>
	<th width="22%">Admission Number</th>
	<th width="22%">Student Name</th>
	<th width="22%">Class</th>
	<th width="22%"><table>
	<tr align="center"><th width="80%">Allocated Credentials</th>
	<th width="80%">Amount</th>
	<th width="20%">Actions</th></tr></table>
	</th> 
	</tr>
	</thead>
	<tbody align="center">';
	//query to get students details of allocated credentials
    $query="SELECT DISTINCT student_user.Name, student_user.admission_no, student_user.sId,class_name
			FROM student_user
			INNER JOIN class
			ON student_user.sId = class.sId
			INNER JOIN class_index
			ON class.classId = class_index.cId
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			WHERE fee_details.fee_credential_id <> 0 
			AND class_index.cId=".$cid."";
	$execute=mysql_query($query);
	while($fetch = mysql_fetch_array($execute))
		{      
			++$sno;    
			$no=0;
			//query to get credentials details
			$query_discount="SELECT DISTINCT fee_credentials.type, fee_credentials.Id, fee_credentials.amount
							FROM fee_credentials
							INNER JOIN fee_details
							ON fee_credentials.Id = fee_details.fee_credential_id
							WHERE fee_details.student_id =".$fetch['sId']
							;
			$execute_discount=mysql_query($query_discount);
			
			echo'     <tr class="odd gradeX">
			<td>'.$sno.'</td>
			<td>'.$fetch['admission_no'].'</td>
			<td>'.$fetch['Name'].'</td>
			<td>'.$fetch['class_name'].'</td>
			<td width="20%"><table style="border-collapse:none">';
			while($fetch_discount=mysql_fetch_array($execute_discount))
				{		
					++$no;		
					echo'    <tr align="center">
					<td width="10%">'.$no.'</td>
					<td width="60%">'.$fetch_discount['type'].'</td> 
					<td width="60%">'.$fetch_discount['amount'].'</td>
					<td class=" " width="30%"> <span class="tip"><a href="fee/delete_credential_allocation.php?f_d_id=
					'.$fetch_discount['Id'].'&sId='.$fetch['sId'].'&cid='.$cid.'" original-title="Remove">
					<img src="images/icon/icon_delete.png">
					</a></span> 
					</td>
					</tr> ';
				}
			echo'					
			</table>
			</td>
			</tr>';
		}
	echo' </tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}
//function to show  fee discounts and allocate 
function fee_discount_allocation()
{ 
	//Author By: Anil Kumar *
	//get class id from function fee discount select class ()
	$cid=$_GET['cId'];
	$sno=0;
	echo' 
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>View Allocated Fee Discounts </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	if(isset($_GET['deleted']))
		{
			echo '<h4 align="center"><span style="color:green">Discount Deleted Successfully!</span></h4>';
		}
	else if(isset($_GET['error']))
		{
			echo '<h4 align="center"><span style="color:green">ERROR!</span></h4>';
		}					
	echo'   
	<table class="table table-bordered table-striped" id="dataTable">
	<thead>';
	echo'    
	<tr>
	<th width="5%">S.No.</th>
	<th width="22%">Admission Number</th>
	<th width="22%">Student Name</th>
	<th width="22%">Class</th>
	<th width="22%"><table>
	<tr align="center"><th width="80%">Allocated Discounts</th>
	<th width="80%">Amount</th>
	<th width="20%">Actions</th></tr></table>
	</th> 
	</tr>
	</thead>
	<tbody align="center">';
	//query to get students details of allocated discount
	$query="SELECT DISTINCT student_user.Name, student_user.admission_no, student_user.sId,class_name
			FROM student_user
			INNER JOIN class
			ON student_user.sId = class.sId
			INNER JOIN class_index
			ON class.classId = class_index.cId
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			WHERE fee_details.fee_discount_id <> 0 
			AND class_index.cId=".$cid."";
	$execute=mysql_query($query);
	while($fetch = mysql_fetch_array($execute))
		{      
			++$sno;    
			$no=0;
			//query to get fee discount details
			$query_discount="SELECT DISTINCT fee_discount_credentials.type, 
							fee_discount_credentials.Id, fee_discount_credentials.amount
							FROM fee_discount_credentials
							INNER JOIN fee_details
							ON fee_discount_credentials.Id = fee_details.fee_discount_id
							WHERE fee_details.student_id =".$fetch['sId']
							;
			$execute_discount=mysql_query($query_discount);
			echo'     <tr class="odd gradeX">
			<td>'.$sno.'</td>
			<td>'.$fetch['admission_no'].'</td>
			<td>'.$fetch['Name'].'</td>
			<td>'.$fetch['class_name'].'</td>
			<td width="20%"><table style="border-collapse:none">';
			while($fetch_discount=mysql_fetch_array($execute_discount))
				{		
					++$no;		
					echo'    <tr align="center">
					<td width="10%">'.$no.'</td>
					<td width="60%">'.$fetch_discount['type'].'</td> 
					<td width="60%">'.$fetch_discount['amount'].'</td>
					<td class=" " width="30%"> <span class="tip"><a href="fee/delete_discount_allocation.php?
					f_d_id='.$fetch_discount['Id'].'&sId='.$fetch['sId'].'&cid='.$cid.'" original-title="Remove">
					<img src="images/icon/icon_delete.png">
					</a></span> 
					</td>
					</tr> ';
				}
			echo'					
			</table>
			</td>
			</tr>';
		}
	echo' </tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}


//function show defalter according to level and class wise
function accounts_show_class()
{
	//Author By: Anil Kumar *		
	$cid=$_GET['cId'];	
	$duration=$_GET['duration'];
	$sno=0;
	$current_session=$_SESSION['current_session_id'];
	$finee="fine";
	$fine=0;
	$totalpaidfee =0;
	$totalduefee = 0;
	$totalduefee1 = 0;
	//write query to fetch fine amount
	$query_fine="SELECT `amount` 
				FROM `fee_credentials`
				WHERE  `type`='".$finee."'";
	$execute_fine=mysql_query($query_fine);
	$fetch_fine=mysql_fetch_array($execute_fine);
	$fine=$fetch_fine['amount'];		

	echo' 
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Not Paid Students List </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	if(isset($_GET['done']))
		{
			$sql_duration="SELECT duration
						  FROM fee_generated_sessions
						  WHERE Id = ".$_GET['f_g_sid'];
			$execute_duration=mysql_query($sql_duration);
			$fetch_duration=mysql_fetch_array($execute_duration);
			echo '<h4 align="center" style="color:green">Paid!! by '.$_GET['name'].' of '.$fetch_duration['duration'].'</h4>';
		}
		if(isset($_GET['clear']))
				{
					$sql_duration="SELECT duration
								  FROM fee_generated_sessions
								  WHERE Id = ".$_GET['f_g_sid'];
					$execute_duration=mysql_query($sql_duration);
					$fetch_duration=mysql_fetch_array($execute_duration);
					echo '
					<h4 align="center" style="color:green">Paid!! by '.$_GET['name'].' of '.$fetch_duration['duration'].'</h4>'                     ;
				}
	echo'                                 
    <table class="table table-bordered table-striped" id="dataTable">
	<thead>';
	echo'    
	<tr>
	<th width="5%">S.No.</th>
	<th  width="12%">Admission No.</th>
	<th width="12%">Name</th>
	<th width="12%">Class</th>
	<th width="60%"><table><thead>';
	/*echo '<th width="12%">Session</th>';*/
	echo '<th width="13%">Duration</th>
	<th width="14%">Tution Fee</th>
	
	<th width="15%">Late Fine</th>
	<th width="12%">Total Fee</th>
	<th width="12%">Last Date</th>
	<th width="12%" style="color:green">Paid Fee</th>
	<th width="12%" style="color:red">Due Fee</th>
	
	</thead></table></th>
	</tr>
	</thead>
	<tbody align="center">';
	//query to get students details (name admission no , class )
	if($duration==0)
	{
		 $query = "SELECT DISTINCT student_user.Name, student_user.admission_no, student_user.sId,class_index.class_name,
			fee_details.student_id
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			WHERE class_index.cId=".$cid."
		    
			AND (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
	        
			ORDER BY admission_no ASC
			";
	}
	elseif($duration<0)
	{
		//annually query
		if($duration==-2)
		{
             $query = "SELECT DISTINCT student_user.Name, student_user.admission_no, student_user.sId,class_index.class_name,
			fee_details.student_id
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			WHERE class_index.cId=".$cid."
		    
			AND (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
	        
			ORDER BY admission_no ASC
			";			
		}//bi- annually query
		elseif($duration==-1)
		{
		 $query = "SELECT DISTINCT student_user.Name, student_user.admission_no, 
		   student_user.sId,class_index.class_name,
			fee_details.student_id
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			WHERE class_index.cId=".$cid."
		    
			AND (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
	        AND (fee_details.fee_generated_session_id=1 OR fee_details.fee_generated_session_id=2)
			ORDER BY admission_no ASC
			";
		}
		//till date query
		elseif($duration==-3)
		{
			date_default_timezone_set('Asia/Kolkata');
		    $date=date('Y-m-d');
			$month_du=preg_split("/\-/",$date);
			 $month_duration=$month_du[1];
			//show defalter aprail to june
			if(($month_duration>3)&&($month_duration<7))
			{
				 $query = "SELECT DISTINCT student_user.Name, student_user.admission_no, 
		   student_user.sId,class_index.class_name,
			fee_details.student_id
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			WHERE class_index.cId=".$cid."
		    
			AND (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
	        AND fee_details.fee_generated_session_id=1 			
			ORDER BY admission_no ASC
			";
			}
			//aprail to september
			elseif(($month_duration>6)&&($month_duration<10))
			{
				 $query = "SELECT DISTINCT student_user.Name, student_user.admission_no, 
		   student_user.sId,class_index.class_name,
			fee_details.student_id
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			WHERE class_index.cId=".$cid."
		    
			AND (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
	        AND (fee_details.fee_generated_session_id=1 OR fee_details.fee_generated_session_id=2)		
			ORDER BY admission_no ASC
			";
			}
			//aprail to september to december
			elseif(($month_duration>9)&&($month_duration<13))
			{
				 $query = "SELECT DISTINCT student_user.Name, student_user.admission_no, 
		   student_user.sId,class_index.class_name,
			fee_details.student_id
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			WHERE class_index.cId=".$cid."
		    
			AND (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
	        AND (fee_details.fee_generated_session_id=1 OR fee_details.fee_generated_session_id=2 
			OR fee_details.fee_generated_session_id=3)		
			ORDER BY admission_no ASC
			";
			}
			//aprail to september to december to march
			elseif(($month_duration>0)&&($month_duration<4))
			{
				 $query = "SELECT DISTINCT student_user.Name, student_user.admission_no, 
		   student_user.sId,class_index.class_name,
			fee_details.student_id
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			WHERE class_index.cId=".$cid."
		    
			AND (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
	        AND (fee_details.fee_generated_session_id=1 OR fee_details.fee_generated_session_id=2 
			OR fee_details.fee_generated_session_id=3 OR fee_details.fee_generated_session_id=7)		
			ORDER BY admission_no ASC
			";
			}

			
		}
	}
	//duration wise
	else
	{
	    $query = "SELECT DISTINCT student_user.Name, student_user.admission_no, student_user.sId,class_index.class_name,
			fee_details.student_id
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			WHERE class_index.cId=".$cid."
		    
			AND (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
	        AND fee_details.fee_generated_session_id=".$duration."
			ORDER BY admission_no ASC
			";
	}
	$execute = mysql_query($query) ;
	while($fetch = mysql_fetch_array($execute))
		{          
			++$sno;   
			if($duration==0)
			{ 
			 $query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
						fee_generated_sessions.session_id, fee_generated_sessions.duration, fee_generated_sessions.last_date,
						fee_generated_sessions.type, fee_details.paid, fee_details.remarks, 
						fee_details.receiving_fee, fee_details.remaning_fee
						
						FROM fee_generated_sessions
						
						INNER JOIN fee_details 
						ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
						
						WHERE fee_details.student_id = ".$fetch['student_id']."
						AND fee_generated_sessions.Id = fee_details.fee_generated_session_id
						
						";
			}
			if($duration<0)
				{
					//annually query
					if($duration == -2)
					{
						 $query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
						fee_generated_sessions.session_id, fee_generated_sessions.duration, fee_generated_sessions.last_date,
						fee_generated_sessions.type, fee_details.paid, fee_details.remarks,
						fee_details.receiving_fee, fee_details.remaning_fee
						FROM fee_generated_sessions
						
						INNER JOIN fee_details 
						ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
						
						WHERE fee_details.student_id = ".$fetch['student_id']."
						
						";			
					}//bi- annually query
					elseif($duration==-1)
					{
					  $query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
						fee_generated_sessions.session_id, fee_generated_sessions.duration, fee_generated_sessions.last_date,
						fee_generated_sessions.type, fee_details.paid, fee_details.remarks,
						fee_details.receiving_fee, fee_details.remaning_fee
						FROM fee_generated_sessions
						
						INNER JOIN fee_details 
						ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
						
						WHERE fee_details.student_id = ".$fetch['student_id']."
						AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2)
						";
					}
					//till date query
					else if($duration==-3)
					{
						date_default_timezone_set('Asia/Kolkata');
						$date=date('Y-m-d');
						$month_du=preg_split("/\-/",$date);
						$month_duration=$month_du[1];
						//show defalter aprail to june
						if(($month_duration>3)&&($month_duration<7))
						{
						 $query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
						fee_generated_sessions.session_id, fee_generated_sessions.duration, fee_generated_sessions.last_date,
						fee_generated_sessions.type, fee_details.paid, fee_details.remarks,
						fee_details.receiving_fee, fee_details.remaning_fee
						FROM fee_generated_sessions
						
						INNER JOIN fee_details 
						ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
						
						WHERE fee_details.student_id = ".$fetch['student_id']."
						AND fee_generated_sessions.Id = 1 
						";
						}
						//aprail to september
						elseif(($month_duration>6)&&($month_duration<10))
						{
						 $query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
						fee_generated_sessions.session_id, fee_generated_sessions.duration, fee_generated_sessions.last_date,
						fee_generated_sessions.type, fee_details.paid, fee_details.remarks,
						fee_details.receiving_fee, fee_details.remaning_fee
						FROM fee_generated_sessions
						
						INNER JOIN fee_details 
						ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
						
						WHERE fee_details.student_id = ".$fetch['student_id']."
						AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2)
						";
						}
						//aprail to september to december
						elseif(($month_duration>9)&&($month_duration<13))
						{
						 $query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
						fee_generated_sessions.session_id, fee_generated_sessions.duration, fee_generated_sessions.last_date,
						fee_generated_sessions.type, fee_details.paid, fee_details.remarks,
						fee_details.receiving_fee, fee_details.remaning_fee
						FROM fee_generated_sessions
						
						INNER JOIN fee_details 
						ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
						
						WHERE fee_details.student_id = ".$fetch['student_id']."
						AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2 
						OR fee_generated_sessions.Id = 3)
						
						";
						}
						//aprail to september to december to march
						elseif(($month_duration>0)&&($month_duration<4))
						{
						 $query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
						fee_generated_sessions.session_id, fee_generated_sessions.duration, fee_generated_sessions.last_date,
						fee_generated_sessions.type, fee_details.paid, fee_details.remarks,
						fee_details.receiving_fee, fee_details.remaning_fee
						FROM fee_generated_sessions
						
						INNER JOIN fee_details 
						ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
						
						WHERE fee_details.student_id = ".$fetch['student_id']."
						AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2 
						OR fee_generated_sessions.Id = 3 OR fee_generated_sessions.Id = 7)
						
						";
						}
			
						
					}
				}
			else
			{
				$query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
						fee_generated_sessions.session_id, fee_generated_sessions.duration, fee_generated_sessions.last_date,
						fee_generated_sessions.type, fee_details.paid, fee_details.remarks,
						fee_details.receiving_fee, fee_details.remaning_fee
						FROM fee_generated_sessions
						
						INNER JOIN fee_details 
						ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
						
						WHERE fee_details.student_id = ".$fetch['student_id']."
						AND fee_generated_sessions.Id = ".$duration."
						
						";
			}
			$execute_dues=mysql_query($query_dues);
			echo'     <tr class="odd gradeX">
			<td width="5%">'.$sno.'</td>
			<td width="12%">'.$fetch['admission_no'].'</td>
			<td width="12%">'.$fetch['Name'].'</td>
			<td width="12%">'.$fetch['class_name'].'</td><td width="60%"><table width=100%>';
			while($fetch_dues=mysql_fetch_array($execute_dues))	          
				{
				if(($fetch_dues['paid']==1)||($fetch_dues['paid']==2))
				  {
					 continue; 
				  }
				  else
				  {  			
					echo'  <tr align="center">';
					$query_session="SELECT session_name
									FROM session_table
									WHERE sId = ".$fetch_dues['session_id']."
									";
					$execute_session=mysql_query($query_session);
					$fetch_session=mysql_fetch_array($execute_session);	
					//query to  show total fee  of  fee  defalter
					//write query to get fee generated session id 
					$query_fee_ge_session_id="SELECT `type`
											FROM `fee_generated_sessions`
											WHERE session_id=".$_SESSION['current_session_id']."
											AND Id=".$fetch_dues[0]."";
					$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
					$fetch_fee=mysql_fetch_array($execute_fee_ge_se_id);
					$month=$fetch_fee['type'];
					//QUERY get cost of transport from destination_rouite------
					$transport_cost=0;
					$trans_amount="SELECT cost
								   FROM destination_route
								   
								   INNER JOIN user_vehicle_details
								   ON user_vehicle_details.did=destination_route.did
								   
								   WHERE user_vehicle_details.uId=".$fetch['sId']."
								   AND destination_route.session_id=".$_SESSION['current_session_id']."";
					$exe_amount_t=mysql_query($trans_amount);
					while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
						{
							$transport_cost=$transport_cost+$fetch_transport_cost[0];
						}
					//query get rent of hostel from room allocation --------
					$hostel_rent=0;
					$hostel_amount="SELECT rent 
									FROM hostel_room_allocation
									WHERE sid=".$fetch['sId']."
									AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
					$exe_rent=mysql_query($hostel_amount);
					while($fetch_rent_cost=mysql_fetch_array($exe_rent))
						{
							$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
						}
					$trans_rent_cost=$hostel_rent+$transport_cost;
					// query to calculate total fee amount
					//query calculate total credentials amount
					$camount=0;
					$fetch_credentials_type="SELECT fee_credentials.type,fee_credentials.amount
											
											FROM fee_credentials
											
											INNER JOIN fee_details
											ON fee_details.fee_credential_id=fee_credentials.id
											
											WHERE fee_details.student_id = ".$fetch['student_id']."
											AND fee_credentials.session_id=".$current_session."
											AND fee_details.fee_generated_session_id=".$fetch_dues[0]."";
					$execute_credentials_type=mysql_query($fetch_credentials_type);
					while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
						{
							$camount=$camount+$fetch_credentials['amount'];
						}
					//query calculate total amount of discount
					$total_discount=0; 
					$fetch_discount_query="SELECT fee_discount_credentials.type,fee_discount_credentials.amount
										 
										  FROM fee_discount_credentials
										  
										  INNER JOIN fee_details
										  ON fee_details.fee_discount_id=fee_discount_credentials.id
										  
										  WHERE fee_details.student_id = ".$fetch['student_id']."
										  AND fee_discount_credentials.session_id=".$current_session."
										  AND fee_details.fee_generated_session_id=".$fetch_dues[0]."";
					$execute_discount_type=mysql_query(  $fetch_discount_query);
					while($fetch_discount=mysql_fetch_array($execute_discount_type))
						{ 
							$total_discount=$total_discount+$fetch_discount['amount'];
						}
					//calculate total fee amount (credentials fee amount-discount fee amount)
					$cre_amount=$camount+$transport_cost;
					$total_fee_amount=$camount-$total_discount+$trans_rent_cost;
					//calculate fee amount according to duration
					//Monthly=1  Quarterly-3, half early=6 and annually=12
					$m=1;
					if($month=="Monthly")
						{
							$m=1; 
						}
					else if($month=="Quarterly")
						{
							$m=3; 
						}
					else if($month=="Half-Yearly")
						{
							$m=6;  
						}	
					else if($month=="Yearly")
						{
							$m=12;  
						}
					$paid_fee= $total_fee_amount*$m;										 
					/*echo'  <td  width="12%">'.$fetch_session['session_name'].'</td>';*/
					$tution_amount = $cre_amount*3;
					echo '<td width="12%">'.$fetch_dues['duration'].'</td>
					<td width="12%">'.$tution_amount.'</td>';
					/*echo '<td width="12%">'.$hostel_rent.'</td>';*/
							//calculate fine
						
					//get current date and show fine
					date_default_timezone_set('Asia/Kolkata');
		             $current_date=date('Y-m-d');
					  $current_id="SELECT date_id FROM dates_d
					             WHERE date='".$current_date."'";
					 $exe_current=mysql_query($current_id);
					 $fetch_current_date=mysql_fetch_array($exe_current);
					 
					 //get paid on
					$paid_4="SELECT paid, paid_on
								 FROM fee_details
								 WHERE student_id=".$fetch['student_id']."
								 AND fee_generated_session_id=".$fetch_dues[0]."";
						$exe_paid_4=mysql_query($paid_4);
						$fetch_paid_4=mysql_fetch_array($exe_paid_4);
					 //if condition check fee paid then fine calculate paid on date else calculate asia colkata
					 if($fetch_paid_4[0] == -1)
					 {
						 
						 $paid_on_fine_date =$fetch_paid_4[1];
					 }
					 else
					 {
						 $paid_on_fine_date = $fetch_current_date[0];
						 
					 }
					if($fetch_dues['last_date']<$paid_on_fine_date)
					  { 
						 $fine_days = $paid_on_fine_date - $fetch_dues['last_date'];
						if($fine_days<=30)
						{
							$calculate_fine=100;
						}
						else if($fine_days<=60)
						{
							$calculate_fine=200;
						}
						else
						{
							$calculate_fine=500;
						}
						/*$calculate_fine=$fine_days*$fine; */
					}
				else
					{
						$calculate_fine=0;  
					}
					$paid_fee_with_fine = $paid_fee + $calculate_fine;
					echo '<td width="12%">'.$calculate_fine.'</td>
					<td width="12%">'.$paid_fee_with_fine.'</td>
					
					';
					//query to get date on the id of date
					$get_date_of_id=
									"SELECT DISTINCT date
									FROM dates_d
									WHERE date_id = ".$fetch_dues['last_date']."";
					$exe_date=mysql_query($get_date_of_id);
					$fetch_date=mysql_fetch_array($exe_date);
					$due_date = $fetch_date[0];
					echo '			
					<td width="9%">'.$due_date.'</td>
					';
					echo '			
					<td width="9%" style="color:green">'.$fetch_dues[8].'</td>';
					$totalpaidfee=$totalpaidfee + $fetch_dues[8];
					//if remaning fee == 0
					if($fetch_dues[9]==0)
					{
						echo '<td width="9%" style="color:red">'.$paid_fee_with_fine.'</td>
					';
					$totalduefee1 =$totalduefee1 + $paid_fee_with_fine;
					}
					else
					{
					echo '<td width="9%" style="color:red">'.$fetch_dues[9].'</td>
					';
					$totalduefee = $totalduefee + $fetch_dues[9] ;
					}
					
					if($fetch_dues['paid']==0)
					{
				/*	echo '<td class=" " width="11%">
	`				<span class="tip"><a class="uibutton " href="accounts_fee_paid_class_wise.php?cid='.$cid.'&id='.$fetch['student_id'].'
					&name='.$fetch['Name']
					.'&f_g_sid='.$fetch_dues['fee_generated_session_id'].'" original-title=""> Pay</a></span> 
					</td>';*/
					/*echo '<td width="12%"><a href="accounts_fee_cheque_details.php?add='.$fetch['admission_no'].'
					&name='.$fetch['Name'].'&sid='.$fetch['sId'].'&duid='.$fetch_dues[0].'">View</a></td></tr>';*/
					}
					else if($fetch_dues['paid']==3)
					{
					/*echo '<td class=" " width="11%">
					<span class="tip"><a class="uibutton " href="accounts_fee_cheque_clear_class.php?id='.$fetch['student_id'].'
					&name='.$fetch['Name']
					.'&f_g_sid='.$fetch_dues['fee_generated_session_id'].'" original-title=""> Pending</a></span> 
					</td>';*/
					/*echo '<td width="12%"><a href="accounts_fee_cheque_details.php?add='.$fetch['admission_no'].'&name='.$fetch['Name'                    ].'&sid='.$fetch['sId'].'&duid='.$fetch_dues[0].'">View</a></td></tr>';*/
					}
					
						else 
					{
						continue;
					}
				}
			}
				
			echo'</table></td></tr>';
		
		}
		$abc =$totalduefee +$totalduefee1;
	echo' </tbody>
	<h4 style="color:green" align="center">TOTAL PAID FEE ='.$totalpaidfee.'</h4>
	<h4 style="color:red" align="center">TOTAL DUE FEE ='.$abc .'</h4>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}


function fee_defaulters_acording_level_dd()
{


		$sn = 0;

$grand_amounts=0;
     $duration=$_GET['duration'];
    // $cid=$_GET['cls'];
     //$d_type=$_GET['d_type'];
 $s=0;
if($duration==1001)
     {
         $dur=1;
         $d="Apr";
         $dr=3;
     }
     elseif($duration==1002)
     {
         $dur=2;
          $d="May";
          $dr=6;
     }
       elseif($duration==1003)
     {
         $dur=3;
         $d="June";
         $dr=9;
     }
       elseif($duration==1004)
     {
         $dur=4;
         $d="July";
         $dr=12;
     }
    
        echo' 
			<div class="row-fluid">
			<!-- Table widget -->
			<div class="widget  span12 clearfix">

			<div class="widget-header">
<h2 align="center" style="color:green">VIDYA BAL BHAWAN PUBLIC SCHOOL</H2>
			<span>Fee Defaulter List of (April-June) of Class ( '.$fetch_class[1].' ) </span>
			</div><!-- End widget-header -->	
			<div class="widget-content">';
 echo '<a href="accounts_fee_school_defalter_print.php?duration='. $duration.'&type=0"><input type="submit" style="color:green" value="PRINT DEFAULTER`S STUDENTS" ></a>';

	echo'                                   
			<table class="table table-bordered table-striped" id="dataTable">
			<thead>';
			echo'    
			<tr>
			<th width="5%">S.No.</th>
			<th  width="12%">Admission No.</th>
			<th width="12%">Name</th>
			<th width="12%">Class</th>
                        <th width="12%">Phone</th>
                        <th width="12%">Father`s Name</th>
			

			</thead>
			
			<tbody align="center">';



 $class="SELECT * FROM class_index  ORDER BY order_by";
     $exe_cid=mysql_query($class);
    while($fetch_class=mysql_fetch_array($exe_cid))
{
//ppppppppppppppppppppppppppppppppppppppppppppp
$cid=$fetch_class[0];
			 $defa="SELECT student_user.* FROM student_user
                             
                                INNER JOIN class
                                ON class.sId=student_user.sId


                               WHERE  class.classId=".$cid."  AND class.session_id=".$_SESSION['current_session_id']." 
				AND student_user.sId NOT IN
				(

SELECT sid FROM fee_receipt_details where due<1 and duration_value >=".$dr."

)

AND student_user.sId NOT IN

(
SELECT sid FROM ews_students
)

AND student_user.sId NOT IN

(
SELECT student_id FROM struck_off_student
)





";

                         $exe_dur=mysql_query($defa);
                        
                       

                        $s=0;
                        while($fetch_dur=mysql_fetch_array($exe_dur))
                        {
                            $student_id=$fetch_dur[0];
                            $du=$fetch_dur['duration_id'];
                          //  $amo="SELECT SUM(monthly_charges) FROM fee_details WHERE student_id=".$student_id." AND duration_id=".$du."";
                           // $exe_du=mysql_query($amo);
                           // $fetch_am=mysql_fetch_array($exe_du);
                            
                            $amo_s="SELECT * FROM student_user WHERE sId=".$student_id." ";
                            $exe_du_s=mysql_query($amo_s);
                            $fetch_am_s=mysql_fetch_array($exe_du_s);

 $amo_s1="SELECT * FROM fee_receipt_details WHERE sid=".$student_id." ";
                            $exe_du_s1=mysql_query($amo_s1);
                            $fetch_am_s1=mysql_fetch_array($exe_du_s1);
                            
                            $cl="SELECT class_name FROM class_index 
                                INNER JOIN class
                                ON class.classId=class_index.cId
                                WHERE class.sId=".$student_id." AND class.session_id=".$_SESSION['current_session_id']." ";
                            $exe_cl=mysql_query($cl);
                            if(!$exe_cl)
                            {
                                
                            }
                            else 
                            {
                            $fetch_cl=mysql_fetch_array($exe_cl);
                            
                            echo '<tr>';
                               echo ' <td>'.++$sn.'</td>';
                               echo ' <td>'. $fetch_am_s['admission_no'].'</td>';
                               echo ' <td>'. $fetch_am_s['Name'].'</td>';
                              echo ' <td>'. $fetch_cl[0].'</td>';
                               echo ' <td>'. $fetch_am_s['Phone No'].'</td>';
                                echo ' <td>'. $fetch_am_s[5].'</td>';
                             //  echo ' <td>'.$fetch_am[0].'</td>';
                               
                               echo '</tr>';

        
  




//echo ' <h5 align="right"><b>Class Teacher Name ....................................................................</b></h5>
                                           //     <h5 //align="right"><b>Sign:........................................................................</b></h5>';



}

      
                            }
                        }
  //  echo '<a href="fee/send_defaulter_message.php?du='. $duration.'"><input type="submit" value="Send message" ></a>';
    
  //  echo '    3:Call all the defaulters students today only i.e. on 30-07-14 for fee submission.<br>';
echo '
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->

	</div><!-- row-fluid -->
	';






  /*   $duration=$_GET['duration'];
     if($duration==1001)
     {
         $dur=1;
         $d="Apr-Jun";
     }
     elseif($duration==1002)
     {
         $dur=4;
          $d="Jul-Sep";
     }
       elseif($duration==1003)
     {
         $dur=7;
         $d="Oct-Dec";
     }
       elseif($duration==1004)
     {
         $dur=10;
         $d="Jan-March";
     }


 echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="accounts_fee_school_defalter_print.php?duration='. $duration.'&type=1"><input type="submit" style="color:green" value="PRINT EWS STUDENTS" ></a>';
        echo'
<br><br>
			<div class="row-fluid">
			<!-- Table widget -->
			<div class="widget  span12 clearfix">
			<div class="widget-header">

			<span>Fee Defaulter List of ( '.$d.' ) </span>
			</div><!-- End widget-header -->	
			<div class="widget-content">';
			 $defa="SELECT * FROM fee_details WHERE paid=0 AND ews=0  AND fee_generated_session_id=".$dur." ";
                        $exe_dur=mysql_query($defa);
                        
                       
			echo'                                   
			<table class="table table-bordered table-striped" id="dataTable">
			<thead>';
			echo'    
			<tr>
			<th width="5%">S.No.</th>
			<th  width="12%">Admission No.</th>
			<th width="12%">Name</th>
			<th width="12%">Class</th>
                        <th width="12%">Phone</th>
                        <th width="12%">Father`s Name</th>
			
			<th width="12%">Amounts</th>
			</thead>
			
			<tbody align="center">';
                        $s=0;
                        while($fetch_dur=mysql_fetch_array($exe_dur))
                        {
                            $student_id=$fetch_dur[1];
                            $du=$fetch_dur['duration_id'];
                            $amo="SELECT SUM(monthly_charges) FROM fee_details WHERE student_id=".$student_id." AND duration_id=".$du."";
                            $exe_du=mysql_query($amo);
                            $fetch_am=mysql_fetch_array($exe_du);
                            
                            $amo_s="SELECT * FROM student_user WHERE sId=".$student_id." ";
                            $exe_du_s=mysql_query($amo_s);
                            $fetch_am_s=mysql_fetch_array($exe_du_s);
                            
                            $cl="SELECT class_name FROM class_index 
                                INNER JOIN class
                                ON class.classId=class_index.cId
                                WHERE class.sId=".$student_id."";
                            $exe_cl=mysql_query($cl);
                            if(!$exe_cl)
                            {
                                
                            }
                            else 
                            {
                            $fetch_cl=mysql_fetch_array($exe_cl);
                            
                            echo '<tr>';
                               echo ' <td>'.++$s.'</td>';
                               echo ' <td>'. $fetch_am_s['admission_no'].'</td>';
                               echo ' <td>'. $fetch_am_s['Name'].'</td>';
                               echo ' <td>'.$fetch_cl[0].'</td>';
                               echo ' <td>'. $fetch_am_s['Phone No'].'</td>';
                                echo ' <td>'. $fetch_am_s[5].'</td>';
                               echo ' <td>'.$fetch_am[0].'</td>';
                               
                               echo '</tr>';

        
                            }
                        }
    
echo' </tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->

	';
echo '<a href="fee/send_defaulter_message.php?du='. $duration.'"><input type="submit" style="color:red" value="SEND MESSAGE" ></a>';



*/






}


function accounts_fee_school_defalter_print()
{





$grand_amounts=0;
     $duration=$_GET['duration'];
    // $cid=$_GET['cls'];
     //$d_type=$_GET['d_type'];
 $s=0;
if($duration==1001)
     {
         $dur=1;
         $d="Apr-June";
         $dr=3;
     }
     elseif($duration==1002)
     {
         $dur=2;
          $d="July-Sept";
          $dr=6;
     }
       elseif($duration==1003)
     {
         $dur=3;
         $d="Oct-Dec";
         $dr=9;
     }
       elseif($duration==1004)
     {
         $dur=4;
         $d="Jan-March";
         $dr=12;
     }
    
        echo' 
			<div class="row-fluid">
			<!-- Table widget -->
			<div class="widget  span12 clearfix">
			<div class="widget-header">
<h3 align="center" style="color:green">VIDYA BAL BHAWAN SR. SEC. SCHOOL</H2>
<h4 align="center" style="color:red">Fee Defaulter List of ('.$d.') </H2>
			
			</div><!-- End widget-header -->	
			<div class="widget-content">';
// echo '<a href="accounts_fee_school_defalter_print.php?duration='. $duration.'&type=0"><input type="submit" style="color:green" value="PRINT GENERAL STUDENTS" ></a>';

	echo'                                   
			<table class="table table-bordered table-striped" id="dataTable">
			<thead>';
			echo'    
			<tr>
			<th width="5%">S.No.</th>
			<th  width="12%">Admission No.</th>
			<th width="12%">Name</th>
			<th width="12%">Class</th>
                        <th width="12%">Phone</th>
                        <th width="12%">Father`s Name</th>
			

			</thead>
			
			<tbody align="center">';



 $class="SELECT * FROM class_index  ORDER BY order_by";
     $exe_cid=mysql_query($class);
    while($fetch_class=mysql_fetch_array($exe_cid))
{

$cid=$fetch_class[0];
if(($cid==17)||($cid==19))
{

}
else
{
//ppppppppppppppppppppppppppppppppppppppppppppp
			 $defa="SELECT student_user.* FROM student_user
                             
                                INNER JOIN class
                                ON class.sId=student_user.sId

                               WHERE  class.classId=".$cid."  AND class.session_id=".$_SESSION['current_session_id']." 
				AND student_user.sId NOT IN
				(
SELECT sid FROM fee_receipt_details where due<1 AND duration_value >=".$dr."

)

AND student_user.sId NOT IN

(
SELECT sid FROM ews_students

)
AND student_user.sId NOT IN

(
SELECT student_id FROM struck_off_student
)


";
                         $exe_dur=mysql_query($defa);
                        
                       
		
                       
                        while($fetch_dur=mysql_fetch_array($exe_dur))
                        {
                            $student_id=$fetch_dur[0];
                            $du=$fetch_dur['duration_id'];
                          //  $amo="SELECT SUM(monthly_charges) FROM fee_details WHERE student_id=".$student_id." AND duration_id=".$du."";
                           // $exe_du=mysql_query($amo);
                           // $fetch_am=mysql_fetch_array($exe_du);
                            
                            $amo_s="SELECT * FROM student_user WHERE sId=".$student_id." ";
                            $exe_du_s=mysql_query($amo_s);
                            $fetch_am_s=mysql_fetch_array($exe_du_s);

 $amo_s1="SELECT * FROM fee_receipt_details WHERE sid=".$student_id." ";
                            $exe_du_s1=mysql_query($amo_s1);
                            $fetch_am_s1=mysql_fetch_array($exe_du_s1);
                            
                            $cl="SELECT class_name FROM class_index 
                                INNER JOIN class
                                ON class.classId=class_index.cId
                                WHERE class.sId=".$student_id." AND class.session_id=".$_SESSION['current_session_id']." ";
                            $exe_cl=mysql_query($cl);
                            if(!$exe_cl)
                            {
                                
                            }
                            else 
                            {
                            $fetch_cl=mysql_fetch_array($exe_cl);
                            
                            echo '<tr>';
                               echo ' <td>'.++$s.'</td>';
                               echo ' <td>'. $fetch_am_s['admission_no'].'</td>';
                               echo ' <td>'. $fetch_am_s['Name'].'</td>';
                              echo ' <td>'. $fetch_cl[0].'</td>';
                               echo ' <td>'. $fetch_am_s['Phone No'].'</td>';
                                echo ' <td>'. $fetch_am_s[5].'</td>';
                             //  echo ' <td>'.$fetch_am[0].'</td>';
                               
                               echo '</tr>';

        
  




//echo ' <h5 align="right"><b>Class Teacher Name ....................................................................</b></h5>
                                           //     <h5 //align="right"><b>Sign:........................................................................</b></h5>';



}

      
                            }
                            }
                        }
  //  echo '<a href="fee/send_defaulter_message.php?du='. $duration.'"><input type="submit" value="Send message" ></a>';
    
  //  echo '    3:Call all the defaulters students today only i.e. on 30-07-14 for fee submission.<br>';
echo '
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';



/*
$grand_amounts=0;
     $duration=$_GET['duration'];
$type=$_GET['type'];
     if($duration==1001)
     {
         $dur=1;
         $d="Apr-Jun";
     }
     elseif($duration==1002)
     {
         $dur=4;
          $d="Jul-Sep";
     }
       elseif($duration==1003)
     {
         $dur=7;
         $d="Oct-Dec";
     }
       elseif($duration==1004)
     {
         $dur=10;
         $d="Jan-March";
     }
        echo' 
			<div class="row-fluid">
			<!-- Table widget -->
			<div class="widget  span12 clearfix">

			<div class="widget-header">
  <h2 style="color:green" align="center">VIDYA BAL BHAWAN SR. SECONDARY SCHOOL</h2>

  <h5 style="color:red" align="center">Fee Defaulter List of ( '.$d.' )</h5>
			
			</div><!-- End widget-header -->	
			<div class="widget-content">';
			 $defa="SELECT * FROM fee_details WHERE paid=0 AND ews=".$type."  AND fee_generated_session_id=".$dur." ";
                        $exe_dur=mysql_query($defa);
                        
                       
			echo'                                   
			<table class="table table-bordered table-striped" id="dataTable">
			<thead>';
			echo'    
			<tr>

			<th width="5%">S.No.</th>
			<th  width="12%">Admission No.</th>
			<th width="12%">Name</th>
			<th width="12%">Class</th>
                        <th width="12%">Phone</th>
                        <th width="12%">Father`s Name</th>
			
			<th width="12%">Amounts</th>

			</thead>
			
			<tbody align="center">';
                        $s=0;
                        while($fetch_dur=mysql_fetch_array($exe_dur))
                        {
                            $student_id=$fetch_dur[1];
                            $du=$fetch_dur['duration_id'];
                            $amo="SELECT SUM(monthly_charges) FROM fee_details WHERE student_id=".$student_id." AND duration_id=".$du."";
                            $exe_du=mysql_query($amo);
                            $fetch_am=mysql_fetch_array($exe_du);
                            
                            $amo_s="SELECT * FROM student_user WHERE sId=".$student_id." ";
                            $exe_du_s=mysql_query($amo_s);
                            $fetch_am_s=mysql_fetch_array($exe_du_s);
                            
                            $cl="SELECT class_name FROM class_index 
                                INNER JOIN class
                                ON class.classId=class_index.cId

                                WHERE class.sId=".$student_id."";
                            $exe_cl=mysql_query($cl);
                            if(!$exe_cl)
                            {
                                
                            }
                            else 
                            {
                            $fetch_cl=mysql_fetch_array($exe_cl);
                            
                            echo '<tr>';
                               echo ' <td>'.++$s.'</td>';
                               echo ' <td>'. $fetch_am_s['admission_no'].'</td>';
                               echo ' <td>'. $fetch_am_s['Name'].'</td>';
                               echo ' <td>'.$fetch_cl[0].'</td>';
                               echo ' <td>'. $fetch_am_s['Phone No'].'</td>';

                                     echo ' <td>'. $fetch_am_s[5].'</td>';


                        
 if($type==1)
{
   echo ' <td>EWS</td>';
}
elseif($type==0)
{
$grand_amounts=$grand_amounts+$fetch_am[0];
       echo ' <td>'.$fetch_am[0].'</td>';
}

                               echo '</tr>';

        
                            }
                        }
   // echo '<a href="fee/send_defaulter_message.php?du='. $duration.'"><input type="submit" value="Send message" ></a>';
echo'
<tr  style="color:red" align="center"><td colspan=6>Grand Total</td><td>'.$grand_amounts.'</td></tr>
 </tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';

*/
}



function fee_defaulters_general_ews_send_sms()
{
	$query="SELECT  *
            FROM  `class_index` 
             ORDER BY class+0 ASC,section ASC";
	$fetch=mysql_query($query) ;	
	
	echo '
	                <div class="row-fluid">
                    
                    		<!-- Widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-align-left"></i> Alert Form </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <!-- title box -->
                                    
                                      <form id="validation_demo" action="users/send_alert_message_general_ews_all_students.php" method="post">	
                                    <input type="hidden" value="all_send" name="to_msg"/>		
                                       <div class="section">
                                      <label>select class</label>
                                      <div>									
<select  class="chzn-select " multiple tabindex="18" name="class_stu_all[]">
    <option value=""></option>
';
$class_name=array();
$i=0;
while($cls=mysql_fetch_array($fetch))
{
$class_name[$i]=$cls['class_name'];	
$cls_name=$cls['class_name'];
echo '

<option value="'.$cls[0].'">'.$cls_name.'</option>';
$i++;

}
echo '
</select>
</div>
</div>';

echo '<div class="section">
	<label>Select Duration<small></small></label>
	<div>
	<select name="duration">
	<option value="">--SELECT DURATION--</option>';

	echo '<option value="1001" >April-June</option>
	      <option value="1002" >July-Sep</option>
	      <option value="1003">Oct-Dec</option>
		  <option value="1004">Jan-March</option>
	</select>
	</div>
	</div>';
	
	echo '<div class="section">
	<label>Select Typr<small></small></label>
	<div>
	<select name="type">
	<option value="">--SELECT TYPE--</option>';

	echo '<option value="0" >EWS</option>
	      <option value="1" >GENERAL</option>
	     
	</select>
	</div>
	</div>';


											echo '

  <div class="section">
                                                 <label>Message</label>   
               <div> <textarea name="message_school" class="validate[required] large" id="all_msg_show"  cols="" rows=""></textarea></div>   
                                                
                                             </div>
                                
                                            <div class="section last">
                                                <div><button class="uibutton normal">Send</button> 
												</div>
                                           </div>

                                        </form>
											
											
											</div>
											</div>
											</div>';	
}
//show defalter level wise
function fee_defaulters_level_class()
{
	//Author By: Anil Kumar *
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Not Pay Students</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';
//    $duration="SELECT Id, duration FROM fee_generated_sessions
//	           WHERE session_id=".$_SESSION['current_session_id']."";
//	$exe_duration=mysql_query($duration);	
	echo'  
	<div class="load_page">
	<div class="formEl_b">	';
	//in this form level of defalters select 
	echo '
	<form id="validation_demo" action="accounts_show_defaulter_according_level.php" method="get"> ';
//	<div class="section ">
//	<label>Select Type <small></small></label>   
//	<div> 
//	<select name="level">
//	<option value="">---SELECT LEVEL---</option>
//	<option value="1">Pre Primary</option>
//	<option value="2"> Primary</option>
//	<option value="3">Secondary</option>
//	<option value="4">Senior Secondary</option>
//	<option value="5">Class Wise</option>
//	<option value="6">School Wise</option>
//	</select>
//	</div>
//	</div>
	echo '<div class="section">
	<label>Select Duration<small></small></label>
	<div>
	<select name="duration">
	<option value="">--SELECT DURATION--</option>';
//	while($fetch_duration_id=mysql_fetch_array($exe_duration))
//	    {
//	        echo '<option value="'.$fetch_duration_id[0].'">'.$fetch_duration_id[1].'</option>';
//		}
	echo '<option value="1001" >April-June</option>
	      <option value="1002" >July-Sep</option>
	      <option value="1003">Oct-Dec</option>
		  <option value="1004">Jan-March</option>
	</select>
	</div>
	</div>';
  /*  echo ' <div class="section">
	       <div>OR </div></div>';
	echo '
	<div class="section">
	<label>Select Date</label>   
	<div><input type="text"  id="datepick" class="datepicker" readonly="readonly" name="date"  />
	</div>
	</div>';*/
	echo '<div class="section last">
	<div>
	<button class="btn submit_form">Submit</button>
	</div>
	</div>
	</form>
	</div>								
	</div>';
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}
//show defalter level wise
function fee_defaulters_level_class_def()
{
	//Author By: Anil Kumar *
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Not Pay Students</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';
//    $duration="SELECT Id, duration FROM fee_generated_sessions
//	           WHERE session_id=".$_SESSION['current_session_id']."";
//	$exe_duration=mysql_query($duration);	
	echo'  
	<div class="load_page">
	<div class="formEl_b">	';
	//in this form level of defalters select 
	echo '
	<form id="validation_demo" action="accounts_fee_class_defaulter_cls.php" method="get"> ';
//	<div class="section ">
//	<label>Select Type <small></small></label>   
//	<div> 
//	<select name="level">
//	<option value="">---SELECT LEVEL---</option>
//	<option value="1">Pre Primary</option>
//	<option value="2"> Primary</option>
//	<option value="3">Secondary</option>
//	<option value="4">Senior Secondary</option>
//	<option value="5">Class Wise</option>
//	<option value="6">School Wise</option>
//	</select>
//	</div>
//	</div>
	echo '<div class="section">
	<label>Select Duration<small></small></label>
	<div>
	<select name="duration">
	<option value="">--SELECT DURATION--</option>';
//	while($fetch_duration_id=mysql_fetch_array($exe_duration))
//	    {
//	        echo '<option value="'.$fetch_duration_id[0].'">'.$fetch_duration_id[1].'</option>';
//		}
	echo '<option value="1001" >April-June</option>
	      <option value="1002" >July-Sep</option>
	      <option value="1003">Oct-Dec</option>
		  <option value="1004">Jan-March</option>
	</select>
	</div>
	</div>';
        $cid="SELECT * FROM class_index ORDER BY order_by";
        $exe_cid=mysql_query($cid);
        echo '<div class="section">
	<label>Select Class<small></small></label>
	<div>
	<select name="cls">
	<option value="">--SELECT Class--</option>';
        
    



//	while($fetch_duration_id=mysql_fetch_array($exe_duration))
//	    {
//	        echo '<option value="'.$fetch_duration_id[0].'">'.$fetch_duration_id[1].'</option>';
//		}
        while($fetch_cid=mysql_fetch_array($exe_cid))
        {
            echo '<option value="'.$fetch_cid[0].'" >'.$fetch_cid[1].'</option>';
        }  
	echo '</select>
	</div>
	</div>';

   echo '<div class="section">
	<label>Select EWS / General<small></small></label>
	<div>
	<select name="d_type">
	<option value="">--SELECT EWS/GENERAL--</option>';
//	while($fetch_duration_id=mysql_fetch_array($exe_duration))
//	    {
//	        echo '<option value="'.$fetch_duration_id[0].'">'.$fetch_duration_id[1].'</option>';
//		}
	echo '<option value="1" >EWS</option>
	      <option value="0" >GENERAL</option>
	     
	</select>
	</div>
	</div>';
  /*  echo ' <div class="section">
	       <div>OR </div></div>';
	echo '
	<div class="section">
	<label>Select Date</label>   
	<div><input type="text"  id="datepick" class="datepicker" readonly="readonly" name="date"  />
	</div>
	</div>';*/
	echo '<div class="section last">
	<div>
	<button class="btn submit_form">Submit</button>
	</div>
	</div>
	</form>
	</div>								
	</div>';
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}
function fee_defaulters_level_class_cls()
{
     $d_type=$_GET['d_type'];
     $duration=$_GET['duration'];
     $cid=$_GET['cls'];
	if($duration==1001)
     {
         $dur=1;
         $d="Apr-June";
         $dr=3;
     }
     elseif($duration==1002)
     {
         $dur=2;
          $d="July-Sep";
            $dr=6;
     }
       elseif($duration==1003)
     {
         $dur=3;
         $d="Oct-Dec";
           $dr=9;
     }
       elseif($duration==1004)
     {
         $dur=4;
         $d="Jan-march";
         $dr=12;
     }
     $class="SELECT * FROM class_index WHERE cId=".$cid."";
     $exe_cid=mysql_query($class);
     $fetch_class=mysql_fetch_array($exe_cid);


//EWS student

$query="SELECT * FROM class_index WHERE cId=".$cid."";
$exe=mysql_query($query);
$fetch_class=mysql_fetch_array($exe);
if($d_type==1){
    echo' 
			<div class="row-fluid">
			<!-- Table widget -->
			<div class="widget  span12 clearfix">
			<div class="widget-header">
			<span>Fee Defaulter List of ('.$d.') of Class ( '.$fetch_class[1].' ) </span>
			</div><!-- End widget-header -->	
			<div class="widget-content">';
			 $defa="SELECT student_user.* FROM student_user
                             
                                INNER JOIN class
                                ON class.sId=student_user.sId
				INNER JOIN ews_students
				ON ews_students.sid=student_user.sId

                               WHERE  class.classId=".$cid."  AND class.session_id=".$_SESSION['current_session_id']." 
				AND student_user.sId NOT IN
				(
SELECT sid FROM fee_receipt_details where due=0 AND cheque_status=0 AND duration_value >=".$dr."

)




";
                         $exe_dur=mysql_query($defa);
                        
                       
			echo'                                   
			<table class="table table-bordered table-striped" id="dataTable">
			<thead>';
			echo'    
			<tr>
			<th width="5%">S.No.</th>
			<th  width="12%">Admission No.</th>
			<th width="12%">Name</th>
			
                        <th width="12%">Phone</th>
                        <th width="12%">Father`s Name</th>
			

			</thead>
			
			<tbody align="center">';
                        $s=0;
                        while($fetch_dur=mysql_fetch_array($exe_dur))
                        {
                            $student_id=$fetch_dur[0];
                            $du=$fetch_dur['duration_id'];
                          //  $amo="SELECT SUM(monthly_charges) FROM fee_details WHERE student_id=".$student_id." AND duration_id=".$du."";
                           // $exe_du=mysql_query($amo);
                           // $fetch_am=mysql_fetch_array($exe_du);
                            
                            $amo_s="SELECT * FROM student_user WHERE sId=".$student_id." ";
                            $exe_du_s=mysql_query($amo_s);
                            $fetch_am_s=mysql_fetch_array($exe_du_s);

 $amo_s1="SELECT * FROM fee_receipt_details WHERE sid=".$student_id." ";
                            $exe_du_s1=mysql_query($amo_s1);
                            $fetch_am_s1=mysql_fetch_array($exe_du_s1);
                            
                            $cl="SELECT class_name FROM class_index 
                                INNER JOIN class
                                ON class.classId=class_index.cId
                                WHERE class.sId=".$student_id." AND class.session_id=".$_SESSION['current_session_id']." ";
                            $exe_cl=mysql_query($cl);
                            if(!$exe_cl)
                            {
                                
                            }
                            else 
                            {
                            $fetch_cl=mysql_fetch_array($exe_cl);
                            
                            echo '<tr>';
                               echo ' <td>'.++$s.'</td>';
                               echo ' <td>'. $fetch_am_s['admission_no'].'</td>';
                               echo ' <td>'. $fetch_am_s['Name'].'</td>';
                              
                               echo ' <td>'. $fetch_am_s['Phone No'].'</td>';
                                echo ' <td>'. $fetch_am_s[5].'</td>';
                             //  echo ' <td>'.$fetch_am[0].'</td>';
                               
                               echo '</tr>';

        
  


        
                            }
                        }

}

else
{
		 echo' 
			<div class="row-fluid">
			<!-- Table widget -->
			<div class="widget  span12 clearfix">
			<div class="widget-header">
			<span>Fee Defaulter List of ('.$d.') of Class ( '.$fetch_class[1].' ) </span>
			</div><!-- End widget-header -->	
			<div class="widget-content">';
			 $defa="SELECT student_user.* FROM student_user
                             
                                INNER JOIN class
                                ON class.sId=student_user.sId
				

                               WHERE  class.classId=".$cid."  AND class.session_id=".$_SESSION['current_session_id']." 
				AND student_user.sId NOT IN
				(
SELECT sid FROM fee_receipt_details where due=0 AND cheque_status=0 AND duration_value >=".$dr."

)

AND student_user.sId NOT IN

(
SELECT sid FROM ews_students

)



";
                         $exe_dur=mysql_query($defa);
                        
                       
			echo'                                   
			<table class="table table-bordered table-striped" id="dataTable">
			<thead>';
			echo'    
			<tr>
			<th width="5%">S.No.</th>
			<th  width="12%">Admission No.</th>
			<th width="12%">Name</th>
			
                        <th width="12%">Phone</th>
                        <th width="12%">Father`s Name</th>
			

			</thead>
			
			<tbody align="center">';
                        $s=0;
                        while($fetch_dur=mysql_fetch_array($exe_dur))
                        {
                            $student_id=$fetch_dur[0];
                            $du=$fetch_dur['duration_id'];
                          //  $amo="SELECT SUM(monthly_charges) FROM fee_details WHERE student_id=".$student_id." AND duration_id=".$du."";
                           // $exe_du=mysql_query($amo);
                           // $fetch_am=mysql_fetch_array($exe_du);
                            
                            $amo_s="SELECT * FROM student_user WHERE sId=".$student_id." ";
                            $exe_du_s=mysql_query($amo_s);
                            $fetch_am_s=mysql_fetch_array($exe_du_s);

 $amo_s1="SELECT * FROM fee_receipt_details WHERE sid=".$student_id." ";
                            $exe_du_s1=mysql_query($amo_s1);
                            $fetch_am_s1=mysql_fetch_array($exe_du_s1);
                            
                            $cl="SELECT class_name FROM class_index 
                                INNER JOIN class
                                ON class.classId=class_index.cId
                                WHERE class.sId=".$student_id." AND class.session_id=".$_SESSION['current_session_id']." ";
                            $exe_cl=mysql_query($cl);
                            if(!$exe_cl)
                            {
                                
                            }
                            else 
                            {
                            $fetch_cl=mysql_fetch_array($exe_cl);
                            
                            echo '<tr>';
                               echo ' <td>'.++$s.'</td>';
                               echo ' <td>'. $fetch_am_s['admission_no'].'</td>';
                               echo ' <td>'. $fetch_am_s['Name'].'</td>';
                              
                               echo ' <td>'. $fetch_am_s['Phone No'].'</td>';
                                echo ' <td>'. $fetch_am_s[5].'</td>';
                             //  echo ' <td>'.$fetch_am[0].'</td>';
                               
                               echo '</tr>';

        
  


        
                            }
                        }




}
  //  echo '<a href="fee/send_defaulter_message.php?du='. $duration.'"><input type="submit" value="Send message" ></a>';
echo' </tbody>
    <button><a href="accounts_fee_class_defaulter_print.php?duration='.$duration.'&cls='.$cid.'&d_type='.$d_type.'">PRINT</a></button>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}
function fee_defaulters_level_class_cls_print()
{
	$grand_amounts=0;
     $duration=$_GET['duration'];
     $cid=$_GET['cls'];
      $d_type=$_GET['d_type'];
 	$s=0;
	if($duration==1001)
     {
         $dur=1;
         $d="Apr-June";
         $dr=3;
     }
     elseif($duration==1002)
     {
         $dur=2;
          $d="July-Sep";
            $dr=6;
     }
       elseif($duration==1003)
     {
         $dur=3;
         $d="Oct-Dec";
           $dr=9;
     }
       elseif($duration==1004)
     {
         $dur=4;
         $d="Jan-march";
         $dr=12;
     }
     $class="SELECT * FROM class_index WHERE cId=".$cid."";
     $exe_cid=mysql_query($class);
     $fetch_class=mysql_fetch_array($exe_cid);
        echo' 
			<div class="row-fluid">
			<!-- Table widget -->
			<div class="widget  span12 clearfix">
			<div class="widget-header">
			<span>Fee Defaulter List of ('.$d.') of Class ( '.$fetch_class[1].' ) </span>
			</div><!-- End widget-header -->	
			<div class="widget-content">';
			 $defa="SELECT student_user.* FROM student_user
                             
                                INNER JOIN class
                                ON class.sId=student_user.sId

                               WHERE  class.classId=".$cid."  AND class.session_id=".$_SESSION['current_session_id']." 
				AND student_user.sId NOT IN
				(
SELECT sid FROM fee_receipt_details where due=0 AND cheque_status=0 AND duration_value >=".$dr."

)

AND student_user.sId NOT IN

(
SELECT sid FROM ews_students

)



";
                         $exe_dur=mysql_query($defa);
                        
                       
			echo'                                   
			<table class="table table-bordered table-striped" id="dataTable">
			<thead>';
			echo'    
			<tr>
			<th width="5%">S.No.</th>
			<th  width="12%">Admission No.</th>
			<th width="12%">Name</th>
			
                        <th width="12%">Phone</th>
                        <th width="12%">Father`s Name</th>
			

			</thead>
			
			<tbody align="center">';
                        $s=0;
                        while($fetch_dur=mysql_fetch_array($exe_dur))
                        {
                            $student_id=$fetch_dur[0];
                            $du=$fetch_dur['duration_id'];
                          //  $amo="SELECT SUM(monthly_charges) FROM fee_details WHERE student_id=".$student_id." AND duration_id=".$du."";
                           // $exe_du=mysql_query($amo);
                           // $fetch_am=mysql_fetch_array($exe_du);
                            
                            $amo_s="SELECT * FROM student_user WHERE sId=".$student_id." ";
                            $exe_du_s=mysql_query($amo_s);
                            $fetch_am_s=mysql_fetch_array($exe_du_s);

 $amo_s1="SELECT * FROM fee_receipt_details WHERE sid=".$student_id." ";
                            $exe_du_s1=mysql_query($amo_s1);
                            $fetch_am_s1=mysql_fetch_array($exe_du_s1);
                            
                            $cl="SELECT class_name FROM class_index 
                                INNER JOIN class
                                ON class.classId=class_index.cId
                                WHERE class.sId=".$student_id." AND class.session_id=".$_SESSION['current_session_id']." ";
                            $exe_cl=mysql_query($cl);
                            if(!$exe_cl)
                            {
                                
                            }
                            else 
                            {
                            $fetch_cl=mysql_fetch_array($exe_cl);
                            
                            echo '<tr>';
                               echo ' <td>'.++$s.'</td>';
                               echo ' <td>'. $fetch_am_s['admission_no'].'</td>';
                               echo ' <td>'. $fetch_am_s['Name'].'</td>';
                              
                               echo ' <td>'. $fetch_am_s['Phone No'].'</td>';
                                echo ' <td>'. $fetch_am_s[5].'</td>';
                             //  echo ' <td>'.$fetch_am[0].'</td>';
                               
                               echo '</tr>';

        
  




//echo ' <h5 align="right"><b>Class Teacher Name ....................................................................</b></h5>
                                           //     <h5 //align="right"><b>Sign:........................................................................</b></h5>';





      
                            }
                        }
  //  echo '<a href="fee/send_defaulter_message.php?du='. $duration.'"><input type="submit" value="Send message" ></a>';
    
  //  echo '    3:Call all the defaulters students today only i.e. on 30-07-14 for fee submission.<br>';
echo '
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}

//show defalter according class wise and level 
function fee_defaulters_acording_level()
{


	//Author By: Anil Kumar *
		
    
	 $duration=$_GET['duration'];
	

			
			
	//if value of select level=6 then show fee defalters of all school
	if($level==6)
		{
			
			$sno=0;
			$current_session=$_SESSION['current_session_id'];
			echo' 
			<div class="row-fluid">
			<!-- Table widget -->
			<div class="widget  span12 clearfix">
			<div class="widget-header">
			<span>Fee Not Paid Students List </span>
			</div><!-- End widget-header -->	
			<div class="widget-content">';
			if(isset($_GET['done']))
				{
					$sql_duration="SELECT duration
					FROM fee_generated_sessions
					WHERE Id = ".$_GET['f_g_sid'];
					$execute_duration=mysql_query($sql_duration);
					$fetch_duration=mysql_fetch_array($execute_duration);
					echo '<h4 align="center" style="color:green">Paid!! by '.$_GET['name'].' of '.$fetch_duration['duration'].
					'</h4>';
				}
			echo'                                   
			<table class="table table-bordered table-striped" id="dataTable">
			<thead>';
			echo'    
			<tr>
			<th width="5%">S.No.</th>
			<th  width="12%">Admission No.</th>
			<th width="12%">Name</th>
			<th width="12%">Class</th>
			<th width="60%"><table><thead>
			
			<th width="12%">Duration</th>
			<th width="12%">Last Date</th>
			<th width="12%">Tution Fee</th>
			
			<th width="12%">Late Fine</th>
			<th width="12%">Total Fee</th>
			<th width="12%" style="color:green">Paid Fee</th>
			<th width="12%" style="color:red">Due Fee</th>
			</thead></table></th>
			</tr>
			</thead>
			<tbody align="center">';
			//query get defaulter students details 
			if(($duration==0)||($duration==-2))
			{
		    $query = "SELECT DISTINCT student_user.Name, student_user.admission_no, student_user.sId,class_index.class_name,
					fee_details.student_id
					
					FROM student_user
					
					INNER JOIN class
					ON student_user.sId = class.sId
					
					INNER JOIN class_index
					ON class.classId = class_index.cId
					
					INNER JOIN fee_details
					ON fee_details.student_id = student_user.sId
					
					WHERE (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
					";
			}
			
			elseif($duration>0)
			{
				 $query = "SELECT DISTINCT student_user.Name, student_user.admission_no,
				    student_user.sId,class_index.class_name,
					fee_details.student_id
					
					FROM student_user
					
					INNER JOIN class
					ON student_user.sId = class.sId
					
					INNER JOIN class_index
					ON class.classId = class_index.cId
					
					INNER JOIN fee_details
					ON fee_details.student_id = student_user.sId
					
					WHERE fee_details.fee_generated_session_id=".$duration."
					
					AND (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
					";
			}
			elseif($duration==-1)
				{
					 $query = "SELECT DISTINCT student_user.Name, student_user.admission_no,
				    student_user.sId,class_index.class_name,
					fee_details.student_id
					
					FROM student_user
					
					INNER JOIN class
					ON student_user.sId = class.sId
					
					INNER JOIN class_index
					ON class.classId = class_index.cId
					ON fee_details.student_id = student_user.sId
					
					WHERE (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
					
					INNER JOIN fee_details
					ON fee_details.student_id = student_user.sId
					
					WHERE (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
					AND (fee_details.fee_generated_session_id=1 OR fee_details.fee_generated_session_id=2)
					";
				}
		elseif($duration==-3)
		{
			date_default_timezone_set('Asia/Kolkata');
		    $date=date('Y-m-d');
			$month_du=preg_split("/\-/",$date);
			$month_duration=$month_du[1];
			//show defalter aprail to june
			if(($month_duration>3)&&($month_duration<7))
			{
			 $query = "SELECT DISTINCT student_user.Name, student_user.admission_no,
				    student_user.sId,class_index.class_name,
					fee_details.student_id
					
					FROM student_user
					
					INNER JOIN class
					ON student_user.sId = class.sId
					
					INNER JOIN class_index
					ON class.classId = class_index.cId
					
					INNER JOIN fee_details
					AND (fee_details.fee_generated_session_id=1 )
					";
			}
			//aprail to september
			elseif(($month_duration>6)&&($month_duration<10))
			{
			 $query = "SELECT DISTINCT student_user.Name, student_user.admission_no,
				    student_user.sId,class_index.class_name,
					fee_details.student_id
					
					FROM student_user
					
					INNER JOIN class
					ON student_user.sId = class.sId
					
					INNER JOIN class_index
					ON class.classId = class_index.cId
					
					INNER JOIN fee_details
					ON fee_details.student_id = student_user.sId
					
					WHERE (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
					AND (fee_details.fee_generated_session_id=1 OR fee_details.fee_generated_session_id=2)
					";
			
			}
			//aprail to september to december
			elseif(($month_duration>9)&&($month_duration<13))
			{
			 $query = "SELECT DISTINCT student_user.Name, student_user.admission_no,
				    student_user.sId,class_index.class_name,
					fee_details.student_id
					
					FROM student_user
					
					INNER JOIN class
					ON student_user.sId = class.sId
					
					INNER JOIN class_index
					ON class.classId = class_index.cId
					
					INNER JOIN fee_details
					ON fee_details.student_id = student_user.sId
					
					WHERE (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
					AND (fee_details.fee_generated_session_id=1 OR fee_details.fee_generated_session_id=2
					OR fee_details.fee_generated_session_id=3)
					";
			}
			//aprail to september to december to march
			elseif(($month_duration>0)&&($month_duration<4))
			{
				 $query = "SELECT DISTINCT student_user.Name, student_user.admission_no,
				    student_user.sId,class_index.class_name,
					fee_details.student_id
					
					FROM student_user
					
					INNER JOIN class
					ON student_user.sId = class.sId
					
					INNER JOIN class_index
					ON class.classId = class_index.cId
					
					INNER JOIN fee_details
					ON fee_details.student_id = student_user.sId
					
					WHERE (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
					AND (fee_details.fee_generated_session_id=1 OR fee_details.fee_generated_session_id=2
					OR fee_details.fee_generated_session_id=3 OR fee_details.fee_generated_session_id=7)
					";
			}

			
		}
			$execute = mysql_query($query) ;
			while($fetch = mysql_fetch_array($execute))
			{            
			++$sno; 
			if($duration>0)
				{
					//query get defaulter students details    
					$query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
								fee_generated_sessions.session_id, fee_generated_sessions.duration,
								 fee_generated_sessions.last_date,
								fee_generated_sessions.type, fee_details.paid, fee_details.remarks
								,fee_details.receiving_fee, fee_details.remaning_fee
								FROM fee_generated_sessions
								
								INNER JOIN fee_details 
								ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
								
								WHERE fee_details.student_id = ".$fetch['student_id']."
								AND fee_generated_sessions.Id=".$duration."
								";
				}
				
				elseif(($duration==0)||($duration==-2))
				{
					//query get defaulter students details    
					$query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
								fee_generated_sessions.session_id, fee_generated_sessions.duration,
								 fee_generated_sessions.last_date,
								fee_generated_sessions.type, fee_details.paid, fee_details.remarks
								,fee_details.receiving_fee, fee_details.remaning_fee
								FROM fee_generated_sessions
								
								INNER JOIN fee_details 
								ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
								
								WHERE fee_details.student_id = ".$fetch['student_id']."
								";

				}
				elseif($duration==-1)
				{
					$query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
								fee_generated_sessions.session_id, fee_generated_sessions.duration,
								 fee_generated_sessions.last_date,
								fee_generated_sessions.type, fee_details.paid, fee_details.remarks
								,fee_details.receiving_fee, fee_details.remaning_fee
								FROM fee_generated_sessions
								
								INNER JOIN fee_details 
								ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
								
								WHERE fee_details.student_id = ".$fetch['student_id']."
								AND (fee_generated_sessions.Id=1 OR fee_generated_sessions.Id=2)
								";					
				}
				elseif($duration==-3)
		{
			date_default_timezone_set('Asia/Kolkata');
		    $date=date('Y-m-d');
			$month_du=preg_split("/\-/",$date);
			$month_duration=$month_du[1];
			//show defalter aprail to june
			if(($month_duration>3)&&($month_duration<7))
			{
			$query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
								fee_generated_sessions.session_id, fee_generated_sessions.duration,
								 fee_generated_sessions.last_date,
								fee_generated_sessions.type, fee_details.paid, fee_details.remarks
								,fee_details.receiving_fee, fee_details.remaning_fee
								FROM fee_generated_sessions
								
								INNER JOIN fee_details 
								ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
								
								WHERE fee_details.student_id = ".$fetch['student_id']."
								AND (fee_generated_sessions.Id=1 )
								";
			}
			//aprail to september
			elseif(($month_duration>6)&&($month_duration<10))
			{
			$query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
								fee_generated_sessions.session_id, fee_generated_sessions.duration,
								 fee_generated_sessions.last_date,
								fee_generated_sessions.type, fee_details.paid, fee_details.remarks
								,fee_details.receiving_fee, fee_details.remaning_fee
								FROM fee_generated_sessions
								
								INNER JOIN fee_details 
								ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
								
								WHERE fee_details.student_id = ".$fetch['student_id']."
								AND (fee_generated_sessions.Id=1 OR fee_generated_sessions.Id=2)
								";
			
			}
			//aprail to september to december
			elseif(($month_duration>9)&&($month_duration<13))
			{
			$query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
								fee_generated_sessions.session_id, fee_generated_sessions.duration,
								 fee_generated_sessions.last_date,
								fee_generated_sessions.type, fee_details.paid, fee_details.remarks
								,fee_details.receiving_fee, fee_details.remaning_fee
								FROM fee_generated_sessions
								
								INNER JOIN fee_details 
								ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
								
								WHERE fee_details.student_id = ".$fetch['student_id']."
								AND (fee_generated_sessions.Id=1 OR fee_generated_sessions.Id=2
								OR fee_generated_sessions.Id=3)
								";
			}
			//aprail to september to december to march
			elseif(($month_duration>0)&&($month_duration<4))
			{
					$query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
								fee_generated_sessions.session_id, fee_generated_sessions.duration,
								 fee_generated_sessions.last_date,
								fee_generated_sessions.type, fee_details.paid, fee_details.remarks
								,fee_details.receiving_fee, fee_details.remaning_fee
								FROM fee_generated_sessions
								
								INNER JOIN fee_details 
								ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
								
								WHERE fee_details.student_id = ".$fetch['student_id']."
								AND (fee_generated_sessions.Id=1 OR fee_generated_sessions.Id=2
								OR fee_generated_sessions.Id=3 OR fee_generated_sessions.Id=7)
								";
			}

			
		}
			$execute_dues=mysql_query($query_dues);
			echo'     
			<tr class="odd gradeX">
			<td width="5%">'.$sno.'</td>
			<td width="12%">'.$fetch['admission_no'].'</td>
			<td width="12%">'.$fetch['Name'].'</td>
			<td width="12%">'.$fetch['class_name'].'</td><td width="60%"><table width=100%>';
			while($fetch_dues=mysql_fetch_array($execute_dues))	          
				{
				if(($fetch_dues['paid']==1)||($fetch_dues['paid']==2))
				  {
					 continue; 
				  }
				  else
				  {  			
					echo'  
					<tr align="center">';
					$query_session="SELECT session_name
									FROM session_table
									WHERE sId = ".$fetch_dues['session_id']."
									";
					$execute_session=mysql_query($query_session);
					$fetch_session=mysql_fetch_array($execute_session);	
					//query to  show total fee  of  fee  defalter
					//write query to get fee generated session id 
					$query_fee_ge_session_id="SELECT `type`
											FROM `fee_generated_sessions`
											WHERE session_id=".$_SESSION['current_session_id']."
											AND Id=".$fetch_dues[0]."";
					$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
					$fetch_fee=mysql_fetch_array($execute_fee_ge_se_id);
					$month=$fetch_fee['type'];
					//QUERY get cost of transport from destination_rouite------
					$transport_cost=0;
					$trans_amount="SELECT cost
								   FROM destination_route
								   
								   INNER JOIN user_vehicle_details
								   ON user_vehicle_details.did=destination_route.did
								   
								   WHERE user_vehicle_details.uId=".$fetch['student_id']."
								   AND destination_route.session_id=".$_SESSION['current_session_id']."";
					$exe_amount_t=mysql_query($trans_amount);
					while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
						{
							$transport_cost=$transport_cost+$fetch_transport_cost[0];
						}
					//query get rent of hostel from room allocation --------
					$hostel_rent=0;
					$hostel_amount="SELECT rent 
									FROM hostel_room_allocation
									WHERE sid=".$fetch['student_id']."
									AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
					$exe_rent=mysql_query($hostel_amount);
					while($fetch_rent_cost=mysql_fetch_array($exe_rent))
						{
							$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
						}
					$trans_rent_cost=$hostel_rent+$transport_cost;
					// query to calculate total fee amount
					//query calculate total credentials amount
					$camount=0;
					$fetch_credentials_type="SELECT fee_credentials.type,fee_credentials.amount
											
											FROM fee_credentials
											
											INNER JOIN fee_details
											ON fee_details.fee_credential_id=fee_credentials.id
											
											WHERE fee_details.student_id = ".$fetch['student_id']."
											AND fee_credentials.session_id=".$current_session."
											AND fee_details.fee_generated_session_id=".$fetch_dues[0]."";
					$execute_credentials_type=mysql_query($fetch_credentials_type);
					while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
						{
							$camount=$camount+$fetch_credentials['amount'];
						}
					//query calculate total amount of discount
					$total_discount=0; 
					$fetch_discount_query="SELECT fee_discount_credentials.type,fee_discount_credentials.amount
										 
										  FROM fee_discount_credentials
										 
										  INNER JOIN fee_details
										  ON fee_details.fee_discount_id=fee_discount_credentials.id
										 
										  WHERE fee_details.student_id = ".$fetch['student_id']."
										  AND fee_discount_credentials.session_id=".$current_session."
										  AND fee_details.fee_generated_session_id=".$fetch_dues[0]."";
					$execute_discount_type=mysql_query(  $fetch_discount_query);
					while($fetch_discount=mysql_fetch_array($execute_discount_type))
						{ 
							$total_discount=$total_discount+$fetch_discount['amount'];
						}
					//calculate total fee
					$cre_amount=$camount+$transport_cost;
					$total_fee_amount=$camount-$total_discount+$trans_rent_cost;
					//calculate fee amount according to duration
					//Monthly=1  Quarterly-3, half early=6 and annually=12
					$m=1;
					if($month=="Monthly")
						{
							$m=1; 
						}
					else if($month=="Quarterly")
						{
							$m=3; 
						}
					else if($month=="Half-Yearly")
						{
							$m=6;  
						}	
					else if($month=="Yearly")
						{
							$m=12;  
						}
					$paid_fee= $total_fee_amount*$m;										 
					echo'  
					
					<td width="12%">'.$fetch_dues['duration'].'</td>
					';
					//query to get date on the id of date
					$get_date_of_id=
									"SELECT DISTINCT date
									FROM dates_d
									WHERE date_id = ".$fetch_dues['last_date']."";
					$exe_date=mysql_query($get_date_of_id);
					$fetch_date=mysql_fetch_array($exe_date);
					$due_date = $fetch_date[0];
					//calculate fine
					//get current date and show fine
					date_default_timezone_set('Asia/Kolkata');
		             $current_date=date('Y-m-d');
					  $current_id="SELECT date_id FROM dates_d
					             WHERE date='".$current_date."'";
					 $exe_current=mysql_query($current_id);
					 $fetch_current_date=mysql_fetch_array($exe_current);
						 //get paid on
					$paid_4="SELECT paid, paid_on
								 FROM fee_details
								 WHERE student_id=".$fetch['student_id']."
								 AND fee_generated_session_id=".$fetch_dues[0]."";
						$exe_paid_4=mysql_query($paid_4);
						$fetch_paid_4=mysql_fetch_array($exe_paid_4);
					 //if condition check fee paid then fine calculate paid on date else calculate asia colkata
					 if($fetch_paid_4[0] == -1)
					 {
						 
						 $paid_on_fine_date =$fetch_paid_4[1];
					 }
					 else
					 {
						 $paid_on_fine_date = $fetch_current_date[0];
						 
					 }
					if($fetch_dues['last_date']<$paid_on_fine_date)
					  { 
						 $fine_days = $paid_on_fine_date - $fetch_dues['last_date'];
						if($fine_days<=30)
						{
							$calculate_fine=100;
						}
						else if($fine_days<=60)
						{
							$calculate_fine=200;
						}
						else
						{
							$calculate_fine=500;
						}
						/*$calculate_fine=$fine_days*$fine; */
					}
				else
					{
						$calculate_fine=0;  
					}
					$paid_fee_with_fine = $paid_fee + $calculate_fine;
					$tution_fee = $cre_amount*3;
					echo '			
					<td width="9%">'.$due_date.'</td>
					<td width="12%">'.$tution_fee.'</td>
					
					<td width="12%">'.$calculate_fine.'</td>
					<td width="12%">'.$paid_fee_with_fine.'</td>
					';
					echo '			
					<td width="9%" style="color:green">'.$fetch_dues[8].'</td>';
					/*$totalpaidfee=$totalpaidfee + $fetch_dues[8];*/
					//if remaning fee == 0
					if($fetch_dues[9]==0)
					{
						echo '<td width="9%" style="color:red">'.$paid_fee_with_fine.'</td>
					';
					/*$totalduefee1 =$totalduefee1 + $paid_fee_with_fine;*/
					}
					else
					{
					echo '<td width="9%" style="color:red">'.$fetch_dues[9].'</td>
					';
					/*$totalduefee = $totalduefee + $fetch_dues[9] ;*/
					}
					if($fetch_dues['paid']==0)
					{
				/*	echo '<td class=" " width="11%">
					<span class="tip"><a class="uibutton "  href="accounts_fee_paid_level_wise.php?level='.$level.'&id='.$fetch['student_id'].'
					&name='.$fetch['Name']
					.'&f_g_sid='.$fetch_dues['fee_generated_session_id'].'" original-title=""> Pay</a></span> 
					</td>';*/
					/*echo '<td width="12%"><a href="accounts_fee_cheque_details.php?add='.$fetch['admission_no'].'&name='.$fetch['Name'                    ].'&sid='.$fetch['sId'].'&duid='.$fetch_dues[0].'">View</a></td></tr>';*/
					}
					else if($fetch_dues['paid']==3)
					{
				/*	echo '<td class=" " width="11%">
					<span class="tip"><a class="uibutton " href="accounts_fee_cheque_clear_class.php?id='.$fetch['student_id'].'
					&name='.$fetch['Name']
					.'&f_g_sid='.$fetch_dues['fee_generated_session_id'].'" original-title=""> Pending</a></span></td> ';*/
					/*echo'
					<td width="12%"><a href="accounts_fee_cheque_details.php?add='.$fetch['admission_no'].'&name='.$fetch['Name'                    ].'&sid='.$fetch['sId'].'&duid='.$fetch_dues[0].'">View</a></td></tr>';*/
					}
						else 
					{
						continue;
					}
				}
			}
			
					echo'</table></td></tr>';
		}
	echo' </tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid --
	';	
	}
	
	//show defalter according to primary and secondary (if level = 1,2,3,4)
	else
		{	
			$sno=0;
			$current_session=$_SESSION['current_session_id'];
			echo' 
			<div class="row-fluid">
			<!-- Table widget -->
			<div class="widget  span12 clearfix">
			<div class="widget-header">
			<span>Fee Not Paid Students List </span>
			</div><!-- End widget-header -->	
			<div class="widget-content">';
			if(isset($_GET['done']))
				{
					$sql_duration="SELECT duration
								  FROM fee_generated_sessions
								  WHERE Id = ".$_GET['f_g_sid'];
					$execute_duration=mysql_query($sql_duration);
					$fetch_duration=mysql_fetch_array($execute_duration);
					echo '
					<h4 align="center" style="color:green">Paid!! by '.$_GET['name'].' of '.$fetch_duration['duration'].'</h4>';
				}
			echo'                                   
			<table class="table table-bordered table-striped" id="dataTable">
			<thead>';
			echo'    <tr>
			<th width="5%">S.No.</th>
			<th  width="12%">Admission No.</th>
			<th width="12%">Name</th>
			<th width="12%">Class</th>
			<th width="60%"><table><thead>
			
			<th width="12%">Duration</th>
			<th width="12%">Tution Fee</th>
			
			<th width="12%">Late Fine</th>
			<th width="12%" style="color:green">Total Fee</th>
			<th width="12%" style="color:red">Last Date</th>
			
			</thead></table></th>
			</tr>
			</thead>
			<tbody align="center">';
			if($duration==0)
			{
		    $query = "SELECT DISTINCT student_user.Name, student_user.admission_no, student_user.sId,class_index.class_name,
					fee_details.student_id
					
					FROM student_user
					
					INNER JOIN class
					ON student_user.sId = class.sId
					
					INNER JOIN class_index
					ON class.classId = class_index.cId
					
					INNER JOIN fee_details
					ON fee_details.student_id = student_user.sId
					
					WHERE class_index.level=".$level."
		            
					AND (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
                     ";
			}
			if($duration<0)
				{
					header('Location:accounts_fee_defalter_level_class.php?no');
				}
			else
			{
				    $query = "SELECT DISTINCT student_user.Name, student_user.admission_no, 
					  student_user.sId,class_index.class_name,
					fee_details.student_id
					
					FROM student_user
					
					INNER JOIN class
					ON student_user.sId = class.sId
					
					INNER JOIN class_index
					ON class.classId = class_index.cId
					
					INNER JOIN fee_details
					ON fee_details.student_id = student_user.sId
					
					WHERE class_index.level=".$level."
		            
					AND (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
					AND fee_details.fee_generated_session_id=".$duration."
                     ";
			}
			$execute = mysql_query($query) ;
			while($fetch = mysql_fetch_array($execute))
				{            
					++$sno; 
					if($duration==0) 
					{  
					$query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
					 fee_generated_sessions.session_id, fee_generated_sessions.duration, fee_generated_sessions.last_date,
					  fee_generated_sessions.type, fee_details.paid, fee_details.remarks
					
					FROM fee_generated_sessions
					INNER JOIN fee_details 
					ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
					WHERE fee_details.student_id = ".$fetch['student_id']."
					AND fee_generated_sessions.Id = fee_details.fee_generated_session_id
                    ";
					}
					if($duration<0)
						{
							header('Location:accounts_fee_defalter_level_class.php?no');
						}
					else
					{
					 $query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
					 fee_generated_sessions.session_id, fee_generated_sessions.duration, fee_generated_sessions.last_date,
					 fee_generated_sessions.type, fee_details.paid, fee_details.remarks
					
					FROM fee_generated_sessions
					INNER JOIN fee_details 
					ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
					WHERE fee_details.student_id = ".$fetch['student_id']."
					AND fee_generated_sessions.Id = ".$duration."
                    ";	
					}
					$execute_dues=mysql_query($query_dues);
					echo'     <tr class="odd gradeX">
					<td width="5%">'.$sno.'</td>
					<td width="12%">'.$fetch['admission_no'].'</td>
					<td width="12%">'.$fetch['Name'].'</td>
					<td width="12%">'.$fetch['class_name'].'</td><td width="60%"><table width=100%>';
					while($fetch_dues=mysql_fetch_array($execute_dues))	          
						{
							if(($fetch_dues['paid']==1)||($fetch_dues['paid']==2))
							  {
								 continue;
								 
							  }
							  else
							  {  			
							echo'  <tr align="center">';
							$query_session="SELECT session_name
											FROM session_table
											WHERE sId = ".$fetch_dues['session_id']."
							";
							$execute_session=mysql_query($query_session);
							$fetch_session=mysql_fetch_array($execute_session);	
							//query to  show total fee  of  fee  defalter
							//write query to get fee generated session id 
							$query_fee_ge_session_id="SELECT `type`
													FROM `fee_generated_sessions`
													WHERE session_id=".$_SESSION['current_session_id']."
													AND Id=".$fetch_dues[0]."";
							$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
							$fetch_fee=mysql_fetch_array($execute_fee_ge_se_id);
							$month=$fetch_fee['type'];
							//QUERY get cost of transport from destination_rouite------
							$transport_cost=0;
							$trans_amount="SELECT cost
										   FROM destination_route
										   
										   INNER JOIN user_vehicle_details
										   ON user_vehicle_details.did=destination_route.did
										   
										   WHERE user_vehicle_details.uId=".$fetch['sId']."
										   AND destination_route.session_id=".$_SESSION['current_session_id']."";
							$exe_amount_t=mysql_query($trans_amount);
							while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
								{
									$transport_cost=$transport_cost+$fetch_transport_cost[0];
								}
							//query get rent of hostel from room allocation --------
							$hostel_rent=0;
							$hostel_amount="SELECT rent 
											FROM hostel_room_allocation
											WHERE sid=".$fetch['sId']."
											AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
							$exe_rent=mysql_query($hostel_amount);
							while($fetch_rent_cost=mysql_fetch_array($exe_rent))
								{
									$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
								}
							$trans_rent_cost=$hostel_rent+$transport_cost;
							// query to calculate total fee amount
							//query calculate total credentials amount
							$camount=0;
							$fetch_credentials_type="SELECT fee_credentials.type,fee_credentials.amount
													
													FROM fee_credentials
													
													INNER JOIN fee_details
													ON fee_details.fee_credential_id=fee_credentials.id
													
													WHERE fee_details.student_id = ".$fetch['student_id']."
													AND fee_credentials.session_id=".$current_session."
													AND fee_details.fee_generated_session_id=".$fetch_dues[0]."";
							$execute_credentials_type=mysql_query($fetch_credentials_type);
							while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
								{
									$camount=$camount+$fetch_credentials['amount'];
								}
							//query calculate total amount of discount
							$total_discount=0; 
							$fetch_discount_query="SELECT fee_discount_credentials.type,fee_discount_credentials.amount
												  
												  FROM fee_discount_credentials
												  
												  INNER JOIN fee_details
												  ON fee_details.fee_discount_id=fee_discount_credentials.id
												  
												  WHERE fee_details.student_id = ".$fetch['student_id']."
												  AND fee_discount_credentials.session_id=".$current_session."
												  AND fee_details.fee_generated_session_id=".$fetch_dues[0]."";
							$execute_discount_type=mysql_query(  $fetch_discount_query);
							while($fetch_discount=mysql_fetch_array($execute_discount_type))
								{ 
									$total_discount=$total_discount+$fetch_discount['amount'];
								}
							//calculate total fee
							$cre_amount=$camount+$transport_cost;
							$total_fee_amount=$camount-$total_discount+$trans_rent_cost;
							//calculate fee amount according to duration
							//Monthly=1  Quarterly-3, half early=6 and annually=12
							$m=1;
							if($month=="Monthly")
								{
									$m=1; 
								}
							else if($month=="Quarterly")
								{
									$m=3; 
								}
							else if($month=="Half-Yearly")
								{
									$m=6;  
								}	
							else if($month=="Yearly")
								{
									$m=12;  
								}
							$paid_fee= $total_fee_amount*$m;
							$tution_fee	=$cre_amount*3;									 
							echo' 
							<td width="12%">'.$fetch_dues['duration'].'</td>
							<td width="12%">'.$tution_fee.'</td>
							';
								//calculate fine
					//get current date and show fine
					date_default_timezone_set('Asia/Kolkata');
		             $current_date=date('Y-m-d');
					  $current_id="SELECT date_id FROM dates_d
					             WHERE date='".$current_date."'";
					 $exe_current=mysql_query($current_id);
					 $fetch_current_date=mysql_fetch_array($exe_current);
					if($fetch_dues['last_date']<$fetch_current_date[0])
					  {
						$fine_days=$fetch_current_date[0] - $fetch_dues['last_date'];
						if($fine_days<=30)
						{
							$calculate_fine=100;
						}
						else if($fine_days<=60)
						{
							$calculate_fine=200;
						}
						else
						{
							$calculate_fine=500;
						}
						/*$calculate_fine=$fine_days*$fine; */
					}
				else
					{
						$calculate_fine=0;  
					}
					$paid_fee_with_fine = $paid_fee + $calculate_fine;
							echo '<td width="12%">'.$calculate_fine.'</td>
							<td width="12%">'.$paid_fee_with_fine.'</td>
							';
							//query to get date on the id of date
							$get_date_of_id=
											"SELECT DISTINCT date
											FROM dates_d
											WHERE date_id = ".$fetch_dues['last_date']."";
							$exe_date=mysql_query($get_date_of_id);
							$fetch_date=mysql_fetch_array($exe_date);
							$due_date = $fetch_date[0];
							echo '			
							<td width="9%">'.$due_date.'</td>
							';
					if($fetch_dues['paid']==0)
					{
				/*	echo '<td class=" " width="11%">
					<span class="tip"><a class="uibutton " href="accounts_fee_paid_level_wise.php?level='.$level.'&id='.$fetch['student_id'].'
					&name='.$fetch['Name']
					.'&f_g_sid='.$fetch_dues['fee_generated_session_id'].'" original-title=""> Pay</a></span> 
					</td>';*/
				/*	echo '<td width="12%"><a href="accounts_fee_cheque_details.php?add='.$fetch['admission_no'].'&name='.$fetch['Name'                    ].'&sid='.$fetch['sId'].'&duid='.$fetch_dues[0].'">View</a></td></tr>';*/
					}
					else if($fetch_dues['paid']==3)
					{
/*					echo '<td class=" " width="11%">
					<span class="tip"><a class="uibutton " href="accounts_fee_cheque_clear_class.php?id='.$fetch['student_id'].'
					&name='.$fetch['Name']
					.'&f_g_sid='.$fetch_dues['fee_generated_session_id'].'" original-title=""> Pending</a></span> 
					</td>';
*/					/*echo '<td width="12%"><a href="accounts_fee_cheque_details.php?add='.$fetch['admission_no'].'&name='.$fetch['Name'                    ].'&sid='.$fetch['sId'].'&duid='.$fetch_dues[0].'">View</a></td></tr>';*/
					}
						else 
					{
						
						continue;
					}
				}
			}
					echo'</table></td></tr>';
				}
		}
	echo' </tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}

//function to show fee not pay students
function fee_defaulters()
{
	
	//Author By: Anil Kumar *
	
	$finee="fine";
	$fine=0;
	//write query to fetch fine amount
	$query_fine="SELECT `amount` 
				FROM `fee_credentials`
				WHERE  `type`='".$finee."'";
	$execute_fine=mysql_query($query_fine);
	$fetch_fine=mysql_fetch_array($execute_fine);
	$fine=$fetch_fine['amount'];	
	$sno=0;
	$current_session=$_SESSION['current_session_id'];
	echo' 
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Not Paid Students List </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	if(isset($_GET['done']))
		{
			$sql_duration="SELECT duration
			FROM fee_generated_sessions
			WHERE Id = ".$_GET['f_g_sid'];
			$execute_duration=mysql_query($sql_duration);
			$fetch_duration=mysql_fetch_array($execute_duration);
			echo '
			<h4 align="center" style="color:green">Paid!! by '.$_GET['name'].' of '.$fetch_duration['duration'].'</h4>';
		}
	if(isset($_GET['clear']))
		{
			$sql_duration="SELECT duration
			FROM fee_generated_sessions
			WHERE Id = ".$_GET['f_g_sid'];
			$execute_duration=mysql_query($sql_duration);
			$fetch_duration=mysql_fetch_array($execute_duration);
			echo '
			<h4 align="center" style="color:green">Paid!! by '.$_GET['name'].' of '.$fetch_duration['duration'].'</h4>';
		}
	echo'                                   
	<table class="table table-bordered table-striped" id="dataTable">
	<thead>';
	echo'    
	<tr>
	<th width="5%">S.No.</th>
	<th  width="12%">Admission No.</th>
	<th width="12%">Name</th>
	<th width="12%">Class</th>
	<th width="90%"><table><thead>
	<th width="12%">Session</th>
	<th width="12%">Duration</th>
	<th width="12%">Last Date</th>
	<th width="12%">Credential Fee</th>
	<th width="12%">Hostel Fee</th>
	<th width="12%">Late Fine</th>
	<th width="12%">Total Fee</th>
	<th width="11%">Remarks</th>
	</thead></table></th>
	</tr>
	</thead>
	<tbody align="center">';
	//get defaulter students personal details according to class
	$query = "SELECT DISTINCT student_user.Name, student_user.admission_no, student_user.sId,class_index.class_name, 
			fee_details.student_id 
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			WHERE (fee_details.paid  = 0 OR fee_details.paid = 3 OR fee_details.paid = 4)
			
			
			ORDER BY admission_no ASC
			";
	$execute = mysql_query($query) ;
	while($fetch = mysql_fetch_array($execute))
		{            
			++$sno; 
			//get defaulter students fee details according to class   
		$query_dues="SELECT DISTINCT(fee_details.fee_generated_session_id), fee_details.student_id,
						fee_generated_sessions.session_id, fee_generated_sessions.duration, fee_generated_sessions.last_date,
						fee_generated_sessions.type, fee_details.paid, fee_details.remarks
						
						FROM fee_generated_sessions
						
						INNER JOIN fee_details 
						ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
						
						WHERE fee_details.student_id = ".$fetch['student_id']."
						 
						
						";
			$execute_dues=mysql_query($query_dues);
			
			echo'     <tr class="odd gradeX">
			<td width="5%">'.$sno.'</td>
			<td width="12%">'.$fetch['admission_no'].'</td>
			<td width="12%">'.$fetch['Name'].'</td>
			<td width="12%">'.$fetch['class_name'].'</td><td width="60%"><table width=100%>';
			
			while($fetch_dues=mysql_fetch_array($execute_dues))	          
				{	
				if(($fetch_dues['paid']==1)||($fetch_dues['paid']==2))
				  {
					 continue; 
				  }
				  else
				  {
					echo'  <tr align="center">';
					$query_session="SELECT session_name
									FROM session_table
									WHERE sId = ".$fetch_dues['session_id']."
									";
					$execute_session=mysql_query($query_session);
					$fetch_session=mysql_fetch_array($execute_session);	
					//query to  show total fee  of  fee  defalter
					//write query to get fee generated session id 
					$query_fee_ge_session_id="SELECT `type`
											FROM `fee_generated_sessions`
											WHERE session_id=".$_SESSION['current_session_id']."
											AND Id=".$fetch_dues[0]."";
					$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
					$fetch_fee=mysql_fetch_array($execute_fee_ge_se_id);
					$month=$fetch_fee['type'];
					//get paid status===============================
		             /*$paid="SELECT paid FROM fee_details
					      
						  INNER JOIN fee_generated_sessions
						  ON fee_generated_sessions.Id = fee_details.fee_generated_session_id
						  
					      WHERE student_id=".$fetch['student_id']."";
				   $exe_paid=mysql_query($paid);
				   $fetch_paid=mysql_fetch_array($exe_paid);*/
	//QUERY get cost of transport from destination_rouite------
	$transport_cost=0;
	$trans_amount="SELECT cost
	               FROM destination_route
				   
				   INNER JOIN user_vehicle_details
				   ON user_vehicle_details.did=destination_route.did
				   
				   WHERE user_vehicle_details.uId=".$fetch['sId']."
				   AND destination_route.session_id=".$_SESSION['current_session_id']."";
	$exe_amount_t=mysql_query($trans_amount);
	while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
		{
			$transport_cost=$transport_cost+$fetch_transport_cost[0];
		}
	//query get rent of hostel from room allocation --------
	$hostel_rent=0;
	$hostel_amount="SELECT rent 
	                FROM hostel_room_allocation
					WHERE sid=".$fetch['sId']."
					AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
	$exe_rent=mysql_query($hostel_amount);
	while($fetch_rent_cost=mysql_fetch_array($exe_rent))
		{
			$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
		}
	$trans_rent_cost=$hostel_rent+$transport_cost;
					// query to calculate total fee amount
					//query calculate total credentials amount
					$camount=0;
					$fetch_credentials_type="SELECT fee_credentials.type,fee_credentials.amount
											
											FROM fee_credentials
											
											INNER JOIN fee_details
											ON fee_details.fee_credential_id=fee_credentials.id
											
											WHERE fee_details.student_id = ".$fetch['student_id']."
											AND fee_credentials.session_id=".$current_session."
											AND fee_details.fee_generated_session_id=".$fetch_dues[0]."";
					$execute_credentials_type=mysql_query($fetch_credentials_type);
					while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
						{
							$camount=$camount+$fetch_credentials['amount'];
						}
					//query calculate total amount of discount
					$total_discount=0; 
					$fetch_discount_query="SELECT fee_discount_credentials.type,fee_discount_credentials.amount
											
											FROM fee_discount_credentials
											
											INNER JOIN fee_details
											ON fee_details.fee_discount_id=fee_discount_credentials.id
											
											WHERE fee_details.student_id = ".$fetch['student_id']."
											AND fee_discount_credentials.session_id=".$current_session."
											AND fee_details.fee_generated_session_id=".$fetch_dues[0]."";
					$execute_discount_type=mysql_query(  $fetch_discount_query);
					while($fetch_discount=mysql_fetch_array($execute_discount_type))
						{ 
							$total_discount=$total_discount+$fetch_discount['amount'];
						}
					//calculate tota fee amounts
					$total_fee_amount=$camount-$total_discount+$trans_rent_cost;
					//calculate fee amount according to duration
					//Monthly=1  Quarterly-3, half early=6 and annually=12
					$m=1;
					if($month=="Monthly")
						{
							$m=1; 
						}
					else if($month=="Quarterly")
						{
							$m=3; 
						}
					else if($month=="Half-Yearly")
						{
							$m=6;  
						}	
					else if($month=="Yearly")
						{
							$m=12;  
						}
					$paid_fee= $total_fee_amount*$m;										 
					echo'  <td  width="9%">'.$fetch_session['session_name'].'</td>
					<td width="11%">'.$fetch_dues['duration'].'</td>
					';
					//query to get date on the id of date
					$get_date_of_id=
									"SELECT DISTINCT date
									FROM dates_d
									WHERE date_id = ".$fetch_dues['last_date']."";
					$exe_date=mysql_query($get_date_of_id);
					$fetch_date=mysql_fetch_array($exe_date);
					$due_date = $fetch_date[0];
					echo '			
					<td width="12%">'.$due_date.'</td>
					<td width="16%">'.$camount.'</td>
					<td width="14%">'.$hostel_rent.'</td>';
										//calculate fine
					//get current date and show fine
					date_default_timezone_set('Asia/Kolkata');
		             $current_date=date('Y-m-d');
					  $current_id="SELECT date_id FROM dates_d
					             WHERE date='".$current_date."'";
					 $exe_current=mysql_query($current_id);
					 $fetch_current_date=mysql_fetch_array($exe_current);
					 //get paid on
					$paid_4="SELECT paid, paid_on
								 FROM fee_details
								 WHERE student_id=".$fetch['student_id']."
								 AND fee_generated_session_id=".$fetch_dues[0]."";
						$exe_paid_4=mysql_query($paid_4);
						$fetch_paid_4=mysql_fetch_array($exe_paid_4);
					 //if condition check fee paid then fine calculate paid on date else calculate asia colkata
					 if($fetch_paid_4[0] == 4)
					 {
						 
						 $paid_on_fine_date =$fetch_paid_4[1];
					 }
					 else
					 {
						 $paid_on_fine_date = $fetch_current_date[0];
						 
					 }
					if($fetch_dues['last_date']<$paid_on_fine_date)
					  { 
						 $fine_days = $paid_on_fine_date - $fetch_dues['last_date'];
						if($fine_days<=30)
						{
							$calculate_fine=100;
						}
						else if($fine_days<=60)
						{
							$calculate_fine=200;
						}
						else
						{
							$calculate_fine=500;
						}
						/*$calculate_fine=$fine_days*$fine; */
					}
				else
					{
						$calculate_fine=0;  
					}
					$paid_fee_with_fine = $paid_fee + $calculate_fine;
					echo '<td width="12%">'.$calculate_fine.'</td>
					<td width="12%">'.$paid_fee_with_fine.'</td>
					';
					if($fetch_dues['paid']==3)
					{
				/*	echo '<td class=" " width="11%">
					<span class="tip"><a class="uibutton " href="accounts_fee_cheque_clear.php?id='.$fetch['student_id'].'
					&name='.$fetch['Name']
					.'&f_g_sid='.$fetch_dues['fee_generated_session_id'].'" original-title=""> Pending</a></span> 
					</td>';*/
					echo '<td width="12%"><a href="accounts_fee_cheque_details.php?add='.$fetch['admission_no'].'&name='.$fetch['Name'                    ].'&sid='.$fetch['sId'].'&duid='.$fetch_dues[0].'">View</a></td></tr>';
					}
					 else 
					{
					echo '<td class=" " width="11%">
					<span class="tip"><a class="uibutton " href="accounts_fee_paid.php?id='.$fetch['student_id'].'
					&name='.$fetch['Name']
					.'&f_g_sid='.$fetch_dues['fee_generated_session_id'].'" original-title=""> Pay</a></span> 
					</td>
					<td width="12%"><a href="accounts_fee_cheque_details.php?add='.$fetch['admission_no'].'&name='.$fetch['Name'                    ].'&sid='.$fetch['sId'].'&duid='.$fetch_dues[0].'">View</a></td></tr>';
					}
					
					
				}
				}
				//loop closed
				
				echo'</table></td></tr>';
				
		}
		//loop closed
		
	echo' </tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}

//function to show fee paid students
function fee_cheque_clear()
{
	echo '
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Paid of Students</span><span style="color:red"> '.$_GET['name'].'</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly add   <span class="netip"><a  class="red" > required Fee details  
	</a></span>
	</div>
	';	
    if (isset($_GET['clear']))
		{
			echo '<h4 align="center"><span style="color:green">FEE PAID SUCCESSFULLY!</h4></span>';
		}	  
	echo'        
	<form id="demo"  action="fee/fee_cheque_clear.php" method="post"> 
	<div> <input type="hidden" name="id" value="'.$_GET['id'].'"/>
	<input type="hidden" name="f_g_sid" value="'.$_GET['f_g_sid'].'">
	<input type="hidden" name="name" value="'.$_GET['name'].'">
	</div>';
	echo '	
	<div class="section">
	<label>Select Cheque Status</label>
	<div>
	<select name="cheque">
	<option value="0">Select Cheque Status</option>	
	<option value="1"> Cheque Clear</option>	';
	echo '<option value="2"> Cheque Bounce</option>';
	echo '</select>
	</div>
	</div>
	  
	<div class="section">
	<label>Cheque Clear Date<small>Select date</small></label>   
	<div>
	<input type="text"  id="birthday" class=" birthday  small " name="date"   />
	</div>
	</div>
	<div class="section">
	<label>Remarks<small>enter remarks</small></label>
	<div>
	<input type="text" name="remarks" maxlength="255">
	</div>
	</div>   
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form" >
	<a class="btn" onclick="ResetForm()" title="Reset  Form">Clear Form</a>
	</div>
	</div>	 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}
//function to show fee paid students
function fee_cheque_clear_level()
{
	echo '
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Paid of Students</span><span style="color:red"> '.$_GET['name'].'</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly add   <span class="netip"><a  class="red" > required Fee details  
	</a></span>
	</div>
	';	
	  if (isset($_GET['clear1']))
		{
			echo '<h4 align="center"><span style="color:green">FEE PAID SUCCESSFULLY!</h4></span>';
		}		  
	echo'        
	<form id="demo"  action="fee/fee_cheque_clear_class.php" method="post"> 
	<div> <input type="hidden" name="id" value="'.$_GET['id'].'"/>
	<input type="hidden" name="f_g_sid" value="'.$_GET['f_g_sid'].'">
	<input type="hidden" name="name" value="'.$_GET['name'].'">
	</div>';
	echo '	
	<div class="section">
	<label>Select Cheque Status</label>
	<div>
	<select name="cheque">
	<option value="0">Select Cheque Status</option>	
	<option value="1"> Cheque Clear</option>	
	
	</select>
	</div>
	</div>	  
	<div class="section">
	<label>Cheque Clear Date<small>Select date</small></label>   
	<div>
	<input type="text"  id="birthday" class=" birthday  small " name="date"   />
	</div>
	</div>
	<div class="section">
	<label>Remarks<small>enter remarks</small></label>
	<div>
	<input type="text" name="remarks" maxlength="255">
	</div>
	</div>   
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form" >
	<a class="btn" onclick="ResetForm()" title="Reset  Form">Clear Form</a>
	</div>
	</div>	 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}
//function to show fee paid students
function fee_cheque_clear_class()
{
	echo '
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Paid of Students</span><span style="color:red"> '.$_GET['name'].'</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly add   <span class="netip"><a  class="red" > required Fee details  
	</a></span>
	</div>
	';	
	  if (isset($_GET['clearc']))
		{
			echo '<h4 align="center"><span style="color:green">FEE PAID SUCCESSFULLY!</h4></span>';
		}		  
	echo'        
	<form id="demo"  action="fee/fee_cheque_clear_level.php" method="post"> 
	<div> <input type="hidden" name="id" value="'.$_GET['id'].'"/>
	<input type="hidden" name="f_g_sid" value="'.$_GET['f_g_sid'].'">
	<input type="hidden" name="name" value="'.$_GET['name'].'">
	</div>';
	echo '	
	<div class="section">
	<label>Select Cheque Status</label>
	<div>
	<select name="cheque">
	<option value="0">Select Cheque Status</option>	
	<option value="1"> Cheque Clear</option>	
	
	</select>
	</div>
	</div>	  
	<div class="section">
	<label>Cheque Clear Date<small>Select date</small></label>   
	<div>
	<input type="text"  id="birthday" class=" birthday  small " name="date"   />
	</div>
	</div>
	<div class="section">
	<label>Remarks<small>enter remarks</small></label>
	<div>
	<input type="text" name="remarks" maxlength="255">
	</div>
	</div>   
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form" >
	<a class="btn" onclick="ResetForm()" title="Reset  Form">Clear Form</a>
	</div>
	</div>	 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}
//function to show fee paid students
function fee_paid()
{
	//Author by: Sagun sir
	
	echo '
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Paid of Students</span><span style="color:red"> '.$_GET['name'].'</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly add   <span class="netip"><a  class="red" > required Fee details  
	</a></span>
	</div>
	';		  
	echo'        
	<form id="demo"  action="fee/fee_paid.php" method="post">'; 
	//form start to get details of fee paid students
	echo '<div> <input type="hidden" name="id" value="'.$_GET['id'].'"/>
	<input type="hidden" name="f_g_sid" value="'.$_GET['f_g_sid'].'">
	<input type="hidden" name="name" value="'.$_GET['name'].'">
	</div>';
	/*	<div class="section">
	<label> Mode of Payment<small>with details(limit 200)</small></label>   
	<div> <input type="text" name="payment" class=" medium" required="required"   ></div>
	</div>*/
	echo '         
	<div class="section">
	<label> Mode of Payment<small>Select mode of payments</small></label>   
	<div>
	<select data-placeholder="Select mode of payments" class="chzn-select" name="payment" tabindex="2" >
	<option value=""></option> 
	<option value="cash">cash</option> 
	<option value="cheque">cheque</option>
	<option value="DD">DD</option>
	</select>
	</div>
	</div>';
	echo '		  
	<div class="section">
	<label>Date<small>Select date</small></label>   
	<div>
	<input type="text"  id="birthday" class=" birthday  small " name="date"   />
	</div>
	</div>
	<div class=section>
	<label>Remarks<small>Enter remark</small></label>
	<div>
	<input type="text" name="remark" maxlength="255">
	</div>
	</div>   
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form" >
	<a class="btn" onclick="ResetForm()" title="Reset  Form">Clear Form</a>
	</div>
	</div>	 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}
//function to show fee paid students
function fee_paid_class()
{
	//Author by: Sagun sir
	 $cid=$_GET['cid'];
	echo '
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Paid of Students</span><span style="color:red"> '.$_GET['name'].'</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly add   <span class="netip"><a  class="red" > required Fee details  
	</a></span>
	</div>
	';		  
	echo'        
	<form id="demo"  action="fee/fee_paid_class_insert.php?cid='.$cid.'" method="post"> 
	<div> <input type="hidden" name="id" value="'.$_GET['id'].'"/>
	<input type="hidden" name="f_g_sid" value="'.$_GET['f_g_sid'].'">
	<input type="hidden" name="name" value="'.$_GET['name'].'">
	
	</div>';
	/*	<div class="section">
	<label> Mode of Payment<small>with details(limit 200)</small></label>   
	<div> <input type="text" name="payment" class=" medium" required="required"   ></div>
	</div>*/
	echo '         
	<div class="section">
	<label> Mode of Payment<small>Select mode of payments</small></label>   
	<div>
	<select data-placeholder="Select mode of payments" class="chzn-select" name="payment" tabindex="2" >
	<option value=""></option> 
	<option value="cash">cash</option> 
	<option value="cheque">cheque</option>
	<option value="DD">DD</option>
	</select>
	</div>
	</div>';
	echo '		  
	<div class="section">
	<label>Date<small>Select date</small></label>   
	<div><input type="text"  id="birthday" class=" birthday  small " name="date"   /></div>
	</div>
	<div class=section>
	<label>Remarks<small>Enter remark</small></label>
	<div>
	<input type="text" name="remarks" maxlength="255">
	</div>
	</div>      
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form" >
	<a class="btn" onclick="ResetForm()" title="Reset  Form">Clear Form</a>
	</div>
	</div>	 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}

//function to show fee paid students
function fee_paid_level()
{
	//Author by: Sagun sir
	$level=$_GET['level'];
	echo '
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Paid of Students</span><span style="color:red"> '.$_GET['name'].'</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly add   <span class="netip"><a  class="red" > required Fee details  
	</a></span>
	</div>
	';		  
	echo'        
	<form id="demo"  action="fee/fee_paid_level_insert.php?level='.$level.'" method="post"> 
	<div> <input type="hidden" name="id" value="'.$_GET['id'].'"/>
	<input type="hidden" name="f_g_sid" value="'.$_GET['f_g_sid'].'">
	<input type="hidden" name="name" value="'.$_GET['name'].'">
	<input type="hidden" name="level" value="'.$level.'">
	</div>';
	/*	<div class="section">
	<label> Mode of Payment<small>with details(limit 200)</small></label>   
	<div> <input type="text" name="payment" class=" medium" required="required"   ></div>
	</div>*/
	echo '         
	<div class="section">
	<label> Mode of Payment<small>Select mode of payments</small></label>   
	<div>
	<select data-placeholder="Select mode of payments" class="chzn-select" name="payment" tabindex="2" >
	<option value=""></option> 
	<option value="cash">cash</option> 
	<option value="cheque">cheque</option>
	<option value="DD">DD</option>
	</select>
	</div>
	</div>';
	echo '		  
	<div class="section">
	<label>Date<small>Select date</small></label>   
	<div><input type="text"  id="birthday" class=" birthday  small " name="date"   /></div>
	</div> 
	<div class=section>
	<label>Remarks<small>Enter remark</small></label>
	<div>
	<input type="text" name="remarks" maxlength="255">
	</div>
	</div>    
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form" >
	<a class="btn" onclick="ResetForm()" title="Reset  Form">Clear Form</a>
	</div>
	</div>	 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}

function fee_details()
{
	//Author by: Sagun sir
	echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span> Generate Fee (Class wise)</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	if(isset($_GET['added']))
		{
			echo'<h4 align="center" style="color:green">SELECTED FEE AND DISCOUNT CREDENTIALS HAS BEEN ASSIGNED TO SELECTED 
			CLASS SUCCESSFULLY!</h4>    ';
		}	
	if(isset($_GET['error']))
		{
			echo'<h4 align="center" style="color:RED">ERROR!!</h4>    ';
		}	
	if(isset($_GET['exit']))
		{
			echo'<h4 align="center" style="color:RED">NOTE : ALLREADY GENERATE THIS FEE!</h4>    ';
		}	
	echo'
	<form id="form1" name="form1" action="fee/assign_credentials.php" method="POST" />
	<input type="hidden" name="issubmit" value="1" />
	<!-- Smart Wizard -->
	<div id="wizardvalidate" class="swMain">
	<ul>
	<li><a href="#step-1">
	<label class="stepNumber">1</label>
	<span class="stepDesc">SELECT DURATION <br />
	<small>(current session)</small>
	</span>
	</a></li>
	<li><a href="#step-2">
	<label class="stepNumber">2</label>
	<span class="stepDesc">SELECT CLASS<br />
	<br/>
	</span>
	</a></li>
	<li><a href="#step-3">
	<label class="stepNumber">3</label>
	<span class="stepDesc">FEE CREDENTIALS<br />
	<small><br/></small>
	</span>
	</a></li>
	<li><a href="#step-4">
	<label class="stepNumber">4</label>
	<span class="stepDesc">CONFIRMATION<br/><br/></span>
	</a></li>
	</ul>
	<div id="step-1" style="width:100%;">	
	<h2   class="StepTitle">Select Duration</h2>';
	//query to get fee generaton session id and duration
	$sql="SELECT Id, duration
		  FROM fee_generated_sessions
		  ";             
	$execute=mysql_query($sql);
	echo'<div  align="center">
	<div style="height:150px">
	<select onblur="confirm_duration(this.value);
	" onchange="populateclass(this.value)" required="required" id="select_w_box">
	<option value=""></option>
	';
	while($fetch=mysql_fetch_array($execute))
		{ 
                    echo' <option value='.$fetch['Id'].' id="'.$fetch['duration'].'" ><strong>'.$fetch['duration'].'</strong></option>';
		}
	echo'</select>
	</div>
	</div>
	</div>
	<div id="step-2" style="width:100%;">
	<h2 class="StepTitle">Step 2: SELECT CLASS</h2>	
	<div id="populateclass" style="height:150px">
	<h4 style="color:red" align="center">KINDLY SELECT DURATION FROM PREVIOUS STEP  FIRST !</h4>  
	</div>    </div>          	   
	<div id="step-3" style="width:100%;">
	<h2 class="StepTitle">Step 3: SELECT FEE CREDENTIALS</h2>	
	<div id="populatecredentials" style="height:100%">
	<h4 style="height:150px;color:red" align="center">KINDLY SELECT CLASS FROM PREVIOUS STEP FIRST !</h4> 
	</div>
	</div>
	<div id="step-4">';
	//get name for that duration on that id
	echo '
	<h2 class="StepTitle">Step 4: CONFIRMATION</h2> 	
	<h5 style="color:red">ARE YOU SURE WANT TO ALLOCATE SELECTED FEE CREDENTIALS  FOR THE SELECTED CLASS ??</h5>				
	<label>DURATION</label>			
	<input type="text"  value="" id="name_session" name="name_session" />
	<input type="hidden" value="" id="session_selected" name="duration"/><br/>
	<label>CLASS</label>			
	<input type="text" value="" name="class" id="class_selected" /><br/>
	<label>FEE CREDENTIALS</label>			
	<textarea row="4" column="50" name="fee" value="" id="fee_credentials" /></textarea>
	<input type="hidden" value="" name="fee_h" id="fee_credentials_h" />
	<label>DISCOUNT FEE CREDENTIALS</label>	
	<textarea row="4" column="50" name="discount" value="" id="fee_discount_credentials" /></textarea>
	<input type="hidden" value="" name="discount_h" id="fee_discount_credentials_h" />			
	</form>
	</div>
	</div><!-- End SmartWizard Content -->  		
	</form> 		
	<div class="clearfix"></div>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';	
}
//generate fee multiple class
function accounts_generate_fee_multiple_class()
{
    echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">

	<div class="widget-header">
	<span>Generate Fee Multiple Class</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->

	';	
	  //get duration
	$du="SELECT * FROM fee_generated_sessions";
        $exe_du=mysql_query($du);
        //getclass
        $cl="SELECT * FROM class_index";
        $exe_cl=mysql_query($cl);
        //get credentials details
         $fee="SELECT * FROM fee_credentials";
        $exe_fee=mysql_query($fee);
	echo'
       <form action="fee/generate_fee_multiple_class.php" method="get">
            <div class="section">
                <label> Select Duration <small></small></label>   
                <div> 
                  <select  class="chzn-select " multiple tabindex="4" name="duration[]">
                    <option value=""></option> 
                    <option value="-1">April - March</option>';
                     
                 while($fetch_du=mysql_fetch_array($exe_du))
                    {
                     
                     echo '  <option value="'.$fetch_du[0].'" >'.$fetch_du[2].'</option> ';

                    }
                  echo '  
                  </select></div>
                 </div><br>
                <div class="section">
                <label> Select Class <small></small></label>   
                <div> 
                  <select  class="chzn-select " multiple tabindex="4" name="class[]">
                    <option value=""></option> 
                    <option value="">class 1-5</option> 
                    <option value="">class 6-8</option>
                    <option value="">class 9-10</option> ';
                 while($fetch_cl=mysql_fetch_array($exe_cl))
                    {
                     
                     echo '  <option value="'.$fetch_cl[0].'" >'.$fetch_cl[1].'</option> ';

                    }
                  echo '  
                  </select></div>
               </div><br>
            <div class="section">
                <label>Select Fee <small></small></label>   
                <div> 
                  <select  class="chzn-select " multiple tabindex="4" name="fee[]">
                    <option value=""></option> ';
                 while($fetch_fee=mysql_fetch_array($exe_fee))
                    {
                     
                     echo '  <option value="'.$fetch_fee[0].'" >'.$fetch_fee[1].'('.$fetch_fee[2].')</option> ';

                    }
                 echo ' </select></div>
               </div>  <br>   
               <div class="section last">
               <div>
<button class="uibutton confirm large" align="center">Generate</button>
</div></div>
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
    
}
//function to show fee generating by accounts Monthly
function generate_fee()
{
	//Author by: Anil sir
	echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Generate Fee</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly Enter  <span class="netip"><a  class="red" > Student\'s Admission
	 Number  </a></span>
	</div>
	';		  
	if(isset($_GET['added']))
		{
			echo '<h4 align="center"><span style="color:green">Fee Generated Successfully!</span></h4>';
		}
	if(isset($_GET['error']))
		{
			echo '<h4 align="center"><span style="color:red">Error!</span></h4>';
		}
	echo'        
	<div class="section">
	<label>Admission No.</label>   
	<div> 
	<input type="text" name="admission_no" id="admission_no"  class=" medium" onkeyup="generate_fee(this.value)" autofocus/>
	</div>
	</div>
	<div id="generate">
	</div>
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}
//in this function select duration 
function fee_paid_details_receipt()
{
	//Author by: Sagun sir
	$current_session=$_SESSION['current_session_id'];
	$query_duration="SELECT duration, Id
					FROM fee_generated_sessions
					WHERE session_id=".$current_session;
	$execute_duration=mysql_query($query_duration);	
	echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Generate Fee</span>
	</div><!-- End widget-header -->
	<div class="widget-content">	';
	echo'      
	<form id="demo"  action="accounts_fee_paid_details.php" method="post"> 
	<div class="section">
	<label>Duration<small>Select duration</small></label>   
	<div> 
	<select  name="du" class=" medium" />';
	while($fetch_du=mysql_fetch_array($execute_duration))
		{		
			echo '
			<option value="'.$fetch_du[1].'">'.$fetch_du[0].'</option>';
		}
	echo '		
	</select>
	</div>
	</div>
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form" >
	</div>
	</div>
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';	

}
//function show all classes of fee receipt
function fee_paid_details()
{
	//Author by: Sagun sir
	$duration_id=$_POST['du'];
	echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Print Fee Receipt</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly Enter  <span class="netip"><a  class="red" > Student\'s
    Admission Number  </a></span>
	</div>
	';		  
	echo'       
	<div class="section">
	<label>Admission No.</label>   
	<div> 
	<input type="text" name="admission_no" id="admission_no"  class=" medium" onkeyup="student_fee_detail(this.value,
	'.$duration_id.')" />
	</div>
	</div>
	<div id="myfeeDiv">
	</div>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';	
}

//function to show receipt of class of fee paid students 
function fee_paid_details_receipt_class()
{
	//Author by: Sagun sir
	$current_session=$_SESSION['current_session_id'];
	$query_duration="SELECT duration, Id
					FROM fee_generated_sessions
					WHERE session_id=".$current_session;
	$execute_duration=mysql_query($query_duration);	
	//get name of the school
	$get_name_school=
					"SELECT `name_school`
					FROM `school_names`
					WHERE `Id`= 3";
	$exe_name_school=mysql_query($get_name_school);
	$fetch_school_name=mysql_fetch_array($exe_name_school);
	$school_name=$fetch_school_name['name_school'];
	echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>'. $school_name.'</span>
	</div><!-- End widget-header -->
	<div class="widget-content">	';
	echo'       
	 <form id="demo"  action="accounts_full_class_fee_print.php" method="post"> 
	<div class="section">
	<label>Duration<small>Select duration</small></label>   
	<div> 
	<select  name="du" class=" medium" />';
	while($fetch_du=mysql_fetch_array($execute_duration))
		{		
			echo '	<option value="'.$fetch_du[1].'">'.$fetch_du[0].'</option>';
		}
	echo '		
	</select>
	</div>
	</div>
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form" >
	</div>
	</div>
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}
//function print fee receipt of full class show
function full_class_fee_print()
{
	//Author by: Sagun sir
	$duid=$_POST['du'];	
	//get name of the school
	$get_name_school=
					"SELECT `name_school`
					FROM `school_names`
					WHERE `Id`= 3";
	$exe_name_school=mysql_query($get_name_school);
	$fetch_school_name=mysql_fetch_array($exe_name_school);
	$school_name=$fetch_school_name['name_school'];	
	echo '	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i> Kindly Select class</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<br/><br/>	
	';
	$q="SELECT DISTINCT `cId`,`class_name` FROM `class_index` ORDER BY `class_name` ASC";
	$q_res=mysql_query($q);
	echo '
	';
	while($res=mysql_fetch_array($q_res))
		{
			$_SESSION["id"]=$res[0];
			echo '
			&nbsp;&nbsp;<a  href="accounts_class_fee_print.php?cId='.$res['cId'].'&duid='.$duid.'"><div  class="shoutcutBox">
			'.$res[1].'
			</div></a>
			';
		}
	
	echo "</div></div></div>";
}
//print fee receipt of all class
function class_fee_print()
{
	//Author by: Anil Kumar *
	//get all the admission numbers
	$duid=$_GET['duid'];	
	$class_id=$_GET['cId'];	
	$query_class_ad="SELECT admission_no , COUNT(admission_no)
					
					FROM student_user
					
					INNER JOIN class
					ON student_user.sId = class.sId
					
					INNER JOIN class_index
					ON class.classId = class_index.cId
					
					WHERE cId='$class_id'
					ORDER BY admission_no ASC";
	$execute_class_ad=mysql_query($query_class_ad);
	$fetch_ad=mysql_fetch_array($execute_class_ad);
	$ad_no_count = $fetch_ad[1];
	$query_class="SELECT admission_no 
				
				  FROM student_user
				 
				  INNER JOIN class
				  ON student_user.sId = class.sId
				
				  INNER JOIN class_index
				  ON class.classId = class_index.cId
				
				  WHERE cId='$class_id'
				  ORDER BY admission_no ASC";
	$execute_class=mysql_query($query_class);
	echo '<div><button type="button" class="uibutton icon next" onclick="print_this_all_cls('.$ad_no_count.')">Print all
	</button></div>
	<div id="cls_full">';
	$prn=0;
	while($fetch_class=mysql_fetch_array($execute_class))			   
		{
			$query="SELECT * , class_name
					
					FROM student_user
					
					INNER JOIN class
					ON student_user.sId = class.sId
					
					INNER JOIN class_index
					ON class.classId = class_index.cId
					WHERE admission_no = '".$fetch_class['admission_no']."'
					";
			$execute=mysql_query($query);
			$fetch=mysql_fetch_array($execute);
			echo' 
			<!-- Widget -->';
			//query to get generation type
			$type="SELECT type
					FROM fee_generated_sessions
					WHERE Id=".$duid."";
			$execute_du=mysql_query($type);
			$fetch_duid=mysql_fetch_array($execute_du);
			$month=$fetch_duid['type'];  
			//calculate fee amount according to duration
			//Monthly=1  Quarterly-3, half early=6 and annually=12
			$m=1;
			if($month=="Monthly")
				{
					$m=1; 
				}
			else if($month=="Quarterly")
				{
					$m=3; 
				}
			else if($month=="Half-Yearly")
				{
					$m=6;  
				}	
			else if($month=="Yearly")
				{
					$m=12;  
				} 
			//JR. Er.Anil's code
			$student_id = $fetch['sId'];
			$current_session=$_SESSION['current_session_id'];
			//Query to get the class of the user student
			$query_class_student = "SELECT class_index.class_name
									FROM class_index
									INNER JOIN class
									ON class.classId = class_index.cId
									WHERE class.sId = '".$fetch['sId']."'
									";
			$execute_class_student = mysql_query($query_class_student);
			$class_student = mysql_fetch_array($execute_class_student);
			//write query to get the session id from  the fee generated session
			$fetch_session_name="SELECT `session_name`
								FROM `session_table` 
								WHERE `sId`=".$current_session."";
			$execute_session_name=mysql_query($fetch_session_name);
			$fetch_session=mysql_fetch_array($execute_session_name);
			$session_name=$current_session['session_name'];
			$class_name = $fetch['class_name'];
			$total_discount=0;
			$finee="fine";
			$fine=0;
			$camount=0;	
			$serial_no=1;
					////query to get students fee details from fee generation 
			$get_detail=
						"SELECT DISTINCT fee_generated_sessions.duration
					
						, fee_generated_sessions.last_date , fee_details.paid , fee_generated_sessions.Id ,fee_details.paid_on
						
						FROM fee_details
						
						INNER JOIN fee_generated_sessions
						
						ON fee_generated_sessions.id = fee_details.fee_generated_session_id 
						
						WHERE fee_details.student_id = '".$fetch['sId']."'
						AND fee_generated_sessions.session_id=".$current_session."
						AND fee_details.fee_generated_session_id=".$duid."";
			$exe_details=mysql_query($get_detail);
			$fetch_details=mysql_fetch_array($exe_details);
			//get name of the school
			$get_name_school=
							"SELECT `name_school`
							FROM `school_names`
							WHERE `Id`= 3";
			$exe_name_school=mysql_query($get_name_school);
			$fetch_school_name=mysql_fetch_array($exe_name_school);
			$school_name=$fetch_school_name['name_school'];
			echo'
			<div class="row-fluid"><br/><br/><br/>
			<!-- Table widget -->
			<div class="widget  span12 clearfix">
			<div class="widget-header">
			<span>FEE RECEIPT of <b style="color:red">'.$fetch['Name'].' ('.$fetch['admission_no'].')</b></span>
			</div><!-- End widget-header -->	
			<div class="widget-content">
			<div  id="cls_'.$fetch['admission_no'].'">
			';
			echo '<div align="center"><h3 style="color:green">school</h3></div>';
			/*echo '<div align="left"><h6>Student : '.$fetch['Name'].'</h6></div>
			<h6>Class : '.$class_name.'</h6>';*/
			
			echo '<h5 align="center"> Current Session : '.$fetch_session['session_name'].'</h5>
			<h5 align="center"> Duration : '.$fetch_details[0].'</h5>
			<h6 align="center">Class : '.$class_name.'</h6>
			<table  class="table table-bordered table-striped">';
			echo '    
			<thead>
			<tr>
			<th align="center" width="5%">S.No.</th>
			<th align="left" width="50%">Credential Type</th>
			<th align="left" width="45%">Amount</th>
			</tr>
			</thead>
			<tbody>';
	//QUERY get cost of transport from destination_rouite------
	$transport_cost=0;
	$trans_amount="SELECT cost
	               FROM destination_route
				   
				   INNER JOIN user_vehicle_details
				   ON user_vehicle_details.did=destination_route.did
				   
				   WHERE user_vehicle_details.uId=$student_id
				   AND destination_route.session_id=".$_SESSION['current_session_id']."";
	$exe_amount_t=mysql_query($trans_amount);
	while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
		{
			$transport_cost=$transport_cost+$fetch_transport_cost[0];
		}
	//query get rent of hostel from room allocation --------
	$hostel_rent=0;
	$hostel_amount="SELECT rent 
	                FROM hostel_room_allocation
					WHERE sid=$student_id
					AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
	$exe_rent=mysql_query($hostel_amount);
	while($fetch_rent_cost=mysql_fetch_array($exe_rent))
		{
			$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
		}
	$trans_rent_cost=$hostel_rent+$transport_cost;
			//start query to fetch credential type
			$fetch_credentials_type="SELECT DISTINCT fee_credentials.type,fee_credentials.amount
									
									FROM fee_credentials
									
									INNER JOIN fee_details
									ON fee_details.fee_credential_id=fee_credentials.id
									
									WHERE student_id=".$fetch['sId']."
									AND fee_credentials.session_id=".$current_session."
									AND fee_details.fee_generated_session_id=".$duid."";
			$execute_credentials_type=mysql_query($fetch_credentials_type);
			while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
				{
					$camount=$camount+$fetch_credentials['amount'];
					//first latter of credential type convert small to capital
					$c=str_split($fetch_credentials['type']);
					$upper=strtoupper($c[0]);
					$count=count($c);
					echo '	<tr>
					<td align="center">'.$serial_no++.'</td>
					<td>';
					echo $upper;
					for($i=1;$i<$count;$i++)
						{
							echo $c[$i];
						}
					echo '	</td>
					<td>'.$fetch_credentials['amount'].'</td></tr>';  
				}
						  echo '<tr>
						  <td align="center">'.$serial_no++.'</td>
						  <td>Transport</td>
						  <td>'.$transport_cost.'</td></tr>
						  ';
						   echo '<tr>
						   <td align="center">'.$serial_no++.'</td>
						  <td>Hostel Rent</td>
						  <td>'.$hostel_rent.'</td></tr>
						  ';
			echo ' <table  class="table table-bordered table-striped">
			<thead>
			<tr>
			<th align="center" width="5%">S.No.</th>
			<th align="left" width="50%">Discount Type</th>
			<th align="left" width="45%">Amount</th>
			</tr>
			</thead>
			<tbody>';
			//query to fetch discoun type of student AND AMOUNT
			$serial_no=1;
			$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
									
									FROM fee_discount_credentials
									
									INNER JOIN fee_details
									ON fee_details.fee_discount_id=fee_discount_credentials.id
									
									WHERE fee_details.student_id=".$fetch['sId']."
									AND fee_discount_credentials.session_id=".$current_session."
									AND fee_details.fee_generated_session_id=".$duid."";
			$execute_discount_type=mysql_query(  $fetch_discount_query);
			while($fetch_discount=mysql_fetch_array($execute_discount_type))
				{ 
					$total_discount=$total_discount+$fetch_discount['amount'];
					//first latter of discount type convert small to capital
					$d=str_split($fetch_discount['type']);
					$upper=strtoupper($d[0]);
					$c=count($d);
					echo '<tr>
					<td align="center">'.$serial_no++.'</td>
					<td>
					';
					echo $upper;
					for($i=2;$i<$c;$i++)
						{
							echo $d[$i];
						}
					echo '</td>
					<td>'.$fetch_discount['amount'].'</td>
					</tr>';
				}
			echo '
			<table  class="table table-bordered table-striped" 
			</thead>
			<tbody>';
			//write query to fetch fine amount
			$query_fine="SELECT `amount` 
						FROM `fee_credentials`
						WHERE  `type`='".$finee."'";
			$execute_fine=mysql_query($query_fine);
			$fetch_fine=mysql_fetch_array($execute_fine);
			$fine=$fetch_fine['amount'];
			////query to get students fee details from fee generation 
			$get_detail=
						"SELECT DISTINCT fee_generated_sessions.duration
					
						, fee_generated_sessions.last_date , fee_details.paid , fee_generated_sessions.Id ,fee_details.paid_on
						
						FROM fee_details
						
						INNER JOIN fee_generated_sessions
						
						ON fee_generated_sessions.id = fee_details.fee_generated_session_id 
						
						WHERE fee_details.student_id = '".$fetch['sId']."'
						AND fee_generated_sessions.session_id=".$current_session."
						AND fee_details.fee_generated_session_id=".$duid."";
			$exe_details=mysql_query($get_detail);
			$fetch_details=mysql_fetch_array($exe_details);
			$due_date =$fetch_details['last_date'];
			$paid_date=$fetch_details['paid_on'];
			//query to get date on the id of date
			/*$get_date_of_id=
			"SELECT date
			FROM dates_d
			WHERE date_id = ".$fetch_details['last_date']."";
			$exe_date=mysql_query($get_date_of_id);
			$fetch_date=mysql_fetch_array($exe_date);
			$last_date = $fetch_date;*/                          
			//check condition due date < paid on date
			if($due_date<$paid_date)
				{
					$fine_days=$paid_date-$due_date;
						if($fine_days<=30)
						{
							$calculate_fine=100;
						}
						else if($fine_days<=60)
						{
							$calculate_fine=200;
						}
						else
						{
							$calculate_fine=500;
						}
				}
			else
				{
					$calculate_fine=0;  
				}
			$to_creden_amunt=$camount+$trans_rent_cost;
						    
			//calculate total fee
			$tfamount= $to_creden_amunt-$total_discount;
			//calculate fee accourding to duration 
			$tfamounttype=$tfamount*$m;
			//calculate fee with fine
			$tfamount=$tfamounttype+$calculate_fine;
			$fee=$to_creden_amunt-$total_discount;
			echo '  <tr>
		    <td width="55%"><b style="color:green">Total Credentials Amount</b></td><td><b style="color:green">
			'.$to_creden_amunt.'
			</b></td>
			</tr>
			<tr>
			<td><b style="color:green">Total Discount</b></td><td><b style="color:green">'.$total_discount.'</b></td>
			</tr>
			<tr>
			<td><b style="color:green">Fee</b></td><td><b style="color:green">'.$fee.'</b></td>
			</tr>
			<tr>
			<td><b style="color:green">Late Fine</b></td><td><b style="color:green">'. $calculate_fine.'</b></td>
			</tr>
			<tr>
			<td><h4 style="color:red">Total Fee</h4></td><td><h4 style="color:red">'.$tfamount.'</h4></td>
			</tr>
			</tbody>
			</table></tbody>
			</table>';
			//start query to fetch credential type				              
			$fetch_credentials_type="SELECT DISTINCT(fee_credentials.Id) ,fee_credentials.type,fee_credentials.amount
									FROM fee_credentials
									INNER JOIN fee_details
									ON fee_details.fee_credential_id=fee_credentials.id
									WHERE student_id=".$fetch['sId']."
									AND fee_credentials.session_id=".$current_session."
									AND fee_details.fee_generated_session_id=".$duid."";
			$execute_credentials_type=mysql_query($fetch_credentials_type);
			while($fetch_credentials=mysql_fetch_array($execute_credentials_type))
				{
					$camount=$camount+$fetch_credentials['amount'];
					/*echo '	<tr>
					<td>'.$fetch_credentials['type'].'</td>
					<td>'.$fetch_credentials['amount'].'</td></tr>';  */
				}
			//query to fetch discountype of student
			$fetch_discount_query="SELECT DISTINCT  (fee_discount_credentials.Id)
								, fee_discount_credentials.type,fee_discount_credentials.amount
								FROM fee_discount_credentials
								INNER JOIN fee_details
								ON fee_details.fee_discount_id=fee_discount_credentials.id
								WHERE fee_details.student_id=".$fetch['sId']."
								AND fee_discount_credentials.session_id=".$current_session."
								AND fee_details.fee_generated_session_id=".$duid."";
			$execute_discount_type=mysql_query(  $fetch_discount_query);
			while($fetch_discount=mysql_fetch_array($execute_discount_type))
				{ 
					$total_discount=$total_discount+$fetch_discount['amount'];
					/*  echo '<tr>
					<td>'.$fetch_discount['type'].'</td>
					<td>'.$fetch_discount['amount'].'</td>
					</tr>';*/
				}
			/*	echo '<tr>
			<td>Total Credentials Amount</td>
			<td>'.$camount.'</td></tr>
			<tr>
			<td>Total Discount</td>
			<td>'. $total_discount.'</td></tr>'; */ 
			$total_fee_amount=$camount-$total_discount;
			/*  	echo '	<tr>
			<td>Total Fee</td>
			<td>'.  $total_fee_amount.'</td></tr>';  */
			//write query to fetch fine amount
			$query_fine="SELECT  `amount` 
						FROM `fee_credentials`
						WHERE  `type`='".$finee."'
						AND fee_credentials.session_id=".$current_session."";
			$execute_fine=mysql_query($query_fine);
			$fetch_fine=mysql_fetch_array($execute_fine);
			$fine=$fetch_fine['amount'];
			//query to get students fee details from fee generation 
			$get_detail=
						"SELECT DISTINCT fee_generated_sessions.duration
						
						, fee_generated_sessions.last_date , fee_details.paid , fee_generated_sessions.Id ,fee_details.paid_on
						
						FROM fee_details
						
						INNER JOIN fee_generated_sessions
						
						ON fee_generated_sessions.id = fee_details.fee_generated_session_id 
						
						WHERE fee_details.student_id = ".$fetch['sId']."
						AND fee_generated_sessions.session_id=".$current_session."
						AND fee_details.fee_generated_session_id=".$duid."";
			$exe_details=mysql_query($get_detail);
			$fetch_details=mysql_fetch_array($exe_details);
			$due_date =$fetch_details['last_date'];
			$paid_date=$fetch_details['paid_on'];
			//query to get date on the id of date
			/*$get_date_of_id=
			"SELECT date
			FROM dates_d
			WHERE date_id = ".$fetch_details['last_date']."";
			$exe_date=mysql_query($get_date_of_id);
			$fetch_date=mysql_fetch_array($exe_date);
			$last_date = $fetch_date;*/ 
									 
			//check condition due date < paid on date
			//and calculate fine
			if($due_date<$paid_date)
				{
					$fine_days=$paid_date-$due_date;
						if($fine_days<=30)
						{
							$calculate_fine=100;
						}
						else if($fine_days<=60)
						{
							$calculate_fine=200;
						}
						else
						{
							$calculate_fine=500;
						}
				}
			else
				{
					$calculate_fine=0;  
				}
			//calculate fee with fine
			$pay_amount=$total_fee_amount+ $calculate_fine;
			/*  echo ' 
			<tr> <td>Fine</td>
			<td>'. $calculate_fine.'</td></tr>
			<tr><td>Paid Amount</td>
			<td>'.$pay_amount.'</td>
			</tr>	';	
			echo ' 
			';*/ 
			//write query to select all fee details if fee not paid		
			$get_detail=
						"SELECT DISTINCT fee_generated_sessions.duration,fee_generated_sessions.last_date , 
						fee_details.paid , fee_generated_sessions.Id ,fee_details.paid_on 
						FROM fee_details
						
						INNER JOIN fee_generated_sessions
						ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
						
						WHERE fee_details.student_id = ".$fetch['sId']."
						AND fee_generated_sessions.session_id=".$current_session."
						AND fee_details.fee_generated_session_id=".$duid."";
			$exe_details=mysql_query($get_detail);
			$ch=array();
			$paid=array();
			$duration=array();
			echo '<tr>';
			$fetch_details=mysql_fetch_array($exe_details);
			if($fetch_details[1] == "")
				{
					$last_date =0;	
				}
			else
				{
					$last_date = $fetch_details[1];	
				}
			//query to get date on the id of date
			$get_date_of_id=
							"SELECT date
							FROM dates_d
							WHERE date_id = ".$last_date."";
			$exe_date=mysql_query($get_date_of_id);
			$fetch_date=mysql_fetch_array($exe_date);
			$due_date = $fetch_date[0];
			$dates=date('d-m-Y',strtotime($due_date));
			//query to get paid on date
			/*	$paidondate="SELECT DISTINCT `paid_on`
			FROM `fee_details`
			WHERE `student_id`=".$_SESSION['user_id']."
			AND `fee_generated_session_id`=".$ge_id;
			$execute_paidondate=mysql_query($paidondate);
			$fetch_paidondate=mysql_fetch_array($execute_paidondate);
			//query to get date on the id of date
			$get_date_of_id=
			"SELECT DISTINCT date
			FROM dates_d
			WHERE date_id = ".$fetch_paidondate['paid_on']."";
			$exe_date=mysql_query($get_date_of_id);
			$fetch_date=mysql_fetch_array($exe_date);
			$payon_date = $fetch_date[0];*/
			echo ' <strong>NOTE: </strong> Last date of fee deposition of duration <span style="color:red">'.$fetch_details[0].'
			</span> of session <span style="color:red">'.$fetch_session['session_name'].'</span> is <span style="color:red">
			'.$dates.'</span>.<br><br>  ';          
			echo '<br><br>  DATE:<br><br> 
			SIGNATURE:	 
			</div></div><div>
			<!--  end widget-content -->';
			echo '			<div align="right" style="display:block" id="prn_one_'.$prn.'" style="cursor:pointer"><img
			 src="images/print.png" onclick="print_this_cls('.$fetch_class['admission_no'].')" style="width:40px
			 " style="height:30px"
			 />
			 </div>
			</div>	
			</div><!-- widget  span12 clearfix-->
			</div><!-- row-fluid -->';
			$prn++;
		}
		//loop close
		
	echo"</div>";
}

//function to show  name of students whose paid fee S 
function fee_paid_students()
{
/*	
	
	if($date=$_GET['date'] == "" || $date=$_GET['date'] == '0')
	{
		$date = 0;
	}
	else
	{
		$date=$_GET['date'];
	}
	if($duration=$_GET['duration'] == "" || $duration=$_GET['duration'] == '0')
	{
		$duration = 0;
	}
	else
	{
	echo	$duration=$_GET['duration'];
	}
	
	*/
	echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span> Fee Paid Students List</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly Select  <span class="netip"><a  class="red" > Session  </a></span>
	</div>
	';
	 $duration="SELECT Id, duration FROM fee_generated_sessions
	           WHERE session_id=".$_SESSION['current_session_id']."";
	$exe_duration=mysql_query($duration);	
	echo '
	<div class="section">
	<label>SELECT DURATION / TYPE<small></small></label>
	<div>
	<br/>
	<select name="duration" onchange="populate_duration_class(this.value);">
	<option value="">SELECT</option>';
	while($fetch_duration_id=mysql_fetch_array($exe_duration))
	    {
	        echo '<option value="'.$fetch_duration_id[0].'">'.$fetch_duration_id[1].'</option>';
		}
	echo '
	<option value="-1">Bi-ANNUALLY</option>
	<option value="-2">ANNUALLY</option>
	<option value="-3">TILL DATE</option>
	</select>
	<br><br>
	
	</div>
	</div>';
	//////	  
	echo'
	<br/><br/>  
	<div>
	<label>SELECT SESSION</label>   
	<div>';
	$sql="SELECT *
	FROM  session_table
	ORDER BY session_name DESC";			 
	$execute=mysql_query($sql);
	echo'
	<br/> <select name="session" id="session" class="small selectBox" padding: 6px;
	" onchange="populate_paid_class(this.value);"> <option value="">SELECT</option>';
	while($fetch=mysql_fetch_array($execute))
		{                     
			echo'              
			<option value="'.$fetch['sId'].'">'.$fetch['session_name'].'</option>';
		}
echo'  
	</select>   <br><hr width="100%" style="height:2px;color:black">
	<br/>
	<div id="myclass"></div>
	</div></div>';
    echo '	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';		
}

function populate_paid_students()
{
	$current_session=$_SESSION['current_session_id'];
	$cid=$_GET['cId'];
	$ssid=$_GET['sId'];
	$du=$_GET['du'];
	if($ssid==-1)
	{
		$query_session="SELECT session_name
	FROM session_table
	WHERE sId = ".$current_session."
	";
	}
	else
	{
	$query_session="SELECT session_name
	FROM session_table
	WHERE sId = ".$ssid."
	";
	}
	$execute_session=mysql_query($query_session);
	$fetch_session=mysql_fetch_array($execute_session);
	$sno=0;
	echo' 
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">';
	$sql_class_name="SELECT class_name
	FROM class_index
	WHERE cId = '".$_GET['cId']."'";
	$query_class=mysql_query($sql_class_name);
	$fetch_class=mysql_fetch_array($query_class);	   
	echo'      <div class="widget-header">
	<span>Students Paid Fee of<i style="color:red"> class '.$fetch_class['class_name'].'</i> </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	echo'                                  
	<table class="table table-bordered table-striped" id="dataTable">';
	if(isset($_GET['unpaid']))
	{
		echo '<h5 align="center" style="color:green" >FEE UNPAID OF ADDMISSION NO-'.$_GET['add'].'</h5>';	
	}
	echo '<h5 align="center" style="color:green" >'.$fetch_session[0].'</h5>
	<thead>';
	echo'    <tr>
	<th width="5%">S.No.</th>
	<th  width="12%">Admission No.</th>
	<th width="12%">Name</th>
	<th width="12%">Duration</th>
	<th width="12%">Credentials Fee</th>
	
	<th width="12%"> Fee</th>
	<th width="12%">Late Fine</th>
	<th width="12%">Total Fee</th>
	<th width="12%">Paid On</th>
	<th width="20%">Remarks</th>
	<th width="20%">Unpaid</th>
	</tr>
	</thead>
	<tbody align="center">';
	//query to get  students and user details from fee details and user table	
	//show details session wise
	if(($ssid!=-1)||($du==-2))
	{	
	//show current duration details annually
	if($du==-2)
	{
		$ssid=$current_session;
	}
	 $query = "SELECT DISTINCT (student_user.Name), student_user.admission_no, student_user.sId,
			class_index.class_name, fee_details.student_id, fee_details.mode_of_payment, 
			fee_details.paid_on, fee_details.paid_on, 
			fee_generated_sessions.duration, fee_generated_sessions.type, 
			fee_details.fee_generated_session_id, fee_details.remarks, fee_generated_sessions.last_date
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			INNER JOIN fee_generated_sessions
			ON fee_details.fee_generated_session_id = fee_generated_sessions.Id
			
			WHERE fee_details.paid = 1
			AND class_index.cId= ".$_GET['cId']."
			AND fee_generated_sessions.session_id = ".$ssid."
			
			";
	}
	//show details of paid students according duration wise
	else if($du > 0)
	{
		 $query = "SELECT DISTINCT (student_user.Name), student_user.admission_no, student_user.sId,
			class_index.class_name, fee_details.student_id, fee_details.mode_of_payment, 
			fee_details.paid_on, fee_details.paid_on, 
			fee_generated_sessions.duration, fee_generated_sessions.type, 
			fee_details.fee_generated_session_id, fee_details.remarks, fee_generated_sessions.last_date
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			INNER JOIN fee_generated_sessions
			ON fee_details.fee_generated_session_id = fee_generated_sessions.Id
			
			WHERE fee_details.paid = 1
			AND class_index.cId= ".$_GET['cId']."
			AND fee_details.fee_generated_session_id = ".$_GET['du']."
			";
	}
	//show till date details and bi annually
	elseif(($du==-3)||($du==-1))
	{
		date_default_timezone_set('Asia/Kolkata');
		    $date=date('m');
			 $month_duration=$date;
			 //show april--june
			 if(($month_duration>3)&&($month_duration<7))
			 {
				 $query = "SELECT DISTINCT (student_user.Name), student_user.admission_no, student_user.sId,
			class_index.class_name, fee_details.student_id, fee_details.mode_of_payment, 
			fee_details.paid_on, fee_details.paid_on, 
			fee_generated_sessions.duration, fee_generated_sessions.type, 
			fee_details.fee_generated_session_id, fee_details.remarks, fee_generated_sessions.last_date
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			INNER JOIN fee_generated_sessions
			ON fee_details.fee_generated_session_id = fee_generated_sessions.Id
			
			WHERE fee_details.paid = 1
			AND class_index.cId= ".$_GET['cId']."
			AND (fee_details.fee_generated_session_id =1)
			";
			 }
			 //show aprail--june--october
			else if((($month_duration>6)&&($month_duration<10))||($du==-1))
			 {
				$query = "SELECT DISTINCT (student_user.Name), student_user.admission_no, student_user.sId,
			class_index.class_name, fee_details.student_id, fee_details.mode_of_payment, 
			fee_details.paid_on, fee_details.paid_on, 
			fee_generated_sessions.duration, fee_generated_sessions.type, 
			fee_details.fee_generated_session_id, fee_details.remarks, fee_generated_sessions.last_date
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			INNER JOIN fee_generated_sessions
			ON fee_details.fee_generated_session_id = fee_generated_sessions.Id
			
			WHERE fee_details.paid = 1
			AND class_index.cId= ".$_GET['cId']."
			AND (fee_details.fee_generated_session_id =1 OR fee_details.fee_generated_session_id =2)
			"; 
			 }
			  //show aprail--june--october--december
			elseif(($month_duration>9)&&($month_duration<13))
			 {
				 	$query = "SELECT DISTINCT (student_user.Name), student_user.admission_no, student_user.sId,
			class_index.class_name, fee_details.student_id, fee_details.mode_of_payment, 
			fee_details.paid_on, fee_details.paid_on, 
			fee_generated_sessions.duration, fee_generated_sessions.type, 
			fee_details.fee_generated_session_id, fee_details.remarks, fee_generated_sessions.last_date
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			INNER JOIN fee_generated_sessions
			ON fee_details.fee_generated_session_id = fee_generated_sessions.Id
			
			WHERE fee_details.paid = 1
			AND class_index.cId= ".$_GET['cId']."
			AND (fee_details.fee_generated_session_id =1 OR fee_details.fee_generated_session_id =2 
			OR fee_details.fee_generated_session_id =3)
			"; 
			 }
			   //show aprail--june--october--december--jan---march
			elseif(($month_duration>0)&&($month_duration<4))
			 {
				 	$query = "SELECT DISTINCT (student_user.Name), student_user.admission_no, student_user.sId,
			class_index.class_name, fee_details.student_id, fee_details.mode_of_payment, 
			fee_details.paid_on, fee_details.paid_on, 
			fee_generated_sessions.duration, fee_generated_sessions.type, 
			fee_details.fee_generated_session_id, fee_details.remarks, fee_generated_sessions.last_date
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			INNER JOIN fee_generated_sessions
			ON fee_details.fee_generated_session_id = fee_generated_sessions.Id
			
			WHERE fee_details.paid = 1
			AND class_index.cId= ".$_GET['cId']."
			AND (fee_details.fee_generated_session_id =1 OR fee_details.fee_generated_session_id =2 
			OR fee_details.fee_generated_session_id =3 OR fee_details.fee_generated_session_id =7)
			";  
			 }
	}
	$execute = mysql_query($query) ;
	while($fetch = mysql_fetch_array($execute))
		{ 
			//QUERY get cost of transport from destination_rouite------
			$transport_cost=0;
			$trans_amount="SELECT cost
						   FROM destination_route
						   
						   INNER JOIN user_vehicle_details
						   ON user_vehicle_details.did=destination_route.did
						   
						   WHERE user_vehicle_details.uId=".$fetch['sId']."
						   AND destination_route.session_id=".$_SESSION['current_session_id']."";
			$exe_amount_t=mysql_query($trans_amount);
			while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
				{
					$transport_cost=$transport_cost+$fetch_transport_cost[0];
				}
			//query get rent of hostel from room allocation --------
			$hostel_rent=0;
			$hostel_amount="SELECT rent 
							FROM hostel_room_allocation
							WHERE sid=".$fetch['sId']."
							AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
			$exe_rent=mysql_query($hostel_amount);
			while($fetch_rent_cost=mysql_fetch_array($exe_rent))
				{
					$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
				}
			$trans_rent_cost=$hostel_rent+$transport_cost;           
			++$sno;    
			$credential = 0;
			//query to get credentials amount and type
			$sql_fee_credentials="SELECT amount
					   FROM fee_credentials
					   INNER JOIN fee_details
					   ON fee_credentials.Id = fee_details.fee_credential_id
					   WHERE fee_details.fee_generated_session_id = ".$fetch['fee_generated_session_id']." 
					   AND fee_details.student_id=".$fetch['sId']."";
			$execute_fee = mysql_query($sql_fee_credentials);
			while($fetch_fee = mysql_fetch_array($execute_fee))
				{
					$credential = $credential + $fetch_fee['amount'];
				}
					   
			$discount = 0;
			//query to get discount types and amount 
			$sql_discount_credentials="SELECT SUM(amount)
					   FROM fee_discount_credentials
					   INNER JOIN fee_details
					   ON fee_discount_credentials.Id = fee_details.fee_discount_id
					   WHERE fee_details.fee_generated_session_id = ".$fetch['fee_generated_session_id']." 
					   AND fee_details.student_id=".$fetch['sId']."";
			$execute_discount = mysql_query($sql_discount_credentials);
			$fetch_discount = mysql_fetch_array($execute_discount);
			$discount=$fetch_discount[0];
			//echo $discount;
			//calculate total discounts credentials amounts
			$total = $credential - $discount+$trans_rent_cost; 	
			echo'     <tr class="odd gradeX">
							<td width="5%">'.$sno.'</td>
							<td width="12%">'.$fetch['admission_no'].'</td>
							<td width="12%">'.$fetch['Name'].'</td>
							 ';
			
			//query to get last date
			$du=$fetch['duration'];
			/* $last_date="SELECT last_date
			FROM fee_generated_sessions
			WHERE fee_generated_sessions.duration='".$du."'
			";
			$execute_last_date=mysql_query($last_date);
			$fetch_last_date=mysql_fetch_array($execute_last_date);	
			$lastdate=$fetch_last_date['last_date'];*/
					
			/*//fine
			$finee="fine";
			$fine=0;
			//write query to fetch fine amount
			$query_fine="SELECT `amount` 
					  FROM `fee_credentials`
					  WHERE  `type`='".$finee."'
					  AND fee_credentials.session_id=".$current_session."";
			$execute_fine=mysql_query($query_fine);
			$fetch_fine=mysql_fetch_array($execute_fine);
			$fine=$fetch_fine['amount'];*/
			//check condition due date < paid on date
			//and calculate fine
		    /*	$paid_date=$fetch['paid_on'];
			  if($lastdate<$paid_date)
				  {
					 $fine_days=$paid_date-$last_date;
					 $calculate_fine=$fine_days*$fine; 
				  }
			  else
				  {
					 $calculate_fine=0;  
				  }
			 */		 
			$sql="SELECT date
					FROM dates_d
					WHERE date_id = '".$fetch['paid_on']."'";									
			$execute_date=mysql_query($sql);
			$fetch_date=mysql_fetch_array($execute_date);
			//calculate fee amount according to duration
			//Monthly=1  Quarterly-3, half early=6 and annually=12
			$month=$fetch['type'];
			$m=1;
			if($month=="Monthly")
				{
					$m=1; 
				}
			else if($month=="Quarterly")
				{
					$m=3; 
				}
			else if($month=="Half-Yearly")
				{
					$m=6;  
				}	
			else if($month=="Yearly")
				{
					$m=12;  
				}
			$creden_amount=$credential+$trans_rent_cost;
			echo'  
			<td width="12%">'.$du.'</td>
			<td width="12%">'.$creden_amount.'</td>
			
			<td width="12%">'.$total.'</td>';
										//calculate fine
					//get current date and show fine
					date_default_timezone_set('Asia/Kolkata');
		             $current_date=date('Y-m-d');
					  $current_id="SELECT date_id FROM dates_d
					             WHERE date='".$current_date."'";
					 $exe_current=mysql_query($current_id);
					 $fetch_current_date=mysql_fetch_array($exe_current);
					if($fetch['last_date']<$fetch['paid_on'])
					  {
						$fine_days=$fetch['paid_on'] - $fetch['last_date'];
						if($fine_days<=30)
						{
							$calculate_fine=100;
						}
						else if($fine_days<=60)
						{
							$calculate_fine=200;
						}
						else
						{
							$calculate_fine=500;
						}
						/*$calculate_fine=$fine_days*$fine; */
					}
				else
					{
						$calculate_fine=0;  
					}
					$paid_fee_with_fine = $total  * $m + $calculate_fine;

			echo '<td width="12%">'.$calculate_fine.'</td>
			<td width="12%">'.$paid_fee_with_fine.'</td>
			<td width="12%">'.$fetch_date['date'].'</td>
			<td width="12%"><a href="accounts_fee_paid_details_mayo.php?add='.$fetch['admission_no'].'
				&name='.$fetch['Name'].'&sid='.$fetch['sId'].'&duid='.$fetch['fee_generated_session_id'].'">VIEW</a></td>';
		/*	echo '<a href="accounts_fee_cheque_details.php?add='.$fetch['admission_no'].'&
			name='.$fetch['Name'].'&sid='.$fetch['sId'].'&duid='.$fetch['fee_generated_session_id'].'">View</a></td>';*/
			
		echo'	<td width="12%"><a href="accounts_fee_paid_fee_unpaid.php?add='.$fetch['admission_no'].'&
			name='.$fetch['Name'].'&sid='.$fetch['sId'].'&duid='.$fetch['fee_generated_session_id'].'&cid='.$cid.'
			&ssId='.$ssid.'&du='.$fetch['duration'].'&classname='.$fetch['class_name'].'">Unpaid</a></td></tr>';
		}
	echo'</table></td></tr>';
	echo' </tbody>
	</table>
	<a href="accounts_fee_paid_students.php" class="uibutton icon answer ">Back</a>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}
//function show form unpaid fee
function accounts_fee_paid_fee_unpaid()
{
	$add=$_GET['add'];
	$sid=$_GET['sid'];
	$duration=$_GET['duid'];
	$du=$_GET['du'];
	$name=$_GET['name'];
	$classname=$_GET['classname'];
	$cid=$_GET['cid'];
	$ssId=$_GET['ssId'];
echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>Unpaid Fee</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	
	
	';		
	 if (isset($_GET['Successfully_Insert']))
				{
					echo "<h5 align=\"center\">ADDED SUCCESSFULLY!</h5>"; 
				}
				
				 if (isset($_GET['error']))
				{
					echo "<h5 align=\"center\">Please try again!</h5>
				"; 
				}
				
	echo'	
   <form id="validation_demo" action="fee/paid_fee_unpaid.php" method="get">  
 
<input type="hidden" name="sid" value="'.$sid.'"/>
<input type="hidden" name="duid" value="'.$duration.'"/>
<input type="hidden" name="cid" value="'.$cid.'"/>
<input type="hidden" name="ssId" value="'.$ssId.'"/>
<div class="section ">
  <label>Student Name<small></small></label>   
   <div> 
 <input type="submit" class=" medium" name="name" value="'.$name.'"/>
   </div>
   </div>
 <div class="section ">
  <label>Admission No.<small></small></label>   
   <div> 
 <input type="submit" class=" medium" name="add" value="'.$add.'"/>
   </div>
   </div>
   <div class="section ">
  <label>Class Name<small></small></label>   
   <div> 
 <input type="submit" class=" medium" name="class" value="'.$classname.'"/>
   </div>
   </div>
 <div class="section numericonly">
  <label>Duration<small></small></label>   
   <div> 
 <input type="submit" class="validate[required] medium" name="du" value="'.$du.'" />
   </div>
   </div>
 	<div class="section">
<label>Date <small></small></label>   
<div><input type="text"  id="birthday" class=" birthday  medium " class="validate[required] medium"  name="date"  />
</div>
</div>
<div class="section ">
 <label>Resion(Why ?)<small></small></label>   
 <div> 
 <textarea name="why" class="validate[required] medium"  ></textarea>
 </div>
 </div>		
<div class="section last">
 <div><button class="uibutton submit" rel="1" type="submit"  >SUBMIT</button>
 <button class="uibutton special" rel="1" type="reset">RESET</button>  
 
</div>
</div>									
</form> 
';
	echo'                              
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';
}
//function show fee paid students details according to school level------------------------
function consolidated_fee_report()
{
	$sno=0;
	$current_session=$_SESSION['current_session_id'];
	$ssid=$_GET['sId'];
	$du=$_GET['du'];
	if($ssid==-1)
	{
	$query_session="SELECT session_name
	FROM session_table
	WHERE sId = ".$current_session."
	";
	}
	else
	{
	$query_session="SELECT session_name
	FROM session_table
	WHERE sId = ".$ssid."
	";
	}
	$execute_session=mysql_query($query_session);
	$fetch_session=mysql_fetch_array($execute_session);	
	echo' 
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">';
	echo'      <div class="widget-header">
	<span>Consolidated Fee Report of <i style="color:red"> School </i> </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	echo'   
	<table class="table table-bordered table-striped" id="dataTable">
	<h5 align="center" style="color:green">'.$fetch_session[0].'</h5>
	<thead>';
	echo'    <tr>
	<th width="5%">S.No.</th>
	<th  width="12%">Admission No.</th>
	<th width="12%">Name</th>
	<th width="12%">Duration</th>
	<th width="12%">Credentials Fee</th>
	
	 <th width="12%"> Fee</th>
	 <th width="12%">Total Fee</th>
	<th width="12%">Payment Mode</th>
	<th width="12%">Paid On</th>
	<th width="19%">Remarks</th>';
	/*echo '<th width="19%">Unpaid</th>';*/
	echo '</tr>
	</thead>
	<tbody align="center">';
	//show fee paid details session wise or annually
	if(($ssid!=-1)||($du==-2))
	{
	if($du==-2)
	{
		$ssid=$current_session;
	}
	$query = "SELECT DISTINCT (student_user.Name), student_user.admission_no, student_user.sId, 
			class_index.class_name,   fee_details.student_id, fee_details.mode_of_payment,
			fee_details.paid_on, fee_details.paid_on, 
			fee_generated_sessions.duration, fee_generated_sessions.type, fee_generated_sessions.Id, fee_details.remarks,
			class_index.cId
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			INNER JOIN fee_generated_sessions
			ON fee_details.fee_generated_session_id = fee_generated_sessions.Id
			
			WHERE fee_details.paid = 1
			AND fee_generated_sessions.session_id = ".$ssid."
			";
	}
			elseif($du>0)
			{
				$query = "SELECT DISTINCT (student_user.Name), student_user.admission_no, student_user.sId, 
			class_index.class_name,   fee_details.student_id, fee_details.mode_of_payment,
			fee_details.paid_on, fee_details.paid_on, 
			fee_generated_sessions.duration, fee_generated_sessions.type, fee_generated_sessions.Id, fee_details.remarks,
			class_index.cId
			
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			INNER JOIN fee_generated_sessions
			ON fee_details.fee_generated_session_id = fee_generated_sessions.Id
			
			WHERE fee_details.paid = 1
			AND fee_details.fee_generated_session_id = ".$du."
			";
			}
			//show till date details and bi annually
	elseif(($du==-3)||($du==-1))
	{
		date_default_timezone_set('Asia/Kolkata');
		    $date=date('Y-m-d');
			$month_du=preg_split("/\-/",$date);
			 $month_duration=$month_du[1];
			 //show aprail--june
			 if(($month_duration>3)&&($month_duration<4))
			 {
			$query = "SELECT DISTINCT (student_user.Name), student_user.admission_no, student_user.sId, 
			class_index.class_name,   fee_details.student_id, fee_details.mode_of_payment,
			fee_details.paid_on, fee_details.paid_on, 
			fee_generated_sessions.duration, fee_generated_sessions.type, fee_generated_sessions.Id, fee_details.remarks,
			class_index.cId
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			INNER JOIN fee_generated_sessions
			ON fee_details.fee_generated_session_id = fee_generated_sessions.Id
			
			WHERE fee_details.paid = 1
			
			AND (fee_details.fee_generated_session_id =1)
			";
			 }
			 //show aprail--june--october
			else if((($month_duration>6)&&($month_duration<10))||($du==-1))
			 {
			$query = "SELECT DISTINCT (student_user.Name), student_user.admission_no, student_user.sId, 
			class_index.class_name,   fee_details.student_id, fee_details.mode_of_payment,
			fee_details.paid_on, fee_details.paid_on, 
			fee_generated_sessions.duration, fee_generated_sessions.type, fee_generated_sessions.Id, fee_details.remarks,
			class_index.cId
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			INNER JOIN fee_generated_sessions
			ON fee_details.fee_generated_session_id = fee_generated_sessions.Id
			
			WHERE fee_details.paid = 1
			
			AND (fee_details.fee_generated_session_id =1 OR fee_details.fee_generated_session_id =2)
			"; 
			 }
			  //show aprail--june--october--december
			elseif(($month_duration>9)&&($month_duration<13))
			 {
			$query = "SELECT DISTINCT (student_user.Name), student_user.admission_no, student_user.sId, 
			class_index.class_name,   fee_details.student_id, fee_details.mode_of_payment,
			fee_details.paid_on, fee_details.paid_on, 
			fee_generated_sessions.duration, fee_generated_sessions.type, fee_generated_sessions.Id, fee_details.remarks,
			class_index.cId
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			INNER JOIN fee_generated_sessions
			ON fee_details.fee_generated_session_id = fee_generated_sessions.Id
			
			WHERE fee_details.paid = 1
			
			AND (fee_details.fee_generated_session_id =1 OR fee_details.fee_generated_session_id =2 
			OR fee_details.fee_generated_session_id =3)
			"; 
			 }
			   //show aprail--june--october--december--jan---march
			elseif(($month_duration>0)&&($month_duration<4))
			 {
				$query = "SELECT DISTINCT (student_user.Name), student_user.admission_no, student_user.sId, 
			class_index.class_name,   fee_details.student_id, fee_details.mode_of_payment,
			fee_details.paid_on, fee_details.paid_on, 
			fee_generated_sessions.duration, fee_generated_sessions.type, fee_generated_sessions.Id, fee_details.remarks,
			class_index.cId
			FROM student_user
			
			INNER JOIN class
			ON student_user.sId = class.sId
			
			INNER JOIN class_index
			ON class.classId = class_index.cId
			
			INNER JOIN fee_details
			ON fee_details.student_id = student_user.sId
			
			INNER JOIN fee_generated_sessions
			ON fee_details.fee_generated_session_id = fee_generated_sessions.Id
			
			WHERE fee_details.paid = 1
			
			AND (fee_details.fee_generated_session_id =1 OR fee_details.fee_generated_session_id =2 
			OR fee_details.fee_generated_session_id =3 OR fee_details.fee_generated_session_id =7)
			";  
			 }
	}
	$execute = mysql_query($query) ;
	while($fetch = mysql_fetch_array($execute))
	{            
	    //QUERY get cost of transport from destination_rouite------
		$transport_cost=0;
		$trans_amount="SELECT cost
					   FROM destination_route
					   
					   INNER JOIN user_vehicle_details
					   ON user_vehicle_details.did=destination_route.did
					   
					   WHERE user_vehicle_details.uId=".$fetch['sId']."
					   AND destination_route.session_id=".$_SESSION['current_session_id']."";
		$exe_amount_t=mysql_query($trans_amount);
		while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
			{
				$transport_cost=$transport_cost+$fetch_transport_cost[0];
			}
		//query get rent of hostel from room allocation --------
		$hostel_rent=0;
		$hostel_amount="SELECT rent 
						FROM hostel_room_allocation
						WHERE sid=".$fetch['sId']."
						AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
		$exe_rent=mysql_query($hostel_amount);
		while($fetch_rent_cost=mysql_fetch_array($exe_rent))
			{
				$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
			}
	$trans_rent_cost=$hostel_rent+$transport_cost;           
	++$sno;    
	$credential = 0;
	//query to get credentials amount and type
	$sql_fee_credentials="SELECT amount
				   FROM fee_credentials
				   INNER JOIN fee_details
				   ON fee_credentials.Id = fee_details.fee_credential_id
				   WHERE fee_details.fee_generated_session_id = ".$fetch[10]." 
				   AND fee_details.student_id=".$fetch['sId']."";
	$execute_fee = mysql_query($sql_fee_credentials);
	while($fetch_fee = mysql_fetch_array($execute_fee))
		{
			$credential = $credential + $fetch_fee['amount'];
		}			   
	$discount = 0;
	//query to get discount types and amount 
	$sql_discount_credentials="SELECT SUM(amount)
				   FROM fee_discount_credentials
				   INNER JOIN fee_details
				   ON fee_discount_credentials.Id = fee_details.fee_discount_id
				   WHERE fee_details.fee_generated_session_id = ".$fetch[10]." 
				   AND fee_details.student_id=".$fetch['sId']."";
	$execute_discount = mysql_query($sql_discount_credentials);
	$fetch_discount = mysql_fetch_array($execute_discount);
	$discount=$fetch_discount[0];
	//echo $discount;
	//calculate total discounts credentials amounts
	$total = $credential - $discount+$trans_rent_cost; 	
	echo'  
	<tr class="odd gradeX">
	<td width="5%">'.$sno.'</td>
	<td width="12%">'.$fetch['admission_no'].'</td>
	<td width="12%">'.$fetch['Name'].'</td>
	 ';
	//query to get last date
	$du=$fetch['duration'];
	/* $last_date="SELECT last_date
	FROM fee_generated_sessions
	WHERE fee_generated_sessions.duration='".$du."'
	";
	$execute_last_date=mysql_query($last_date);
	$fetch_last_date=mysql_fetch_array($execute_last_date);	
	$lastdate=$fetch_last_date['last_date'];*/
				
	/*//fine
	$finee="fine";
	$fine=0;
	//write query to fetch fine amount
	$query_fine="SELECT `amount` 
				  FROM `fee_credentials`
				  WHERE  `type`='".$finee."'
				  AND fee_credentials.session_id=".$current_session."";
	$execute_fine=mysql_query($query_fine);
	$fetch_fine=mysql_fetch_array($execute_fine);
	$fine=$fetch_fine['amount'];*/
	//check condition due date < paid on date
	//and calculate fine
    /*	$paid_date=$fetch['paid_on'];
	  if($lastdate<$paid_date)
	  {
		 $fine_days=$paid_date-$last_date;
		 $calculate_fine=$fine_days*$fine; 
	  }
	  else
	  {
		
		 $calculate_fine=0;  
	  }
			*/		 
	
	$sql="SELECT date
	FROM dates_d
	WHERE date_id = '".$fetch['paid_on']."'";									
	$execute_date=mysql_query($sql);
	$fetch_date=mysql_fetch_array($execute_date);
	//calculate fee amount according to duration
	//Monthly=1  Quarterly-3, half early=6 and annually=12
	$month=$fetch['type'];
	$m=1;
	if($month=="Monthly")
		{
			$m=1; 
		}
	else if($month=="Quarterly")
		{
			$m=3; 
		}
	else if($month=="Half-Yearly")
		{
			$m=6;  
		}	
	else if($month=="Yearly")
		{
			$m=12;  
		}
	$creden_amount=$credential+$trans_rent_cost;
	echo'  
	<td width="12%">'.$du.'</td>
	<td width="12%">'.$creden_amount.'</td>
	
	<td width="12%">'.$total.'</td>
	<td width="12%">'.$total*$m.'</td>
	<td width="12%">'.$fetch['mode_of_payment'].'</td>
	<td width="12%">'.$fetch_date['date'].'</td>
	<td width="12%"><a href="accounts_fee_paid_details_mayo.php?add='.$fetch['admission_no'].'
				&name='.$fetch['Name'].'&sid='.$fetch['sId'].'&duid='.$fetch['Id'].'">VIEW</a></td>';

/*	echo '<a href="accounts_fee_cheque_details.php?add='.$fetch['admission_no'].'
	&name='.$fetch['Name'].'&sid='.$fetch['sId'].'&duid='.$fetch['Id'].'">View</a></td>';*/
	/*echo '<td width="12%"><a href="fee/paid_fee_unpaid.php?add='.$fetch['admission_no'].'&
	name='.$fetch['Name'].'&sid='.$fetch['sId'].'&duid='.$fetch[10].'&cid='.$fetch['cId'].'
	&ssId='.$fetch['sId'].'">Unpaid</a></td></tr>';*/
	}
	echo'</table></td></tr>';
	echo' </tbody>
	</table>
	<a href="accounts_fee_paid_students.php" class="uibutton icon answer ">Back</a>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}
//function show students fee summary details accourding duration
function student_fee_summary()
{
	echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Students Fee Summary</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly Enter  <span class="netip"><a  class="red" > 
	Student\'s Admission Number  </a></span>
	</div>
	';		  
	echo'       
	<div class="section">
	<label>Admission No.</label>   
	<div> 
	<input type="text" name="admission_no" id="admission_no"  class=" medium" onkeyup="student_fee_summary()" />
	</div>
	</div>
	<div id="mycl">
	</div>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';	
}
//function is used to refund fee
function refund_fee()
{
	echo'
	<div class="row-fluid">
	<!-- Widget --> <div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Refund Fee </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';
	if(isset($_GET['done']))
	  {
		echo '<h4 align="center" style="color:green">FEE REFUND SUCCESSFULLY!</h4>';
	  }
	else if(isset($_GET['error']))
	  {
		echo '<h4 align="center" style="color:red">ERROR!</h4>';
	  }					
	echo'
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly Enter  <span class="netip"><a  class="red" > Student\'s
	 Admission Number  </a></span>
	</div>
	';		  
	echo'       
	<div class="section">
	<label>Admission No.</label>   
	<div> 
	<input type="text" name="admission_no" id="admission_no"  class=" medium" onkeyup="fee_refund(this.value)" />
	</div>
	</div>
	<div id="refund">
	</div>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';	
}	

//function to get total fee 
function total_fee_details() 
{	  
	echo '<div class="row-fluid">					
	<div class="span12  widget clearfix">
	<div class="widget-header">
	<span>Total Fee Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	<div class="row-fluid"> 		  
	';			  	 		 		 	 		 
	echo'
	<form action="fee_manager_session.php" method="get" name="session" >										 	
	<label><h4 align="center">select Month Session<h4><small></label>   
	<div align="center">											
	<select  data-placeholder="Choose  Session..." class="chzn-select" tabindex="2" name="session">        
	<option value=""></option>'; 								
	//query to get month from `fee_generated_sessions`  table
	  $get_session=
	"SELECT `type`,`Id`,`duration`
	FROM `fee_generated_sessions` 
	";  
	$execute_session=mysql_query($get_session);
	while( $fetch_session=mysql_fetch_array($execute_session))//looping for select all month
		{
			$session_name=$fetch_session['type'];
			$session_id=$fetch_session['Id'];
			$duration=$fetch_session['duration'];
			echo'
			<option value="'.$session_id.'">'. $duration.'</option>';
		} 						  			  				 		 			  					   
	echo'  </select>  
	<input type="hidden" id="'.$session_id.'"    > 
	</div>									                                  				
	<br></br>			   																	
	<div class="section last" align="center">
	<button  class="uibutton submit_form" >Go</button>
	</div>
	</form>
	';	 		 	 	  	  	  
	echo'	                                  
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';								  
}    
  

 //END JR. MANISH  
 
//function to show the student fee credentials details discount and  paid fee
function fee_not_paid_details()
{
	$id=$_GET['id'];
	
	$student_id = $_SESSION['user_id'];
	$current_session=$_SESSION['current_session_id'];
	$add_no=$_GET['add_no'];
	$admission_no=$_GET['add_no'];
	//write query to get student addmission no	
	$query_1="SELECT * , class_name ,cId
	FROM student_user
	INNER JOIN class
	ON student_user.sId = class.sId
	INNER JOIN class_index
	ON class.classId = class_index.cId
	WHERE admission_no = ".$admission_no."
	";
	$execute_1=mysql_query($query_1);
	$fetch_1=mysql_fetch_array($execute_1);
	//write query to get students id by addmission no.
	$stu_id="SELECT sId
	FROM student_user
	WHERE admission_no = ".$admission_no."
	";
	$exe_id=mysql_query($stu_id);
	$fetch_id=mysql_fetch_array($exe_id);
	$stud_id=$fetch_id['sId'];
	
	//Query to get the class of the user student
	$query_class_student = "SELECT class_index.class_name
						  FROM class_index
						  
						  INNER JOIN class
						  ON class.classId = class_index.cId
						  
						  WHERE class.sId = $student_id
						  ";
	$execute_class_student = mysql_query($query_class_student);
	$class_student = mysql_fetch_array($execute_class_student);
	//write query to get the session id from  the fee generated session
	$fetch_session_name="SELECT `session_name`
						FROM `session_table` 
						WHERE `sId`=".$current_session."";
	$execute_session_name=mysql_query($fetch_session_name);
	$fetch_session=mysql_fetch_array($execute_session_name);
	$session_name=$current_session['session_name'];
	$class_name = $class_student[0];
	$total_discount=0;
	$camount=0;	
	$serial_no=1;
	echo'
	<div class="row-fluid">
	
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Student : '.$fetch_1['Name'].'</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<h6>Class : '.$fetch_1['class_name'].'</h6>
	<h5 align="center"> Current Session : '.$fetch_session['session_name'].'</h5>
	<table  class="table table-bordered table-striped" align="center">
	<thead>
	<tr>
	<th align="center" width="5%">S.No.</th>
	<th align="left" width="50%">Credential Type</th>
	<th align="left" width="45%">Amount</th>
	</tr>
	</thead>
	<tbody>';
	$query_fee_ge_session_id="SELECT `Id`, `type`
	FROM `fee_generated_sessions`
	WHERE session_id=".$_SESSION['current_session_id']."";
	$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
	while($fetch_fee=mysql_fetch_array($execute_fee_ge_se_id))
		{
			$month=$fetch_fee['type'];
			$fee_ge_id=$fetch_fee[0];
		}
	 //QUERY get cost of transport from destination_rouite------
		$transport_cost=0;
		$trans_amount="SELECT cost
					   FROM destination_route
					   
					   INNER JOIN user_vehicle_details
					   ON user_vehicle_details.did=destination_route.did
					   
					   WHERE user_vehicle_details.uId=$stud_id
					   AND destination_route.session_id=".$_SESSION['current_session_id']."";
		$exe_amount_t=mysql_query($trans_amount);
		while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
			{
				$transport_cost=$transport_cost+$fetch_transport_cost[0];
			}
		//query get rent of hostel from room allocation --------
		$hostel_rent=0;
		$hostel_amount="SELECT rent 
						FROM hostel_room_allocation
						WHERE sid=$stud_id
						AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
		$exe_rent=mysql_query($hostel_amount);
		while($fetch_rent_cost=mysql_fetch_array($exe_rent))
			{
				$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
			}
		$trans_rent_cost=$hostel_rent+$transport_cost;
	 
	//start query to fetch credential type and amount
	//and calculate total credential amount
	  $camount=0;    
	$fetch_credentials_type="SELECT DISTINCT fee_credentials.type,fee_credentials.amount
						  FROM fee_credentials
						  INNER JOIN fee_details
						  ON fee_details.fee_credential_id=fee_credentials.id
						  WHERE student_id=".$stud_id." 
						  AND fee_credentials.session_id=".$current_session."
						  AND fee_details.fee_generated_session_id=".$id."";
	$execute_credentials_type=mysql_query($fetch_credentials_type);
	while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
		{
			$camount=$camount+$fetch_credentials['amount'];
			//first latter of credential type convert small to capital
			$c=str_split($fetch_credentials['type']);
			$upper=strtoupper($c[0]);
			$count=count($c);
			echo '	<tr>
			<td align="center">'.$serial_no++.'</td>
			<td>';
			echo $upper;
			for($i=1;$i<$count;$i++)
			{
			 echo $c[$i];  
			}
			
			echo '   
			<td>'.$fetch_credentials['amount'].'</td></tr>';  
		}
		if($hostel_rent!=0)
		{
	echo '<tr>
	<td align="center">'.$serial_no++.'</td>
	<td>Transport</td>
	<td>'.$transport_cost.'</td></tr>
	';}
	if($hostel_rent!=0)
	{
	 echo '<tr>
	 <td align="center">'.$serial_no++.'</td>
	<td>Hostel Rent</td>
	<td>'.$hostel_rent.'</td></tr>
	';}
	
/*	echo ' <table  class="table table-bordered table-striped">
   <thead>
  	<tr>
   <th align="center" width="5%">S.No.</th>
	<th align="left" width="50%">Discount Type</th>
	<th align="left" width="45%">Amount</th>
	</tr>
	</thead>
	<tbody>';*/
	//query to fetch discoun type of student and amount
	// and calculate total discount amount
	$serial_no=1;
	$total_discount=0;
	$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
						 FROM fee_discount_credentials
						 INNER JOIN fee_details
						 ON fee_details.fee_discount_id=fee_discount_credentials.id
						 WHERE fee_details.student_id=".$stud_id."
						AND fee_discount_credentials.session_id=".$current_session."
						 AND fee_details.fee_generated_session_id=".$id."";
	 $execute_discount_type=mysql_query(  $fetch_discount_query);
	while($fetch_discount=mysql_fetch_array($execute_discount_type))
		 { 
			 $total_discount=$total_discount+$fetch_discount['amount'];
			  //first latter of discount type convert small to capital
			 $d=str_split($fetch_discount['type']);
			 $upper=strtoupper($d[0]);
			 $c=count($d);
			 echo '<tr>
					<td align="center">'.$serial_no++.'</td>
				   <td>';
				   echo $upper;
				   for($i=1;$i<$c;$i++)
				   {
					 echo $d[$i];  
				   }  
			echo '  <td>'.$fetch_discount['amount'].'</td> 
			 </tr>';
		 }
	  echo '
	 <table  class="table table-bordered table-striped"  >
	 <thead>
	</thead>
	  <tbody>';
	  //write query to get id and type from fee generated session id 
	 $cre_amount=$camount+$trans_rent_cost;
	  $tfamount=$cre_amount-$total_discount;
	  //calculate fee Monthly ,quartrely.........
	/*	   $m=1;
		  if($month=="Monthly")
		  {
			 $m=1; 
		  }
		  else if($month=="Quarterly")
		  {
			 $m=3; 
		  }
		  else
		  {
			$m=12;  
		  }
		  $total_fee_amount=$tfamount*$m;*/
	  echo '  <tr>
		<td width="55%"><b style="color:green">Total Tution Fee</b></td><td><b style="color:green">
		'.$cre_amount.'</b></td>
	  </tr> ';
	/*echo ' <tr>
	 <td><b style="color:green">Total Discount</b></td><td><b style="color:green">'.$total_discount.'</b></td>
	  </tr>';*/
	  $abc=$tfamount*3;
	 echo ' <tr>
	 <td><h4 style="color:red">Total Fee(quarterly)</h4></td><td><h4 style="color:red">'.$abc.'</h4></td>
	  </tr>
	  </tbody>
	 </table></tbody>
	  </table>
	  <a href="accounts_student_fee_summary.php" class="uibutton icon answer ">Back</a>
	 '; 
	echo '	   
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}
//manish codding manager side
//function to call fee_manager_get_session
function session_details()
{
	echo  '<div class="row-fluid">                           
	<div class="span12  widget clearfix">
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Session</span>
	</div><!-- End widget-header -->		  
	<div class="widget-content"><br />
	<div class="row-fluid"> 			
	';	
	if(isset($_GET['select']))
	{
		echo '<h5 align="center" style="color:red">SELECT CLASS AND DURATION PROPERLY</h5>';
	}
	//get time 
	 $time_offset ="525"; // Change this to your time zone
	 $time_a = ($time_offset * 120);
	 $today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	
	echo'

	<form action="fee_manager_total_calculation.php" method="get" name="session" >	
	<div class="section">								
	<label>Select Session<small></small></label>   
	<div >											
	<select  data-placeholder="Choose  Session..." class="chzn-select" id="duration1" tabindex="2"  name="session"
	onchange="show_duration_se_class(this.value);" >        
	<option value=""></option>'; 								
	//query to get month from `fee_generated_sessions`  table
	$get_session="SELECT `sId`,`session_name`
				FROM `session_table` ";
	$get_session_exe=mysql_query($get_session);
	while($get_session_fetch=mysql_fetch_array($get_session_exe))
		{
			$sid=$get_session_fetch['sId'];	
			$session_name=$get_session_fetch['session_name'];
			echo'
			<option value="'.$sid.'">'. $session_name.'</option>';
		} 							 
	echo'  </select>       
	   </div></div>
	   <div id="se_du_class"></div>										                                  				   																	
	   <div class="section last">
					<label></label>
					<div>
					
					<input type="submit" name="submit" value="SUBMIT">
	             </div>
	              </div> 
	   </form>
	';
	//query to get session name and session_id	
	echo'                                       
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';			
		
}
//function to select month session 
function duration_details()

{  

	if($_GET['session'] == "")
	{
		header('location:fee_manager_get_session.php');
		
	}
		else
	{
		echo '<div class="row-fluid">					
		<div class="span12  widget clearfix">
		<div class="widget-header">
		<span><i class="icon-align-center"></i>Select Type & Duration</span>
		</div><!-- End widget-header -->	
		<div class="widget-content"><br />
		';		
		//query to get month from `fee_generated_sessions`  table
		$get_duration=
					"SELECT  `duration`
					FROM `fee_generated_sessions` 
					WHERE `session_id`=".$_GET['session']."";  
		$execute_duration=mysql_query($get_duration);
		$fetch_duration=mysql_fetch_array($execute_duration);
		//query to get type
		$get_session_type=
						"SELECT  DISTINCT `type` 
						FROM `fee_generated_sessions` 
						WHERE `session_id`=".$_GET['session']."";  							
		echo'
		<form action="fee_manager_total.php" method="get" name="session" id="session">
		<input type="hidden" name="session" value="'.$_GET['session'].'" />	
		
		<div class="section">
		<label>Select Type</label>           
		<div>
		<select data-placeholder="Choose Type..." class="chzn-select" name="type" id="type" onchange="show_type();" >        
		<option value=""></option>'; 								
		//query to get month from `fee_generated_sessions`  table
		$duration= $fetch_duration['duration'];
		
		$execute_session_type=mysql_query($get_session_type);
		while( $fetch_session_type=mysql_fetch_array($execute_session_type))//looping for select all month session
			{	
				// query to get id 	
				$get_session_id=
								"SELECT   `Id`
								 FROM `fee_generated_sessions` 
								 WHERE `type`='".$fetch_session_type['type']."'";
				 $execute_session_id=mysql_query($get_session_id);
				 $fetch_session_id=mysql_fetch_array($execute_session_id);								 			      					     	         $session_id=$fetch_session_id['Id'];            											
				echo'
				<option value="'.$fetch_session_type['type'].'">'.$fetch_session_type['type'].'</option>';
			}           			  					   
		echo'  </select>       
		</div>
		</div>						 
		<div id="data_type"></div>																																
		<div class="section last">
		<input type="submit"  class="uibutton submit_form" value="submit">	  		
		</div>
		</form>
		';   
		echo'                                       
		</div><!-- row-fluid column-->
		</div><!--  end widget-content -->
		</div><!-- widget  span12 clearfix-->      
		';	
	}
}
//function to total fee details
function  fee_total_details()
{	
	echo  '<div class="row-fluid">                           
	<div class="span12  widget clearfix">
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Class  Fee Details</span>
	</div><!-- End widget-header -->		  
	<div class="widget-content"><br />
	<div class="row-fluid"> 			
	';	
	if(($_GET['type'] == "")||($_GET['duration'] == ""))
	{
		header('location:fee_manager_duration.php?session='.$_GET['session'].'');
	}
	else
		//query to get session name
		$session_name="SELECT `session_name`
		FROM `session_table` 
		WHERE `sId`=".$_GET['session']." ";
		$exe_session_name=mysql_query($session_name);
		$fetch_session_name=mysql_fetch_array($exe_session_name);
		$name=$fetch_session_name['session_name'];
		echo '<h5 style="color:green"align="center"><td >'.$name.'</td></h5>';			
		
		$disp='';
		$q="SELECT `cId`,`class_name` FROM class_index ORDER BY class, section ASC";
		$q_res=mysql_query($q);
		echo '<h2 align="center">';
		echo' <div class="span4"><h3 style="color:">Kindly select class</h3></div>';
		echo '</h2>';
		echo "<br>";
		echo "<br>";
		echo "<br>";
		echo '<ol class="rounded-list">';
		while($res=mysql_fetch_array($q_res))//looping for getting  class id
		{	
			echo '<tr class="row-fluid" id="added_rows">
			<td><div class="row-fluid">
			<div class="span6">							                                                              
			<li><a href="fee_manager_total_calculation.php?class_id='.$res['cId'].'&&session='.$_GET['session'].'
			&&type='.$_GET['type'].'&&duration='.$_GET['duration'].'">'.$res[1].'Class</a>                                                                    	                                 
			</div>
			</div><!-- end row-fluid  -->
			</td>
			</tr>';	 	 
		}
	echo'                                       
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';										
	

}
//function to calaculate total fee through class basis                                    montlhly         MANISH  function     
function total_calculation_details()
{
	echo '<div class="row-fluid">					
	<div class="span12  widget clearfix">
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Total Fee Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	<div class="row-fluid"> 		  
	';
	 
	if(($_GET['duration']=="")||($_GET['class_id']==""))	
	{
		header('Location:fee_manager_get_session.php?select');
	}
	
	//query to get session name
	$session_name="SELECT `session_name`
				  FROM `session_table` 
				  WHERE `sId`=".$_GET['session']." ";
	$exe_session_name=mysql_query($session_name);
	$fetch_session_name=mysql_fetch_array($exe_session_name);
	$name=$fetch_session_name['session_name'];
	echo '<h5 style="color:green"align="center"><td >'.$name.'</td></h5>';				  
	//query to get type
	  $get_session_type=
					"SELECT   `duration` ,`type`
					FROM `fee_generated_sessions` 
					WHERE `session_id`=".$_GET['session']." AND `Id`=".$_GET['duration']."";  
	$execute_session_type=mysql_query($get_session_type);
	$fetch_session_type=mysql_fetch_array($execute_session_type);
	//query to get month from `fee_generated_sessions`  table
	$get_session=
				"SELECT `Id`,`duration`,`type`
				FROM `fee_generated_sessions` 
				WHERE `Id`='".$_GET['duration']."' AND`session_id`=".$_GET['session']."";  
	$execute_session=mysql_query($get_session);
	$fetch_session=mysql_fetch_array($execute_session);//looping for select all month session	 
	$session_type=$fetch_session['type'];
	$session_id=$fetch_session['Id'];
	$duration=$fetch_session['duration'];
	
	//query to get session id 
	/*
	$get_session_id="SELECT  fee_credentials.session_id, fee_discount_credentials.session_id 
	FROM fee_generated_sessions
			   
	INNER JOIN fee_credentials
	ON fee_credentials.session_id = fee_generated_sessions.session_id
	
	INNER JOIN fee_discount_credentials
	ON fee_discount_credentials.session_id = fee_generated_sessions.session_id
	
	WHERE fee_generated_sessions.session_id = ".$_GET['session']." ";
	$exe_get_session_id=mysql_query($get_session_id);
	$fetch_get_session_id=mysql_fetch_array($exe_get_session_id);*/
	$cre_id=$_GET['session'];
	$dis_id=$_GET['session'];
	
	$dis_callect=0;
	$collect=0;		 
	$dis_callect_ex=0;
	$collect_ex=0;
	$due_collect=0;
	$due_discount=0;
	//query to get total credentials amounts                                           =====START PAID FEE CALCULATION+++
	//according duration and till date
	if($_GET['duration']<0)
	{
	date_default_timezone_set('Asia/Kolkata');
		    $date=date('m');
			 $month_duration=$date;
			 //show april--june
			 if((($month_duration>3)&&($month_duration<7))&&($_GET['duration']==-3))
			 { 
			 
			   $get_paid="SELECT fee_credentials.type, fee_credentials.session_id, fee_credentials.amount,																																																																								 				fee_generated_sessions.duration  
				,fee_details.paid
				
				FROM fee_details
				
				INNER JOIN fee_credentials
				ON fee_credentials.Id = fee_details.fee_credential_id
				
				INNER JOIN fee_generated_sessions
				ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				INNER JOIN class
				ON class.sId = fee_details.student_id
				
				WHERE class.classId = ".$_GET['class_id']."
				AND (fee_generated_sessions.Id = 1)
			    AND fee_generated_sessions.session_id =".$_GET['session']." ";
			 }
			 //show aprail--june--october
			else if((($month_duration>6)&&($month_duration<10))||($_GET['duration']==-1))
			 {
				   $get_paid="SELECT fee_credentials.type, fee_credentials.session_id, fee_credentials.amount,																																																																								 				fee_generated_sessions.duration  
				,fee_details.paid
				
				FROM fee_details
				
				INNER JOIN fee_credentials
				ON fee_credentials.Id = fee_details.fee_credential_id
				
				INNER JOIN fee_generated_sessions
				ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				INNER JOIN class
				ON class.sId = fee_details.student_id
				
				WHERE class.classId = ".$_GET['class_id']."
				AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2)
			    AND fee_generated_sessions.session_id =".$_GET['session']." ";
			 }
			  //show aprail--june--october--december
			elseif((($month_duration>9)&&($month_duration<13))&&($_GET['duration']==-3))
			 {
				 
				$get_paid="SELECT fee_credentials.type, fee_credentials.session_id, fee_credentials.amount,																																																																								 				fee_generated_sessions.duration  
				,fee_details.paid
				
				FROM fee_details
				
				INNER JOIN fee_credentials
				ON fee_credentials.Id = fee_details.fee_credential_id
				
				INNER JOIN fee_generated_sessions
				ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				INNER JOIN class
				ON class.sId = fee_details.student_id
				
				WHERE class.classId = ".$_GET['class_id']."
				AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2 OR fee_generated_sessions.Id = 3)
			    AND fee_generated_sessions.session_id =".$_GET['session']." ";
			 }
			   //show aprail--june--october--december--jan---march
			elseif((($month_duration>0)&&($month_duration<4))||($_GET['duration']==-2))
			 {
				 
				$get_paid="SELECT fee_credentials.type, fee_credentials.session_id, fee_credentials.amount,																																																																								 				fee_generated_sessions.duration  
				,fee_details.paid
				
				FROM fee_details
				
				INNER JOIN fee_credentials
				ON fee_credentials.Id = fee_details.fee_credential_id
				
				INNER JOIN fee_generated_sessions
				ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				INNER JOIN class
				ON class.sId = fee_details.student_id
				
				WHERE class.classId = ".$_GET['class_id']."
				AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2 
				OR fee_generated_sessions.Id = 3 OR fee_generated_sessions.Id = 7)
			    AND fee_generated_sessions.session_id =".$_GET['session']." ";
			 }
	}
	else
	{
	 $get_paid="SELECT fee_credentials.type, fee_credentials.session_id, fee_credentials.amount,																																																																								 				fee_generated_sessions.duration  
				,fee_details.paid
				
				FROM fee_details
				
				INNER JOIN fee_credentials
				ON fee_credentials.Id = fee_details.fee_credential_id
				
				INNER JOIN fee_generated_sessions
				ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				INNER JOIN class
				ON class.sId = fee_details.student_id
				
				WHERE class.classId = ".$_GET['class_id']."
				AND fee_generated_sessions.Id = ".$_GET['duration']."
			    AND fee_generated_sessions.session_id =".$_GET['session']." ";
	}
	$execute_paid=mysql_query($get_paid);
	while($fetch_paid=mysql_fetch_array($execute_paid))//looping fo select the paid type 
		{
			$paid=$fetch_paid['paid'];
			$paid_credential=$fetch_paid[2];
			if($paid==1)
				{
					$collect=$collect+$paid_credential;
				}	
		
		}
	//QUERY get cost of transport from destination_rouite------
	  $transport_cost=0;
	  $trans_amount="SELECT DISTINCT fee_details.student_id, destination_route.cost
					 FROM destination_route
					 
					 INNER JOIN user_vehicle_details
					 ON user_vehicle_details.did=destination_route.did
					 
					 INNER JOIN class

					 ON class.sId =`user_vehicle_details`.uId
					 
					 INNER JOIN fee_details
					 ON fee_details.student_id=`user_vehicle_details`.uId
					 
					 WHERE fee_details.paid=1
					 AND destination_route.session_id=".$_GET['session']." 
					 AND class.classId = ".$_GET['class_id']."";
	  $exe_amount_t=mysql_query($trans_amount);
	  while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
		  {
			  $transport_cost=$transport_cost+$fetch_transport_cost[1];
		  }
	  //query get rent of hostel from room allocation --------
	  $hostel_rent=0;
     $hostel_amount="SELECT DISTINCT fee_details.student_id, hostel_room_allocation.rent 
					  FROM hostel_room_allocation
					  
					   INNER JOIN class
					   ON class.sId =hostel_room_allocation.sid
					   
					  INNER JOIN fee_details
					  ON fee_details.student_id=hostel_room_allocation.sid
					   
					  WHERE fee_details.paid=1
					  AND hostel_room_allocation.session_id=".$_GET['session']." 
					  AND class.classId = ".$_GET['class_id']."";
	  $exe_rent=mysql_query($hostel_amount);
	  while($fetch_rent_cost=mysql_fetch_array($exe_rent))
		  {
			  $hostel_rent=$hostel_rent+$fetch_rent_cost[1];
		  }
	   $trans_rent_cost=$hostel_rent+$transport_cost;	
	
	//query to get total discounts amounts
	 $get_paid="SELECT fee_discount_credentials.type,																																																																													                fee_discount_credentials.session_id,
				 fee_discount_credentials.amount , fee_generated_sessions.duration  
				,fee_details.paid
				
				FROM fee_details
			
				INNER JOIN fee_discount_credentials
				ON fee_discount_credentials.Id = fee_details.fee_discount_id 
				
				INNER JOIN fee_generated_sessions
				ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				INNER JOIN class
				ON class.sId = fee_details.student_id
				
				WHERE class.classId = ".$_GET['class_id']."
				AND fee_generated_sessions.Id = ".$_GET['duration']." AND fee_generated_sessions.session_id =
				".$_GET['session']." ";
	
	$execute_paid=mysql_query($get_paid);
	while($fetch_paid=mysql_fetch_array($execute_paid))//looping fo select the paid type 
		{
			$paid=$fetch_paid['paid'];
			$dis_credential=$fetch_paid[2];	
			if($paid==1)
			{
				$dis_callect=$dis_callect+$dis_credential;
			}		
		}				
	 $total_callect=$collect-$dis_callect+$trans_rent_cost;
	//total collection			  
	echo '<br></br>';	
	
	//query to fetch type month..quarter.......
	$query_month="SELECT `duration`,`session_id`,`type`
				FROM   `fee_generated_sessions` 
				WHERE `Id`=".$_GET['duration']." AND`session_id`='".$_GET['session']."'";
	$exe_month=mysql_query($query_month);
	$fetch_month=mysql_fetch_array($exe_month);
	$session=$fetch_month['type'];
	if($session=="Quarterly")                               //condition for session
	{
		$total_callect=$total_callect*3;	
	}  
	else if($session=="Half-Yearly")
	{
		$total_callect=$total_callect*6;			
	}
	else if($session=="Yearly")
	{
		$total_callect=$total_callect*12;
	}
	else if($session=="Monthly")
	{
		$total_callect=$total_callect;
	}							
	//query to get session id 
	$get_session="SELECT  fee_credentials.session_id, fee_discount_credentials.session_id 
				FROM fee_generated_sessions
				
				INNER JOIN fee_credentials
				ON fee_credentials.session_id = fee_generated_sessions.session_id
				
				INNER JOIN fee_discount_credentials
				ON fee_discount_credentials.session_id = fee_generated_sessions.session_id
				
				WHERE fee_generated_sessions.session_id = ".$_GET['session']." ";
	
	$exe_get_session=mysql_query($get_session);
	$fetch_get_session=mysql_fetch_array($exe_get_session);
	$cre_id=$fetch_get_session[0];
	$dis_id=$fetch_get_session[1];
	
	//query to get student id  	
	$value=0;
	$i=0;
	//query to get credentials amount                                                  ==calculate total fee paid and unpaid
	//according duration and till date
	if($_GET['duration']<0)
	{
	date_default_timezone_set('Asia/Kolkata');
		    $date=date('m');
			 $month_duration=$date;
			 //show april--june
			 if((($month_duration>3)&&($month_duration<7))&&($_GET['duration']==-3))
			 {
				 
			$get_credential=
				"SELECT fee_credentials.type, fee_credentials.session_id,  SUM(fee_credentials.amount),
				 fee_generated_sessions.duration
				
				,fee_details.paid
				
				FROM fee_details
				
				INNER JOIN fee_credentials
				
				ON fee_credentials.Id = fee_details.fee_credential_id
				
				INNER JOIN fee_generated_sessions
				
				ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				INNER JOIN class
				
				ON class.sId = fee_details.student_id
				
				WHERE class.classId = ".$_GET['class_id']."
				AND (fee_generated_sessions.Id = 1)
			    AND fee_generated_sessions.session_id =".$_GET['session']." ";
			 }
			 //show aprail--june--october
			else if((($month_duration>6)&&($month_duration<10))||($_GET['duration']==-1))
			 {
				
		$get_credential=
				"SELECT fee_credentials.type, fee_credentials.session_id,  SUM(fee_credentials.amount),
				 fee_generated_sessions.duration
				
				,fee_details.paid
				
				FROM fee_details
				
				INNER JOIN fee_credentials
				
				ON fee_credentials.Id = fee_details.fee_credential_id
				
				INNER JOIN fee_generated_sessions
				
				ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				INNER JOIN class
				
				ON class.sId = fee_details.student_id
				
				WHERE class.classId = ".$_GET['class_id']."
				AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2)
			    AND fee_generated_sessions.session_id =".$_GET['session']." ";
			 }
			  //show aprail--june--october--december
			elseif((($month_duration>9)&&($month_duration<13))&&($_GET['duration']==-3))
			 {
				 
	       $get_credential=
				"SELECT fee_credentials.type, fee_credentials.session_id,  SUM(fee_credentials.amount),
				 fee_generated_sessions.duration
				
				,fee_details.paid
				
				FROM fee_details
				
				INNER JOIN fee_credentials
				
				ON fee_credentials.Id = fee_details.fee_credential_id
				
				INNER JOIN fee_generated_sessions
				
				ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				INNER JOIN class
				
				ON class.sId = fee_details.student_id
				
				WHERE class.classId = ".$_GET['class_id']."
				AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2 OR fee_generated_sessions.Id = 3)
			    AND fee_generated_sessions.session_id =".$_GET['session']." ";
			 }
			   //show aprail--june--october--december--jan---march
			elseif((($month_duration>0)&&($month_duration<4))||($_GET['duration']==-2))
			 {
				 
				$get_credential=
				"SELECT fee_credentials.type, fee_credentials.session_id,  SUM(fee_credentials.amount),
				 fee_generated_sessions.duration
				
				,fee_details.paid
				
				FROM fee_details
				
				INNER JOIN fee_credentials
				
				ON fee_credentials.Id = fee_details.fee_credential_id
				
				INNER JOIN fee_generated_sessions
				
				ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				INNER JOIN class
				
				ON class.sId = fee_details.student_id
				
				WHERE class.classId = ".$_GET['class_id']."
				AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2 
				OR fee_generated_sessions.Id = 3 OR fee_generated_sessions.Id = 7)
			    AND fee_generated_sessions.session_id =".$_GET['session']." ";
			 }
	}
	else
	{
		 $get_credential=
				"SELECT fee_credentials.type, fee_credentials.session_id, SUM(fee_credentials.amount),
				 fee_generated_sessions.duration
				
				,fee_details.paid
				
				FROM fee_details
				
				INNER JOIN fee_credentials
				
				ON fee_credentials.Id = fee_details.fee_credential_id
				
				INNER JOIN fee_generated_sessions
				
				ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				INNER JOIN class
				
				ON class.sId = fee_details.student_id
				
				WHERE class.classId = ".$_GET['class_id']."
				
				AND fee_generated_sessions.Id = ".$_GET['duration']." 
				AND fee_generated_sessions.session_id =".$_GET['session']."";
	}

	/*"SELECT fee_credentials.amount, fee_discount_credentials.amount
	FROM fee_credentials
	INNER JOIN fee_details ON fee_details.fee_credential_id = fee_credentials.Id
	INNER JOIN fee_discount_credentials ON fee_details.fee_discount_id = fee_discount_credentials.Id";*/
	
	$execute_credential=mysql_query($get_credential);	
	$fetch_crendential=mysql_fetch_array($execute_credential);	//looping for calculate credentials amount	
	
	$collect_ex=$fetch_crendential[2];					
							
	
	//QUERY get cost of transport from destination_rouite------
		$transport_cost=0;
		 $trans_amount="SELECT DISTINCT fee_details.student_id, destination_route.cost
					   FROM destination_route
					   
					   INNER JOIN user_vehicle_details
					   ON user_vehicle_details.did=destination_route.did
					   
					   INNER JOIN class
	
					   ON class.sId =`user_vehicle_details`.uId
					   
					   INNER JOIN fee_details
					   ON fee_details.student_id=`user_vehicle_details`.uId
					   
					   WHERE class.classId = ".$_GET['class_id']."
					   AND destination_route.session_id=".$_GET['session']." 
					   ";
		$exe_amount_t=mysql_query($trans_amount);
		while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
			{
				$transport_cost=$transport_cost+$fetch_transport_cost[1];
			}
		//query get rent of hostel from room allocation --------
		$hostel_rent=0;
		
	 $hostel_amount="SELECT DISTINCT fee_details.student_id, hostel_room_allocation.rent 
						FROM hostel_room_allocation
						
						 INNER JOIN class
						 ON class.sId =hostel_room_allocation.sid
						 
						INNER JOIN fee_details
						ON fee_details.student_id=hostel_room_allocation.sid
						 
						WHERE class.classId = ".$_GET['class_id']."
						AND hostel_room_allocation.session_id=".$_GET['session']." 
						";
		$exe_rent=mysql_query($hostel_amount);
		while($fetch_rent_cost=mysql_fetch_array($exe_rent))
			{
				$hostel_rent=$hostel_rent+$fetch_rent_cost[1];
			}
		$trans_rent_cost=$hostel_rent+$transport_cost;
	//query to get discount amounts
	 $get_credential=
				"SELECT fee_discount_credentials.type , fee_discount_credentials.session_id ,
				
				 fee_discount_credentials.amount , fee_generated_sessions.duration
				
				,fee_details.paid
				
				FROM fee_details
				
				
				
				INNER JOIN fee_discount_credentials
				
				ON fee_discount_credentials.Id = fee_details.fee_discount_id 
				
				INNER JOIN fee_generated_sessions
				
				ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				INNER JOIN class
				
				ON class.sId = fee_details.student_id
				
				WHERE class.classId = ".$_GET['class_id']."
				
				AND fee_generated_sessions.Id = ".$_GET['duration']." AND fee_generated_sessions.session_id =
				".$_GET['session']."
				";
	/*"SELECT fee_credentials.amount, fee_discount_credentials.amount
	FROM fee_credentials
	INNER JOIN fee_details ON fee_details.fee_credential_id = fee_credentials.Id
	INNER JOIN fee_discount_credentials ON fee_details.fee_discount_id = fee_discount_credentials.Id";*/
	$execute_credential=mysql_query($get_credential);	
	while($fetch_crendential=mysql_fetch_array($execute_credential))	//looping for calculate credentials amount	
	{					
	$dis_callect_ex=$dis_callect_ex+$fetch_crendential[2];		  		   			
	}
	$value=$collect_ex-$dis_callect_ex+$trans_rent_cost;		
	
	//query to fetch type month..quarter.......
	$query_month_type="SELECT  `duration`,`type`
	FROM   `fee_generated_sessions` 
	WHERE `Id`=".$_GET['duration']." AND`session_id`='".$_GET['session']."'";
	$exe_month_type=mysql_query($query_month_type);
	$fetch_month_type=mysql_fetch_array($exe_month_type);
	$session=$fetch_month_type['type'];
	if($session=="Quarterly")
	{
		 $value=$value*3;
	}  
	else if($session=="Half-Yearly")
	{
		$value=$value*6;
	}
	else if($session=="Yearly")
	{
		$value=$value*12;
	}	
	else if($session=="Monthly")
	{
		$value=$value;
	}											  
	$i=0;
	$discount=0; 
	//query to get discount credntials ---------------------------------------------------------------------------------------------------------------
	$get_discount=
				" SELECT fee_discount_credentials.amount,fee_discount_credentials.session_id
				FROM fee_discount_credentials
				
				INNER JOIN fee_details
				ON fee_details.fee_discount_id = fee_discount_credentials.Id 
				
				INNER JOIN class
				ON class.sId = fee_details.student_id
				
				WHERE class.classId = ".$_GET['class_id']." AND fee_discount_credentials.session_id=".$_GET['session']."
				AND fee_details.fee_generated_session_id =".$_GET['duration']."
				
				";
	$execute_discount=mysql_query($get_discount);
	while($fetch_discount=mysql_fetch_array($execute_discount))//collect discount credential and paid credential
		{
			$disconnt_amount[$i]=$fetch_discount['amount'];
			$discount=$discount+$disconnt_amount[$i];		
			$i++;		
		}			   	 
	$discount;//discount credentials						
	
	//query to fetch type month..quarter.......
	$query_month_name="SELECT  `type`,`session_id`
						FROM   `fee_generated_sessions` 
						WHERE `Id`=".$_GET['duration']." AND `session_id`=".$_GET['session']."";
	$exe_month_name=mysql_query($query_month_name);
	$fetch_month_name=mysql_fetch_array($exe_month_name);
	$session=$fetch_month_name['type'];
	if($session=="Quarterly")
	{
		 $discount=$discount*3;
	}  
	else if($session=="Half-Yearly")
	{
		$discount=$discount*6;
	}		
	else if($session=="Yearly")
	{
		$discount=$discount*12;
	}	
	else if($session=="Monthly")
	{
		$discount=$discount;
	}	
	//total expected calculation============================================================================================
	 $expected_amount=$value-$discount;	    
	//query to get month 
	//query to get month from `dates_d` table
	$get_session="SELECT `type`, `duration`,`session_id`
				FROM `fee_generated_sessions` 
				WHERE `Id`=".$_GET['duration']." AND `session_id`=".$_GET['session']."
				";  
	$execute_session = mysql_query($get_session);
	$fetch_session = mysql_fetch_array($execute_session);		 
	$session_type = $fetch_session['duration'];
	//total due collection                                                     +++++++++++++++CALCULATE DUE TOTAL
	
	///========get receiving fee
		//according duration and till date
	if($_GET['duration']<0)
	{
	date_default_timezone_set('Asia/Kolkata');
		    $date=date('m');
			 $month_duration=$date;
			 //show april--june
			 if((($month_duration>3)&&($month_duration<7))&&($_GET['duration']==-3))
			 {
		    $receiv="SELECT SUM(fee_details.receiving_fee) FROM fee_details
			         INNER JOIN class_index
					 ON class_index.cId=fee_details.cid
				
				WHERE paid=4
				AND fee_details.cid=".$_GET['class_id']."
				AND (fee_details.fee_generated_session_id = 1)
			   ";
			 }
			 //show aprail--june--october
			else if((($month_duration>6)&&($month_duration<10))||($_GET['duration']==-1))
			 {
			 $receiv="SELECT SUM(fee_details.receiving_fee) FROM fee_details
			         INNER JOIN class_index
					 ON class_index.cId=fee_details.cid
				
				WHERE paid=4
				AND fee_details.cid=".$_GET['class_id']."
				AND (fee_details.fee_generated_session_id = 1 OR fee_details.fee_generated_session_id = 2)
			   ";
			 }
			  //show aprail--june--october--december
			elseif((($month_duration>9)&&($month_duration<13))&&($_GET['duration']==-3))
			 {
	     $receiv="SELECT SUM(fee_details.receiving_fee) FROM fee_details
			         INNER JOIN class_index
					 ON class_index.cId=fee_details.cid
				
				WHERE paid=4
				AND fee_details.cid=".$_GET['class_id']."
				AND (fee_details.fee_generated_session_id = 1 OR fee_details.fee_generated_session_id = 2 
				OR fee_details.fee_generated_session_id = 3)
			     ";
			 }
			   //show aprail--june--october--december--jan---march
			elseif((($month_duration>0)&&($month_duration<4))||($_GET['duration']==-2))
			 {
			$receiv="SELECT SUM(fee_details.receiving_fee) FROM fee_details
			         INNER JOIN class_index
					 ON class_index.cId=fee_details.cid
				
				WHERE paid=4
				AND fee_details.cid=".$_GET['class_id']."
				AND (fee_details.fee_generated_session_id = 1 OR fee_details.fee_generated_session_id = 2 
				OR fee_details.fee_generated_session_id = 3 OR fee_details.fee_generated_session_id = 7)
			    ";
			 }
	}
	else
	{
			$receiv="SELECT SUM(fee_details.receiving_fee) FROM fee_details
			         INNER JOIN class_index
					 ON class_index.cId=fee_details.cid
			         WHERE paid=4
					 AND fee_details.fee_generated_session_id=".$_GET['duration']."
					 AND fee_details.cid=".$_GET['class_id']."";
			
	}
	$exe_receive=mysql_query($receiv);
			$fetch_rece=mysql_fetch_array($exe_receive);																	            $receiving_fee = $fetch_rece[0];
		 
			//CALCULATE TOTAL DEUE=======================
				//according duration and till date
	if($_GET['duration']<0)
	{
		$TOTAL_A = $value*3;
			$COLLECT_A = $total_callect *3 + $receiving_fee;
			 $DUE_A =  $TOTAL_A - $COLLECT_A;
	}
	else
	{
			$TOTAL_A = $value;
			$COLLECT_A = $total_callect + $receiving_fee;
			 $DUE_A =  $TOTAL_A - $COLLECT_A;
	}
	$c_id = $_GET['class_id'];	
	//Query to get the class of the user
	$query_class_student = "
						SELECT class_index.class_name , class.sId
						FROM class_index		
						INNER JOIN class
						ON class.classId = class_index.cId		
						WHERE class.classId = $c_id
						";
	$execute_class_student = mysql_query($query_class_student);					  		  		  		  	  
	echo'		                                                                         	 
	<table  class="table table-bordered table-striped"  >
	<thead>
	<tr>
	<th>Class</th>	
	<th>Duration Name</th>		
	<th>Expected Fee(Rs.)</th>					
	<th>Total collected Fee(Rs.)</th>			
	<th>Due Fee(Rs.)fine not include</th>							
	</tr>
	</thead>	   	
	<tbody align="center">';	
	$class_student = mysql_fetch_array($execute_class_student);
	echo'<tr>';
	echo '<td>'.$class_student[0].'</td>
	<td>'. $session_type.'</td>
	<td>'.$TOTAL_A.'</td>
	<td><a href="accounts_populate_paid_students.php?cId='.$c_id.'&du='.$_GET['duration'].'&sId=-1">'. $COLLECT_A.'
	</a></td>
	<td><a href="accounts_show_class.php?cId='.$c_id.'&duration='.$_GET['duration'].'">'.$DUE_A.'</a></td>			   			   	
	</tr>
	';				     		
	echo'
	</tbody>
	</table>     
	';		
	echo " <script type='text/javascript'>
	$(function () {
	var chart;
	$(document).ready(function() {
	var colors = Highcharts.getOptions().colors,
	categories = ['Total', 'Collected', 'Due'],
	name = '',
	data = [{
	y: ".$TOTAL_A.",
	color: colors[0],
	}, {
	y: ".$COLLECT_A.",
	color: colors[1],
	}, {
	y: ".$DUE_A.",
	color: colors[2],
	}];
	
	function setChart(name, categories, data, color) {
	chart.xAxis[0].setCategories(categories);
	chart.series[0].remove();
	chart.addSeries({
	name: name,
	data: data,
	color: color || 'white'
	});
	}
	chart = new Highcharts.Chart({
	chart: {
	renderTo: 'container',
	type: 'column'
	},
	title: {                           
	text: '<strong> Class ".$class_student[0]."</strong>Total Fee Details '
	},
	subtitle: {
	text: ''
	},
	xAxis: {
	categories: categories
	},
	yAxis: {
	title: {
	text: 'Total Fee Generate'
	}
	},
	plotOptions: {
	column: {
	cursor: 'pointer',
	point: {
	events: {
	click: function() {
	}
	}
	},
	dataLabels: {
	enabled: true,
	color: colors[0],
	style: {
	fontWeight: 'bold'
	},
	formatter: function() {
	return this.y +'Rs';
	}
	}
	}
	},
	tooltip: {
	formatter: function() {
	var point = this.point,
	s = this.x +':<b>'+ this.y +'Rs</b><br/>';
	if (point.drilldown) {
	s += 'Click to view '+ point.category +' versions';
	} else {
	s += '';
	}
	return s;
	}
	},
	series: [{
	name: name,
	data: data,
	color: 'white'
	}],
	exporting: {
	enabled: false
	}
	});
	});
	});
	</script>
	";
	echo'
	<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
	';	
	echo'                                  
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';											
}

//total fee details without class
//function to call fee_manager_session.php
function session_total_details()
{
	echo  '<div class="row-fluid">                           
	<div class="span12  widget clearfix">
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Session</span>
	</div><!-- End widget-header -->		  
	<div class="widget-content"><br />
	<div class="row-fluid"> 			
	';	
	if(isset($_GET['select']))
	{
		echo '<h5 align="center" style="color:red">SELECT SESSION AND DURATION PROPERLY</h5>';
	}
	//get time 
	 $time_offset ="525"; // Change this to your time zone
	 $time_a = ($time_offset * 120);
	 $today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	
	echo'

	<form action="fee_manager_session.php"  method="get" name="session" >	
	<div class="section">								
	<label>select Session<small></small></label>   
	<div >											
	<select  data-placeholder="Choose  Session..." class="chzn-select" id="duration2" tabindex="2"  name="session"
	onchange="show_duration_se(this.value);" >        
	<option value=""></option>'; 								
	//query to get month from `fee_generated_sessions`  table
	$get_session="SELECT `sId`,`session_name`
				FROM `session_table` ";
	$get_session_exe=mysql_query($get_session);
	while($get_session_fetch=mysql_fetch_array($get_session_exe))
		{
			$sid=$get_session_fetch['sId'];	
			$session_name=$get_session_fetch['session_name'];
			echo'
			<option value="'.$sid.'">'. $session_name.'</option>';
		} 							 
	echo'  </select>       
	   </div></div>
	   <div id="se_du"></div>										                                  				   																	
	   <div class="section last">
					<label></label>
					<div>
					
					<input type="submit" name="submit" value="SUBMIT">
	             </div>
	             </div> 
	   </form>
	';
	//query to get session name and session_id	
	echo'                                       
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';			
		
}

//function to select month session 
function month_details()

{  
	if($_GET['session'] == "")
	{
		header('location:fee_manager_total_session.php');
	}
	else
		{
			echo '<div class="row-fluid">					
			<div class="span12  widget clearfix">
			<div class="widget-header">
			<span><i class="icon-align-center"></i>Select Type And Duration</span>
			</div><!-- End widget-header -->	
			<div class="widget-content"><br />
			<div class="row-fluid"> 		  
			';			
			//query to get month from `fee_generated_sessions`  table
			$get_duration=
						"SELECT  `duration`
						FROM `fee_generated_sessions` 
						WHERE `session_id`=".$_GET['session']."";  
			$execute_duration=mysql_query($get_duration);
			$fetch_duration=mysql_fetch_array($execute_duration);
			//query to get type
			$get_session_type=
						"SELECT  DISTINCT `type` 
						FROM `fee_generated_sessions` 
						WHERE `session_id`=".$_GET['session']."";  							
			echo'
			<form action="fee_manager_session.php" method="get" name="session" >	
			<input type="hidden" id="session" name="session" value="'.$_GET['session'].'">							 	
			<div class="section ">
			<label>Select Type<small></small></label>           
			<div> 							
			<select  data-placeholder="Choose  Type..." class="chzn-select" tabindex="2" name="type"
			id="type" onchange="show_type()";   
			<option value=""></option>'; 								
			//query to get month from `fee_generated_sessions`  table
			$duration= $fetch_duration['duration'];
			$execute_session_type=mysql_query($get_session_type);
			while( $fetch_session_type=mysql_fetch_array($execute_session_type))//looping for select all month session
				{	
					// query to get id 	
					$get_session_id=
					"SELECT   `Id`
					FROM `fee_generated_sessions` 
					WHERE `type`='".$fetch_session_type['type']."'";
					$execute_session_id=mysql_query($get_session_id);
					$fetch_session_id=mysql_fetch_array($execute_session_id);								 			      					     	            $session_id=$fetch_session_id['Id'];            											
					echo'
					<option value="'.$fetch_session_type['type'].'">'.$fetch_session_type['type'].'</option>';
				}           			  					   
			echo'  </select>       
			</div>
			</div>	
			<div id="data_type"></div>								                                  																		
			<div class="section last">
			<input type="submit"  class="uibutton submit_form" value="submit" " >							
			</form>
			';   				   	
			echo'	                                  
			</div><!-- row-fluid column-->
			</div><!--  end widget-content -->
			</div><!-- widget  span12 clearfix-->
			</div><!-- row-fluid -->
			';							
		}
}
//function to get total fee 
/*  function total_fee_details() 
  {	  
	  echo '<div class="row-fluid">					
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Total Fee Details</span>
      </div><!-- End widget-header -->	
      <div class="widget-content"><br />
      <div class="row-fluid"> 		  
	';
	  echo'	                                  
     </div><!-- row-fluid column-->
     </div><!--  end widget-content -->
     </div><!-- widget  span12 clearfix-->
     </div><!-- row-fluid -->
';								  
	  } */   
  
  //function to call fee total session details                                      yearly             MANISH function
function fee_total_session_details()
{	  	  
	if($_GET['duration'] == "")
		{
			header('location:fee_manager_total_session.php?select');
		}
	else
		{
			echo '<div class="row-fluid">					
			<div class="span12  widget clearfix">
			<div class="widget-header">
			<span><i class="icon-align-center"></i>Total Fee Details</span>
			</div><!-- End widget-header -->	
			<div class="widget-content"><br />
			<div class="row-fluid"> 		  
			';		    
			//query to get session name
			$session_name="SELECT `session_name`
				FROM `session_table` 
				WHERE `sId`=".$_GET['session']." ";
			$exe_session_name=mysql_query($session_name);
			$fetch_session_name=mysql_fetch_array($exe_session_name);
			$name=$fetch_session_name['session_name'];
			echo '<h5 style="color:green"align="center"><td >'.$name.'</td></h5>';				
			//query to get type
			$get_session_type=
							"SELECT   `duration` ,`type`
							FROM `fee_generated_sessions` 
							WHERE `session_id`=".$_GET['session']."  AND `duration`=
							".$_GET['duration']."";  
			$execute_session_type=mysql_query($get_session_type);
			$fetch_session_type=mysql_fetch_array($execute_session_type);
			//query to get month from `fee_generated_sessions`  table
			$get_session=
						"SELECT `Id`,`duration`,`type`
						FROM `fee_generated_sessions` 
						WHERE `duration`='".$_GET['duration']."' AND `session_id`=".$_GET
						['session']."";  
			$execute_session=mysql_query($get_session);
			$fetch_session=mysql_fetch_array($execute_session);//looping for select all month sessio
			$session_type=$fetch_session['type'];
			$session_id=$fetch_session['Id'];
			$duration=$fetch_session['duration'];
			$collect=0;
			$discount_collect=0;
			$total_credential=0;	  													  
			$collect_paid=0;	
			$total_cred=0;
			$total_dis_cred=0;
			$total=0;
			$dis_callect_paid=0;                                                      //=======START TOTAL FEE CALCULATION
			//query to get session id ete
			$get_session_id="SELECT  fee_credentials.session_id, fee_discount_credentials.session_id 
							FROM fee_generated_sessions
							
							INNER JOIN fee_credentials
							ON fee_credentials.session_id = fee_generated_sessions.session_id
							
							INNER JOIN fee_discount_credentials
							ON fee_discount_credentials.session_id = fee_generated_sessions.session_id
							
							WHERE fee_generated_sessions.session_id = ".$_GET['session']." ";
			$exe_get_session_id=mysql_query($get_session_id);
			$fetch_get_session_id=mysql_fetch_array($exe_get_session_id);
			$cre_id=$fetch_get_session_id[0];
			$dis_id=$fetch_get_session_id[1];
			//query to get total collected fee 
			//query to get total credentials amounts
			//according duration and till date
	       if($_GET['duration']<0)
	          {
	         date_default_timezone_set('Asia/Kolkata');
		    $date=date('m');
			 $month_duration=$date;
			 //show april--june
			 if((($month_duration>3)&&($month_duration<7))&&($_GET['duration']==-3))
			 { 
			$get_paid="SELECT  fee_credentials.session_id, fee_credentials.type,fee_credentials.amount,																																																											
						fee_generated_sessions.duration  
						,fee_details.paid
						FROM fee_details
						
						INNER JOIN fee_credentials
						ON fee_credentials.Id = fee_details.fee_credential_id
						
						INNER JOIN fee_generated_sessions
						ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				WHERE fee_generated_sessions.session_id=".$_GET['session']."
				AND (fee_generated_sessions.Id = 1)
			    ";
			 }
			 //show aprail--june--october
			else if((($month_duration>6)&&($month_duration<10))||($_GET['duration']==-1))
			 {
				$get_paid="SELECT  fee_credentials.session_id, fee_credentials.type,fee_credentials.amount,																																																											
						fee_generated_sessions.duration  
						,fee_details.paid
						FROM fee_details
						
						INNER JOIN fee_credentials
						ON fee_credentials.Id = fee_details.fee_credential_id
						
						INNER JOIN fee_generated_sessions
						ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				WHERE fee_generated_sessions.session_id=".$_GET['session']."
				AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2 )
			    ";
			 }
			  //show aprail--june--october--december
			elseif((($month_duration>9)&&($month_duration<13))&&($_GET['duration']==-3))
			 {
					$get_paid="SELECT  fee_credentials.session_id, fee_credentials.type,fee_credentials.amount,																																																											
						fee_generated_sessions.duration  
						,fee_details.paid
						FROM fee_details
						
						INNER JOIN fee_credentials
						ON fee_credentials.Id = fee_details.fee_credential_id
						
						INNER JOIN fee_generated_sessions
						ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				WHERE fee_generated_sessions.session_id=".$_GET['session']."
				AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2 
				OR fee_generated_sessions.Id = 3 )
			    ";
			 }
			   //show aprail--june--october--december--jan---march
			elseif((($month_duration>0)&&($month_duration<4))||($_GET['duration']==-2))
			 { 
				$get_paid="SELECT  fee_credentials.session_id, fee_credentials.type,fee_credentials.amount,																																																											
						fee_generated_sessions.duration  
						,fee_details.paid
						FROM fee_details
						
						INNER JOIN fee_credentials
						ON fee_credentials.Id = fee_details.fee_credential_id
						
						INNER JOIN fee_generated_sessions
						ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				WHERE fee_generated_sessions.session_id=".$_GET['session']."
				AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2 
				OR fee_generated_sessions.Id = 3 OR fee_generated_sessions.Id = 7)
			    ";
			 }
			}
			else
			{
			$get_paid="SELECT  fee_credentials.session_id, fee_credentials.type,fee_credentials.amount,																																																											
								fee_generated_sessions.duration  
								,fee_details.paid
								FROM fee_details
								
								INNER JOIN fee_credentials
								ON fee_credentials.Id = fee_details.fee_credential_id
								
								INNER JOIN fee_generated_sessions
								ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
								
								WHERE fee_generated_sessions.Id = ".$_GET['duration']." 
								AND fee_generated_sessions.session_id=".$_GET['session']."";
			}	
			$execute_paid=mysql_query($get_paid);
			while($fetch_paid=mysql_fetch_array($execute_paid))//looping fo select the paid type 
				{
					$paid=$fetch_paid['paid'];
					$paid_credential=$fetch_paid[2];
					
					$total_cred=$total_cred+$paid_credential;
				}
			//QUERY get cost of transport from destination_rouite------
			$transport_cost=0;
			$trans_amount="SELECT DISTINCT fee_details.student_id, destination_route.cost
							FROM destination_route
							
							INNER JOIN user_vehicle_details
							ON user_vehicle_details.did=destination_route.did
							
							INNER JOIN fee_details
							ON fee_details.student_id=`user_vehicle_details`.uId
							
							WHERE destination_route.session_id=".$_GET['session']." 
							
							";
			$exe_amount_t=mysql_query($trans_amount);
			while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
				{
					$transport_cost=$transport_cost+$fetch_transport_cost[1];
				}
			//query get rent of hostel from room allocation --------
			$hostel_rent=0;
			$hostel_amount="SELECT DISTINCT fee_details.student_id, hostel_room_allocation.rent 
							FROM hostel_room_allocation
							
							INNER JOIN fee_details
							ON fee_details.student_id=hostel_room_allocation.sid
							
							WHERE  hostel_room_allocation.session_id=".$_GET['session']." 
							";
			$exe_rent=mysql_query($hostel_amount);
			while($fetch_rent_cost=mysql_fetch_array($exe_rent))
				{
					$hostel_rent=$hostel_rent+$fetch_rent_cost[1];
				}
			$trans_rent_cost=$hostel_rent+$transport_cost;	
			//query to get discounts amounts
			$get_paid="SELECT fee_discount_credentials.type ,																																																														 		fee_discount_credentials.session_id,
					fee_discount_credentials.amount , fee_generated_sessions.duration  
					,fee_details.paid
					
					FROM fee_details
					
					INNER JOIN fee_discount_credentials
					ON fee_discount_credentials.Id = fee_details.fee_discount_id 
					
					INNER JOIN fee_generated_sessions
					ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
					
					WHERE fee_generated_sessions.Id = ".$_GET['duration']." 
					AND fee_generated_sessions.session_id=".$_GET['session']."";
			$execute_paid=mysql_query($get_paid);
			while($fetch_paid=mysql_fetch_array($execute_paid))//looping fo select the paid type 
				{
					$paid=$fetch_paid['paid'];
					$dis_credential=$fetch_paid[2];	     
					$total_dis_cred=$total_dis_cred+$dis_credential;
				}
			$total=$total_cred-$total_dis_cred+$trans_rent_cost;
			
			//query to fetch type month..quarter.......
			$query_month="SELECT `duration`,`session_id`,`type`
						  FROM   `fee_generated_sessions` 
						  WHERE `duration`=".$_GET['duration']." AND`session_id`=
						  '".$_GET['session']."'";
			$exe_month=mysql_query($query_month);
			$fetch_month=mysql_fetch_array($exe_month);
			$session=$fetch_month['type'];
			if( $session=="Quarterly")                  //condition for session
				{
					$total=$total*3;
				}  			   
			else if( $session=="Half-Yearly")
				{
					$total=$total*6;
				}
			
			else if( $session=="Yearly")
				{
					$total=$total*12;
				}
			else if( $session=="Monthly")
				{
					$total=$total;
				}                                                                     //========complete TOTAL
				                                                                        //=========START PAID                
			//query to get session id 
			$get_session="SELECT  fee_credentials.session_id, fee_discount_credentials.session_id 
						FROM fee_generated_sessions
						
						INNER JOIN fee_credentials
						ON fee_credentials.session_id = fee_generated_sessions.session_id
						
						INNER JOIN fee_discount_credentials
						ON fee_discount_credentials.session_id = fee_generated_sessions.session_id
						
						WHERE fee_generated_sessions.session_id = ".$_GET['session']." ";
			$exe_get_session=mysql_query($get_session);
			$fetch_get_session=mysql_fetch_array($exe_get_session);
			$cre_id=$fetch_get_session[0];
			$dis_id=$fetch_get_session[1];
			$paid_credential_fee=0;
			$dis_credential_fee=0;
			$tot_credential=0;
			$tot_dis_credential=0;
			$total_collected_fee=0;
			//query to calculate total credentials
				//according duration and till date
	       if($_GET['duration']<0)
	          {
	         date_default_timezone_set('Asia/Kolkata');
		    $date=date('m');
			 $month_duration=$date;
			 //show april--june
			 if((($month_duration>3)&&($month_duration<7))&&($_GET['duration']==-3))
			 { 
			$get_paid_fee="SELECT fee_credentials.type, fee_credentials.session_id, fee_credentials.amount,  
						  fee_generated_sessions.duration, fee_details.paid,fee_details.Id
						  FROM fee_details
						   INNER JOIN fee_credentials
							ON fee_credentials.Id = fee_details.fee_credential_id
							
							INNER JOIN fee_generated_sessions
							ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				WHERE fee_generated_sessions.session_id=".$_GET['session']."
				AND (fee_generated_sessions.Id = 1)
			    ";
			 }
			 //show aprail--june--october
			else if((($month_duration>6)&&($month_duration<10))||($_GET['duration']==-1))
			 {
		$get_paid_fee="SELECT fee_credentials.type, fee_credentials.session_id, fee_credentials.amount,  
						  fee_generated_sessions.duration, fee_details.paid,fee_details.Id
						  FROM fee_details
						   INNER JOIN fee_credentials
							ON fee_credentials.Id = fee_details.fee_credential_id
							
							INNER JOIN fee_generated_sessions
							ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				WHERE fee_generated_sessions.session_id=".$_GET['session']."
				AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2 )
			    ";
			 }
			  //show aprail--june--october--december
			elseif((($month_duration>9)&&($month_duration<13))&&($_GET['duration']==-3))
			 {
					$get_paid_fee="SELECT fee_credentials.type, fee_credentials.session_id, fee_credentials.amount,  
						  fee_generated_sessions.duration, fee_details.paid,fee_details.Id
						  FROM fee_details
						   INNER JOIN fee_credentials
							ON fee_credentials.Id = fee_details.fee_credential_id
							
							INNER JOIN fee_generated_sessions
							ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				WHERE fee_generated_sessions.session_id=".$_GET['session']."
				AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2 
				OR fee_generated_sessions.Id = 3 )
			    ";
			 }
			   //show aprail--june--october--december--jan---march
			elseif((($month_duration>0)&&($month_duration<4))||($_GET['duration']==-2))
			 { 
					$get_paid_fee="SELECT fee_credentials.type, fee_credentials.session_id, fee_credentials.amount,  
						  fee_generated_sessions.duration, fee_details.paid,fee_details.Id
						  FROM fee_details
						   INNER JOIN fee_credentials
							ON fee_credentials.Id = fee_details.fee_credential_id
							
							INNER JOIN fee_generated_sessions
							ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
				
				WHERE fee_generated_sessions.session_id=".$_GET['session']."
				AND (fee_generated_sessions.Id = 1 OR fee_generated_sessions.Id = 2 
				OR fee_generated_sessions.Id = 3 OR fee_generated_sessions.Id = 7)
			    ";
			 }
			}
			else
			{
			$get_paid_fee="SELECT fee_credentials.type, fee_credentials.session_id, fee_credentials.amount,  
						  fee_generated_sessions.duration, fee_details.paid,fee_details.Id
						  FROM fee_details
						   INNER JOIN fee_credentials
							ON fee_credentials.Id = fee_details.fee_credential_id
							
							INNER JOIN fee_generated_sessions
							ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
							
							WHERE fee_generated_sessions.Id =  ".$_GET['duration']." 
							AND fee_generated_sessions.session_id=".$_GET['session']."";
			}
			
			$execute_paid_fee=mysql_query($get_paid_fee);
			while($fetch_paid_fee=mysql_fetch_array($execute_paid_fee))//looping fo select the paid type 
				{
					$paid=$fetch_paid_fee['paid'];		  		
					$paid_credential_fee=$fetch_paid_fee[2];
					if($paid==1)
						{
							$tot_credential=$tot_credential+ $paid_credential_fee;
						}
				}
			//QUERY get cost of transport from destination_rouite------
			$transport_cost=0;
			$trans_amount="SELECT DISTINCT fee_details.student_id, destination_route.cost
							FROM destination_route
							
							INNER JOIN user_vehicle_details
							ON user_vehicle_details.did=destination_route.did
							
							INNER JOIN fee_details
							ON fee_details.student_id=`user_vehicle_details`.uId
							
							WHERE destination_route.session_id=".$_GET['session']." 
							AND (fee_details.paid=1 OR fee_details.paid=4)
							";
			$exe_amount_t=mysql_query($trans_amount);
			while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
				{
					$transport_cost=$transport_cost+$fetch_transport_cost[1];
				}
			//query get rent of hostel from room allocation --------
			$hostel_rent=0;
			$hostel_amount="SELECT DISTINCT fee_details.student_id, hostel_room_allocation.rent 
							FROM hostel_room_allocation
							
							INNER JOIN fee_details
							ON fee_details.student_id=hostel_room_allocation.sid
							
							WHERE hostel_room_allocation.session_id=".$_GET['session']."
							AND (fee_details.paid=1 OR fee_details.paid=4)
							
							";
			$exe_rent=mysql_query($hostel_amount);
			while($fetch_rent_cost=mysql_fetch_array($exe_rent))
				{
					$hostel_rent=$hostel_rent+$fetch_rent_cost[1];
				}
			$trans_rent_cost=$hostel_rent+$transport_cost;	
			//query to get total discounts
			$get_paid_fee="SELECT fee_discount_credentials.type , fee_discount_credentials.session_id, 
							fee_discount_credentials.amount , fee_generated_sessions.duration
							,fee_details.paid,fee_details.Id
							
							FROM fee_details
							
							INNER JOIN fee_discount_credentials
							ON fee_discount_credentials.Id = fee_details.fee_discount_id 
							
							INNER JOIN fee_generated_sessions
							ON fee_generated_sessions.Id = fee_details.fee_generated_session_id 
							
							WHERE fee_generated_sessions.Id =".$_GET['duration']." 
							AND fee_generated_sessions.session_id=".$_GET['session']."";
			
			$execute_paid_fee=mysql_query($get_paid_fee);
			while($fetch_paid_fee=mysql_fetch_array($execute_paid_fee))//looping fo select the paid type 
				{
				$paid=$fetch_paid_fee['paid'];		  		
				$dis_credential_fee=$fetch_paid_fee[2];
				if($paid==1)
					{
						$tot_dis_credential=$tot_dis_credential+$dis_credential_fee;
					}
				}
				//TOTAL COLLECTED FEE CALCULATE                                                   ==================
			$total_collected_fee = $tot_credential-$tot_dis_credential+$trans_rent_cost;
			
			//query to fetch type month..quarter.......
			 $query_month_type="SELECT `duration`,`session_id`,`type`
							FROM   `fee_generated_sessions` 
							WHERE `duration`=".$_GET['duration']." AND`session_id`='".$_GET['session']."'";
			$exe_month_type=mysql_query($query_month_type);
			$fetch_month_type=mysql_fetch_array($exe_month_type);
			$session=$fetch_month_type['type'];
			if($session=="Quarterly")                  //condition for session
				{
					$total_collected_fee= $total_collected_fee*3;
				}  			   
			else if($session=="Half-Yearly")
				{
					$total_collected_fee= $total_collected_fee*6;
				}
			else if($session=="Yearly")
				{
					$total_collected_fee= $total_collected_fee*12;
				}
			else if($session=="Monthly")
				{
					$total_collected_fee= $total_collected_fee;
				}
		///========get receiving fee
				//according duration and till date
	if($_GET['duration']<0)
	{
	date_default_timezone_set('Asia/Kolkata');
		    $date=date('m');
			 $month_duration=$date;
			 //show april--june
			 if((($month_duration>3)&&($month_duration<7))&&($_GET['duration']==-3))
			 {
		    $receiv="SELECT SUM(receiving_fee) FROM fee_details
			         WHERE paid=4
				AND (fee_details.fee_generated_session_id = 1)
			   ";
			 }
			 //show aprail--june--october
			else if((($month_duration>6)&&($month_duration<10))||($_GET['duration']==-1))
			 {
			$receiv="SELECT SUM(receiving_fee) FROM fee_details
			         WHERE paid=4
				AND (fee_details.fee_generated_session_id = 1 OR fee_details.fee_generated_session_id = 2)
			   ";
			 }
			  //show aprail--june--october--december
			elseif((($month_duration>9)&&($month_duration<13))&&($_GET['duration']==-3))
			 {
	     $receiv="SELECT SUM(receiving_fee) FROM fee_details
			         WHERE paid=4
				AND (fee_details.fee_generated_session_id = 1 OR fee_details.fee_generated_session_id = 2 
				OR fee_details.fee_generated_session_id = 3)
			     ";
			 }
			   //show aprail--june--october--december--jan---march
			elseif((($month_duration>0)&&($month_duration<4))||($_GET['duration']==-2))
			 {
			$receiv="SELECT SUM(receiving_fee) FROM fee_details
			         WHERE paid=4
				AND (fee_details.fee_generated_session_id = 1 OR fee_details.fee_generated_session_id = 2 
				OR fee_details.fee_generated_session_id = 3 OR fee_details.fee_generated_session_id = 7)
			    ";
			 }
	}
	else
	{
			$receiv="SELECT SUM(receiving_fee) FROM fee_details
			         WHERE paid=4
					 AND fee_generated_session_id=".$_GET['duration']."";
			
	}
			$exe_receive=mysql_query($receiv);
			$fetch_rece=mysql_fetch_array($exe_receive);																	            $receiving_fee = $fetch_rece[0]; 
			                                                                                        //=====COMPLETE COLLECTED
				                                                                                 //=====START REMANING
			$total_a= $total*3;
			$collect_a = $total_collected_fee * 3 +  $receiving_fee;
			$due_a = $total_a - $collect_a; 
			
			
			//query to get month from `dates_d` table
			$get_session="SELECT `type`, `duration`,`session_id`
						FROM `fee_generated_sessions` 
						WHERE `Id`='".$_GET['duration']."'  AND `session_id`=".$_GET['session']." 
						";  
			$execute_session=mysql_query($get_session);
			$fetch_session=mysql_fetch_array($execute_session);		 
			$session_type=$fetch_session[1];	 
			echo'		                                                                         	 
			<table  class="table table-bordered table-striped"  >
			<thead>
			<tr>			
			<th>Duration Name</th>		
			<th>Expected Fee(Rs.)</th>					
			<th>Total collected Fee(Rs.)</th>			
			<th>Due Fee(Rs.)</th>							
			</tr>
			</thead>	   	
			<tbody align="center">';
			echo'
			<tr>
			<td>'. $session_type.'</td>	
			<td>'.$total_a.'</td>
			<td><a href="accounts_consolidated_fee_report.php?du='.$_GET['duration'].'&sId=-1">'.$collect_a.'</a></td>
			<td><a href="accounts_show_defaulter_according_level.php?level=6&duration='.$_GET['duration'].'">
			'.$due_a.'</a></td>
			</tr>
			';				
			echo'
			</tbody>
			</table>     	      
			';	
			echo "
			<script type='text/javascript'>
			$(function () {
			var chart;
			$(document).ready(function() {
			var colors = Highcharts.getOptions().colors,
			categories = ['Total', 'Collected', 'Due'],
			data = [{
			y: ".$total_a.",
			color: colors[0],
			}, {
			y: ".$collect_a.",
			color: colors[1],
			}, {
			y: ".$due_a.",
			color: colors[2],
			}];
			function setChart( categories, data, color) {
			chart.xAxis[0].setCategories(categories);
			chart.addSeries({
			data: data,
			color: color || 'white'
			});
			}
			chart = new Highcharts.Chart({
			chart: {
			renderTo: 'container',
			type: 'column'
			},
			title: {                           
			text: 'Total Fee Details '
			},
			subtitle: {
			text: ''
			},
			xAxis: {
			categories: categories
			},
			yAxis: {
			title: {
			text: 'Total Fee Generate'
			}
			},
			plotOptions: {
			column: {
			cursor: 'pointer',
			point: {
			events: {
			click: function() {
			}
			}
			},
			dataLabels: {
			enabled: true,
			color: colors[0],
			style: {
			fontWeight: 'bold'
			},
			formatter: function() {
			return this.y +'Rs';
			}
			}
			}
			},
			tooltip: {
			formatter: function() {
			var point = this.point,
			s = this.x +':<b>'+ this.y +'Rs</b><br/>';
			if (point.drilldown) {
			s += 'Click to view '+ point.category +' versions';
			} else {
			s += '';
			}
			return s;
			}
			},
			series: [{
			
			data: data,
			color: 'white'
			}],
			exporting: {
			enabled: false
			}
			});
			});
			});
			</script>
			";
			echo'
			<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
			';				 	 	  	  	  
			echo'	                                  
			</div><!-- row-fluid column-->
			</div><!--  end widget-content -->
			</div><!-- widget  span12 clearfix-->
			</div><!-- row-fluid -->
			';						
		}
}

//function show accounts fee cheque details
function accounts_fee_paid_details()
{
	//get student details 
	$add=$_GET['add'];
	$sid=$_GET['sid'];
	$name=$_GET['name'];
	$duid=$_GET['duid'];
    echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th>Admission no.</th>
	<th>Student Name</th>
	<th>Class</th>
	<th>Mode of Payment</th>
	<th>Cheque No.</th>
	<th>Amounts</th>
	<th>Date</th>
	</tr>
	</thead>
	<tbody align="center">';
	   	$class="SELECT class_index.class_name
					   FROM class_index
					   INNER JOIN class
					   ON class.classId=class_index.cId
					   WHERE class.sId=".$sid."
					   ";
				$exe_class=mysql_query($class);
				$fetch_class=mysql_fetch_array($exe_class);
				$class = $fetch_class[0];

				 $cheque_details="SELECT * 
								 FROM mayo_fee_details
								 WHERE admission_no=".$add."
								 ORDER BY date ASC
								 ";
				/* $cheque_details="SELECT * 
								 FROM fee_cheque_cash_amount_details
								 WHERE student_id=".$sid."
								";*/
				$exe_ch_details=mysql_query($cheque_details);
				while($fetch_che_details=mysql_fetch_array($exe_ch_details))
				 {
					if(($fetch_che_details[3]=='cash')||($fetch_che_details[3]=="Cash"))
					{
						$mode="Cash";
						$chque_no="--";
					}
					else
					{
						$mode="Cheque";
						$chque_no=$fetch_che_details[3];
					}
				$date_q="SELECT date FROM dates_d
				         WHERE date_id=".$fetch_che_details[0]."";
				$exe_date=mysql_query($date_q);
				$fetch_date=mysql_fetch_array($exe_date);
				$date=date('d-M-Y',strtotime($fetch_date[0]));
				echo '<tr>
				<td>'.$add.'</td>
				<td>'.$name.'</td>
				<td>'.$class.'</td>
				<td>'.$mode.'</td>
				<td>'.$chque_no.'</td>
				<td>'.$fetch_che_details[2].'</td>
				<td>'.$date.'</td>';
      /*          //get paid from fee detaiuls
				$paid_status="SELECT paid
								 FROM fee_details
								 WHERE student_id=".$sid."
								 AND fee_generated_session_id=".$i."";
				$exe_paid_status=mysql_query($paid_status);
				$fetch_paid=mysql_fetch_array($exe_paid_status);
				$paid = $fetch_paid[0];
			
				 if($fetch_che_details['cheque_status'] == 1)
					{
						echo '<td style="color:green">Cheque Clear</td>';
					}
				else if($fetch_che_details['cheque_status']==2)
					{
						echo '<td style="color:red">Cheque Bounce</td>';
					}
				else if(($fetch_che_details['cheque_status']==0)&&($paid == 3))
					{
						echo '<td style="color:green">Cheque Pending</td>';
					}
				else
				   {
						echo '<td style="color:green">No Status</td>';
					}*/
				
				echo '</tr>';
				}
		 
		 
	/*}*/
	echo'
	</tbody>
	</table>
	</div>
	</div>
	</div>
	  ';		
}

//function show accounts fee cheque details
function accounts_fee_cheque_details()
{
	//get student details 
	$add=$_GET['add'];
	$sid=$_GET['sid'];
	$name=$_GET['name'];
	$duid=$_GET['duid'];
    echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	
	<th>Admission no.</th>
	<th>Student Name</th>
	<th>Duration</th>
	<th>Cheque N0</th>
	<th>Cheque Amounts</th>
	<th>Cash Amounts</th>
	<th>Date</th>
	<th>Cheque Remarks</th>
	<th>Other Discount</th>
	<th>Other Fine</th>
	<th>Cheque status</th>
	</tr>
	</thead>
	<tbody align="center">';
/*	echo '<form action="#" method="get">
	<div class="section">
	<lable>Cheque no.<small>enter cheque number</small></label>
	<div>
	<input type="text" name="cheque_no" maxlength="30">
	</div>
	</div>
	*/
	
				//query get duration 
				$duration="SELECT duration
						   FROM fee_generated_sessions
						   WHERE Id=".$duid."";
				$exe_duration=mysql_query($duration);
				$fetch_duration=mysql_fetch_array($exe_duration);
				//query get remarks details from fee_details table 
				$remarks_query="SELECT remarks, cheque_no, paid_on
					  FROM fee_details
					  WHERE student_id=".$sid."
					  AND fee_generated_session_id=".$duid."";
				$remarks=mysql_query($remarks_query);
				$fetch_remarks=mysql_fetch_array($remarks);
				$paid_on=$fetch_remarks[2];
				//get cheque detailks from fee cheque details table
			/*	if($fetch_remarks[1]=="")
				{
					
				}
				else
				{*/
				 $cheque_details="SELECT * 
								 FROM fee_cheque_cash_amount_details
								 WHERE student_id=".$sid."
								 AND duration_id=".$duid."";
				/* $cheque_details="SELECT * 
								 FROM fee_cheque_cash_amount_details
								 WHERE student_id=".$sid."
								";*/
				$exe_ch_details=mysql_query($cheque_details);
				while($fetch_che_details=mysql_fetch_array($exe_ch_details))
				{
				echo '<tr>
				<td>'.$add.'</td>
				<td>'.$name.'</td>
				<td>'.$fetch_duration[0].'</td>
				<td>'.$fetch_che_details[2].'</td>
				<td>'.$fetch_che_details[3].'</td>
				<td>'.$fetch_che_details[4].'</td>';
				$date="SELECT date FROM dates_d
				       WHERE date_id=".$paid_on."";
				$exe_date=mysql_query($date);
				$fetch_date=mysql_fetch_array($exe_date);
				$format =date('d-m-y',strtotime($fetch_date[0]));
				
				echo '
				<td>'.$format.'</td>
				<td>'.$fetch_che_details[8].'</td>
				<td>'.$fetch_che_details[6].'</td>
				<td>'.$fetch_che_details[7].'</td>';
				//get paid from fee detaiuls
				$paid_status="SELECT paid
								 FROM fee_details
								 WHERE student_id=".$sid."
								 AND fee_generated_session_id=".$duid."";
				$exe_paid_status=mysql_query($paid_status);
				$fetch_paid=mysql_fetch_array($exe_paid_status);
				$paid = $fetch_paid[0];
			
				 if($fetch_che_details['cheque_status'] == 1)
					{
						echo '<td style="color:green">Cheque Clear</td>';
					}
				else if($fetch_che_details['cheque_status']==2)
					{
						echo '<td style="color:red">Cheque Bounce</td>';
					}
				else if(($fetch_che_details['cheque_status']==0)&&($paid == 3))
					{
						echo '<td style="color:green">Cheque Pending</td>';
					}
				else
				   {
						echo '<td style="color:green">No Status</td>';
					}
				
				echo '</tr>';
				}
	
	/*}*/
	echo'
	</tbody>
	</table>
	</div>
	</div>
	</div>
	  ';		
}
//function is used to pay fee  or subbmmittted of students

function accounts_fee_submit()
{
	echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">

	<div class="widget-header">
	<span>Submit Fee</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	
	';	
	if(isset($_GET['submit']))
		{
			echo '<h4 align="center"><span style="color:green">Fee Submitted Successfully!</span></h4>';
		}	  
	if(isset($_GET['added']))
		{
			echo '<h4 align="center"><span style="color:green">Fee Generated Successfully!</span></h4>';
		}
	if(isset($_GET['clear1']))
		{
			echo '<h4 align="center"><span style="color:red">Cheque Clear!</span></h4>';
		}
	if(isset($_GET['error0']))
		{
			echo '<h4 align="center"><span style="color:red">Cheque Bounce!</span></h4>';
		}
	if(isset($_GET['error']))
		{
			echo '<h4 align="center"><span style="color:red">Error!</span></h4>';
		}
                if(isset($_GET['unvalid_ref']))
		{
			echo '<h4 align="center"><span style="color:red"> NOT VALID (Ref.No.)</span></h4>';
		}
	echo'
    <table>
	<tr>
	<td> 
	<label style="margin-left:50px">Admission No</label> 
	<input type="text" name="admission_no" id="admission_no"  class=" medium" onkeyup="subbmit_fee(this.value)" autofocus />
	</td>
	<td>
	<label style="margin-left:75px">Student Name OR Father`s Name</label>   
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="text"  name="admission_no" id="admission_no" class=" medium" onkeyup="subbmit_fee_name(this.value)"  />
	</td>
	<td>
	<label style="margin-left:120px"">Select Class</label>   
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ';
     echo ' <select name="class" id="class" onchange="shows_classes(this.value);">
	 <option value="0">SELECT CLASS</option>';
	//query get class details
	$class="SELECT * FROM class_index order by order_by ";
	$exe_class=mysql_query($class);
	while($fetch_class=mysql_fetch_array($exe_class))
		{
	echo '
	<option value="'.$fetch_class[0].'">'.$fetch_class[1].'</option>';
		}
		echo '</select></td><td>
	
	</td>
	</tr>
	</table>
	
	<div id="subbmit_fee">
	</div>
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';	
	
}
//function show subbmit fee page and fee paid
function accounts_fee_submit_class()
{

   //Author : Anil Kumar*
	//get all the admission numbers
$number_month=3;
$transport_previous=0;
$remaning_unpaid_fee=0;
$discount=0;
$remaning_fee=0;
$flag_paid_due_unpaid_1=0;
$flag_paid_due_unpaid_2=0;
$current_session=$_SESSION['current_session_id'];
$ews =1 ;

	 $get_admission_no=
		"SELECT admission_no, sId
		FROM student_user
		WHERE sId = '".$_GET['data']."'";
	$exe_aadmission=mysql_query($get_admission_no);
	if(!$exe_aadmission)
		{
                       
			return;
                        
			
		}
	else
	{
            $fetch_ad_no=mysql_fetch_array($exe_aadmission);
            $sid=$fetch_ad_no[1];
            if($fetch_ad_no[1] == $_GET['data'])
            {
            $admission_no=$fetch_ad_no[0] ;

            $query="SELECT * , class_name, class, classId
            FROM student_user
            INNER JOIN class
            ON student_user.sId = class.sId
            INNER JOIN class_index
            ON class.classId = class_index.cId
            WHERE admission_no ='".$admission_no."'
            ";
            $execute=mysql_query($query);
            $fetch=mysql_fetch_array($execute);
            $student_id = $fetch['sId'];
            $class = $fetch['class'];
		$cid = $fetch['classId'];
            //get duration from fe generated session 
            $query_duration ="SELECT *
                              FROM fee_generated_sessions
                                            ";                          
            $execute_duration=mysql_query($query_duration);
            echo' 
            <!-- Widget -->
            <table align="center" class="table table-bordered table-striped" id="dataTable">
            <tr height="50%" align="center">
            <td rowspan="2" width="10%"><img src="user_image/'.$fetch['image'].'" style="height:100px" style="width:100px"/></td>  
            <td width="10%">ADMISSION NO</td>  
            <td width="10%">NAME</td>  
            <td width="10%">CLASS</td>  
            <td width="10%">DOB</td>  
            <td width="10%">GENDER</td>  
            <td width="10%">FATHER</td>  
            <td width="10%">MOTHER</td>  
            <td width="10%">PHONE   </td>
            </tr>
            <tr height="50%" align="center">
            <td>'.$fetch['admission_no'].'</td>
            <td>'.$fetch['Name'].'</td>  
            <td >'.$fetch['class_name'].'</td>  
            <td>'.$fetch['DOB'].'</td>  
            <td>'.$fetch['gender'].'</td>  
            <td>'.$fetch['Father\'s Name'].'</td>  
            <td>'.$fetch['Mother\'s Name'].'</td>  
            <td>'.$fetch['Phone No'].'</td>   
            </tr>
            </table>';



////////////////////////////////////        SELECT  MONTH  ***************************************
	echo '<b style="color:blue">SELECT MONTH</b>';

	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '<select name="number_month" class="small"  id="number_month" onchange="two_month_fee(\''.$admission_no.'\',this.value);">
	<option value="3">3 MONTH</option>
	<option value="1">1 MONTH</option>
	<option value="2">2 MONTH</option>
	
	<option value="4">4 MONTH</option>
	<option value="5">5 MONTH</option>
	<option value="6">6 MONTH</option>
	<option value="7">7 MONTH</option>
	<option value="8">8 MONTH</option>
	<option value="9">9 MONTH</option>
	<option value="10">10 MONTH</option>
	<option value="11">11 MONTH</option>
	<option value="12">12 MONTH</option>
	';
	echo'   </select> ';
////////////////////////////////////        SELECT  MONTH  ***************************************

           echo ' 
		
	 <form name="fee" id="form1" method="post" action="fee/update_subbmit_fee_details.php?add='.$admission_no.'">'; 
         echo '<div id="show_quterly_fee">';

          
           
              
            // GET PAID  ON DATE  ID
            date_default_timezone_set('Asia/Kolkata');
            $date=date('d-m-Y');
            
             $flag_dates=0;
             $format=date('Y-m-d',strtotime($date));
             $pre_date11="SELECT date_id FROM dates_d
                          WHERE date='".$format."'";
             $exe_pre_date11=mysql_query($pre_date11);
             $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
             $paid_on_date_id=$fetch_pre_date11[0];
            // GET FIRST MONTH OR STARTING MONTH OF COLLECTING FEE
            $dur_id_min="SELECT MAX(receipt_id) FROM fee_receipt_details WHERE sid=".$student_id." 
			 AND session_id=".$current_session." AND cheque_status=0";
            $exe_du_min_id=mysql_query($dur_id_min);
	    if(!$exe_du_min_id)
		{
			$fm=1;
		}
	    else
		{
            		$fetch_min_id = mysql_fetch_array($exe_du_min_id);
			$duration_val = "SELECT * FROM fee_receipt_details WHERE receipt_id=".$fetch_min_id[0]."";
			$exe_du_val = mysql_query($duration_val);
			if(!$exe_du_val)
			{
				$fm=1;

			}
			else
			{
				$fetch_du_val = mysql_fetch_array($exe_du_val);
				$discount = $fetch_du_val['discount'];
				$dues = $fetch_du_val['due'];
				$fm = $fetch_du_val[10] + 1;
			}

		}
		// GET LAST OR END MONTH OF COLLECTION OF FEEE..
             	$lm = $fm + $number_month - 1;
            
                //GET DURATION NAME AND last ON DATE ID 
		$duration_fm="SELECT * FROM fee_generated_sessions WHERE Id=".$fm."";
		$exe_fm=mysql_query($duration_fm);
		$fetch_fm=mysql_fetch_array($exe_fm);

		$duration_lm="SELECT * FROM fee_generated_sessions WHERE Id=".$lm."";
		$exe_lm=mysql_query($duration_lm);
		$fetch_lm=mysql_fetch_array($exe_lm);

		$durations_name=$fetch_fm[2]."-".$fetch_lm[2];
		$last_date_id=$fetch_fm[3];
		//CALCULATE FINE

		if($last_date_id < $paid_on_date_id)
		{
		    $fine_days = $paid_on_date_id - $last_date_id;
		    $fine = $fine_days * 10;
		}
		else 
		{
		    $fine=0;
		}
		// GET EWS OR NOT
		$ews_sid = "SELECT * FROM ews_students";
		$exe_ews = mysql_query($ews_sid);
		while($fetch_ews = mysql_fetch_array($exe_ews))
		{
			if($fetch_ews[1] == $student_id)
			{
				$fine=0;
				$ews = 0;
				break;
			}
		}
      	$fine=0;
              //###################################################################################################
                                              //  STATUS   LASER  SHOW
	 if($fm<13)
	{          
	 	echo '<h5 align="center" style="color:purple" onclick="show_paid_details();"><a><u>'.$durations_name.'</u></a></h5>';     
	} 
	else
	{
		echo '<h5 align="center" style="color:purple" onclick="show_paid_details();"><a><u>FEE PAID APRIL TO MARCH</u></a></h5>';
	}  
           echo ' <div style="display:none" id="sop">';
           echo '<table width="100%" border=1>
           <thead><tr >
           <th>Receipt No.</th>
           <th>Date</th>
           <th>Paid by</th>
           <th>Duration</th>
   
           <th style="color: red">Annual Charge</th>

           <th style="color: green">Tution Fee</th>
           <th style="color: green">Dev. Charge</th>
       
           <th style="color: blue">Dis.</th>
           <th style="color: blue">Fine</th>

           <th style="color: red">Total</th>
           <th style="color: red">Paid</th>
           <th style="color: red">Due</th>
<th style="color: red">Status</th>
           </tr></thead><tbody align="center">';
             $fee_receipt="SELECT * FROM fee_receipt_details WHERE sid=".$student_id." AND session_id=".$_SESSION['current_session_id']."";
             $exe_fee_rec=mysql_query($fee_receipt);
             while($fetch_fee_rec=mysql_fetch_array($exe_fee_rec))
             {
                echo '
                     <tr>
                     <td>'.$fetch_fee_rec[1].'</td>';
                 
                     $dateid="SELECT date FROM dates_d WHERE date_id='".$fetch_fee_rec['date']."'";
                       $exe_dates=mysql_query($dateid);
                     $fetch_dates=mysql_fetch_array($exe_dates);
                        $paiddates=$fetch_dates[0];
                        $format_1=date('d-m-Y',strtotime($paiddates));
                       echo '  <td>'.$format_1.'</td>';
                       
                         echo '    <td>'.$fetch_fee_rec['mode'].'</td>';
                         echo '  <td>'.$fetch_fee_rec['duration_name'].'</td>';

                         $amount_total=$fetch_fee_rec['amounts']+$fetch_fee_rec['due'];                   
echo '    
<td style="color: red">'.$fetch_fee_rec['annual_1'].'</td>


<td style="color: green">'.$fetch_fee_rec['month_1'].'</td>
<td style="color: green">'.$fetch_fee_rec['month_2'].'</td>


<td style="color: blue">'.$fetch_fee_rec['discount'].'</td>
<td style="color: blue">'.$fetch_fee_rec['fine'].'</td>

<td style="color: red">'.$amount_total.'</td>
<td style="color: red">'.$fetch_fee_rec['amounts'].'</td>
<td style="color: red">'.$fetch_fee_rec['due'].'</td>   
  ';
if($fetch_fee_rec['cheque_status']==0)
{
echo ' <td style="color: green">Clear</td>  ';
}
else
{
echo '<td style="color: red">Cancel</td>  ';
}
      echo '
                 </tr>';
             }
             echo '  </tbody></table>          ';
////////////////////////////////////////  TRANSPORT ////////////////
echo '<h4 align="center" style="color: purple"> <u> <u>TRANSPORT  DETAILS</u></u></h2>';
 echo '<table width="100%" border=1>
           <thead><tr >
           <th>Receipt No.</th>
           <th>Date</th>
           <th>Paid by</th>
           <th>Duration</th>
           <th> Transport Charge</th>

           </tr></thead><tbody align="center">';
             $fee_receipt="SELECT * FROM fee_receipt_details_transport WHERE sid=".$student_id." AND session_id=".$_SESSION['current_session_id']."";
             $exe_fee_rec=mysql_query($fee_receipt);
             while($fetch_fee_rec=mysql_fetch_array($exe_fee_rec))
             {
                echo '
                     <tr>
                     <td style="color: #fa8072"><b>'.$fetch_fee_rec[1].'</td>';
                 
                     $dateid="SELECT date FROM dates_d WHERE date_id='".$fetch_fee_rec['date']."'";
                       $exe_dates=mysql_query($dateid);
                     $fetch_dates=mysql_fetch_array($exe_dates);
                        $paiddates=$fetch_dates[0];
                        $format_1=date('d-m-Y',strtotime($paiddates));
                       echo '  <td style="color: #fa8072"><b>'.$format_1.'</td>';
                       
                         echo '    <td style="color: #fa8072"><b>'.$fetch_fee_rec['mode'].'</td>';
                         echo '  <td style="color: #fa8072"><b>'.$fetch_fee_rec['duration_name'].'</td>';                  
			echo '    
			<td  style="color: #fa8072"><b>'.$fetch_fee_rec['amounts'].'</b></td>  
                 </tr>';
             }
             echo '  </tbody></table> <br> </div>           ';


        
       
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if($fm <13)
{
	echo '
	<div>
	<input type="checkbox" name="cheque" onclick="show_textbox(2)" id="ch_2">&nbsp;<b style="color: #8a2be2 "><u>CHEQUE</u></b>
	<div align="center" id="val_2"></div>
	</div>';


	//echo'<label>Enter UTRN NO.:<input type="textbox" name="utrn_no"/></label><br><label>Bank Name:<input type="text" name="bank" id="bank"/></label><br><label>Branch Name:<input type="text" name="branch" id="branch"/></label>';

	echo'     <input type="hidden" name="cr_id1" id="fee_credentials2"/>
	<input type="hidden" name="cr_id2" id="fee_credentials1"/>';


	$sum_annual=0;
	//GET SUM ANNUAL
	$sum_annual ="SELECT SUM(only_annual) FROM fee_receipt_details WHERE sid =".$student_id."";
	$exe_sum_ann = mysql_query($sum_annual);
	$fetch_sum_ann = mysql_fetch_array($exe_sum_ann);
	$sum_annual = $fetch_sum_ann[0];
	// GET ANNUAL DETAILS
	$annual = "SELECT * FROM fee_charges_name WHERE charge_type = 1";
	$exe_annual = mysql_query($annual);
	$fetch_annual = mysql_fetch_array($exe_annual);  
	$year = $fetch_annual[4];
	//GET HALF YERALY DETAILS
	$bi_annual = "SELECT * FROM fee_charges_name WHERE charge_type = 2";
	$exe_bi_annual = mysql_query($bi_annual);
	$fetch_bi_annual = mysql_fetch_array($exe_bi_annual); 
	$half_year_1 = $fetch_bi_annual[4];
	$half_year_2 = $fetch_bi_annual[5];
/*
//GET FEE CHARGESS DETAILS  ACCORDING TO MONTH
//get all  fee
//  YEARLY
if(($sum_annual == 0)&&($fm <= $year)&&($lm >= $year)&& ((($fm <= $half_year_1)&&($lm >= $half_year_2))||(($fm <= $half_year_2)&&($lm >= $half_year_2)) ) )
{
	$charge="SELECT * FROM fee_charges_structure WHERE fee_type_id < 3 AND class =".$cid."";

}
//YEARLY
elseif (($sum_annual == 0)&&($fm <= $year)&&($lm >= $year))
{

	$charge="SELECT * FROM fee_charges_structure WHERE fee_type_id < 2 AND class =".$cid."";
}
//HALFYEARLY
elseif ( (($fm <= $half_year_1)&&($lm >= $half_year_2))||(($fm <= $half_year_2)&&($lm >= $half_year_2)) )
{
	$charge="SELECT * FROM fee_charges_structure WHERE (fee_type_id = 0 OR fee_type_id = 3 ) AND class =".$cid."";

}
//MONTHLY
else
{

	$charge="SELECT * FROM fee_charges_structure WHERE fee_type_id = 0 AND class =".$cid."";
}

*/

//********************************************************************************************************************************
//////////////////////////  GET  FEE CHARGES NAME AND AMOUNTS  //////////////////////////////////////////////////////////////////
//TUTION FEE
$mtf ="SELECT * FROM fee_charges_structure WHERE fee_name_id = 1 AND class =".$cid."";
$exe_mtf =mysql_query($mtf);
$fetch_mtf =mysql_fetch_array($exe_mtf);
//DEVELOPMENT CHARGE
$mdf ="SELECT * FROM fee_charges_structure WHERE fee_name_id = 2 AND class =".$cid."";
$exe_mdf =mysql_query($mdf);
$fetch_mdf =mysql_fetch_array($exe_mdf);


/////////////////////////////////////////  ANNUAL FEE//////////////////////
if(($sum_annual == 0)&&($fm <= $year)&&($lm >= $year))
{
//ANNUAL
//if($cid==70)
//$aaf ="SELECT * FROM fee_charges_structure WHERE fee_name_id = 4 AND class =".$cid."";
//else
$aaf ="SELECT * FROM fee_charges_structure WHERE fee_name_id = 3 AND class =".$cid."";
$exe_aaf =mysql_query($aaf);
$fetch_aaf =mysql_fetch_array($exe_aaf);


}



///////////////////////////////////////////  TRANSPORT //////////////////////////
$transport=0;
$trans_amount=" SELECT cost
		FROM destination_route

		INNER JOIN user_vehicle_details
		ON user_vehicle_details.did=destination_route.did

		WHERE user_vehicle_details.uId=".$student_id."
		AND user_vehicle_details.status=1
		AND destination_route.session_id=".$_SESSION['current_session_id']."";
$exe_amount_t=mysql_query($trans_amount);
if(!$exe_amount_t)
{
	$transport=0;
}
else 
{
	$fetch_transport_cost=mysql_fetch_array($exe_amount_t);
	$transport = $fetch_transport_cost[0];
}
// GET FIRST MONTH OR STARTING MONTH OF COLLECTING FEE
$dur_id_tr="SELECT MAX(receipt_id) FROM fee_receipt_details_transport WHERE sid=".$student_id." 
	 AND session_id=".$current_session." AND cheque_status=0";
$exe_du_min_tr=mysql_query($dur_id_tr);
if(!$exe_du_min_tr)
{
	
		$uid="SELECT * FROM user_vehicle_details WHERE uId=".$student_id."";
		$exe_uid=mysql_query($uid);
		$fetch_uid=mysql_fetch_array($exe_uid); 
		$lm_tr = $fetch_uid['from']-1;
}
else
{
	$fetch_min_tr = mysql_fetch_array($exe_du_min_tr);
	$duration_val_tr = "SELECT * FROM fee_receipt_details_transport WHERE receipt_id=".$fetch_min_tr[0]."";
	$exe_du_val_tr = mysql_query($duration_val_tr);
	if(!$exe_du_val_tr)
	{
		
		$uid="SELECT * FROM user_vehicle_details WHERE uId=".$student_id."";
		$exe_uid=mysql_query($uid);
		$fetch_uid=mysql_fetch_array($exe_uid); 
		$lm_tr = $fetch_uid['from']-1;

	}
	else
	{
		$fetch_du_val_tr = mysql_fetch_array($exe_du_val_tr);		
		$lm_tr = $fetch_du_val_tr[10];
	}

}

$tr_number_month = $lm - $lm_tr ;
if($tr_number_month < 0)
{
	$tr_number_month = 0;
}
if($transport == 0)
{
	$tr_month = "Not use";
}
else
{
	$tr_month = $tr_number_month ."-Month";
}

///////////////////////////////////////////////   VIEW  IN TABLE ////////////////////////////
echo'
<table  class="table table-bordered table-striped" align="center" font-size="18">
	<thead >
		<tr>';
		if($dues > 0)
		{
			echo '<th style="color:red">Dues Balance</th>';
		}
		if(($sum_annual == 0)&&($fm <= $year)&&($lm >= $year))
		{
			echo '  <th>Annual Charge </th>
				
				
			      ';$a2 =0;
		}
		else
		{
			$a1 =0;
			$a2 =0;
		}
		if ( (($fm <= $half_year_1)&&($lm >= $half_year_2))||(($fm <= $half_year_2)&&($lm >= $half_year_2)) )
		{
			$hyf =0;
			//echo '
				//<th>Computer Fee </th>
			//';
		}///////
		else
		{
			$hyf =0;
		}
		echo '
				<th > Tution Fee</th>
				<th>Development Charge </th>
				
				<th style="color:red">Transport Charge<b style="color:blue">('.$tr_month.')</b></th>
				<th STYLE="color:green">Discount </th>
				<th STYLE="color:green">Fine </th>

				
		';
		echo '</tr>
	</thead>
	
	<tbody align="center">
		<tr>';
if($dues > 0)
{
	echo '<td ><input type="text" name="d10" onkeyup="radio_get_total();" id="d10" value="'.$dues.'" style="width:60px;color:red;"></td>';
}
else
{
	echo '<input type="hidden" name="d10" onkeyup="radio_get_total();" id="d10" value="0" >';
}
		if(($sum_annual == 0)&&($fm <= $year)&&($lm >= $year))
		{
			$a1 = $fetch_aaf[4] * $ews ;
			$a2 = $fetch_aef[4] * $ews ;
			echo '  <td><input type="text" name="a1" onkeyup="radio_get_total();" id="a1" value="'.$a1.'" style="width:60px;">  </td>
				<input type="hidden" name="a2" onkeyup="radio_get_total();" id="a2" value="'.$a2.'" style="width:60px;"> 
				
			      ';
		}
		else
		{
			echo '  <input type="hidden" name="a1" id="a1" value="0" style="width:60px;"> 
				<input type="hidden" name="a2" id="a2" value="0" style="width:60px;">
				
					      ';
		}
		
		if ( (($fm <= $half_year_1)&&($lm >= $half_year_2))||(($fm <= $half_year_2)&&($lm >= $half_year_2)) )
		{
			$hyf = $fetch_hyf[4] * $ews ;
			echo '
				<input type="hidden" name="h1" id="h1" onkeyup="radio_get_total();" value="'.$hyf.'" style="width:60px;">
			';
		}///////
		else
		{
		          echo '
						<input type="hidden" name="h1" id="h1" value="0" style="width:60px;">
					';
		}
		

		// CALCULATE  FEE  ACCORDING TO NUMBER OF MONTH
		$tr = $transport * $tr_number_month;

		$m1 = $fetch_mtf[4] * $number_month * $ews ;
		$m2 = $fetch_mdf[4] * $number_month * $ews ;
		//$m3 = $fetch_maf[4] * $number_month * $ews ;
		$m3 =0;
		echo '
			<td><input type="text" name="m1" onkeyup="radio_get_total();" required="required" id="m1" value="'.$m1.'" style="width:60px;"> </td>
			<td><input type="text" name="m2" onkeyup="radio_get_total();" id="m2" value="'.$m2.'" style="width:60px;"> </td>
			<input type="hidden" name="m3" onkeyup="radio_get_total();" id="m3" value="'.$m3.'" style="width:60px;">
			<td><input type="text"  name="m4" onkeyup="radio_get_total();" id="m4" value="'.$tr.'" style="width:60px;color:red;"></td>
			<td ><input type="text" name="m5" onkeyup="radio_get_total();" id="m5" value="'.$discount.'" style="width:60px;color:green;"></td>
			<td STYLE=""><input type="text" onkeyup="radio_get_total();" name="m6" id="m6" value="'.$fine.'" style="width:60px;color:green;"></td>
				
		';


             echo '</tr>
	</tbody>
</table>';

	 //echo $discount;
	$admn="SELECT * FROM student_user WHERE sId=".$student_id." ";
$exe_admn=mysql_query($admn);
$fetch_admn=mysql_fetch_array($exe_admn);

if(($fm == 1)&&($fetch_admn['admission_no'] >10322))
{
	$admission =200;
	echo '
	<input type="hidden" style="width:60px;" onkeyup="admission_show();" id="ad3"  name="ad3" value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	ADMISSION FEE<input type="text" style="width:60px;" onkeyup="admission_show();" id="ad1" name="ad1" value="200">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="hidden" style="width:60px;" onkeyup="admission_show();" id="ad2" name="ad2" value="0">';

}
else
{

	$admission =0;
	echo '
	<input type="hidden" style="width:60px;" onkeyup="admission_show();" id="ad3"  name="ad3" value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	ADMISSION FEE<input type="TEXT" style="width:60px;" onkeyup="admission_show();" id="ad1" name="ad1" value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="hidden" style="width:60px;" onkeyup="admission_show();" id="ad2" name="ad2" value="0">';


}

	 //echo $discount;
	 $total_fee = $m1 +$m2 +$m3+ $a1+ $a2+ $hyf +$fine+ $tr -$discount + $dues + $admission;    


 
//      //       //    //   SHOW  TOTAL AND CALCULATE  //    //    //      //           //   //
echo '<b style="color:RED">TOTAL  AMOUNTS:
<input type="text" name="change_no_total" id="change_no_total" style="width:90px;color:red" disabled="disabled" value="'.$total_fee.'"></b>';
//  HIDDEN  TOTALS
echo '
<input type="hidden" name="change_total" id="change_total"  value="'.$total_fee.'">';
echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
//GET  TOTALS
echo '<b style="color:GREEN">RECEIVING  AMOUNTS:
<input type="text" name="get_total" id="get_total" style="width:100px;color:green" value="'.$total_fee.'"></b>';

echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
//ONCLICK  TOTALS
//echo '<b style="color:blue">ADD  TOTAL: &nbsp;&nbsp;
 //<input type="radio" id="radio" name="radio_total" onClick="radio_get_total()"  >';
 echo '<br><br>';
  //      //       //    //  DATE  AND   RECEIPT   NUMBER  //    //    //      //           // 
	$rec_id="SELECT MAX(receipt_id) FROM fee_receipt_details";
	$exe_rec=mysql_query($rec_id);
	$fetch_rec=mysql_fetch_array($exe_rec);
	$rec_ids=$fetch_rec[0];
	$r=$rec_ids+1;

 	 $rec_id_tr="SELECT MAX(receipt_id) FROM fee_receipt_details_transport";
           $exe_rec_tr=mysql_query($rec_id_tr);
           $fetch_rec_tr=mysql_fetch_array($exe_rec_tr);
           $rec_ids_tr=$fetch_rec_tr[0];
           $tr = $rec_ids_tr + 1; 

	echo '<b style="color:#C71585">Receipt.No.:<input type="text" name="ref_no" style="width:50px;" disabled="disabled" value="'.$r.' / '.$tr.'">';

	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
	$format=date('d-m-Y',strtotime($date));
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

	echo 'DATE : <input type="date" name="date" style="width:130px;color:#800080;" value="'.$format.'"  >';

	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

	echo '<input type="submit" value="COLLECT">';
	echo '<br><br><br>';
        
//        echo 'TUTION FEE DISCOUNT&nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" name="tution_fee_discount" value="0" style="width:60px;">';
	
	echo '<b style="color:#8a2be2;" ><u>ALL  FEE</u>&nbsp;&nbsp;<input type="radio" id="t1" name="fee_type" checked="checked"  value="1">';         
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '  <b style="color:#8a2be2;" ><u> SCHOOL  FEE</u>&nbsp;&nbsp;<input type="radio" id="t2" name="fee_type"   value="2">';    
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '<u>  TRANSPORT  FEE</u>&nbsp;&nbsp;<input type="radio" id="t3" name="fee_type"   value="3">';  
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	if($dues > 0)
	{
		echo '<u>  DUES BALANCE</u>&nbsp;&nbsp;<input type="radio" id="t4" name="fee_type"   value="4">';  
	}

}
else
{
echo '<br><br><br><h2 align="center" style="color:#9400d3">  THANK YOU !  ('.$fetch['Name'].')</h2>';

}
      
  //echo $durations_name;echo $quter_value;echo '<br>';echo $quter;
       
              echo ' 


<input type="hidden" name="sid" value="'.$student_id.'"> 

<input type="hidden" name="fm" value="'.$fm.'"> 

<input type="hidden" name="lm" value="'.$lm.'"> 
                  
<input type="hidden" name="du" value="'.$durations_name.'"> 

<input type="hidden" name="ews" value="'.$ews.'"> 
<input type="hidden" name="transport_val" value="'.$transport.'"> 
<input type="hidden" name="lm_tr" value="'.$lm_tr.'"> 

</div>

       </form>

       ';
       
         
                 }//if close
        }//else
        

        

         
             
}
//function print receipt
function accounts_fee_receipt_print_new()
{
	//Author : Anil Kumar*
	$remaning_fee_in_table=$_GET['remaning_fee_in_table'];
        $re_fe=$_GET['re_fe'];
	$duration=$_GET['duration'];
	$ad_no=$_GET['ad_no'];
	$session_id=$_SESSION['current_session_id'];
if($duration>1000)
{
    echo '<h4 style="color:green">successfully paid fee and complete fee receipt</h4>';
}
else 
{
	//query get student id from student_user
	$sid_query="SELECT sId, Name 
	            FROM student_user
				WHERE admission_no=".$ad_no;
	$exe_sid=mysql_query($sid_query);
	$fetch_sid=mysql_fetch_array($exe_sid);
	$student_id=$fetch_sid[0];
	//query get session id
	 $session_id_ge="SELECT session_id, last_date 
	                FROM fee_generated_sessions
					WHERE Id=".$duration."";
	$exe_se=mysql_query($session_id_ge);
	$fetch=mysql_fetch_array($exe_se);
	$current_session=$fetch[0];
	$due_date=$fetch[1];
	//query get paid on date
	$paid_date="SELECT paid_on
	           FROM fee_details
			   WHERE student_id=".$student_id."
			   AND fee_generated_session_id=".$duration."";
	$exe_date=mysql_query($paid_date);
	$fetch_dates=mysql_fetch_array($exe_date);
	$paid_date=$fetch_dates[0];
	/*$format=date('Y-m-d',strtotime($date));*/
	//querey get fee details from table name=>fee_detais
	 $query_fee="SELECT * 
	            FROM fee_details
			   WHERE student_id=".$student_id."
			   AND fee_generated_session_id=".$duration."";
	$exe_fee=mysql_query($query_fee);
	//calculate fine
	//query get date id from dates_d
/*	echo $date_id="SELECT date_id
	          FROM dates_d
			  WHERE date='".$date."'";
	$execute_date=mysql_query($date_id);
	$fetch_date=mysql_fetch_array($execute_date);
	$paid_date=$fetch_date[0];*/
	//define fine
	$fine="fine";
	//query get fine amount/days
    $query_fine="SELECT amount
	             FROM fee_credentials
				 WHERE type='".$fine."'
				 AND session_id=".$_SESSION['current_session_id']." ";
	$exe_fine=mysql_query($query_fine);
	$fetch_fine=mysql_fetch_array($exe_fine);
	$fine_amount=$fetch_fine[0];
	//check condition due date < paid on date
	//and calculate fine
	if($due_date<$paid_date)
		{
			$fine_days=$paid_date-$due_date;
		     	if($fine_days<=30)
				{
					$calculate_fine=100;
				}
				else if($fine_days<=60)
				{
					$calculate_fine=200;
				}
				else
				{
					$calculate_fine=500;
				}
		}
	else
		{
			$calculate_fine=0;  
		}
	//Query to get the class of the user student
	$query_class_student = "SELECT DISTINCT class_index.class_name
							FROM class_index
			
							INNER JOIN class
							ON class.classId = class_index.cId
			
							WHERE class.sId = '".$student_id."'
							";
	$execute_class_student = mysql_query($query_class_student);
	$class_student = mysql_fetch_array($execute_class_student);
	//write query to get the session id from  the fee generated session
	$fetch_session_name="SELECT `session_name`
						 FROM `session_table` 
						 WHERE `sId`=".$session_id."";
	$execute_session_name=mysql_query($fetch_session_name);
	$fetch_session=mysql_fetch_array($execute_session_name);
	$session_name=$current_session['session_name'];
	//select duration name
	$dura="SELECT duration FROM fee_generated_sessions
	       WHERE Id=".$duration."";
	$exe_dura=mysql_query($dura);
	$fetch_dura=mysql_fetch_array($exe_dura);

	    echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Receipt </span>
	</div><!-- End widget-header -->	
	<div class="widget-content" id="d_'.$student_id.'">
	<table  class="table table-bordered table-striped" align="center" >';
	//get name of the school
	$get_name_school=
					"SELECT `name_school`
					FROM `school_names`
					WHERE `Id`= 3";
	$exe_name_school=mysql_query($get_name_school);
	$fetch_school_name=mysql_fetch_array($exe_name_school);
        echo '<h3  align="center" style="color:red">';
	echo $fetch_school_name['name_school'];
	echo ' </h3>
	
	
	 <label align="center"> Current Session : '.$fetch_session['session_name'].'</label>
	 <label align="center"> Duration : '.$fetch_dura[0].'</label>
             <b>Student : '.$fetch_sid[1].'</b>&nbsp;
	<b>Class : '.$class_student[0].'</b>
	<table  class="table table-bordered table-striped">';
	echo '<thead>
	<tr>
	<th align="center" width="5%">S.No.</th>
	<th align="left" width="50%">Tution Fee Type</th>
	<th align="left" width="45%">Amount</th>
	</tr>
	</thead>';
	$total_discount=0;
	$camount=0;	
	$serial_no=1;
	$fetch_fee=mysql_fetch_array($exe_fee);

	
	//QUERY get cost of transport from destination_rouite------
	$tranport_cost=0;
	$trans_amount="SELECT cost
	               FROM destination_route
				   
				   INNER JOIN user_vehicle_details
				   ON user_vehicle_details.did=destination_route.did
				   
				   WHERE user_vehicle_details.uId=$student_id
				   AND destination_route.session_id=".$_SESSION['current_session_id']."";
	$exe_amount_t=mysql_query($trans_amount);
	while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
		{
			$tranport_cost=$tranport_cost+$fetch_transport_cost[0];
		}
	//query get rent of hostel from room allocation --------
	$hostel_rent=0;
	$hostel_amount="SELECT rent 
	                FROM hostel_room_allocation
					WHERE sid=$student_id
					AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
	$exe_rent=mysql_query($hostel_amount);
	while($fetch_rent_cost=mysql_fetch_array($exe_rent))
		{
			$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
		}
	$trans_rent_cost=$hostel_rent+$tranport_cost;

	echo '<tbody>';
	//start query to fetch credential type and amount
	//and calculate total credential amount
	$security=1;
	$fetch_credentials_type="SELECT DISTINCT fee_credentials.type,fee_credentials.amount
							FROM fee_credentials
							INNER JOIN fee_details
							ON fee_details.fee_credential_id=fee_credentials.id
							WHERE fee_details.student_id=".$student_id." 
							AND fee_credentials.session_id=".$current_session."
							AND fee_details.fee_generated_session_id=".$duration."";
	$execute_credentials_type=mysql_query($fetch_credentials_type);
	while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
		{
			$camount=$camount+$fetch_credentials['amount'];
			//first latter of credential type convert small to capital
			$c=str_split($fetch_credentials['type']);
			$upper=strtoupper($c[0]);
			$count=count($c);
			echo '	<tr>
			<td align="center">'.$serial_no++.'</td>
			<td>';
			echo $upper;
			for($i=1;$i<$count;$i++)
				{
					echo $c[$i];  
				}
				
			echo '   <td>'.$fetch_credentials['amount'].'</td></tr>';  
				
				
		}
		 
	//calculate credentials with hostel fee and teransport fee
	$credential_amount=$camount+$trans_rent_cost;
	if($tranport_cost!=0)
	{
	echo '<tr>
	<td align="center">'.$serial_no++.'</td>
	<td>Transport Fee</td>
	<td>'.$tranport_cost.'</td></tr>';
	}
	if($hostel_rent!=0)
	{
	echo '<tr>
	<td align="center">'.$serial_no++.'</td>
	<td>Hostel Fee</td>
	<td>'.$hostel_rent.'</td></tr>
	';
	}
		// and calculate total discount amount
	$serial_no=1;
	$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
						  FROM fee_discount_credentials
						  INNER JOIN fee_details
						  ON fee_details.fee_discount_id=fee_discount_credentials.id
						  WHERE fee_details.student_id=".$student_id."
						  AND fee_discount_credentials.session_id=".$current_session."
						  AND fee_details.fee_generated_session_id=".$duration."";
	$execute_discount_type=mysql_query(  $fetch_discount_query);
	while($fetch_discount=mysql_fetch_array($execute_discount_type))
		{ 
			$total_discount=$total_discount+$fetch_discount['amount'];
		}
		if($total_discount!=0)
		{
	echo ' <table  class="table table-bordered table-striped">
	<thead>
	<tr>
	<th align="center" width="5%">S.No.</th>
	<th align="left" width="50%">Discount Type</th>
	<th align="left" width="45%">Amount</th>
	</tr>
	</thead>';
		}
	echo '<tbody>';
	//query to fetch discoun type of student and amount
	// and calculate total discount amount
	$serial_no=1;
	$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
						  FROM fee_discount_credentials
						  INNER JOIN fee_details
						  ON fee_details.fee_discount_id=fee_discount_credentials.id
						  WHERE fee_details.student_id=".$student_id."
						  AND fee_discount_credentials.session_id=".$current_session."
						  AND fee_details.fee_generated_session_id=".$duration."";
	$execute_discount_type=mysql_query(  $fetch_discount_query);
	while($fetch_discount=mysql_fetch_array($execute_discount_type))
		{ 
			$total_discount=$total_discount+$fetch_discount['amount'];
			//first latter of discount type convert small to capital
			$d=str_split($fetch_discount['type']);
			$upper=strtoupper($d[0]);
			$c=count($d);
			echo '<tr>
			<td align="center">'.$serial_no++.'</td>
			<td>';
			echo $upper;
			for($i=1;$i<$c;$i++)
				{
					echo $d[$i];  
				}
			echo '  <td>'.$fetch_discount['amount'].'</td>
			</tr>';
		}
	echo '
	<table  class="table table-bordered table-striped"  cellpadding="" cellspacing="0">
	<thead>
	</thead>
	<tbody>';
	//write query to get id and type from fee generated session id 
	$query_fee_ge_session_id="SELECT DISTINCT `type`, last_date, duration
							  FROM `fee_generated_sessions`
							  WHERE Id=".$duration."";
	$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
	$fetch_fee_ge=mysql_fetch_array($execute_fee_ge_se_id);
	$month=$fetch_fee_ge['type'];
		
	$tfamount=$credential_amount-$total_discount;
	//calculate fee amount according to duration
	//Monthly=1  Quarterly-3, half early=6 and annually=12
	$m=1;
	if($month=="Monthly")
		{
			$m=1; 
		}
	else if($month=="Quarterly")
		{
			$m=3; 
		}
	else if($month=="Half-Yearly")
		{
			 $m=6;  
		}	
	else if($month=="Yearly")
		{
			$m=12;  
		}
		
	//total fee according duration and with fine
	$fee_quterly = $tfamount * $m;
	$total_fee_amount_fine=$tfamount*$m+ $calculate_fine;
/*	//query get remaning fee from fee details
	$remaning_old=0;
	$query="SELECT remaning_fee
	            FROM fee_details
				WHERE student_id=".$student_id."
				AND fee_generated_session_id<>".$duration."";
	$exe_fe=mysql_query($query);
	while($fetch_fe=mysql_fetch_array($exe_fe))
	{
	$remaning_old=$fetch_fe[0];
	}*/
	//previous duration remaning fees
		//calculate other duration fee if fee not pay in previous duration       =====previous duration remaning fee=====
		date_default_timezone_set('Asia/Kolkata');
		$today_date=date('Y-m-d');
		$format_today=date('Y-m-d',strtotime($today_date));
		//get date id from date _d
		 $pre_date="SELECT date_id FROM dates_d
		           WHERE date='".$format_today."'";
		$exe_pre_date=mysql_query($pre_date);
		$fetch_pre_date=mysql_fetch_array($exe_pre_date);
		$pre_ddate=$fetch_pre_date[0];
		$total_previous_remaning_fee=0;
		$fine_previoun =0;
		$other_duration_remaning_fee="SELECT * FROM fee_generated_sessions
		                              WHERE Id<>".$duration."
									  AND last_date<=".$due_date."";
									 
		$exe_other_duration_remaning_fee=mysql_query($other_duration_remaning_fee);
		while($fetch_other_duration_remaning_fee=mysql_fetch_array($exe_other_duration_remaning_fee))
			{
			   $duration_previous=$fetch_other_duration_remaning_fee[0];
			   //calculate fee
			   $previous_paid="SELECT paid, receiving_fee FROM fee_details
							   WHERE student_id=".$student_id."
							   AND fee_generated_session_id=".$duration_previous."";
			  $exe_pre_paid=mysql_query($previous_paid);
			  $fetch_previous_paid=mysql_fetch_array($exe_pre_paid);
			  $check_bounce_due_fee=$fetch_previous_paid[1];
		     if(($fetch_previous_paid[0]==1)||($fetch_previous_paid[0]==2)||($fetch_previous_paid[0]==3))
		      {
			     $flag_paid=1  ;
				 $total_fee_amount_previous=0;
		      }
		     else
		       {
					
			/*		echo '
					<table  class="table table-bordered table-striped"  >
					<thead>
					</thead>
					<tbody>';*/
					//write query to get id and type from fee generated session id 
					$previoun_fine=0;
					 $query_fee_ge_session_id="SELECT DISTINCT `type`, last_date, duration
											  FROM `fee_generated_sessions`
											  WHERE Id=".$duration_previous."";
					$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
					$fetch_fee_pre=mysql_fetch_array($execute_fee_ge_se_id);
					$month_previous=$fetch_fee_pre['type'];
					$previous_duration_show = $fetch_fee_pre['duration'];
					//calculate previous duration remaning fee
				/*	$tfamount_previous = $credential_amount_previous - $total_discount_previous;*/
					//calculate fine previous duration
					
					if($fetch_fee_pre[1]<$pre_ddate)
						{
						$day_fine = $pre_ddate- $fetch_fee_pre[1];
								
						  if($day_fine<=30)
						  {
							  $previoun_fine=100;
						  }
						  else if($day_fine<=60)
						  {
							  $previoun_fine=200;
						  }
						  else if($day_fine<1)
						  {
							  $previoun_fine=0;  
						  }
						  else 
						  {
							  $previoun_fine=500;
						  }
						}
						$fine_previoun = $fine_previoun + $previoun_fine;
			   }
			}
		
	//calculate total remaning_fee in all duration in both query and add in second query by while loop
	
/*	$que_remaning_duration="SELECT DISTINCT fee_generated_session_id
	                        FROM fee_details
							WHERE student_id=".$student_id."";
	$exe_remaning_duration=mysql_query($que_remaning_duration);
	while($fetch_remaning_duration=mysql_fetch_array($exe_remaning_duration))
		{*/
	//get duration
	//get total remaning fee in all duration
	$calculate_total_remaning_fee_of_stu_in_table=0;
	 $total_remaning_query="SELECT DISTINCT remaning_fee
	                        FROM fee_details
							WHERE student_id=".$student_id."
							AND fee_generated_session_id<>".$duration."";
	$exe_total_remaning_in_table=mysql_query($total_remaning_query);
	while($fetch_total_remaning=mysql_fetch_array($exe_total_remaning_in_table))
	{
	$calculate_total_remaning_fee_of_stu_in_table = $calculate_total_remaning_fee_of_stu_in_table+$fetch_total_remaning[0];
	}

		/*}*/
	//query get remaning fee from fee details
	$remaning_fee=0;
	$query_fee="SELECT remaning_fee, receiving_fee, paid, cheque_no, previous_fine
	            FROM fee_details
				WHERE student_id=".$student_id."
				AND fee_generated_session_id=".$duration."";
	$exe_fee=mysql_query($query_fee);
	$fetch_fee=mysql_fetch_array($exe_fee);
	$check_no=$fetch_fee[3];
	//due fee
	$remaning_fee=$fetch_fee[0];
	//paid fee
	$receiving_fee= $fetch_fee[1];
	$paid_status=$fetch_fee[2];
	//if fee paid then remaning fee show ==0
	$show_remaning_zero=0;
	$store_pre_fine = $fetch_fee[4];
	//calculate remeaning fee after update query when remaning fee == 0 in table
		//dues fee in select duration in table
	if($fetch_fee[2]==1)
	{
	$due_fee_in_se_du=$total_fee_amount_fine - $fetch_fee[1];
	$duestd=$fetch_fee[0] - $due_fee_in_se_du;
	}
	else
	{
		$duestd=0;
	} 
	 //total fee
	/* $total_feee=$remaning_fee+$total_fee_amount;*/
		// get cheque amounts
     $chek_rs="SELECT * FROM fee_cheque_cash_amount_details
	          WHERE cheque_no='".$check_no."'";
	$exe_chek=mysql_query($chek_rs);
	$fetch_rs=mysql_fetch_array($exe_chek);
	/*echo '<tr>
	<td width="55%"><b style="color:">Total Tution Fee</b></td><td><b style="color:">'.$credential_amount.'
	</b></td>
	</tr>';*/
	if($total_discount>0)
	{
	echo '<tr>
	<td><b style="color:">Total Discount</b></td><td><b style="color:">'.$total_discount.'</b></td>
	</tr>';
	}
	 $type="SELECT * FROM fee_generated_sessions
	        WHERE Id=".$duration."";
	$exe_type=mysql_query($type);
	$fetch_type=mysql_fetch_array($exe_type);
		echo ' <tr style="color:green">
	<td width="55%" ><b>Fee : (Monthly) </b></td><td><b>'.$tfamount.'</b></td>
	</tr>
                    <tr style="color:green">
	<td width="55%" ><b>Fee : ('.$fetch_type['type'].') </b></td><td><b>'.$fee_quterly.'</b></td>
	</tr>';
	//select query get previous remaning fee from fee details
		$query_pri_fee_store="SELECT pre_remaning_fee
		                      FROM fee_details
							  WHERE student_id=".$student_id."
							  AND fee_generated_session_id=".$duration."";
		$exe_fee_store=mysql_query($query_pri_fee_store);
		$fetch_fee_store=mysql_fetch_array($exe_fee_store);
		$previous_fee_store_in_feedetails = $fetch_fee_store[0];
	//query get previou duration remaning fee  
	$pre_duration_remaning_fee="SELECT pre_remaning_fee, paid FROM fee_details
	                           WHERE student_id=".$student_id."
							   AND fee_generated_session_id=".$duration."";
	$exe_pre_du_re_fee=mysql_query($pre_duration_remaning_fee);
	$fetch_pre_du_re_fee=mysql_fetch_array($exe_pre_du_re_fee);
	$abc = $fetch_pre_du_re_fee[0] + $calculate_total_remaning_fee_of_stu_in_table - $store_pre_fine;
	
	echo'
    <tr>
	<td><b>Remaning Fee</b></td><td>'.$abc.'</td>
	</tr>';
		//calculate cheque bounce fee
			 $cheque_fine = "SELECT COUNT(admission_no) AS admission
			                FROM fee_cheque_details
							WHERE admission_no=".$ad_no."
							AND paid_duration=".$duration."";
			$exe_cheque_fine=mysql_query($cheque_fine);
			$fetch_cheque_fine=mysql_fetch_array($exe_cheque_fine);
			$cheque_bounce_no = $fetch_cheque_fine[0];
			$cheque_bounce_amount = $cheque_bounce_no *200;
			if($cheque_bounce_no>0)
			{
			echo '<tr><td><b>Cheque Bounce Charge</td><td><b>'.$cheque_bounce_amount.'</td>';
			}
	$TOTAL_FINE= $store_pre_fine + $calculate_fine;
	echo'
	<tr>
	<td><b>Late Fine</b></td><td><b>'.$TOTAL_FINE.'</b></td>
	</tr>';
	//calculate total fee 
	//total fee =  total fee amount + remaning fee + due fee
	 $total_fee = $total_fee_amount_fine + $calculate_total_remaning_fee_of_stu_in_table + $abc + $store_pre_fine 
	               +$cheque_bounce_amount;
	echo '<tr style="color:green">
	<td><b>Total Fee</td><td><b>'.$total_fee.'</td>
	</tr>
	<tr>
	<td><b>Paid  Fee</b></td><td><b>'.$receiving_fee.'</b></td>
	</tr>
	<tr>
	<td><b>Due Fee</b></td><td><b>'.$remaning_fee.'</b></td>
	</tr>';
	if($receiving_fee==0)
	{
	echo '<tr>
	<td><b style="color:red">Payee Fee</b></td><td><b style="color:red">'.$total_fee.'</b></td>
	</tr>';
	}
	else
	{
	echo '<tr>
	<td><b style="color:red">Payee Fee</b></td><td><b style="color:red">'.$remaning_fee.'</b></td>
	</tr>';
	}echo '
	</tbody>
	</table></tbody>
	</table>';
    //query get date from  dates_d table
	$date_format="SELECT date
	              FROM dates_d
				  WHERE date_id=".$due_date."";
	$exe_format_date=mysql_query($date_format);
	$fetch_format_date=mysql_fetch_array($exe_format_date);
	$format_date=date('d-m-Y',strtotime($fetch_format_date[0]));
	echo ' <strong>NOTE: </strong> Last date of fee deposition of duration <span style="color:red">'.$fetch_dura[0].'
	</span> of session <span style="color:red">'.$fetch_session['session_name'].'</span> is <span style="color:red">
	'.$format_date.'</span>.<br><br>  ';
	/*if(($check_no=="")||($check_no==0))
	{
		echo 'By cheque : <b>0</b> By cash :<b> '.$receiving_fee.'</b>'; 	
	}
	else
	{
		echo 'By cheque : <b>'.$fetch_rs[3].'</b> By cash :<b> '.$fetch_rs[4].'</b>'; 
	}*/
	echo '<br><br>  DATE:<br><br> 
	SIGNATURE:	 
	</div>
	</div><!--  end widget-content -->';
	echo '		
	<div align="right" style="cursor:pointer"><img src="images/print.png" onclick="print_val('.$student_id.')"
	style="width:40px" style="height:30px" /></div>
	</div><!-- widget  span12 clearfix-->
	
	</div><!-- row-fluid --><br><br><br>';		
}
}

//function to show the student fee details if  paid
function students_fee_paid_detailss()
{
	//Author By: Anil Kumar *
	$current_session=$_SESSION['current_session_id'];

    $duration=$_GET['id'];
	$student_id = $_SESSION['user_id'];
	$session_id=$_SESSION['current_session_id'];

	//query get student id from student_user
	$sid_query="SELECT admission_no, Name 
	            FROM student_user
				WHERE sId=".$student_id;
	$exe_sid=mysql_query($sid_query);
	$fetch_sid=mysql_fetch_array($exe_sid);
	$student_addmision=$fetch_sid[0];
	//query get session id
	 $session_id_ge="SELECT session_id, last_date 
	                FROM fee_generated_sessions
					WHERE Id=".$duration."";
	$exe_se=mysql_query($session_id_ge);
	$fetch=mysql_fetch_array($exe_se);
	$current_session=$fetch[0];
	$due_date=$fetch[1];
	//query get paid on date
	 $paid_date="SELECT paid_on
	           FROM fee_details
			   WHERE student_id=".$student_id."
			   AND fee_generated_session_id=".$duration."";
	$exe_date=mysql_query($paid_date);
	$fetch_date=mysql_fetch_array($exe_date);
	$paid_date=$fetch_date[0];
	/*$format=date('Y-m-d',strtotime($date));*/
	//querey get fee details from table name=>fee_detais
	 $query_fee="SELECT * 
	            FROM fee_details
			   WHERE student_id=".$student_id."
			   AND fee_generated_session_id=".$duration."";
	$exe_fee=mysql_query($query_fee);
	//calculate fine
	//query get date id from dates_d
/*	$date_id="SELECT date_id
	          FROM dates_d
			  WHERE date='".$format."'";
	$execute_date=mysql_query($date_id);
	$fetch_date=mysql_fetch_array($execute_date);
	$paid_date=$fetch_date[0];*/
	//define fine
	$fine="fine";
	//query get fine amount/days
    $query_fine="SELECT amount
	             FROM fee_credentials
				 WHERE type='".$fine."'
				 AND session_id=".$_SESSION['current_session_id']." ";
	$exe_fine=mysql_query($query_fine);
	$fetch_fine=mysql_fetch_array($exe_fine);
	$fine_amount=$fetch_fine[0];
	//check condition due date < paid on date
	//and calculate fine
	if($due_date<$paid_date)
		{
			$fine_days=$paid_date-$due_date;
		    	if($fine_days<=30)
				{
					$calculate_fine=100;
				}
				else if($fine_days<=60)
				{
					$calculate_fine=200;
				}
				else
				{
					$calculate_fine=500;
				}
		}
	else
		{
			$calculate_fine=0;  
		}
	//Query to get the class of the user student
	$query_class_student = "SELECT DISTINCT class_index.class_name
							FROM class_index
			
							INNER JOIN class
							ON class.classId = class_index.cId
			
							WHERE class.sId = '".$student_id."'
							";
	$execute_class_student = mysql_query($query_class_student);
	$class_student = mysql_fetch_array($execute_class_student);
	//write query to get the session id from  the fee generated session
	$fetch_session_name="SELECT `session_name`
						 FROM `session_table` 
						 WHERE `sId`=".$session_id."";
	$execute_session_name=mysql_query($fetch_session_name);
	$fetch_session=mysql_fetch_array($execute_session_name);
	$session_name=$current_session['session_name'];
	//select duration name
	$dura="SELECT duration FROM fee_generated_sessions
	       WHERE Id=".$duration."";
	$exe_dura=mysql_query($dura);
	$fetch_dura=mysql_fetch_array($exe_dura);

	    echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Receipt </span>
	</div><!-- End widget-header -->	
	<div class="widget-content" id="s_'.$student_id.'">
	<table  class="table table-bordered table-striped" align="center" >';
	//get name of the school
	$get_name_school=
					"SELECT `name_school`
					FROM `school_names`
					WHERE `Id`= 3";
	$exe_name_school=mysql_query($get_name_school);
	$fetch_school_name=mysql_fetch_array($exe_name_school);
	echo $fetch_school_name['name_school'];
	echo ' </h3>
	
	<h5>Student : '.$fetch_sid[1].'</h5>
	<h6>Class : '.$class_student[0].'</h6>
	 <h5 align="center"> Current Session : '.$fetch_session['session_name'].'</h5>
	 <h5 align="center"> Duration : '.$fetch_dura[0].'</h5>
	<table  class="table table-bordered table-striped">';
	echo '<thead>
	<tr>
	<th align="center" width="5%">S.No.</th>
	<th align="left" width="50%">Credential Type</th>
	<th align="left" width="45%">Amount</th>
	</tr>
	</thead>';
	$total_discount=0;
	$camount=0;	
	$serial_no=1;
	

	
	//QUERY get cost of transport from destination_rouite------
	$tranport_cost=0;
	$trans_amount="SELECT cost
	               FROM destination_route
				   
				   INNER JOIN user_vehicle_details
				   ON user_vehicle_details.did=destination_route.did
				   
				   WHERE user_vehicle_details.uId=$student_id
				   AND destination_route.session_id=".$_SESSION['current_session_id']."";
	$exe_amount_t=mysql_query($trans_amount);
	while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
		{
			$tranport_cost=$tranport_cost+$fetch_transport_cost[0];
		}
	//query get rent of hostel from room allocation --------
	$hostel_rent=0;
	$hostel_amount="SELECT rent 
	                FROM hostel_room_allocation
					WHERE sid=$student_id
					AND hostel_room_allocation.session_id=".$_SESSION['current_session_id']."";
	$exe_rent=mysql_query($hostel_amount);
	while($fetch_rent_cost=mysql_fetch_array($exe_rent))
		{
			$hostel_rent=$hostel_rent+$fetch_rent_cost[0];
		}
	$trans_rent_cost=$hostel_rent+$tranport_cost;

	echo '<tbody>';
	//start query to fetch credential type and amount
	//and calculate total credential amount
	$fetch_credentials_type="SELECT DISTINCT fee_credentials.type,fee_credentials.amount
							FROM fee_credentials
							INNER JOIN fee_details
							ON fee_details.fee_credential_id=fee_credentials.id
							WHERE fee_details.student_id=".$student_id." 
							AND fee_credentials.session_id=".$current_session."
							AND fee_details.fee_generated_session_id=".$duration."";
	$execute_credentials_type=mysql_query($fetch_credentials_type);
	while( $fetch_credentials=mysql_fetch_array($execute_credentials_type))
		{
			$camount=$camount+$fetch_credentials['amount'];
			//first latter of credential type convert small to capital
			$c=str_split($fetch_credentials['type']);
			$upper=strtoupper($c[0]);
			$count=count($c);
			echo '	<tr>
			<td align="center">'.$serial_no++.'</td>
			<td>';
			echo $upper;
			for($i=1;$i<$count;$i++)
				{
					echo $c[$i];  
				}
			echo '   <td>'.$fetch_credentials['amount'].'</td></tr>';  
		}
		 
	//calculate credentials with hostel fee and teransport fee
	$credential_amount=$camount+$trans_rent_cost;
	echo '<tr>
	<td align="center">'.$serial_no++.'</td>
	<td>Transport Fee</td>
	<td>'.$tranport_cost.'</td></tr>';
	echo '<tr>
	<td align="center">'.$serial_no++.'</td>
	<td>Hostel Fee</td>
	<td>'.$hostel_rent.'</td></tr>
	';
	echo ' <table  class="table table-bordered table-striped">
	<thead>
	<tr>
	<th align="center" width="5%">S.No.</th>
	<th align="left" width="50%">Discount Type</th>
	<th align="left" width="45%">Amount</th>
	</tr>
	</thead>
	<tbody>';
	//query to fetch discount type of student and amount
	// and calculate total discount amount
	$serial_no=1;
	$fetch_discount_query="SELECT DISTINCT fee_discount_credentials.type,fee_discount_credentials.amount
						  FROM fee_discount_credentials
						  INNER JOIN fee_details
						  ON fee_details.fee_discount_id=fee_discount_credentials.id
						  WHERE fee_details.student_id=".$student_id."
						  AND fee_discount_credentials.session_id=".$current_session."
						  AND fee_details.fee_generated_session_id=".$duration."";
	$execute_discount_type=mysql_query(  $fetch_discount_query);
	while($fetch_discount=mysql_fetch_array($execute_discount_type))
		{ 
			$total_discount=$total_discount+$fetch_discount['amount'];
			//first latter of discount type convert small to capital
			$d=str_split($fetch_discount['type']);
			$upper=strtoupper($d[0]);
			$c=count($d);
			echo '<tr>
			<td align="center">'.$serial_no++.'</td>
			<td>';
			echo $upper;
			for($i=1;$i<$c;$i++)
				{
					echo $d[$i];  
				}
			echo '  <td>'.$fetch_discount['amount'].'</td>
			</tr>';
		}
	echo '
	<table  class="table table-bordered table-striped"  >
	<thead>
	</thead>
	<tbody>';
	//write query to get id and type from fee generated session id 
	$query_fee_ge_session_id="SELECT DISTINCT `type`, last_date, duration
							  FROM `fee_generated_sessions`
							  WHERE Id=".$duration."";
	$execute_fee_ge_se_id=mysql_query($query_fee_ge_session_id);
	$fetch_fee_ge=mysql_fetch_array($execute_fee_ge_se_id);
	$month=$fetch_fee_ge['type'];
		
	$tfamount=$credential_amount-$total_discount;
	//calculate fee amount according to duration
	//Monthly=1  Quarterly-3, half early=6 and annually=12
	$m=1;
	if($month=="Monthly")
		{
			$m=1; 
		}
	else if($month=="Quarterly")
		{
			$m=3; 
		}
	else if($month=="Half-Yearly")
		{
			 $m=6;  
		}	
	else if($month=="Yearly")
		{
			$m=12;  
		}
	//total fee according duration and with fine
	$total_fee_amount=$tfamount*$m+ $calculate_fine;
/*	//query get remaning fee from fee details
	$remaning_old=0;
	$query="SELECT remaning_fee
	            FROM fee_details
				WHERE student_id=".$student_id."
				AND fee_generated_session_id<>".$duration."";
	$exe_fe=mysql_query($query);
	while($fetch_fe=mysql_fetch_array($exe_fe))
	{
	$remaning_old=$fetch_fe[0];
	}*/
	
	
	//calculate total remaning_fee in all duration in both query and add in second query by while loop
	
/*	$que_remaning_duration="SELECT DISTINCT fee_generated_session_id
	                        FROM fee_details
							WHERE student_id=".$student_id."";
	$exe_remaning_duration=mysql_query($que_remaning_duration);
	while($fetch_remaning_duration=mysql_fetch_array($exe_remaning_duration))
		{*/
	//get duration
	//get total remaning fee in all duration
	$calculate_total_remaning_fee_of_stu_in_table=0;
	 $total_remaning_query="SELECT DISTINCT remaning_fee
	                        FROM fee_details
							WHERE student_id=".$student_id."
							AND fee_generated_session_id<>".$duration."";
	$exe_total_remaning_in_table=mysql_query($total_remaning_query);
	while($fetch_total_remaning=mysql_fetch_array($exe_total_remaning_in_table))
	{
	 $calculate_total_remaning_fee_of_stu_in_table = $calculate_total_remaning_fee_of_stu_in_table+$fetch_total_remaning[0];
	}

		/*}*/
	//query get remaning fee from fee details
	$remaning_fee=0;
    $query_fee="SELECT remaning_fee, receiving_fee, paid, cheque_no
	            FROM fee_details
				WHERE student_id=".$student_id."
				AND fee_generated_session_id=".$duration."";
	$exe_fee=mysql_query($query_fee);
	$fetch_fee=mysql_fetch_array($exe_fee);
	$check_no=$fetch_fee[3];
	//due fee
	 $remaning_fee=$fetch_fee[0];
	//paid fee
	 $receiving_fee= $fetch_fee[1];
	 $paid_status=$fetch_fee[2];
	//if fee paid then remaning fee show ==0
	$show_remaning_zero=0;
	//calculate remeaning fee after update query when remaning fee == 0 in table
		//dues fee in select duration in table
	if($fetch_fee[2]==1)
	{
	 $due_fee_in_se_du=$total_fee_amount-$fetch_fee[1];
	 $duestd=$fetch_fee[0]-$due_fee_in_se_du;
	}
	else
	{
		$duestd=0;
	}
	// get cheque amounts
	 $chek_rs="SELECT * FROM fee_cheque_cash_amount_details
	          WHERE cheque_no='".$check_no."'";
	$exe_chek=mysql_query($chek_rs);
	$fetch_rs=mysql_fetch_array($exe_chek);

	echo '  <tr>
	<td width="55%"><b style="color:">Total Credentials Amount</b></td><td><b style="color:">'.$credential_amount.'
	</b></td>
	</tr>
	<tr>
	<td><b style="color:">Total Discount</b></td><td><b style="color:">'.$total_discount.'</b></td>
	</tr>
	<tr>
	<td><b style="color:">Fee</b</td><td><b style="color:">'.$tfamount.'</b></td>
	</tr>
    <tr>';
	//if condition show remaning fee == 0 if fee paid
/*	if($paid_status==1)
	{*/
		echo '<td><b>Remaning Fee</b></td><td><b >'.$duestd.'</b></td>';
/*	}
	else
	{
		echo '<td><b >Remaning Fee</b></td><td><b >'.$show_remaning_zero.'</b></td>';
		
	}*/
	echo '</tr>
	<tr>
	<td><b >Late Fine</b></td><td><b >'.$calculate_fine.'</b></td>
	</tr>';
	//calculate total fee 
	//total fee =  total fee amount + remaning fee + due fee
	$total_fee=$total_fee_amount + $calculate_total_remaning_fee_of_stu_in_table + $duestd;
	echo '<tr>
	<td><b style="color:">Total Fee</b></td><td><b style="color:">'.$total_fee.'</b></td>
	</tr>
		<tr>
	<td><b style="color:">Paid Fee</b></td><td><b style="color:">'.$receiving_fee.'</b></td>
	</tr>
		<tr>
	<td><b style="color:">Due Fee</b></td><td><b style="color:">'.$remaning_fee.'</b></td>
	</tr>';
	if($receiving_fee==0)
	{
	echo '<tr>
	<td><h5 style="color:">Payee Fee</h4></td><td><h5 style="color:">'.$total_fee.'</h4></td>
	</tr>';
	}
	else
	{
	echo '<tr>
	<td><h4 style="color:red">Payee Fee</h4></td><td><h3 style="color:red">'.$remaning_fee.'</h4></td>
	</tr>';
	}
	echo '</tbody>
	</table></tbody>
	</table>';
	//get date from dates_d
	 $date_id="SELECT date
	          FROM dates_d
			  WHERE date_id='".$due_date."'";
	$execute_date=mysql_query($date_id);
	$fetch_date=mysql_fetch_array($execute_date);
	$show_due_date=$fetch_date[0];
	$dmy_format=date('d-m-Y',strtotime($show_due_date));
	echo ' <strong>NOTE: </strong> Last date of fee deposition of duration <span style="color:red">'.$fetch_dura[0].'
	</span> of session <span style="color:red">'.$fetch_session['session_name'].'</span> is <span style="color:red">
	'.$dmy_format.'</span> ';
	         
	echo '<br><br>  DATE:<br><br> 
	SIGNATURE:	 
	</div>
	</div><!--  end widget-content -->';
	echo '		
	<div align="right" style="cursor:pointer"><img src="images/print.png" onclick="print_this_new_receipt('.$student_id.')"
	style="width:40px" style="height:30px" /></div>
	</div><!-- widget  span12 clearfix-->
	
	</div><!-- row-fluid --><br><br><br>';	
}

//function show fee paid students on select duration and date
function accounts_fee_paid_students_duration()
{
	
		//Author By: Anil Kumar *
	
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Not Pay Students</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';
    $duration="SELECT Id, duration FROM fee_generated_sessions
	           WHERE session_id=".$_SESSION['current_session_id']."";
	$exe_duration=mysql_query($duration);	
	echo'  
	<div class="load_page">
	<div class="formEl_b">	';
	//in this form level of defalters select 
	echo '
	<form id="validation_demo" action="accounts_fee_paid_students.php" method="get">'; 
	echo '
	<div class="section">
	<label>Select Duration<small></small></label>
	<div>
	<select name="duration">
	<option value="">--SELECT DURATION--</option>';
	while($fetch_duration_id=mysql_fetch_array($exe_duration))
	    {
	        echo '<option value="'.$fetch_duration_id[0].'">'.$fetch_duration_id[1].'</option>';
		}
	echo '<option value="-1">Till Date</option>
	</select>
	</div>
	</div>';
    echo ' <div class="section">
	       <div>OR 
		   </div>
		   </div>';
	echo '
	<div class="section">
	<label>Select Date<small>Till date</small></label>   
	<div><input type="text"  id="datepick" class="datepicker" readonly="readonly" name="date"  />
	</div>
	</div>
	<div class="section last">
	<div>
	<button class="btn submit_form">Submit</button>
	</div>
	</div>
	</form>
	</div>								
	</div>';
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
	
}
//manish coding
//cheque clearing details
function fee_cheque_form()
{
echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>Cheque Clearing Form</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	
	<a href="accounts_fee_cheque_view_details.php"><button class="uibutton confirm">View All Cheque Details</button></a>
	<a href="accounts_fee_cheque_view_monthly_details.php"><button class="uibutton confirm">View Monthly Cheque Details</button></a>';		
	 if (isset($_GET['Successfully_Insert']))
				{
					echo "<h5 align=\"center\">ADDED SUCCESSFULLY!</h5>"; 
				}
				
				 if (isset($_GET['error']))
				{
					echo "<h5 align=\"center\">Please try again!</h5>
				"; 
				}
	echo'	
   <form id="validation_demo" action="fee/insert_cheque_details.php" method="post">  
   <div class="section ">
    <label>Select Duration<small></small></label>        
         
		 <div >
		 <select  data-placeholder="Choose Duration..." class="chzn-select" tabindex="2" name="duration"  >        
			    <option value=""></option>'; 								
			
		//query to get year
		$get_visitor_year="SELECT *
	                       FROM fee_generated_sessions
				   ";
	$execute_visitor_year=mysql_query($get_visitor_year);
	while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
	      {   
			 $duration=$fetch_visitor_year['duration'];
			 $duration_id=$fetch_visitor_year[0];
	
	echo'
		     <option value="'.$duration_id.'"><b>'. $duration.'</b></option>';
		  }
				  					   
	 echo'  </select> 
	           </div>   
    </div>	
 
 <div class="section ">
 <label>Admission No.<small></small></label>   
 <div> 
 <input  type="text" name="admission_no" class="validate[required] medium"  >
 </div>
 </div>

 <div class="section ">
  <label>Cheque No.<small></small></label>   
   <div> 
 <input type="text" class="validate[required] medium" name="cheque_no" />
   </div>
   </div>
 <div class="section numericonly">
  <label>Cheque Amount<small></small></label>   
   <div> 
 <input type="text" class="validate[required] medium" name="amount" />
   </div>
   </div>
 	<div class="section">
<label>Date of Cheque <small></small></label>   
<div><input type="text"  id="birthday" class=" birthday  medium " name="date"  />
</div>
</div>		
<div class="section last">
 <div><button class="uibutton submit" rel="1" type="submit"  >Save</button>
 <button class="uibutton special" rel="1" type="reset">Reset</button>  
</div>
</div>									
</form> 
';
	echo'                              
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';					
}


//view fee cheque details
function view_fee_cheque_details()
{
	
	
	echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View Cheque Details</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	
	';		
	echo' 		                                                                         	 
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
    <tr>
	<th>Admission No</th>		   
	<th>Name</th>
	<th>Class</th>
	<th>Cheque No.</th>
	<th>Amount</th>
	<th>Duration</th>
	<th>Date</th>	
	<th style="color:green"><b>Status</th>	
    </tr>
    </thead>	 	
	<tbody align="center"> ';	
	
	
	$cheque_details="SELECT *,student_user.admission_no,student_user.Name,fee_generated_sessions.duration,student_user.sId
	                 FROM fee_cheque_details
					 
					 INNER JOIN fee_generated_sessions
					 ON fee_generated_sessions.Id=fee_cheque_details.duration_id
					 AND fee_generated_sessions.session_id=fee_cheque_details.session_id
					 
					 INNER JOIN student_user
					 ON fee_cheque_details.admission_no=student_user.admission_no 
					 ";
					 
	$exe_cheque_details=mysql_query($cheque_details);
	while($fetch_cheque=mysql_fetch_array($exe_cheque_details))
	{        
	     $get_class="SELECT * ,class_index.class_name
		              FROM class
					INNER JOIN class_index
					ON class.classId =class_index.cId 
					WHERE  class.sId=".$fetch_cheque['sId']."";
					  
		$exe_class=mysql_query($get_class);
		$fetch_class=mysql_fetch_array($exe_class);
	 	$class=$fetch_class['class_name'];
	    //echo $date=date($fetch_cheque['date'], 'd/m/Y ');  
		//date_format($fetch_cheque['date'], 'd/m/Y ');
	   echo"
	     <tr>
		 <td>".$fetch_cheque['admission_no']."</td>
		  <td>".$fetch_cheque['Name']."</td>
		  <td>".$class."</td> 
		   <td>".$fetch_cheque['cheque_no']."</td> 
		   <td>".$fetch_cheque['amount']."</td>
		    <td>".$fetch_cheque['duration']."</td>
			 <td>".date('d-m-Y',strtotime($fetch_cheque['date']))."</td>";
			 if($fetch_cheque['status']==1)	
			 {
				 echo ' <td style="color:green"><b>Paid</td>';
			 }
			 else
			 {
				 echo ' <td style="color:red"><b>Due</td>';
				 
			 }
	}
echo'</tbody>
</table>                          
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';				
	
	
}

//view montly cheque 
function view_monthly_fee_cheque_details()
{
	echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View Cheque Monthly Details</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	
	';		
	
echo'	
   <form >  
   <div class="section ">
    <label>Select Duration<small></small></label>        
         
		 <div >
		 <select  data-placeholder="Choose Duration..." class="chzn-select" tabindex="2" name="duration" id="duration" onchange="show_cheque_details()" >        
			    <option value=""></option>'; 								
			
		//query to get year
		$get_visitor_year="SELECT *
	                       FROM fee_generated_sessions";
	$execute_visitor_year=mysql_query($get_visitor_year);
	while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
	      {   
			 $duration=$fetch_visitor_year['duration'];
			 $duration_id=$fetch_visitor_year[0];
	
	echo'
		     <option value="'.$duration_id.'"><b>'. $duration.'</b></option>';
		  }
				  					   
	 echo'  </select> 
    </div>   
    </div>
	<span id="cheque_show">
	</form>	 ';	
	
	echo'</tbody>
</table>                          
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';				
	
	
}

//refund form
function fee_refund_form()
{
	
	
	 $sid=$_GET['sid'];
	$f_g_sid=$_GET['f_g_sid'];
echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>Cheque Clearing Form</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	
	
	';		
	 if (isset($_GET['Successfully_Insert']))
				{
					echo "<h5 align=\"center\">ADDED SUCCESSFULLY!</h5>"; 
				}
				
				 if (isset($_GET['error']))
				{
					echo "<h5 align=\"center\">Please try again!</h5>
				"; 
				}
	echo'	
   <form id="validation_demo" action="fee/refunding_fee.php" method="get">  
 
<input type="hidden" name="sid" value="'.$sid.'"/>
<input type="hidden" name="f_g_sid" value="'.$f_g_sid.'"/>
 <div class="section ">
  <label>Cheque No.<small></small></label>   
   <div> 
 <input type="text" class=" medium" name="cheque_no" />
   </div>
   </div>
 <div class="section numericonly">
  <label>Cheque/Cash Amount<small></small></label>   
   <div> 
 <input type="text" class="validate[required] medium" name="amount" />
   </div>
   </div>
 	<div class="section">
<label>Date <small></small></label>   
<div><input type="text"  id="birthday" class=" birthday  medium " name="date"  />
</div>
</div>
<div class="section ">
 <label>Resion(Why ?)<small></small></label>   
 <div> 
 <input  type="text" name="resion" class="validate[required] medium"  >
 </div>
 </div>		
<div class="section last">
 <div><button class="uibutton submit" rel="1" type="submit"  >Save</button>
 <button class="uibutton special" rel="1" type="reset">Reset</button>  
</div>
</div>									
</form> 
';
	echo'                              
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';						
}
//view refund details
function view_refund_fee_details()
{
	
$sid=$_GET['sid'];
//$session_id=$_GET['session_id'];
	echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View Cheque Details</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	
	';		
	echo' 		                                                                         	 
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
    <tr>
	<th>Admission No</th>		   
	<th>Name</th>
	<th>Class</th>
	<th>Cheque No.</th>
	<th>Amount</th>
	<th>Duration</th>
	<th>Date</th>		
    </tr>
    </thead>	 	
	<tbody align="center"> ';	
	
 $cheque_details="SELECT *,student_user.admission_no,student_user.Name,fee_generated_sessions.duration,student_user.sId
	                 FROM fee_refund_details
					 
					 INNER JOIN fee_generated_sessions
					 ON fee_generated_sessions.Id=fee_refund_details.duration_id
					
					 
					 INNER JOIN student_user
					 ON fee_refund_details.sid=student_user.sId
					 WHERE  fee_refund_details.sid=".$sid." AND  fee_refund_details.session_id=".$_SESSION['current_session_id']."";
					 
	$exe_cheque_details=mysql_query($cheque_details);
	while($fetch_cheque=mysql_fetch_array($exe_cheque_details))
	{        
	     $get_class="SELECT * ,class_index.class_name
		              FROM class
					INNER JOIN class_index
					ON class.classId =class_index.cId 
					WHERE  class.sId=".$fetch_cheque['sId']."";
					  
		$exe_class=mysql_query($get_class);
		$fetch_class=mysql_fetch_array($exe_class);
	 	$class=$fetch_class['class_name'];
	    //echo $date=date($fetch_cheque['date'], 'd/m/Y ');  
		//date_format($fetch_cheque['date'], 'd/m/Y ');
	   echo"
	     <tr>
		 <td>".$fetch_cheque['admission_no']."</td>
		  <td>".$fetch_cheque['Name']."</td>
		  <td>".$class."</td> 
		   <td>".$fetch_cheque['cheque_no']."</td> 
		   <td>".$fetch_cheque['amount']."</td>
		    <td>".$fetch_cheque['duration']."</td>
			 <td>".date('d-m-Y',strtotime($fetch_cheque['date']))."</td>
	   ";		
	}	
	
	
echo'</tbody>
</table>                              
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';			
	
}

//view refund details
function view_refund_fee_school()
{
	

//$session_id=$_GET['session_id'];
	echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View Cheque Details</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	
	';		
	echo' 		                                                                         	 
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
    <tr>
	<th>Admission No</th>		   
	<th>Name</th>
	<th>Class</th>
	<th>Cheque No.</th>
	<th>Amount</th>
	<th>Duration</th>
	<th>Date</th>		
    </tr>
    </thead>	 	
	<tbody align="center"> ';	
	
 $cheque_details="SELECT *,student_user.admission_no,student_user.Name,fee_generated_sessions.duration,student_user.sId
	                 FROM fee_refund_details
					 
					 INNER JOIN fee_generated_sessions
					 ON fee_generated_sessions.Id=fee_refund_details.duration_id
					
					 
					 INNER JOIN student_user
					 ON fee_refund_details.sid=student_user.sId
					 WHERE fee_refund_details.session_id=".$_SESSION['current_session_id']."";
					 
	$exe_cheque_details=mysql_query($cheque_details);
	while($fetch_cheque=mysql_fetch_array($exe_cheque_details))
	{        
	     $get_class="SELECT * ,class_index.class_name
		              FROM class
					INNER JOIN class_index
					ON class.classId =class_index.cId 
					";
					  
		$exe_class=mysql_query($get_class);
		$fetch_class=mysql_fetch_array($exe_class);
	 	$class=$fetch_class['class_name'];
	    //echo $date=date($fetch_cheque['date'], 'd/m/Y ');  
		//date_format($fetch_cheque['date'], 'd/m/Y ');
	   echo"
	     <tr>
		 <td>".$fetch_cheque['admission_no']."</td>
		  <td>".$fetch_cheque['Name']."</td>
		  <td>".$class."</td> 
		   <td>".$fetch_cheque['cheque_no']."</td> 
		   <td>".$fetch_cheque['amount']."</td>
		    <td>".$fetch_cheque['duration']."</td>
			 <td>".date('d-m-Y',strtotime($fetch_cheque['date']))."</td>
	   ";		
	}	
	
	
echo'</tbody>
</table>                              
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';			
	
}


//add fee credentials or fee charges
function accounts_fee_charges_name()
{
    
    //Author By: Sagun sir	
	echo '
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ADD Fee Charges </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"> Kindly add   <span class="netip"><a  class="red" > basic fee credentials  </a></span>
	</div>
	';		  
	if (isset($_GET['sss']))
		{
			echo '<h4 align="center"><span style="color:green">FEE CHARGE ADDED SUCCESSFULLY!</h4></span>';
		}

	echo'      
	<form id="demo"  action="fee/select_charge_type.php" method="get"> 
	<div class="section">
	<label>Credential Type</label>   
	<div> 
	<input type="text" name="charge" class=" medium" required="required" autofocus/>
        <input type="radio" name="radio" id="0" value="0" onClick="select_charge_types(this.id);" checked="checked"/>Monthly
        <input type="radio" name="radio" id="1" value="1" onclick="select_charge_types(this.id);">Yearly
	</div>
	</div>
	
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form" >
	<a class="btn" onclick="ResetForm()" title="Reset  Form">Clear Form</a>
	
	</div>
	</div>	 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
	$sno=0;
	echo' 
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>View Fee Charges </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<table class="table table-bordered table-striped" id="dataTable">
	<thead>';
	echo'   
	<tr>
	<th width="5%">S.No.</th>
	<th width="28%">CHARGES NAME</th>
	<th width="28%">CHARGES TYPE</th>
	<th width="28%">SESSION</th>
	
	</tr>
	</thead>
	<tbody align="center">';	  
	$query = "SELECT * 
			FROM fee_charges_name
			ORDER BY charge_type ASC
			";
			$execute = mysql_query($query) ;
	while($fetch = mysql_fetch_array($execute))
		{            
			++$sno;    
			$query_session="SELECT *
							FROM session_table
							WHERE sId=".$fetch['session_id']
							;
			$execute_session=mysql_query($query_session);
			while($fetch_session=mysql_fetch_array($execute_session))						       
				{  
					echo'     <tr class="odd gradeX">
					<td>'.$sno.'</td>
					<td>'.$fetch[1].'</td>';
                                        if($fetch[2]==0)
                                        {
                                            echo '<td>MONTHLY </td>';
                                        }
                                        else 
                                        {
                                            echo '<td>YEARLY </td>';
                                        }
                                        
                                        echo '
					<td>'.$fetch_session['session_name'].'</td>';
//				echo '	<td class=" ">
//					<span class="tip"><a href="accounts_edit_fee_charges.php?id='.$fetch['Id'].'" original-title="Edit">
//					<img src="images/icon/icon_edit.png" ></a></span> 
//					<span class="tip"><a href="fee/delete_fee_charges.php?id='.$fetch['Id'].'" >
//					<img src="images/icon/icon_delete.png" >
//					</a></span> 
//					</td>';
					echo '</tr>';
				}		
		}
	
	echo' </tbody>';
//	echo '<a class="btn btn-success" href="accounts_fee_credentials_class_select.php">View Fee Credentials Allocation</a>';
	echo '</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';

}

//generate fee structure 
function accounts_fee_charges_structure()
{
    
          
        $ctr_class=0;
        $array_class_id= array();		//Array to store the time_slot_id
        $array_class_name = array();	//Array to store the time_slot_name
        
        //get all the class id's of the folww. teacher
        $class_name="SELECT * FROM class_index ORDER BY order_by";
        $exe_cls=mysql_query($class_name);
       while($fetch_class=mysql_fetch_array($exe_cls))
       {
           
           $array_class_id[$ctr_class]= $fetch_class[0];		//Array to store the time_slot_id
           $array_class_name[$ctr_class] =$fetch_class[1];
           $ctr_class++;
       }
       //get number of month fee type
       $count="SELECT COUNT(charge_type) AS type FROM fee_charges_name WHERE charge_type=0";
       $xee_count=mysql_query($count);
       $fetch_count=mysql_fetch_array($xee_count);
       $m=$fetch_count['type'];
       //get number of yearly fee type
       $count="SELECT COUNT(charge_type) AS type FROM fee_charges_name WHERE charge_type=1";
       $xee_count=mysql_query($count);
       $fetch_count=mysql_fetch_array($xee_count);
       $y=$fetch_count['type'];
	 //get number of half yearly fee type
       $count="SELECT COUNT(charge_type) AS type FROM fee_charges_name WHERE charge_type=2";
       $xee_count=mysql_query($count);
       $fetch_count=mysql_fetch_array($xee_count);
       $h=$fetch_count['type'];
	 //get number of admission fee type
       $count="SELECT COUNT(charge_type) AS type FROM fee_charges_name WHERE charge_type=3";
       $xee_count=mysql_query($count);
       $fetch_count=mysql_fetch_array($xee_count);
       $a=$fetch_count['type'];
        //get charges name
        $current_session=$_SESSION['current_session_id'];
        $query_get_time_slot = "
				SELECT *
				FROM `fee_charges_name`
				WHERE session_id=".$current_session."
				ORDER BY charge_type ASC";
        $execute_charge = mysql_query($query_get_time_slot);

      
        //Now Store all the time_slot ID and Name in an array
        $ctr_charge = 0;
        $array_charge_id= array();		//Array to store the time_slot_id
        $array_charge_name = array();	//Array to store the time_slot_name
        $array_charge_type = array();
        while($get_charge = mysql_fetch_array($execute_charge))
        {
            $array_charge_id[$ctr_charge] = $get_charge [0];
            $array_charge_name[$ctr_charge] = $get_charge [1];
            $array_charge_type[$ctr_charge] = $get_charge [2];
            ++$ctr_charge;
        }
        echo'
        <div class="row-fluid">

        <!-- Table widget -->
        <div class="widget  span12 clearfix">

        <div class="widget-header">
        <span>FEE STRUCTURE</span>
        </div><!-- End widget-header -->	

        <div class="widget-content">
        <form action="fee/insert_fee_structure.php" method="post">
        <table  class="table table-bordered table-striped">';
//        if(isset($_GET["delete_time"]))
//        {
//          echo '<h4 align="center" style="color:red">Time Table Deleted Successfully </h34>';
//        }
    

        echo '   <thead>  
            <tr  style="color:blue" align="center">
            <td style="color:red" align="center" rowspan="2"><b>FEE&rArr; <br> CLASS &dArr;</b></td>
            <td colspan="'.$m.'">MONTHLY</td>
            <td colsapn="'.$y.'" >YEARLY</td>
		<td colsapn="'.$a.'" >NEW ADMISSION</td>
		<td colsapn="'.$h.'" >HALF YEARLY</td>
            </tr>
        <tr>     
        
        ';
        $para_array_class_id = json_encode($array_class_id);
        //Loop to get the time_slot_name in place
        for($i=0;$i<$ctr_charge;$i++)
        {
            echo '<td align="center" style="color:brown" width="5%"><b>'.$array_charge_name[$i].'</b>
                  <br> <input type="checkbox" id="colums_'.$array_charge_id[$i].'" onclick=\'fee_structure_colums('.$array_charge_id[$i].','.$ctr_class.','.$para_array_class_id.',this.value);\'> </td>';
        }
        echo'</tr>     
        </thead> ';

        echo'<tbody>
        ';
        //End of the header part

        //Now loop to generate the days and also get the details of the subject and teacher
     $para_array_charge_id = json_encode($array_charge_id);
        for($cls=0;$cls<$ctr_class;$cls++)//class loop
        {
            //Loop will generate days, 0->Monday, 1-> Tuesday etc...
            echo '<tr>';

            echo '<td style="color:green" align="center"><b>Class : '.$array_class_name[$cls].'</b>
                   <br> <input type="checkbox" id="rows_'.$array_class_id[$cls].'" onclick=\'fee_structure_rows('.$array_class_id[$cls].','.$ctr_charge.','.$para_array_charge_id.',this.value);\'></td>';

            //Loop to get the details from the time_table table for the corresponding day, time_slot_id, class_id
           
            
            for($ij=0;$ij<$ctr_charge;$ij++) //tution fee loop for text
            {
         
                 echo '<td width="5%"><input type="text" id="charge_'.$array_class_id[$cls].'_'.$array_charge_id[$ij].'" name="charge_'.$array_class_id[$cls].'_'.$array_charge_id[$ij].'" style="width:70px;" autofocus/></td>';
            }

            echo '</tr>';
        }
        
        echo'
        </tbody>	
        </table>
        
<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form" >
	 <button type="reset"  title="Reset  Form">Clear Form</button>
	
	</div>
	</div>	
';
        
//                echo ' <a class="btn btn-danger" href="#" onclick="print_time_table_teachers()">
//                 <i class="icon-trash icon-white"></i> PRINT TIME TABLE</a> ';
                 /*									
                 <a class="uibutton special large" href="admin_make_entry.php?class_id='.$class_id.'">Make an Entry</a>*/


                 echo '       </div><!--  end widget-content -->
                 </div><!-- widget  span12 clearfix-->

                 </div><!-- row-fluid -->';


           echo ' </div>';
        
//        echo '
//        </br> </br> 
//        <a class="btn btn-danger" href="#" onclick="delete_class_time_table('.$class_id.')">
//        <i class="icon-trash icon-white"></i> Delete Time Table</a>';
        /*									
        <a class="uibutton special large" href="admin_make_entry.php?class_id='.$class_id.'">Make an Entry</a>*/
        echo '       </div><!--  end widget-content -->
        </div><!-- widget  span12 clearfix-->
        </div><!-- row-fluid -->';
    
    
}
//fee chargre structure view

//generate fee structure 
function accounts_fee_charges_structure_view()
{
    
          
        $ctr_class=0;
        $array_class_id= array();		//Array to store the time_slot_id
        $array_class_name = array();	//Array to store the time_slot_name
        
        //get all the class id's of the folww. teacher
        $class_name="SELECT * FROM class_index ORDER BY order_by";
        $exe_cls=mysql_query($class_name);
       while($fetch_class=mysql_fetch_array($exe_cls))
       {
           
           $array_class_id[$ctr_class]= $fetch_class[0];		//Array to store the time_slot_id
           $array_class_name[$ctr_class] =$fetch_class[1];
           $ctr_class++;
       }
       //get number of month fee type
       $count="SELECT COUNT(charge_type) AS type FROM fee_charges_name WHERE charge_type=0";
       $xee_count=mysql_query($count);
       $fetch_count=mysql_fetch_array($xee_count);
       $m=$fetch_count['type'];
       //get number of yearly fee type
       $count="SELECT COUNT(charge_type) AS type FROM fee_charges_name WHERE charge_type=1";
       $xee_count=mysql_query($count);
       $fetch_count=mysql_fetch_array($xee_count);
       $y=$fetch_count['type'];
	 //get number of half yearly fee type
       $count="SELECT COUNT(charge_type) AS type FROM fee_charges_name WHERE charge_type=2";
       $xee_count=mysql_query($count);
       $fetch_count=mysql_fetch_array($xee_count);
       $h=$fetch_count['type'];
	 //get number of admission fee type
       $count="SELECT COUNT(charge_type) AS type FROM fee_charges_name WHERE charge_type=3";
       $xee_count=mysql_query($count);
       $fetch_count=mysql_fetch_array($xee_count);
       $a=$fetch_count['type'];
        //get charges name
        $current_session=$_SESSION['current_session_id'];
        $query_get_time_slot = "
				SELECT *
				FROM `fee_charges_name`
				WHERE session_id=".$current_session."
				ORDER BY charge_type ASC";
        $execute_charge = mysql_query($query_get_time_slot);
       

        //Now Store all the time_slot ID and Name in an array
        $ctr_charge = 0;
        $array_charge_id= array();		//Array to store the time_slot_id
        $array_charge_name = array();	//Array to store the time_slot_name
        $array_charge_type = array();
        while($get_charge = mysql_fetch_array($execute_charge))
        {
            $array_charge_id[$ctr_charge] = $get_charge [0];
            $array_charge_name[$ctr_charge] = $get_charge [1];
            $array_charge_type[$ctr_charge] = $get_charge [2];
            ++$ctr_charge;
        }
        echo'
        <div class="row-fluid">

        <!-- Table widget -->
        <div class="widget  span12 clearfix">

        <div class="widget-header">
        <span>FEE STRUCTURE</span>
        </div><!-- End widget-header -->	

        <div class="widget-content">
        <form action="fee/insert_fee_structure.php" method="post">
        <table  class="table table-bordered table-striped">';
        if(isset($_GET["quter"]))
        {
          echo '<h4 align="center" style="color:green">FEE STRUCTURE GENERATED SUCCESSFULLY!</h4>';
        }
    

        echo '   <thead>  
            <tr  style="color:blue" align="center">
            <td style="color:red;border:1px solid black; width:7%;" align="center" rowspan="2"><b>FEE&rArr; <br> CLASS &dArr;</b></td>
            <td colspan="'.$m.'" >MONTHLY</td>
            <td colsapn="'.$y.'" >YEARLY</td>
            </tr>
        <tr>     
        
        ';
        //Loop to get the time_slot_name in place
        for($i=0;$i<$ctr_charge;$i++)
        {
            echo '<td align="center" style="color:brown;border:1px solid black;" width="5%"><b>'.$array_charge_name[$i].'</b></td>';
        }
        echo'</tr>     
        </thead> ';

        echo'<tbody>
        ';
        //End of the header part

        //Now loop to generate the days and also get the details of the subject and teacher
     
        for($cls=0;$cls<$ctr_class;$cls++)//class loop
        {
            //Loop will generate days, 0->Monday, 1-> Tuesday etc...
            echo '<tr>';

            echo '<td style="color:green;border:1px solid black;" align="center"><b>'.$array_class_name[$cls].'</b></td>';

            //Loop to get the details from the time_table table for the corresponding day, time_slot_id, class_id
           
            
            for($ij=0;$ij<$ctr_charge;$ij++)//tution fee loop for text
            {
                
                //get detaills of fee charge structure
                  $charge_stru="SELECT * FROM fee_charges_structure
                    WHERE class='".$array_class_id[$cls]."'
                        AND fee_name_id=".$array_charge_id[$ij]."";
                $exe_chargess=mysql_query($charge_stru);
               while($fetch_chargess=mysql_fetch_array($exe_chargess))
               {
                   $temo = 0;
                 
                    $temo = $fetch_chargess['amounts'];
                   
                   if($fetch_chargess['amounts']>0)
                   {
                        echo '<td style="border:1px solid black;">'.$fetch_chargess['amounts'].'</td>';
                      
                   }
                   else 
                   {
                       echo '<td style="border:1px solid black;">0</td>';
                 
                   }
               }
            }

            echo '</tr>';
        }
        
        echo'
        </tbody>	
        </table>
        
';
        
//                echo ' <a class="btn btn-danger" href="#" onclick="print_time_table_teachers()">
//                 <i class="icon-trash icon-white"></i> PRINT TIME TABLE</a> ';
                 /*									
                 <a class="uibutton special large" href="admin_make_entry.php?class_id='.$class_id.'">Make an Entry</a>*/


                 echo '       </div><!--  end widget-content -->
                 </div><!-- widget  span12 clearfix-->

                 </div><!-- row-fluid -->';


           echo ' </div>';
        
//        echo '
//        </br> </br> 
//        <a class="btn btn-danger" href="#" onclick="delete_class_time_table('.$class_id.')">
//        <i class="icon-trash icon-white"></i> Delete Time Table</a>';
        /*									
        <a class="uibutton special large" href="admin_make_entry.php?class_id='.$class_id.'">Make an Entry</a>*/
        echo '       </div><!--  end widget-content -->
        </div><!-- widget  span12 clearfix-->
        </div><!-- row-fluid -->';
    
    
}
//PRINT RECEIPT
function accounts_fee_receipt_print_new_vidya()
{
    //Author : Anil Kumar* sakarpur
    
    
    
    $fine=0;
     $discount=0;
    $due_next=0;
    $admission_fee=0;
    $other_charge=0;
    $fee_receipt_due_next_time_paid=0;
    if(isset($_GET['admission_fee']))
    {
        $admission_fee=$_GET['admission_fee'];
    }
     if(isset($_GET['other_charge']))
    {
        $other_charge=$_GET['other_charge'];
    }
    
    if(isset($_GET['ref_no']))
    {
        $ref_no=$_GET['ref_no'];
    }
     if(isset($_GET['next_paid']))
    {
        $next_paid_due=$_GET['next_paid'];
       $fee_receipt_due_next_time_paid=1;
    }
	$quter=$_GET['quter'];
        $student_id=$_GET['student_id'];
        if($quter==4)
{
$year_name=2015;
}
else
{
$year_name=2014;
}
        
        //get receipt no from fee receipt details
         $rece_id="SELECT MAX(receipt_id) FROM fee_receipt_details WHERE sid=".$student_id." AND ref_no=".$ref_no."";
        $exe_recpt=mysql_query($rece_id);
        $fetch_receipt=mysql_fetch_array($exe_recpt);
        $receipt_no=$fetch_receipt[0];
        $class=$_GET['class'];
        $rm_fee=$_GET['rm_fee'];
        $discount=$_GET['discount'];
        if(isset($_GET['transport_cost']))
        {
         $transport_quter=$_GET['transport_cost'];
        }
         
        $year_month_fee_charge=$_GET['ymfc'];//with transporet
        
        $year_month_fee_charge =$year_month_fee_charge + $admission_fee + $other_charge;
        
           $paid=$_GET['paid'];//with transporet
           
         $paid = $paid +   $admission_fee + $other_charge;
//           if($fee_receipt_due_next_time_paid==1)
//           {
              $max_recpt="SELECT MAX(receipt_id) FROM fee_receipt_details";
               $exe_rec=mysql_query($max_recpt);
               $fetch_rec=mysql_fetch_array($exe_rec);
               
               $receipt_id=$fetch_rec[0];
                
               
            $star_rec="SELECT * FROM fee_receipt_details WHERE receipt_id=".$receipt_id."";
               $exe_star_rec=mysql_query($star_rec);
               $fetch_star_rec=mysql_fetch_array($exe_star_rec);
               $select_date_id=$fetch_star_rec['date'];
               $du_value=$fetch_star_rec[9];
              $tra=0;
                if(($du_value>1000)&&($du_value<1005))
                 {
                    $into=3;
                 }
                 elseif(($du_value>1004)&&($du_value<1007))
                 {
                    $into=6;
                 }
                   elseif(($du_value>1006)&&($du_value<1008))
                 {
                    $into=12;
                 }
                   else
                 {
                     if($du_value==$quter)
                      {
                          $into=1;
                      }
                      else 
                      {
                          $into=$du_value-$quter+1;
                      } 
                 }
               //get date
               $select_date="SELECT date FROM dates_d WHERE date_id=".$select_date_id."";
               $exe_da=mysql_query($select_date);
               $fetch_date=mysql_fetch_array($exe_da);
               $date=$fetch_date[0];
               
               $due=$fetch_star_rec['due'];
//           }
//         else 
//         {
//         
//                if(isset($_GET['due_next']))
//                {
//                $due_next=$_GET['due_next'];
//                }
//                if($due_next > 0)
//                {
//                    $due =$due_next;
//                }
//                else
//                {
//                    $due = $year_month_fee_charge - $paid;
//                }
//         }
        
         $durations_name=$_GET['durations_name'];
        $due_balance=0;
        $due_balance=$_GET['due_balance'];
        $ews_sid="SELECT * FROM fee_details WHERE student_id=".$student_id."";
        $exe_ews=mysql_query($ews_sid);
        $fetch_ews=mysql_fetch_array($exe_ews);
        $ews=$fetch_ews['ews'];
        
        //duration name
            date_default_timezone_set('Asia/Kolkata');
//          echo ' <h5 style="color:green" >  '.$date=date('d-m-Y').' Duration (April-June)</h5>  ';
            $dates=date('d-M-Y');
            $month=explode("-",$dates);
          
//            if($quter == 1)
//            {
//                 $durations_name="April-June";
                  
//
//            }
//            elseif($quter == 2)
//            {
//                 $durations_name="July-Sep";
//
//            }
//             elseif($quter == 3)
//            {
//                 $durations_name="Oct-Dec";
//
//            }
//             elseif($quter == 4)
//            {
//                 $durations_name="Jan-March";
//
//            }
//           
            
	//query get student id from student_user
	$sid_query="SELECT *
	            FROM student_user
				WHERE sId=".$student_id;
	$exe_sid=mysql_query($sid_query);
	$fetch_sid=mysql_fetch_array($exe_sid);
	$admission_no=$fetch_sid['admission_no'];
        $name=$fetch_sid['Name'];
        $father_name=$fetch_sid[5];
        $phone=$fetch_sid['Phone No'];
	//query get session id
	
	//query get paid on date
//	$paid_date="SELECT paid_on
//	           FROM fee_details
//			   WHERE student_id=".$student_id."
//			   AND fee_generated_session_id=".$duration."";
//	$exe_date=mysql_query($paid_date);
//	$fetch_dates=mysql_fetch_array($exe_date);
//	$paid_date=$fetch_dates[0];
	/*$format=date('Y-m-d',strtotime($date));*/
	//querey get fee details from table name=>fee_detais
         $transport_cost=0;
         $trans_amount="SELECT cost
                                   FROM destination_route

                                   INNER JOIN user_vehicle_details
                                   ON user_vehicle_details.did=destination_route.did

                                   WHERE user_vehicle_details.uId=$student_id
                                   AND destination_route.session_id=".$_SESSION['current_session_id']."
                                   AND user_vehicle_details.status=1";
        $exe_amount_t=mysql_query($trans_amount);
        while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
                {
                        $transport_cost=$fetch_transport_cost[0];
                }
	//Query to get the class of the user student
	$query_class_student = "SELECT DISTINCT class_index.class_name
							FROM class_index
			
							INNER JOIN class
							ON class.classId = class_index.cId
			
							WHERE class.sId = '".$student_id."'
							";
	$execute_class_student = mysql_query($query_class_student);
	$class_student = mysql_fetch_array($execute_class_student);
        $class_section=$class_student['class_name'];
	//write query to get the session id from  the fee generated session
        $session_id=$_SESSION['current_session_id'];
	$fetch_session_name="SELECT `session_name`
						 FROM `session_table` 
						 WHERE `sId`=".$session_id."";
	$execute_session_name=mysql_query($fetch_session_name);
	$fetch_session=mysql_fetch_array($execute_session_name);
	$session_name=$fetch_session['session_name'];
	
		//get name of the school
	$get_name_school=
					"SELECT `name_school`
					FROM `school_names`
					WHERE `Id`= 1";
	$exe_name_school=mysql_query($get_name_school);
	$fetch_school_name=mysql_fetch_array($exe_name_school);
	    echo' 
	<div class="row-fluid">';
        
        
      // echo ' <p align="center" style="margin-top:150Px" id="watermark">Vidya Bal Bhawan</p>';
        

       echo ' <!-- Table widget -->
        
         
	<div class="widget  span12 clearfix">
        <table  width="100%">
        <tr><td align="center" valign="bottom" style="border:0px;solid black; color:BLACK; font-size:12px; ">
	<b>';
	echo $fetch_school_name['name_school'];
	echo '</b>
     
	
	</div><!-- End widget-header -->	
	<div class="widget-content" id="d_'.$student_id.'">
	';

//        echo '<p align="center" style="color:BLACK; font-size:14px; position:absolute; left:200px; top:0px ">';
//	echo $fetch_school_name['name_school'];
//	echo ' </p>';
            
	
	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:0px;border-collapse:collapse;">';
            


      echo ' <tr style="border:0px;solid black">
        <td colspan="2" valign="top" ALIGN="CENTER" style="border:0px;solid black; font-size:6px"> <b>KONDLI GHAROLI ROAD, MAYUR VIHAR III, DELHI-110096</b></td></tr>';
        echo ' <tr style="border:0px;solid black">
        <td width="68%" ALIGN="right" style="border:0px;solid black; font-size:8px"> <b>Tel. : 011-22627876, 22626299</b></td>

<td  ALIGN="right" width="32%" style="border:0px;solid black; font-size:10px"> <b>( Account`s Copy )</b></td>
</tr>';
         echo ' <tr style="border:0px;solid black">
        <td colspan="2" ALIGN="CENTER" style="border:0px;solid black; font-size:8px"> <b>E-mail : vidyabalbhawan@gmail.com</b></td></tr>';
        echo ' <tr style="border:0px;solid black">
        <td colspan="2" ALIGN="CENTER" style="border:0px;solid black; font-size:10px"> <b>Session 2014-15</b><br/><br/></td></tr>';
            
   
echo '<tr style="border:0px;solid black">
        <td  style="border:0px;solid black; font-size:10px"> <b>Receipt.No.: '.$receipt_id.' </b></td>';
//            echo '  <td  style="border:0px;solid black; font-size:10px"><b>(Fee Receipt) </b></td>';
        $ddd=explode("-",$date);
        $y=$ddd[0];
        $m=$ddd[1];
        $d=$ddd[2];
        $dates_d=$d.'-'.$m.'-'.$y;
       echo '  <td align="right" style="border:0px;solid black; font-size:10px" > <b>Date</b> : '.$dates_d.'</td>
         </tr>
         <tr style="border:0px;solid black">
         <td style="border:0px;solid black; font-size:10px"> <b>Admission No.: </b> '.$admission_no.'</td>
              <td  align="right" style="border:0px;solid black; font-size:10px"> <b>Class & Sec. : </b> '.$class_section.'</td>


</tr>
          <tr style="border:0px;solid black">
          <td colspan="2" style="border:0px;solid black; font-size:10px"> <b>Student Name :</b> '.$name.'</td>
        
     </tr>
         <tr style="border:0px;solid black">
         
        <td colspan="2" align="left" style="border:0px;solid black; font-size:10px"> <b>Father`s Name : </b> '.$father_name.'</td>
     </tr>
             
       
	
             <tr align="center" style="border:0px;solid black; font-size:10px">';
 
        echo '<td colspan="2" style="border:0px;solid black; font-size:10px"><b>Fee of the month ('.$durations_name.')-'.$year_name.'</td></tr>
			 ';
        
         if($ews==1)
        {
             
           echo '  <tr align="center" style="border:0px;solid black; font-size:10px">';
 
        echo '<td colspan="2" style="border:0px;solid black; font-size:10px">EWS STUDENT</td></tr>
			 ';
        }
	
       
               echo '
        </table>';
//        echo '<p> Fee   the   month   of  ( '.$durations_name.' )  -  2014</p>';
         if(($quter==1)&&($admission_fee !=1 ))
        {
           //year mnth
           $fee_structure_details ="SELECT * FROM fee_charges_structure
                                     WHERE class='".$class."'
                                     AND session_id=".$session_id."";
        }
        else 
        {
            //only month
            $fee_structure_details ="SELECT * FROM fee_charges_structure
                                     WHERE class='".$class."'
                                     AND session_id=".$session_id."
                                         AND fee_type_id= 0";
        }
        $exe_structure=mysql_query($fee_structure_details);
       
        $sn=1;
        $total=0;
	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:1px solid black;border-collapse:collapse; font-size:10px">';
	echo '<thead>
	<tr>
	
	<th style="font-size:10px" >PARTICULARS</th>
	<th style="font-size:10px" align="right">AMOUNT</th>
	</tr><tbody >';
        
              //previous due SHOW
        if($due_balance >0 )
        {
            
               echo '<tr>
            <td align="left" style="font-size:10px" ><b>Previous Balance</b></td>';
         
           
                echo '  <td align="right" style="font-size:10px" ><b>'.$due_balance.'</b></tr>';
           
         
          echo '  </tr>';
            
        }
        
        
        while ($fetch_fee_structure=mysql_fetch_array($exe_structure))
        {
            //query get tution fee name
            $charge_name="SELECT charge_name FROM fee_charges_name WHERE Id=".$fetch_fee_structure[2]."";
            $exe_name=mysql_query($charge_name);
            $fetch_charge_name=mysql_fetch_array($exe_name);
            $total= $total + $fetch_fee_structure[4];
            echo' <tr style="border:0px;solid black; font-size:10px">';
             echo' <td align="left" style="font-size:10px" >'.$fetch_charge_name[0].'</td>';
             if($ews==1)
             {
                 echo '<td align="right" style="font-size:10px">0</td>';
             }
             else 
             {
                if($fetch_fee_structure[3]==1)
                 {
                          
              echo' <td align="right" style="font-size:10px" >'.$fetch_fee_structure[4].'</td></tr>';
                }
                elseif($fetch_fee_structure[3]==0)
                 {
                  $charge=$fetch_fee_structure[4]*$into;
                   echo' <td align="right" style="font-size:10px" >'.$charge.'</td></tr>';
                 }
                else
                 {
                          
              echo' <td align="right" style="font-size:10px" >'.$fetch_fee_structure[4].'</td></tr>';
                }
                   
           


             }
           $sn++;   
        }
        if($transport_cost>0)
        {
            // echo' <tr style="border:0px;solid black; font-size:10px">';
            // echo' <td align="left" style="border:0px;solid black; font-size:10px" >Transport Charge</td>';
             if($ews==1)
            {
                // echo '<td align="right" style="border:0px;solid black; font-size:10px">0</td>';
                  $tra=0;
             }
             else 
             {
                 $tra=$transport_cost*$into;
              // echo' <td align="right" style="border:0px;solid black; font-size:10px" >'.$tra.'</td></tr>';
             }
             
            
        }
        
        
        // ADMISSION FEE AND OTHER FEE CHARGE 
         if($admission_fee ==1 )
        {
            
               echo '<tr>
            <td align="left" style="font-size:10px" >Admission Fee </td>';
         
           
                echo '  <td align="right" style="font-size:10px" >200</tr>';
           
         
          echo '  </tr>';
            
        }
             if($discount>0 )
        {
            
               echo '<tr>
            <td align="left" style="font-size:10px" >Tution Fee Dis.</td>';
         
           
                echo '  <td align="right" style="font-size:10px" >'.$discount.'</tr>';
           
         
          echo '  </tr>';
            
        }
        
          // ADMISSION FEE AND OTHER FEE CHARGE 
         if($other_charge >0 )
        {
            
               echo '<tr>
            <td align="left" style="font-size:10px" >Other Charge</td>';
         
           
                echo '  <td align="right" style="font-size:10px" >'.$other_charge.'</tr>';
           
         
          echo '  </tr>';
            
        }
        //ffffine
               // ADMISSION FEE AND OTHER FEE CHARGE 
         if($fetch_star_rec['fine']>0 )
        {
            
               echo '<tr>
            <td align="left" style="font-size:10px" >Other Charge</td>';
         
           
                echo '  <td align="right" style="font-size:10px" >'.$fetch_star_rec['fine'].'</tr>';
           
         
          echo '  </tr>';
            
        }
        
        
  
            
//        $trans_quter=$transport_cost*3;
        $year_month_fee_charge=$year_month_fee_charge+$transport_quter;;
        echo '<tr>
            <td align="left" style="font-size:10px" ><b>Total : ( '.$durations_name.' ) </b></td>';
//          if($ews==1)
//           {
//                echo '  <td align="right" style="font-size:10px" ><b>0</b></tr>';
//           }
//           else 
//           {
        //total with fine
        $totals=$fetch_star_rec['fine']+$year_month_fee_charge;
$totals=$totals-$tra;
           if($ews==1)
              {
                 $totals=0;
               }
          
                echo '  <td align="right" style="font-size:10px" ><b>'.$totals.'</b></tr>';
//           }
         
          echo '  </tr>';
          
          //paids
          $clear=$_GET['clear'];
          if($ews==1)
           {
              $value=0;
//                     echo '<tr>
//            <td width="50%" align="left" style="font-size:10px" ><b>Paid : 0</b></td>';
//                     echo '
//            <td  width="50%" align="left" style="font-size:10px" ><b>Due : 0</b></td>';
//                     echo '  </tr>';
           }
           else if($clear==1)
           {

              if($ews==1)
              {
                 $totals=0;
               }
               $value = $totals;
             
                echo '<tr>
            <td  width="50%" align="left"  style="font-size:10px" ><b>Paid </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:10px" ><b>'.$totals.' </b></td>';
                        echo '  </tr>';
                         echo '<tr> 
            <td  width="50%" align="left"  style="font-size:10px" ><b>Due </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:10px" ><b>0</b></td>';
                        echo '  </tr>';
           }
           else{
               $paid=$fetch_star_rec['fine']+$paid;
 $paid= $paid-$tra;
                  if($ews==1)
              {
                 $paid=0;
               }
               $value = $paid;
                echo '<tr>
            <td  width="50%" align="left"  style="font-size:10px" ><b>Paid </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:10px" ><b>'.$paid.' </b></td>';
                        echo '  </tr>';
                         echo '<tr>
            <td  width="50%" align="left" style="font-size:10px" ><b>Due </b></td>';
                        echo '
            <td  width="50%" align="right"  style="font-size:10px" ><b>'.$due.'</b></td>';
                        echo '  </tr>';
               
//                   echo '<tr>
//            <td colspan="2" width="50%" align="center" style="font-size:10px" ><b>PAID: ( '.$paid.' ) </b></td>';
//                        echo '
//            <td  width="50%" align="center" style="font-size:10px" ><b>DUE: ( '.$due.' ) </b></td>';
//                        echo '  </tr>';
           }
         //check cash details
            
            $cheque=$_GET['cheque'];
            if($cheque>0)
            {
                echo '<tr style="border:0px;solid black; font-size:10px">
            <td colspan="2"   align="left" style="border:0px;solid black; font-size:10px" ><b>Paid by &nbsp;: &nbsp;Cheque No.:'.$cheque.'</b></td> </tr>';
              //  echo '<td  align="left" style="border:0px;solid black; font-size:10px">Cheque No.:'.$cheque.'</td>';
            }
            else 
            {
              if($ews!=1)
              {
                
            
                    echo '<tr style="border:0px;solid black; font-size:10px">
            <td colspan="2"   align="left" style="border:0px;solid black; font-size:10px" ><b>Paid by &nbsp;: &nbsp;Cash</b></td> </tr>';
               // echo '<td  align="left" style="border:0px;solid black; font-size:10px">Cash</td>';
              }
            }
            
            echo '    ';
            
              echo '<tr style="border:0px;solid black; font-size:10px">
              <td colspan="2"  align="left" style="border:0px;solid black; font-size:10px" ><b style="text-transform:uppercase;">';
              if(($value=="")||($value==0))
              {
                  echo ' In Words - Zero only';
              }
              else 
              {
                    echo ' In Words - '.convertNumber($value).' only';
              }
            echo ' </b></td></tr>';
          
                 echo '<tr style="border:0px;solid black; font-size:10px">
            <td  colspan="2"  align="left" style="border:0px;solid black; font-size:10px" ><b>Date : &nbsp;&nbsp;'.$dates_d.'</b></td>';

                //echo '<td  align="left" style="border:0px;solid black; font-size:10px">'.$date.'</td>';

            echo '  </tr>';
            
            
            //bank 
             $cheque=$_GET['cheque'];
            if($cheque>0)
            {
                  echo '<tr style="border:0px;solid black; font-size:10px">
            <td   align="left" style="border:0px;solid black; font-size:10px" >Bank : '.$_GET['bank'].'</td>';
           
            echo '<td  align="left" style="border:0px;solid black; font-size:10px">Branch : '.$_GET['branch'].'</td></tr>';
            }
           
              
        echo '</tbody>
	</thead>';
	
	
	
          echo '</table>';
	echo '
            <table width="100%" style="border:0px;solid black; font-size:10px">
            <tr>
	<td style="border:0px;solid black; font-size:10px; padding-top:40px">Cashier Sign. </td>
<td align="right" style="border:0px;solid black; font-size:10px; padding-top:40px">Depositor Sign </td>
</tr>
        </table>
        </td></tr></table>
	</div><!--  end widget-content -->';
      
        
        //**************************************************************************************************
        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        //                                PARRENTS  COPY
if(($transport_cost>0)&&($cheque>0))
{
echo '<br><br>';
}
       else if(($transport_cost>0)||($cheque>0))
        {
echo '<br><br><br>';
        }
else 
{
        echo '<br><br><br>';
}
        
        $fine=0;
     $discount=0;
    $due_next=0;
    $admission_fee=0;
    $other_charge=0;
    $fee_receipt_due_next_time_paid=0;
    if(isset($_GET['admission_fee']))
    {
        $admission_fee=$_GET['admission_fee'];
    }
     if(isset($_GET['other_charge']))
    {
        $other_charge=$_GET['other_charge'];
    }
    
    if(isset($_GET['ref_no']))
    {
        $ref_no=$_GET['ref_no'];
    }
     if(isset($_GET['next_paid']))
    {
        $next_paid_due=$_GET['next_paid'];
       echo $fee_receipt_due_next_time_paid=1;
    }
	$quter=$_GET['quter'];
        $student_id=$_GET['student_id'];
        
        //get receipt no from fee receipt details
         $rece_id="SELECT MAX(receipt_id) FROM fee_receipt_details WHERE sid=".$student_id." AND ref_no=".$ref_no."";
        $exe_recpt=mysql_query($rece_id);
        $fetch_receipt=mysql_fetch_array($exe_recpt);
        $receipt_no=$fetch_receipt[0];
        $class=$_GET['class'];
        $rm_fee=$_GET['rm_fee'];
        $discount=$_GET['discount'];
        if(isset($_GET['transport_cost']))
        {
         $transport_quter=$_GET['transport_cost'];
        }
         
        $year_month_fee_charge=$_GET['ymfc'];//with transporet
        
        $year_month_fee_charge =$year_month_fee_charge + $admission_fee + $other_charge;
        
           $paid=$_GET['paid'];//with transporet
           
         $paid = $paid +   $admission_fee + $other_charge;
//           if($fee_receipt_due_next_time_paid==1)
//           {
              $max_recpt="SELECT MAX(receipt_id) FROM fee_receipt_details";
               $exe_rec=mysql_query($max_recpt);
               $fetch_rec=mysql_fetch_array($exe_rec);
               
               $receipt_id=$fetch_rec[0];
               
            $star_rec="SELECT * FROM fee_receipt_details WHERE receipt_id=".$receipt_id."";
               $exe_star_rec=mysql_query($star_rec);
               $fetch_star_rec=mysql_fetch_array($exe_star_rec);
               $select_date_id=$fetch_star_rec['date'];
              $du_value=$fetch_star_rec[9];
                if(($du_value>1000)&&($du_value<1005))
                 {
                    $into=3;
                 }
                 elseif(($du_value>1004)&&($du_value<1007))
                 {
                    $into=6;
                 }
                   elseif(($du_value>1006)&&($du_value<1008))
                 {
                    $into=12;
                 }
                   else
                 {
                     if($du_value==$quter)
                      {
                          $into=1;
                      }
                      else 
                      {
                          $into=$du_value-$quter+1;
                      } 
                 }
               //get date
               $select_date="SELECT date FROM dates_d WHERE date_id=".$select_date_id."";
               $exe_da=mysql_query($select_date);
               $fetch_date=mysql_fetch_array($exe_da);
               $date=$fetch_date[0];
               
               $due=$fetch_star_rec['due'];
//           }
//         else 
//         {
//         
//                if(isset($_GET['due_next']))
//                {
//                $due_next=$_GET['due_next'];
//                }
//                if($due_next > 0)
//                {
//                    $due =$due_next;
//                }
//                else
//                {
//                    $due = $year_month_fee_charge - $paid;
//                }
//         }
        
         $durations_name=$_GET['durations_name'];
        $due_balance=0;
        $due_balance=$_GET['due_balance'];
        $ews_sid="SELECT * FROM fee_details WHERE student_id=".$student_id."";
        $exe_ews=mysql_query($ews_sid);
        $fetch_ews=mysql_fetch_array($exe_ews);
        $ews=$fetch_ews['ews'];
        
        //duration name
            date_default_timezone_set('Asia/Kolkata');
//          echo ' <h5 style="color:green" >  '.$date=date('d-m-Y').' Duration (April-June)</h5>  ';
            $dates=date('d-M-Y');
            $month=explode("-",$dates);
          
//            if($quter == 1)
//            {
//                 $durations_name="April-June";
                  
//
//            }
//            elseif($quter == 2)
//            {
//                 $durations_name="July-Sep";
//
//            }
//             elseif($quter == 3)
//            {
//                 $durations_name="Oct-Dec";
//
//            }
//             elseif($quter == 4)
//            {
//                 $durations_name="Jan-March";
//
//            }
//           
            
	//query get student id from student_user
	$sid_query="SELECT *
	            FROM student_user
				WHERE sId=".$student_id;
	$exe_sid=mysql_query($sid_query);
	$fetch_sid=mysql_fetch_array($exe_sid);
	$admission_no=$fetch_sid['admission_no'];
        $name=$fetch_sid['Name'];
        $father_name=$fetch_sid[5];
        $phone=$fetch_sid['Phone No'];
	//query get session id
	
	//query get paid on date
//	$paid_date="SELECT paid_on
//	           FROM fee_details
//			   WHERE student_id=".$student_id."
//			   AND fee_generated_session_id=".$duration."";
//	$exe_date=mysql_query($paid_date);
//	$fetch_dates=mysql_fetch_array($exe_date);
//	$paid_date=$fetch_dates[0];
	/*$format=date('Y-m-d',strtotime($date));*/
	//querey get fee details from table name=>fee_detais
         $transport_cost=0;
         $trans_amount="SELECT cost
                                   FROM destination_route

                                   INNER JOIN user_vehicle_details
                                   ON user_vehicle_details.did=destination_route.did

                                   WHERE user_vehicle_details.uId=$student_id
                                   AND destination_route.session_id=".$_SESSION['current_session_id']."
                                    AND user_vehicle_details.status=1";
        $exe_amount_t=mysql_query($trans_amount);
        while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
                {
                        $transport_cost=$fetch_transport_cost[0];
                }
	//Query to get the class of the user student
	$query_class_student = "SELECT DISTINCT class_index.class_name
							FROM class_index
			
							INNER JOIN class
							ON class.classId = class_index.cId
			
							WHERE class.sId = '".$student_id."'
							";
	$execute_class_student = mysql_query($query_class_student);
	$class_student = mysql_fetch_array($execute_class_student);
        $class_section=$class_student['class_name'];
	//write query to get the session id from  the fee generated session
        $session_id=$_SESSION['current_session_id'];
	$fetch_session_name="SELECT `session_name`
						 FROM `session_table` 
						 WHERE `sId`=".$session_id."";
	$execute_session_name=mysql_query($fetch_session_name);
	$fetch_session=mysql_fetch_array($execute_session_name);
	$session_name=$fetch_session['session_name'];
	
		//get name of the school
	$get_name_school=
					"SELECT `name_school`
					FROM `school_names`
					WHERE `Id`= 1";
	$exe_name_school=mysql_query($get_name_school);
	$fetch_school_name=mysql_fetch_array($exe_name_school);
	    echo'
	<div class="row-fluid">';
        
        
      // echo ' <p align="center" style="margin-top:150Px" id="watermark">Vidya Bal Bhawan</p>';
        

       echo ' <!-- Table widget -->
        
	<div class="widget  span12 clearfix">
        <table  width="100%">
        <tr><td align="center" valign="bottom" style="border:0px;solid black; color:BLACK; font-size:12px; ">
	<b>';
	echo $fetch_school_name['name_school'];
	echo '</b>
     
	
	</div><!-- End widget-header -->	
	<div class="widget-content" id="d_'.$student_id.'">
	';

//        echo '<p align="center" style="color:BLACK; font-size:14px; position:absolute; left:200px; top:0px ">';
//	echo $fetch_school_name['name_school'];
//	echo ' </p>';
            
	
	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:0px;border-collapse:collapse;">';
            


      echo ' <tr style="border:0px;solid black">
        <td colspan="2" valign="top" ALIGN="CENTER" style="border:0px;solid black; font-size:6px"> <b>KONDLI GHAROLI ROAD, MAYUR VIHAR III, DELHI-110096</b></td></tr>';
           echo ' <tr style="border:0px;solid black">

        <td  ALIGN="right" width="68%" style="border:0px;solid black; font-size:8px"> <b>Tel. : 011-22627876, 22626299</b></td>

<td  ALIGN="right" width="32%" style="border:0px;solid black; font-size:10px"> <b>( Parent`s Copy )</b></td>
</tr>';
         echo ' <tr style="border:0px;solid black">
        <td colspan="2" ALIGN="CENTER" style="border:0px;solid black; font-size:8px"> <b>E-mail : vidyabalbhawan@gmail.com</b></td></tr>';
        echo ' <tr style="border:0px;solid black">
        <td colspan="2" ALIGN="CENTER" style="border:0px;solid black; font-size:10px"> <b>Session 2014-15</b><br/><br/></td></tr>';
      
       

echo '<tr style="border:0px;solid black">
        <td  style="border:0px;solid black; font-size:10px"> <b>Receipt.No.: '.$receipt_id.' </b></td>';
//            echo '  <td  style="border:0px;solid black; font-size:10px"><b>(Fee Receipt) </b></td>';
        $ddd=explode("-",$date);
        $y=$ddd[0];
        $m=$ddd[1];
        $d=$ddd[2];
        $dates_d=$d.'-'.$m.'-'.$y;
       echo '  <td align="right" style="border:0px;solid black; font-size:10px" > <b>Date</b> : '.$dates_d.'</td>
         </tr>
         <tr style="border:0px;solid black">
         <td style="border:0px;solid black; font-size:10px"> <b>Admission No.: </b> '.$admission_no.'</td>
              <td  align="right" style="border:0px;solid black; font-size:10px"> <b>Class & Sec. : </b> '.$class_section.'</td>


</tr>
          <tr style="border:0px;solid black">
          <td colspan="2" style="border:0px;solid black; font-size:10px"> <b>Student Name :</b> '.$name.'</td>
        
     </tr>
         <tr style="border:0px;solid black">
         
        <td colspan="2" align="left" style="border:0px;solid black; font-size:10px"> <b>Father`s Name : </b> '.$father_name.'</td>
     </tr>
             
       
	
             <tr align="center" style="border:0px;solid black; font-size:10px">';
 
        echo '<td colspan="2" style="border:0px;solid black; font-size:10px"><b>Fee of the month ('.$durations_name.')-'.$year_name.'</td></tr>
			 ';
        
         if($ews==1)
        {
             
           echo '  <tr align="center" style="border:0px;solid black; font-size:10px">';
 
        echo '<td colspan="2" style="border:0px;solid black; font-size:10px">EWS STUDENT</td></tr>
			 ';
        }
	
        
               echo '
        </table>';
//        echo '<p> Fee   the   month   of  ( '.$durations_name.' )  -  2014</p>';
            if(($quter==1)&&($admission_fee !=1 ))
        {
           //year mnth
           $fee_structure_details ="SELECT * FROM fee_charges_structure
                                     WHERE class='".$class."'
                                     AND session_id=".$session_id."";
        }
        else 
        {
            //only month
            $fee_structure_details ="SELECT * FROM fee_charges_structure
                                     WHERE class='".$class."'
                                     AND session_id=".$session_id."
                                         AND fee_type_id= 0";
        }
        $exe_structure=mysql_query($fee_structure_details);
       
        $sn=1;
        $total=0;
	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:1px solid black;border-collapse:collapse; font-size:10px">';
	echo '<thead>
	<tr>
	
	<th style="font-size:10px" >PARTICULARS</th>
	<th style="font-size:10px" align="right">AMOUNT</th>
	</tr><tbody >';
        
            //previous due SHOW
        if($due_balance >0 )
        {
            
               echo '<tr>
            <td align="left" style="font-size:10px" ><b>Previous Balance</b></td>';
         
           
                echo '  <td align="right" style="font-size:10px" ><b>'.$due_balance.'</b></tr>';
           
         
          echo '  </tr>';
            
        }
        
        
        while ($fetch_fee_structure=mysql_fetch_array($exe_structure))
        {
            //query get tution fee name
            $charge_name="SELECT charge_name FROM fee_charges_name WHERE Id=".$fetch_fee_structure[2]."";
            $exe_name=mysql_query($charge_name);
            $fetch_charge_name=mysql_fetch_array($exe_name);
            $total= $total + $fetch_fee_structure[4];
            echo' <tr style="border:0px;solid black; font-size:10px">';
             echo' <td align="left" style="font-size:10px" >'.$fetch_charge_name[0].'</td>';
             if($ews==1)
             {
                 echo '<td align="right" style="font-size:10px">0</td>';
             }
             else 
             {
                if($fetch_fee_structure[3]==1)
                 {
                          
              echo' <td align="right" style="font-size:10px" >'.$fetch_fee_structure[4].'</td></tr>';
                }
                elseif($fetch_fee_structure[3]==0)
                 {
                  $charge=$fetch_fee_structure[4]*$into;
                   echo' <td align="right" style="font-size:10px" >'.$charge.'</td></tr>';
                 }
                else
                 {
                          
              echo' <td align="right" style="font-size:10px" >'.$fetch_fee_structure[4].'</td></tr>';
                }
                   
           


             }
           $sn++;   
        }
        if($transport_cost>0)
        {
            // echo' <tr style="border:0px;solid black; font-size:10px">';
            // echo' <td align="left" style="border:0px;solid black; font-size:10px" >Transport Charge</td>';
             if($ews==1)
            {
                // echo '<td align="right" style="border:0px;solid black; font-size:10px">0</td>';
                  $tra=0;
             }
             else 
             {
                 $tra=$transport_cost*$into;
              // echo' <td align="right" style="border:0px;solid black; font-size:10px" >'.$tra.'</td></tr>';
             }
             
            
        }
        
        
        // ADMISSION FEE AND OTHER FEE CHARGE 
         if($admission_fee ==1 )
        {
            
               echo '<tr>
            <td align="left" style="font-size:10px" >Admission Fee </td>';
         
           
                echo '  <td align="right" style="font-size:10px" >200</tr>';
           
         
          echo '  </tr>';
            
        }
             if($discount>0 )
        {
            
               echo '<tr>
            <td align="left" style="font-size:10px" >Tution Fee Dis.</td>';
         
           
                echo '  <td align="right" style="font-size:10px" >'.$discount.'</tr>';
           
         
          echo '  </tr>';
            
        }
        
          // ADMISSION FEE AND OTHER FEE CHARGE 
         if($other_charge >0 )
        {
            
               echo '<tr>
            <td align="left" style="font-size:10px" >Other Charge</td>';
         
           
                echo '  <td align="right" style="font-size:10px" >'.$other_charge.'</tr>';
           
         
          echo '  </tr>';
            
        }
        //ffffine
               // ADMISSION FEE AND OTHER FEE CHARGE 
         if($fetch_star_rec['fine']>0 )
        {
            
               echo '<tr>

            <td align="left" style="font-size:10px" >Other Charge</td>';
         
           
                echo '  <td align="right" style="font-size:10px" >'.$fetch_star_rec['fine'].'</tr>';
           
         
          echo '  </tr>';
            
        }
        
        
  
            
//        $trans_quter=$transport_cost*3;
        $year_month_fee_charge=$year_month_fee_charge+$transport_quter;;
        echo '<tr>
            <td align="left" style="font-size:10px" ><b>Total : ( '.$durations_name.' ) </b></td>';
//          if($ews==1)
//           {
//                echo '  <td align="right" style="font-size:10px" ><b>0</b></tr>';
//           }
//           else 
//           {
        //total with fine
        $totals=$fetch_star_rec['fine']+$year_month_fee_charge;
$totals=$totals-$tra;
           if($ews==1)
              {
                 $totals=0;
               }
          
                echo '  <td align="right" style="font-size:10px" ><b>'.$totals.'</b></tr>';
//           }
         
          echo '  </tr>';
          
          //paids
          $clear=$_GET['clear'];
          if($ews==1)
           {
              $value=0;
//                     echo '<tr>
//            <td width="50%" align="left" style="font-size:10px" ><b>Paid : 0</b></td>';
//                     echo '
//            <td  width="50%" align="left" style="font-size:10px" ><b>Due : 0</b></td>';
//                     echo '  </tr>';
           }
           else if($clear==1)
           {

              if($ews==1)
              {
                 $totals=0;
               }
               $value = $totals;
             
                echo '<tr>
            <td  width="50%" align="left"  style="font-size:10px" ><b>Paid </b></td>';
                        echo '

            <td  width="50%" align="right" style="font-size:10px" ><b>'.$totals.' </b></td>';
                        echo '  </tr>';
                         echo '<tr> 
            <td  width="50%" align="left"  style="font-size:10px" ><b>Due </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:10px" ><b>0</b></td>';
                        echo '  </tr>';
           }
           else{
               $paid=$fetch_star_rec['fine']+$paid;
 $paid= $paid-$tra;
                  if($ews==1)
              {
                 $paid=0;
               }
               $value = $paid;
                echo '<tr>
            <td  width="50%" align="left"  style="font-size:10px" ><b>Paid </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:10px" ><b>'.$paid.' </b></td>';
                        echo '  </tr>';
                         echo '<tr>
            <td  width="50%" align="left" style="font-size:10px" ><b>Due </b></td>';
                        echo '
            <td  width="50%" align="right"  style="font-size:10px" ><b>'.$due.'</b></td>';
                        echo '  </tr>';
               
//                   echo '<tr>
//            <td colspan="2" width="50%" align="center" style="font-size:10px" ><b>PAID: ( '.$paid.' ) </b></td>';
//                        echo '
//            <td  width="50%" align="center" style="font-size:10px" ><b>DUE: ( '.$due.' ) </b></td>';
//                        echo '  </tr>';
           }
         //check cash details
            
            $cheque=$_GET['cheque'];
            if($cheque>0)
            {
                echo '<tr style="border:0px;solid black; font-size:10px">
            <td colspan="2"   align="left" style="border:0px;solid black; font-size:10px" ><b>Paid by &nbsp;: &nbsp;Cheque No.:'.$cheque.'</b></td> </tr>';
              //  echo '<td  align="left" style="border:0px;solid black; font-size:10px">Cheque No.:'.$cheque.'</td>';
            }
            else 
            {
              if($ews!=1)
              {
                
            
                    echo '<tr style="border:0px;solid black; font-size:10px">

            <td colspan="2"   align="left" style="border:0px;solid black; font-size:10px" ><b>Paid by &nbsp;: &nbsp;Cash</b></td> </tr>';
               // echo '<td  align="left" style="border:0px;solid black; font-size:10px">Cash</td>';
              }
            }
            
            echo '    ';
            
              echo '<tr style="border:0px;solid black; font-size:10px">
              <td colspan="2"  align="left" style="border:0px;solid black; font-size:10px" ><b style="text-transform:uppercase;">';
              if(($value=="")||($value==0))
              {
                  echo ' In Words - Zero only';
              }
              else 
              {
                    echo ' In Words - '.convertNumber($value).' only';
              }
            echo ' </b></td></tr>';
          
                 echo '<tr style="border:0px;solid black; font-size:10px">
            <td  colspan="2"  align="left" style="border:0px;solid black; font-size:10px" ><b>Date : &nbsp;&nbsp;'.$dates_d.'</b></td>';

                //echo '<td  align="left" style="border:0px;solid black; font-size:10px">'.$date.'</td>';

            echo '  </tr>';
            
            
            //bank 
             $cheque=$_GET['cheque'];
            if($cheque>0)
            {
                  echo '<tr style="border:0px;solid black; font-size:10px">
            <td   align="left" style="border:0px;solid black; font-size:10px" >Bank : '.$_GET['bank'].'</td>';
           
            echo '<td  align="left" style="border:0px;solid black; font-size:10px">Branch : '.$_GET['branch'].'</td></tr>';
            }
           
              
        echo '</tbody>
	</thead>';
	
	
	
          echo '</table>';
	echo '
            <table width="100%" style="border:0px;solid black; font-size:10px">
            <tr>

	<td style="border:0px;solid black; font-size:10px; padding-top:40px">Cashier Sign. </td>

<td align="right" style="border:0px;solid black; font-size:10px; padding-top:40px">Depositor Sign </td>

</tr>

        </table>        </td></tr></table>
	</div><!--  end widget-content -->
        
';
    
          
        
        
//	echo '		
//	<div align="right" style="cursor:pointer"><img src="images/print.png" onclick="print_val('.$student_id.')"
//	style="width:40px" style="height:30px" /></div>';
	echo'</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';


//////////////////////////////////////////////////////// tr(()////////////////////////////////////
 if($transport_cost>0)
        {
  echo' <br><br><br><br>
	<div class="row-fluid">';
        
        $usid="SELECT * FROM routes_data
                INNER JOIN user_vehicle_details
                ON user_vehicle_details.route_id=routes_data.route_id
                WHERE user_vehicle_details.uId=".$student_id."";
       $exe_usid=mysql_query($usid);
       $fetch_usid=mysql_fetch_array($exe_usid);
$travels=$fetch_usid['name'];
      // echo ' <p align="center" style="margin-top:150Px" id="watermark">Vidya Bal Bhawan</p>';
        

       echo ' <!-- Table widget -->

        
         
	<div class="widget  span12 clearfix">
        <table  width="100%">

        <tr><td align="center" valign="bottom" style="border:0px;solid black; color:BLACK; font-size:12px; ">
	<b>';
	echo $travels;
	echo '</b>

     
	
	</div><!-- End widget-header -->	
	<div class="widget-content" id="d_'.$student_id.'">

	';

//        echo '<p align="center" style="color:BLACK; font-size:14px; position:absolute; left:200px; top:0px ">';
//	echo $fetch_school_name['name_school'];
//	echo ' </p>';
            
	
	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:0px;border-collapse:collapse;">';
            


    
        echo ' <tr style="border:0px;solid black">
        <td width="68%" ALIGN="right" style="border:0px;solid black; font-size:10px"> <b>TRANSPORT RECEIPT</b></td>

<td  ALIGN="right" width="32%" style="border:0px;solid black; font-size:10px"> <b>(Transport Copy )</b></td>
</tr>';
    
        echo ' <tr style="border:0px;solid black">
        <td colspan="2" ALIGN="CENTER" style="border:0px;solid black; font-size:10px"> <b>Session 2014-15</b><br/><br/></td></tr>';
            
   
echo '<tr style="border:0px;solid black">
        <td  style="border:0px;solid black; font-size:10px"> <b>Receipt.No.: '.$receipt_id.' </b></td>';
//            echo '  <td  style="border:0px;solid black; font-size:10px"><b>(Fee Receipt) </b></td>';
        $ddd=explode("-",$date);
        $y=$ddd[0];
        $m=$ddd[1];
        $d=$ddd[2];
        $dates_d=$d.'-'.$m.'-'.$y;
       echo '  <td align="right" style="border:0px;solid black; font-size:10px" > <b>Date</b> : '.$dates_d.'</td>
         </tr>
         <tr style="border:0px;solid black">';
    //   echo '  <td style="border:0px;solid black; font-size:10px"> <b>Admission No.: </b> '.$admission_no.'</td>';

           echo '   <td  align="right" style="border:0px;solid black; font-size:10px"> <b>Class & Sec. : </b> '.$class_section.'</td>


</tr>
          <tr style="border:0px;solid black">
          <td colspan="2" style="border:0px;solid black; font-size:10px"> <b>Passenger Name :</b> '.$name.'</td>
        
     </tr>
         <tr style="border:0px;solid black">

         
        <td colspan="2" align="left" style="border:0px;solid black; font-size:10px"> <b>Father`s Name : </b> '.$father_name.'</td>
     </tr>
             
       
	
             <tr align="center" style="border:0px;solid black; font-size:10px">';
 
        echo '<td colspan="2" style="border:0px;solid black; font-size:10px"><b>Fee of the month ('.$durations_name.')-'.$year_name.'</td></tr>
			 ';
        
     
	
       
               echo '
        </table>';
//        echo '<p> Fee   the   month   of  ( '.$durations_name.' )  -  2014</p>';
         if(($quter==1)&&($admission_fee !=1 ))
        {
           //year mnth
           $fee_structure_details ="SELECT * FROM fee_charges_structure
                                     WHERE class='".$class."'
                                     AND session_id=".$session_id."";
        }
        else 
        {
            //only month
            $fee_structure_details ="SELECT * FROM fee_charges_structure
                                     WHERE class='".$class."'
                                     AND session_id=".$session_id."
                                         AND fee_type_id= 0";
        }
        $exe_structure=mysql_query($fee_structure_details);
       
        $sn=1;
        $total=0;
	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:1px solid black;border-collapse:collapse; font-size:10px">';
	echo '<thead>
	<tr>
	
	<th style="font-size:10px" >PARTICULARS</th>
	<th style="font-size:10px" align="right">AMOUNT</th>

	</tr><tbody >';
        

  
        if($transport_cost>0)
        {
             echo' <tr style="border:0px;solid black; font-size:10px">';
             echo' <td align="left" style="border:0px;solid black; font-size:10px" >Transport Charge</td>';
          
                 $tra=$transport_cost*$into;
               echo' <td align="right" style="border:0px;solid black; font-size:10px" >'.$tra.'</td></tr>';
  
        }
        
        
        // ADMISSION FEE AND OTHER FEE CHARGE 
       
          // ADMISSION FEE AND OTHER FEE CHARGE 
       
        //ffffine
               // ADMISSION FEE AND OTHER FEE CHARGE 
     
        
        
        
  
            
//        $trans_quter=$transport_cost*3;
        $year_month_fee_charge=$year_month_fee_charge+$transport_quter;;
        echo '<tr>
            <td align="left" style="font-size:10px" ><b>Total : ( '.$durations_name.' ) </b></td>';
//          if($ews==1)
//           {
//                echo '  <td align="right" style="font-size:10px" ><b>0</b></tr>';
//           }
//           else 
//           {
        //total with fine
        $totals=$fetch_star_rec['fine']+$year_month_fee_charge;
      
                echo '  <td align="right" style="font-size:10px" ><b>'.$tra.'</b></tr>';
//           }
         
          echo '  </tr>';
          
          //paids
        
    
         

            
               $value = $tra;
             
                echo '<tr>
            <td  width="50%" align="left"  style="font-size:10px" ><b>Paid </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:10px" ><b>'.$tra.' </b></td>';
                        echo '  </tr>';
                         echo '<tr> 
            <td  width="50%" align="left"  style="font-size:10px" ><b>Due </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:10px" ><b>0</b></td>';
                        echo '  </tr>';
           
       
               $value = $tra;
       
               
//                   echo '<tr>
//            <td colspan="2" width="50%" align="center" style="font-size:10px" ><b>PAID: ( '.$paid.' ) </b></td>';
//                        echo '
//            <td  width="50%" align="center" style="font-size:10px" ><b>DUE: ( '.$due.' ) </b></td>';
//                        echo '  </tr>';
           
         //check cash details
            
        
         
          
                
            
                    echo '<tr style="border:0px;solid black; font-size:10px">
            <td colspan="2"   align="left" style="border:0px;solid black; font-size:10px" ><b>Paid by &nbsp;: &nbsp;Cash</b></td> </tr>';
               // echo '<td  align="left" style="border:0px;solid black; font-size:10px">Cash</td>';
          
            echo '    ';
            
              echo '<tr style="border:0px;solid black; font-size:10px">
              <td colspan="2"  align="left" style="border:0px;solid black; font-size:10px" ><b style="text-transform:uppercase;">';
           
                    echo ' In Words - '.convertNumber($value).' only';
              
            echo ' </b></td></tr>';
          
                 echo '<tr style="border:0px;solid black; font-size:10px">
            <td  colspan="2"  align="left" style="border:0px;solid black; font-size:10px" ><b>Date : &nbsp;&nbsp;'.$dates_d.'</b></td>';

                //echo '<td  align="left" style="border:0px;solid black; font-size:10px">'.$date.'</td>';

            echo '  </tr>';
            
            
            //bank 
             $cheque=$_GET['cheque'];

              
        echo '</tbody>
	</thead>';
	
	
	
          echo '</table>';
	echo '
            <table width="100%" style="border:0px;solid black; font-size:10px">

            <tr>
	<td style="border:0px;solid black; font-size:10px; padding-top:40px">Cashier Sign. </td>
<td align="right" style="border:0px;solid black; font-size:10px; padding-top:40px">Depositor Sign </td>
</tr>
        </table>
        </td></tr></table>
	</div><!--  end widget-content -->';
      
    echo' <br><br><br>
	<div class="row-fluid">';
        
        
      // echo ' <p align="center" style="margin-top:150Px" id="watermark">Vidya Bal Bhawan</p>';
        

       echo ' <!-- Table widget -->

        
         
	<div class="widget  span12 clearfix">
        <table  width="100%">

        <tr><td align="center" valign="bottom" style="border:0px;solid black; color:BLACK; font-size:12px; ">
	<b>';
	echo $travels;
	echo '</b>

     
	
	</div><!-- End widget-header -->	
	<div class="widget-content" id="d_'.$student_id.'">

	';

//        echo '<p align="center" style="color:BLACK; font-size:14px; position:absolute; left:200px; top:0px ">';
//	echo $fetch_school_name['name_school'];
//	echo ' </p>';
            
	
	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:0px;border-collapse:collapse;">';
            


    
        echo ' <tr style="border:0px;solid black">
        <td width="68%" ALIGN="right" style="border:0px;solid black; font-size:10px"> <b>TRANSPORT RECEIPT</b></td>

<td  ALIGN="right" width="32%" style="border:0px;solid black; font-size:10px"> <b>( Parent`s Copy )</b></td>
</tr>';
    
        echo ' <tr style="border:0px;solid black">
        <td colspan="2" ALIGN="CENTER" style="border:0px;solid black; font-size:10px"> <b>Session 2014-15</b><br/><br/></td></tr>';
            
   
echo '<tr style="border:0px;solid black">
        <td  style="border:0px;solid black; font-size:10px"> <b>Receipt.No.: '.$receipt_id.' </b></td>';
//            echo '  <td  style="border:0px;solid black; font-size:10px"><b>(Fee Receipt) </b></td>';
        $ddd=explode("-",$date);
        $y=$ddd[0];
        $m=$ddd[1];
        $d=$ddd[2];
        $dates_d=$d.'-'.$m.'-'.$y;
       echo '  <td align="right" style="border:0px;solid black; font-size:10px" > <b>Date</b> : '.$dates_d.'</td>
         </tr>
         <tr style="border:0px;solid black">';
       // echo ' <td style="border:0px;solid black; font-size:10px"> <b>Admission No.: </b> '.$admission_no.'</td>';

          echo '    <td  align="right" style="border:0px;solid black; font-size:10px"> <b>Class & Sec. : </b> '.$class_section.'</td>


</tr>

          <tr style="border:0px;solid black">
          <td colspan="2" style="border:0px;solid black; font-size:10px"> <b>Passenger Name :</b> '.$name.'</td>
        
     </tr>

         <tr style="border:0px;solid black">

         
        <td colspan="2" align="left" style="border:0px;solid black; font-size:10px"> <b>Father`s Name : </b> '.$father_name.'</td>

     </tr>
             
       
	

             <tr align="center" style="border:0px;solid black; font-size:10px">';
 
        echo '<td colspan="2" style="border:0px;solid black; font-size:10px"><b>Fee of the month ('.$durations_name.')-'.$year_name.'</td></tr>
			 ';
        
     
	
       
               echo '
        </table>';
//        echo '<p> Fee   the   month   of  ( '.$durations_name.' )  -  2014</p>';
         if(($quter==1)&&($admission_fee !=1 ))
        {
           //year mnth
           $fee_structure_details ="SELECT * FROM fee_charges_structure
                                     WHERE class='".$class."'
                                     AND session_id=".$session_id."";
        }
        else 
        {
            //only month
            $fee_structure_details ="SELECT * FROM fee_charges_structure
                                     WHERE class='".$class."'
                                     AND session_id=".$session_id."
                                         AND fee_type_id= 0";
        }
        $exe_structure=mysql_query($fee_structure_details);
       
        $sn=1;
        $total=0;
	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:1px solid black;border-collapse:collapse; font-size:10px">';
	echo '<thead>
	<tr>
	
	<th style="font-size:10px" >PARTICULARS</th>
	<th style="font-size:10px" align="right">AMOUNT</th>


	</tr><tbody >';
        

  
        if($transport_cost>0)
        {
             echo' <tr style="border:0px;solid black; font-size:10px">';
             echo' <td align="left" style="border:0px;solid black; font-size:10px" >Transport Charge</td>';
          
                 $tra=$transport_cost*$into;
               echo' <td align="right" style="border:0px;solid black; font-size:10px" >'.$tra.'</td></tr>';
  
        }
        
        
        // ADMISSION FEE AND OTHER FEE CHARGE 
       
          // ADMISSION FEE AND OTHER FEE CHARGE 
       
        //ffffine
               // ADMISSION FEE AND OTHER FEE CHARGE 
     
        
        
        
  
            
//        $trans_quter=$transport_cost*3;
        $year_month_fee_charge=$year_month_fee_charge+$transport_quter;;
        echo '<tr>

            <td align="left" style="font-size:10px" ><b>Total : ( '.$durations_name.' ) </b></td>';
//          if($ews==1)
//           {
//                echo '  <td align="right" style="font-size:10px" ><b>0</b></tr>';
//           }
//           else 
//           {
        //total with fine
        $totals=$fetch_star_rec['fine']+$year_month_fee_charge;
      
                echo '  <td align="right" style="font-size:10px" ><b>'.$tra.'</b></tr>';
//           }
         
          echo '  </tr>';
          
          //paids
         
    
         

            
               $value = $tra;
             
                echo '<tr>
            <td  width="50%" align="left"  style="font-size:10px" ><b>Paid </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:10px" ><b>'.$tra.' </b></td>';
                        echo '  </tr>';
                         echo '<tr> 

            <td  width="50%" align="left"  style="font-size:10px" ><b>Due </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:10px" ><b>0</b></td>';
                        echo '  </tr>';
           
       
               $value = $tra;
       
               
//                   echo '<tr>
//            <td colspan="2" width="50%" align="center" style="font-size:10px" ><b>PAID: ( '.$paid.' ) </b></td>';
//                        echo '
//            <td  width="50%" align="center" style="font-size:10px" ><b>DUE: ( '.$due.' ) </b></td>';
//                        echo '  </tr>';
           
         //check cash details
            
            
         
          
                
            
                    echo '<tr style="border:0px;solid black; font-size:10px">
            <td colspan="2"   align="left" style="border:0px;solid black; font-size:10px" ><b>Paid by &nbsp;: &nbsp;Cash</b></td> </tr>';
               // echo '<td  align="left" style="border:0px;solid black; font-size:10px">Cash</td>';
          
            echo '    ';
            
              echo '<tr style="border:0px;solid black; font-size:10px">

              <td colspan="2"  align="left" style="border:0px;solid black; font-size:10px" ><b style="text-transform:uppercase;">';
           
                    echo ' In Words - '.convertNumber($value).' only';
              
            echo ' </b></td></tr>';
          
                 echo '<tr style="border:0px;solid black; font-size:10px">
            <td  colspan="2"  align="left" style="border:0px;solid black; font-size:10px" ><b>Date : &nbsp;&nbsp;'.$dates_d.'</b></td>';

                //echo '<td  align="left" style="border:0px;solid black; font-size:10px">'.$date.'</td>';

            echo '  </tr>';
            
            
            //bank 
             $cheque=$_GET['cheque'];

              
        echo '</tbody>
	</thead>';
	

          echo '</table>';
	echo '
            <table width="100%" style="border:0px;solid black; font-size:10px">

            <tr>
	<td style="border:0px;solid black; font-size:10px; padding-top:40px">Cashier Sign. </td>
<td align="right" style="border:0px;solid black; font-size:10px; padding-top:40px">Depositor Sign </td>
</tr>
        </table>
        </td></tr></table>
	</div><!--  end widget-content -->';
      
      

}


	if($cheque>0)
	{
	  $message = "D/P, Thank you for paying Rs. $value as fee of your ward $name of class $class_section for the duration $durations_name. ";
	}
	else
	{
	  $message = "D/P, Thank you for paying Rs. $value as fee of your ward $name of class $class_section for the duration $durations_name. ";
	}
      echo '<input type="hidden" id="send_sms" name="'.$phone.'" value="'.$message.'"/>';
       //sendSMS($phone, $message);
        

}

function accounts_fee_today_collection_all()
{
$sn=1;

$cash=0;
$chq=0;

$tution_fee=0;
$development_fee=0;
$activity_fee=0;
$exam_fee=0;
$computer_fee=0;
$annual_charge=0;

$total_transport=0;

$fine=0;
$disc=0;
$total_paid=0;
$total_due=0;

$total_fee=0;
$grand_total_fee=0;

echo '<br><br><br><br><br>';

 $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
          $april_june=date('m');
	$format=date('Y-m-d',strtotime($date));
$dd_format=date('d-M-Y',strtotime($date));
      
        $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
        $paid_on_date_id=$fetch_pre_date11[0];
      
      echo '<table width="100%">
          <tr>
          <td><input type="radio" name="daybook" value="today" id="today" onclick="collection_type(this.value);" checked="checked"><b style="color:red">TODAY</td>
          <td><input type="radio" name="daybook" value="date" id="date" onclick="collection_type(this.value);"><b style="color:red">DATE</td>

           <td><input type="radio" name="daybook" value="month" id="month" onclick="collection_type(this.value);"><b style="color:red">MONTHLY</td>
           <td><input type="radio" name="daybook" value="quter" id="quter" onclick="collection_type(this.value);"><b style="color:red">QUARTERLY</td>
          <td> <input type="radio" name="daybook" value="year" id="year" onclick="collection_type(this.value);"><b style="color:red">YEARLY</td>
           
          </tr>
     </table>';
/*      
         echo'<br><form action="accounts_fee_today_collection_all_print.php" method="get">
        <div class="section">
        <input type="date" name="date" value="'.$format.'">
        </div>
        <div class="section last">

        <input type="submit" value="PRINT">
        </div>
        </form>';

echo'<form action="fee/fee_excel.php" method="get">
     
 <div class="section">
        <input type="date" name="date_a" value="'.$format.'">
        </div>

 <div class="section">
        <input type="date" name="date_b" value="'.$format.'">
        </div>

        <div class="section last">

        <input type="submit" value="Download">
        </div>
        </form>';
*/      
    echo'
        <div id="replace_collection">
    <div class="row-fluid" style="width:120%;">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    <div class="widget-header">
    <span>
      Fee Collection of Today &nbsp; ( '.$dd_format.' )
     </span>
    </div><!-- End widget-header -->	
    <div class="widget-content" style="">
    <table  class="table table-bordered table-striped" >
    <thead>
    <tr>
    <th>S.No.</th>
    <th>Receipt<br> No.</th>
    
    <th>Admission<br> No.</th>
    <th>Student<br> Name</th>
    <th>Father`s<br> Name</th>
    <th>Class</th>
    <th>Duration</th>';
    //fee structure details
//    $april_june=date('m');
    
//    if(($april_june==4))
//    {
      // $fee_charges="SELECT * FROM fee_charges_name";
//    }
//    else 
//    {
//        $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
//    }
//    $exe_fee_charge=mysql_query($fee_charges);
//    while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
//    {
//        $array_fee_charge=explode(" ",$fetch_fee_c['charge_name']);
//        echo '<th>'.$array_fee_charge[0].'<br>'.$array_fee_charge[1].'</th>';
//    }
    echo '  
    <th>Cash/<br>Cheque</th>
    <th>Annual<br>Charge</th>
     
     
    <th>Tution<br>Fee</th>   
    <th>Development<br>Charge</th>   
    
    <th>Fine</th>
    <th>Discount</th>
    <th>TOTAL</th>
    <th>Paid</th>
    <th>Due</th>
    </tr>
    </thead>
    <tbody align="center">
    ';	
    
    $today="SELECT * FROM fee_receipt_details WHERE date=".$paid_on_date_id." AND cheque_status=0";
    $exe_today=mysql_query($today);
    if(!$exe_today)
    {
        
    }
    else 
    {
    while($fetch_today=mysql_fetch_array($exe_today))
    {
       
        
        
        //get student detaILS 
        $name="SELECT * FROM student_user WHERE sId=".$fetch_today['sid']."";
        $exe_name=mysql_query($name);
        $fetch_name=mysql_fetch_array($exe_name);
        
	//GET CLASS NAME
        $class="SELECT class_name, class FROM class_index
INNER JOIN class
ON class.classId= class_index.cId

WHERE class.sId=".$fetch_today['sid']."";
        $exe_class=mysql_query($class);
        $fetch_class=mysql_fetch_array($exe_class);
        echo '
            <tr>
               <td>'.$sn++.'</td>
               <td>'.$fetch_today[1].'</td>
                <td>'.$fetch_name['admission_no'].'</td>
                <td>'.$fetch_name['Name'].'</td>
                <td>'.$fetch_name[5].'</td>
                <td>'.$fetch_class[0].'</td>
                <td>'.$fetch_today['duration_name'].'</td>';

                if($fetch_today['cheque_no']==0)
                {
                   $cash += $fetch_today['amounts'];
                   echo '<td>Cash</td>';
                }
                else 
                {
                      $chq += $fetch_today['amounts'];
                      echo '<td>'.$fetch_today['cheque_no'].'</td>';
                }

$tt=$fetch_today['amount']+$fetch_today['fine'];

echo'
                <td>'.$fetch_today['annual_1'].'</td>
              
                <td>'.$fetch_today['month_1'].'</td>
                <td>'.$fetch_today['month_2'].'</td>
               
                <td>'.$fetch_today['fine'].'</td>
                <td>'.$fetch_today['discount'].'</td>
      		<td style="color:purple;">'.($fetch_today['amounts'] + $fetch_today['due']).'</td>
                <td style="color:green;">'.$fetch_today['amounts'].'</td>
		<td style="color:red;">'.$fetch_today['due'].'</td>';

$tution_fee     += $fetch_today['month_1'];
$development_fee+= $fetch_today['month_2'];


$annual_charge  += $fetch_today['annual_1'];

$fine += $fetch_today['fine'];
$disc += $fetch_today['discount'];


$total_fee += $tution_fee + $development_fee + $activity_fee + $exam_fee + $computer_fee + $annual_charge ;

$grand_total_fee += $total_fee;

$total_paid += $fetch_today['amounts'];
$total_due += $fetch_today['due'];

       echo ' </tr>';
        

//$total=$total+$fetch_today['amount']+$fetch_today['fine'];
    }
    }


echo'
<tr style="font-weight:bold;">
	<td style="color:purple" align="center" colspan="8">GRAND TOTAL</td>
	<td style="color:blue">'.$annual_charge.'</td>

	<td style="color:blue">'.$tution_fee.'</td>
	<td style="color:blue">'.$development_fee.'</td>
	
	<td style="color:blue">'.$fine.'</td>
	<td style="color:blue">'.$disc.'</td>
	<td style="color:purple">'.($total_paid + $total_due).'</td>
	<td style="color:green">'.$total_paid.'</td>
	<td style="color:red">'.$total_due.'</td>
</tr>';
    
echo'
    </tbody>
    </table>';

        //transport charges
	$get_transport = "SELECT SUM(amounts) AS `transport` FROM `fee_receipt_details_transport` WHERE date=".$paid_on_date_id."";
	$exe_transport = mysql_query($get_transport);
	$fetch_transport= mysql_fetch_array($exe_transport);

	$total_transport = $fetch_transport[0];


echo '<br><br>

	<h5 style="color:green">TODAY COLLECTION : </h5>
	<h5 style="color:green">CASH  (  '.$cash.'  )</h5>
	<h5 style="color:green">CHEQUE (  '.$chq.'  )</h5>
	<h5 style="color:green">TOTAL (  '.($cash + $chq).'  )</h5>

	<h5 style="color:green">TRANSPORT FEE (  '.$total_transport.'  )</h5>';
	

   echo ' </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->'; 
    echo '</div>';
    
}

function accounts_fee_today_collection_all_print()
{
   $s=1;
$c=0;$ch=0;$t=0;

$get_date=$_GET['date'];
$tution_fee=0;
$development_fee=0;
$grand_total_transport=0;
$grand_all_due=0;
$grand_all_total=0;
$anual_g=0;
 $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
          $april_june=date('m');
	$format=date('Y-m-d',strtotime($date));
      
        $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$get_date."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
        $paid_on_date_id=$fetch_pre_date11[0];
      
 
      $df=date('d-M-Y',strtotime($get_date));
   
      
    echo'
        <div id="replace_collection">
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    <div class="widget-header">
    
       <h2 style="color:green" align="center">VIDYA BAL BHAWAN SR. SECONDARY SCHOOL</h2>
     
    </div><!-- End widget-header -->	
    <div class="widget-content">
    <table  class="table table-bordered table-striped" id="dataTable" >
<h5 style="color:green" align="center">Fee Collection of '.$df.'</h5>
    <thead>
    <tr>
    <th>S.No.</th>
    <th>Receipt<br> No.</th>
    
    <th>Admission<br> No.</th>
    <th>Student<br> Name</th>
    <th>Father`s<br> Name</th>
    <th>Class</th>
    <th>Date</th>
    <th>Duration</th>
 <th>Annual Charge</th>';
    //fee structure details
    $april_june=date('m');
    
    if(($april_june==4))
    {
      // $fee_charges="SELECT * FROM fee_charges_name";
    }
    else 
    {
        $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
    }
    $exe_fee_charge=mysql_query($fee_charges);
    while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
    {
        $array_fee_charge=explode(" ",$fetch_fee_c['charge_name']);
        echo '<th>'.$array_fee_charge[0].'<br>'.$array_fee_charge[1].'</th>';
    }
    echo '
       
    <th>Transport</th>
    <th>Cash/<br>Cheque</th>
    <th>Paid</th>
    <th>Due</th>
    </tr>
    </thead>

    <tbody align="center">
    ';	
    $i=1;
    
    $today="SELECT * FROM fee_receipt_details WHERE date=".$paid_on_date_id."";
    $exe_today=mysql_query($today);
    if(!$exe_today)
    {
        
    }
    else 
    {
    while($fetch_today=mysql_fetch_array($exe_today))
    {


      $du_value=$fetch_today[9];
              $quter=$fetch_today[7];
                if(($du_value>1000)&&($du_value<1005))
                 {
                    $into=3;
                 }
                 elseif(($du_value>1004)&&($du_value<1007))
                 {
                    $into=6;
                 }
                   elseif(($du_value>1006)&&($du_value<1008))
                 {
                    $into=12;
                 }
                   else
                 {
                     if($du_value==$quter)
                      {
                          $into=1;
                      }
                      else 
                      {
                          $into=$du_value-$quter+1;
                      } 
                 }
        
        //get fee details
        $fee_detail="SELECT * FROM fee_details WHERE student_id=".$fetch_today['sid']."";
        $exe_details=mysql_query($fee_detail);
        $fetch_details=mysql_fetch_array($exe_details);
        $ews=$fetch_details['ews'];
        
        
        //get student detaILS 
        $name="SELECT * FROM student_user WHERE sId=".$fetch_today['sid']."";
        $exe_name=mysql_query($name);
        $fetch_name=mysql_fetch_array($exe_name);
        //transport charges
          $transport_cost=0;
         $trans_amount="SELECT cost
                                   FROM destination_route

                                   INNER JOIN user_vehicle_details
                                   ON user_vehicle_details.did=destination_route.did


                                   WHERE user_vehicle_details.uId=".$fetch_today['sid']."
                                   AND destination_route.session_id=".$_SESSION['current_session_id']."";
        $exe_amount_t=mysql_query($trans_amount);
        while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
                {
                        $transport_cost=$transport_cost+$fetch_transport_cost[0];
                }
        //GET CLASS NAME
        $class="SELECT class_name, class FROM class_index WHERE cId=".$fetch_name['class_id']."";
        $exe_class=mysql_query($class);
        $fetch_class=mysql_fetch_array($exe_class);
        echo '
            <tr>
               <td>'.$s++.'</td>
               <td>'.$fetch_today[0].'</td>
                   

                <td>'.$fetch_name['admission_no'].'</td>
                <td>'.$fetch_name['Name'].'</td>
                <td>'.$fetch_name[5].'</td>
                <td>'.$fetch_class[0].'</td>';
$did=$fetch_today['date'];
 $get_date="SELECT * FROM dates_d WHERE date_id=$did";
$exe_date=mysql_query($get_date);
$fetch_date=mysql_fetch_array($exe_date);
$date=$fetch_date['date'];
echo' <td>'.$date.'</td>';
               echo' <td>'.$fetch_today['duration_name'].'</td>';
             $chargesa="SELECT * FROM fee_charges_structure
                WHERE fee_type_id=1 AND class='".$fetch_class[1]."'";
            $exe_chargesa=mysql_query($chargesa);
            $fetch_chargea=mysql_fetch_array($exe_chargesa);


            if($ews==1)
            {
                echo '<td>0</td>';
            }
        else  if(($fetch_today[9]==1001)||($fetch_today[9]==1005)||($fetch_today[9]==1007)||($fetch_today[9]==1))
        {
          echo '<td>'.$fetch_chargea['amounts'].'</td>';
$anual_g=$anual_g+$fetch_chargea['amounts'];
          }
    else 
{

  echo '<td>0</td>';
}
          
        if(($fetch_today[9]==1001)||($fetch_today[9]==1005)||($fetch_today[9]==1007)||($fetch_today[9]==1))
        {
            $not_annual=0;
          // $fee_charges="SELECT * FROM fee_charges_name";
        }
        else 
        {
             $not_annual=1;
            $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
        }
        $exe_fee_charge=mysql_query($fee_charges);
        while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
        {
            $charges="SELECT * FROM fee_charges_structure
                WHERE fee_name_id=".$fetch_fee_c[0]." AND class='".$fetch_class[1]."'";
            $exe_charges=mysql_query($charges);
            $fetch_charge=mysql_fetch_array($exe_charges);
            if($ews==1)
            {
                echo '<td>0</td>';
            }
        else 
        {
             $total_head_breakup = $fetch_charge['amounts']*$into;
             if($fetch_fee_c[0]==4)
                {
                   $tution_fee = $tution_fee +$total_head_breakup;
                }
             else
               {
                   $development_fee = $development_fee +$total_head_breakup;
               }
		//if transport paid only then all fee ==0
		if($fetch_today['amount']==$fetch_today['transport'])
		{
			echo '<td>0</td>';
		}
		else{
            		echo '<td>'.$total_head_breakup.'</td>';
		}
        }
        }
//        if($not_annual==1)
//        {
//            echo '<td>-</td>';
//        }
               $transport_quterly_into=$transport_cost*$into;
if($fetch_today['due']==$transport_quterly_into)
{

}
else 
{
$grand_total_transport = $grand_total_transport + $transport_quterly_into;
}
//if due > 0 due==transpot then treansport == 0
if(($fetch_today['due'] > 0)&&($fetch_today['due'] == $fetch_today['transport']))
{
	echo '<td>0</td>';
}
else
{
            echo '<td>'.$transport_quterly_into.'</td>';
}
                if($fetch_today['cheque_no']==0)
                {
                   $c=$c+$fetch_today['amount']+$fetch_today['fine'];
                   echo '<td>Cash</td>';
                }
                else 
                {
                      $ch=$ch+$fetch_today['amount']+$fetch_today['fine'];
                      echo '<td>'.$fetch_today['cheque_no'].'</td>';
                }
$tt=$fetch_today['amount']+$fetch_today['fine'];

if($tt==0)
{
$tt=$fetch_today['transport'];
}
                echo ' <td style="color:green">'.$tt.'</td>';
$grand_all_total = $grand_all_total + $tt;
                echo ' <td style="color:red">'.$fetch_today['due'].'</td>';
$grand_all_due = $grand_all_due + $fetch_today['due'];
       echo ' </tr>';
        
      $i++;  

$total=$total+$fetch_today['amount']+$fetch_today['fine'];
    }
    }

echo ' <td style="color:blue" align="center" colspan="8">GRAND TOTAL</td>';
echo ' <td style="color:blue">'.$anual_g.'</td>';
echo ' <td style="color:blue">'.$tution_fee.'</td>';
echo ' <td style="color:blue">'.$development_fee.'</td>';
echo ' <td style="color:blue">'.$grand_total_transport.'</td>';
echo ' <td style="color:blue"></td>';
echo ' <td style="color:blue">'.$grand_all_total.'</td>';
echo ' <td style="color:blue">'.$grand_all_due.'</td>';
    
echo'
    </tbody>
    </table>';

$t=$c+$ch;

//echo '<h5 style="color:green">CASH (  '.$c.'  )</h5>';
//echo '<h5 style="color:green">CHEQUE (  '.$ch.'  )</h5>';
//echo '<h5 style="color:green">TOTAL (  '.$total.'  )</h5>';
echo '</div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->'; 
    echo '</div>';
    
}



function accounts_fee_today_collection_hide()
{
   $sn=1;
 $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
          $april_june=date('m');
	$format=date('Y-m-d',strtotime($date));
      
        $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
        $paid_on_date_id=$fetch_pre_date11[0];
    //  $paid_on_date_id=3474;
     
    
     
      
         echo'<form action="accounts_fee_today_collection_print.php" method="get">
        <div class="section">

        <input type="date" name="date" value="'.$format.'">
        </div>
        <div class="section last">

        <input type="submit" value="PRINT">
        </div>
        </form>';
      
    echo'
        <div id="replace_collection">
    <div class="row-fluid">

    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    <div class="widget-header">

   
      <h2 style="color:green" align="center">VIDYA BAL BHAWAN SR. SECONDARY SCHOOL</h2>
 
    </div><!-- End widget-header -->	

    <div class="widget-content">
    <table  class="table table-bordered table-striped" id="dataTable" >
    <thead>
    <tr>
 <th>S.No.</th>
    <th>Receipt<br> No.</th>
   
    <th>Admission<br> No.</th>
    <th>Student<br> Name</th>
    <th>Father`s<br> Name</th>
    <th>Class</th>
    <th>Duration</th>';
    //fee structure details
    $april_june=date('m');
    
    if(($april_june==4))
    {
      // $fee_charges="SELECT * FROM fee_charges_name";
    }
    else 
    {
        $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
    }
    $exe_fee_charge=mysql_query($fee_charges);
    while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
    {
        $array_fee_charge=explode(" ",$fetch_fee_c['charge_name']);
        echo '<th>'.$array_fee_charge[0].'<br>'.$array_fee_charge[1].'</th>';
    }
    echo '
       
    <th>Transport</th>
    <th>Cheque No.</th>
    <th>Paid</th>
    <th>Due</th>
    </tr>
    </thead>
    <tbody align="center">
    ';	
    $i=1;
    
    $today="SELECT * FROM fee_receipt_details WHERE date=".$paid_on_date_id." AND cheque_no>0";
    $exe_today=mysql_query($today);
    if(!$exe_today)
    {
        
    }
    else 
    {
    while($fetch_today=mysql_fetch_array($exe_today))
    {
        
        //get fee details
        $fee_detail="SELECT * FROM fee_details WHERE student_id=".$fetch_today['sid']."";
        $exe_details=mysql_query($fee_detail);
        $fetch_details=mysql_fetch_array($exe_details);
        $ews=$fetch_details['ews'];
        
        
        //get student detaILS 
        $name="SELECT * FROM student_user WHERE sId=".$fetch_today['sid']."";
        $exe_name=mysql_query($name);
        $fetch_name=mysql_fetch_array($exe_name);
        //transport charges
          $transport_cost=0;
         $trans_amount="SELECT cost
                                   FROM destination_route

                                   INNER JOIN user_vehicle_details
                                   ON user_vehicle_details.did=destination_route.did

                                   WHERE user_vehicle_details.uId=".$fetch_today['sid']."
                                   AND destination_route.session_id=".$_SESSION['current_session_id']."";
        $exe_amount_t=mysql_query($trans_amount);
        while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
                {
                        $transport_cost=$transport_cost+$fetch_transport_cost[0];
                }
        //GET CLASS NAME
        $class="SELECT class_name FROM class_index WHERE cId=".$fetch_name['class_id']."";
        $exe_class=mysql_query($class);
        $fetch_class=mysql_fetch_array($exe_class);
        echo '
            <tr>
               <td>'.$sn.'</td>
               <td>'.$fetch_today[0].'</td>
                 
                <td>'.$fetch_name['admission_no'].'</td>
                <td>'.$fetch_name['Name'].'</td>
                <td>'.$fetch_name[5].'</td>
                <td>'.$fetch_class[0].'</td>
                <td>'.$fetch_today['duration_name'].'</td>';
        
          
        if(($fetch_today[9]==1001)||($fetch_today[9]==1005)||($fetch_today[9]==1007)||($fetch_today[9]==1))
        {
            $not_annual=0;
          // $fee_charges="SELECT * FROM fee_charges_name";
        }
        else 
        {
             $not_annual=1;
            $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
        }
        $exe_fee_charge=mysql_query($fee_charges);
        while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
        {
            $charges="SELECT * FROM fee_charges_structure
                WHERE fee_name_id=".$fetch_fee_c[0]."";
            $exe_charges=mysql_query($charges);
            $fetch_charge=mysql_fetch_array($exe_charges);
            if($ews==1)
            {
                echo '<td>0</td>';
            }
        else 
        {
            echo '<td>'.$fetch_charge['amounts'].'</td>';
        }
        }
//        if($not_annual==1)
//        {
//            echo '<td>-</td>';
//        }
            echo '<td>'.$transport_cost.'</td>';
                if($fetch_today['cheque_no']==0)
                {
                    echo '<td>Cash</td>';
                }
                else 
                {
                    echo '<td>'.$fetch_today['cheque_no'].'</td>';
                }
               echo ' <td style="color:green">'.$fetch_today['amount'].'</td>';
                echo ' <td style="color:red">'.$fetch_today['due'].'</td>';

       echo ' </tr>';
        
      $i++; 
        $sn++; 
    }
    }

    echo'
    </tbody>
    </table>
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->'; 
    echo '</div>';
}
function accounts_fee_today_collection_print()
{
   $sn=1;
$total_paid=0;
$total_due=0;
$total_transport=0;
 $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
          $april_june=date('m');
$date=$_GET['date'];
	$format=date('Y-m-d',strtotime($date));
      $formats=date('d-M-Y',strtotime($date));
        $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
        $paid_on_date_id=$fetch_pre_date11[0];
    //  $paid_on_date_id=3474;
   
      
    echo'
        <div id="replace_collection">
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    <div class="widget-header">
   
      <h2 style="color:green" align="center">VIDYA BAL BHAWAN SR. SECONDARY SCHOOL</h2>
 
    </div><!-- End widget-header -->	
    <div class="widget-content">
    <table  class="table table-bordered table-striped" id="dataTable" >';

echo '<h5 align="center" style="color:red">'.$formats.'</h5>';
 echo '   <thead>
    <tr>
 <th>S.No.</th>
    <th>Receipt<br> No.</th>
   
    <th>Admission<br> No.</th>
    <th>Student<br> Name</th>
    <th>Father`s<br> Name</th>
    <th>Class</th>

    <th>Duration</th>';
    //fee structure details
    $april_june=date('m');
    
    if(($april_june==4))
    {
      // $fee_charges="SELECT * FROM fee_charges_name";
    }
    else 
    {
        $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
    }
    $exe_fee_charge=mysql_query($fee_charges);
    while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
    {
        $array_fee_charge=explode(" ",$fetch_fee_c['charge_name']);
        echo '<th>'.$array_fee_charge[0].'<br>'.$array_fee_charge[1].'</th>';
    }
    echo '

       

    <th>Transport</th>

    <th>Cheque No.</th>

    <th>Paid</th>

    <th>Due</th>

    </tr>

    </thead>

    <tbody align="center">

    ';	
    $i=1;
    
    $today="SELECT * FROM fee_receipt_details WHERE date=".$paid_on_date_id." AND cheque_no>0";
    $exe_today=mysql_query($today);
    if(!$exe_today)
    {
        
    }
    else 
    {
    while($fetch_today=mysql_fetch_array($exe_today))
    {
        
        //get fee details
        $fee_detail="SELECT * FROM fee_details WHERE student_id=".$fetch_today['sid']."";
        $exe_details=mysql_query($fee_detail);
        $fetch_details=mysql_fetch_array($exe_details);
        $ews=$fetch_details['ews'];
        
        
        //get student detaILS 
        $name="SELECT * FROM student_user WHERE sId=".$fetch_today['sid']."";
        $exe_name=mysql_query($name);
        $fetch_name=mysql_fetch_array($exe_name);
        //transport charges
          $transport_cost=0;
         $trans_amount="SELECT cost
                                   FROM destination_route



                                   INNER JOIN user_vehicle_details

                                   ON user_vehicle_details.did=destination_route.did



                                   WHERE user_vehicle_details.uId=".$fetch_today['sid']."

                                   AND destination_route.session_id=".$_SESSION['current_session_id']."";
        $exe_amount_t=mysql_query($trans_amount);
        while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
                {
                        $transport_cost=$transport_cost+$fetch_transport_cost[0];
                }
        //GET CLASS NAME
        $class="SELECT class_name FROM class_index WHERE cId=".$fetch_name['class_id']."";
        $exe_class=mysql_query($class);
        $fetch_class=mysql_fetch_array($exe_class);
        echo '

            <tr>

               <td>'.$sn.'</td>
               <td>'.$fetch_today[0].'</td>

                 

                <td>'.$fetch_name['admission_no'].'</td>

                <td>'.$fetch_name['Name'].'</td>

                <td>'.$fetch_name[5].'</td>

                <td>'.$fetch_class[0].'</td>

                <td>'.$fetch_today['duration_name'].'</td>';
        
          
        if(($fetch_today[9]==1001)||($fetch_today[9]==1005)||($fetch_today[9]==1007)||($fetch_today[9]==1))
        {
            $not_annual=0;
          // $fee_charges="SELECT * FROM fee_charges_name";
        }
        else 
        {
             $not_annual=1;
            $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
        }
        $exe_fee_charge=mysql_query($fee_charges);
        while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
        {
            $charges="SELECT * FROM fee_charges_structure

                WHERE fee_name_id=".$fetch_fee_c[0]."";
            $exe_charges=mysql_query($charges);
            $fetch_charge=mysql_fetch_array($exe_charges);
            if($ews==1)
            {
                echo '<td>0</td>';
            }
        else 
        {
            echo '<td>'.$fetch_charge['amounts'].'</td>';
        }
        }
//        if($not_annual==1)
//        {
//            echo '<td>-</td>';
//        }
      
            echo '<td>'.$transport_cost.'</td>';
                if($fetch_today['cheque_no']==0)
                {
                    echo '<td>Cash</td>';
                }
                else 
                {
                    echo '<td>'.$fetch_today['cheque_no'].'</td>';
                }
               echo ' <td style="color:green">'.$fetch_today['amount'].'</td>';
                echo ' <td style="color:red">'.$fetch_today['due'].'</td>';
       $total_transport=$total_transport+$transport_cost;
$total_paid=$total_paid+$fetch_today['amount'];
$total_due=  $total_due+$fetch_today['due'];

       echo ' </tr>';
        
      $i++; 
        $sn++; 
    }
    }
echo $total_paid;echo '<br>';echo $total_due;
    echo'

<tr style="color:red" align="center"> <td colspan="11">Grand Total</td><td>'.$total_paid.'</td><td>'.$total_due.'</td></tr>
    </tbody>
    </table>
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->'; 
    echo '</div>';
}

function accounts_fee_today_collection_bank()
{
   $sn=1;
 $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
          $april_june=date('m');
	$format=date('Y-m-d',strtotime($date));
      
        $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
        $paid_on_date_id=$fetch_pre_date11[0];
      

      

         echo'<br><br><br><br><br>';
      
    echo'
        <div id="replace_collection">
    <div class="row-fluid">

    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    <div class="widget-header">

   
      <h2 style="color:green" align="center">VIDYA BAL BHAWAN SR. SECONDARY SCHOOL</h2>
 
    </div><!-- End widget-header -->	

    <div class="widget-content">

	<form action="fee/bank_details_excel.php" method="get">

		<div class="section">
			<label>FROM</label>
			<div>
				<input type="date" name="from" value="'.$format.'">
			</div>
		</div>
		
		<div class="section">
			<label>To</label>
			<div>
				<input type="date" name="to" value="'.$format.'">
			</div>
		</div>
		
		<div class="section last" align="center">
			<br><br>
			<input type="submit" value="DOWNLOAD EXCEL (.xls)">
		</div>
        </form>
<br><br>
    <table  class="table table-bordered table-striped" id="dataTable" >
    <thead>

    <tr>

 <th>S.No.</th>

    <th>Receipt<br> No.</th>

   

    <th>Admission<br> No.</th>

    <th>Student<br> Name</th>

    <th>Father`s<br> Name</th>

    <th>Class</th>

    <th>Duration</th>';
    //fee structure details
    $april_june=date('m');
    
    if(($april_june==4))
    {
      // $fee_charges="SELECT * FROM fee_charges_name";
    }
    else 
    {
        $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
    }
    $exe_fee_charge=mysql_query($fee_charges);
    while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
    {
        $array_fee_charge=explode(" ",$fetch_fee_c['charge_name']);
      //  echo '<th>'.$array_fee_charge[0].'<br>'.$array_fee_charge[1].'</th>';
    }
    echo '

       

    

    <th>Cheque No.</th>

    <th>Amounts</th>

    

    </tr>

    </thead>

    <tbody align="center">

    ';	
    $i=1;
    
    $today="SELECT * FROM fee_receipt_details WHERE date=".$paid_on_date_id." AND cheque_no>0";
    $exe_today=mysql_query($today);
    if(!$exe_today)
    {
        
    }
    else 
    {
    while($fetch_today=mysql_fetch_array($exe_today))
    {
   
        
        
        //get student detaILS 
        $name="SELECT * FROM student_user WHERE sId=".$fetch_today['sid']."";
        $exe_name=mysql_query($name);
        $fetch_name=mysql_fetch_array($exe_name);
        //transport charges
          $transport_cost=0;
         $trans_amount="SELECT cost

                                   FROM destination_route



                                   INNER JOIN user_vehicle_details

                                   ON user_vehicle_details.did=destination_route.did



                                   WHERE user_vehicle_details.uId=".$fetch_today['sid']."

                                   AND destination_route.session_id=".$_SESSION['current_session_id']."";
        $exe_amount_t=mysql_query($trans_amount);
        while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
                {
                        $transport_cost=$fetch_transport_cost[0];
                }
        //GET CLASS NAME
        $class="SELECT class_name FROM class_index WHERE cId=".$fetch_name['class_id']."";
        $exe_class=mysql_query($class);
        $fetch_class=mysql_fetch_array($exe_class);
        echo '

            <tr>

               <td>'.$sn.'</td>

               <td>'.$fetch_today[0].'</td>

                 

                <td>'.$fetch_name['admission_no'].'</td>

                <td>'.$fetch_name['Name'].'</td>

                <td>'.$fetch_name[5].'</td>

                <td>'.$fetch_class[0].'</td>

                <td>'.$fetch_today['duration_name'].'</td>';
        
          
        if(($fetch_today[9]==1001)||($fetch_today[9]==1005)||($fetch_today[9]==1007)||($fetch_today[9]==1))
        {
            $not_annual=0;
          // $fee_charges="SELECT * FROM fee_charges_name";
        }
        else 
        {
             $not_annual=1;
            $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
        }
        $exe_fee_charge=mysql_query($fee_charges);
        while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
        {
            $charges="SELECT * FROM fee_charges_structure

                WHERE fee_name_id=".$fetch_fee_c[0]."";
            $exe_charges=mysql_query($charges);
            $fetch_charge=mysql_fetch_array($exe_charges);
            if($ews==1)
            {
               // echo '<td>0</td>';
            }
        else 
        {
           // echo '<td>'.$fetch_charge['amounts'].'</td>';
        }
        }
//        if($not_annual==1)
//        {
//            echo '<td>-</td>';
//        }
         //   echo '<td>'.$transport_cost.'</td>';
                if($fetch_today['cheque_no']==0)
                {
                    echo '<td>Cash</td>';
                }
                else 
                {
                    echo '<td>'.$fetch_today['cheque_no'].'</td>';
                }
                   
                       $a_f_total=$fetch_today['amount']+$fetch_today['fine'];
               echo ' <td style="color:green">'.$a_f_total.'</td>';
              //  echo ' <td style="color:red">'.$fetch_today['due'].'</td>';

       echo ' </tr>';
        $grand_total=$a_f_total+$grand_total;
      $i++; 
        $sn++; 
    }
    }

    echo'

    </tbody>

    </table>

    </div><!--  end widget-content -->

    </div><!-- widget  span12 clearfix-->

    </div><!-- row-fluid -->'; 
    echo '</div>';
}


function accounts_fee_today_collection_bank_print()
{
   $sn=1;
  $grand_total=0;
 $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
         $date=$_GET['date'];
          $april_june=date('m');
	$format=date('Y-m-d',strtotime($date));
         $formats=date('d-M-Y',strtotime($date));
        $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
        $paid_on_date_id=$fetch_pre_date11[0];
   
      
    echo'
        <div id="replace_collection">
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    <div class="widget-header">
   
      <h2 style="color:green" align="center">VIDYA BAL BHAWAN SR. SECONDARY SCHOOL</h2>
 
    </div><!-- End widget-header -->	
    <div class="widget-content">
    <table  class="table table-bordered table-striped" id="dataTable" >

<h5 align="center" style="color:red">'.$formats.'</h5>
    <thead>


    <tr>


 <th>S.No.</th>

    <th>Receipt<br> No.</th>


   

    <th>Admission<br> No.</th>



    <th>Student<br> Name</th>



    <th>Father`s<br> Name</th>



    <th>Class</th>



    <th>Duration</th>';
    //fee structure details
    $april_june=date('m');
    
    if(($april_june==4))
    {
      // $fee_charges="SELECT * FROM fee_charges_name";
    }
    else 
    {
        $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
    }
    $exe_fee_charge=mysql_query($fee_charges);
    while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
    {
        $array_fee_charge=explode(" ",$fetch_fee_c['charge_name']);
      //  echo '<th>'.$array_fee_charge[0].'<br>'.$array_fee_charge[1].'</th>';
    }
    echo '



       



    



    <th>Cheque No.</th>



    <th>Amounts</th>



    



    </tr>



    </thead>



    <tbody align="center">



    ';	
    $i=1;
    
    $today="SELECT * FROM fee_receipt_details WHERE date=".$paid_on_date_id." AND cheque_no>0";
    $exe_today=mysql_query($today);
    if(!$exe_today)
    {
        
    }
    else 
    {
    while($fetch_today=mysql_fetch_array($exe_today))
    {
        
        //get fee details
        $fee_detail="SELECT * FROM fee_details WHERE student_id=".$fetch_today['sid']."";
        $exe_details=mysql_query($fee_detail);
        $fetch_details=mysql_fetch_array($exe_details);
        $ews=$fetch_details['ews'];
        
        
        //get student detaILS 
        $name="SELECT * FROM student_user WHERE sId=".$fetch_today['sid']."";
        $exe_name=mysql_query($name);
        $fetch_name=mysql_fetch_array($exe_name);
        //transport charges
          $transport_cost=0;
         $trans_amount="SELECT cost



                                   FROM destination_route







                                   INNER JOIN user_vehicle_details



                                   ON user_vehicle_details.did=destination_route.did







                                   WHERE user_vehicle_details.uId=".$fetch_today['sid']."



                                   AND destination_route.session_id=".$_SESSION['current_session_id']."";
        $exe_amount_t=mysql_query($trans_amount);
        while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
                {
                        $transport_cost=$fetch_transport_cost[0];
                }
        //GET CLASS NAME
        $class="SELECT class_name FROM class_index WHERE cId=".$fetch_name['class_id']."";
        $exe_class=mysql_query($class);
        $fetch_class=mysql_fetch_array($exe_class);
        echo '



            <tr>



               <td>'.$sn.'</td>



               <td>'.$fetch_today[0].'</td>



                 



                <td>'.$fetch_name['admission_no'].'</td>



                <td>'.$fetch_name['Name'].'</td>



                <td>'.$fetch_name[5].'</td>



                <td>'.$fetch_class[0].'</td>



                <td>'.$fetch_today['duration_name'].'</td>';
        
          
        if(($fetch_today[9]==1001)||($fetch_today[9]==1005)||($fetch_today[9]==1007)||($fetch_today[9]==1))
        {
            $not_annual=0;
          // $fee_charges="SELECT * FROM fee_charges_name";
        }
        else 
        {
             $not_annual=1;
            $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
        }
        $exe_fee_charge=mysql_query($fee_charges);
        while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
        {
            $charges="SELECT * FROM fee_charges_structure



                WHERE fee_name_id=".$fetch_fee_c[0]."";
            $exe_charges=mysql_query($charges);
            $fetch_charge=mysql_fetch_array($exe_charges);
            if($ews==1)
            {
               // echo '<td>0</td>';
            }
        else 
        {
           // echo '<td>'.$fetch_charge['amounts'].'</td>';
        }
        }
//        if($not_annual==1)
//        {
//            echo '<td>-</td>';
//        }
         //   echo '<td>'.$transport_cost.'</td>';
                if($fetch_today['cheque_no']==0)
                {
                    echo '<td>Cash</td>';
                }
                else 
                {
                    echo '<td>'.$fetch_today['cheque_no'].'</td>';
                }
                      $total_amounts=$fetch_today['amounts'];
               echo ' <td style="color:green">'.$total_amounts.'</td>';
              //  echo ' <td style="color:red">'.$fetch_today['due'].'</td>';

       echo ' </tr>';
        $grand_total=$total_amounts+$grand_total;
      $i++; 
        $sn++; 
    }
    }

    echo'

<tr style="color:red"> <td colspan="8">Grand Total</td>
      <td>'.$grand_total.'</td></tr>

    </tbody>



    </table>



    </div><!--  end widget-content -->



    </div><!-- widget  span12 clearfix-->



    </div><!-- row-fluid -->'; 
    echo '</div>';
}

function accounts_collection_type()
{


$id=$_GET['val'];

if($id=="today")
{
     $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
	$format=date('Y-m-d',strtotime($date));

        $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
      $paid_on_date_id=$fetch_pre_date11[0];

    echo'
        <div id="replace_collection">
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    <div class="widget-header">
    <span>
      DayBook Details of Today
     </span>
    </div><!-- End widget-header -->	
    <div class="widget-content">
    <table  class="table table-bordered table-striped" id="dataTable" >
    <thead>
    <tr>
    <th>Receipt<br> No.</th>
    <th>Ref.<br> No.</th>
    <th>Admission<br> No.</th>
    <th>Student<br> Name</th>
    <th>Father`s<br> Name</th>
    <th>Class</th>
    <th>Duration</th>';
    //fee structure details
    $april_june=date('m');
    
    if(($april_june==4))
    {
       $fee_charges="SELECT * FROM fee_charges_name";
    }
    else 
    {
        $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
    }
    $exe_fee_charge=mysql_query($fee_charges);
    while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
    {
        echo '<th>'.$fetch_fee_c['charge_name'].'</th>';
    }
    echo '
       
    <th>Transport</th>
    <th>Cash/<br>Cheque</th>
    <th>Paid</th>
    <th>Due</th>
    </tr>
    </thead>
    <tbody align="center">
    ';	
    $i=1;
    
    $today="SELECT * FROM fee_receipt_details WHERE date=".$paid_on_date_id."";
    $exe_today=mysql_query($today);
    while($fetch_today=mysql_fetch_array($exe_today))
    {
        $fee_detail="SELECT * FROM fee_details WHERE student_id=".$fetch_today['sid']."";
        $exe_details=mysql_query($fee_detail);
        $fetch_details=mysql_fetch_array($exe_details);
        $ews=$fetch_details['ews'];
        //get student detaILS 
        $name="SELECT * FROM student_user WHERE sId=".$fetch_today['sid']."";
        $exe_name=mysql_query($name);
        $fetch_name=mysql_fetch_array($exe_name);
        //transport charges
          $transport_cost=0;
         $trans_amount="SELECT cost
                                   FROM destination_route

                                   INNER JOIN user_vehicle_details
                                   ON user_vehicle_details.did=destination_route.did

                                   WHERE user_vehicle_details.uId=".$fetch_today['sid']."
                                   AND destination_route.session_id=".$_SESSION['current_session_id']."";
        $exe_amount_t=mysql_query($trans_amount);
        while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
                {
                        $transport_cost=$transport_cost+$fetch_transport_cost[0];
                }
        //GET CLASS NAME
        $class="SELECT class_name FROM class_index WHERE cId=".$fetch_name['class_id']."";
        $exe_class=mysql_query($class);
        $fetch_class=mysql_fetch_array($exe_class);
        echo '
            <tr>
               <td>'.$fetch_today[0].'</td>
                   <td>'.$fetch_today['ref_no'].'</td>
                <td>'.$fetch_name['admission_no'].'</td>
                <td>'.$fetch_name['Name'].'</td>
                <td>'.$fetch_name[5].'</td>
                <td>'.$fetch_class[0].'</td>
                <td>'.$fetch_today['duration_name'].'</td>';
        
          
        if(($fetch_today[9]==1001)||($fetch_today[9]==1005)||($fetch_today[9]==1007)||($fetch_today[9]==1))
        {
            $not_annual=0;
           $fee_charges="SELECT * FROM fee_charges_name";
        }
        else 
        {
             $not_annual=1;
            $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
        }
        $exe_fee_charge=mysql_query($fee_charges);
        while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
        {
            $charges="SELECT * FROM fee_charges_structure
                WHERE fee_name_id=".$fetch_fee_c[0]."";
            $exe_charges=mysql_query($charges);
            $fetch_charge=mysql_fetch_array($exe_charges);
            if($ews==1)
            {
                echo '<td>0</td>';
            }
            else 
            {
                 echo '<td>'.$fetch_charge['amounts'].'</td>';
            }
        }
//        if($not_annual==1)
//        {
//            echo '<td>-</td>';
//        }
            echo '<td>'.$transport_cost.'</td>';
                if($fetch_today['cheque_no']==0)
                {
                    echo '<td>Cash</td>';
                }
                else 
                {
                    echo '<td>'.$fetch_today['cheque_no'].'</td>';
                }
               echo ' <td style="color:green">'.$fetch_today['amount'].'</td>';
                echo ' <td style="color:red">'.$fetch_today['due'].'</td>';

       echo ' </tr>';
        
      $i++;  
    }

    echo'
    </tbody>
    </table>
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->'; 
    echo '</div>';
}
elseif($id=="date")
{
    
      $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
	$format=date('Y-m-d',strtotime($date));

        $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
      $paid_on_date_id=$fetch_pre_date11[0];
      
      
    echo ' 

';


   echo'
        <div id="replace_collection">
            
            <div class="section">
            <label>Select Date</label>   
            <div><input type="text"  id="birthday" value="date" class=" birthday  small " name="birthday" onchange="date_picker(this.value);" /></div>
        </div>  
            
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    <div class="widget-header">
    <span>
      DayBook  According to Date
     </span>
    </div><!-- End widget-header -->	
    <div class="widget-content">
    <table  class="table table-bordered table-striped" id="dataTable" >
    <thead>
    <tr>
    <th>Receipt<br> No.</th>
    <th>Ref.<br> No.</th>
    <th>Admission<br> No.</th>
    <th>Student<br> Name</th>
    <th>Father`s<br> Name</th>
    <th>Class</th>
    <th>Duration</th>';
    //fee structure details
    $april_june=date('m');
    
    if(($april_june==4))
    {
       $fee_charges="SELECT * FROM fee_charges_name";
    }
    else 
    {
        $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
    }
    $exe_fee_charge=mysql_query($fee_charges);
    while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
    {
        echo '<th>'.$fetch_fee_c['charge_name'].'</th>';
    }
    echo '
       
    <th>Transport</th>
    <th>Cash/<br>Cheque</th>
    <th>Paid</th>
    <th>Due</th>
    </tr>
    </thead>
    <tbody align="center">
    ';	
    $i=1;
    
    $today="SELECT * FROM fee_receipt_details WHERE date=".$paid_on_date_id."";
    $exe_today=mysql_query($today);
    while($fetch_today=mysql_fetch_array($exe_today))
    {
        
        //get fee details
        $fee_detail="SELECT * FROM fee_details WHERE student_id=".$fetch_today['sid']."";
        $exe_details=mysql_query($fee_detail);
        $fetch_details=mysql_fetch_array($exe_details);
        $ews=$fetch_details['ews'];
        
        //get student detaILS 
        $name="SELECT * FROM student_user WHERE sId=".$fetch_today['sid']."";
        $exe_name=mysql_query($name);
        $fetch_name=mysql_fetch_array($exe_name);
        //transport charges
          $transport_cost=0;
         $trans_amount="SELECT cost
                                   FROM destination_route

                                   INNER JOIN user_vehicle_details
                                   ON user_vehicle_details.did=destination_route.did

                                   WHERE user_vehicle_details.uId=".$fetch_today['sid']."
                                   AND destination_route.session_id=".$_SESSION['current_session_id']."";
        $exe_amount_t=mysql_query($trans_amount);
        while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
                {
                        $transport_cost=$transport_cost+$fetch_transport_cost[0];
                }
        //GET CLASS NAME
        $class="SELECT class_name FROM class_index WHERE cId=".$fetch_name['class_id']."";
        $exe_class=mysql_query($class);
        $fetch_class=mysql_fetch_array($exe_class);
        echo '
            <tr>
               <td>'.$fetch_today[0].'</td>
                   <td>'.$fetch_today['ref_no'].'</td>
                <td>'.$fetch_name['admission_no'].'</td>
                <td>'.$fetch_name['Name'].'</td>
                <td>'.$fetch_name[5].'</td>
                <td>'.$fetch_class[0].'</td>
                <td>'.$fetch_today['duration_name'].'</td>';
        
          
        if(($fetch_today[9]==1001)||($fetch_today[9]==1005)||($fetch_today[9]==1007)||($fetch_today[9]==1))
        {
            $not_annual=0;
           $fee_charges="SELECT * FROM fee_charges_name";
        }
        else 
        {
             $not_annual=1;
            $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
        }
        $exe_fee_charge=mysql_query($fee_charges);
        while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
        {
            $charges="SELECT * FROM fee_charges_structure
                WHERE fee_name_id=".$fetch_fee_c[0]."";
            $exe_charges=mysql_query($charges);
            $fetch_charge=mysql_fetch_array($exe_charges);
            if($ews==1)
            {
                echo '<td>0</td>';
            }
            else 
            {
            echo '<td>'.$fetch_charge['amounts'].'</td>';
            }
        }
//        if($not_annual==1)
//        {
//            echo '<td>-</td>';
//        }
            echo '<td>'.$transport_cost.'</td>';
                if($fetch_today['cheque_no']==0)
                {
                    echo '<td>Cash</td>';
                }
                else 
                {
                    echo '<td>'.$fetch_today['cheque_no'].'</td>';
                }
               echo ' <td style="color:green">'.$fetch_today['amount'].'</td>';
                echo ' <td style="color:red">'.$fetch_today['due'].'</td>';

       echo ' </tr>';
        
      $i++;  
    }

    echo'
    </tbody>
    </table>
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->'; 
    echo '</div>';
}
elseif($id=="month")
{
    
      $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
	$format=date('Y-m-d',strtotime($date));

        $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
      $paid_on_date_id=$fetch_pre_date11[0];
 
      $month="SELECT * FROM fee_generated_sessions";
      $exe=mysql_query($month);

 
   echo'
        <div id="replace_collection">
        
     
     <div class="section">
           <label>Select Month</label>   
           <div>
          <select name="month">
          <option value="">--select monthly--</option>';
       while($fetch=mysql_fetch_array($exe))
       {
           echo '<option value="'.$fetch[0].'">'.$fetch[2].'</option>';
       }
echo '</select></div></div>
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    <div class="widget-header">
    <span>
      DayBook Details Monthly
     </span>
    </div><!-- End widget-header -->	
    <div class="widget-content">
    <table  class="table table-bordered table-striped" id="dataTable" >
    <thead>
    <tr>
    <th>Receipt<br> No.</th>
    <th>Ref.<br> No.</th>
    <th>Admission<br> No.</th>
    <th>Student<br> Name</th>
    <th>Father`s<br> Name</th>
    <th>Class</th>
    <th>Duration</th>';
    //fee structure details
    $april_june=date('m');
     $year=date('Y');
    $max_min="SELECT MAX(date_id), MIN(date_id) FROM dates_d WHERE month_of_year=".$april_june." AND year=".$year."";
    $exe_max_min=mysql_query($max_min);
    $fetch_max_min=mysql_fetch_array($exe_max_min);
    $min_date=$fetch_max_min[1];
    $max_date=$fetch_max_min[0];
    if(($april_june==4))
    {
       $fee_charges="SELECT * FROM fee_charges_name";
    }
    else 
    {
        $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
    }
    $exe_fee_charge=mysql_query($fee_charges);
    while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
    {
        echo '<th>'.$fetch_fee_c['charge_name'].'</th>';
    }
    echo '
       
    <th>Transport</th>
    <th>Cash/<br>Cheque</th>
    <th>Paid</th>
    <th>Due</th>
    </tr>
    </thead>
    <tbody align="center">
    ';	
    $i=1;
    
    $today="SELECT * FROM fee_receipt_details WHERE date BETWEEN ".$min_date." AND ".$max_date."";
    $exe_today=mysql_query($today);
    while($fetch_today=mysql_fetch_array($exe_today))
    {
        
        //get fee details
        $fee_detail="SELECT * FROM fee_details WHERE student_id=".$fetch_today['sid']."";
        $exe_details=mysql_query($fee_detail);
        $fetch_details=mysql_fetch_array($exe_details);
        $ews=$fetch_details['ews'];
        
        //get student detaILS 
        $name="SELECT * FROM student_user WHERE sId=".$fetch_today['sid']."";
        $exe_name=mysql_query($name);
        $fetch_name=mysql_fetch_array($exe_name);
        //transport charges
          $transport_cost=0;
         $trans_amount="SELECT cost
                                   FROM destination_route

                                   INNER JOIN user_vehicle_details
                                   ON user_vehicle_details.did=destination_route.did

                                   WHERE user_vehicle_details.uId=".$fetch_today['sid']."
                                   AND destination_route.session_id=".$_SESSION['current_session_id']."";
        $exe_amount_t=mysql_query($trans_amount);
        while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
                {
                        $transport_cost=$transport_cost+$fetch_transport_cost[0];
                }
        //GET CLASS NAME
        $class="SELECT class_name FROM class_index WHERE cId=".$fetch_name['class_id']."";
        $exe_class=mysql_query($class);
        $fetch_class=mysql_fetch_array($exe_class);
        echo '
            <tr>
               <td>'.$fetch_today[0].'</td>
                   <td>'.$fetch_today['ref_no'].'</td>
                <td>'.$fetch_name['admission_no'].'</td>
                <td>'.$fetch_name['Name'].'</td>
                <td>'.$fetch_name[5].'</td>
                <td>'.$fetch_class[0].'</td>
                <td>'.$fetch_today['duration_name'].'</td>';
        
          
        if(($fetch_today[9]==1001)||($fetch_today[9]==1005)||($fetch_today[9]==1007)||($fetch_today[9]==1))
        {
            $not_annual=0;
           $fee_charges="SELECT * FROM fee_charges_name";
        }
        else 
        {
             $not_annual=1;
            $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
        }
        $exe_fee_charge=mysql_query($fee_charges);
        while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
        {
            $charges="SELECT * FROM fee_charges_structure
                WHERE fee_name_id=".$fetch_fee_c[0]."";
            $exe_charges=mysql_query($charges);
            $fetch_charge=mysql_fetch_array($exe_charges);
            if($ews==1)
            {
                echo '<td>0</td>';
            }
            else 
            {
            echo '<td>'.$fetch_charge['amounts'].'</td>';
            }
        }
//        if($not_annual==1)
//        {
//            echo '<td>-</td>';
//        }
            echo '<td>'.$transport_cost.'</td>';
                if($fetch_today['cheque_no']==0)
                {
                    echo '<td>Cash</td>';
                }
                else 
                {
                    echo '<td>'.$fetch_today['cheque_no'].'</td>';
                }
               echo ' <td style="color:green">'.$fetch_today['amount'].'</td>';
                echo ' <td style="color:red">'.$fetch_today['due'].'</td>';

       echo ' </tr>';
        
      $i++;  
    }

    echo'
    </tbody>
    </table>
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->'; 
    echo '</div>';
}
elseif($id=="quter")
{
    
      $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
	$format=date('Y-m-d',strtotime($date));

        $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
      $paid_on_date_id=$fetch_pre_date11[0];
 
      $month="SELECT * FROM fee_generated_sessions";
      $exe=mysql_query($month);
     

 
       

      echo'
        <div id="replace_collection">';
        
      echo '<div class="section">
           <label>Select Qurter</label>   
           <div>
          <select name="month">
          <option value="">--select quarterly--</option>';
      
           echo '<option value="1001">April-June</option>';
            echo '<option value="1002">July-Sep</option>';
             echo '<option value="1003">Oct-Dec</option>';
              echo '<option value="1004">Jan-March</option>';
echo '</select></div></div>
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    <div class="widget-header">
    <span>
      DayBook Details Quarterly
     </span>
    </div><!-- End widget-header -->	
    <div class="widget-content">
    <table  class="table table-bordered table-striped" id="dataTable" >
    <thead>
    <tr>
    <th>Receipt<br> No.</th>
    <th>Ref.<br> No.</th>
    <th>Admission<br> No.</th>
    <th>Student<br> Name</th>
    <th>Father`s<br> Name</th>
    <th>Class</th>
    <th>Duration</th>';
    //fee structure details
    $april_june=date('m');
     $year=date('Y');
    $max_min="SELECT MAX(date_id), MIN(date_id) FROM dates_d WHERE month_of_year=".$april_june." AND year=".$year."";
    $exe_max_min=mysql_query($max_min);
    $fetch_max_min=mysql_fetch_array($exe_max_min);
    $min_date=$fetch_max_min[1] - 30;
    $max_date=$fetch_max_min[0] + 30;
    if(($april_june==4))
    {
       $fee_charges="SELECT * FROM fee_charges_name";
    }
    else 
    {
        $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
    }
    $exe_fee_charge=mysql_query($fee_charges);
    while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
    {
        echo '<th>'.$fetch_fee_c['charge_name'].'</th>';
    }
    echo '
       
    <th>Transport</th>
    <th>Cash/<br>Cheque</th>
    <th>Paid</th>
    <th>Due</th>
    </tr>
    </thead>
    <tbody align="center">
    ';	
    $i=1;
    
    $today="SELECT * FROM fee_receipt_details WHERE date BETWEEN ".$min_date." AND ".$max_date."";
    $exe_today=mysql_query($today);
    while($fetch_today=mysql_fetch_array($exe_today))
    {
        
        //get fee details
        $fee_detail="SELECT * FROM fee_details WHERE student_id=".$fetch_today['sid']."";
        $exe_details=mysql_query($fee_detail);
        $fetch_details=mysql_fetch_array($exe_details);
        $ews=$fetch_details['ews'];
        
        
        //get student detaILS 
        $name="SELECT * FROM student_user WHERE sId=".$fetch_today['sid']."";
        $exe_name=mysql_query($name);
        $fetch_name=mysql_fetch_array($exe_name);
        //transport charges
          $transport_cost=0;
         $trans_amount="SELECT cost
                                   FROM destination_route

                                   INNER JOIN user_vehicle_details
                                   ON user_vehicle_details.did=destination_route.did

                                   WHERE user_vehicle_details.uId=".$fetch_today['sid']."
                                   AND destination_route.session_id=".$_SESSION['current_session_id']."";
        $exe_amount_t=mysql_query($trans_amount);
        while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
                {
                        $transport_cost=$transport_cost+$fetch_transport_cost[0];
                }
        //GET CLASS NAME
        $class="SELECT class_name FROM class_index WHERE cId=".$fetch_name['class_id']."";
        $exe_class=mysql_query($class);
        $fetch_class=mysql_fetch_array($exe_class);
        echo '
            <tr>
               <td>'.$fetch_today[0].'</td>
                   <td>'.$fetch_today['ref_no'].'</td>
                <td>'.$fetch_name['admission_no'].'</td>
                <td>'.$fetch_name['Name'].'</td>
                <td>'.$fetch_name[5].'</td>
                <td>'.$fetch_class[0].'</td>
                <td>'.$fetch_today['duration_name'].'</td>';
        
          
        if(($fetch_today[9]==1001)||($fetch_today[9]==1005)||($fetch_today[9]==1007)||($fetch_today[9]==1))
        {
            $not_annual=0;
           $fee_charges="SELECT * FROM fee_charges_name";
        }
        else 
        {
             $not_annual=1;
            $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
        }
        $exe_fee_charge=mysql_query($fee_charges);
        while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
        {
            $charges="SELECT * FROM fee_charges_structure
                WHERE fee_name_id=".$fetch_fee_c[0]."";
            $exe_charges=mysql_query($charges);
            $fetch_charge=mysql_fetch_array($exe_charges);
            if($ews==1)
            {
                echo '<td>0</td>';
            }
            else 
            {
            echo '<td>'.$fetch_charge['amounts'].'</td>';
            }
        }
//        if($not_annual==1)
//        {
//            echo '<td>-</td>';
//        }
            echo '<td>'.$transport_cost.'</td>';
                if($fetch_today['cheque_no']==0)
                {
                    echo '<td>Cash</td>';
                }
                else 
                {
                    echo '<td>'.$fetch_today['cheque_no'].'</td>';
                }
               echo ' <td style="color:green">'.$fetch_today['amount'].'</td>';
                echo ' <td style="color:red">'.$fetch_today['due'].'</td>';

       echo ' </tr>';
        
      $i++;  
    }

    echo'
    </tbody>
    </table>
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->'; 
    echo '</div>';
}
elseif($id=="year")
{
    
      $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
	$format=date('Y-m-d',strtotime($date));

        $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
      $paid_on_date_id=$fetch_pre_date11[0];

   echo'
        <div id="replace_collection">
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    <div class="widget-header">
    <span>
      DayBook Details Yearly
     </span>
    </div><!-- End widget-header -->	
    <div class="widget-content">
    <table  class="table table-bordered table-striped" id="dataTable" >
    <thead>
    <tr>
    <th>Receipt<br> No.</th>
    <th>Ref.<br> No.</th>
    <th>Admission<br> No.</th>
    <th>Student<br> Name</th>
    <th>Father`s<br> Name</th>
    <th>Class</th>
    <th>Duration</th>';
    //fee structure details
    $april_june=date('m');
    
//    if(($april_june==4))
//    {
       $fee_charges="SELECT * FROM fee_charges_name";
//    }
//    else 
//    {
//        $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
//    }
    $exe_fee_charge=mysql_query($fee_charges);
    while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
    {
        echo '<th>'.$fetch_fee_c['charge_name'].'</th>';
    }
    echo '
       
    <th>Transport</th>
    <th>Cash/<br>Cheque</th>
    <th>Paid</th>
    <th>Due</th>
    </tr>
    </thead>
    <tbody align="center">
    ';	
    $i=1;
    
    $today="SELECT * FROM fee_receipt_details ";
    $exe_today=mysql_query($today);
    while($fetch_today=mysql_fetch_array($exe_today))
    {
        
        //get fee details
        $fee_detail="SELECT * FROM fee_details WHERE student_id=".$fetch_today['sid']."";
        $exe_details=mysql_query($fee_detail);
        $fetch_details=mysql_fetch_array($exe_details);
        $ews=$fetch_details['ews'];
        
        //get student detaILS 
        $name="SELECT * FROM student_user WHERE sId=".$fetch_today['sid']."";
        $exe_name=mysql_query($name);
        $fetch_name=mysql_fetch_array($exe_name);
        //transport charges
          $transport_cost=0;
         $trans_amount="SELECT cost
                                   FROM destination_route

                                   INNER JOIN user_vehicle_details
                                   ON user_vehicle_details.did=destination_route.did

                                   WHERE user_vehicle_details.uId=".$fetch_today['sid']."
                                   AND destination_route.session_id=".$_SESSION['current_session_id']."";
        $exe_amount_t=mysql_query($trans_amount);
        while($fetch_transport_cost=mysql_fetch_array($exe_amount_t))
                {
                        $transport_cost=$transport_cost+$fetch_transport_cost[0];
                }
        //GET CLASS NAME
        $class="SELECT class_name FROM class_index WHERE cId=".$fetch_name['class_id']."";
        $exe_class=mysql_query($class);
        $fetch_class=mysql_fetch_array($exe_class);
        echo '
            <tr>
               <td>'.$fetch_today[0].'</td>
                   <td>'.$fetch_today['ref_no'].'</td>
                <td>'.$fetch_name['admission_no'].'</td>
                <td>'.$fetch_name['Name'].'</td>
                <td>'.$fetch_name[5].'</td>
                <td>'.$fetch_class[0].'</td>
                <td>'.$fetch_today['duration_name'].'</td>';
        
          
//        if(($fetch_today[9]==1001)||($fetch_today[9]==1005)||($fetch_today[9]==1007)||($fetch_today[9]==1))
//        {
            $not_annual=0;
           $fee_charges="SELECT * FROM fee_charges_name";
//        }
//        else 
//        {
//             $not_annual=1;
//            $fee_charges="SELECT * FROM fee_charges_name WHERE charge_type=0";
//        }
        $exe_fee_charge=mysql_query($fee_charges);
        while($fetch_fee_c=mysql_fetch_array($exe_fee_charge))
        {
            $charges="SELECT * FROM fee_charges_structure
                WHERE fee_name_id=".$fetch_fee_c[0]."";
            $exe_charges=mysql_query($charges);
            $fetch_charge=mysql_fetch_array($exe_charges);
            if($ews==1)
            {
                echo '<td>0</td>';
            }
            else 
            {
            echo '<td>'.$fetch_charge['amounts'].'</td>';
            }
        }
//        if($not_annual==1)
//        {
//            echo '<td>-</td>';
//        }
            echo '<td>'.$transport_cost.'</td>';
                if($fetch_today['cheque_no']==0)
                {
                    echo '<td>Cash</td>';
                }
                else 
                {
                    echo '<td>'.$fetch_today['cheque_no'].'</td>';
                }
               echo ' <td style="color:green">'.$fetch_today['amount'].'</td>';
                echo ' <td style="color:red">'.$fetch_today['due'].'</td>';

       echo ' </tr>';
        
      $i++;  
    }

    echo'
    </tbody>
    </table>
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->'; 
    echo '</div>';
    
}




}
//
function accounts_submit_fee_class()
{
   $current_session=$_SESSION['current_session_id'];

 $cid=$_GET['id'];
	if($cid==0)
	{
		
	}
	else
	{
	$query="SELECT * , class_name
	FROM student_user
	INNER JOIN class
	ON student_user.sId = class.sId
	INNER JOIN class_index
	ON class.classId = class_index.cId
	WHERE class.classId  = ".$cid."
	";
	$execute=mysql_query($query);
    echo'
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    
    
   	
    <div class="widget-content">
    <table  class="table table-bordered table-striped" id="dataTable" >
    <thead>
    <tr>
   <td width="10%">ADMISSION NO</td>  
	<td width="10%">NAME</td>  
	<td width="10%">CLASS</td>  
	<td width="10%">DOB</td>  
	<td width="10%">GENDER</td>  
	<td width="10%">FATHER</td>  
	<td width="10%">MOTHER</td>  
	<td width="10%">PHONE   </td>
    </tr>
    </thead>
    <tbody align="center">
    ';	
    	while($fetch=mysql_fetch_array($execute))
	{
	echo'
	<tr height="50%" align="center">
	<td>'.$fetch['admission_no'].'</td>
	<td><a href="accounts_subbmit_fee_class.php?data='.$fetch['sId'].'">'.$fetch['Name'].'</a></td>  
	<td >'.$fetch['class_name'].'</td>  
	<td>'.$fetch['DOB'].'</td>  
	<td>'.$fetch['gender'].'</td>  
	<td>'.$fetch['Father\'s Name'].'</td>  
	<td>'.$fetch['Mother\'s Name'].'</td>  
	<td>'.$fetch['Guardian\'s Contact'].'</td>   
	</tr>';
	}
	echo '</tbody></table><br/><br/>';
	}

    echo'
    </tbody>
    </table>
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->'; 
    
}

function accounts_fee_receipt()
{
    $session_id=$_SESSION['current_session_id'];
    $fee_receipt="SELECT * FROM fee_receipt_details ";
    $exe_receipt=mysql_query($fee_receipt);
if(isset($_GET['delete_receipt']))
    {
        echo '<h4 align="center" style="color:red">DELETE FEE RECEIPT SUCCESSFULY!</h4>'; 
    }
     if(isset($_GET['delete_receipt_no']))
    {
        echo '<h4 align="center" style="color:red">CAN`T DELETE IN MIDDLE!</h4>'; 
    }
if(isset($_GET['delete_c']))
    {
        echo '<h4 align="center" style="color:red">CHEQUE BOUNCE SUCCESSFULY!</h4>'; 
    }
     if(isset($_GET['delete_c_no']))
    {
        echo '<h4 align="center" style="color:red">CAN`T CHEQUE BOUNCE!</h4>'; 
    }
    echo'
    <div id="replace_collection">
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    <div class="widget-header">
    <span>
    Fee Receipt
    </span>
    </div><!-- End widget-header -->	
    <div class="widget-content">
    <table  class="table table-bordered table-striped" id="dataTable"  >
    <thead align="left">
    <tr style="color:green">
    <th>Receipt<br> No.</th>
    <th>Admission<br> No.</th>
    <th>Student<br> Name</th>
    <th>Father`s<br> Name</th>
    <th>Class</th>
    <th>Duration</th>
    <th>Date</th>
<th>Cheque<br>/Cash</th>
    <th>Bank</th>
    <th>Status</th>
    ';
//    echo '<th>Transport</th>
//    <th>Discount</th>';
    echo '
    <th style="color:green">Total  Fee</th>
    <th style="color:green">Paid</th>
    <th style="color:red">Due</th>
    
    <th>Modify</th>
    </tr>
    </thead>
    <tbody align="left">
     ';
    while($fetch_receipt=mysql_fetch_array($exe_receipt))
    {
        //STUDENT DETAILSS
        $student_details="SELECT * FROM student_user WHERE sId=".$fetch_receipt['sid']."";
        $exe_student=mysql_query($student_details);
        $fetch_student=mysql_fetch_array($exe_student);
        //CLASS
        $class_name="SELECT class_name FROM class_index
            INNER JOIN class
            ON class.classId =class_index.cId
            WHERE class.sId = ".$fetch_receipt['sid']."
            AND class.session_id= ".$session_id."";
        $exe_class=mysql_query($class_name);
        $fetch_class=mysql_fetch_array($exe_class);
        
        echo '<tr>';
        echo '<td>'.$fetch_receipt[1].'</td>';
        echo '<td>'.$fetch_student['admission_no'].'</td>';
        echo '<td><a href="accounts_fee_receipt_print_new_vidya_2.php?fee_receipt1='.$fetch_receipt[1].'"> '.$fetch_student['Name'].'</a></td>';
        echo '<td>'.$fetch_student[5].'</td>';
        echo '<td>'.$fetch_class['class_name'].'</td>';
        echo '<td>'.$fetch_receipt['duration_name'].'</td>';
        
        //date
        $date="SELECT date FROM dates_d WHERE date_id=".$fetch_receipt['date']."";
        $exe_date=mysql_query($date);
        $fetch_date=mysql_fetch_array($exe_date);
	$dd = date('d-M-Y', strtotime($fetch_date[0]));
        echo '<td>'.$dd.'</td>';
       // echo '<td>'.$fetch_receipt['pre_remaning_fee'].'</td>';
//        echo '<td>0</td>';
//        echo '<td>'.$fetch_receipt['discount'].'</td>';
       // echo '<td>'.$fetch_receipt['fine'].'</td>';
        //
	if($fetch_receipt['cheque_no'] == 0)
        {
          echo '<td>Cash</td>';
        }
        else 
        {
            echo '<td><a href="#" onclick="cheque_bounce('.$fetch_receipt[1].');">'.$fetch_receipt['cheque_no'].'</a></td>';
        }
        echo '<td>'.$fetch_receipt['bank'].'</td>';
	if($fetch_receipt['cheque_status']==-1)
	{
		echo '<td>Bounce</td>';
	}
	else 
	{
		echo '<td>Clear</td>';
	}
        $total = $fetch_receipt['amounts'] + $fetch_receipt['due'] ;
        
        echo '<td style="color:green">'.$total.'</td>';
        echo '<td style="color:green">'.$fetch_receipt['amounts'].'</td>';
        echo '<td style="color:red">'.$fetch_receipt['due'].'</td>';
        
        
        
           echo '<td style="color:red">';
//           echo '  <span class="tip"><a href="#" original-title="Edit">
//			<img src="images/icon/icon_edit.png"></a></span> ';
		echo '	<span class="tip"><a href="#" onclick="delete_receipt('.$fetch_receipt[1].');" original-title="Delete">
			<img src="images/icon/icon_delete.png"></a></span> ';
        echo '</td>';
        echo '</tr>';
    }
    echo'
    </tbody>
    </table>
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';   
}
function accounts_fee_today_receipt()
{
    $session_id=$_SESSION['current_session_id'];
 $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
          $april_june=date('m');
	$format=date('Y-m-d',strtotime($date));
      
       $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
        $paid_on_date_id=$fetch_pre_date11[0];
    $fee_receipt="SELECT * FROM fee_receipt_details WHERE date=".$paid_on_date_id."";
    $exe_receipt=mysql_query($fee_receipt);
if(isset($_GET['delete_receipt']))
    {
        echo '<h4 align="center" style="color:red">DELETE FEE RECEIPT SUCCESSFULY!</h4>'; 
    }
     if(isset($_GET['delete_receipt_no']))
    {
        echo '<h4 align="center" style="color:red">CAN`T DELETE IN MIDDLE!</h4>'; 
    }
    echo'

    <div id="replace_collection">
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">

    <div class="widget-header">
    <span>
    Fee Receipt

    </span>
    </div><!-- End widget-header -->	
    <div class="widget-content">

    <table  class="table table-bordered table-striped" id="dataTable"  >
    <thead align="left">
    <tr style="color:green">
    <th>Receipt<br> No.</th>

    <th>Admission<br> No.</th>

    <th>Student<br> Name</th>

    <th>Father`s<br> Name</th>

    <th>Class</th>

    <th>Duration</th>

    <th>Date</th>
	
    <th>Cheque<br>/Cash</th>

    <th>Bank</th>

    <th>Branch</th>
    ';
//    echo '<th>Transport</th>
//    <th>Discount</th>';
    echo '

    <th style="color:green">Total  Fee</th>

    <th style="color:green">Paid</th>

    <th style="color:red">Due</th>


    <th>Modify</th>

    </tr>

    </thead>

    <tbody align="left">

     ';
    while($fetch_receipt=mysql_fetch_array($exe_receipt))
    {
        //STUDENT DETAILSS
        $student_details="SELECT * FROM student_user WHERE sId=".$fetch_receipt['sid']."";
        $exe_student=mysql_query($student_details);
        $fetch_student=mysql_fetch_array($exe_student);
        //CLASS
        $class_name="SELECT class_name FROM class_index
            INNER JOIN class
            ON class.classId =class_index.cId
            WHERE class.sId = ".$fetch_receipt['sid']."
            AND class.session_id= ".$session_id."";
        $exe_class=mysql_query($class_name);
        $fetch_class=mysql_fetch_array($exe_class);
        
        echo '<tr>';
        echo '<td>'.$fetch_receipt[1].'</td>';
        echo '<td>'.$fetch_student['admission_no'].'</td>';
        echo '<td><a href="accounts_fee_receipt_print_new_vidya_2.php?fee_receipt1='.$fetch_receipt['receipt_id'].'"> '.$fetch_student['Name'].'</a></td>';
        echo '<td>'.$fetch_student[5].'</td>';
        echo '<td>'.$fetch_class['class_name'].'</td>';
        echo '<td>'.$fetch_receipt['duration_name'].'</td>';
        
        //date
        $date="SELECT date FROM dates_d WHERE date_id=".$fetch_receipt['date']."";
        $exe_date=mysql_query($date);
        $fetch_date=mysql_fetch_array($exe_date);
	$dd =date('d-M-Y', strtotime($fetch_date[0]));
        echo '<td>'.$dd.'</td>';
      if($fetch_receipt['cheque_no'] == 0)
        {
          echo '<td>Cash</td>';
        }
        else 
        {
            echo '<td>'.$fetch_receipt['cheque_no'].'</td>';
        }
        echo '<td>'.$fetch_receipt['bank'].'</td>';
        echo '<td>'.$fetch_receipt['branch'].'</td>';
//        echo '<td>0</td>';
//        echo '<td>'.$fetch_receipt['discount'].'</td>';
      
        //
        $total = $fetch_receipt['amounts'] + $fetch_receipt['due'] ;
        
        echo '<td style="color:green">'.$total.'</td>';
        echo '<td style="color:green">'.$fetch_receipt['amounts'].'</td>';
        echo '<td style="color:red">'.$fetch_receipt['due'].'</td>';
        
        
           echo '<td style="color:red">';
//           echo '  <span class="tip"><a href="#" original-title="Edit">
//			<img src="images/icon/icon_edit.png"></a></span> ';
		echo '	<span class="tip"><a href="#" onclick="delete_receipt('.$fetch_receipt[1].');" original-title="Delete">
			<img src="images/icon/icon_delete.png"></a></span> ';
        echo '</td>';
        echo '</tr>';
    }
    echo'
    </tbody>
    </table>
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';   
}
//transport_receipt
function accounts_fee_today_collection()
{
    $session_id=$_SESSION['current_session_id'];
 $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
          $april_june=date('m');
	$format=date('Y-m-d',strtotime($date));
      
       $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
        $paid_on_date_id=$fetch_pre_date11[0];
    //$fee_receipt="SELECT * FROM fee_receipt_details_transport WHERE date=".$paid_on_date_id."";
	$fee_receipt="SELECT * FROM fee_receipt_details_transport ";
    $exe_receipt=mysql_query($fee_receipt);
if(isset($_GET['delete_receipt']))
    {
        echo '<h4 align="center" style="color:red">DELETE FEE RECEIPT SUCCESSFULY!</h4>'; 
    }
     if(isset($_GET['delete_receipt_no']))
    {
        echo '<h4 align="center" style="color:red">CAN`T DELETE IN MIDDLE!</h4>'; 
    }
    echo'

    <div id="replace_collection">
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">

    <div class="widget-header">
    <span>
    Fee Receipt Details Of Transport

    </span>
    </div><!-- End widget-header -->	
    <div class="widget-content">

    <table  class="table table-bordered table-striped" id="dataTable"  >
    <thead align="left">
    <tr style="color:green">
    <th>Receipt<br> No.</th>

    <th>Admission<br> No.</th>

    <th>Student<br> Name</th>

    <th>Father`s<br> Name</th>

    <th>Class</th>

    <th>Duration</th>

    <th>Date</th>
	
    <th>Cheque<br>/Cash</th>

    
    ';
//    echo '<th>Transport</th>
//    <th>Discount</th>';
    echo '

    

    <th style="color:green">Transport Fee</th>

    


    <th>Modify</th>

    </tr>

    </thead>

    <tbody align="left">

     ';
    while($fetch_receipt=mysql_fetch_array($exe_receipt))
    {
        //STUDENT DETAILSS
        $student_details="SELECT * FROM student_user WHERE sId=".$fetch_receipt['sid']."";
        $exe_student=mysql_query($student_details);
        $fetch_student=mysql_fetch_array($exe_student);
        //CLASS
        $class_name="SELECT class_name FROM class_index
            INNER JOIN class
            ON class.classId =class_index.cId
            WHERE class.sId = ".$fetch_receipt['sid']."
            AND class.session_id= ".$session_id."";
        $exe_class=mysql_query($class_name);
        $fetch_class=mysql_fetch_array($exe_class);
        
        echo '<tr>';
        echo '<td>'.$fetch_receipt[1].'</td>';
        echo '<td>'.$fetch_student['admission_no'].'</td>';
        echo '<td><a href="accounts_fee_receipt_print_new_vidya_2.php?fee_receipt2='.$fetch_receipt['receipt_id'].'"> '.$fetch_student['Name'].'</a></td>';
        echo '<td>'.$fetch_student[5].'</td>';
        echo '<td>'.$fetch_class['class_name'].'</td>';
        echo '<td>'.$fetch_receipt['duration_name'].'</td>';
        
        //date
        $date="SELECT date FROM dates_d WHERE date_id=".$fetch_receipt['date']."";
        $exe_date=mysql_query($date);
        $fetch_date=mysql_fetch_array($exe_date);
	$dd =date('d-M-Y', strtotime($fetch_date[0]));
        echo '<td>'.$dd.'</td>';
      if($fetch_receipt['cheque_no'] == 0)
        {
          echo '<td>Cash</td>';
        }
        else 
        {
		echo '<td>Cash</td>';
            //echo '<td>'.$fetch_receipt['cheque_no'].'</td>';
        }
       // echo '<td>'.$fetch_receipt['bank'].'</td>';
        //echo '<td>'.$fetch_receipt['branch'].'</td>';
//        echo '<td>0</td>';
//        echo '<td>'.$fetch_receipt['discount'].'</td>';
      
        //
        $total = $fetch_receipt['amounts'] + $fetch_receipt['due'] ;
        
        //echo '<td style="color:green">'.$total.'</td>';
        echo '<td style="color:green">'.$fetch_receipt['amounts'].'</td>';
       // echo '<td style="color:red">'.$fetch_receipt['due'].'</td>';
        
        
           echo '<td style="color:red">';
//           echo '  <span class="tip"><a href="#" original-title="Edit">
//			<img src="images/icon/icon_edit.png"></a></span> ';
		echo '	<span class="tip"><a href="#" onclick="delete_receipt_tr('.$fetch_receipt[1].');" original-title="Delete">
			<img src="images/icon/icon_delete.png"></a></span> ';
        echo '</td>';
        echo '</tr>';
    }
    echo'
    </tbody>
    </table>
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';   
}

//transport_receipt
function accounts_fee_today_collection1()
{
    $session_id=$_SESSION['current_session_id'];
 $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');
          $april_june=date('m');
	$format=date('Y-m-d',strtotime($date));
      
       $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
        $paid_on_date_id=$fetch_pre_date11[0];
    //$fee_receipt="SELECT * FROM fee_receipt_details_transport WHERE date=".$paid_on_date_id."";
	$fee_receipt="SELECT * FROM fee_receipt_details_transport WHERE date=".$paid_on_date_id." ";
    $exe_receipt=mysql_query($fee_receipt);
if(isset($_GET['delete_receipt']))
    {
        echo '<h4 align="center" style="color:red">DELETE FEE RECEIPT SUCCESSFULY!</h4>'; 
    }
     if(isset($_GET['delete_receipt_no']))
    {
        echo '<h4 align="center" style="color:red">CAN`T DELETE IN MIDDLE!</h4>'; 
    }
    echo'

    <div id="replace_collection">
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">

    <div class="widget-header">
    <span>
    Fee Receipt Details Of Transport

    </span>
    </div><!-- End widget-header -->	
    <div class="widget-content">

    <table  class="table table-bordered table-striped" id="dataTable"  >
    <thead align="left">
    <tr style="color:green">
    <th>Receipt<br> No.</th>

    <th>Admission<br> No.</th>

    <th>Student<br> Name</th>

    <th>Father`s<br> Name</th>

    <th>Class</th>

    <th>Duration</th>

    <th>Date</th>
	
    <th>Cheque<br>/Cash</th>

    
    ';
//    echo '<th>Transport</th>
//    <th>Discount</th>';
    echo '

    

    <th style="color:green">Transport Fee</th>

    


    <th>Modify</th>

    </tr>

    </thead>

    <tbody align="left">

     ';
    while($fetch_receipt=mysql_fetch_array($exe_receipt))
    {
        //STUDENT DETAILSS
        $student_details="SELECT * FROM student_user WHERE sId=".$fetch_receipt['sid']."";
        $exe_student=mysql_query($student_details);
        $fetch_student=mysql_fetch_array($exe_student);
        //CLASS
        $class_name="SELECT class_name FROM class_index
            INNER JOIN class
            ON class.classId =class_index.cId
            WHERE class.sId = ".$fetch_receipt['sid']."
            AND class.session_id= ".$session_id."";
        $exe_class=mysql_query($class_name);
        $fetch_class=mysql_fetch_array($exe_class);
        
        echo '<tr>';
        echo '<td>'.$fetch_receipt[1].'</td>';
        echo '<td>'.$fetch_student['admission_no'].'</td>';
        echo '<td><a href="accounts_fee_receipt_print_new_vidya_2.php?fee_receipt2='.$fetch_receipt['receipt_id'].'"> '.$fetch_student['Name'].'</a></td>';
        echo '<td>'.$fetch_student[5].'</td>';
        echo '<td>'.$fetch_class['class_name'].'</td>';
        echo '<td>'.$fetch_receipt['duration_name'].'</td>';
        
        //date
        $date="SELECT date FROM dates_d WHERE date_id=".$fetch_receipt['date']."";
        $exe_date=mysql_query($date);
        $fetch_date=mysql_fetch_array($exe_date);
	$dd =date('d-M-Y', strtotime($fetch_date[0]));
        echo '<td>'.$dd.'</td>';
      if($fetch_receipt['cheque_no'] == 0)
        {
          echo '<td>Cash</td>';
        }
        else 
        {
		echo '<td>Cash</td>';
            //echo '<td>'.$fetch_receipt['cheque_no'].'</td>';
        }
       // echo '<td>'.$fetch_receipt['bank'].'</td>';
        //echo '<td>'.$fetch_receipt['branch'].'</td>';
//        echo '<td>0</td>';
//        echo '<td>'.$fetch_receipt['discount'].'</td>';
      
        //
        $total = $fetch_receipt['amounts'] + $fetch_receipt['due'] ;
        
        //echo '<td style="color:green">'.$total.'</td>';
        echo '<td style="color:green">'.$fetch_receipt['amounts'].'</td>';
       // echo '<td style="color:red">'.$fetch_receipt['due'].'</td>';
        
        
           echo '<td style="color:red">';
//           echo '  <span class="tip"><a href="#" original-title="Edit">
//			<img src="images/icon/icon_edit.png"></a></span> ';
		echo '	<span class="tip"><a href="#" onclick="delete_receipt_tr('.$fetch_receipt[1].');" original-title="Delete">
			<img src="images/icon/icon_delete.png"></a></span> ';
        echo '</td>';
        echo '</tr>';
    }
    echo'
    </tbody>
    </table>
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';   
}


//PRINT RECEIPT17307
function accounts_fee_receipt_print_new_vidya_2()
{
    //Author : Anil Kumar*
    $discount=0;
//	$quter=$_GET['quter'];
       $receipt1=$_GET['fee_receipt1'];
       $receipt2=$_GET['fee_receipt2'];
	//$receipt3=$_GET['fee_receipt3'];
       $session_id=$_SESSION['current_session_id'];
      $current_session=$session_id;




//////////////////////////////////////////////////////////    transport /////////////////
// anil_transport

//////////////////////////////////////////////////////// tr(()////////////////////////////////////
 if($receipt2>0)
 {



  echo' 
	<div class="row-fluid">';
        
      // echo ' <p align="center" style="margin-top:150Px" id="watermark">Vidya Bal Bhawan</p>';
        

       echo ' <!-- Table widget -->

        
         
	<div class="widget  span12 clearfix">
        <table  width="100%">

        <tr><td align="center" valign="bottom" style="border:0px;solid black; color:BLACK; font-size:12px; ">
	<b>';
	//echo $travels;
	echo '</b>

     
	
	</div><!-- End widget-header -->	
	<div class="widget-content" id="d_'.$student_id.'">

	';

 // GET FEE RECEIPT DETAILS
        // GET FEE RECEIPT DETAILS
        $fee_receipt_get="SELECT * FROM fee_receipt_details_transport WHERE receipt_id=".$receipt2."";
        $exe_receipt=mysql_query($fee_receipt_get);
        $fetch_fee_receipt=mysql_fetch_array($exe_receipt);
       
       $quter=$fetch_fee_receipt['quter'];
       
       $fine=$fetch_fee_receipt['fine'];
//        $class=$_GET['class'];
//        $rm_fee=$_GET['rm_fee'];
        $discount=$fetch_fee_receipt['discount']; 
//        $transport_quter=$_POST['transport_cost'];
         
       $receipt_no=$fetch_fee_receipt[1];
        $ref_no=$fetch_fee_receipt['ref_no'];
        $cheque=$fetch_fee_receipt['cheque_no'];
        $paid=$fetch_fee_receipt['amount'];//with transporet
        
        $due = $fetch_fee_receipt['due'];
        $year_month_fee_charge = $due + $paid;//with transporet
         $durations_name=$fetch_fee_receipt['duration_name'];
        
         $due_balance=$fetch_fee_receipt['pre_remaning_fee'];
          $du_value=$fetch_fee_receipt[9];
                

          $admission_fee=$fetch_fee_receipt['admission_fee'];
    
        
        $dates="SELECT date FROM dates_d WHERE date_id=".$fetch_fee_receipt['date']."";
        $exe_dates=mysql_query($dates);
        $fetch_dates=mysql_fetch_array($exe_dates);
        $date=$fetch_dates[0];
        $ddd=explode("-",$date);
        $y=$ddd[0];
        $m=$ddd[1];
        $d=$ddd[2];
        $dates_d=$d.'-'.$m.'-'.$y;
      
	//query get student id from student_user
	$sid_query="SELECT *
	            FROM student_user
				WHERE sId=".$fetch_fee_receipt['sid'];
	$exe_sid=mysql_query($sid_query);
	$fetch_sid=mysql_fetch_array($exe_sid);
	$admission_no=$fetch_sid['admission_no'];
        $name=$fetch_sid['Name'];
        $father_name=$fetch_sid[5];
	//query get session id

	//Query to get the class of the user student
	$query_class_student = "SELECT DISTINCT class_index.class_name, class_index.class
							FROM class_index
			
							INNER JOIN class
							ON class.classId = class_index.cId
			
							WHERE class.sId = '".$fetch_fee_receipt['sid']."'
							";
	$execute_class_student = mysql_query($query_class_student);
	$class_student = mysql_fetch_array($execute_class_student);
        $class_section=$class_student['class_name'];
        $class=$class_student['class'];
	//write query to get the session id from  the fee generated session
       
	$fetch_session_name="SELECT `session_name`
						 FROM `session_table` 
						 WHERE `sId`=".$session_id."";
	$execute_session_name=mysql_query($fetch_session_name);
	$fetch_session=mysql_fetch_array($execute_session_name);
	$session_name=$fetch_session['session_name'];




        $usid="SELECT * FROM routes_data
                INNER JOIN user_vehicle_details
                ON user_vehicle_details.route_id=routes_data.route_id
                WHERE user_vehicle_details.uId=".$fetch_fee_receipt['sid']."";
       $exe_usid=mysql_query($usid);
       $fetch_usid=mysql_fetch_array($exe_usid);
$travels=$fetch_usid[1];
	

//        echo '<p align="center" style="color:BLACK; font-size:14px; position:absolute; left:200px; top:0px ">';
//	echo $fetch_school_name['name_school'];
//	echo ' </p>';
            
	
	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:0px;border-collapse:collapse;">';
            

        echo ' <tr style="border:0px;solid black">
        <td  ALIGN="right" style="border:0px;solid black; font-size:14px"> <b>'.$travels.'</b></td>
<td  ALIGN="right" width="32%" style="border:0px;solid black; font-size:10px"> <b>(Transport Copy )</b></td>

</tr>';

    
        echo ' <tr style="border:0px;solid black">
        <td width="68%" colspan="2" ALIGN="center" style="border:0px;solid black; font-size:14px"> <b>TRANSPORT RECEIPT</b></td>


</tr>';
    
        echo ' <tr style="border:0px;solid black">
        <td colspan="2" ALIGN="CENTER" style="border:0px;solid black; font-size:14px"> <b>Session 2016-17</b><br/><br/></td></tr>';
            
   
echo '<tr style="border:0px;solid black">
        <td  style="border:0px;solid black; font-size:12px"> <b>Receipt.No.: '.$receipt2.' </b></td>';
//            echo '  <td  style="border:0px;solid black; font-size:10px"><b>(Fee Receipt) </b></td>';
       
       echo '  <td align="right" style="border:0px;solid black; font-size:12px" > <b>Date</b> : '.$dates_d.'</td>
         </tr>
         <tr style="border:0px;solid black">';
    //   echo '  <td style="border:0px;solid black; font-size:10px"> <b>Admission No.: </b> '.$admission_no.'</td>';

           echo '   <td  align="right" style="border:0px;solid black; font-size:12px"> <b>Class & Sec. : </b> '.$class_section.'</td>


</tr>
          <tr style="border:0px;solid black">
          <td colspan="2" style="border:0px;solid black; font-size:12px"> <b>Passenger Name :</b> '.$name.'</td>
        
     </tr>
         <tr style="border:0px;solid black">

         
        <td colspan="2" align="left" style="border:0px;solid black; font-size:12px"> <b>Father`s Name : </b> '.$father_name.'</td>
     </tr>
             
       
	
             <tr align="center" style="border:0px;solid black; font-size:12px">';
 
        echo '<td colspan="2" style="border:0px;solid black; font-size:12px"><b>Fee of the month ('.$durations_name.')</td></tr>
			 ';
        
     
	
       
               echo '
        </table>';

	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:1px solid black;border-collapse:collapse; font-size:10px">';
	echo '<thead>
	<tr>
	
	<th style="font-size:12px" >PARTICULARS</th>
	<th style="font-size:12px" align="right">AMOUNT</th>

	</tr><tbody >';
        

  
       
             echo' <tr style="border:0px;solid black; font-size:12px">';
             echo' <td align="left" style="border:0px;solid black; font-size:12px" >Transport Charge</td>';
          
                 
               echo' <td align="right" style="border:0px;solid black; font-size:12px" >'.$fetch_fee_receipt['amounts'].'</td></tr>';
  

      
        echo '<tr>
            <td align="left" style="font-size:12px" ><b>Total : ( '.$durations_name.' ) </b></td>';

      
                echo '  <td align="right" style="font-size:12px" ><b>'.$fetch_fee_receipt['amounts'].'</b></tr>';

         
          echo '  </tr>';

               $value = $fetch_fee_receipt['amounts'];
             
                echo '<tr>
            <td  width="50%" align="left"  style="font-size:12px" ><b>Paid </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:12px" ><b>'.$fetch_fee_receipt['amounts'].' </b></td>';
                        echo '  </tr>';
                         echo '<tr> 
            <td  width="50%" align="left"  style="font-size:12px" ><b>Due </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:12px" ><b>0</b></td>';
                        echo '  </tr>';
           
       
               $value = $fetch_fee_receipt['amounts'];
       
               
//                   echo '<tr>
//            <td colspan="2" width="50%" align="center" style="font-size:10px" ><b>PAID: ( '.$paid.' ) </b></td>';
//                        echo '
//            <td  width="50%" align="center" style="font-size:10px" ><b>DUE: ( '.$due.' ) </b></td>';
//                        echo '  </tr>';
           
         //check cash details
            
          
            
                    echo '<tr style="border:0px;solid black; font-size:10px">
            <td colspan="2"   align="left" style="border:0px;solid black; font-size:12px" ><b>Paid by &nbsp;: &nbsp;Cash</b></td> </tr>';
               // echo '<td  align="left" style="border:0px;solid black; font-size:10px">Cash</td>';
          
            echo '    ';
            
              echo '<tr style="border:0px;solid black; font-size:10px">
              <td colspan="2"  align="left" style="border:0px;solid black; font-size:12px" ><b style="text-transform:uppercase;">';
           
                    echo ' In Words - '.convertNumber($value).' only';
              
            echo ' </b></td></tr>';
          
                 echo '<tr style="border:0px;solid black; font-size:10px">
            <td  colspan="2"  align="left" style="border:0px;solid black; font-size:12px" ><b>Date : &nbsp;&nbsp;'.$dates_d.'</b></td>';

                //echo '<td  align="left" style="border:0px;solid black; font-size:10px">'.$date.'</td>';

            echo '  </tr>';
            
            
            //bank 
           

              
        echo '</tbody>
	</thead>';
	
	
	
          echo '</table>';
	echo '
            <table width="100%" style="border:0px;solid black; font-size:10px">

            <tr>
	<td style="border:0px;solid black; font-size:12px; padding-top:40px">Cashier Sign. </td>
<td align="right" style="border:0px;solid black; font-size:12px; padding-top:40px">Depositor Sign </td>
</tr>
        </table>
        </td></tr></table>
	</div><!--  end widget-content -->';
       echo '<br><br><br>'; 
    echo' <br><br><br>
	<div class="row-fluid">';
       
        
      // echo ' <p align="center" style="margin-top:150Px" id="watermark">Vidya Bal Bhawan</p>';
        
/*
       echo ' <!-- Table widget -->

        
         
	<div class="widget  span12 clearfix">
        <table  width="100%">

        <tr><td align="center" valign="bottom" style="border:0px;solid black; color:BLACK; font-size:12px; ">
	<b>';
	echo $travels;
	echo '</b>

     
	
	</div><!-- End widget-header -->	
	<div class="widget-content" id="d_'.$student_id.'">

	';

//        echo '<p align="center" style="color:BLACK; font-size:14px; position:absolute; left:200px; top:0px ">';
//	echo $fetch_school_name['name_school'];
//	echo ' </p>';
            
	
	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:0px;border-collapse:collapse;">';
            


    
        echo ' <tr style="border:0px;solid black">
        <td width="68%" ALIGN="right" style="border:0px;solid black; font-size:10px"> <b>TRANSPORT RECEIPT</b></td>

<td  ALIGN="right" width="32%" style="border:0px;solid black; font-size:10px"> <b>( Parent`s Copy )</b></td>
</tr>';
    
        echo ' <tr style="border:0px;solid black">
        <td colspan="2" ALIGN="CENTER" style="border:0px;solid black; font-size:10px"> <b>Session 2015-16</b><br/><br/></td></tr>';
            
   
echo '<tr style="border:0px;solid black">
        <td  style="border:0px;solid black; font-size:10px"> <b>Receipt.No.: '.$receipt2.' </b></td>';
//            echo '  <td  style="border:0px;solid black; font-size:10px"><b>(Fee Receipt) </b></td>';
   
       echo '  <td align="right" style="border:0px;solid black; font-size:10px" > <b>Date</b> : '.$dates_d.'</td>
         </tr>
         <tr style="border:0px;solid black">';
       // echo ' <td style="border:0px;solid black; font-size:10px"> <b>Admission No.: </b> '.$admission_no.'</td>';

          echo '    <td  align="right" style="border:0px;solid black; font-size:10px"> <b>Class & Sec. : </b> '.$class_section.'</td>


</tr>

          <tr style="border:0px;solid black">
          <td colspan="2" style="border:0px;solid black; font-size:10px"> <b>Passenger Name :</b> '.$name.'</td>
        
     </tr>

         <tr style="border:0px;solid black">

         
        <td colspan="2" align="left" style="border:0px;solid black; font-size:10px"> <b>Father`s Name : </b> '.$father_name.'</td>

     </tr>
             
       
	

             <tr align="center" style="border:0px;solid black; font-size:10px">';
 
        echo '<td colspan="2" style="border:0px;solid black; font-size:10px"><b>Fee of the month ('.$durations_name.')</td></tr>
			 ';
        
     
	
       
               echo '
        </table>';

	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:1px solid black;border-collapse:collapse; font-size:10px">';
	echo '<thead>
	<tr>
	
	<th style="font-size:10px" >PARTICULARS</th>
	<th style="font-size:10px" align="right">AMOUNT</th>


	</tr><tbody >';
        

             echo' <tr style="border:0px;solid black; font-size:10px">';
             echo' <td align="left" style="border:0px;solid black; font-size:10px" >Transport Charge</td>';
          
          
               echo' <td align="right" style="border:0px;solid black; font-size:10px" >'.$fetch_fee_receipt['amounts'].'</td></tr>';

        echo '<tr>

            <td align="left" style="font-size:10px" ><b>Total : ( '.$durations_name.' ) </b></td>';


      
                echo '  <td align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['amounts'].'</b></tr>';

         
          echo '  </tr>';

            
               $value = $fetch_fee_receipt['amounts'];
             
                echo '<tr>
            <td  width="50%" align="left"  style="font-size:10px" ><b>Paid </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['amounts'].' </b></td>';
                        echo '  </tr>';
                         echo '<tr> 

            <td  width="50%" align="left"  style="font-size:10px" ><b>Due </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:10px" ><b>0</b></td>';
                        echo '  </tr>';
           
       
               $value = $fetch_fee_receipt['amounts'];
       
              
            
                    echo '<tr style="border:0px;solid black; font-size:10px">
            <td colspan="2"   align="left" style="border:0px;solid black; font-size:10px" ><b>Paid by &nbsp;: &nbsp;Cash</b></td> </tr>';
               // echo '<td  align="left" style="border:0px;solid black; font-size:10px">Cash</td>';
          
            echo '    ';
            
              echo '<tr style="border:0px;solid black; font-size:10px">

              <td colspan="2"  align="left" style="border:0px;solid black; font-size:10px" ><b style="text-transform:uppercase;">';
           
                    echo ' In Words - '.convertNumber($value).' only';
              
            echo ' </b></td></tr>';
          
                 echo '<tr style="border:0px;solid black; font-size:10px">
            <td  colspan="2"  align="left" style="border:0px;solid black; font-size:10px" ><b>Date : &nbsp;&nbsp;'.$dates_d.'</b></td>';

                //echo '<td  align="left" style="border:0px;solid black; font-size:10px">'.$date.'</td>';

            echo '  </tr>';
            
            
            //bank 
            

              
        echo '</tbody>
	</thead>';
	

          echo '</table>';
	echo '
            <table width="100%" style="border:0px;solid black; font-size:10px">

            <tr>
	<td style="border:0px;solid black; font-size:10px; padding-top:40px">Cashier Sign. </td>
<td align="right" style="border:0px;solid black; font-size:10px; padding-top:40px">Depositor Sign </td>
</tr>
        </table>
        </td></tr></table>
	</div><!--  end widget-content -->';*/
      
    echo' <br>'; 

}  //receipt2>0










//anil_accounts
       

if($receipt1 > 0)
{
       // GET FEE RECEIPT DETAILS
        $fee_receipt_get="SELECT * FROM fee_receipt_details WHERE receipt_id=".$receipt1."";
        $exe_receipt=mysql_query($fee_receipt_get);
        $fetch_fee_receipt=mysql_fetch_array($exe_receipt);

	$student_id = $fetch_fee_receipt['sid'];
   ////////  GET  SID  EWS  OR  NOT
		$ews=0;
         // GET EWS OR NOT

		 $ews_sid = "SELECT * FROM ews_students";
		$exe_ews = mysql_query($ews_sid);
		while($fetch_ews = mysql_fetch_array($exe_ews))
		{
			if($fetch_ews[1] == $student_id)
			{
				 $ews = 1;
				break;
			}
		}


	$fm=$fetch_fee_receipt[8];
       $lm=$fetch_fee_receipt[10];
        $quter=$fetch_fee_receipt['quter'];
       
       $fine=$fetch_fee_receipt['fine'];
//        $class=$_GET['class'];
//        $rm_fee=$_GET['rm_fee'];
        $discount=$fetch_fee_receipt['discount']; 
//        $transport_quter=$_POST['transport_cost'];
         
       $receipt_no=$fetch_fee_receipt[1];
        $ref_no=$fetch_fee_receipt['ref_no'];
        $cheque=$fetch_fee_receipt['cheque_no'];
        $utrn=$fetch_fee_receipt['utrn_no'];
        $mode=$fetch_fee_receipt['mode'];
        $paid=$fetch_fee_receipt['amount'];//with transporet
        
        $due = $fetch_fee_receipt['due'];
        $year_month_fee_charge = $due + $paid;//with transporet
         $durations_name=$fetch_fee_receipt['duration_name'];
        
         $due_balance=$fetch_fee_receipt['pre_remaning_fee'];
          $du_value=$fetch_fee_receipt[9];
                

          $admission_fee=$fetch_fee_receipt['admission_fee'];
    
        
        $dates="SELECT date FROM dates_d WHERE date_id=".$fetch_fee_receipt['date']."";
        $exe_dates=mysql_query($dates);
        $fetch_dates=mysql_fetch_array($exe_dates);
        $date=$fetch_dates[0];
        $ddd=explode("-",$date);
        $y=$ddd[0];
        $m=$ddd[1];
        $d=$ddd[2];
        $dates_d=$d.'-'.$m.'-'.$y;
      
	//query get student id from student_user
	$sid_query="SELECT *
	            FROM student_user
				WHERE sId=".$fetch_fee_receipt['sid'];
	$exe_sid=mysql_query($sid_query);
	$fetch_sid=mysql_fetch_array($exe_sid);
	$admission_no=$fetch_sid['admission_no'];
        $name=$fetch_sid['Name'];
        $father_name=$fetch_sid[5];
	//query get session id

	//Query to get the class of the user student
	$query_class_student = "SELECT DISTINCT class_index.class_name, class_index.class
							FROM class_index
			
							INNER JOIN class
							ON class.classId = class_index.cId
			
							WHERE class.sId = '".$fetch_fee_receipt['sid']."'
							";
	$execute_class_student = mysql_query($query_class_student);
	$class_student = mysql_fetch_array($execute_class_student);
        $class_section=$class_student['class_name'];
        $class=$class_student['class'];
	//write query to get the session id from  the fee generated session
       
	$fetch_session_name="SELECT `session_name`
						 FROM `session_table` 
						 WHERE `sId`=".$session_id."";
	$execute_session_name=mysql_query($fetch_session_name);
	$fetch_session=mysql_fetch_array($execute_session_name);
	$session_name=$fetch_session['session_name'];
	
		//get name of the school
	$get_name_school=
					"SELECT *
					FROM `school_names`
					WHERE `Id`= 1";
	$exe_name_school=mysql_query($get_name_school);
	$fetch_school_name=mysql_fetch_array($exe_name_school);
	    echo'
	<div class="row-fluid">';
      
       // <p align="center" style="margin-top:100Px" id="watermark">Vidya Bal Bhawan</p>
        echo '	<!-- Table widget -->
	<div class="widget  span12 clearfix">
        <table  width="100%">
        <tr><td align="center" valign="bottom" style="border:0px;solid black; color:BLACK; font-size:12px; ">
	<b>';
	echo $fetch_school_name['name_school'];
	echo '</b>
     
	
	</div><!-- End widget-header -->	
	<div class="widget-content" id="d_'.$student_id.'">
	';

//        echo '<p align="center" style="color:BLACK; font-size:14px; position:absolute; left:200px; top:0px ">';
//	echo $fetch_school_name['name_school'];
//	echo ' </p>';
            
	
	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:0px;border-collapse:collapse;">';
            


        echo ' <tr style="border:0px;solid black">
        <td colspan="2" valign="top" ALIGN="CENTER" style="border:0px;solid black; font-size:6px"> <b> '.$fetch_school_name['2'].'</b></td></tr>';
        echo ' <tr style="border:0px;solid black">
        <td  ALIGN="right" width="68%" style="border:0px;solid black; font-size:8px"> <b> '.$fetch_school_name['3'].'</b></td>

  <td  ALIGN="right" width="32%" style="border:0px;solid black; font-size:10px"> <b></b></td>

</tr>';
         echo ' <tr style="border:0px;solid black">
        <td colspan="2" ALIGN="CENTER" style="border:0px;solid black; font-size:8px"> <b>'.$fetch_school_name['4'].' </b></td></tr>';
        echo ' <tr style="border:0px;solid black">
        <td colspan="2" ALIGN="CENTER" style="border:0px;solid black; font-size:10px"> <b>'.$fetch_school_name['5'].'</b><br/><br/></td></tr>';
            

echo '<tr style="border:0px;solid black">
        <td  style="border:0px;solid black; font-size:10px"> <b>Receipt.No.: '.$receipt_no.'</b></td>';
//            echo '  <td  style="border:0px;solid black; font-size:10px"><b>(Fee Receipt) </b></td>';
        
       echo '  <td align="right" style="border:0px;solid black; font-size:10px" > <b>Date</b> : '.$dates_d.'</td>
         </tr>
         <tr style="border:0px;solid black">
         <td style="border:0px;solid black; font-size:10px"> <b>Admission No.: </b> '.$admission_no.'</td>
              <td  align="right" style="border:0px;solid black; font-size:10px"> <b>Class & Sec. : </b> '.$class_section.'</td>


</tr>
          <tr style="border:0px;solid black">
          <td colspan="2" style="border:0px;solid black; font-size:10px"> <b>Name :</b> '.$name.'</td>
        
     </tr>
         <tr style="border:0px;solid black">
         
        <td colspan="2" align="left" style="border:0px;solid black; font-size:10px"> <b>Father`s Name : </b> '.$father_name.'</td>
     </tr>
     
             <tr align="center" style="border:0px;solid black; font-size:10px">';
 
        echo '<td colspan="2" style="border:0px;solid black; font-size:10px"><b>Fee of the month ('.$durations_name.')</b></td></tr>
			 ';
       
         if($ews==1)
        {
             
           echo '  <tr align="center" style="border:0px;solid black; font-size:10px">';
 
        echo '<td colspan="2" style="border:0px;solid black; font-size:10px">EWS STUDENT</td></tr>
			 ';
        }
	
        
               echo '
        </table>';

	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:1px solid black;border-collapse:collapse; font-size:10px">';
	echo '<thead>
	<tr>
	
	<th style="font-size:10px" >PARTICULARS</th>
	<th style="font-size:10px" align="right">AMOUNT</th>
	</tr><tbody >';
    
     

	if(($due_balance >0 )||($due_balance >0 ))
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Previous Due Balance </b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$due_balance.'</b></tr>';
		echo '  </tr>';
	}
///  feee  charge
if( $fetch_fee_receipt['ad1']>0 )
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Admission Fee</b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['ad1'].'</b></tr>';
		echo '  </tr>';
	}

if(( $fetch_fee_receipt['annual_1']>0 ) || (($ews == 1)&&($fm == 1)) )
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Annual Charge</b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['annual_1'].'</b></tr>';
		echo '  </tr>';
	}
if( $fetch_fee_receipt['annual_2']>0 )
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Exam Fee</b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['annual_2'].'</b></tr>';
		echo '  </tr>';
	}
if($fetch_fee_receipt['half_year']>0 )
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Exam Fee</b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['half_year'].'</b></tr>';
		echo '  </tr>';
	}
if(($fetch_fee_receipt['month_1']>0)||($ews == 1))
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Tution Fee</b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['month_1'].'</b></tr>';
		echo '  </tr>';
	}
if(($fetch_fee_receipt['month_2']>0 )||($ews == 1))
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Devlopment Charge</b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['month_2'].'</b></tr>';
		echo '  </tr>';
	}



	if($discount >0 )
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Discount Fee</b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$discount.'</b></tr>';
		echo '  </tr>';
	}
	if($fine >0 )
	{
		echo '<tr>
		<td align="left" style="font-size:10px" >Other Charges</td>';
		echo '  <td align="right" style="font-size:10px" >'.$fine.'</tr>';
		echo '  </tr>';
	}

        
//        $trans_quter=$transport_cost*3;
$total = $fetch_fee_receipt['amounts'] + $fetch_fee_receipt['due'];

        echo '<tr>
            <td align="left" style="font-size:10px" ><b>Total : ( '.$durations_name.' ) </b></td>';
          if($ews==1)
           {
                echo '  <td align="right" style="font-size:10px" ><b>0</b></tr>';
           }
           else 
           {
                echo '  <td align="right" style="font-size:10px" ><b>'.$total.'</b></tr>';
           }
         
          echo '  </tr>';
          
          //paids
          
          if($ews==1)
           {
              $value=0;
           }
         
           else {
                 
               $value = $fetch_fee_receipt['amounts'];
                echo '<tr>
            <td  width="50%" align="left"  style="font-size:10px" ><b>Paid </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['amounts'].' </b></td>';
                        echo '  </tr>';
                         echo '<tr>
            <td  width="50%" align="left" style="font-size:10px" ><b>Due </b></td>';
                        echo '
            <td  width="50%" align="right"  style="font-size:10px" ><b>'.$fetch_fee_receipt['due'].'</b></td>';
                        echo '  </tr>';
           }
//                   echo '<tr>
//            <td colspan="2" width="50%" align="center" style="font-size:10px" ><b>PAID: ( '.$paid.' ) </b></td>';
//                        echo '
//            <td  width="50%" align="center" style="font-size:10px" ><b>DUE: ( '.$due.' ) </b></td>';
//                        echo '  </tr>';
           
         //check cash details
            
            if($cheque>0)
            {
                echo '<tr style="border:0px;solid black; font-size:10px">
            <td colspan="2"   align="left" style="border:0px;solid black; font-size:10px" ><b>Paid by &nbsp;: &nbsp;Cheque No.:'.$cheque.'</b></td></tr> ';
              //  echo '<td  align="left" style="border:0px;solid black; font-size:10px">Cheque No.:'.$cheque.'</td>';
            }

            elseif($utrn>0)
            {
                echo '<tr style="border:0px;solid black; font-size:10px">
            <td colspan="2"   align="left" style="border:0px;solid black; font-size:10px" ><b>Paid by &nbsp;: & &nbsp;UTRN No.:'.$utrn.'</b></td></tr> ';
              //  echo '<td  align="left" style="border:0px;solid black; font-size:10px">Cheque No.:'.$cheque.'</td>';
            }


            else 
            {
               if($ews==0)
              {
                    echo '<tr style="border:0px;solid black; font-size:10px">
            <td colspan="2"   align="left" style="border:0px;solid black; font-size:10px" ><b>Paid by &nbsp;: &nbsp;Cash</b></td></tr> ';
               // echo '<td  align="left" style="border:0px;solid black; font-size:10px">Cash</td>';
            }}
            
            echo '    ';
            
              echo '<tr style="border:0px;solid black; font-size:10px">
              <td colspan="2"  align="left" style="border:0px;solid black; font-size:10px" ><b style="text-transform:uppercase;">';
              if(($value=="")||($value==0))
              {
                  echo ' In Words - Zero only';
              }
              else 
              {
                    echo ' In Words - '.convertNumber($value).' only';
              }
            echo ' </b></td></tr>';
          
                 echo '<tr style="border:0px;solid black; font-size:10px">
            <td  colspan="2"  align="left" style="border:0px;solid black; font-size:10px" ><b>Date : &nbsp;&nbsp;'.$dates_d.'</b></td>';

                //echo '<td  align="left" style="border:0px;solid black; font-size:10px">'.$date.'</td>';

            echo '  </tr>';
            
            
            //bank 
             
          if($cheque>0)
            {
                $bank=$fetch_fee_receipt['bank'];
                $branch=$fetch_fee_receipt['branch'];
                  echo '<tr style="border:0px;solid black; font-size:10px">
            <td   align="left" style="border:0px;solid black; font-size:10px" >Bank : '.$bank.'</td>';
           
            echo '<td  align="left" style="border:0px;solid black; font-size:10px">Branch : '.$branch.'</td></tr>';
            }

       elseif($utrn>0)
            {
                $bank=$fetch_fee_receipt['bank'];
                $branch=$fetch_fee_receipt['branch'];
                  echo '<tr style="border:0px;solid black; font-size:10px">
            <td   align="left" style="border:0px;solid black; font-size:10px" >Bank : '.$bank.'</td>';
           
            echo '<td  align="left" style="border:0px;solid black; font-size:10px">Branch : '.$branch.'</td></tr>';
            }
           
              
        echo '</tbody>
	</thead>';
	
	
	
          echo '</table>';
	echo '
            <table width="100%" style="border:0px;solid black; font-size:10px">
            <tr>
	<td style="border:0px;solid black; font-size:10px; padding-top:50px">Cashier Sign. </td>
<td align="right" style="border:0px;solid black; font-size:10px; padding-top:50px"> Depositor Sign.</td>
</tr>
        </table>
        </td></tr></table>
	</div><!--  end widget-content -->';
//	echo '		
//	<div align="right" style="cursor:pointer"><img src="images/print.png" onclick="print_val('.$student_id.')"
//	style="width:40px" style="height:30px" /></div>';
	echo'</div><!-- widget  span12 clearfix-->
	
	</div><!-- row-fluid -->';		
////////////////////////////////////////////////////////////////////////////////////////////////
///*********************************************parent copy***********************************
//!!@######################################################################################33333
//   anil_parents
/*
echo '<br><br>';
 //Author : Anil Kumar*
      // GET FEE RECEIPT DETAILS
        $fee_receipt_get="SELECT * FROM fee_receipt_details WHERE receipt_id=".$receipt1."";
        $exe_receipt=mysql_query($fee_receipt_get);
        $fetch_fee_receipt=mysql_fetch_array($exe_receipt);

	$student_id = $fetch_fee_receipt['sid'];
   ////////  GET  SID  EWS  OR  NOT
		$ews=0;
         // GET EWS OR NOT
		$ews_sid = "SELECT * FROM ews_students";
		$exe_ews = mysql_query($ews_sid);
		while($fetch_ews = mysql_fetch_array($exe_ews))
		{
			if($fetch_ews[1] == $student_id)
			{
				$ews = 1;
				break;
			}
		}


	$fm=$fetch_fee_receipt[8];
       $lm=$fetch_fee_receipt[10];
        $quter=$fetch_fee_receipt['quter'];
       
       $fine=$fetch_fee_receipt['fine'];
//        $class=$_GET['class'];
//        $rm_fee=$_GET['rm_fee'];
        $discount=$fetch_fee_receipt['discount']; 
//        $transport_quter=$_POST['transport_cost'];
         
       $receipt_no=$fetch_fee_receipt[1];
        $ref_no=$fetch_fee_receipt['ref_no'];
        $cheque=$fetch_fee_receipt['cheque_no'];
        $paid=$fetch_fee_receipt['amount'];//with transporet
        
        $due = $fetch_fee_receipt['due'];
        $year_month_fee_charge = $due + $paid;//with transporet
         $durations_name=$fetch_fee_receipt['duration_name'];
        
         $due_balance=$fetch_fee_receipt['pre_remaning_fee'];
          $du_value=$fetch_fee_receipt[9];
                

          $admission_fee=$fetch_fee_receipt['admission_fee'];
    
        
        $dates="SELECT date FROM dates_d WHERE date_id=".$fetch_fee_receipt['date']."";
        $exe_dates=mysql_query($dates);
        $fetch_dates=mysql_fetch_array($exe_dates);
        $date=$fetch_dates[0];
        $ddd=explode("-",$date);
        $y=$ddd[0];
        $m=$ddd[1];
        $d=$ddd[2];
        $dates_d=$d.'-'.$m.'-'.$y;
      
	//query get student id from student_user
	$sid_query="SELECT *
	            FROM student_user
				WHERE sId=".$fetch_fee_receipt['sid'];
	$exe_sid=mysql_query($sid_query);
	$fetch_sid=mysql_fetch_array($exe_sid);
	$admission_no=$fetch_sid['admission_no'];
        $name=$fetch_sid['Name'];
        $father_name=$fetch_sid[5];
	//query get session id

	//Query to get the class of the user student
	$query_class_student = "SELECT DISTINCT class_index.class_name, class_index.class
							FROM class_index
			
							INNER JOIN class
							ON class.classId = class_index.cId
			
							WHERE class.sId = '".$fetch_fee_receipt['sid']."'
							";
	$execute_class_student = mysql_query($query_class_student);
	$class_student = mysql_fetch_array($execute_class_student);
        $class_section=$class_student['class_name'];
        $class=$class_student['class'];
	//write query to get the session id from  the fee generated session
       
	$fetch_session_name="SELECT `session_name`
						 FROM `session_table` 
						 WHERE `sId`=".$session_id."";
	$execute_session_name=mysql_query($fetch_session_name);
	$fetch_session=mysql_fetch_array($execute_session_name);
	$session_name=$fetch_session['session_name'];
	
		//get name of the school
	$get_name_school=
					"SELECT *
					FROM `school_names`
					WHERE `Id`= 1";
	$exe_name_school=mysql_query($get_name_school);
	$fetch_school_name=mysql_fetch_array($exe_name_school);
	    echo'
	<div class="row-fluid">';
      
       // <p align="center" style="margin-top:100Px" id="watermark">Vidya Bal Bhawan</p>
        echo '	<!-- Table widget -->
	<div class="widget  span12 clearfix">
        <table  width="100%">
        <tr><td align="center" valign="bottom" style="border:0px;solid black; color:BLACK; font-size:12px; ">
	<b>';
	echo $fetch_school_name['name_school'];
	echo '</b>
     
	
	</div><!-- End widget-header -->	
	<div class="widget-content" id="d_'.$student_id.'">
	';

//        echo '<p align="center" style="color:BLACK; font-size:14px; position:absolute; left:200px; top:0px ">';
//	echo $fetch_school_name['name_school'];
//	echo ' </p>';
            
	
	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:0px;border-collapse:collapse;">';
            


        echo ' <tr style="border:0px;solid black">
        <td colspan="2" valign="top" ALIGN="CENTER" style="border:0px;solid black; font-size:6px"> <b> '.$fetch_school_name['2'].'</b></td></tr>';
        echo ' <tr style="border:0px;solid black">
        <td  ALIGN="right" width="68%" style="border:0px;solid black; font-size:8px"> <b> '.$fetch_school_name['3'].'</b></td>

  <td  ALIGN="right" width="32%" style="border:0px;solid black; font-size:10px"> <b>( Parent`s Copy )</b></td>

</tr>';
         echo ' <tr style="border:0px;solid black">
        <td colspan="2" ALIGN="CENTER" style="border:0px;solid black; font-size:8px"> <b>'.$fetch_school_name['4'].' </b></td></tr>';
        echo ' <tr style="border:0px;solid black">
        <td colspan="2" ALIGN="CENTER" style="border:0px;solid black; font-size:10px"> <b>'.$fetch_school_name['5'].'</b><br/><br/></td></tr>';
            

echo '<tr style="border:0px;solid black">
        <td  style="border:0px;solid black; font-size:10px"> <b>Receipt.No.: '.$receipt_no.'</b></td>';
//            echo '  <td  style="border:0px;solid black; font-size:10px"><b>(Fee Receipt) </b></td>';
        
       echo '  <td align="right" style="border:0px;solid black; font-size:10px" > <b>Date</b> : '.$dates_d.'</td>
         </tr>
         <tr style="border:0px;solid black">
         <td style="border:0px;solid black; font-size:10px"> <b>Admission No.: </b> '.$admission_no.'</td>
              <td  align="right" style="border:0px;solid black; font-size:10px"> <b>Class & Sec. : </b> '.$class_section.'</td>


</tr>
          <tr style="border:0px;solid black">
          <td colspan="2" style="border:0px;solid black; font-size:10px"> <b>Name :</b> '.$name.'</td>
        
     </tr>
         <tr style="border:0px;solid black">
         
        <td colspan="2" align="left" style="border:0px;solid black; font-size:10px"> <b>Father`s Name : </b> '.$father_name.'</td>
     </tr>
     
             <tr align="center" style="border:0px;solid black; font-size:10px">';
 
        echo '<td colspan="2" style="border:0px;solid black; font-size:10px"><b>Fee of the month ('.$durations_name.')</b></td></tr>
			 ';
        
         if($ews==1)
        {
             
           echo '  <tr align="center" style="border:0px;solid black; font-size:10px">';
 
        echo '<td colspan="2" style="border:0px;solid black; font-size:10px">EWS STUDENT</td></tr>
			 ';
        }
	
        
               echo '
        </table>';

	echo '<table class="table table-bordered table-striped"  width="100%" align="center" style="border:1px solid black;border-collapse:collapse; font-size:10px">';
	echo '<thead>
	<tr>
	
	<th style="font-size:10px" >PARTICULARS</th>
	<th style="font-size:10px" align="right">AMOUNT</th>
	</tr><tbody >';
    
     

	if(($due_balance >0 )||($due_balance >0 ))
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Previous Due Balance </b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$due_balance.'</b></tr>';
		echo '  </tr>';
	}
///  feee  charge
if( $fetch_fee_receipt['ad1']>0 )
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Admission Fee</b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['ad1'].'</b></tr>';
		echo '  </tr>';
	}

if(( $fetch_fee_receipt['annual_1']>0 ) || (($ews == 1)&&($fm == 1)) )
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Annual Charge</b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['annual_1'].'</b></tr>';
		echo '  </tr>';
	}
if( $fetch_fee_receipt['annual_2']>0 )
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Exam Fee</b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['annual_2'].'</b></tr>';
		echo '  </tr>';
	}
if($fetch_fee_receipt['half_year']>0 )
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Exam Fee</b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['half_year'].'</b></tr>';
		echo '  </tr>';
	}
if(($fetch_fee_receipt['month_1']>0)||($ews == 1))
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Tution Fee</b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['month_1'].'</b></tr>';
		echo '  </tr>';
	}
if(($fetch_fee_receipt['month_2']>0 )||($ews == 1))
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Devlopment Charge</b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['month_2'].'</b></tr>';
		echo '  </tr>';
	}



	if($discount >0 )
	{
		echo '<tr>
		<td align="left" style="font-size:10px" ><b>Discount Fee</b></td>';
		echo '  <td align="right" style="font-size:10px" ><b>'.$discount.'</b></tr>';
		echo '  </tr>';
	}
	if($fine >0 )
	{
		echo '<tr>
		<td align="left" style="font-size:10px" >Other Charges</td>';
		echo '  <td align="right" style="font-size:10px" >'.$fine.'</tr>';
		echo '  </tr>';
	}

        
//        $trans_quter=$transport_cost*3;
$total = $fetch_fee_receipt['amounts'] + $fetch_fee_receipt['due'];

        echo '<tr>
            <td align="left" style="font-size:10px" ><b>Total : ( '.$durations_name.' ) </b></td>';
          if($ews==1)
           {
                echo '  <td align="right" style="font-size:10px" ><b>0</b></tr>';
           }
           else 
           {
                echo '  <td align="right" style="font-size:10px" ><b>'.$total.'</b></tr>';
           }
         
          echo '  </tr>';
          
          //paids
          
          if($ews==1)
           {
              $value=0;
           }
         
           else {
                 
               $value = $fetch_fee_receipt['amounts'];
                echo '<tr>
            <td  width="50%" align="left"  style="font-size:10px" ><b>Paid </b></td>';
                        echo '
            <td  width="50%" align="right" style="font-size:10px" ><b>'.$fetch_fee_receipt['amounts'].' </b></td>';
                        echo '  </tr>';
                         echo '<tr>
            <td  width="50%" align="left" style="font-size:10px" ><b>Due </b></td>';
                        echo '
            <td  width="50%" align="right"  style="font-size:10px" ><b>'.$fetch_fee_receipt['due'].'</b></td>';
                        echo '  </tr>';
           }
//                   echo '<tr>
//            <td colspan="2" width="50%" align="center" style="font-size:10px" ><b>PAID: ( '.$paid.' ) </b></td>';
//                        echo '
//            <td  width="50%" align="center" style="font-size:10px" ><b>DUE: ( '.$due.' ) </b></td>';
//                        echo '  </tr>';
           
         //check cash details
            
            if($cheque>0)
            {
                echo '<tr style="border:0px;solid black; font-size:10px">
            <td colspan="2"   align="left" style="border:0px;solid black; font-size:10px" ><b>Paid by &nbsp;: &nbsp;Cheque No.:'.$cheque.'</b></td></tr> ';
              //  echo '<td  align="left" style="border:0px;solid black; font-size:10px">Cheque No.:'.$cheque.'</td>';
            }
            else 
            {
               if($ews==0)
              {
                    echo '<tr style="border:0px;solid black; font-size:10px">
            <td colspan="2"   align="left" style="border:0px;solid black; font-size:10px" ><b>Paid by &nbsp;: &nbsp;Cash</b></td></tr> ';
               // echo '<td  align="left" style="border:0px;solid black; font-size:10px">Cash</td>';
            }}
            
            echo '    ';
            
              echo '<tr style="border:0px;solid black; font-size:10px">
              <td colspan="2"  align="left" style="border:0px;solid black; font-size:10px" ><b style="text-transform:uppercase;">';
              if(($value=="")||($value==0))
              {
                  echo ' In Words - Zero only';
              }
              else 
              {
                    echo ' In Words - '.convertNumber($value).' only';
              }
            echo ' </b></td></tr>';
          
                 echo '<tr style="border:0px;solid black; font-size:10px">
            <td  colspan="2"  align="left" style="border:0px;solid black; font-size:10px" ><b>Date : &nbsp;&nbsp;'.$dates_d.'</b></td>';

                //echo '<td  align="left" style="border:0px;solid black; font-size:10px">'.$date.'</td>';

            echo '  </tr>';
            
            
            //bank 
             
          if($cheque>0)
            {
                $bank=$fetch_fee_receipt['bank'];
                $branch=$fetch_fee_receipt['branch'];
                  echo '<tr style="border:0px;solid black; font-size:10px">
            <td   align="left" style="border:0px;solid black; font-size:10px" >Bank : '.$bank.'</td>';
           
            echo '<td  align="left" style="border:0px;solid black; font-size:10px">Branch : '.$branch.'</td></tr>';
            }
           
              
        echo '</tbody>
	</thead>';
	
	
	
          echo '</table>';
	echo '
            <table width="100%" style="border:0px;solid black; font-size:10px">
            <tr>
	<td style="border:0px;solid black; font-size:10px; padding-top:50px">Cashier Sign. </td>
<td align="right" style="border:0px;solid black; font-size:10px; padding-top:50px"> Depositor Sign.</td>
</tr>
        </table>
        </td></tr></table>
	</div><!--  end widget-content -->';
//	echo '		
//	<div align="right" style="cursor:pointer"><img src="images/print.png" onclick="print_val('.$student_id.')"
//	style="width:40px" style="height:30px" /></div>';
	echo'</div><!-- widget  span12 clearfix-->
	
	</div><!-- row-fluid -->';	
*/

}//close if  receipt > 0 anilst





	if($cheque>0)
	{
	  $message = "D/P, Thank you for paying Rs. $value as fee of your ward $name of class $class_section for the duration $durations_name. ";
	}
	else
	{
	  $message = "D/P, Thank you for paying Rs. $value as fee of your ward $name of class $class_section for the duration $durations_name. ";
	}
      echo '<input type="hidden" id="send_sms" name="'.$phone.'" value="'.$message.'"/>';
       //sendSMS($phone, $message);












}


//CONVERT NUMBER INTO WORDS

function convertNumber($number)
{
  //  list($integer, $fraction) = explode(".", (string) $number);
        $integer=$number;
        $fraction=0;
    $output = "";

    if ($integer{0} == "-")
    {
        $output = "negative ";
        $integer    = ltrim($integer, "-");
    }
    else if ($integer{0} == "+")
    {
        $output = "positive ";
        $integer    = ltrim($integer, "+");
    }

    if ($integer{0} == "0")
    {
        $output .= "zero";
    }
    else
    {
        $integer = str_pad($integer, 36, "0", STR_PAD_LEFT);
        $group   = rtrim(chunk_split($integer, 3, " "), " ");
        $groups  = explode(" ", $group);

        $groups2 = array();
        foreach ($groups as $g)
        {
            $groups2[] = convertThreeDigit($g{0}, $g{1}, $g{2});
        }

        for ($z = 0; $z < count($groups2); $z++)
        {
            if ($groups2[$z] != "")
            {
                $output .= $groups2[$z] . convertGroup(11 - $z) . (
                        $z < 11
                        && !array_search('', array_slice($groups2, $z + 1, -1))
                        && $groups2[11] != ''
                        && $groups[11]{0} == '0'
                            ? " and "
                            : ", "
                    );
            }
        }

        $output = rtrim($output, ", ");
    }

    if ($fraction > 0)
    {
        $output .= " point";
        for ($i = 0; $i < strlen($fraction); $i++)
        {
            $output .= " " . convertDigit($fraction{$i});
        }
    }

    return $output;
}

function convertGroup($index)
{
    switch ($index)
    {
        case 11:
            return " decillion";
        case 10:
            return " nonillion";
        case 9:
            return " octillion";
        case 8:
            return " septillion";
        case 7:
            return " sextillion";
        case 6:
            return " quintrillion";
        case 5:
            return " quadrillion";
        case 4:
            return " trillion";
        case 3:
            return " billion";
        case 2:
            return " million";
        case 1:
            return " thousand";
        case 0:
            return "";
    }
}

function convertThreeDigit($digit1, $digit2, $digit3)
{
    $buffer = "";

    if ($digit1 == "0" && $digit2 == "0" && $digit3 == "0")
    {
        return "";
    }

    if ($digit1 != "0")
    {
        $buffer .= convertDigit($digit1) . " hundred";
        if ($digit2 != "0" || $digit3 != "0")
        {
            $buffer .= " and ";
        }
    }

    if ($digit2 != "0")
    {
        $buffer .= convertTwoDigit($digit2, $digit3);
    }
    else if ($digit3 != "0")
    {
        $buffer .= convertDigit($digit3);
    }

    return $buffer;
}

function convertTwoDigit($digit1, $digit2)
{
    if ($digit2 == "0")
    {
        switch ($digit1)
        {
            case "1":
                return "ten";
            case "2":
                return "twenty";
            case "3":
                return "thirty";
            case "4":
                return "forty";
            case "5":
                return "fifty";
            case "6":
                return "sixty";
            case "7":
                return "seventy";
            case "8":
                return "eighty";
            case "9":
                return "ninety";
        }
    } else if ($digit1 == "1")
    {
        switch ($digit2)
        {
            case "1":
                return "eleven";
            case "2":
                return "twelve";
            case "3":
                return "thirteen";
            case "4":
                return "fourteen";
            case "5":
                return "fifteen";
            case "6":
                return "sixteen";
            case "7":
                return "seventeen";
            case "8":
                return "eighteen";
            case "9":
                return "nineteen";
        }
    } else
    {
        $temp = convertDigit($digit2);
        switch ($digit1)
        {
            case "2":
                return "twenty-$temp";
            case "3":
                return "thirty-$temp";
            case "4":
                return "forty-$temp";
            case "5":
                return "fifty-$temp";
            case "6":
                return "sixty-$temp";
            case "7":
                return "seventy-$temp";
            case "8":
                return "eighty-$temp";
            case "9":
                return "ninety-$temp";
        }
    }
}

function convertDigit($digit)
{
    switch ($digit)
    {
        case " ":
            return "zero";
        case "0":
            return "zero";
        case "1":
            return "one";
        case "2":
            return "two";
        case "3":
            return "three";
        case "4":
            return "four";
        case "5":
            return "five";
        case "6":
            return "six";
        case "7":
            return "seven";
        case "8":
            return "eight";
        case "9":
            return "nine";
    }
}
//function is used to asign ews
function accounts_fee_ews_asign()
{
    $q="SELECT `cId`,`class_name` FROM class_index ORDER BY class, section ASC";
	$q_res=mysql_query($q);
	echo '<div class="row-fluid">
	<div class="span12  widget clearfix">
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Asign EWS</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';
	if (isset($_GET['added_user']))
		{
			echo "<h5 align=\"center\">User Added to the specified route successfully!!</h5>";
		}
	if(isset($_GET['error_adding_vehicle']))
		{
			echo "<h5 align=\"center\" style='color:red'>Error!!</h5>";
		}	
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");
	echo '<h5 style="color:grey" align="center">'.$today.'</h5>';
	$disp='';
	$q="SELECT `cId`,`class_name` FROM class_index ORDER BY class, section ASC";
	$q_res=mysql_query($q);
	echo '<ol class="rounded-list">';
	while($res=mysql_fetch_array($q_res))
		{
			echo '<tr class="row-fluid" id="added_rows">
			<td><div class="row-fluid">
			<div class="span6">	
			';
			echo'						                                                              
			<li><a href="accounts_fee_ews_student.php?class_id='.$res['cId'].'"><span class="button approve" 
			id="btn">'.$res[1].'</span></a>                                                                     
			</div>
			</div><!-- end row-fluid  -->
			</td>
			</tr>';	 	 
		}
	echo'	
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';	
}
//show student and asign ews
function accounts_fee_ews_student()
{
    $class_id=$_GET['class_id'];
//get students of tha6t class
$get_stu_class=
"SELECT *
FROM student_user
INNER JOIN class
ON class.sId = student_user.sId
WHERE class.classId=".$class_id." AND class.session_id=".$_SESSION['current_session_id']."";	
$exe_stu_class=mysql_query($get_stu_class);
echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">
<div class="widget-header">
<span><i><img src="images/icon/gray_shadow18/location.png"/></i>Asign EWS</span>
</div><!-- End widget-header -->	
<div class="widget-content">';

if(isset($_GET['dallocate']))
    {
            $name=$_GET['name'];
            echo "<h5 align=\"center\" style='color:red'>Asign EWS of <b style='color:green'>(".$name." )</b></h5>";
    }
echo'<a href="accounts_fee_ews_asign.php"><button class="uibutton confirm" type="button">Back</button></a>';
echo'
<table  class="table table-bordered table-striped" id="dataTable" >
                              
				   <thead> 
				   <tr>
                                   <th>Admission No.</th>
				   <th>Student Name</th>
                                   <th>Father`s Name</th>
                                    
				   
				   <th>Contact No.</th>
				   <th>Action</th>
				   </thead>
				   <tbody align="center">
';	
while($fetch_stu_class=mysql_fetch_array($exe_stu_class))
{
echo '<tr>
    <td>'.$fetch_stu_class['admission_no'].'</td>
<td>'.$fetch_stu_class['Name'].'</td>
    <td>'.$fetch_stu_class[5].'</td>
        <td>'.$fetch_stu_class[4].'</td>
';
//  **************************   FEE_DETAILS ************* CONNECT  FEE  AND TRANSPORT ******************************
 //duration name
date_default_timezone_set('Asia/Kolkata');
$date=date('d-m-Y');
$month=explode("-",$date);


$flagsss_ews=1;
$ewss="SELECT * FROM ews_students";
$exe_ewss=mysql_query($ewss);
while($fetch_ewss=mysql_fetch_array($exe_ewss))
{
    if($fetch_ewss['sid']  == $fetch_stu_class[0])
    {
        $flagsss_ews=0;
        break;
    }
 
}
$status=0;
    if($status ==0 )
    {
        if($flagsss_ews == 1)
        {
    echo '<td style="color:green"><a class="btn btn-success" href="fee/asign_ews.php?student_id='.$fetch_stu_class[0].'&&name_stu='.$fetch_stu_class['Name'].'&&class_id='.$class_id.'">Asign</a></td>
    </tr>';	
        }
        elseif($flagsss_ews==0)
        {
              echo '<td style="color:green"><a class="btn btn-cancel" href="fee/asign_ews.php?student_id='.$fetch_stu_class[0].'&&name_stu='.$fetch_stu_class['Name'].'&&class_id='.$class_id.'">DE-Asign</a></td>
    </tr>';
        }
    }
//    elseif($status ==1 )
//    {
//         echo '<td style="color:red"><a class="btn btn-danger" href="transport/dallocate_vehicle_student.php?student_id='.$fetch_stu_class[0].'&&name_stu='.$fetch_stu_class['Name'].'&&class_id='.$class_id.'">Dallocate</a></td>
//         </tr>';
//    }
}
echo 
'</tbody>
</table>
</div>
</div>
</div>';
}

//show student and asign ews
function accounts_fee_ews_student_asign_allready()
{
    
//get students of tha6t class

echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">
<div class="widget-header">
<span><i><img src="images/icon/gray_shadow18/location.png"/></i>Asign EWS</span>
</div><!-- End widget-header -->	
<div class="widget-content">';



echo'
<table  class="table table-bordered table-striped" id="dataTable" >
                              
				   <thead> 
				   <tr>
                                   <th>Admission No.</th>
				   <th>Student Name</th>
                                   <th>Father`s Name</th>
                                    
				   <th>Class</th>
				   <th>Contact No.</th>
				   
				   </thead>
				   <tbody align="center">
';	
$cl="SELECT * FROM class_index ORDER BY order_by";
$exe_cl=mysql_query($cl);
while($fetch_cl=mysql_fetch_array($exe_cl))
{

$ewss="SELECT * FROM student_user 
    INNER JOIN ews_students
    ON ews_students.sid=student_user.sId
   INNER JOIN class
	ON class.sId=student_user.sId WHERE class.classId=".$fetch_cl[0]."";
$exe_ewss=mysql_query($ewss);

while($fetch_stu_class=mysql_fetch_array($exe_ewss))
{
    $class="SELECT class_index.class_name FROM class_index
        INNER JOIN class
        ON class.classId=class_index.cId
        WHERE class.sId=".$fetch_stu_class[0]."";
    $exe_class=mysql_query($class);
    $fetch_class=mysql_fetch_array($exe_class);
echo '<tr>
    <td>'.$fetch_stu_class['admission_no'].'</td>
<td>'.$fetch_stu_class['Name'].'</td>
    <td>'.$fetch_stu_class[5].'</td>
        <td>'.$fetch_class['class_name'].'</td>
        <td>'.$fetch_stu_class[4].'</td>
';
//  **************************   FEE_DETAILS ************* CONNECT  FEE  AND TRANSPORT ******************************
 //duration name
date_default_timezone_set('Asia/Kolkata');
$date=date('d-m-Y');
$month=explode("-",$date);



// GET TRANSPORT COST FROM DESTINATION ROUTE
//     $student_id=$fetch_stu_class['sId'];
//   $get_last_asign_transport_id="SELECT MAX(Id) FROM user_vehicle_details
//                                 WHERE uId=".$student_id." AND session_id=".$_SESSION['current_session_id']."";
//   $exe_last=mysql_query($get_last_asign_transport_id);
//   $fetch_last_tr_id=mysql_fetch_array($exe_last);
//   $max_id=$fetch_last_tr_id[0];
//
//    $get_last_asign_transport_id1="SELECT status FROM user_vehicle_details
//                                 WHERE Id=".$max_id." AND session_id=".$_SESSION['current_session_id']."";
//   $exe_last1=mysql_query($get_last_asign_transport_id1);
//   if(!$exe_last1)
//   {
//       $status=0;
//   }
//   else 
//   {
//        $fetch_last_tr_id1=mysql_fetch_array($exe_last1);
//        $status=$fetch_last_tr_id1[0];
//   }

//$status=0;
//    if($status ==0 )
//    {
//        if($flagsss_ews == 1)
//        {
//    echo '<td style="color:green"><a class="btn btn-success" href="fee/asign_ews.php?student_id='.$fetch_stu_class[0].'&&name_stu='.$fetch_stu_class['Name'].'&&class_id='.$class_id.'">Asign</a></td>
//    </tr>';	
//        }
//        elseif($flagsss_ews==0)
//        {
//              echo '<td style="color:red">EWS student</td>
//    </tr>';
//        }
//    }
//    elseif($status ==1 )
//    {
//         echo '<td style="color:red"><a class="btn btn-danger" href="transport/dallocate_vehicle_student.php?student_id='.$fetch_stu_class[0].'&&name_stu='.$fetch_stu_class['Name'].'&&class_id='.$class_id.'">Dallocate</a></td>
//         </tr>';
//    }
}
}
echo 
'</tbody>
<button><a href="accounts_fee_ews_print.php">prinp</a></button>
</table>
</div>
</div>
</div>';
}

function accounts_fee_today_collection_cash()
{
   $sn=1;
 $current_session=$_SESSION['current_session_id'];
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d');

          $april_june=date('m');
	$format=date('Y-m-d',strtotime($date));
      
        $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
        $paid_on_date_id=$fetch_pre_date11[0];
    
$get_school = "SELECT * FROM `school_names`";
$exe_school = mysql_query($get_school);
$fetch_school=mysql_fetch_array($exe_school);

$school_name=$fetch_school[1];
     
    echo'<br><br><br>
        <div id="replace_collection">
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    <div class="widget-header">
   
      <h2 style="color:green" align="center">'.$school_name.'</h2>
 
    </div><!-- End widget-header -->	
    <div class="widget-content">';
  
      
         echo'<form action="accounts_fee_today_collection_cash_print.php" method="get">
        
	<div class="section">
		<label>FROM</label>
		<div><input type="date" name="from" value="'.$format.'"></div>
        </div>

	<div class="section">
		<label>TO</label>
		<div><input type="date" name="to" value="'.$format.'"></div>
        </div>

	<div class="section">
		<label>TYPE</label>
		<div>
			<select name="type">
				<option value="2">..select type..</option>
				<option value="0">CASH</option>
				<option value="1">CHEQUE</option>
				<option value="2">ALL(CASH/CHEQUE)</option>
				<option value="3">TRANSPORT</option>
			</select>
		</div>
        </div>

        <div class="section last" align="center">
	        <br><br>
		<input type="submit" value="PRINT">
        </div>

        </form>
      
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->'; 
    echo '</div>';
}




function accounts_fee_today_collection_cash_print()
{
    
$sn=1;
$session_id=$_SESSION['current_session_id'];
$cash=0;
$chq=0;

$tution_fee=0;
$development_fee=0;
$activity_fee=0;
$exam_fee=0;
$computer_fee=0;
$annual_charge=0;

$total_transport=0;

$fine=0;
$disc=0;
$total_paid=0;
$total_due=0;

$total_fee=0;
$grand_total_fee=0;

$from= $_GET['from'];
$to  = $_GET['to'];

$type= $_GET['type'];  // type:0 = CASH;    type:1 = CHEQUE;     type:2 = ALL


 $current_session=$_SESSION['current_session_id'];

 $format_to=date('Y-m-d',strtotime($to));

 $format_from=date('Y-m-d',strtotime($from));


$dd_format=date('d-M-Y',strtotime($date));
      
        $get_to_date="SELECT date_id FROM dates_d
                     WHERE date='".$format_to."'";
        $exe_to_date=mysql_query($get_to_date);
        $fetch_to_date=mysql_fetch_array($exe_to_date);
        $to_date_id=$fetch_to_date[0];
      
      
        $get_from_date="SELECT date_id FROM dates_d
                     WHERE date='".$format_from."'";
        $exe_from_date=mysql_query($get_from_date);
        $fetch_from_date=mysql_fetch_array($exe_from_date);
        $from_date_id=$fetch_from_date[0];
      

$get_school = "SELECT * FROM `school_names`";
$exe_school = mysql_query($get_school);
$fetch_school=mysql_fetch_array($exe_school);

$school_name=$fetch_school[1];
      
    echo'
        <div id="replace_collection">
    <div class="row-fluid" style="width:120%;">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
    <div class="widget-header">
    
      <h2 style="color:green" align="center">'.$school_name.'</h2>
     
    </div><!-- End widget-header -->	
    <div class="widget-content" style="">';
    //only  cash  cheque
if($type<3)
{

echo '
    <table  class="table table-bordered table-striped" >
    <thead>
    <tr>
    <th>S.No.</th>
    <th>Receipt<br> No.</th>
    <th>Date</th>
    
    <th>Admission<br> No.</th>
    <th>Student<br> Name</th>
    <th>Father`s<br> Name</th>
    <th>Class</th>
    <th>Duration</th>
    <th>Cash/<br>Cheque</th>
    <th>Admission Fee</th> 
    <th>Annual<br>Charge</th>
     
    <th>Tution<br>Fee</th>   
    <th>Development<br>Charge</th>   
     
    <th>Fine</th>
    <th>Discount</th>
    <th style="color:purple;">TOTAL</th>
    <th style="color:green;">Paid</th>
    <th style="color:red;">Due</th>
    </tr>
    </thead>
    <tbody align="center">
    ';	
    

if($type==2)
    $today="SELECT * FROM fee_receipt_details WHERE date>=".$from_date_id." AND date<=".$to_date_id." AND cheque_status=0";

elseif($type==1)
    $today="SELECT * FROM fee_receipt_details WHERE date>=".$from_date_id." AND date<=".$to_date_id." AND cheque_no>0 AND cheque_status=0";

else
    $today="SELECT * FROM fee_receipt_details WHERE date>=".$from_date_id." AND date<=".$to_date_id." AND cheque_no=0 AND cheque_status=0";

    $exe_today=mysql_query($today);
    if(!$exe_today)
    {
        
    }
    else 
    {
    while($fetch_today=mysql_fetch_array($exe_today))
    {
        
        
        
        //get student detaILS 
        $name="SELECT * FROM student_user WHERE sId=".$fetch_today['sid']."";
        $exe_name=mysql_query($name);
        $fetch_name=mysql_fetch_array($exe_name);
        
	//GET CLASS NAME
        $class="SELECT class_name, class FROM class_index
		INNER JOIN class
		ON class.classId=class_index.cId

WHERE class.sId=".$fetch_today[2]."";
        $exe_class=mysql_query($class);
        $fetch_class=mysql_fetch_array($exe_class);
        echo '
            <tr>
               <td>'.$sn++.'</td>
               <td>'.$fetch_today[1].'</td>';
		 //date
        $date="SELECT date FROM dates_d WHERE date_id=".$fetch_today['date']."";
        $exe_date=mysql_query($date);
        $fetch_date=mysql_fetch_array($exe_date);
	$dd =date('d-M-Y', strtotime($fetch_date[0]));
        
echo'
		  <td>'.$dd.'</td>  
                <td>'.$fetch_name['admission_no'].'</td>
                <td>'.$fetch_name['Name'].'</td>
                <td>'.$fetch_name[5].'</td>
                <td>'.$fetch_class[0].'</td>
                <td>'.$fetch_today['duration_name'].'</td>';

                if($fetch_today['cheque_no']==0)
                {
                   $cash += $fetch_today['amounts'];
                   echo '<td>Cash</td>';
                }
                else 
                {
                      $chq += $fetch_today['amounts'];
                      echo '<td>'.$fetch_today['cheque_no'].'</td>';
                }

$tt=$fetch_today['amount']+$fetch_today['fine'];

echo' <td>'.$fetch_today['ad1'].'</td>
                <td>'.$fetch_today['annual_1'].'</td>
                
                <td>'.$fetch_today['month_1'].'</td>
                <td>'.$fetch_today['month_2'].'</td>
            
                <td>'.$fetch_today['fine'].'</td>
                <td>'.$fetch_today['discount'].'</td>
      		<td style="color:purple;">'.($fetch_today['amounts'] + $fetch_today['due']).'</td>
                <td style="color:green;">'.$fetch_today['amounts'].'</td>
		<td style="color:red;">'.$fetch_today['due'].'</td>';

$tution_fee     += $fetch_today['month_1'];
$development_fee+= $fetch_today['month_2'];

$ad1       += $fetch_today['ad1'];

$annual_charge  += $fetch_today['annual_1'];

$fine += $fetch_today['fine'];
$disc += $fetch_today['discount'];


$total_fee += $tution_fee + $development_fee + $activity_fee + $exam_fee + $computer_fee + $annual_charge ;

$grand_total_fee += $total_fee;

$total_paid += $fetch_today['amounts'];
$total_due += $fetch_today['due'];

       echo ' </tr>';
        

//$total=$total+$fetch_today['amount']+$fetch_today['fine'];
    }
    }


echo'
<tr style="font-weight:bold;">
	<td style="color:purple" align="center" colspan="9">GRAND TOTAL</td>
<td style="color:blue">'.$ad1.'</td>
	<td style="color:blue">'.$annual_charge.'</td>

	<td style="color:blue">'.$tution_fee.'</td>
	<td style="color:blue">'.$development_fee.'</td>
	
	<td style="color:blue">'.$fine.'</td>
	<td style="color:blue">'.$disc.'</td>
	<td style="color:purple">'.($total_paid + $total_due).'</td>
	<td style="color:green">'.$total_paid.'</td>
	<td style="color:red">'.$total_due.'</td>
</tr>';
    
echo'
    </tbody>
    </table>';
}
else
{

	
echo '
    <table  class="table table-bordered table-striped" >
    <thead>
    <tr>
    <th>S.No.</th>
    <th>Receipt<br> No.</th>
    
    <th>Admission<br> No.</th>
    <th>Student<br> Name</th>
    <th>Father`s<br> Name</th>
    <th>Class</th>
    <th>Duration</th>
    <th>Cash/<br>Cheque</th>
    
    <th style="color:purple;">TOTAL</th>
  
    </tr>
    </thead>
    <tbody align="center">
    ';	
    

    $today="SELECT * FROM fee_receipt_details_transport WHERE date>=".$from_date_id." AND date<=".$to_date_id."";

    $exe_today=mysql_query($today);
    if(!$exe_today)
    {
        
    }
    else 
    {
    while($fetch_today=mysql_fetch_array($exe_today))
    {
        
        
        
        //get student detaILS 
        $name="SELECT * FROM student_user WHERE sId=".$fetch_today['sid']."";
        $exe_name=mysql_query($name);
        $fetch_name=mysql_fetch_array($exe_name);
        
	//GET CLASS NAME
        $class="SELECT class_name, class FROM class_index
		INNER JOIN class
		ON class.classId=class_index.cId

WHERE class.sId=".$fetch_today[2]."";
        $exe_class=mysql_query($class);
        $fetch_class=mysql_fetch_array($exe_class);
        echo '
            <tr>
               <td>'.$sn++.'</td>
               <td>'.$fetch_today[1].'</td>
                <td>'.$fetch_name['admission_no'].'</td>
                <td>'.$fetch_name['Name'].'</td>
                <td>'.$fetch_name[5].'</td>
                <td>'.$fetch_class[0].'</td>
                <td>'.$fetch_today['duration_name'].'</td>';

                if($fetch_today['cheque_no']==0)
                {
                   $cash += $fetch_today['amounts'];
                   echo '<td>Cash</td>';
                }
                else 
                {
                      $chq += $fetch_today['amounts'];
                      echo '<td>'.$fetch_today['cheque_no'].'</td>';
                }

$tt=$fetch_today['amount']+$fetch_today['fine'];

echo' 
      		<td style="color:purple;">'.($fetch_today['amounts'] + $fetch_today['due']).'</td>
                ';

$tution_fee     += $fetch_today['month_1'];
$development_fee+= $fetch_today['month_2'];

$ad1       += $fetch_today['ad1'];

$annual_charge  += $fetch_today['annual_1'];

$fine += $fetch_today['fine'];
$disc += $fetch_today['discount'];


$total_fee += $tution_fee + $development_fee + $activity_fee + $exam_fee + $computer_fee + $annual_charge ;

$grand_total_fee += $total_fee;

$total_paid += $fetch_today['amounts'];
$total_due += $fetch_today['due'];

       echo ' </tr>';
        

//$total=$total+$fetch_today['amount']+$fetch_today['fine'];
    }
    }


echo'
<tr style="font-weight:bold;">
	<td style="color:purple" align="center" colspan="8">GRAND TOTAL</td>

	<td style="color:purple">'.($total_paid + $total_due).'</td>
	
</tr>';
    
echo'
    </tbody>
    </table>';



}
	

   echo ' </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->'; 
    echo '</div>';
}


function accounts_fee_enter_receipt_no()
{
    echo '<div class="row-fluid">
	<div class="span12  widget clearfix">
	<div class="widget-header">
	<span><i class="icon-align-center"></i>ADD TOTAL BETWEEN ENTER RECEIPT NO.</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';
	echo '<div class="section numericonly">
            RECEIPT NO. FROM <input type="text" id="low" name="low">
            </div><br>
            <div class="section numericonly">
            RECEIPT NO. TO <input type="text" id="up" name="up">
            </div><br>
            <div class="section last">
            RECEIPT NO.<input type="submit" id="add" onclick="add();" name="add" value="ADD SCHOOL FEE +">
            </div>
            <div id="add"></div><br><br><br><br>
';


echo '<div class="section numericonly">
            RECEIPT NO. FROM <input type="text" id="lowtr" name="low">
            </div><br>
            <div class="section numericonly">
            RECEIPT NO. TO <input type="text" id="uptr" name="up">
            </div><br>
            <div class="section last">
            RECEIPT NO.<input type="submit" id="add" onclick="add_tr();" name="add" value="ADD TRANSPORT FEE +">
            </div>
            <div id="add_tr"></div>
';
    	echo'	
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
    
}



function accounts_fee_ews_print()
{
$ss=1;
echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">
<h4 align="center" style="color:green">VIDYA BAL BHAWAN SR. SEC. SCHOOL</H4>	
<div class="widget-content">';



echo'
<table  class="table table-bordered table-striped" id="dataTable" >
            <h6 align="center" style="color:RED">EWS STUDENT DETAILS</H6>	                  
				   <thead> 
				   <tr>
<th>S.No.</th>
                                   <th>Admission No.</th>
				   <th>Student Name</th>
                                   <th>Father`s Name</th>
                                    <th>Mother`s Name</th>

				   <th>Class</th>
				   <th>Contact No.</th>
				   
				   </thead>

				   <tbody align="center">
';	

$cl="SELECT * FROM class_index ORDER BY order_by";
$exe_cl=mysql_query($cl);
while($fetch_cl=mysql_fetch_array($exe_cl))
{



$ewss="SELECT * FROM student_user 

    INNER JOIN ews_students
    ON ews_students.sid=student_user.sId
	INNER JOIN class
    ON class.sId=student_user.sId
 WHERE class.classId=".$fetch_cl[0]."
 ";
$exe_ewss=mysql_query($ewss);

while($fetch_stu_class=mysql_fetch_array($exe_ewss))
{
    $class="SELECT class_index.class_name FROM class_index
        INNER JOIN class
        ON class.classId=class_index.cId
        WHERE class.sId=".$fetch_stu_class[0]."";
    $exe_class=mysql_query($class);
    $fetch_class=mysql_fetch_array($exe_class);
if($fetch_class['class_name'] == "")
{

}
else
{
echo '<tr  > 
<td style="font-size:12px;">'.$ss++.'</td>
    <td style="font-size:12px;">'.$fetch_stu_class['admission_no'].'</td>
<td style="font-size:12px;">'.strtoupper($fetch_stu_class['Name']).'</td>
    <td style="font-size:12px;">'.strtoupper($fetch_stu_class[5]).'</td>
     <td style="font-size:12px;">'.strtoupper($fetch_stu_class[6]).'</td>
        <td style="font-size:12px;">'.$fetch_class['class_name'].'</td>
        <td style="font-size:12px;">'.$fetch_stu_class[4].'</td>
</tr>';


}
}
}
//  **************************   FEE_DETAILS ************* CONNECT  FEE  AND TRANSPORT ******************************

echo 
'</tbody>

</table>

</div>

</div>

</div>';
}




function accounts_fee_paid()
{
	//Author By: Anil Kumar *
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Not Pay Students</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';
//    $duration="SELECT Id, duration FROM fee_generated_sessions
//	           WHERE session_id=".$_SESSION['current_session_id']."";
//	$exe_duration=mysql_query($duration);	
	echo'  
	<div class="load_page">
	<div class="formEl_b">	';
	//in this form level of defalters select 
	echo '
	<form id="validation_demo" action="accounts_fee_paid_cls_show.php" method="get"> ';
//	<div class="section ">
//	<label>Select Type <small></small></label>   
//	<div> 
//	<select name="level">
//	<option value="">---SELECT LEVEL---</option>
//	<option value="1">Pre Primary</option>
//	<option value="2"> Primary</option>
//	<option value="3">Secondary</option>
//	<option value="4">Senior Secondary</option>
//	<option value="5">Class Wise</option>
//	<option value="6">School Wise</option>
//	</select>
//	</div>
//	</div>
	echo '<div class="section">
	<label>Select Duration<small></small></label>
	<div>
	<select name="duration">
	<option value="">--SELECT DURATION--</option>';
//	while($fetch_duration_id=mysql_fetch_array($exe_duration))
//	    {
//	        echo '<option value="'.$fetch_duration_id[0].'">'.$fetch_duration_id[1].'</option>';
//		}
	echo '<option value="1001" >April-June</option>
	      <option value="1002" >July-Sep</option>
	      <option value="1003">Oct-Dec</option>
		  <option value="1004">Jan-March</option>
	</select>
	</div>
	</div>';
        $cid="SELECT * FROM class_index ORDER BY order_by";
        $exe_cid=mysql_query($cid);
        echo '<div class="section">
	<label>Select Class<small></small></label>
	<div>
	<select name="cls">
	<option value="">--SELECT Class--</option>';
//	while($fetch_duration_id=mysql_fetch_array($exe_duration))
//	    {
//	        echo '<option value="'.$fetch_duration_id[0].'">'.$fetch_duration_id[1].'</option>';
//		}
        while($fetch_cid=mysql_fetch_array($exe_cid))
        {
            echo '<option value="'.$fetch_cid[0].'" >'.$fetch_cid[1].'</option>';
        }  
	echo '</select>
	</div>
	</div>';
  /*  echo ' <div class="section">
	       <div>OR </div></div>';
	echo '
	<div class="section">
	<label>Select Date</label>   
	<div><input type="text"  id="datepick" class="datepicker" readonly="readonly" name="date"  />
	</div>
	</div>';*/
	echo '<div class="section last">
	<div>
	<button class="btn submit_form">Submit</button>
	</div>
	</div>
	</form>
	</div>								
	</div>';
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}
function fee_paid_class_show()
{
     $duration=$_GET['duration'];
     $cid=$_GET['cls'];
     if($duration==1001)
     {
         $dur=1;
         $d="Apr-Jun";
     }
     elseif($duration==1002)
     {
         $dur=4;
          $d="Jul-Sep";
     }
       elseif($duration==1003)
     {
         $dur=7;
         $d="Oct-Dec";
     }
       elseif($duration==1004)
     {
         $dur=10;
         $d="Jan-March";
     }
     $class="SELECT * FROM class_index WHERE cId=".$cid."";
     $exe_cid=mysql_query($class);
     $fetch_class=mysql_fetch_array($exe_cid);
        echo' 
			<div class="row-fluid">
			<!-- Table widget -->
			<div class="widget  span12 clearfix">
			<div class="widget-header">
			<span>Fee Paid Student List of ( '.$d.' ) of Class ( '.$fetch_class[1].' ) </span>
			</div><!-- End widget-header -->	
			<div class="widget-content">';
			 $defa="SELECT * FROM fee_details
                             
                                INNER JOIN class
                                ON class.sId=fee_details.student_id

                               WHERE (paid=1 OR paid=4 ) AND class.classId=".$cid." AND fee_generated_session_id=".$dur." 
                              AND class.session_id=".$_SESSION['current_session_id']."";
                         $exe_dur=mysql_query($defa);
                        
                       
			echo'                                   
			<table class="table table-bordered table-striped" id="dataTable">
			<thead>';
			echo'    
			<tr>
			<th width="5%">S.No.</th>
			<th  width="12%">Admission No.</th>
			<th width="12%">Name</th>
			
                        <th width="12%">Phone</th>
                        <th width="12%">Father`s Name</th>
                          <th width="12%">Date</th>
			<th width="12%">MOD</th>
			<th width="12%">Amounts</th>
                        <th width="12%">Paid</th>
                        <th width="12%">Dues</th>
			</thead>
			
			<tbody align="center">';
                        $s=0;
                        while($fetch_dur=mysql_fetch_array($exe_dur))
                        {
                            $student_id=$fetch_dur[1];
                            $du=$fetch_dur['duration_id'];
                            $amo="SELECT SUM(monthly_charges),SUM(receiving_fee),SUM(remaning_fee) FROM fee_details WHERE student_id=".$student_id." AND duration_id=".$du."";
                            $exe_du=mysql_query($amo);
                            $fetch_am=mysql_fetch_array($exe_du);
                            
                            $amo_s="SELECT * FROM student_user WHERE sId=".$student_id." ";
                            $exe_du_s=mysql_query($amo_s);
                            $fetch_am_s=mysql_fetch_array($exe_du_s);
                            
                            $cl="SELECT class_name FROM class_index 
                                INNER JOIN class
                                ON class.classId=class_index.cId
                                WHERE class.sId=".$student_id."";
                            $exe_cl=mysql_query($cl);
                            if(!$exe_cl)
                            {
                                
                            }
                            else 
                            {
                            $fetch_cl=mysql_fetch_array($exe_cl);
                            
                            echo '<tr>';
                               echo ' <td>'.++$s.'</td>';
                               echo ' <td>'. $fetch_am_s['admission_no'].'</td>';
                               echo ' <td>'. $fetch_am_s['Name'].'</td>';
                              
                               echo ' <td>'. $fetch_am_s['Phone No'].'</td>';
                                echo ' <td>'. $fetch_am_s[5].'</td>';
                               
                                    $datesh="SELECT date FROM dates_d WHERE date_id=".$fetch_dur[6]."";
                                     $exe_date=mysql_query($datesh);
                                      $fetch_d=mysql_fetch_array($exe_date);
                                 $format=explode('-',$fetch_d[0]);
                                  
                                 echo ' <td>'.$format[2].'-'.$format[1].'-'.$format[0].'</td>';
                                     echo ' <td>'.$fetch_dur[7].'</td>';
                                 
                               echo ' <td>'.$fetch_am[0].'</td>';
                               echo ' <td>'.$fetch_am[1].'</td>';
                                echo ' <td>'.$fetch_am[2].'</td>';
                               echo '</tr>';

        
                            }
                        }
  //  echo '<a href="fee/send_defaulter_message.php?du='. $duration.'"><input type="submit" value="Send message" ></a>';
echo' </tbody>
    <button><a href="accounts_fee_class_paid_print.php?duration='.$duration.'&cls='.$cid.'">prinp</a></button>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}
function accounts_fee_paid_print()
{
     $duration=$_GET['duration'];
     $cid=$_GET['cls'];
     if($duration==1001)
     {
         $dur=1;
         $d="Apr-Jun";
     }
     elseif($duration==1002)
     {
         $dur=4;
          $d="Jul-Sep";
     }
       elseif($duration==1003)
     {
         $dur=7;
         $d="Oct-Dec";
     }
       elseif($duration==1004)
     {
         $dur=10;
         $d="Jan-March";
     }
     $class="SELECT * FROM class_index WHERE cId=".$cid."";
     $exe_cid=mysql_query($class);
     $fetch_class=mysql_fetch_array($exe_cid);
        echo' 

			<div class="row-fluid">
			<!-- Table widget -->
			<div class="widget  span12 clearfix">
			<div class="widget-header">
                           <h3 align="center" style="color:green">VIDYA BAL BHAWAN SR. SEC. SCHOOL</H4>

    <div class="widget-header">
    <h5 align="center" style="color:red">
    Fee Paid Students Of Class '.$fetch_class[1].'


    </h5>
    <h4 align="right"><b>Date : '.$date.'</b></h4>
			</div><!-- End widget-header -->	
			<div class="widget-content">';
			 $defa="SELECT * FROM fee_details
                             

                                INNER JOIN class
                                ON class.sId=fee_details.student_id

                                WHERE (paid=1 OR paid=4 ) AND class.classId=".$cid." AND fee_generated_session_id=".$dur." 
                              AND class.session_id=".$_SESSION['current_session_id']."";
                         $exe_dur=mysql_query($defa);
                        
                       
			echo'                                   

			<table class="table table-bordered table-striped" id="dataTable">
			<thead>';
			echo'    
			<tr>

			<th width="5%">S.No.</th>
			<th  width="12%">Admission No.</th>
			<th width="12%">Name</th>
			

                        <th width="12%">Phone</th>
                        <th width="12%">Father`s Name</th>
                          <th width="12%">Date</th>
			<th width="12%">MOD</th>
			<th width="12%">Amounts</th>
                        <th width="12%">Paid</th>
                        <th width="12%">Dues</th>
			</thead>
			
			<tbody align="center">';
                        $s=0;
                        while($fetch_dur=mysql_fetch_array($exe_dur))
                        {
                            $student_id=$fetch_dur[1];
                            $du=$fetch_dur['duration_id'];
                            $amo="SELECT SUM(monthly_charges),SUM(receiving_fee),SUM(remaning_fee) FROM fee_details WHERE student_id=".$student_id." AND duration_id=".$du."";
                            $exe_du=mysql_query($amo);
                            $fetch_am=mysql_fetch_array($exe_du);
                            
                            $amo_s="SELECT * FROM student_user WHERE sId=".$student_id." ";
                            $exe_du_s=mysql_query($amo_s);
                            $fetch_am_s=mysql_fetch_array($exe_du_s);
                            
                            $cl="SELECT class_name FROM class_index 
                                INNER JOIN class
                                ON class.classId=class_index.cId
                                WHERE class.sId=".$student_id."";
                            $exe_cl=mysql_query($cl);
                            if(!$exe_cl)
                            {
                                
                            }
                            else 
                            {
                            $fetch_cl=mysql_fetch_array($exe_cl);
                            
                            echo '<tr>';
                               echo ' <td>'.++$s.'</td>';
                               echo ' <td>'. $fetch_am_s['admission_no'].'</td>';
                               echo ' <td>'. $fetch_am_s['Name'].'</td>';
                              
                               echo ' <td>'. $fetch_am_s['Phone No'].'</td>';
                                echo ' <td>'. $fetch_am_s[5].'</td>';
                               
                                    $datesh="SELECT date FROM dates_d WHERE date_id=".$fetch_dur[6]."";
                                     $exe_date=mysql_query($datesh);
                                      $fetch_d=mysql_fetch_array($exe_date);
                                 $format=explode('-',$fetch_d[0]);
                                  
                                 echo ' <td>'.$format[2].'-'.$format[1].'-'.$format[0].'</td>';
                                     echo ' <td>'.$fetch_dur[7].'</td>';
                                 
                               echo ' <td>'.$fetch_am[0].'</td>';
                               echo ' <td>'.$fetch_am[1].'</td>';
                                echo ' <td>'.$fetch_am[2].'</td>';
                               echo '</tr>';

        
                            }
                        }
  //  echo '<a href="fee/send_defaulter_message.php?du='. $duration.'"><input type="submit" value="Send message" ></a>';
echo'  </table><br><br>
    

        1: This list contains the students who have deposit their fee.<br>
        2: The `0` under the payment Heading means child belong to the EWS category.<br><br>
                                                <h5 align="right"><b>Class Teacher Name ....................................................................</b></h5>
                                                <h5 align="right"><b>Sign:........................................................................</b></h5>
    
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}
function accounts_fee_paid_date()
{
	//Author By: Anil Kumar *
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Not Pay Students</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';
//    $duration="SELECT Id, duration FROM fee_generated_sessions
//	           WHERE session_id=".$_SESSION['current_session_id']."";
//	$exe_duration=mysql_query($duration);	
	echo'  
	<div class="load_page">
	<div class="formEl_b">	';
	//in this form level of defalters select 
	echo '

	<form id="validation_demo" action="accounts_fee_paid_cls_date_show.php" method="get"> ';
//	<div class="section ">
//	<label>Select Type <small></small></label>   
//	<div> 
//	<select name="level">
//	<option value="">---SELECT LEVEL---</option>
//	<option value="1">Pre Primary</option>
//	<option value="2"> Primary</option>
//	<option value="3">Secondary</option>
//	<option value="4">Senior Secondary</option>
//	<option value="5">Class Wise</option>
//	<option value="6">School Wise</option>
//	</select>
//	</div>
//	</div>
	echo '<div class="section">
	<label>Select Date<small></small></label>
	<div>
	<input type="date" name="date"> 
	</div>
	</div>';
        $cid="SELECT * FROM class_index ORDER BY order_by";
        $exe_cid=mysql_query($cid);
        echo '<div class="section">
	<label>Select Class<small></small></label>
	<div>
	<select name="cls">
	<option value="">--SELECT Class--</option>';
//	while($fetch_duration_id=mysql_fetch_array($exe_duration))
//	    {
//	        echo '<option value="'.$fetch_duration_id[0].'">'.$fetch_duration_id[1].'</option>';
//		}
        while($fetch_cid=mysql_fetch_array($exe_cid))
        {
            echo '<option value="'.$fetch_cid[0].'" >'.$fetch_cid[1].'</option>';
        }  
	echo '</select>
	</div>
	</div>';
  /*  echo ' <div class="section">

	       <div>OR </div></div>';

	echo '
	<div class="section">
	<label>Select Date</label>   
	<div><input type="text"  id="datepick" class="datepicker" readonly="readonly" name="date"  />
	</div>
	</div>';*/
	echo '<div class="section last">
	<div>
	<button class="btn submit_form">Submit</button>
	</div>
	</div>
	</form>
	</div>								
	</div>';
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}

function  fee_paid_class_show_date()
{
    
$date_total=0;
$date_due=0;
    $session_id=$_SESSION['current_session_id'];
 $current_session=$_SESSION['current_session_id'];
 $date=$_GET['date'];
$class=$_GET['cls'];
	$srn=1;
         //CLASS
        $class_name="SELECT class_name FROM class_index
            
            WHERE cId=".$class."
            ";
        $exe_class=mysql_query($class_name);
        $fetch_class=mysql_fetch_array($exe_class);
          $april_june=date('m');
	$format=date('Y-m-d',strtotime($date));
      
       $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
        $paid_on_date_id=$fetch_pre_date11[0];
  $fee_receipt="SELECT * FROM fee_receipt_details 
                 INNER JOIN class
                 ON class.sId=fee_receipt_details.sid
                 WHERE date >=".$paid_on_date_id."
                 AND fee_receipt_details.cheque_status=0
                 AND class.classId=".$class." AND class.session_id=".$_SESSION['current_session_id']."";
    $exe_receipt=mysql_query($fee_receipt);
date_default_timezone_set('Asia/Kolkata');
$date=date('d-M-Y');
    echo'

    <div id="replace_collection">
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
<h3 align="center" style="color:green">VIDYA BAL BHAWAN SR. SEC. SCHOOL</H4>
    <div class="widget-header">
    <h5 align="center" style="color:red">
    Fee Paid Students Of Class '.$fetch_class[0].'

    </h5>
    <h4 align="right"><b>Date : '.$date.'</b></h4>
    </div><!-- End widget-header -->	
    <div class="widget-content">

    <table  class="table table-bordered table-striped" id="dataTable"  >
    
    <tr style="color:green">
    <th>S. <br>No.</th>
    <th>Receipt<br> No.</th>

    <th>Admission<br> No.</th>

    <th>Student<br> Name</th>

    <th>Father`s<br> Name</th>

   <th>Cash/<br>Cheque </th>

    <th>Duration</th>

    <th>Date</th>
    ';
  // echo '  <th>Previous<br>Dues</th>';
//    echo '<th>Transport</th>
//    <th>Discount</th>';
    echo '

   

    <th style="color:green">Paid</th>

    <th style="color:red">Dues</th>

    

    

    

    

    </tr>

  

    

     ';
    while($fetch_receipt=mysql_fetch_array($exe_receipt))
    {
        //STUDENT DETAILSS
        $student_details="SELECT * FROM student_user WHERE sId=".$fetch_receipt[2]."";
        $exe_student=mysql_query($student_details);
        $fetch_student=mysql_fetch_array($exe_student);
        //CLASS
        $class_name="SELECT class_name FROM class_index
            INNER JOIN class
            ON class.classId =class_index.cId
            WHERE class.sId = ".$fetch_receipt[2]."
            AND class.session_id= ".$session_id."";
        $exe_class=mysql_query($class_name);
        $fetch_class=mysql_fetch_array($exe_class);
        
        echo '<tr style="border:1;solid black; color:BLACK; font-size:12px;">';
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$srn++.'</td>';
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$fetch_receipt[0].'</td>';
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$fetch_student['admission_no'].'</td>';
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$fetch_student['Name'].'</a></td>';
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$fetch_student[5].'</td>';
       // echo '<td>'.$fetch_class['class_name'].'</td>';
         if($fetch_receipt['cheque_no'] == 0)
        {
          echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">Cash</td>';
        }
        else 
        {
            echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">Cheque</td>';
           // echo '<td>'.$fetch_receipt['cheque_no'].'</td>';
        }
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$fetch_receipt['duration_name'].'</td>';
        
        //date
        $date="SELECT date FROM dates_d WHERE date_id=".$fetch_receipt['date']."";
        $exe_date=mysql_query($date);
        $fetch_date=mysql_fetch_array($exe_date);
            $dts=explode('-',$fetch_date[0]);
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$dts[2].'-'.$dts[1].'-'.$dts[0].'</td>';
       //echo '<td>'.$fetch_receipt['pre_remaning_fee'].'</td>';
//        echo '<td>0</td>';
//        echo '<td>'.$fetch_receipt['discount'].'</td>';
       // echo '<td>'.$fetch_receipt['fine'].'</td>';
        //
        $total = $fetch_receipt['amount'] + $fetch_receipt['due'] ;
        
       // echo '<td style="color:green">'.$total.'</td>';
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$fetch_receipt['amount'].'</td>';
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$fetch_receipt['due'].'</td>';
        
     $date_total=$date_total+$fetch_receipt['amount'];
$date_due=$date_due+$fetch_receipt['due'];
      //  echo '<td>'.$fetch_receipt['bank'].'</td>';
       // echo '<td>'.$fetch_receipt['branch'].'</td>';
          // echo '<td style="color:red">';
//           echo '  <span class="tip"><a href="#" original-title="Edit">
//			<img src="images/icon/icon_edit.png"></a></span> ';

        echo '</tr>';
    }
 
    echo'
    </tbody>
    <button><a href="accounts_fee_paid_cls_date_print.php?date='.$format.'&cls='.$class.'">PRINT</a></button>
    </table>
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->

    </div><!-- row-fluid -->';   
}
function  fee_paid_class_show_date_print()
{
$date_total=0;
$date_due=0;
    $session_id=$_SESSION['current_session_id'];
 $current_session=$_SESSION['current_session_id'];
 $date=$_GET['date'];
 $class=$_GET['cls'];
	$srn=1;
         //CLASS
        $class_name="SELECT class_name FROM class_index
            
            WHERE cId=".$class."
            ";
        $exe_class=mysql_query($class_name);
        $fetch_class=mysql_fetch_array($exe_class);
          $april_june=date('m');
	$format=date('Y-m-d',strtotime($date));
      
       $pre_date11="SELECT date_id FROM dates_d
                     WHERE date='".$format."'";
        $exe_pre_date11=mysql_query($pre_date11);
        $fetch_pre_date11=mysql_fetch_array($exe_pre_date11);
        $paid_on_date_id=$fetch_pre_date11[0];
  $fee_receipt="SELECT * FROM fee_receipt_details 
                 INNER JOIN class
                 ON class.sId=fee_receipt_details.sid
                 WHERE date >=".$paid_on_date_id."
                 AND fee_receipt_details.cheque_status=0
                 AND class.classId=".$class." AND class.session_id=".$_SESSION['current_session_id']."";
    $exe_receipt=mysql_query($fee_receipt);
date_default_timezone_set('Asia/Kolkata');
$date=date('d-M-Y');
    echo'

    <div id="replace_collection">
    <div class="row-fluid">
    <!-- Table widget -->
    <div class="widget  span12 clearfix">
<h3 align="center" style="color:green">VIDYA BAL BHAWAN SR. SEC. SCHOOL</H4>
    <div class="widget-header">
    <h5 align="center" style="color:red">
    Fee Paid Students Of Class '.$fetch_class[0].'

    </h5>
    <h4 align="right"><b>Date : '.$date.'</b></h4>
    </div><!-- End widget-header -->	
    <div class="widget-content">

    <table  class="table table-bordered table-striped" id="dataTable"  >
    
    <tr style="color:green">
    <th>S. <br>No.</th>
    <th>Receipt<br> No.</th>

    <th>Admission<br> No.</th>

    <th>Student<br> Name</th>

    <th>Father`s<br> Name</th>

   <th>Cash/<br>Cheque </th>

    <th>Duration</th>

    <th>Date</th>
    ';
  // echo '  <th>Previous<br>Dues</th>';
//    echo '<th>Transport</th>
//    <th>Discount</th>';
    echo '

   
<th style="color:blue">Total</th>

    <th style="color:green">Paid</th>

    <th style="color:red">Dues</th>

    

    

    

    

    </tr>

  

    

     ';
    while($fetch_receipt=mysql_fetch_array($exe_receipt))
    {
        //STUDENT DETAILSS
        $student_details="SELECT * FROM student_user WHERE sId=".$fetch_receipt[2]."";
        $exe_student=mysql_query($student_details);
        $fetch_student=mysql_fetch_array($exe_student);
        //CLASS
        $class_name="SELECT class_name FROM class_index
            INNER JOIN class
            ON class.classId =class_index.cId
            WHERE class.sId = ".$fetch_receipt[2]."
            AND class.session_id= ".$session_id."";
        $exe_class=mysql_query($class_name);
        $fetch_class=mysql_fetch_array($exe_class);
        
        echo '<tr style="border:1;solid black; color:BLACK; font-size:12px;">';
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$srn++.'</td>';
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$fetch_receipt[0].'</td>';
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$fetch_student['admission_no'].'</td>';
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$fetch_student['Name'].'</a></td>';
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$fetch_student[5].'</td>';
       // echo '<td>'.$fetch_class['class_name'].'</td>';
         if($fetch_receipt['cheque_no'] == 0)
        {
          echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">Cash</td>';
        }
        else 
        {
            echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">Cheque</td>';
           // echo '<td>'.$fetch_receipt['cheque_no'].'</td>';
        }
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$fetch_receipt['duration_name'].'</td>';
        
        //date
        $date="SELECT date FROM dates_d WHERE date_id=".$fetch_receipt['date']."";
        $exe_date=mysql_query($date);
        $fetch_date=mysql_fetch_array($exe_date);
            $dts=explode('-',$fetch_date[0]);
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$dts[2].'-'.$dts[1].'-'.$dts[0].'</td>';
       //echo '<td>'.$fetch_receipt['pre_remaning_fee'].'</td>';
//        echo '<td>0</td>';
//        echo '<td>'.$fetch_receipt['discount'].'</td>';
       // echo '<td>'.$fetch_receipt['fine'].'</td>';
        //
        $total = $fetch_receipt['amounts'] + $fetch_receipt['due'] ;
        
        echo '<td style="color:green">'.$total.'</td>';
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$fetch_receipt['amounts'].'</td>';
        echo '<td style="border:1;solid black; color:BLACK; font-size:12px;">'.$fetch_receipt['due'].'</td>';
        
     $date_total=$date_total+$fetch_receipt['amounts'];
$date_due=$date_due+$fetch_receipt['due'];
      //  echo '<td>'.$fetch_receipt['bank'].'</td>';
       // echo '<td>'.$fetch_receipt['branch'].'</td>';
          // echo '<td style="color:red">';
//           echo '  <span class="tip"><a href="#" original-title="Edit">
//			<img src="images/icon/icon_edit.png"></a></span> ';

        echo '</tr>';
    }
	$ttr=$date_total+$date_due;
    echo'
   <tr style="color:red" align="center"><td colspan="8">Grand Total</td><td>'.$ttr.'</td><td>'.$date_total.'</td><td>'.$date_due.'</td></tr>
    
    </table><br><br>
    
        1: This list contains the students who have deposit their fee.<br>
        2: The `0` under the payment Heading means child belong to the EWS category.<br><br>
                                                <h5 align="right"><b>Class Teacher Name ....................................................................</b></h5>
                                                <h5 align="right"><b>Sign:........................................................................</b></h5>
    </div><!--  end widget-content -->
    </div><!-- widget  span12 clearfix-->
    </div><!-- row-fluid -->';   
}


function accounts_fee_transport_defaulter()
{
      
      //Author By: Anil Kumar *
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Fee Not Pay Students</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';
//    $duration="SELECT Id, duration FROM fee_generated_sessions
//	           WHERE session_id=".$_SESSION['current_session_id']."";
//	$exe_duration=mysql_query($duration);	
	echo'  
	<div class="load_page">
	<div class="formEl_b">	';
	//in this form level of defalters select 
	echo '
	<form id="validation_demo" action="accounts_fee_transport_defaulter_show.php" method="get"> ';
//	<div class="section ">
//	<label>Select Type <small></small></label>   
//	<div> 
//	<select name="level">
//	<option value="">---SELECT LEVEL---</option>
//	<option value="1">Pre Primary</option>
//	<option value="2"> Primary</option>
//	<option value="3">Secondary</option>
//	<option value="4">Senior Secondary</option>
//	<option value="5">Class Wise</option>
//	<option value="6">School Wise</option>
//	</select>
//	</div>
//	</div>
	echo '<div class="section">
	<label>Select Duration<small></small></label>
	<div>
	<select name="duration">
	<option value="">--SELECT Duration--</option>';
//	while($fetch_duration_id=mysql_fetch_array($exe_duration))
//	    {
//	        echo '<option value="'.$fetch_duration_id[0].'">'.$fetch_duration_id[1].'</option>';
//		}
	echo '<option value="1001" >April-June</option>
	      <option value="1002" >July-Sep</option>
	      <option value="1003">Oct-Dec</option>
		  <option value="1004">Jan-March</option>
	</select>
	</div>
	</div>';
        $cid="SELECT * FROM class_index ORDER BY order_by";
        $exe_cid=mysql_query($cid);
//        echo '<div class="section">
//	<label>Select Class<small></small></label>
//	<div>
//	<select name="cls">
//	<option value="">--SELECT Class--</option>';
//        
//    
//
//
//
////	while($fetch_duration_id=mysql_fetch_array($exe_duration))
////	    {
////	        echo '<option value="'.$fetch_duration_id[0].'">'.$fetch_duration_id[1].'</option>';
////		}
//        while($fetch_cid=mysql_fetch_array($exe_cid))
//        {
//            echo '<option value="'.$fetch_cid[0].'" >'.$fetch_cid[1].'</option>';
//        }  
//	echo '</select>
//	</div>
//	</div>';

  echo '<div class="section">
<label>SELECT ROUTE</label>
<div>
<select name="route">
<option value="">SELECT ROUTE</option>
';
//get all the routes from routes_data
$get_routes=
"SELECT main_route,route_id
FROM routes_data";
$exe_routes=mysql_query($get_routes);
while($fetch_routes=mysql_fetch_array($exe_routes))
{
echo '<option value="'.$fetch_routes[1].'" id="'.$fetch_routes[0].'">'.$fetch_routes[0].'</option>';	
	
}
echo '
</select>
</div>
</div>';


echo '<div class="section">

	<label>Select Type<small></small></label>
	<div>
	<select name="t">
	<option value="">--SELECT Type--</option>';
//	while($fetch_duration_id=mysql_fetch_array($exe_duration))
//	    {
//	        echo '<option value="'.$fetch_duration_id[0].'">'.$fetch_duration_id[1].'</option>';
//		}
	echo '<option value="1" >Paid List</option>

	      <option value="2" >Defaulter List</option>
	</select>

	</div>
	</div>';

  /*  echo ' <div class="section">
	       <div>OR </div></div>';
	echo '
	<div class="section">
	<label>Select Date</label>   
	<div><input type="text"  id="datepick" class="datepicker" readonly="readonly" name="date"  />
	</div>
	</div>';*/
	echo '<div class="section last">
	<div>
	<button class="btn submit_form">Submit</button>
	</div>
	</div>
	</form>
	</div>								
	</div>';
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
      
}

function accounts_fee_transport_defaulter_show()
{
      
       $dur=$_GET['duration'];
    $t=$_GET['t'];
      $route=$_GET['route'];
   /*  if($duration==1001)
     {
         $dur=1;
         $d="Apr-Jun";
     }
     elseif($duration==1002)
     {
         $dur=4;
          $d="Jul-Sep";
     }
       elseif($duration==1003)
     {
         $dur=7;
         $d="Oct-Dec";
     }
       elseif($duration==1004)
     {
         $dur=10;
         $d="Jan-March";
     }*/
//     $class="SELECT * FROM class_index WHERE cId=".$cid."";
//     $exe_cid=mysql_query($class);
//     $fetch_class=mysql_fetch_array($exe_cid);
date_default_timezone_set('Asia/Kolkata');
$date=date('d-M-Y');

if($t==1)
{
$route_detail="SELECT * FROM routes_data WHERE route_id=".$route."";
$exe_route=mysql_query($route_detail);
$fetch_route=mysql_fetch_array($exe_route);
        echo' <br><br>
			<div class="row-fluid">
			<!-- Table widget -->
			<div class="widget  span12 clearfix">
                        <h2 align="center" style="color:green">VIDYA BAL BHAWAN SR. SEC. SCHOOL</H2>
			<div class="widget-header">';
 
 // echo '   <h4 align="center" style="color:green"> Paid List of  ('.$d.')  of Class ( '.$fetch_class[1].' ) </H6>';
                         if($d_type==0)
{
                     echo '   <h4 align="center" style="color:green">Transport Fee  List of  (JULY-SEP)  of Route ( '.$fetch_route[1].' ) </H6>';
}
elseif($d_type==1)
{
  echo '   <h4 align="center" style="color:green">Slip Not Updated Defaulter List of  (JULY-SEP)  of Class ( '.$fetch_class[1].' ) </H6>';
}

			echo '<h4 align="right">Date : '.$date.'</h4>
			</div><!-- End widget-header -->	
			<div class="widget-content">';


			 $defa="SELECT * FROM fee_receipt_details_transport
                             
                                INNER JOIN user_vehicle_details
                                ON user_vehicle_details.uId=fee_receipt_details_transport.sid
                                  
INNER JOIN class

                                ON class.sId=fee_receipt_details_transport.sid

                               WHERE user_vehicle_details.session_id=".$_SESSION['current_session_id']." AND user_vehicle_details.route_id=".$route." AND class.session_id=".$_SESSION['current_session_id']." AND fee_receipt_details_transport.date>3830

ORDER BY receipt_id";
                         $exe_dur=mysql_query($defa);
                        
                       
			echo'                                   
			<table class="table table-bordered table-striped" id="dataTable">
			<thead>';
			echo'    
			<tr>
			<th width="5%">S.No.</th>
			<th width="5%">Receipt No.</th>
			<th  width="12%">Admission No.</th>
			<th width="12%">Name</th>
			<th width="12%">Class</th>
                        <th width="12%">Phone</th>
                        <th width="12%">Father`s Name</th>

			 <th width="12%">Date</th>
			 <th width="12%">Month</th>
 <th width="12%">Amounts</th>
			
			</thead>
			
			<tbody align="center">';
                        $s=0;
                        while($fetch_dur=mysql_fetch_array($exe_dur))
                        {
                            $student_id=$fetch_dur[2];
                            $du=$fetch_dur['duration_id'];
                            
                            
                            $amo_s="SELECT * FROM student_user WHERE sId=".$student_id." ";
                            $exe_du_s=mysql_query($amo_s);
                            $fetch_am_s=mysql_fetch_array($exe_du_s);
                            
                         $cl="SELECT class_name FROM class_index 
                                INNER JOIN class
                                ON class.classId=class_index.cId
                                WHERE class.sId=".$student_id." AND class.session_id=".$_SESSION['current_session_id']."";
                            $exe_cl=mysql_query($cl);
                          
                            $fetch_cl=mysql_fetch_array($exe_cl);
                            
                            echo '<tr>';
                               echo ' <td>'.++$s.'</td>';

echo ' <td>'. $fetch_dur[1].'</td>';
                               echo ' <td>'. $fetch_am_s['admission_no'].'</td>';
                               echo ' <td>'. $fetch_am_s['Name'].'</td>';
                              echo ' <td>'. $fetch_cl['class_name'].'</td>';
                               echo ' <td>'. $fetch_am_s['Phone No'].'</td>';
                                echo ' <td>'. $fetch_am_s[5].'</td>';

$dd="SELECT date FROM dates_d WHERE date_id=".$fetch_dur['date']."";
$exe_dd=mysql_query($dd);
$fetch_dd=mysql_fetch_array($exe_dd);

$dates=date("d-M-Y",strtotime($fetch_dd[0]));
echo ' <td>'. $dates.'</td>';
echo ' <td>'. $fetch_dur[9].'</td>';
echo ' <td>'. $fetch_dur['amounts'].'</td>';
                              // echo ' <td>'.$fetch_am[0].'</td>';
                               
                               echo '</tr>';
                        }
  
echo' </tbody>
   
	</table>
           ';
echo ' 
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}
else
{


	$route_detail="SELECT * FROM routes_data WHERE route_id=".$route."";
$exe_route=mysql_query($route_detail);
$fetch_route=mysql_fetch_array($exe_route);
        echo' <br><br>
			<div class="row-fluid">
			<!-- Table widget -->
			<div class="widget  span12 clearfix">
                       <h2 align="center" style="color:green">VIDYA BAL BHAWAN SR. SEC. SCHOOL</H2>
			<div class="widget-header">';
 
 // echo '   <h4 align="center" style="color:green"> Paid List of  ('.$d.')  of Class ( '.$fetch_class[1].' ) </H6>';
                         if($d_type==0)
{
                     echo '   <h4 align="center" style="color:green">Transport Fee Defaulter List of  (JULY-SEP)  of Route ( '.$fetch_route[1].' ) </H6>';
}
elseif($d_type==1)
{
  echo '   <h4 align="center" style="color:green">Slip Not Updated Defaulter List of  (JULY-SEP)  of Class ( '.$fetch_class[1].' ) </H6>';
}

			echo '<h4 align="right">Date : '.$date.'</h4>
			</div><!-- End widget-header -->	
			<div class="widget-content">';
			 $defa="SELECT * FROM user_vehicle_details
                                              
				INNER JOIN class

                                ON class.sId=user_vehicle_details.uId

                               WHERE user_vehicle_details.session_id=".$_SESSION['current_session_id']." 
				AND user_vehicle_details.route_id=".$route." 
				AND class.session_id=".$_SESSION['current_session_id']."

					AND user_vehicle_details.uId NOT IN
					(

						SELECT sid FROM fee_receipt_details_transport WHERE duration_value>4

					)


			        ";
                         $exe_dur=mysql_query($defa);
                        
                       
			echo'                                   
			<table class="table table-bordered table-striped" id="dataTable">
			<thead>';
			echo'    
			<tr>
			<th width="5%">S.No.</th>

			<th  width="12%">Admission No.</th>
			<th width="12%">Name</th>
			<th width="12%">Class</th>
                        <th width="12%">Phone</th>
                        <th width="12%">Father`s Name</th>
				 <th width="12%">Stop</th>

 			<th width="12%">Amounts</th>
			<th width="12%">Month</th>
			</thead>
			
			<tbody align="center">';
                        $s=0;



                        while($fetch_dur=mysql_fetch_array($exe_dur))
                        {
                            $student_id=$fetch_dur['uId'];
                          
                            $des="SELECT * FROM destination_route WHERE dId=".$fetch_dur['dId']."";
				$exe_des=mysql_query($des);
				$fetch_des=mysql_fetch_array($exe_des);
                            
                            $amo_s="SELECT * FROM student_user WHERE sId=".$student_id." ";
                            $exe_du_s=mysql_query($amo_s);
                            $fetch_am_s=mysql_fetch_array($exe_du_s);
                            
                         $cl="SELECT class_name FROM class_index 
                                INNER JOIN class
                                ON class.classId=class_index.cId
                                WHERE class.sId=".$student_id." AND class.session_id=".$_SESSION['current_session_id']."";
                            $exe_cl=mysql_query($cl);
                          
                            $fetch_cl=mysql_fetch_array($exe_cl);
 $data1="SELECT MAX(receipt_id) FROM fee_receipt_details_transport WHERE sid=".$student_id."";
$exe_data1=mysql_query($data1);
$fetch_data1=mysql_fetch_array($exe_data1);

 $data2="SELECT * FROM fee_receipt_details_transport WHERE receipt_id=".$fetch_data1[0]."";
$exe_data2=mysql_query($data2);
$fetch_data2=mysql_fetch_array($exe_data2);

 $int =6-$fetch_data2[10];
                            
                            echo '<tr>';
                               echo ' <td>'.++$s.'</td>';

                               echo ' <td>'. $fetch_am_s['admission_no'].'</td>';
                               echo ' <td>'. $fetch_am_s['Name'].'</td>';
                              echo ' <td>'. $fetch_cl['class_name'].'</td>';
                               echo ' <td>'. $fetch_am_s['Phone No'].'</td>';
                                echo ' <td>'. $fetch_am_s[5].'</td>';
echo ' <td>'. $fetch_des[1].'</td>';
$amount=$fetch_des[3]*$int;
echo ' <td>'.$amount.'</td>';
echo ' <td>'.$int.' - Month</td>';
                              // echo ' <td>'.$fetch_am[0].'</td>';
                               
                               echo '</tr>';
                        }
  
echo' </tbody>
   
	</table>
           ';
echo ' 
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';






}
}
?>


