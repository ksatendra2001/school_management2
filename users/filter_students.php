<?php
header('Content-Type: application/json');
require_once('../config/config.php');
require_once('../include/session.php');
require_once('../include/check.php');
$session_id=$_SESSION['current_session_id'];
$religion = $_GET['religion'];
$category = $_GET['category'];
$gender = $_GET['gender'];
$class = $_GET['class'];

$set_value = 0;
$query_value = '';

if($_GET['religion'] != '')
{
	$query_value = $query_value.'`religion` LIKE "'.$_GET['religion'].'"';
	$set_value = 1;
}
if($_GET['category'] != '')
{
	$query_value = $query_value.' AND `category` LIKE "'.$_GET['category'].'"';
}
if($_GET['gender'] != '')
{
	$query_value = $query_value.' AND `gender` LIKE "'.$_GET['gender'].'"';
}
if($_GET['class'] != '')
{
	$query_value = $query_value.' AND class_index.class_name LIKE "'.$_GET['class'].'"';
}

if($set_value == 0)
{
	$query_value_array = array();
	
	$ctr = 0;
	$query_value_array = explode(' ',$query_value);
	$query_value = ' ';
	foreach($query_value_array as $query_array)
	{
		if($ctr >= 2)
		{
			$query_value = $query_value.' '.$query_array;
		}
		++$ctr;
	}
}
//Query to get the data
$query_get_data = "
	SELECT `admission_no`,`Name`,`Father's Name`,`Mother's Name`,YEAR(CURDATE()) - YEAR(DOB) AS age, `gender`, `religion`, `category`, `Phone No`, class_index.class_name as class_name
	FROM `student_user`
	
	INNER JOIN `class`
	ON student_user.sId = class.sId
	
	INNER JOIN `class_index`
	ON class.classId = class_index.cId
	
	WHERE  
";
$query_get_data = $query_get_data.$query_value;
$query_get_data = $query_get_data.' ORDER BY age';
//echo $query_get_data;

$execute_get_data = mysql_query($query_get_data);
$data_array = array();
$ctr = 0;
while($get_data = mysql_fetch_array($execute_get_data))
{
	$data_array[$ctr]['admission_no'] = $get_data['admission_no'];
	$data_array[$ctr]['Name'] = $get_data['Name'];
	$data_array[$ctr]['f_name'] = $get_data['Father\'s Name'];
	$data_array[$ctr]['m_name'] = $get_data['Mother\'s Name'];
	$data_array[$ctr]['age'] = $get_data['age'];
	$data_array[$ctr]['gender'] = $get_data['gender'];
	$data_array[$ctr]['religion'] = $get_data['religion'];
	$data_array[$ctr]['category'] = $get_data['category'];
		$data_array[$ctr]['phone'] = $get_data['Phone No'];
	$data_array[$ctr]['class_name'] = $get_data['class_name'];
	++$ctr;
}

echo json_encode($data_array);

?>
