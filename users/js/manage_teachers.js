// JavaScript Document

function show_edit(id)
{
	//Function to show the edit symbol
	var ele = document.getElementById(id);
	ele.style.visibility = "visible";
}

function hide_edit(id)
{
	//Function to hide the edit symbol
	var ele = document.getElementById(id);
	ele.style.visibility = "hidden";
}

function change_edit_symbol_over(id)
{
	//Function to change the edit symbol
	var ele = document.getElementById(id);
	ele.src = "img/edit-over.gif";
}

function change_edit_symbol_out(id)
{
	//Function to change the edit symbol
	var ele = document.getElementById(id);
	ele.src = "img/edit.gif";
}

function redirect_sybmbol(id,teacher_name,teacher_id)
{
	//Function to redicrect to view Class Subject relation
	var url = "manage_users_show_class_subject_teacher.php?teacher_name="+teacher_name+"&teacher_id="+teacher_id;
	window.location.href = url;
}

function redirect_edit_class_teacher(id)
{
	var url = "manage_users_edit_class_subject_teacher.php?id="+id;
	window.location.href = url;
}

function delete_teacher(id)
{
	if(confirm("Do you want to delete the user"))
	{
		window.location.href = "users/delete_user_teacher.php?id="+id;
	}
}