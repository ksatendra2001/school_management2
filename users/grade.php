<?php

// function to calculate the 
function calculate_grade($m)
{
   $grade = '';
   if($m==5)
   {
     $grade = 'A';	
   }
 else if($m==4)
    {
      $grade = 'B';
    }
    else if($m==3)
    {
      $grade = 'C';
    }
    else if($m==2)
    {
      $grade = 'D';
    }
    else if($m==1)
    {
      $grade = 'E';
    }
   
    return $grade;
}


// function to calculate the final grade of the student
function calculate_total_grade($m)
{
   $grade = '';
 if($m<=100 && $m > 90)
   {
     $grade = 'A1';	
   }
 else if($m<=90 && $m > 80)
    {
      $grade = 'A2';
    }
 else if($m<=80 && $m > 70)
    {
      $grade = 'B1';
    }
 else if($m<=70 && $m > 60)
    {
      $grade = 'B2';
    }
 else if($m<=60 && $m > 50)
    {
      $grade = 'C1';
    }
 else if($m<=50 && $m > 40)
    {
      $grade = 'C2';
    }
 else if($m<=40 && $m > 30)
    {
      $grade = 'D';
    }
    else if($m<=30 && $m >20)
    {
      $grade = 'E1';
    }
 else if($m<=20 && $m >=0)
    {
        $grade = 'E2';
     }
    return $grade;
}

// function to calculate the the skill grade
function calculate_grade_point($m)
{
   $grade = '';
 if($m>=10 && $m > 9)
   {
     $grade = 'A';	
   }
 else if($m<=9 && $m > 8)
   {
      $grade = 'B';
   }
 else if($m<=8 && $m > 7)
    {
      $grade = 'C';
    }
 else if($m<=7 && $m > 6)
    {
      $grade = 'D';
    }
 else 
    {
      $grade = 'E';
    }
    return $grade;
}

?>