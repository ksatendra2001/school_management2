<?php
require_once('../include/functions_dashboard.php');
require_once('../config/config.php');
require_once('../include/session.php');
require_once('../include/check.php');


//This function separates the extension from the rest of the file name and returns it 
function findexts ($filename) 
{ 
	$filename = strtolower($filename) ; 
	$exts = split("[/\\.]", $filename) ; 
	$n = count($exts)-1; 
	$exts = $exts[$n]; 
	return $exts; 
} 

//This will check whether the username eneterd exists or not
function check_username($username_check)
{
	$queryUsername = mysql_query("SELECT * FROM `login` WHERE `username` = '$username_check' AND `privilege` = '3'");
	$username_exists = mysql_affected_rows();	//Assigns the number of rows affected
	
	if($username_exists == 0)
	{
		return $username_check = 0;					//If username doesnot exists
	}
	else
	{
		return $username_check = 1;					//If username exists
	}
}




/*//get admission_no
$get_admission_no="SELECT *
                   FROM student_user
				   WHERE admission_no='".$_POST['admission_no']."'";
$exe_admission_no=mysql_query($get_admission_no);
$fetch_admission_no=mysql_fetch_array($exe_admission_no);
$add_no=$fetch_admission_no['admission_no'];
if($add_no==$admission_no)
{
	header("Location: ../manage_user_add_new_student.php?addmissionexist");
}

else
{*/
//Getting all the details from the 
$admission_no = $_POST['admission_no'];
$name = $_POST['name'];
$username = $_POST['username'];
$password = $_POST['password'];
$confirm_password = $_POST['confirm_password'];
$gender = $_POST['gender'];
$dob = $_POST['dob'];
$email_id = $_POST['email_id'];
$phone_no = $_POST['phone_no'];
$students_class = $_POST['students_class'];
$father_name = $_POST['father_name'];
$mothers_name = $_POST['mothers_name'];
$guardians_email = $_POST['guardians_email'];
$local_address = $_POST['local_address'];
$blood_group = $_POST['blood_group'];
$picture = $_FILES['image']['name'];
$category=$_POST['category'];
$religion=$_POST['religion'];
$ews=$_POST['ews'];
$doa=$_POST['doa'];
$father_occupation = $_POST['father_occupation'];
$father_annual_income = $_POST['father_annual_income'];
$mothers_occupation = $_POST['mothers_occupation'];
$mothers_annual_income = $_POST['mothers_annual_income'];

//echo $name.' '.$admission_no.' '.$username.' '.$password.' '.$confirm_password.' '.$dob.' '.$email_id.' '.$phone_no.' '.$blood_group.' '.$gender.' '.$father_name.' '.$mothers_name.' '.$local_address.' '.$guardians_email.' '.$picture.''.$students_class;

//Check whether the username exists or not
$username_check = check_username($username);

$username_check; 	//If this variable is 1 then the username exists, else it doesnot

if($username_check == 1)
{
	header("Location: ../manage_user_add_new_student.php?usernameExists");
}

//This applies function to find the extension of the file 
$ext = findexts ($_FILES['image']['name']) ;

if($password != $confirm_password)
{
	//echo 'Error Password Mismatch';
	header("Location: ../manage_user_add_new_student.php?passwordMismatch");
}

else
{
	//This line assigns a timestamp to the name
	$ran = time();
	

	//This takes the random number (or timestamp) you generated and adds a . on the end, so it is ready of the file 	extension to be appended.
	$ran2 = $ran.$name.".";

	//This assigns the subdirectory you want to save into... make sure it exists!
	$target = "../user_image/";
	//This combines the directory, the random file name, and the extension
	$target = $target.$ran2.$ext; 
 
	if(move_uploaded_file($_FILES['image']['tmp_name'], $target)) 
	{
		$image_upload_name = $ran2.$ext;
		//Query to insert in the Users table
		
		 $name.' '.$admission_no.' '.$username.' '.$password.' '.$confirm_password.' '.$dob.' '.$email_id.' '.$phone_no.' '.$blood_group.' '.$category.' '.$gender.' '.$father_name.' '.$mothers_name.' '.$local_address.' '.$guardians_email.' '.$picture.''.$students_class ;
		
		
		 $query = "
		INSERT INTO `student_user` 
		(`Name`,`Email`,`DOB`,`Phone No`,`Father's Name`,`Mother's Name`,`Local Address`,`Guardian's Email`,`admission_no`,`gender`,`religion`,`category`,`Blood Group`,`image`,`father_occupation`,`father_annual_income`,`mothers_occupation`,`mothers_annual_income`,`admission_date`,`ews`) 
		VALUES
		('$name','$email_id','$dob','$phone_no','$father_name','$mothers_name','$local_address','$guardians_email','$admission_no','$gender','$religion','$category','$blood_group','$image_upload_name','$father_occupation','$father_annual_income','$mothers_occupation','$mothers_annual_income','$doa','$ews')";
		
		$query_users = mysql_query($query);
		
		//Retrieve the uid of the user details entered
		
		$query_uid = mysql_query("SELECT `sId` from `student_user` WHERE `image` = '$image_upload_name'");
		$fetch_uid = mysql_fetch_array($query_uid);
		$sId = $fetch_uid[0];
		
		$password = md5($password);	//To encrypt the password
		
		//Query to insert in the login table
		$query_login = mysql_query("INSERT INTO `login` (`username`,`password`,`lId`,`privilege`,`status`) VALUES ('$username','$password','$sId','3','0')");
		
		//To generate username and password for parent login
		$parent_login_username = 'parent_'.$username;
		
		$query_login_parent = mysql_query("INSERT INTO `login` (`username`,`password`,`lId`,`privilege`,`status`) VALUES ('$parent_login_username','$password','$sId','4','0')");
		
		//Now insert the student in the class in the current session ID, get current session ID from session table
		
		$query_get_session_id = "SELECT `sId` FROM `session_table` WHERE `current_session` = 1";
		$execute_session_id = mysql_query($query_get_session_id);
		$session_id  = mysql_fetch_array($execute_session_id);
		$session_id = $session_id[0];
				
		$queryInsertStudentToClass = mysql_query("INSERT INTO `class` (`sId`,`classId`,`session_id`) VALUES ('$sId','$students_class','$session_id')");
		
		
		//echo "The file has been uploaded as ".$ran2.$ext;
		header("Location: ../manage_user_add_new_student.php?studentAdded");
		
	}
	else
	{
		//Error in uploading the image
		
		header("Location: ../manage_user_add_new_student.php?ERROR");
		
	}
	
}



?>
