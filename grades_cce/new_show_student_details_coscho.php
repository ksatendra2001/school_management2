<?php
require_once('include/functions_dashboard.php');
require_once('config/config.php');
require_once('include/session.php');
require_once('include/userdetail.php');
require_once('include/check.php');
require_once('include/grades_cce.php');

logged_in();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <title>School Management System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon.ico"/> 

        <!-- CSS Stylesheet-->
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap-responsive.css" />
        <link type="text/css" rel="stylesheet" href="css/zice.style.css"/>

		
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->  
       <script type="text/javascript" src="grades_cce/js/cce.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.ui.min.js"></script> 
       <script type="text/javascript" src="components/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="components/ui/timepicker.js"></script>
        <script type="text/javascript" src="components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="components/form/form.js"></script>
        <script type="text/javascript" src="components/elfinder/js/elfinder.full.js"></script>
        <script type="text/javascript" src="components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="components/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="components/jscrollpane/jscrollpane.min.js"></script>
        <script type="text/javascript" src="components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="components/chosen/chosen.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="components/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="components/flot/flot.js"></script>
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>       
		<script type="text/javascript" src="components/Jcrop/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="components/smartWizard/jquery.smartWizard.min.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/zice.custom.js"></script>
		 


<script type="text/javascript">
$(document).ready(function(){
  $("#flip").click(function(){
    $("#panel").slideToggle();
	$("#panel1").slideUp();
	$("#panel2").slideUp();
	$("#panel3").slideUp();
	
  });
});
</script>

<script type="text/javascript">
$(document).ready(function(){
  $("#flip1").click(function(){
    $("#panel1").slideToggle();
	$("#panel").slideUp();
	$("#panel2").slideUp();
	$("#panel3").slideUp();
  });
});
</script>

<script type="text/javascript">
$(document).ready(function(){
  $("#flip2").click(function(){
    $("#panel2").slideToggle();
	$("#panel1").slideUp();
	$("#panel3").slideUp();
	$("#panel").slideUp();
  });
});
</script>

<script type="text/javascript">
$(document).ready(function(){
  $("#flip3").click(function(){
    $("#panel3").slideToggle();
    $("#panel2").slideUp(); 
	$("#panel1").slideUp(); 
	$("#panel").slideUp(); 
   
	
  });
});
</script>


<style type="text/css"> 
#panel,#flip
{

text-align:center;

}
#panel
{
padding:30px;
display:none;
}
</style>

<style type="text/css"> 
#panel1,#flip1
{

text-align:center;

}
#panel1
{
padding:30px;
display:none;
}
</style>

<style type="text/css"> 
#panel2,#flip2
{

text-align:center;

}
#panel2
{
padding:30px;
display:none;
}
</style>

<style type="text/css"> 
#panel3,#flip3
{

text-align:center;

}
#panel3
{
padding:30px;
display:none;
}
</style>


		</head>        
        <body>        
<div id="header">
<?php

 top_header();//function for calling header in function_admin.php

?>
</div>


<div id="left_menu">
<?php

 left_menu();//function for calling left menu in function_admin.php

?>
 </div>

<div id="content">
<div class="inner">
      
<?php
 top_menu();//function for calling top menu in function_admin.php
?>



<?php
 	

$cosho_area_id=$_GET['area_id'];
$class_id=$_GET['class'];
$coscho_id=$_GET['coscho_id'];
$term=$_GET['term'];
$test=$_GET['test'];

 $get_class=
          "SELECT *
          FROM class_index
          WHERE cId=$class_id
         ";
          $exe_get_class=mysql_query($get_class);
         $fetch_classes=mysql_fetch_array($exe_get_class);
$level=$fetch_classes['level'];
$clas_name=$fetch_classes['class_2'];



 
                    echo '

<table class="table table-bordered table-striped"  border="2" width="100%"  
<thead >
<tr>
<th rowspan="2" width=10%>Admission No.</th>
</tr>
<tr>
<th  align="center">Name</th>
<th>Grade</th>
<th>Discriptive Indicator</th>

</tr>

</thead>';

    echo '
           <tbody align="center">';
           //get student names on the class id
   
   $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$class_id."'
                AND session_id='".$_SESSION['current_session_id']."' ORDER BY  student_user.Name ASC";
                $exe=mysql_query($query);
                     while($fetch_students = mysql_fetch_array($exe))
                     {         $student_id=$fetch_students['sId'];
                     
                      $get_details_for_update=
"SELECT *
FROM new_cce_coscho_indicator_marks_table
WHERE coscho_id = ".$coscho_id."
AND student_id = ".$student_id."
AND area_id = ".$cosho_area_id."
    AND class_id = ".$class_id."
AND session_id = ".$_SESSION['current_session_id']." AND term_id = ".$term."
    AND test_id = ".$test." ";
$exe_details=mysql_query($get_details_for_update);
$fetch_details=mysql_fetch_array($exe_details);
    $grades=$fetch_details['grade'];   
    $dis_indicator=$fetch_details['indicator_name'];         
                     
                 


                    
                  echo'
 <form id="validation_demo" action="grades_cce/new_grade_cce_insert_coscho_indicator_marks.php" method="get"> ';


             echo' 
	<input type="hidden" name="student_id" value="'.$student_id.'">
	<input type="hidden" name="class" value="'.$class_id.'">
	<input type="hidden" name="term" value="'.$term.'">
	<input type="hidden" name="test" value="'.$test.'">
	<input type="hidden" name="cosho_area_id" value="'.$cosho_area_id.'">


  <tr>
                    <td>'.$fetch_students['admission_no'].'</td>
                    <td align="left" width="30%">'.$fetch_students['Name'].'</td>';

             
     echo'
                
                    
             <td align="center">
<select  name="grade" id="type_coscho1"  style="width:100px;">
 <option value="'.$grades.'">'.$grades.'</option>
<option value="A+">A+</option>
<option value="A">A</option>
<option value="B+">B+</option>
<option value="B">B</option>
<option value="C">C</option>
<option value="D">D</option>
<option value="E">E</option>
</select></td><td align="center">


<select  name="discriptive_indicator" id="type_coscho1"  style="width:350px;">
 <option value="'.$dis_indicator.'">'.$dis_indicator.'</option>';


$query_get_indicator="SELECT * FROM new_grade_cce_new_indicators WHERE cosho_area_id=".$cosho_area_id." AND level_2=4";
$exe_query_indi=mysql_query($query_get_indicator);
while($get_indicator=mysql_fetch_array($exe_query_indi))
{
$indicator_new=$get_indicator['indicator_name'];


            echo' 
<option value="'.$indicator_new.'">'.$indicator_new.'</option>';

}


echo'

</select></td>

<td><button class="btn submit_form" >Submit</button></
td>
</tr>

<span id="type_coscho1"></span>
<span id="student_coscho1"></span>
</form>

	  '; 



                     }     

                     
                     echo'</tbody></table>


';




?>




<?php
 footer(); //function for calling footer in function_admin.php
?>       

 <script type="text/javascript">
$("#grades_cce").addClass("select");
</script>  
        </body>
      </html>
