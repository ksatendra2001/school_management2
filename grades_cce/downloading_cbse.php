<?php
require_once('../include/session.php');
require_once('../include/check.php');
require_once('../include/connect.php');
require_once('../include/userdetails.php');

//query to select all fields from scho_fields
/*$query_get_fields=
"SELECT * 
FROM scho_fields";
$query_exe=mysql_query($query_get_fields);

 while($fetch_fields=mysql_fetch_array($query_exe))
{
	
}*/
    function cleanData(&$str)
  {
    if($str == 't') $str = 'TRUE';
    if($str == 'f') $str = 'FALSE';
    if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
      $str = "'$str";
    }
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    $str = mb_convert_encoding($str, 'UTF-16LE', 'UTF-8');
  }

  // filename for download
  $filename = "website_data_" . date('Ymd') . ".csv";

  header("Content-Disposition: attachment; filename=\"$filename\"");
  header("Content-Type: text/csv; charset=UTF-16LE");

  $out = fopen("php://output", 'w');

  $flag = false;
  $result = mysql_query("SELECT * FROM subject") or die('Query failed!');
  while(false !== ($row = mysql_fetch_array($result))) {
    if(!$flag) {
      // display field/column names as first row
      fputcsv($out, array_keys($row), ',', '"');
      $flag = true;
    }
    array_walk($row, 'cleanData');
    fputcsv($out, array_values($row), ',', '"');
  }

  fclose($out);
  exit;

?>