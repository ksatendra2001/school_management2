<?php
require_once('include/functions_dashboard.php');
require_once('config/config.php');
require_once('include/session.php');
require_once('include/userdetail.php');
require_once('include/check.php');
require_once('include/grades_cce.php');

logged_in();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <title>School Management System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon.ico"/> 

        <!-- CSS Stylesheet-->
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap-responsive.css" />
        <link type="text/css" rel="stylesheet" href="css/zice.style.css"/>

		
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->  
		
        <script type="text/javascript" src="js/jquery.min.js"></script>
        
        <script type="text/javascript" src="js/zice.custom.js"></script>
        <script>
  function select_stu(class_id,testId,subject_id)
{       
   
   
   exam_type_id=$("#exam_type_id").val();
   

$.ajax({
		type: 'GET',
		
		url: 'grades_cce/grade_cce_xi_xii_stu_insert_marks.php',
		data: {exam_type_id:exam_type_id,class_id:class_id,testId:testId,subject_id:subject_id},
		success: function(data) {
			$('#show_stu').html(data);
                     
		}
		});	

   
}
           
           
           
           
           function insert_marks(student_id,subject_id,class_id,test_id,exam_type_id)
{       //alert(student_id);
   
    
   marks=$("#marks_"+student_id).val();
   //alert(marks);

$.ajax({
		type: 'GET',
		
		url: 'grades_cce/grade_cce_xi_xii_marks.php',
		data: {student_id:student_id,subject_id:subject_id,marks:marks,class_id:class_id,test_id:test_id,exam_type_id:exam_type_id},
		success: function(data) {
			$('#marks_').html(data);
                    
		}
		});	

    if(data == 1)
                 {
                            //If the update is successful
                            $(id).css("color","GREEN");
                            $(id).css("border-color","GREEN");
                 }
               else{
                         //Error in updating the details
                            $(id).css("color","#953b39");
                            $(id).css("border-color","red");
                   }
}

           </script>
<script type="text/javascript" src="grades_cce/js/cce.js"></script>
        
     
		</head>        
        <body>        
<div id="header">
<?php

 top_header();//function for calling header in function_admin.php

?>
</div>


<div id="left_menu">
<?php

 left_menu();//function for calling left menu in function_admin.php

?>
 </div>

<div id="content">
<div class="inner">
      
<?php
 top_menu();//function for calling top menu in function_admin.php
?>



<?php
new_grade_cce_add_marks_xi_xii_student();//function for calling dashboard in function_admin.php
?>




<?php
 footer(); //function for calling footer in function_admin.php
?>       

 <script type="text/javascript">
$("#grades_cce").addClass("select");
</script>  
        </body>
      </html>
