<?php

header( 'Content-Type: text/html; charset=utf-8' ); 
require_once('../include/session.php');
require_once('../include/check.php');
require_once('../config/config.php');
require_once('../include/userdetail.php');
 $class_id=$_GET['class_id'];
$student_id=$_GET['student_id'];



// //////////////////********************** query to get the marks and store its in a array for class 1 *********

$array_report = array();
$i=0;
// query to get the subject on the basis of class.
$get_subject = "SELECT DISTINCT subject_id FROM cce_subject_type_skills_id_table WHERE class_id=".$class_id."";
$exe_subject = mysql_query($get_subject);
while($fetch_subject= mysql_fetch_array($exe_subject))
{
   // query to get the subject name
   $get_subject_name = "SELECT `subject` FROM subject WHERE subject_id=".$fetch_subject['subject_id']."";
   $exe_subject_name = mysql_query($get_subject_name);
   $fetch_subject_name = mysql_fetch_array($exe_subject_name);
   $report[$i]['subject'] = $fetch_subject_name['subject'];
   
       //query to get the type_id
         $get_type_id = "SELECT DISTINCT type_id FROM cce_subject_type_skills_id_table
         WHERE cce_subject_type_skills_id_table.subject_id = ".$fetch_subject['subject_id']."
         AND cce_subject_type_skills_id_table.class_id = ".$class_id."";
   
        $type =array();
        $j=0;
        $exe_type_id = mysql_query($get_type_id);
        while($fetch_type_id = mysql_fetch_array($exe_type_id))
         {
            //query to get the type_id
            $get_type_name = "SELECT * 
            FROM cce_types_activity_table
            WHERE type_id = ".$fetch_type_id['type_id']."";
            $exe_type_name = mysql_query($get_type_name);
            $fetch_type_name =mysql_fetch_array($exe_type_name);
            
           // $type[$j]['type_name'] = $fetch_type['type_name'];

            // query to get the skill name
            $get_skill_id = "SELECT skill_id,sts_id FROM cce_subject_type_skills_id_table 
            WHERE type_id = ".$fetch_type_id['type_id']." 
            AND subject_id = ".$fetch_subject['subject_id']."
            AND class_id = ".$class_id."" ;
            
            $exe_skill_id = mysql_query($get_skill_id);
            while($fetch_skill_id = mysql_fetch_array($exe_skill_id))
            {
            $sts_id = $fetch_skill_id['sts_id'];
            $skill = array();
            $k = 0;
            // query to get the get the marks of first_term c.t-1
             $get_marks_t1_ct1 = "SELECT marks FROM cce_marks_scho_type_skills_table
             WHERE student_id = ".$student_id." AND sts_id = ".$sts_id." AND term_test_id = 1";
             $exe_marks_t1_ct1 = mysql_query($get_marks_t1_ct1);
             $fetch_markd_t1_ct1 = mysql_fetch_array($exe_marks_t1_ct1);
             $marks_t1_ct1 = $fetch_markd_t1_ct1['marks'];
             
             // query to get the get the marks of first_term c.t-2
             $get_marks_t1_ct2 = "SELECT marks FROM cce_marks_scho_type_skills_table
             WHERE student_id = ".$student_id." AND sts_id = ".$sts_id." AND term_test_id = 2";
             $exe_marks_t1_ct2 = mysql_query($get_marks_t1_ct2);
             $fetch_markd_t1_ct2 = mysql_fetch_array($exe_marks_t1_ct2);
             $marks_t1_ct2 = $fetch_markd_t1_ct2['marks'];
             
             $total = $marks_t1_ct2 + $marks_t1_ct1; //echo '<br>';
             //$grd = $total/2;
             $max_marks=0;
             //$grade = calculate_grade($total); // function to calculate and return the grade 
            if(!($fetch_skill_id['skill_id']))
            {
               $remarks = "";
               // query to get the remarks from cce_scho_activity_remarks_table
               $get_remarks = "SELECT cce_scho_remarks_table.remarks FROM cce_scho_remarks_table
                       INNER JOIN cce_scho_activity_remarks_table
                       ON cce_scho_remarks_table.id = cce_scho_activity_remarks_table.remarks_id
                       WHERE cce_scho_activity_remarks_table.subject_id = ".$fetch_subject['subject_id']."
                       AND cce_scho_activity_remarks_table.activity_id = ".$fetch_type_id['type_id']." 
                       AND cce_scho_activity_remarks_table.class_id = ".$class_id." " ;
             $exe_remarks = mysql_query($get_remarks);  
             $fetch_remarks = mysql_fetch_array($exe_remarks);
             
             $remarks =  $fetch_remarks['remarks'];
              
              $subject_name = $fetch_subject_name['subject'];
              $type_name = $fetch_type_name['type_name'];
              $skill_name = $fetch_skill_details['skill_name'];
              $total_marks='';
               if($fetch_type_id['type_id'] == 1)  
                   {
                     
                     $max_marks = ($total*2);
                     $total_marks = $total;
                     $array_report[$subject_name][$type_name][$type_name]['t1']['total'] = $total_marks;
                   }
                   else
                   {
                     // echo $fetch_type_id['type_id']; 
                     $max_marks = (($total*100)/10);// echo '<br>';
                     $total_marks1 = calculate_total_grade($max_marks);
                     $array_report[$subject_name][$type_name][$type_name]['t1']['total'] = $total_marks1;
                   }
              
             
              
               //$skill_name = "Written assessment";
              $array_report[$subject_name][$type_name][$type_name]['t1']['ct1']['marks'] = $marks_t1_ct1;
              $array_report[$subject_name][$type_name][$type_name]['t1']['ct2']['marks'] = $marks_t1_ct2;
             
              $array_report[$subject_name][$type_name][$type_name]['t1']['grade'] = calculate_total_grade($max_marks);
              $array_report[$subject_name][$type_name][$type_name]['t1']['remarks'] = $remarks;
            }
            else
            {
               $remarks = "";
              //query to get the remarks from cce_scho_activity_remarks_table
               $get_remarks = "SELECT cce_scho_remarks_table.remarks FROM cce_scho_remarks_table
                       INNER JOIN cce_scho_activity_remarks_table
                       ON cce_scho_remarks_table.id = cce_scho_activity_remarks_table.remarks_id
                       WHERE cce_scho_activity_remarks_table.subject_id = ".$fetch_subject['subject_id']."
                       AND cce_scho_activity_remarks_table.activity_id = ".$fetch_type_id['type_id']." 
                       AND cce_scho_activity_remarks_table.skill_id = ".$fetch_skill_id['skill_id']." 
                       AND cce_scho_activity_remarks_table.class_id = ".$class_id." " ;
               $exe_remarks = mysql_query($get_remarks);  
               $fetch_remarks = mysql_fetch_array($exe_remarks);
                       
              $remarks =  $fetch_remarks['remarks'];
            // query to get the skill details
            $get_skill_details ="SELECT * FROM cce_name_skills_table
            WHERE skill_id = ".$fetch_skill_id['skill_id']." ";
            $exe_skill_details = mysql_query($get_skill_details);
            while($fetch_skill_details = mysql_fetch_array($exe_skill_details))
            { 
             
              // echo $total*100/10; echo '<br>';
               $max_marks = ($total*100)/5;
               $max_marks1 = (($total*100)/10);
               //$skill[$k]['skill'] = $fetch_skill_details['skill_name'];
               $subject_name = $fetch_subject_name['subject'];
               $type_name = $fetch_type_name['type_name'];
               $skill_name = $fetch_skill_details['skill_name'];
               $array_report[$subject_name][$type_name][$skill_name]['t1']['ct1']['marks'] = calculate_grade($marks_t1_ct1);
               $array_report[$subject_name][$type_name][$skill_name]['t1']['ct2']['marks'] = calculate_grade($marks_t1_ct2);
               $array_report[$subject_name][$type_name][$skill_name]['t1']['total'] = calculate_total_grade($max_marks1);
               $array_report[$subject_name][$type_name][$skill_name]['t1']['grade'] = calculate_total_grade($max_marks1);
               $array_report[$subject_name][$type_name][$skill_name]['t1']['remarks'] = $remarks;
               //$grades = calculate_grade($grd);
              }
             }
            }
         }
    
}

//print_r($array_report);
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['ct1']['marks']; echo'<br>';
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'];echo'<br>';
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['total'];echo'<br>';
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['grade'];echo'<br>';

// query  that is use to access the co-corricular activity parts from cce_co_scho_marks_table table of database on the basic of term_id, class_id and activity_id and skill_id.
 $co_curricular = array();
 
//query to get the activity
   $get_co_curricular_act = "SELECT id,co_activity_name FROM cce_co_scho_activity_table WHERE term=1";
   $exe_co_curricular_act = mysql_query($get_co_curricular_act);
   while($fetch_co_curricular_act = mysql_fetch_array($exe_co_curricular_act))
   {
      $activity = $fetch_co_curricular_act['co_activity_name'];
      //query to get the skill from cce_co_scho_area_table table
       $get_co_curricular_skill = "SELECT id,co_skill FROM cce_co_scho_area_table WHERE co_activity_id=".$fetch_co_curricular_act['id']." ";
       $exe_co_curricular_skill = mysql_query($get_co_curricular_skill);
       $num_rows = mysql_num_rows($exe_co_curricular_skill);
       if($num_rows>0)
       {
       while($fetch_co_curricular_skill = mysql_fetch_array($exe_co_curricular_skill))
             {
              $skill = $fetch_co_curricular_skill['co_skill'];
              //query to get the marks from cce_co_scho_marks_table table
               $get_co_curricular_marks = "SELECT grade FROM cce_co_scho_marks_table WHERE student_id = $student_id AND class_id = $class_id AND term_id = 1 AND activity_id = ".$fetch_co_curricular_act['id']." AND skill_id =".$fetch_co_curricular_skill['id']." ";
               
               $exe_co_curricular_marks = mysql_query($get_co_curricular_marks);
               $fetch_co_curricular_marks = mysql_fetch_array($exe_co_curricular_marks);
               $fetch_co_curricular_marks['grade'];
               $co_curricular[$activity][$skill]['g'] = $fetch_co_curricular_marks['grade'];
               
             }
       }
       else
       {
           //query to get the marks from cce_co_scho_marks_table table
            $get_co_curricular_marks0 = "SELECT grade FROM cce_co_scho_marks_table WHERE student_id = $student_id AND class_id = $class_id AND term_id = 1 AND activity_id = ".$fetch_co_curricular_act['id']." AND skill_id = 0 ";
            
             $exe_co_curricular_marks0 = mysql_query($get_co_curricular_marks0);
             $fetch_co_curricular_marks0 = mysql_fetch_array($exe_co_curricular_marks0);
             
             $co_curricular[$activity][$activity]['g'] = $fetch_co_curricular_marks0['grade'];
       }
       
       
   }

//print_r($co_curricular);







?>

<style>
table,th,td,tr
{
border:1px solid black;
font-size: 12px;
}
</style>
<?php
   if($class_id==7)
 {
 echo '<p style="z-index:1; position:absolute; top:3px; left:75px"><font size="6%" style="font-size:33px"><strong>VIDYA BAL BHAWAN SR. SEC. SCHOOL </strong></font></p>';
    echo'
<table  width="100%" style="border:0px">
<tbody ><tr><td>
<table  width="100%" style="border:0px">
<tbody style="border:0px">
<tr style="border:0px">
<td align="left" width="25%" style="border:0px"></td>
</tr>
<tr style="border:0px">';echo'<td align="left" style="border:0px"position:absolute; top:0px; ><img src="../images/logo/School.jpg" width=35% ></td>';
echo'<br><div ><td  valign="top" width="150%" style="margin-left:-2px; width:100%;border:0px">
<font size="3" style="margin-left:70px; width:100%;" style="font-size:20px"><br>
      Kondli Gharoli Road, Mayur Vihar III, Delhi -110096 </font>
      <font size="4" style="margin-left:20px; width:100%;" style="font-size:20px"></font><br>
    <b style="margin-left:-30px; width:100%;"><font size="4" style="font-size:18px">Record of Academic Performance Term-I (2014-2015)</b>  </font> </td>
</tr>
</tbody>
</table>';


//get the name of the student on that id
$subject_id=0;
$get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB,`Blood Group`
FROM student_user
WHERE sId = ".$_GET['student_id']."";
$exe_s_name=mysql_query($get_s_name);
$fetch_s_name=mysql_fetch_array($exe_s_name);
$name_student=$fetch_s_name[0];
$blood=$fetch_s_name['Blood Group'];

// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.*
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.sId = ".$_GET['student_id']."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 $class_name=$fetch_cls_name['class'];
        $section_name=$fetch_cls_name['section'];
         
echo '
         
<div class="row-fluid">
<div class="widget  span12 clearfix">
<div class="widget-header">
<table  width="100%" id=8   valign="top" class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<tbody >
<tr width="30%" style="border:0px solid black;border-collapse:collapse;">
<td align="left" style="border:0px solid black;border-collapse:collapse;"><font size="" style="font-size:15px">Student Name: </font><b  style="font-size:13px"> '.$name_student.'</b></td>
<td align="left"  style="border:0px solid black;border-collapse:collapse;"><font size="" style="font-size:15px">Class :</font><b  style="font-size:13px" width="25%"> '.$class_name.'       '.$section_name.' </b><font size="" style="font-size:15px;margin-left:30px; width:100%">Admission No.:<b style="margin-left:10px; width:100%;"  style="font-size:13px" >'.$fetch_s_name[3].'    </b></font></td>
<td align="left" style="border:0px solid black;border-collapse:collapse;"><font size="" style="font-size:15px">Examination Roll.No:<b  style="font-size:13px" ></td>
</tr>
<tr width="30%" style="border:0px solid black;border-collapse:collapse;">
<td align="left" style="border:0px solid black;border-collapse:collapse;" ><font size="" style="font-size:15px">Father`s Name: <b style="font-size:13px">'.$fetch_s_name[1].'</b></font></td>
<td align="left"width="30" style="border:0px solid black;border-collapse:collapse;" ><font size="" style="font-size:15px">Mother`s Name:<b style="font-size:13px"  width="25%">  '.$fetch_s_name[2].' </b></font></td> 
<td align="left" style="border:0px solid black;border-collapse:collapse;"><font size="" style="font-size:15px">D.O.B.-<b style="font-size:13px" > '.date("jS F Y",strtotime($fetch_s_name[4])).' <b></b></b></font></td>
</tr>
</tbody></table>  							
       </div><!-- End widget-header -->	          							
<div class="widget-content" align="center">						
<!-- Table widget -->

<table  class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<thead >                                          
<tr>
<th rowspan="2" colspan="1" width="8%"><font size="2">SUBJECT</font></th> <th rowspan="2"  width="10%"><font size="2">PARAMETER</font></th><th colspan="5" align="center">Term-I</th></tr>
<tr><th  align="left" width="6%">C.T.-I</font></th><th  align="left" width="6%">C.T.-II</font></th><th  align="center" width="6%">TOTAL</font></th><th  width="6%"> Grade</font></th><th> Remarks</font></th></tr>    																	
  </thead> 															
  </thead> 
  <tbody align="center" style="border:1px solid black;">
        
              
           
         <tr align="center">
              <td valign="left" width="8%"><b  style="font-size:12px">ENGLISH</b></td> 
              <td valign="" width="17%" colspan="" align="left"><b>Written Assessment</b></td> 
              <td width="3%" style="border:1px solid black;"></td>
              <td width="3%" style="border:1px solid black;"></td>
              <td width="3%" style="border:1px solid black;"><b></b></td>
           
              <td style="border:1px solid black;"><b></b></td><td align="left"><b></td>
          </tr>
         
         <tr align="center">
         <td valign="" width="10%" rowspan="2" align="left"><b>Reading Skill</b></td>
            <td valign="" width="10%" colspan="" align="left">Pronunciation</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
           
           
            <td><b></b></td><td align="left"><b></td>
         </tr>
        <tr align="center">
            <td valign="" width="10%"  colspan="" align="left">Fluency</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
          
           
            <td><b></b></td><td align="left"><b></td>
         </tr>
           
        <tr align="center">
            <td valign="" width="10%" style="font-size:12;" rowspan="4" align="left"><b>Writing Skill</b></td> 
            <td valign="" width="10%" align="left">Creative Writing</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
        
            
            <td><b></b></td><td align="left"><b></td>
         </tr>
        <tr align="center">
            <td valign="" width="10%" align="left">Hand Writing</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
           
            <td><b></b></td><td align="left"><b></td>
         </tr>
        <tr align="center">
            <td valign="" width="10%"  align="left">Grammar</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
     
        
            <td><b></b></td><td align="left"><b></td>
         </tr>
         <tr align="center">
            <td valign="" width="10%"  align="left">Spelling</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
     
        
            <td><b></b></td><td align="left"><b> </td>
         </tr>
      <tr align="center">
            <td valign="" width="12%" rowspan="2" align="left"><b>Speaking Skill</b></td>
            <td valign="top" width="10%"  align="left">Conversation</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
        
            
            <td><b></b></td><td align="left"><b></td>
        </tr>
       <tr align="center">
           <td valign="" width="10%"  align="left"> Recitation</td>
           <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
          
           
            <td><b></b></td><td align="left"><b></td>
        </tr>
       <tr align="center">
          <td valign="" width="10%" colspan="2" align="left"><b>Dictation</b></td>
           <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
          
            
            <td><b></b></td><td align="left"><b></td>
       </tr>
       
        <tr align="center">
              <td valign="center" width="8%"><b  style="font-size:12px">HINDI</b></td> <td valign="left" width="17%" align="left"><b><b>Written Assessment</b></b></td> 
              <td width="3%"></td>
              <td width="3%"></td>
              <td width="3%"><b></b></td>
             
          
              <td style="border:1px solid black;"><b></b></td><td align="left"><b></td>
          </tr>
         
        <tr align="center">
         <td valign="" width="10%" rowspan="2" align="left"><b>Reading Skill</b></td>
            <td valign="top" width="10%" colspan="" align="left">Pronunciation</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
        
        
            <td><b></b></td><td align="left"><b></td>
         </tr>
        <tr align="center">
            <td valign="" width="10%"  colspan="" align="left">Fluency</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
     
            
            <td><b></b></td><td align="left"><b></td>
         </tr>
           
       <tr align="center">
            <td valign="" width="10%" rowspan="4" align="left"><b>Writing Skill</b></td> 
            <td valign="" width="10%" align="left">Creative Writing</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
   
            
            <td><b></b></td><td align="left"><b></td>
         </tr>
        <tr align="center">
            <td valign="top" width="10%"   align="left">Hand Writing</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
       
         
            <td><b></b></td><td align="left"><b>  </td>
         </tr>
        <tr align="center">
            <td valign="" width="10%"  align="left">Grammar</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
            
           
            <td><b></b></td><td align="left"><b></td>
         </tr>
         <tr align="center">
            <td valign="" width="10%"  align="left">Spelling</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
     
        
            <td><b></b></td><td align="left"><b> </td>
         </tr>
       <tr align="center">
            <td valign="" width="12%" rowspan="2" align="left"><b>Speaking Skill</b></td>
            <td valign="top" width="10%"  align="left">Conversation</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
           
           
            <td><b></b></td><td align="left"><b></td>
        </tr>
        <tr align="center">
           <td valign="" width="10%"  align="left"> Recitation</td>
           <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
           
          
            <td><b></b></td><td align="left"><b></td>
        </tr>
       <tr align="center">
          <td  width="10%" colspan="2" align="left"><b>Dictation</b></td>
           <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
      
           
            <td><b></b></td><td align="left"><b></td>
       </tr>
     
  <tr align="center">
          <td valign="center" width="8%"><b  style="font-size:12px">MATHS</b></td> 
          <td valign="top" width="17%" align="left"><b>Written Assessment</b></td>
         <td width="3%"></td><td width="3%"></td><td width="3%"><b></b></td>
         
         <td><b></b></td>      <td align="left"><b></td>
     </tr>
  <tr align="center">
          <td valign="top" width="10%" colspan="2" align="left"><b>Concept</b></td>
           <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
      
            
            <td><b></b></td><td align="left"><b></td>
     </tr>
   <tr align="center">
         <td valign="top" width="10%" colspan="2" align="left"><b>Tables</b></td>
          <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
         
           
            <td><b></b></td><td align="left"><b></td>
    </tr>
   <tr align="center">
        <td width="10%" colspan="2" align="left"><b>Mental Ability</b></td> <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
         
           
            <td><b></b></td><td align="left"><b> </td>
     </tr>
  <tr align="center">
           <td valign="top" width="10%" colspan="2" align="left"><b>Activity</b></td> <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
            
          
            <td><b></b></td><td align="left"><b></td>
     </tr>  
              
                


<tr align="center">
       
         <td width="8%"><b  style="font-size:12px">EVS</b></td><td valign="top" width="17%" align="left"><b>Written Assessment</b></td> <td width="3%"></td>
        <td width="3%"></td>
        <td width="3%"><b></b></td>
    
        <td><b></b></td>   <td align="left"><b></td>
                </tr>
                
      <tr align="center">
                 <td valign="top" width="10%" colspan="2" align="left"><b>Group Discussion</b></td> <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
         
            
            <td><b></b></td>   <td align="left"><b></td>
       </tr>
     <tr align="center">
                   <td valign="top" width="10%" colspan="2" align="left"><b>Activity</b></td> <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
         
            <td><b></b></td>   <td align="left"><b></td>
                </tr>
     <tr align="center">
                   <td valign="top" width="10%" colspan="2" align="left"><b>Project</b></td> <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
            
            <td><b></b></td>   <td align="left"><b></td>
     </tr>  
               


 <tr align="center">
         <td valign="center" width="8%"><b  style="font-size:12px">COMPUTER</b></td> 
         <td valign="top" width="17%" align="left"><b>Written Assessment</b></td> 
         <td width="3%"></td><td width="3%"></td><td width="3%"><b></b></td>
        
         <td><b></b></td>   <td align="left"><b></td>
   </tr>
                
  <tr align="center">
              <td valign="top" width="10%" colspan="2" align="left"><b>Practical</b></td> <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
        
          
            <td><b></b></td>   <td align="left"><b> </td>
   </tr>
   <tr align="center">
            <td valign="top" width="10%" colspan="2" align="left"><b>Viva</b></td> <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
            <td><b></b></td><td align="left"><b></b></td>
    </tr>
   <tr align="center">
            <td valign="top" width="10%" colspan="2" align="left"><b>Project</b></td><td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
        
            
            <td><b></b></td><td align="left"><b></td>
   </tr>  
               



   <tr align="center">
        <td valign="center" width="8%"><b  style="font-size:12px">G.K</b></td> <td valign="top" width="17%" align="left"><b>Written Assessment<b></td> 
         <td width="3%"></td><td width="3%"></td>
         <td width="3%"><b></b></td>
      
         <td><b></b></td>   <td align="left"><b></td>
   </tr>
                
                <tr align="center">
                  <td valign="top" width="10%" colspan="2" align="left"><b>Current Knowledge</b></td> <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
            
          
            <td><b></b></td>   <td align="left"><b> </td>
                </tr>
                  
        <tr  width="100%">
<td align="left"  colspan="10"><b  style="font-size:18px"><font size="3">Attendance:</font></b><b style="margin-left:180px; width:100%;"  style="font-size:18px"><font size="3">Blood Group:</b> '.$blood.'<b style="margin-left:100px; width:100%;" style="font-size:18px"><font size="3">Height:</b><b style="margin-left:90px; width:100%;" style="font-size:18px"><font size="3">Weight:</b></td> 

</tr>    


</tbody></table>
';                      
      $get_subject = "
      SELECT DISTINCT subject.subject,subject.subject_id,scho_activity_table.*,types_of_activities.*
      FROM subject
      INNER JOIN scho_marks
      ON subject.subject_id =scho_marks.subject_id
      INNER JOIN scho_activity_table
      ON scho_activity_table.id =scho_marks.scho_act_id
      INNER JOIN types_of_activities
      ON types_of_activities.Id=scho_activity_table.type_id
      INNER JOIN class
      ON class.sId=scho_marks.student_id
      WHERE scho_marks.student_id=".$_GET['student_id']." ";

//      echo '<br><table class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
//             <thead>
//                <tr style="border:1px solid black;">
//                  <th align="center" colspan="2" style="border:1px solid black;"><b>Co-Curricular Activities</b></th>
//                  <th align="center" style="border:1px solid black;"><b>GRADE</b></font></th>
//                  <th align="center" style="border:1px solid black;"><b>Personality Development</b></th>
//                  <th align="center"><b>GRADE</b></th>
//               </tr>
//            </thead>
//                <tr align="center">
//                  <td align="left" rowspan="3"><b>GAMES</b></font></td><td align="left">Team Spirit</td><td align="center"><b></b></td><td align="left">Attitude</td><td></td>
//                  
//               </tr>
//               <tr align="center">
//                  <td align="left">Discipline</td><td></td><td align="left">Confidence</td><td></td>
//                  
//               </tr>
//               <tr align="center">
//                   <td align="left">Enthusiasm</td><td></td><td align="left">Care of Belongings</td><td></td>
//                  
//               </tr>
//               <tr align="center">
//                  <td align="left" rowspan="3"><b>Art & Craft</b></font></td><td align="left">Neatness</td><td></td><td align="left">Discipline</td><td></td>
//                  
//               </tr>
//               <tr align="center">
//                  <td align="left">Colouring</td><td></td><td align="left">Regularity & Punctuality</td><td></td>
//                  
//               </tr>
//               <tr align="center">
//                  <td align="left">Book</td><td></td><td align="left">Respect for school property</td><td></td>
//                  
//               </tr>
//               <tr align="center">
//                  <td align="left" rowspan="2"><b>MUSIC/DANCE</b></font></td><td align="left">Rhythm</td><td></td><td align="left">Sharing and Caring</td><td></td>
//                  
//               </tr>
//               <tr align="center">
//                  <td align="left">Interest</td><td></td><td align="left">Imaginative Skills</td><td></td>
//                  
//               </tr>
//              
//           <tbody align="center">
//           </tbody></table>';
       echo'<table class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
        <thead>
              <tr style="border:1px solid black;">
                <th align="center" colspan="6" style="border:1px solid black;"><b  style="font-size:12px">Co-Curricular Activities</b></th><th align="center" style="border:1px solid black;" colspan="4"><b  style="font-size:12px">Personality  Development</b></th></tr>
              <tr> <th align="center"  style="border:1px solid black;" colspan="2"><b> Activities</b></th>
                <th align="center" style="border:1px solid black;"><b>GRADE</b></font></th>
                <th align="center"  style="border:1px solid black;" colspan="2"><b>Activities</b></th>
                <th align="center" style="border:1px solid black;"><b>GRADE</b></font></th>
                <th align="center" style="border:1px solid black;" ><b>Activities</b></th>
                 <th align="center"><b>GRADE</b></th>
                 <th align="center" style="border:1px solid black;" ><b>Activities</b></th>
                 <th align="center"><b>GRADE</b></th>
                </tr>  </thead>';
      
      echo'<tr ><th rowspan="3" align="left" width="10%">GAMES<td>Team Spirit</td><td align="center"><b></b></td><th rowspan="3" align="left" width="10%">ART & CRAFT<td>Neatness</td><td align="center"><b></b></td><td>Attitude</td></td><td align="center"><b></b></td><td>Confidence</td><td align="center"><b></b></td><tr><td>Discipline</td><td align="center"><b></b></td><td>Colouring</td><td align="center"><b></b></td><td>Care of Belongings</td><td align="center"><b></b></td><td>Discipline</td><td align="center"><b></b></td></th><tr><td>Enthusiasm</td><td align="center"><b></b></td><td>Book Work</td><td align="center"><b></b></td><td>Regularity & Punctuality</td><td align="center"><b></b></td><td>Respect for School Property</td><td align="center"><b></b></td></th></tr><th align="left">MUSIC & DANCE</th><td>Rhythm</td><td align="center"><b></b></td><td colspan="2">Interest</td><td align="center"><b></b></td><td>Sharing & Caring</td><td align="center"><b></b></td><td>Imaginative Skills</td><td align="center"><b></b></td></tr></tr>';
      echo'  <table width="100%" align="left" id="77"  height=60 style="border:0px"><tbody>
<td  style="float:left;border:0px" valign="bottom"><b><font size="2"><b >Date of Issuing:</b><b><font size="2"><b  style="margin-left:90px; width:100%;">Parent\'s signature</b><font size="2"><b  style="margin-left:80px; width:100%;">Class Teacher`s signature</font></b><b  style="margin-left:70px; width:100%;"><font size="2">Principal`s signature</b></font></td>
</tbody></table>';
echo'</tbody></table>';    
}
else if($class_id==19)
 {
//header( 'Content-Type: text/html; charset=utf-8' ); 
require_once('../include/session.php');
require_once('../include/check.php');
require_once('../config/config.php');
require_once('../include/userdetail.php');
 $class_id=$_GET['class_id'];
$student_id=$_GET['student_id'];
?>
<style>
table,th,td,tr
{
border:1px solid black;
font-size: 11px;
}
</style>
<?php
  echo '<p style="z-index:1; position:absolute; top:3px; left:75px"><font size="6%" style="font-size:33px"><strong>VIDYA BAL BHAWAN SR. SEC. SCHOOL </strong></font></p>';
    echo'
<table  width="100%" style="border:0px">
<tbody ><tr><td>
<table  width="100%" style="border:0px">
<tbody style="border:0px">
<tr style="border:0px">
<td align="left" width="25%" style="border:0px"></td>
</tr>
<tr style="border:0px">';echo'<td align="left" style="border:0px"position:absolute; top:0px; ><img src="../images/logo/School.jpg" width=32% ></td>';
echo'<br><div ><td  valign="top" width="150%" style="margin-left:-2px; width:100%;border:0px">
<font size="3" style="margin-left:70px; width:100%;" style="font-size:20px"><br>
      Kondli Gharoli Road, Mayur  Vihar III, Delhi -110096 </font>
      <font size="4" style="margin-left:20px; width:100%;" style="font-size:20px"></font><br>
    <b style="margin-left:-30px; width:100%;"><font size="4" style="font-size:18px">Record of Academic Performance Term-I (2014-2015)</b>  </font> </td>
</tr>
</tbody>
</table>';



//get the name of the student on that id
$subject_id=0;
$get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB,`Blood Group`
FROM student_user
WHERE sId = ".$_GET['student_id']."";
$exe_s_name=mysql_query($get_s_name);
$fetch_s_name=mysql_fetch_array($exe_s_name);
$name_student=$fetch_s_name[0];
$blood=$fetch_s_name['Blood Group'];

// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.*
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.sId = ".$_GET['student_id']."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 $class_name=$fetch_cls_name['class'];
        $section_name=$fetch_cls_name['section'];
         
echo '
         
<div class="row-fluid">
<div class="widget  span12 clearfix">
<div class="widget-header">
<table  width="100%" id=8   valign="top" class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<tbody >
<tr width="30%" style="border:0px solid black;border-collapse:collapse;">
<td align="left" style="border:0px solid black;border-collapse:collapse;"><font size="" style="font-size:15px">Student Name: </font><b  style="font-size:13px"> '.$name_student.'</b></td>
<td align="left"  style="border:0px solid black;border-collapse:collapse;"><font size="" style="font-size:15px">Class :</font><b  style="font-size:13px" width="25%"> '.$class_name.'       '.$section_name.' </b><font size="" style="font-size:15px;margin-left:30px; width:100%">Admission No.:<b style="margin-left:10px; width:100%;"  style="font-size:13px" >'.$fetch_s_name[3].'    </b></font></td>
<td align="left" style="border:0px solid black;border-collapse:collapse;"><font size="" style="font-size:15px">Examination Roll.No:<b  style="font-size:13px" ></td>
</tr>
<tr width="30%" style="border:0px solid black;border-collapse:collapse;">
<td align="left" style="border:0px solid black;border-collapse:collapse;" ><font size="" style="font-size:15px">Father`s Name: <b style="font-size:13px">'.$fetch_s_name[1].'</b></font></td>
<td align="left"width="30" style="border:0px solid black;border-collapse:collapse;" ><font size="" style="font-size:15px">Mother`s Name:<b style="font-size:13px"  width="25%">  '.$fetch_s_name[2].' </b></font></td> 
<td align="left" style="border:0px solid black;border-collapse:collapse;"><font size="" style="font-size:15px">D.O.B.-<b style="font-size:13px" > '.date("jS F Y",strtotime($fetch_s_name[4])).' <b></b></b></font></td>
</tr>
</tbody></table>  							
       </div><!-- End widget-header -->	          							
<div class="widget-content" align="center">						
<!-- Table widget -->

<table  class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<thead >                                          
<tr">
<th rowspan="2" colspan="1" width="10%"><font size="2">SUBJECT</font></th> <th rowspan="2"  width="10%"><font size="2">PARAMETER</font></th><th colspan="5" align="center">Term-I</th></tr>
<tr><th  align="left" width="6%">C.T.-I</font></th><th  align="left" width="6%">C.T.-II</font></th><th  align="center" width="6%">TOTAL</font></th><th  width="6%"> Grade</font></th><th> Remarks</font></th></tr>																
  </thead> 															
  </thead> 
  <tbody align="center" style="border:1px solid black;">
        
              
           
         <tr align="center">
              <td valign="left" width="10%"><b>ENGLISH</b></td> 
              <td valign="" width="17%" colspan="" align="left"style="font-size:12px"><b>Written Assessment</b></td> 
              <td width="3%" style="border:1px solid black;" style="font-size:12px"></td>
              <td width="3%" style="border:1px solid black;" style="font-size:12px"></td>
              <td width="3%" style="border:1px solid black;" style="font-size:12px"><b></b></td>
           
              <td style="border:1px solid black;" style="font-size:12px"><b></b></td><td></td>
          </tr>
         
         <tr align="center">
         <td valign="" width="10%" rowspan="2" align="left" style="font-size:12px"><b>Reading Skill</b></td>
            <td valign="" width="10%" colspan="" align="left" style="font-size:12px">Pronunciation</td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
           
           
            <td><b></b></td><td></td>
         </tr>
        <tr align="center">
            <td valign="" width="10%"  colspan="" align="left" style="font-size:12px">Fluency</td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
          
           
            <td style="font-size:12px"><b></b></td><td></td>
         </tr>
           
        <tr align="center">
            <td valign="" width="10%" style="font-size:12;" rowspan="4" align="left" ><b>Writing Skill</b></td> 
            <td valign="" width="10%" align="left" style="font-size:12px">Creative Writing</td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
        
            
            <td style="font-size:12px"> <b></b></td><td></td>
         </tr>
        <tr align="center">
            <td valign="" width="10%" align="left" style="font-size:12px">Hand Writing</td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
           
            <td style="font-size:12px"><b></b></td><td></td>
         </tr>
        <tr align="center">
            <td valign="" width="10%"  align="left">Grammar</td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
     
        
            <td style="font-size:12px"><b></b></td><td></td>
         </tr>
         <tr align="center">
            <td valign="" width="10%"  align="left">Spelling</td>
            <td width="3%"></td>
            <td width="3%"></td>
            <td width="3%"><b></b></td>
     
        
            <td><b></b></td><td></td>
         </tr>
      <tr align="center">
            <td valign="" width="12%" rowspan="2" align="left" style="font-size:12px"><b>Speaking Skill</b></td>
            <td valign="top" width="10%"  align="left" style="font-size:12px">Conversation</td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
        
            
            <td style="font-size:12px"><b></b></td><td></td>
        </tr>
       <tr align="center">
           <td valign="" width="10%"  align="left" style="font-size:12px"> Recitation</td>
           <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
          
           
            <td style="font-size:12px"><b></b></td><td></td>
        </tr>
       <tr align="center">
          <td valign="" width="10%" colspan="2" align="left" style="font-size:12px"><b>Dictation</b></td>
           <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
          
            
            <td style="font-size:12px"><b></b></td><td></td>
       </tr>
       
        <tr align="center">
              <td valign="center" width="10%"><b>HINDI</b></td> <td valign="left" width="17%" align="left" style="font-size:12px"><b><b>Written Assessment</b></b></td> 
              <td width="3%" style="font-size:12px"></td>
              <td width="3%" style="font-size:12px"></td>
              <td width="3%" style="font-size:12px"><b></b></td>
             
          
              <td style="border:1px solid black;" style="font-size:12px"><b></b></td><td></td>
          </tr>
         
        <tr align="center">
         <td valign="" width="10%" rowspan="2" align="left" style="font-size:12px"><b>Reading Skill</b></td>
            <td valign="top" width="10%" colspan="" align="left" style="font-size:12px">Pronunciation</td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
        
        
            <td style="font-size:12px"><b></b></td><td></td>
         </tr>
        <tr align="center">
            <td valign="" width="10%"  colspan="" align="left" style="font-size:12px">Fluency</td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
     
            
            <td style="font-size:12px"><b></b></td><td></td>
         </tr>
           
       <tr align="center">
            <td valign="" width="10%" rowspan="4" align="left" style="font-size:12px"><b>Writing Skill</b></td> 
            <td valign="" width="10%" align="left" style="font-size:12px">Creative Writing</td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
   
            
            <td style="font-size:12px"><b></b></td><td></td>
         </tr>
        <tr align="center">
            <td valign="top" width="10%"   align="left" style="font-size:12px">Hand Writing</td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
       
         
            <td style="font-size:12px"><b></b></td><td></td>
         </tr>
        <tr align="center">
            <td valign="" width="10%"  align="left" style="font-size:12px">Grammar</td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
            
           
            <td style="font-size:12px"><b></b></td><td></td>
         </tr>
         <tr align="center">
            <td valign="" width="10%"  align="left" style="font-size:12px">Spelling</td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
            
           
            <td style="font-size:12px"><b></b></td><td></td>
         </tr>
       <tr align="center">
            <td valign="" width="12%" rowspan="2" align="left" style="font-size:12px"><b>Speaking Skill</b></td>
            <td valign="top" width="10%"  align="left" style="font-size:12px">Conversation</td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
           
           
            <td style="font-size:12px"><b></b></td><td></td>
        </tr>
        <tr align="center">
           <td valign="" width="10%"  align="left" style="font-size:12px"> Recitation</td>
           <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
           
          
            <td style="font-size:12px"><b></b></td><td></td>
        </tr>
       <tr align="center">
          <td  width="17%" colspan="2" align="left" style="font-size:12px"><b>Dictation</b></td>
           <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
      
           
            <td style="font-size:12px"><b></b></td><td></td>
       </tr>
     
  <tr align="center">
          <td valign="center" width="10%" style="font-size:12px"><b>MATHS</b></td> 
          <td valign="top" width="17%" align="left" style="font-size:12px"><b>Written Assessment</b></td>
         <td width="3%" style="font-size:12px"></td><td width="3%" style="font-size:12px"></td><td width="3%" style="font-size:12px"><b></b></td>
         
         <td style="font-size:12px"><b></b></td><td></td>
     </tr>
  <tr align="center">
          <td valign="top" width="10%" colspan="2" align="left" style="font-size:12px"><b>Concept</b></td>
           <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
      
            
            <td style="font-size:12px"><b></b></td><td></td>
     </tr>
   <tr align="center">
         <td valign="top" width="10%" colspan="2" align="left" style="font-size:12px"><b>Tables</b></td>
          <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
         
           
            <td style="font-size:12px"><b></b></td><td></td>
    </tr>
   <tr align="center">
        <td width="17%" colspan="2" align="left" style="font-size:12px"><b>Mental Ability</b></td> <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
         
           
            <td style="font-size:12px"><b></b></td><td></td>
     </tr>
  <tr align="center">
           <td valign="top" width="10%" colspan="2" align="left" style="font-size:12px"><b>Activity</b></td> <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
            
          
            <td style="font-size:12px"><b></b></td><td></td>
     </tr>  
              
                


<tr align="center">
       
         <td width="10%" style="font-size:12px" ><b>SCIENCE</b></td><td valign="top" width="17%" align="left" style="font-size:12px"><b>Written Assessment</b></td> <td width="3%" style="font-size:12px"></td>
        <td width="3%" style="font-size:12px"></td>
        <td width="3%" style="font-size:12px"><b></b></td>
    
        <td style="font-size:12px"><b></b></td><td></td>
                </tr>
                
      <tr align="center">
                 <td valign="top" width="10%" colspan="2" align="left" style="font-size:12px"><b>Group Discussion</b></td> <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
         
            
            <td><b></b></td><td></td>
       </tr>
     <tr align="center">
                   <td valign="top" width="10%" colspan="2" align="left" style="font-size:12px"><b>Activity</b></td> <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
         
            <td style="font-size:12px"><b></b></td><td></td>
                </tr>
     <tr align="center">
                   <td valign="top" width="10%" colspan="2" align="left" style="font-size:12px"><b>Project</b></td> <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
            
            <td style="font-size:12px"><b></b></td><td></td>
     </tr>  
               
<tr align="center">
        <td valign="center" width="10%" style="font-size:12px"><b>SST</b></td> <td valign="top" width="17%" align="left" style="font-size:12px"><b>Written Assessment<b></td> 
         <td width="3%" style="font-size:12px"></td><td width="3%" style="font-size:12px"></td>
         <td width="3%" style="font-size:12px"><b></b></td>
      
         <td style="font-size:12px"><b></b></td><td></td>
   </tr>
                
                <tr align="center">
                  <td valign="top" width="10%" colspan="2" align="left" style="font-size:12px"><b>Environmental Sensitivity</b></td> <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
            
          
            <td style="font-size:12px"><b></b></td><td></td>
                </tr>
                  


 <tr align="center">
                  <td valign="top" width="10%" colspan="2" align="left" style="font-size:12px"><b>Group Discussion</b></td> <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
            
          
            <td style="font-size:12px"><b></b></td><td></td>
 </tr>
  
 <tr align="center">
                  <td valign="top" width="10%" colspan="2" align="left" style="font-size:12px"><b>Activity</b></td> <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
            
          
            <td style="font-size:12px"><b></b></td><td></td>
 </tr>


 <tr align="center">
         <td valign="center" width="10%" style="font-size:12px"><b>COMPUTER</b></td> 
         <td valign="top" width="17%" align="left" style="font-size:12px"><b>Written Assessment</b></td> 
         <td width="3%"></td><td width="3%" style="font-size:12px"></td><td width="3%"><b></b></td>
        
         <td style="font-size:12px"><b></b></td><td></td>
   </tr>
                
  <tr align="center">
              <td valign="top" width="10%" colspan="2" align="left" style="font-size:12px"><b>Practical</b></td> <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
        
          
            <td style="font-size:12px"><b></b></td><td></td>
   </tr>
   <tr align="center">
            <td valign="top" width="10%" colspan="2" align="left" style="font-size:12px"><b>Viva</b></td> <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
           
            
            <td style="font-size:12px"><b></b></td><td></td>
    </tr>
   <tr align="center">
            <td valign="top" width="10%" colspan="2" align="left" style="font-size:12px"><b>Project</b></td> <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
        
            
            <td style="font-size:12px"><b></b></td><td></td>
   </tr>  
               



   <tr align="center">
        <td valign="center" width="10%" style="font-size:12px"><b>G.K</b></td> <td valign="top" width="17%" align="left" style="font-size:12px"><b>Written Assessment<b></td> 
         <td width="3%" style="font-size:12px"></td><td width="3%" style="font-size:12px"></td>
         <td width="3%" style="font-size:12px"><b></b></td>
      
         <td style="font-size:12px"><b></b></td><td></td>
   </tr>
                
      <tr align="center">
                  <td valign="top" width="10%" colspan="2" align="left" style="font-size:12px"><b>Current Knowledge</b></td> <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"></td>
            <td width="3%" style="font-size:12px"><b></b></td>
            
          
            <td style="font-size:12px"><b></b></td><td></td>
    </tr>
    
<tr  width="100%">
<td align="left"  colspan="10"><b  style="font-size:18px"><font size="2">Attendance:</font></b><b style="margin-left:180px; width:100%;"  style="font-size:18px"><font size="2">Blood Group:</b> '.$blood.'<b style="margin-left:100px; width:100%;" style="font-size:18px"><font size="2">Height:</b><b style="margin-left:90px; width:100%;" style="font-size:18px"><font size="2">Weight:</b></td> 

</tr>    

   

</tbody></table>
';                      
      $get_subject = "
      SELECT DISTINCT subject.subject,subject.subject_id,scho_activity_table.*,types_of_activities.*
      FROM subject
      INNER JOIN scho_marks
      ON subject.subject_id =scho_marks.subject_id
      INNER JOIN scho_activity_table
      ON scho_activity_table.id =scho_marks.scho_act_id
      INNER JOIN types_of_activities
      ON types_of_activities.Id=scho_activity_table.type_id
      INNER JOIN class
      ON class.sId=scho_marks.student_id
      WHERE scho_marks.student_id=".$_GET['student_id']." ";

//      echo '<br><table class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
//             <thead>
//                <tr style="border:1px solid black;">
//                  <th align="center" colspan="4" style="border:1px solid black;"><b>Co-Curricular Activities</b></th>
//                  <th align="center" style="border:1px solid black;"><b>GRADE</b></font></th>
//                  <th align="center" style="border:1px solid black;" ><b>Personality  Development</b></th>
//                  <th align="center"><b>GRADE</b></th>
//               </tr>
//            </thead>
//                <tr align="center">
//                  <td align="left" rowspan="3"><b>GAMES</b></font></td><td align="left">Team Spirit</td><td align="center"><b></b></td><td align="left">Attitude</td><td align="center"><b></b></td>
//                  
//               </tr>
//               <tr align="center">
//                  <td align="left">Discipline</td><td align="center"><b></b></td><td align="left">Confidence</td><td align="center"><b></b></td>
//                  
//               </tr>
//               <tr align="center">
//                   <td align="left">Enthusiasm</td><td align="center"><b></b></td><td align="left">Care of Belongings</td><td align="center"><b></b></td>
//                  
//               </tr>
//               <tr align="center">
//                  <td align="left" rowspan="3"><b>Art & Craft</b></font></td><td align="left">Neatness</td><td align="center"><b></b></td><td align="left">Discipline</td><td align="center"><b></b></td>
//                  
//               </tr>
//               <tr align="center">
//                  <td align="left">Colouring</td><td align="center"><b></b></td><td align="left">Regularity & Punctuality</td><td align="center"><b></b></td>
//                  
//               </tr>
//               <tr align="center">
//                  <td align="left">Book</td><td align="center"><b></b></td><td align="left">Respect for school property</td><td align="center"><b></b></td>
//                  
//               </tr>
//               <tr align="center">
//                  <td align="left" rowspan="2"><b>MUSIC/DANCE</b></font></td><td align="left">Rhythm</td><td align="center"><b></b></td><td align="left">Sharing and Caring</td><td align="center"><b></b></td>
//                  
//               </tr>
//               <tr align="center">
//                  <td align="left">Interest</td><td align="center"><b></b></td><td align="left">Imaginative Skills</td><td> </td>
//                  
//               </tr>
//              
//           <tbody align="center">
//           </tbody></table>';
      
      echo'<table class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
        <thead>
              <tr style="border:1px solid black;">
                <th align="center" colspan="6" style="border:1px solid black;"><b  style="font-size:12px">Co-Curricular Activities</b></th><th align="center" style="border:1px solid black;" colspan="4"><b  style="font-size:12px">Personality  Development</b></th></tr>
              <tr> <th align="center"  style="border:1px solid black;" colspan="2"><b> Activities</b></th>
                <th align="center" style="border:1px solid black;"><b>GRADE</b></font></th>
                <th align="center"  style="border:1px solid black;" colspan="2"><b>Activities</b></th>
                <th align="center" style="border:1px solid black;"><b>GRADE</b></font></th>
                <th align="center" style="border:1px solid black;" ><b>Activities</b></th>
                 <th align="center"><b>GRADE</b></th>
                 <th align="center" style="border:1px solid black;" ><b>Activities</b></th>
                 <th align="center"><b>GRADE</b></th>
                </tr>  </thead>';
      
      echo'<tr ><th rowspan="3" align="left" width="10%">GAMES<td>Team Spirit</td><td align="center"><b></b></td><th rowspan="3" align="left" width="10%">ART & CRAFT<td>Neatness</td><td align="center"><b></b></td><td>Attitude</td></td><td align="center"><b></b></td><td>Confidence</td><td align="center"><b></b></td><tr><td>Discipline</td><td align="center"><b></b></td><td>Colouring</td><td align="center"><b></b></td><td>Care of Belongings</td><td align="center"><b></b></td><td>Discipline</td><td align="center"><b></b></td></th><tr><td>Enthusiasm</td><td align="center"><b></b></td><td align="left">Book Work</td><td align="center"><b></b></td><td>Regularity & Punctuality</td><td align="center"><b></b></td><td>Respect for School Property</td><td align="center"><b></b></td></th></tr><th align="left">MUSIC & DANCE</th><td>Rhythm</td><td align="center"><b></b></td><td colspan="2">Interest</td><td align="center"><b></b></td><td>Sharing & Caring</td><td align="center"><b></b></td><td>Imaginative Skills</td><td align="center"><b></b></td></tr></tr>';
      
    echo'  <table width="100%" align="left" id="77" height=35   style="border:0px"><tbody>
<td  style="float:left;border:0px" valign="bottom"><b><font size="2"><b >Date of Issuing:</b><b><font size="2"><b  style="margin-left:80px; width:100%;">Parent\'s signature</b><font size="2"><b  style="margin-left:80px; width:100%;">Class Teacher\'s signature</font></b><b  style="margin-left:70px; width:80%;"><font size="2">Principal\'s signature</b></font></td>
</tbody></table>';
echo'</tbody></table>';
 

     
     
     
     
 }

?>