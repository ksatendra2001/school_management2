<?php
header( 'Content-Type: text/html; charset=utf-8' ); 
require_once('../include/session.php');
require_once('../include/check.php');
require_once('../config/config.php');
require_once('../include/userdetail.php');
require_once('../include/grade.php');
$class_id=$_GET['class_id'];
$student_id=$_GET['student_id'];
$session_id=$_SESSION['current_session_id'];
//******************************* Query to get the marks and store in array format*******************************
$array_report = array();
$i=0;
// query to get the subject on the basis of class.
$get_subject = "SELECT DISTINCT subject_id FROM cce_subject_type_skills_id_table WHERE class_id=".$class_id."";
$exe_subject = mysql_query($get_subject);
while($fetch_subject= mysql_fetch_array($exe_subject))
{
   // query to get the subject name
   $get_subject_name = "SELECT `subject` FROM subject WHERE subject_id=".$fetch_subject['subject_id']."";
   $exe_subject_name = mysql_query($get_subject_name);
   $fetch_subject_name = mysql_fetch_array($exe_subject_name);
   $report[$i]['subject'] = $fetch_subject_name['subject'];
   
     //query to get the type_id
         $get_type_id = "SELECT DISTINCT type_id FROM cce_subject_type_skills_id_table
         WHERE cce_subject_type_skills_id_table.subject_id = ".$fetch_subject['subject_id']."
         AND cce_subject_type_skills_id_table.class_id = ".$class_id."";
   
        $type =array();
        
        $j=0;
        $exe_type_id = mysql_query($get_type_id);
        while($fetch_type_id = mysql_fetch_array($exe_type_id))
         {
            //query to get the type_id
            $get_type_name = "SELECT * 
            FROM cce_types_activity_table
            WHERE type_id = ".$fetch_type_id['type_id']."";
            $exe_type_name = mysql_query($get_type_name);
            $fetch_type_name =mysql_fetch_array($exe_type_name);
            
           // $type[$j]['type_name'] = $fetch_type['type_name'];

            // query to get the skill name
           $get_skill_id = "SELECT skill_id,sts_id FROM cce_subject_type_skills_id_table 
            WHERE type_id = ".$fetch_type_id['type_id']." 
            AND subject_id = ".$fetch_subject['subject_id']."
            AND class_id = ".$class_id."" ;
            
            $exe_skill_id = mysql_query($get_skill_id);
            while($fetch_skill_id = mysql_fetch_array($exe_skill_id))
            {
            $sts_id = $fetch_skill_id['sts_id'];
            $skill = array();
            $k = 0;
            //query to get the get the marks of first_term c.t-1
             $get_marks_t1_ct1 = "SELECT marks FROM cce_marks_scho_type_skills_table
             WHERE student_id = ".$student_id." AND sts_id = ".$sts_id." AND term_test_id = 7";
             $exe_marks_t1_ct1 = mysql_query($get_marks_t1_ct1);
             $fetch_markd_t1_ct1 = mysql_fetch_array($exe_marks_t1_ct1);
             $marks_t1_ct1 = $fetch_markd_t1_ct1['marks'];
             
             // query to get the get the marks of first_term c.t-2
             $get_marks_t1_ct2 = "SELECT marks FROM cce_marks_scho_type_skills_table
             WHERE student_id = ".$student_id." AND sts_id = ".$sts_id." AND term_test_id = 8";
             $exe_marks_t1_ct2 = mysql_query($get_marks_t1_ct2);
             $fetch_markd_t1_ct2 = mysql_fetch_array($exe_marks_t1_ct2);
             $marks_t1_ct2 = $fetch_markd_t1_ct2['marks'];
             
             // query to get the get the marks of term2 c.t-111
             $get_marks_t2_ct1 = "SELECT marks FROM cce_marks_scho_type_skills_table
             WHERE student_id = ".$student_id." AND sts_id = ".$sts_id." AND term_test_id = 9";
             $exe_marks_t2_ct1 = mysql_query($get_marks_t2_ct1);
             $fetch_markd_t2_ct1 = mysql_fetch_array($exe_marks_t2_ct1);
             $marks_t2_ct1 = $fetch_markd_t2_ct1['marks'];
             
             
              
             // query to get the get the marks of term2 c.t-4
             $get_marks_t2_ct2 = "SELECT marks FROM cce_marks_scho_type_skills_table
             WHERE student_id = ".$student_id." AND sts_id = ".$sts_id." AND term_test_id = 10";
             $exe_marks_t2_ct2 = mysql_query($get_marks_t2_ct2);
             $fetch_markd_t2_ct2 = mysql_fetch_array($exe_marks_t2_ct2);
             $marks_t2_ct2 = $fetch_markd_t2_ct2['marks'];
             
             
             $t11_total = $marks_t1_ct2 + $marks_t1_ct1;
             $t22_total = $marks_t2_ct2 + $marks_t2_ct1;
             
            
             $total = ($t11_total+$t22_total);
            // $grade_total = 0;
             $t1_marks='';
             $t2_marks='';
             $marks1_t1_ct1 = '';
             $marks1_t1_ct2 = '';
             $marks1_t2_ct1 = '';
             $marks1_t2_ct2 = '';
             $sub_total='';
             $grade = '';
             //$grade = calculate_grade_point($total); // function to calculate and return the grade 
            if(!($fetch_skill_id['skill_id']))
            {
               
                if($fetch_type_id['type_id'] == 1)  
                   {
                     $t1_total = ($t11_total);
                     $t2_total = ($t22_total);
                     $marks1_t1_ct1 = $marks_t1_ct1;
                     $marks1_t1_ct2 = $marks_t1_ct2;
                     $marks1_t2_ct1 = $marks_t2_ct1;
                     $marks1_t2_ct2 = $marks_t2_ct2;
                     $sub_total = $total;
                     $grade = calculate_total_grade($sub_total);
                   }
                   else
                   {
                     $t1_marks = (($t11_total*100)/10);
                     $t2_marks = (($t22_total*100)/10);
                     $total1 = (($total*100)/20);
                     $sub_total = calculate_total_grade($total1);
                     $t1_total = calculate_total_grade($t1_marks);
                     $t2_total= calculate_total_grade($t2_marks);
                     
                     $marks1_t1_ct1 = calculate_grade($marks_t1_ct1);
                     $marks1_t1_ct2 = calculate_grade($marks_t1_ct2);
                     $marks1_t2_ct1 = calculate_grade($marks_t2_ct1);
                     $marks1_t2_ct2 = calculate_grade($marks_t2_ct2);
                     
                     $grade = calculate_total_grade($total1);
                   }
                   
               $subject_name = $fetch_subject_name['subject'];
               $type_name = $fetch_type_name['type_name'];
               $skill_name = $fetch_skill_details['skill_name'];
               $array_report[$subject_name][$type_name][$type_name]['t1']['ct1']['marks'] = $marks1_t1_ct1;
               $array_report[$subject_name][$type_name][$type_name]['t1']['ct2']['marks'] = $marks1_t1_ct2;
               $array_report[$subject_name][$type_name][$type_name]['t1']['total'] = $t1_total;
               $array_report[$subject_name][$type_name][$type_name]['t2']['ct1']['marks'] = $marks1_t2_ct1;
               $array_report[$subject_name][$type_name][$type_name]['t2']['ct2']['marks'] = $marks1_t2_ct2;
               $array_report[$subject_name][$type_name][$type_name]['t2']['total'] = $t2_total;
               $array_report[$subject_name][$type_name][$type_name]['t1+t2']= $sub_total;
               $array_report[$subject_name][$type_name][$type_name]['grade'] = $grade;
               //$skill_name = "Written assessment";
               //$array_report[$subject_name][$type_name][$type_name][$marks_t1_ct1][$marks_t1_ct2][$total][$grade] = 0;
            }
            else
            {
               $t1_total = (($t11_total*100)/10);
               $t2_total = (($t22_total*100)/10);
               $total1 = (($total*100)/20);
               $sub_total = calculate_total_grade($total1);
            // query to get the skill details
            $get_skill_details ="SELECT * FROM cce_name_skills_table
            WHERE skill_id = ".$fetch_skill_id['skill_id']." ";
            $exe_skill_details = mysql_query($get_skill_details);
            while($fetch_skill_details = mysql_fetch_array($exe_skill_details))
            { 
               //$skill[$k]['skill'] = $fetch_skill_details['skill_name'];
               $subject_name = $fetch_subject_name['subject'];
               $type_name = $fetch_type_name['type_name'];
               $skill_name = $fetch_skill_details['skill_name'];
               $array_report[$subject_name][$type_name][$skill_name]['t1']['ct1']['marks'] = calculate_grade($marks_t1_ct1);
               $array_report[$subject_name][$type_name][$skill_name]['t1']['ct2']['marks'] = calculate_grade($marks_t1_ct2);
               $array_report[$subject_name][$type_name][$skill_name]['t1']['total'] = calculate_total_grade($t1_total);
               $array_report[$subject_name][$type_name][$skill_name]['t2']['ct1']['marks'] = calculate_grade($marks_t2_ct1);
               $array_report[$subject_name][$type_name][$skill_name]['t2']['ct2']['marks'] =calculate_grade($marks_t2_ct2);
               $array_report[$subject_name][$type_name][$skill_name]['t2']['total'] = calculate_total_grade($t2_total);
              // echo $t1_total; echo'<br>';
               //echo $t2_total; echo'<br>'; 
              
               $array_report[$subject_name][$type_name][$skill_name]['t1+t2']=  $sub_total;
               $array_report[$subject_name][$type_name][$skill_name]['grade'] = $sub_total;
               //$array_report[$subject_name][$type_name][$skill_name][$marks_t1_ct1][$marks_t1_ct2][$total][$grade] = 0;
              
               $k++;
            }
             //$type[$j]['skill_name'] = $skill;
             $j++;
            }
            }
         }
       //$report[$i]['type'] = $type;
    //print_r($report);
}
//print_r($array_report);
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['ct1']['marks']; echo'<br>';
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'];echo'<br>';
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['total'];echo'<br>';
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['ct1']['marks']; echo'<br>';
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'];echo'<br>';
// echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['total'];echo'<br>';
// echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['grade'];echo'<br>';
//echo $array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1+t2'];

// query to part that is use to access the co-corricular activity parts from cce_co_scho_marks_table table of database on the basic of term_id, class_id and activity_id and skill_id.
 $co_curricular = array();
 
//query to get the activity
   $get_co_curricular_act = "SELECT id,co_activity_name FROM cce_co_scho_activity_table WHERE level=2";
   $exe_co_curricular_act = mysql_query($get_co_curricular_act);
   while($fetch_co_curricular_act = mysql_fetch_array($exe_co_curricular_act))
   {
      $activity = $fetch_co_curricular_act['co_activity_name'];
      //query to get the skill from cce_co_scho_area_table table
       $get_co_curricular_skill = "SELECT id,co_skill FROM cce_co_scho_area_table WHERE co_activity_id=".$fetch_co_curricular_act['id']." ";
       $exe_co_curricular_skill = mysql_query($get_co_curricular_skill);
       $num_rows = mysql_num_rows($exe_co_curricular_skill);
       if($num_rows>0)
       {
       while($fetch_co_curricular_skill = mysql_fetch_array($exe_co_curricular_skill))
             {
              $skill = $fetch_co_curricular_skill['co_skill'];
              //query to get the marks from cce_co_scho_marks_table table
               $get_co_curricular_marks = "SELECT grade FROM cce_co_scho_marks_table WHERE student_id = $student_id AND class_id = $class_id AND term_id = 2 AND activity_id = ".$fetch_co_curricular_act['id']." AND skill_id =".$fetch_co_curricular_skill['id']." ";
               
               $exe_co_curricular_marks = mysql_query($get_co_curricular_marks);
               $fetch_co_curricular_marks = mysql_fetch_array($exe_co_curricular_marks);
               $fetch_co_curricular_marks['grade'];
               $co_curricular[$activity][$skill]['g'] = $fetch_co_curricular_marks['grade'];
               
             }
       }
       else
       {
            //query to get the marks from cce_co_scho_marks_table table
            $get_co_curricular_marks0 = "SELECT grade FROM cce_co_scho_marks_table WHERE student_id = $student_id AND class_id = $class_id AND term_id = 2 AND activity_id = ".$fetch_co_curricular_act['id']." AND skill_id = 0 ";
            
             $exe_co_curricular_marks0 = mysql_query($get_co_curricular_marks0);
             $fetch_co_curricular_marks0 = mysql_fetch_array($exe_co_curricular_marks0);
             
             $co_curricular[$activity][$activity]['g'] = $fetch_co_curricular_marks0['grade'];
       }
       
       
   }

  // print_r($co_curricular);




if(($class_id==19)||($class_id==20)||($class_id==21)||($class_id==22))
{
     ?>
<style>
table,th,td,tr
{
border:1px solid black;
font-size: 11px;
}
</style>
<?php 
      
      
   // To generate the term report of class-id 19 *************************************************************************************************************************************************************************************

echo '<p style="z-index:1; position:absolute; top:4px; left:90px"><font size="6%" ><strong>VIDYA BAL BHAWAN SR. SEC. SCHOOL </strong></font></p>';
    echo'
<table  width="100%" style="border:0px">
<tbody ><tr><td>
<table  width="100%" style="border:0px">
<tbody style="border:0px">
<tr style="border:0px">
<td align="left" width="25%" style="border:0px"></td>
</tr>
<tr style="border:0px">';echo'<td align="left" style="border:0px"position:absolute; top:0px; ><img src="../images/logo/School.jpg" width=40% ></td>';
echo'<br><div ><td  valign="top" width="150%" style="margin-left:-2px; width:100%;border:0px">
<font size="4" style="margin-left:0px; width:100%;" style="font-size:20px"><br>
      Kondli Gharoli Road, Mayur vihar III, Delhi -110096 <br>Phone No.: 22627876,22626299,22621494  Fax-22617007 </font>
      <font size="4" style="margin-left:20px; width:100%;" style="font-size:20px"></font><br>
    <b style="margin-left:-30px; width:100%;"><font size="5">Record of Acadmic Performance(2014-2015)</b>  </font> </td>
</tr>
</tbody>
</table>';

//get the name of the student on that id
$subject_id=0;
$get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB,`Blood Group`
FROM student_user
WHERE sId = ".$_GET['student_id']."";
$exe_s_name=mysql_query($get_s_name);
$fetch_s_name=mysql_fetch_array($exe_s_name);
$name_student=$fetch_s_name[0];
$blood=$fetch_s_name['Blood Group'];

// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.*
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.sId = ".$_GET['student_id']."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 $class_name=$fetch_cls_name['class'];
        $section_name=$fetch_cls_name['section'];
         
echo '
         
<div class="row-fluid">
<div class="widget  span12 clearfix">
<div class="widget-header">
<table  width="100%" id=8   valign="top" class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<tr width="30%" style="border:0px solid black;">
<td align="left" style="border:0px solid black;"><b style="font-size:12px">Student Name:  '.$name_student.'</b></font></td>
<td align="left" style="border:0px solid black;"><b style="font-size:12px">Class : '.$class_name.'       '.$section_name.' </b><b style="margin-left:10px; width:100%;" ><font size="2" style="font-size:12px">Admission No.: '.$fetch_s_name[3].'    </b></font></td>
<td align="left" style="border:0px solid black;"><b style="font-size:12px">Examination Roll.No:</b></td>
</tr>
<tr width="30%" style="border:0px solid black;">
<td align="left" style="border:0px solid black;"><b style="font-size:12px">Fathers Name: '.$fetch_s_name[1].'</b></font></td>
<td align="left"width="30" style="border:0px solid black;"> <b style="font-size:12px">Mothers Name:  '.$fetch_s_name[2].' </b></font></td> 
<td align="left" style="border:0px solid black;"><b style="font-size:12px" >D.O.B.- '.date("jS F Y",strtotime($fetch_s_name[4])).' <b></b></b></font></td>
</tr>
</tbody></table>  							
</div><!-- End widget-header -->	          							
<div class="widget-content" align="center">						
<!-- Table widget -->

<table  class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<thead >                                          
<tr">
<th rowspan="2" colspan="2" width="27%"><font size="3">SUBJECT</font></th> <th colspan="3" align="center">Term-I</th><th colspan="3" align="center" style="border:1px;">Term-II</th><th colspan="2" align="cener">Grand Total</font></th></tr>
<tr><th  align="center" style="border:1px solid black;">C.T.-I</font></th><th  align="center" style="border:1px solid black;">C.T.-II</font></th><th  align="center"  width="5%">TOTAL</font></th><th  align="center" style="border:1px solid black;">C.T.-III</font></th><th  align="center" style="border:1px solid black;">C.T.-IV</font></th><th  align="center"  width="5%">TOTAL</font></th><th  align="center" width="10%">TOTAL</font></th>
<th style="border:1px solid black;" width="8%"> Grade</font></th>										
 </tr>    
 
  </thead> 
  <tbody align="center" style="border:1px solid black;">
        
              
        <tr align="center">
              <td valign="left" width="10%"><b>ENGLISH</b></td> 
              <td valign="" width="10%" colspan="" align="left"><b  style="font-size:12px">Written Assessment</b></td> 
              <td width="8%" style="border:1px solid black;">'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['grade'].'</b></td
          </tr>
         
         <tr align="center">
         <td valign="" width="17%" rowspan="2" align="left"><b  style="font-size:14px">Reading Skill</b></td>
            <td valign="" width="17%" colspan="" align="left">Pronunciation</td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  colspan="" align="left">Fluency</td>
             <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Reading Skills']['Fluency']['grade'].'</b></td>
         </tr>
           
        <tr align="center">
            <td valign="" width="17%" style="font-size:12;" rowspan="4" align="left"><b  style="font-size:14px">Writing Skill</b></td> 
            <td valign="" width="17%" align="left">Creative writing</td>
            
             <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%" align="left">Hand Writing</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  align="left">Grammar</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['grammar']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['grammar']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['grammar']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['grammar']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['grammar']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['grammar']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['grammar']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Writing Skills']['grammar']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  align="left"  style="font-size:12px">Spelling</td>
             <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Writing Skills']['Spelling']['grade'].'</b></td>
         </tr>
      <tr align="center">
            <td valign="" width="17%" rowspan="2" align="left"><b  style="font-size:14px">Speaking Skill</b></td>
            <td valign="top" width="17%"  align="left">Conversation</td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['grade'].'</b></td>
        </tr>
       <tr align="center">
           <td valign="" width="17%"  align="left"> Recitation</td>
           <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['grade'].'</b></td>
        </tr>
       <tr align="center">
          <td valign="" width="17%" colspan="2" align="left" style="font-size:14px"><b>Dictation</b></td>
          <td width="8%">'.$array_report['ENGLISH']['Dictation']['Dictation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Dictation']['Dictation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Dictation']['Dictation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Dictation']['Dictation']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Dictation']['Dictation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Dictation']['Dictation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Dictation']['Dictation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Dictation']['Dictation']['grade'].'</b></td>
       </tr>
       
        <tr align="center">
              <td valign="center" width="10%"><b>HINDI</b></td> <td valign="left" width="10%" align="left"><b  style="font-size:12px">Written Assessment</b></td> 
                <td width="8%" style="border:1px solid black;">'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['HINDI']['Written Assesment']['Written Assesment']['grade'].'</b></td
          </tr>
         
          <tr align="center">
         <td valign="" width="17%" rowspan="2" align="left"><b  style="font-size:14px">Reading Skill</b></td>
            <td valign="" width="17%" colspan="" align="left">Pronunciation</td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Reading Skills']['Pronunciation']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  colspan="" align="left">Fluency</td>
             <td width="8%">'.$array_report['HINDI']['Reading Skills']['Fluency']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Fluency']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Fluency']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Fluency']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Fluency']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Fluency']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Fluency']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Reading Skills']['Fluency']['grade'].'</b></td>
         </tr>
           
        <tr align="center">
            <td valign="" width="17%" style="font-size:12;" rowspan="4" align="left"><b  style="font-size:14px">Writing Skill</b></td> 
            <td valign="" width="17%" align="left">Creative writing</td>
            
             <td width="8%">'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Writing Skills']['Creative Writing']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%" align="left">Hand Writing</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Hand writing']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Hand writing']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Hand writing']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Hand writing']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Hand writing']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Hand writing']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Hand writing']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Writing Skills']['Hand writing']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  align="left">Grammar</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['grammar']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['grammar']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['grammar']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['grammar']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['grammar']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['grammar']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['grammar']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Writing Skills']['grammar']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  align="left"  style="font-size:12px">Spelling</td>
             <td width="8%">'.$array_report['HINDI']['Writing Skills']['Spelling']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Spelling']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Spelling']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Spelling']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Spelling']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Spelling']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Spelling']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Writing Skills']['Spelling']['grade'].'</b></td>
         </tr>
      <tr align="center">
            <td valign="" width="17%" rowspan="2" align="left"><b  style="font-size:14px">Speaking Skill</b></td>
            <td valign="top" width="17%"  align="left">Conversation</td>
            <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Conversation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Conversation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Conversation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Conversation']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Conversation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Conversation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Conversation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Speaking Skills']['Conversation']['grade'].'</b></td>
        </tr>
       <tr align="center">
           <td valign="" width="17%"  align="left"> Recitation</td>
           <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Recitation']['t1']['ct1']['marks'].'</td>
           <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Recitation']['t1']['ct2']['marks'].'</td>
           <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Recitation']['t1']['total'].'</b></td>
           <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Recitation']['t2']['ct1']['marks'].'</td>
           <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Recitation']['t2']['ct2']['marks'].'</b></td>
          <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Recitation']['t2']['total'].'</b></td>
           <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Recitation']['t1+t2'].'</b></td>
           <td><b>'.$array_report['HINDI']['Speaking Skills']['Recitation']['grade'].'</b></td>
        </tr>
       <tr align="center">
          <td valign="" width="17%" colspan="2" align="left" style="font-size:14px"><b>Dictation</b></td>
          <td width="8%">'.$array_report['HINDI']['Dictation']['Dictation']['t1']['ct1']['marks'].'</td>
          <td width="8%">'.$array_report['HINDI']['Dictation']['Dictation']['t1']['ct2']['marks'].'</td>
          <td width="8%"><b>'.$array_report['HINDI']['Dictation']['Dictation']['t1']['total'].'</b></td>
          <td width="8%">'.$array_report['HINDI']['Dictation']['Dictation']['t2']['ct1']['marks'].'</td>
          <td width="8%">'.$array_report['HINDI']['Dictation']['Dictation']['t2']['ct2']['marks'].'</b></td>
          <td width="8%"><b>'.$array_report['HINDI']['Dictation']['Dictation']['t2']['total'].'</b></td>
          <td width="8%"><b>'.$array_report['HINDI']['Dictation']['Dictation']['t1+t2'].'</b></td>
          <td><b>'.$array_report['HINDI']['Dictation']['Dictation']['grade'].'</b></td>
       </tr>
     
  <tr align="center">
          <td valign="center" width="10%"><b>MATHS</b></td> 
          <td valign="top" width="17%" align="left"><b  style="font-size:12px">Written Assessment</b></td>
          <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
          <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
          <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['grade'].'</b></td>
   </tr>
  <tr align="center">
          <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Concept</b></td>
          <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Concept']['Concept']['t1']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Concept']['Concept']['t1']['ct2']['marks'].'</td>
         <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Concept']['Concept']['t1']['total'].'</b></td>
         <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Concept']['Concept']['t2']['ct1']['marks'].'</td>
         <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Concept']['Concept']['t2']['ct2']['marks'].'</b></td>
         <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Concept']['Concept']['t2']['total'].'</b></td>
         <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Concept']['Concept']['t1+t2'].'</b></td>
        <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Concept']['Concept']['grade'].'</b></td>
     </tr>
   <tr align="center">
       <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Tables</b></td>
       <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Concept']['Concept']['t1']['ct1']['marks'].'</td>
       <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Tables']['Tables']['t1']['ct2']['marks'].'</td>
      <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Tables']['Tables']['t1']['total'].'</b></td>
     <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Tables']['Tables']['t2']['ct1']['marks'].'</td>
     <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Tables']['Tables']['t2']['ct2']['marks'].'</b></td>
     <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Tables']['Tables']['t2']['total'].'</b></td>
     <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Tables']['Tables']['t1+t2'].'</b></td>
    <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Tables']['Tables']['grade'].'</b></td>
    </tr>
   <tr align="center">
        <td width="17%" colspan="2" align="left" style="font-size:14px"><b>Mental Ability</b></td>
        <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t1']['ct1']['marks'].'</td>
       <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t1']['ct2']['marks'].'</td>
       <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t1']['total'].'</b></td>
       <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Mental Ability']['Mental Ability']['grade'].'</b></td>
     </tr>
  <tr align="center">
           <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Activity</b></td>
           <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Activity']['Activity']['t1']['ct1']['marks'].'</td>
           <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Activity']['Activity']['t1']['ct2']['marks'].'</td>
           <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Activity']['Activity']['t1']['total'].'</b></td>
           <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Activity']['Activity']['t2']['ct1']['marks'].'</td>
           <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Activity']['Activity']['t2']['ct2']['marks'].'</b></td>
           <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Activity']['Activity']['t2']['total'].'</b></td>
           <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Activity']['Activity']['t1+t2'].'</b></td>
          <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Activity']['Activity']['grade'].'</b></td>
 </tr>  
            

<tr align="center">
       
         <td width="10%"><b>SCIENCE</b></td>
         <td valign="top" width="17%" align="left"><b style="font-size:12px">Written Assessment</b></td> 
               <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Written Assesment']['Written Assesment']['grade'].'</b></td
</tr>
<tr align="center">
                 <td valign="top" width="17%" colspan="2" align="left"  style="font-size:14px"><b>Group Discussion</b></td> 
             <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Group Discussion']['Group Discussion']['grade'].'</b></td>
</tr>
<tr align="center">
           <td valign="top" width="17%" colspan="2" align="left"  style="font-size:14px"><b>Activity</b></td>
                             
            <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Activity']['Activity']['t1']['ct1']['marks'].'</td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Activity']['Activity']['t1']['ct2']['marks'].'</td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Activity']['Activity']['t1']['total'].'</b></td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Activity']['Activity']['t2']['ct1']['marks'].'</td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Activity']['Activity']['t2']['ct2']['marks'].'</b></td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Activity']['Activity']['t2']['total'].'</b></td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Activity']['Activity']['t1+t2'].'</b></td>
            <td style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Activity']['Activity']['grade'].'</b></td>
</tr>
     <tr align="center">
        <td valign="top" width="17%" colspan="2" align="left"  style="font-size:14px"><b>Project</b>
              <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Project']['Project']['t1']['ct1']['marks'].'</td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Project']['Project']['t1']['ct2']['marks'].'</td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Project']['Project']['t1']['total'].'</b></td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Project']['Project']['t2']['ct1']['marks'].'</td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SCIENCE']['Project']['Project']['t2']['ct2']['marks'].'</b></td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Project']['Project']['t2']['total'].'</b></td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Project']['Project']['t1+t2'].'</b></td>
            <td style="border:1px solid black;"><b>'.$array_report['SCIENCE']['Project']['Project']['grade'].'</b></td>
     </tr>  
       <tr align="center">
        <td valign="center" width="10%"><b>SST</b></td> <td valign="top" width="17%" align="left" style="font-size:12px"><b>Written Assessment<b></td> 
         <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
          <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
          <td style="border:1px solid black;"><b>'.$array_report['SST']['Written Assesment']['Written Assesment']['grade'].'</b></td>
   </tr>
 <tr align="center">
                  <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Environmental Sensitivity</b></td> 
                <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['t1']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['t1']['ct2']['marks'].'</td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['t1']['total'].'</b></td>
          <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['t2']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['t2']['ct2']['marks'].'</b></td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['t2']['total'].'</b></td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['t1+t2'].'</b></td>
          <td style="border:1px solid black;"><b>'.$array_report['SST']['Environmental Sensitivity']['Environmental Sensitivity']['grade'].'</b></td>
      </tr>
                
 <tr align="center">
         <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Group discussion</b></td>
             <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Group Discussion']['Group Discussion']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Group Discussion']['Group Discussion']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Group Discussion']['Group Discussion']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Group Discussion']['Group Discussion']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Group Discussion']['Group Discussion']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Group Discussion']['Group Discussion']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Group Discussion']['Group Discussion']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['SST']['Group Discussion']['Group Discussion']['grade'].'</b></td>
       </tr>
                  
   <tr align="center">
                <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Activity</b></td> 
                <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Activity']['Activity']['t1']['ct1']['marks'].'</td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Activity']['Activity']['t1']['ct2']['marks'].'</td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Activity']['Activity']['t1']['total'].'</b></td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Activity']['Activity']['t2']['ct1']['marks'].'</td>
            <td width="8%" style="border:1px solid black;">'.$array_report['SST']['Activity']['Activity']['t2']['ct2']['marks'].'</b></td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Activity']['Activity']['t2']['total'].'</b></td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['SST']['Activity']['Activity']['t1+t2'].'</b></td>
            <td style="border:1px solid black;"><b>'.$array_report['SST']['Activity']['Activity']['grade'].'</b></td>
       </tr>                       


  <tr align="center">
         <td valign="center" width="10%"><b>COMPUTER</b></td> 
         <td valign="top" width="17%" align="left"><b  style="font-size:12px">Written Assessment</b></td> 
          <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['grade'].'</b></td
   </tr>
                
  <tr align="center">
              <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Practical</b></td> 
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Practical']['Practical']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Practical']['Practical']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Practical']['Practical']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Practical']['Practical']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Practical']['Practical']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Practical']['Practical']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Practical']['Practical']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Practical']['Practical']['grade'].'</b></td>
</tr>
   <tr align="center">
            <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Viva</b></td> 
           <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Viva']['Viva']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Viva']['Viva']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Viva']['Viva']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Viva']['Viva']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Viva']['Viva']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Viva']['Viva']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Viva']['Viva']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Viva']['Viva']['grade'].'</b></td>
    </tr>
   <tr align="center">
            <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Project</b></td> 
           <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Project']['Project']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Project']['Project']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Project']['Project']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Project']['Project']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Project']['Project']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Project']['Project']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Project']['Project']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Project']['Project']['grade'].'</b></td>
   </tr>  
               

   <tr align="center">
        <td valign="center" width="10%"><b>G.K</b></td> <td valign="top" width="17%" align="left"><b  style="font-size:12px">Written Assessment<b></td> 
          <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['GK']['Written Assesment']['Written Assesment']['grade'].'</b></td
   </tr>
<tr align="center">
                  <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Current Knowledge</b></td>
                  <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t2']['ct2']['marks'].'</b></td>
               <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['GK']['Current Knowledge']['Current Knowledge']['grade'].'</b></td>
</tr>
        

';
echo'<tr  width="100%">
<td align="left"  colspan="10"><b  style="font-size:18px"><font size="3">Attendance:</font></b><b style="margin-left:160px; width:100%;"  style="font-size:18px"><font size="3">Blood Group:</b> '.$blood.'<b style="margin-left:100px; width:100%;" style="font-size:18px"><font size="3">Height:</b><b style="margin-left:100px; width:100%;" style="font-size:18px"><font size="3">Weight:</b></td> 

</tr>
</tbody></table>
';                      
      $get_subject = "
      SELECT DISTINCT subject.subject,subject.subject_id,scho_activity_table.*,types_of_activities.*
      FROM subject
      INNER JOIN scho_marks
      ON subject.subject_id =scho_marks.subject_id
      INNER JOIN scho_activity_table
      ON scho_activity_table.id =scho_marks.scho_act_id
      INNER JOIN types_of_activities
      ON types_of_activities.Id=scho_activity_table.type_id
      INNER JOIN class
      ON class.sId=scho_marks.student_id
      WHERE scho_marks.student_id=".$_GET['student_id']." ";

        echo'<table class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
        <thead>
              <tr style="border:1px solid black;">
                <th align="center" colspan="6" style="border:1px solid black;"><b  style="font-size:14px">Co-Curricular Activities</b></th><th align="center" style="border:1px solid black;" colspan="4"><b  style="font-size:14px">Personality  Development</b></th></tr>
              <tr> <th align="center"  style="border:1px solid black;" colspan="2"><b> Activities</b></th>
                <th align="center" style="border:1px solid black;"><b>GRADE</b></font></th>
                <th align="center"  style="border:1px solid black;" colspan="2"><b>Activities</b></th>
                <th align="center" style="border:1px solid black;"><b>GRADE</b></font></th>
                <th align="center" style="border:1px solid black;" ><b>Activities</b></th>
                 <th align="center"><b>GRADE</b></th>
                 <th align="center" style="border:1px solid black;" ><b>Activities</b></th>
                 <th align="center"><b>GRADE</b></th>
                </tr>  </thead>';
      
     echo'<tr >
       <th rowspan="3" align="center" width="10%">GAMES<td>Team Spirit</td>
       <td align="center"><b>'.$co_curricular['GAMES']['Team spirit']['g'].'</b></td>
       <th rowspan="3" align="left" width="10%">ART & CRAFT<td>Neatness</td><td align="center"><b>'.$co_curricular['ART & CRAFT']['Neatness']['g'].'</b></td>
       <td>Attitude</td></td><td align="center"><b>'.$co_curricular['Attitude']['Attitude']['g'].'</b></td>
       <td>Confidence</td><td align="center"><b>'.$co_curricular['Confidence']['Confidence']['g'].'</b></td>
       </tr>
       <tr>
       <td>Discipline</td><td align="center"><b>'.$co_curricular['GAMES']['Discipline']['g'].'</b></td>
       <td>Colouring</td><td align="center"><b>'.$co_curricular['ART & CRAFT']['Coloring']['g'].'</b></td>
       <td>Care of Belongings</td><td align="center"><b>'.$co_curricular['Care of belongings']['Care of belongings']['g'].'</b></td>
       <td>Discipline</td><td align="center"><b>'.$co_curricular['Discipline']['Discipline']['g'].'</b></td></th>
       <tr>
         <td>Enthusiasm</td>
         <td align="center"><b>'.$co_curricular['GAMES']['Enthusiasm']['g'].'</b></td>
         <td>Book Work</td><td align="center"><b>'.$co_curricular['ART & CRAFT']['Book Work']['g'].'</b></td>
         <td>Regularity & Punctuality</td><td align="center"><b>'.$co_curricular['Regularity & Punctuality']['Regularity & Punctuality']['g'].'</b></td><td>Respect for School Property</td><td align="center"><b>'.$co_curricular['Respect for school property']['Respect for school property']['g'].'</b></td></th>
       </tr>
       <th align="center">MUSIC & DANCE</th><td>Rhythm</td> <td align="center"><b>'.$co_curricular['MUSIC & DANCE']['Rhythm']['g'].'</b></td>
       <td colspan="2">Interest</td><td align="center"><b>'.$co_curricular['MUSIC & DANCE']['INTEREST']['g'].'</b></td>
       <td>Sharing & Caring</td><td align="center"><b>'.$co_curricular['Sharing & Caring']['Sharing & Caring']['g'].'</b></td>
       <td>Imaginative Skills</td><td align="center"><b>'.$co_curricular['Imaginative skills']['Imaginative skills']['g'].'</b></td></tr></tr>';
    echo'  <table width="100%" align="left" id="77" height=40 style="border:0px"><tbody>
<td  style="float:left;border:0px" valign="bottom"><b><font size="2"><b >Parent\'s signature</b><font size="2"><b  style="margin-left:140px; width:100%;">Class Teacher\'s signature</font></b><b  style="margin-left:160px; width:100%;"><font size="2">Principal\'s signature</b></font></td>
</tbody></table>';
    
echo'</tbody></table>';
}

elseif(($class_id==7)||($class_id==8)||($class_id==9)||($class_id==10)||($class_id==11)||($class_id==12)||($class_id==13)||($class_id==14)||($class_id==15)||($class_id==16)||($class_id==17)||($class_id==18))
 {
//    ?>
<style>
table,th,td,tr
{
border:1px solid black;
font-size: 11px;
}
</style>
<?php
   echo '<p style="z-index:1; position:absolute; top:3px; left:90px"><font size="6%" ><strong>VIDYA BAL BHAWAN SR. SEC. SCHOOL </strong></font></p>';
    echo'
<table  width="100%" style="border:0px">
<tbody ><tr><td>
<table  width="100%" style="border:0px">
<tbody style="border:0px">
<tr style="border:0px">
<td align="left" width="25%" style="border:0px"></td>
</tr>
<tr style="border:0px">';echo'<td align="left" style="border:0px"position:absolute; top:0px; ><img src="../images/logo/School.jpg" width=47% ></td>';
echo'<br><div ><td  valign="top" width="150%" style="margin-left:-2px; width:100%;border:0px">
<font size="4" style="margin-left:0px; width:100%;" style="font-size:20px"><br>
      Kondli Gharoli Road, Mayur vihar III, Delhi -110096 <br>Phone No.: 627876,22626299,22621494  Fax-22617007 </font>
      <font size="4" style="margin-left:20px; width:100%;" style="font-size:20px"></font><br>
    <b style="margin-left:-30px; width:100%;"><font size="5">Record of Acadmic Performance(2014-2015)</b>  </font> </td>
</tr>
</tbody>
</table>';


//get the name of the student on that id
$subject_id=0;
$get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB,`Blood Group`
FROM student_user
WHERE sId = ".$_GET['student_id']."";
$exe_s_name=mysql_query($get_s_name);
$fetch_s_name=mysql_fetch_array($exe_s_name);
$name_student=$fetch_s_name[0];
$blood=$fetch_s_name['Blood Group'];

// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.*
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.sId = ".$_GET['student_id']."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 $class_name=$fetch_cls_name['class'];
        $section_name=$fetch_cls_name['section'];
         
echo '
         
<div class="row-fluid">
<div class="widget  span12 clearfix">
<div class="widget-header">
<table  width="100%" id=8   valign="top" class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<tbody >
<tr width="30%" style="border:0px solid black;">
<td align="left" style="border:0px solid black;"><b style="font-size:14px">Student Name:  '.$name_student.'</b></font></td>
<td align="left" style="border:0px solid black;"><b style="font-size:14px">Class : '.$class_name.'       '.$section_name.' </b><b style="margin-left:10px; width:100%;" ><font size="2" style="font-size:14px">Admission No.: '.$fetch_s_name[3].'    </b></font></td>
<td align="left" style="border:0px solid black;"><b style="font-size:14px">Examination Roll.No:</b></td>
</tr>
<tr width="30%" style="border:0px solid black;">
<td align="left" style="border:0px solid black;"><b style="font-size:14px">Fathers Name: '.$fetch_s_name[1].'</b></font></td>
<td align="left"width="30" style="border:0px solid black;"> <b style="font-size:14px">Mothers Name:  '.$fetch_s_name[2].' </b></font></td> 
<td align="left" style="border:0px solid black;"><b style="font-size:14px" >D.O.B.- '.date("jS F Y",strtotime($fetch_s_name[4])).' <b></b></b></font></td>
</tr>
</tbody></table>  							
       </div><!-- End widget-header -->	          							
<div class="widget-content" align="center">						
<!-- Table widget -->';


echo '<table  class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<thead >                                          
<tr">
<th rowspan="2" colspan="2" width="27%"><font size="3">SUBJECT</font></th> <th colspan="3" align="center">Term-I</th><th colspan="3" align="center" style="border:1px;">Term-II</th><th colspan="2" align="cener">Grand Total</font></th></tr>
<tr><th  align="center" style="border:1px solid black;">C.T.-I</font></th><th  align="center" style="border:1px solid black;">C.T.-II</font></th><th  align="center"  width="5%">TOTAL</font></th><th  align="center" style="border:1px solid black;">C.T.-III</font></th><th  align="center" style="border:1px solid black;">C.T.-IV</font></th><th  align="center" width="5%">TOTAL</font></th><th  align="center" width="6%">TOTAL</font></th>
<th style="border:1px solid black;"  width="8%"> Grade</font></th>										
 </tr>    																
</thead> 
 <tbody align="center" style="border:1px solid black;">
        
           
         <tr align="center">
              <td valign="left" width="10%"><b>ENGLISH</b></td> 
              <td valign="" width="10%" colspan="" align="left"><b  style="font-size:12px">Written Assessment</b></td> 
              <td width="8%" style="border:1px solid black;">'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['ENGLISH']['Written Assesment']['Written Assesment']['grade'].'</b></td
          </tr>
         
         <tr align="center">
         <td valign="" width="17%" rowspan="2" align="left"><b  style="font-size:14px">Reading Skill</b></td>
            <td valign="" width="17%" colspan="" align="left">Pronunciation</td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Reading Skills']['Pronunciation']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  colspan="" align="left">Fluency</td>
             <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t2']['ct1']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Reading Skills']['Fluency']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Reading Skills']['Fluency']['grade'].'</b></td>
         </tr>
           
        <tr align="center">
            <td valign="" width="17%" style="font-size:12;" rowspan="4" align="left"><b  style="font-size:14px">Writing Skill</b></td> 
            <td valign="" width="17%" align="left">Creative writing</td>
            
             <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t2']['ct1']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Writing Skills']['Creative Writing']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%" align="left">Hand Writing</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t2']['ct1']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Writing Skills']['Hand writing']['grade'].'</b></td>
         </tr>
         <tr align="center">
            <td valign="" width="17%"  align="left">Grammar</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['grammar']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['grammar']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['grammar']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['grammar']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['grammar']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['grammar']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['grammar']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Writing Skills']['grammar']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  align="left"  style="font-size:12px">Spelling</td>
             <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t2']['ct1']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Writing Skills']['Spelling']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Writing Skills']['Spelling']['grade'].'</b></td>
         </tr>
      <tr align="center">
            <td valign="" width="17%" rowspan="2" align="left"><b  style="font-size:14px">Speaking Skill</b></td>
            <td valign="top" width="17%"  align="left">Conversation</td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t2']['ct1']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Speaking Skills']['Conversation']['grade'].'</b></td>
        </tr>
       <tr align="center">
           <td valign="" width="17%"  align="left"> Recitation</td>
           <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t2']['ct1']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Speaking Skills']['Recitation']['grade'].'</b></td>
        </tr>
       <tr align="center">
          <td valign="" width="17%" colspan="2" align="left" style="font-size:14px"><b>Dictation</b></td>
          <td width="8%">'.$array_report['ENGLISH']['Dictation']['Dictation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['ENGLISH']['Dictation']['Dictation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Dictation']['Dictation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['ENGLISH']['Dictation']['Dictation']['t2']['ct1']['marks'].'</td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Dictation']['Dictation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Dictation']['Dictation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['ENGLISH']['Dictation']['Dictation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['ENGLISH']['Dictation']['Dictation']['grade'].'</b></td>
       </tr>
       
        <tr align="center">
              <td valign="center" width="10%"><b>HINDI</b></td> <td valign="left" width="10%" align="left"><b  style="font-size:12px">Written Assessment</b></td> 
                <td width="8%" style="border:1px solid black;">'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['HINDI']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['HINDI']['Written Assesment']['Written Assesment']['grade'].'</b></td
          </tr>
         
          <tr align="center">
         <td valign="" width="17%" rowspan="2" align="left"><b  style="font-size:14px">Reading Skill</b></td>
            <td valign="" width="17%" colspan="" align="left">Pronunciation</td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Pronunciation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Reading Skills']['Pronunciation']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  colspan="" align="left">Fluency</td>
             <td width="8%">'.$array_report['HINDI']['Reading Skills']['Fluency']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Fluency']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Fluency']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Reading Skills']['Fluency']['t2']['ct1']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Fluency']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Fluency']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Reading Skills']['Fluency']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Reading Skills']['Fluency']['grade'].'</b></td>
         </tr>
           
        <tr align="center">
            <td valign="" width="17%" style="font-size:12;" rowspan="4" align="left"><b  style="font-size:14px">Writing Skill</b></td> 
            <td valign="" width="17%" align="left">Creative writing</td>
            
             <td width="8%">'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t2']['ct1']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Creative Writing']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Writing Skills']['Creative Writing']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%" align="left">Hand Writing</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Hand writing']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Hand writing']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Hand writing']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Hand writing']['t2']['ct1']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Hand writing']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Hand writing']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Hand writing']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Writing Skills']['Hand writing']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  align="left">Grammar</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['grammar']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['grammar']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['grammar']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['grammar']['t2']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['grammar']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['grammar']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['grammar']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Writing Skills']['grammar']['grade'].'</b></td>
         </tr>
        <tr align="center">
            <td valign="" width="17%"  align="left"  style="font-size:12px">Spelling</td>
             <td width="8%">'.$array_report['HINDI']['Writing Skills']['Spelling']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Spelling']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Spelling']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Writing Skills']['Spelling']['t2']['ct1']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Spelling']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Spelling']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Writing Skills']['Spelling']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Writing Skills']['Spelling']['grade'].'</b></td>
         </tr>
      <tr align="center">
            <td valign="" width="17%" rowspan="2" align="left"><b  style="font-size:14px">Speaking Skill</b></td>
            <td valign="top" width="17%"  align="left">Conversation</td>
            <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Conversation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Conversation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Conversation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Conversation']['t2']['ct1']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Conversation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Conversation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Conversation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Speaking Skills']['Conversation']['grade'].'</b></td>
        </tr>
       <tr align="center">
           <td valign="" width="17%"  align="left"> Recitation</td>
           <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Recitation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Recitation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Recitation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Speaking Skills']['Recitation']['t2']['ct1']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Recitation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Recitation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Speaking Skills']['Recitation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Speaking Skills']['Recitation']['grade'].'</b></td>
        </tr>
       <tr align="center">
          <td valign="" width="17%" colspan="2" align="left" style="font-size:14px"><b>Dictation</b></td>
          <td width="8%">'.$array_report['HINDI']['Dictation']['Dictation']['t1']['ct1']['marks'].'</td>
            <td width="8%">'.$array_report['HINDI']['Dictation']['Dictation']['t1']['ct2']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Dictation']['Dictation']['t1']['total'].'</b></td>
            <td width="8%">'.$array_report['HINDI']['Dictation']['Dictation']['t2']['ct1']['marks'].'</td>
            <td width="8%"><b>'.$array_report['HINDI']['Dictation']['Dictation']['t2']['ct2']['marks'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Dictation']['Dictation']['t2']['total'].'</b></td>
            <td width="8%"><b>'.$array_report['HINDI']['Dictation']['Dictation']['t1+t2'].'</b></td>
            <td><b>'.$array_report['HINDI']['Dictation']['Dictation']['grade'].'</b></td>
       </tr>
     
  <tr align="center">
          <td valign="center" width="10%"><b>MATHS</b></td> 
          <td valign="top" width="17%" align="left"><b  style="font-size:12px">Written Assessment</b></td>
          <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Written Assesment']['Written Assesment']['grade'].'</b></td>
   </tr>
  <tr align="center">
          <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Concept</b></td>
           <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Concept']['Concept']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Concept']['Concept']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Concept']['Concept']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Concept']['Concept']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Concept']['Concept']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Concept']['Concept']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Concept']['Concept']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Concept']['Concept']['grade'].'</b></td>
     </tr>
   <tr align="center">
         <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Tables</b></td>
          <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Concept']['Concept']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Tables']['Tables']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Tables']['Tables']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Tables']['Tables']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Tables']['Tables']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Tables']['Tables']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Tables']['Tables']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Tables']['Tables']['grade'].'</b></td>
    </tr>
   <tr align="center">
        <td width="17%" colspan="2" align="left" style="font-size:14px"><b>Mental Ability</b></td>
            <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Mental Ability']['Mental Ability']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Mental Ability']['Mental Ability']['grade'].'</b></td>
     </tr>
  <tr align="center">
           <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Activity</b></td>
           <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Activity']['Activity']['t1']['ct1']['marks'].'</td>
           <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Activity']['Activity']['t1']['ct2']['marks'].'</td>
           <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Activity']['Activity']['t1']['total'].'</b></td>
           <td width="8%" style="border:1px solid black;">'.$array_report['MATHS']['Activity']['Activity']['t2']['ct1']['marks'].'</td>
           <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Activity']['Activity']['t2']['ct2']['marks'].'</b></td>
           <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Activity']['Activity']['t2']['total'].'</b></td>
           <td width="8%" style="border:1px solid black;"><b>'.$array_report['MATHS']['Activity']['Activity']['t1+t2'].'</b></td>
          <td style="border:1px solid black;"><b>'.$array_report['MATHS']['Activity']['Activity']['grade'].'</b></td>
 </tr>  
            
<tr align="center">
       
         <td width="10%"><b>EVS</b></td><td valign="top" width="17%" align="left"><b  style="font-size:12px">Written Assessment</b></td>
           <td width="8%" style="border:1px solid black;">'.$array_report['EVS']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['EVS']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['EVS']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['EVS']['Written Assesment']['Written Assesment']['grade'].'</b></td
      </tr>
      <tr align="center">
                 <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Group Discussion</b></td> 
                <td width="8%" style="border:1px solid black;">'.$array_report['EVS']['Group Discussion']['Group Discussion']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['EVS']['Group Discussion']['Group Discussion']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Group Discussion']['Group Discussion']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['EVS']['Group Discussion']['Group Discussion']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Group Discussion']['Group Discussion']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Group Discussion']['Group Discussion']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Group Discussion']['Group Discussion']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['EVS']['Group Discussion']['Group Discussion']['grade'].'</b></td>
       </tr>
     <tr align="center">
                   <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Activity</b></td>
                    <td width="8%" style="border:1px solid black;">'.$array_report['EVS']['Activity']['Activity']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['EVS']['Activity']['Activity']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Activity']['Activity']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['EVS']['Activity']['Activity']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Activity']['Activity']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Activity']['Activity']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Activity']['Activity']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['EVS']['Activity']['Activity']['grade'].'</b></td>
     </tr>
     <tr align="center">
          <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Project</b></td> 
          <td width="8%" style="border:1px solid black;">'.$array_report['EVS']['Project']['Project']['t1']['ct1']['marks'].'</td>
          <td width="8%" style="border:1px solid black;">'.$array_report['EVS']['Project']['Project']['t1']['ct2']['marks'].'</td>
          <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Project']['Project']['t1']['total'].'</b></td>
           <td width="8%" style="border:1px solid black;">'.$array_report['EVS']['Project']['Project']['t2']['ct1']['marks'].'</td>
           <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Project']['Project']['t2']['ct2']['marks'].'</b></td>
            <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Project']['Project']['t2']['total'].'</b></td>
           <td width="8%" style="border:1px solid black;"><b>'.$array_report['EVS']['Project']['Project']['t1+t2'].'</b></td>
          <td style="border:1px solid black;"><b>'.$array_report['EVS']['Project']['Project']['grade'].'</b></td>
     </tr>  
               


 <tr align="center">
         <td valign="center" width="10%"><b>COMPUTER</b></td> 
         <td valign="top" width="17%" align="left"><b  style="font-size:12px">Written Assessment</b></td> 
          <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Written Assesment']['Written Assesment']['grade'].'</b></td
   </tr>
                
  <tr align="center">
              <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Practical</b></td> 
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Practical']['Practical']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Practical']['Practical']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Practical']['Practical']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Practical']['Practical']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Practical']['Practical']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Practical']['Practical']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Practical']['Practical']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Practical']['Practical']['grade'].'</b></td>
   </tr>
   <tr align="center">
            <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Viva</b></td> 
           <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Viva']['Viva']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Viva']['Viva']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Viva']['Viva']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Viva']['Viva']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Viva']['Viva']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Viva']['Viva']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Viva']['Viva']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Viva']['Viva']['grade'].'</b></td>
    </tr>
   <tr align="center">
            <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Project</b></td> 
           <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Project']['Project']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Project']['Project']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Project']['Project']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['COMPUTER']['Project']['Project']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Project']['Project']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Project']['Project']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Project']['Project']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['COMPUTER']['Project']['Project']['grade'].'</b></td>
   </tr>  
               

   <tr align="center">
        <td valign="center" width="10%"><b>G.K</b></td> <td valign="top" width="17%" align="left"><b  style="font-size:12px">Written Assessment<b></td> 
          <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Written Assesment']['Written Assesment']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Written Assesment']['Written Assesment']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Written Assesment']['Written Assesment']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Written Assesment']['Written Assesment']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Written Assesment']['Written Assesment']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Written Assesment']['Written Assesment']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Written Assesment']['Written Assesment']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['GK']['Written Assesment']['Written Assesment']['grade'].'</b></td
   </tr>
<tr align="center">
                  <td valign="top" width="17%" colspan="2" align="left" style="font-size:14px"><b>Current Knowledge</b></td>
                  <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t1']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t1']['ct2']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t1']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;">'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t2']['ct1']['marks'].'</td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t2']['ct2']['marks'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t2']['total'].'</b></td>
              <td width="8%" style="border:1px solid black;"><b>'.$array_report['GK']['Current Knowledge']['Current Knowledge']['t1+t2'].'</b></td>
              <td style="border:1px solid black;"><b>'.$array_report['GK']['Current Knowledge']['Current Knowledge']['grade'].'</b></td
</tr>
                


<tr  width="100%">
<td align="left"  colspan="10"><b  style="font-size:18px"><font size="3">Attendance:</font></b><b style="margin-left:160px; width:100%;"  style="font-size:18px"><font size="3">Blood Group:</b> '.$blood.'<b style="margin-left:100px; width:100%;" style="font-size:18px"><font size="3">Height:</b><b style="margin-left:100px; width:100%;" style="font-size:18px"><font size="3">Weight:</b></td> 

</tr>
</tbody></table>
';                      
      $get_subject = "
      SELECT DISTINCT subject.subject,subject.subject_id,scho_activity_table.*,types_of_activities.*
      FROM subject
      INNER JOIN scho_marks
      ON subject.subject_id =scho_marks.subject_id
      INNER JOIN scho_activity_table
      ON scho_activity_table.id =scho_marks.scho_act_id
      INNER JOIN types_of_activities
      ON types_of_activities.Id=scho_activity_table.type_id
      INNER JOIN class
      ON class.sId=scho_marks.student_id
      WHERE scho_marks.student_id=".$_GET['student_id']." ";

      echo'<table class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
        <thead>
              <tr style="border:1px solid black;">
                <th align="center" colspan="6" style="border:1px solid black;"><b  style="font-size:14px">Co-Curricular Activities</b></th><th align="center" style="border:1px solid black;" colspan="4"><b  style="font-size:14px">Personality  Development</b></th></tr>
              <tr> <th align="center"  style="border:1px solid black;" colspan="2"><b> Activities</b></th>
                <th align="center" style="border:1px solid black;"><b>GRADE</b></font></th>
                <th align="center"  style="border:1px solid black;" colspan="2"><b>Activities</b></th>
                <th align="center" style="border:1px solid black;"><b>GRADE</b></font></th>
                <th align="center" style="border:1px solid black;" ><b>Activities</b></th>
                 <th align="center"><b>GRADE</b></th>
                 <th align="center" style="border:1px solid black;" ><b>Activities</b></th>
                 <th align="center"><b>GRADE</b></th>
                </tr>  </thead>';
      
     echo'<tr >
       <th rowspan="3" align="center" width="10%">GAMES<td>Team Spirit</td>
       <td align="center"><b>'.$co_curricular['GAMES']['Team spirit']['g'].'</b></td>
       <th rowspan="3" align="left" width="10%">ART & CRAFT<td>Neatness</td><td align="center"><b>'.$co_curricular['ART & CRAFT']['Neatness']['g'].'</b></td>
       <td>Attitude</td></td><td align="center"><b>'.$co_curricular['Attitude']['Attitude']['g'].'</b></td>
       <td>Confidence</td><td align="center"><b>'.$co_curricular['Confidence']['Confidence']['g'].'</b></td>
       </tr>
       <tr>
       <td>Discipline</td><td align="center"><b>'.$co_curricular['GAMES']['Discipline']['g'].'</b></td>
       <td>Colouring</td><td align="center"><b>'.$co_curricular['ART & CRAFT']['Coloring']['g'].'</b></td>
       <td>Care of Belongings</td><td align="center"><b>'.$co_curricular['Care of belongings']['Care of belongings']['g'].'</b></td>
       <td>Discipline</td><td align="center"><b>'.$co_curricular['Discipline']['Discipline']['g'].'</b></td></th>
       <tr>
         <td>Enthusiasm</td>
         <td align="center"><b>'.$co_curricular['GAMES']['Enthusiasm']['g'].'</b></td>
         <td>Book Work</td><td align="center"><b>'.$co_curricular['ART & CRAFT']['Book Work']['g'].'</b></td>
         <td>Regularity & Punctuality</td><td align="center"><b>'.$co_curricular['Regularity & Punctuality']['Regularity & Punctuality']['g'].'</b></td><td>Respect for School Property</td><td align="center"><b>'.$co_curricular['Respect for school property']['Respect for school property']['g'].'</b></td></th>
       </tr>
       <th align="center">MUSIC & DANCE</th><td>Rhythm</td> <td align="center"><b>'.$co_curricular['MUSIC & DANCE']['Rhythm']['g'].'</b></td>
       <td colspan="2">Interest</td><td align="center"><b>'.$co_curricular['MUSIC & DANCE']['INTEREST']['g'].'</b></td>
       <td>Sharing & Caring</td><td align="center"><b>'.$co_curricular['Sharing & Caring']['Sharing & Caring']['g'].'</b></td>
       <td>Imaginative Skills</td><td align="center"><b>'.$co_curricular['Imaginative skills']['Imaginative skills']['g'].'</b></td></tr></tr>';
    echo'  <table width="100%" align="left" id="77"  height=90 style="border:0px"><tbody>
<td  style="float:left;border:0px" valign="bottom"><b><font size="2"><b>Parent\'s signature</b><font size="2"><b  style="margin-left:140px; width:100%;">Class Teacher\'s signature</font></b><b  style="margin-left:160px; width:100%;"><font size="2">Principal\'s signature</b></font></td>
</tbody></table>';
    
echo'</tbody></table>';
 }
//////////////////////////////////////////////////////////calsssss  5

else if(($class_id==23)||($class_id==24)||($class_id==25)||($class_id==26))
{
   ?> 
    <style>
table,th,td,tr
{
border:1px solid black;
font-size: 19px;
}
</style>

    <?php
    
  

  $jke=1;
    echo '<p style="z-index:1; position:absolute; top:5px; left:80px"><font style="font-size:38px;"><strong>VIDYA BAL BHAWAN SR. SEC. SCHOOL </strong></font></p>';
    echo'
<table  width="100%" style="border:0px">
<tbody ><tr><td>
<table  width="100%" style="border:0px">
<tbody style="border:0px">
<tr style="border:0px">
<td align="left" width="25%" style="border:0px"></td>
</tr>
<tr style="border:0px">';echo'<td align="left" style="border:0px"position:absolute; top:0px; ><img src="../images/logo/School.jpg" width="32%"  ></td>';
echo'<br><div ><td  valign="top" width="150%" style="margin-left:-2px; width:100%;border:0px">
<font style="margin-left:0px; width:100%; font-size-size:12px;"" align="center"><br>
      Kondli Gharoli Road, Mayur Vihar III, Delhi -110096 </font>
     <br>
    <b style="margin-left:-20px; width:100%;"><font style="font-size:24px;">Record of Academic Performance(2014-2015)</b>  </font> </td>
</tr>
</tbody>
</table>';




//get the name of the student on that id
$subject_id=0;
$get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB,`Blood Group`
FROM student_user
WHERE sId = ".$_GET['student_id']."";
$exe_s_name=mysql_query($get_s_name);
$fetch_s_name=mysql_fetch_array($exe_s_name);
$name_student=$fetch_s_name[0];
$blood=$fetch_s_name['Blood Group'];

// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.*
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.sId = ".$_GET['student_id']."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 $class_name=$fetch_cls_name['class'];
        $section_name=$fetch_cls_name['section'];
         
echo '
         
<div class="row-fluid">
<div class="widget  span12 clearfix">
<div class="widget-header">
<table id=8   valign="top" class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<tbody  width="100%">
<tr width="40%"  style="border:0px solid black;border-collapse:collapse;">
<td align="left" width="40%"  style="border:0px solid black;border-collapse:collapse;">Student Name: <b style="font-size:16px"> '.$name_student.'</b></font></td>
<td align="left" width="25%"  style="border:0px solid black;border-collapse:collapse;">Admission No.:<b style="font-size:16px" > '.$fetch_s_name[3].'</b></font></td>
<td align="left"  style="border:0px solid black;border-collapse:collapse;">Examination Roll.No:<b style="font-size:16px"></b></td>
</tr>
<tr width="20%"  style="border:0px solid black;border-collapse:collapse;">
<td align="left" width="40%"  style="border:0px solid black;border-collapse:collapse;">Fathers Name:<b style="font-size:16px"> '.$fetch_s_name[1].'</b></font></td>
<td align="left" width="25%"  style="border:0px solid black;border-collapse:collapse;">Class & Section:<b style="font-size:16px" > '.$class_name.'       '.$section_name.' </b></font></td> 
<td align="left"   style="border:0px solid black;border-collapse:collapse;">D.O.B.- <b style="font-size:16px">'.date("jS F Y",strtotime($fetch_s_name[4])).' <b></b></b></font></td>
</tr>
<tr  width="40%"   style="border:0px solid black;border-collapse:collapse;">
<td align="left" width="40%"  style="border:0px solid black;border-collapse:collapse;">Mothers Name: <b style="font-size:16px"> '.$fetch_s_name[2].'</b></font></td >
<td align="left" width="25%"  style="border:0px solid black;border-collapse:collapse;">Blood Group:<b style="font-size:16px"> '.$blood.'</b></td> 
    <td  align="left"   style="border:0px solid black;border-collapse:collapse;"><font style="font-size:20px">Height: <b  width:100%;" style="font-size:14px"><font style="margin-left:40px; width:100%;" style="font-size:12px"></b><font size=""  style="font-size:20px">Weight:<b></td>
</tr>
</tbody></table>  							
       </div><!-- End widget-header -->	          							
<div class="widget-content" align="center">						
<!-- Table widget -->

<table  class="table table-bordered table-striped" width="100%" valign="top" align="center" style="border:0px solid black;border-collapse:collapse;">
<thead >                                          
<tr">
   <th width="16%" style="font-size:16px" align="left">SCHOLASTIC AREA</th> 
   <th colspan="4" align="center"  style="font-size:16px;width:28%;">TERM-I</th>
   <th colspan="4" align="center"  style="font-size:16px;width:28%;">TERM-II</th>
   <th colspan="4" align="center"  style="font-size:16px;width:28%;">FINAL ASSESSMENT</th>
</tr>
<tr height="40" style="background-color:#CCCCCC;-webkit-print-color-adjust: exact;">
   <th style="font-size:20px;width:16%" >&nbsp;&nbsp;&nbsp;&nbsp;SUBJECT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
   <th align="center" style="font-size:18px;width:7%;">FA1<br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;">FA2<br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;">SA1<br>(30%)</th>
   <th align="center" style="font-size:18px;width:7%;"> Total<br>(50%)</th>
   <th align="center" style="font-size:18px;width:7%;">FA3<br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;"> FA4 <br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;">SA2<br>(30%)</th>
   <th align="center" style="font-size:18px;width:7%;"> Total<br>(50%)</th>
   <th align="center" style="font-size:18px;width:7%;">FA<br>(40%)</th>
   <th align="center" style="font-size:18px;width:7%;">SA<br>(60%)</th>
   <th align="center" style="font-size:18px;width:7%;">Overall<br>(FA+SA)<br  style="font-size:18px">100%</th>
   <th align="center" style="font-size:17px;width:7%;">Grade Point</th>
</tr> 															
  </thead> 
  
';

$total_max_sa_marks=0;
$total_max_fa_marks=0;
           $total_max_marks=0;
$marks_fa=array();
$marks_sa=array();
$sid=array();
$marks_total=0;
$get_marks=0;
$ctr=0; $sum_total_fa_get_marks=0;
    $final_grade=array(); 
    $sum_sa=0;

  $total_fas_marks=0;
     $get_subject = "
SELECT DISTINCT subject.subject, subject.subject_id, `cce_marks_scho_type_skills_table`.sts_id,cce_subject_type_skills_id_table.class_id
FROM subject
INNER JOIN `cce_subject_type_skills_id_table` 
ON subject.subject_id =`cce_subject_type_skills_id_table` .subject_id

INNER JOIN `cce_marks_scho_type_skills_table`
ON `cce_marks_scho_type_skills_table`.sts_id=cce_subject_type_skills_id_table.sts_id

INNER JOIN  cce_term_test_table
ON cce_term_test_table.id=cce_marks_scho_type_skills_table.term_test_id

 
INNER JOIN class
ON class.sId=`cce_marks_scho_type_skills_table` .student_id
WHERE `cce_marks_scho_type_skills_table` .student_id=".$_GET['student_id']."  AND cce_term_test_table.class_id=$class_id
                                                                  ";
$exe_subject = mysql_query($get_subject);
$j=0;
while($fetch_subject = mysql_fetch_array($exe_subject))
{         
 $sts_id=$fetch_subject['sts_id'];
$sub_id=$fetch_subject['subject_id'];
//$term_dd=$fetch_subject['term_id'];

 echo '
<tr>
<td style="font-size:16px"><b>'.$fetch_subject['subject'].'</td> ';
  
    $get_term_id="SELECT   term_id  FROM  cce_term_table   ";
    $exe_term=mysql_query($get_term_id);
    while($fetch_term_id=mysql_fetch_array($exe_term))
    {           $term=$fetch_term_id[0];
    
$get_marks="SELECT cce_marks_scho_type_skills_table.* ,class.classId 
          FROM  `cce_marks_scho_type_skills_table` 
          
 INNER JOIN cce_term_test_table
                  ON cce_marks_scho_type_skills_table.term_test_id=cce_term_test_table.id

 INNER JOIN cce_term_table
                  ON cce_term_table.term_id=cce_term_test_table.term_id
           INNER JOIN class 
         ON class.sId=cce_marks_scho_type_skills_table.student_id
       WHERE  cce_marks_scho_type_skills_table.sts_id=$sts_id  AND cce_marks_scho_type_skills_table.student_id= ".$_GET['student_id']." AND cce_term_table.term_id=$term AND cce_term_test_table.class_id=$class_id   AND cce_marks_scho_type_skills_table.session_id=$session_id"   ;
 $exe_marks=mysql_query($get_marks);
 while($fetch_marks=mysql_fetch_array($exe_marks))
 {
 $marks=$fetch_marks['marks'];
   $term_test_id=$fetch_marks[1];
  $cid=$fetch_marks['classId'];
   $get_test_id="SELECT cce_test_table.* FROM cce_test_table
                  INNER JOIN cce_term_test_table
                  ON cce_term_test_table.test_id=cce_test_table.test_id	
                  INNER JOIN cce_term_table
                  ON cce_term_table.term_id=cce_term_test_table.term_id
                
                  WHERE cce_term_test_table.id=$term_test_id  AND cce_term_test_table.class_id=$cid   AND cce_term_table.term_id=$term AND cce_term_test_table.class_id=$class_id";
              
 $exe_get_id=  mysql_query($get_test_id);
 while($fetch_test_id=mysql_fetch_array($exe_get_id))
 {   $test_id=$fetch_test_id['test_id'];
      $ctr++;
  $max_marks=$fetch_test_id['max_marks'];
$get_marks=$get_marks+$marks;
if(($test_id==5)||($test_id==6))
   {   
                                             
                                                            $total_max_fa_marks=$total_max_fa_marks+$max_marks;
                                                         
                                                             $marks_grade= ($marks *100)/$max_marks ;



                                  }
                                    if(($test_id == 7))
                                  {
                                   $total_max_sa_marks=$total_max_sa_marks+$max_marks;
                                       $marks_grade = $marks *100/$max_marks ;


                                  }     
 }


    //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($marks_grade>90) && ($marks_grade<=100))
                                   {
                                           $grade1 = 10;
                                           $g ='A1' ;
                                   }
                                   if(($marks_grade>80) && ($marks_grade<=90))
                                   {
                                           $grade1 = 9;
                                           $g ='A2' ;
                                   }
                                   if(($marks_grade>70) && ($marks_grade<=80))
                                   {
                                           $grade1 = 8;
                                           $g ='B1' ;
                                   }
                                   if(($marks_grade>60) && ($marks_grade<=70))
                                   {
                                           $grade1= 7;
                                           $g ='B2' ;
                                   }
                                   if(($marks_grade>50) && ($marks_grade<=60))
                                   {
                                           $grade1 = 6;
                                           $g ='C1' ;
                                   }
                                   if(($marks_grade>40) && ($marks_grade<=50))
                                   {
                                           $grade1 = 5;
                                           $g ='C2' ;
                                   }
                                   if(($marks_grade>32) && ($marks_grade<=40))
                                   {
                                          $grade1 = 4;
                                          $g ='D' ;
                                   }
                                        if(($marks_grade>20) && ($marks_grade<=32))
                                   {
                                           $grade1 = 3;
                                           $g ='E1' ;
                                   }
                                    if(($marks_grade>=0) && ($marks_grade<=32))
                                   {
                                           $grade1 = 2;
                                           $g ='E2' ;
                                   }


echo'<td align="center">'.$g.'</td>';
 $marks_total=$get_marks;
 if($jke<4)
{
 $total_max_marks=$total_max_marks+$max_marks;
}
$jke++;
$total_max_marks;
$total=($get_marks*100)/$total_max_marks;

$grade1_sum1 = 0;

                              

                                 //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($total>90) && ($total<=100))
                                   {
                                           $grade1 = 10;
                                           $gg ='A1' ;
                                   }
                                   if(($total>80) && ($total<=90))
                                   {
                                           $grade1 = 9;
                                           $gg ='A2' ;
                                   }
                                   if(($total>70) && ($total<=80))
                                   {
                                           $grade1 = 8;
                                           $gg ='B1' ;
                                   }
                                   if(($total>60) && ($total<=70))
                                   {
                                           $grade1= 7;
                                           $gg ='B2' ;
                                   }
                                   if(($total>50) && ($total<=60))
                                   {
                                           $grade1 = 6;
                                           $gg ='C1' ;
                                   }
                                   if(($total>40) && ($total<=50))
                                   {
                                           $grade1 = 5;
                                           $gg ='C2' ;
                                   }
                                   if(($total>32) && ($total<=40))
                                   {
                                          $grade1 = 4;
                                          $gg ='D' ;
                                   }
                                   if(($total>20) && ($total<=32))
                                   {
                                           $grade1 = 3;
                                           $gg ='E1' ;
                                   }
                                    if(($total>=0) && ($total<=20))
                                   {
                                           $grade1 = 2;
                                           $gg ='E2' ;
                                   }
                                   
         
                                   
 }   //echo   $final_grade=$gg;        
$sum_total_fa_get_marks=0;
$sum_total_sa_get_marks=0;
     echo'<td align="center">'.$gg.'</td>';
    }
         $get_test_id="SELECT * FROM cce_test_table ";
     $exe_test_id=mysql_query($get_test_id);
     while($fetch_test_id=mysql_fetch_array($exe_test_id))
     {  
            $fa_id=$fetch_test_id[0];
      if(($fa_id==5)||($fa_id==6)||($fa_id==8)||($fa_id==9))
   {    
                  $get_fa_marks="SELECT DISTINCT  SUM(marks) AS marks  FROM  `cce_marks_scho_type_skills_table` 
                                         
                                  INNER JOIN  `cce_term_test_table` 
                                  ON `cce_term_test_table` .id=cce_marks_scho_type_skills_table.term_test_id

                                   INNER  JOIN  `cce_test_table` 
                                   ON `cce_test_table`.test_id=cce_term_test_table.test_id 
                                   
                                  INNER JOIN cce_subject_type_skills_id_table
                                  ON cce_subject_type_skills_id_table.sts_id=cce_marks_scho_type_skills_table.sts_id
                                WHERE cce_test_table.test_id=$fa_id AND cce_marks_scho_type_skills_table.student_id=$student_id "
                         . "AND cce_subject_type_skills_id_table.subject_id=$sub_id AND cce_term_test_table.class_id=$class_id    AND cce_marks_scho_type_skills_table.session_id=$session_id";
                          $fa_total_marks=mysql_query($get_fa_marks);
                         while($fetch_fa_marks=mysql_fetch_array($fa_total_marks))
                         {
                          
                             $sum_fa=$fetch_fa_marks['marks'];
                         '<br>'. $sum_total_fa_get_marks=$sum_total_fa_get_marks+$sum_fa.'';
                         }


   
     }
   

//     
    }
    
     //$get_fa=($sum_total_fa_get_marks*100)/100;
                                 //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($sum_total_fa_get_marks>90) && ($sum_total_fa_get_marks<=100))
                                   {
                                           $grade1 = 10;
                                           $gggfa ='A1' ;
                                   }
                                   if(($sum_total_fa_get_marks>80) && ($sum_total_fa_get_marks<=90))
                                   {
                                           $grade1 = 9;
                                           $gggfa ='A2' ;
                                   }
                                   if(($sum_total_fa_get_marks>70) && ($sum_total_fa_get_marks<=80))
                                   {
                                           $grade1 = 8;
                                           $gggfa ='B1' ;
                                   }
                                   if(($sum_total_fa_get_marks>60) && ($sum_total_fa_get_marks<=70))
                                   {
                                           $grade1= 7;
                                           $gggfa ='B2' ;
                                   }
                                   if(($sum_total_fa_get_marks>50) && ($sum_total_fa_get_marks<=60))
                                   {
                                           $grade1 = 6;
                                           $gggfa ='C1' ;
                                   }
                                   if(($sum_total_fa_get_marks>40) && ($sum_total_fa_get_marks<=50))
                                   {
                                           $grade1 = 5;
                                           $gggfa ='C2' ;
                                   }
                                   if(($sum_total_fa_get_marks>32) && ($sum_total_fa_get_marks<=40))
                                   {
                                          $grade1 = 4;
                                          $gggfa ='D' ;
                                   }
                                   if(($sum_total_fa_get_marks>20) && ($sum_total_fa_get_marks<=32))
                                   {
                                           $grade1 = 3;
                                           $gggfa ='E1' ;
                                   }
                                    if(($sum_total_fa_get_marks>=0) && ($sum_total_fa_get_marks<=20))
                                   {
                                           $grade1 = 2;
                                           $gggfa ='E2' ;
                                   }
    
    
   echo'<td align="center">'.$gggfa.'</td>';     
     
          
   /////////////////////total sa//////////////////////////////////////////////////
     $get_test_sa_id="SELECT * FROM cce_test_table ";
     $exe_test_sa_id=mysql_query($get_test_sa_id);
     while($fetch_test_sa_id=mysql_fetch_array($exe_test_sa_id))
     {  
            $sa_id=$fetch_test_sa_id[0];
      if(($sa_id==7)||($sa_id==10))
       {    
                   $get_sa_marks="SELECT DISTINCT  SUM(marks) AS marks  FROM  `cce_marks_scho_type_skills_table` 
                                         
                                  INNER JOIN  `cce_term_test_table` 
                                  ON `cce_term_test_table` .id=cce_marks_scho_type_skills_table.term_test_id

                                   INNER  JOIN  `cce_test_table` 
                                   ON `cce_test_table`.test_id=cce_term_test_table.test_id 
                                   
                                  INNER JOIN cce_subject_type_skills_id_table
                                  ON cce_subject_type_skills_id_table.sts_id=cce_marks_scho_type_skills_table.sts_id
                                WHERE cce_test_table.test_id=$sa_id AND cce_marks_scho_type_skills_table.student_id=$student_id "
                         . "AND cce_subject_type_skills_id_table.subject_id=$sub_id AND cce_term_test_table.class_id=$class_id   AND cce_marks_scho_type_skills_table.session_id=$session_id";
                          $sa_total_marks=mysql_query($get_sa_marks);
                         while($fetch_sa_marks=mysql_fetch_array($sa_total_marks))
                         {
                          
                              $sum_sa=$fetch_sa_marks['marks'];
                         '<br>'. $sum_total_sa_get_marks=$sum_total_sa_get_marks+$sum_sa.'';
                         }


   
     }
   

//     
 
    }
        $get_sa=($sum_total_sa_get_marks*100)/200;
      //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($get_sa>90) && ($get_sa<=100))
                                   {
                                           $grade1 = 10;
                                           $gggsa  ='A1' ;
                                   }
                                   if(($get_sa>80) && ($get_sa<=90))
                                   {
                                           $grade1 = 9;
                                           $gggsa  ='A2' ;
                                   }
                                   if(($get_sa>70) && ($get_sa<=80))
                                   {
                                           $grade1 = 8;
                                           $gggsa  ='B1' ;
                                   }
                                   if(($get_sa>60) && ($get_sa<=70))
                                   {
                                           $grade1= 7;
                                           $gggsa  ='B2' ;
                                   }
                                   if(($get_sa>50) && ($get_sa<=60))
                                   {
                                           $grade1 = 6;
                                           $gggsa  ='C1' ;
                                   }
                                   if(($get_sa>40) && ($get_sa<=50))
                                   {
                                           $grade1 = 5;
                                           $gggsa  ='C2' ;
                                   }
                                   if(($get_sa>32) && ($get_sa<=40))
                                   {
                                          $grade1 = 4;
                                          $gggsa  ='D' ;
                                   }
                                   if(($get_sa>20) && ($get_sa<=32))
                                   {
                                           $grade1 = 3;
                                           $gggsa  ='E1' ;
                                   }
                                    if(($get_sa>=0) && ($get_sa<=20))
                                   {
                                           $grade1 = 2;
                                           $gggsa ='E2' ;
                                   }
    

    
    
   echo'<td align="center">'.$gggsa .'</td>';     
     $sum_fa_sa_total=$sum_total_sa_get_marks+$sum_total_fa_get_marks;
     
     $grand_total=($sum_fa_sa_total*100)/300;
      //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade_point = 0;
                                   if(($grand_total>90) && ($grand_total<=100))
                                   {
                                           $point = 10;
                                           $grand_total_grade ='A1' ;
                                   }
                                   if(($grand_total>80) && ($grand_total<=90))
                                   {
                                           $point = 9;
                                           $grand_total_grade ='A2' ;
                                   }
                                   if(($grand_total>70) && ($grand_total<=80))
                                   {
                                           $point = 8;
                                           $grand_total_grade ='B1' ;
                                   }
                                   if(($grand_total>60) && ($grand_total<=70))
                                   {
                                           $point= 7;
                                           $grand_total_grade ='B2' ;
                                   }
                                   if(($grand_total>50) && ($grand_total<=60))
                                   {
                                           $point = 6;
                                           $grand_total_grade ='C1' ;
                                   }
                                   if(($grand_total>40) && ($grand_total<=50))
                                   {
                                           $point = 5;
                                           $grand_total_grade ='C2' ;
                                   }
                                   if(($grand_total>32) && ($grand_total<=40))
                                   {
                                          $point = 4;
                                          $grand_total_grade ='D' ;
                                   }
                                   if(($grand_total>20) && ($grand_total<=32))
                                   {
                                           $point = 3;
                                           $grand_total_grade ='E1' ;
                                   }
                                    if(($grand_total>=0) && ($grand_total<=20))
                                   {
                                           $point = 2;
                                           $grand_total_grade ='E2' ;
                                   }
                                   
   echo'<td align="center">'.$grand_total_grade.'</td>';       
      
  $total_overall=$total_overall+$sum_fa_sa_total;
   
  $total_overall_per=($total_overall*100)/2100;
  
   $total_point=($sum_fa_sa_total*100)/300;
                                 //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade_point = 0;
                                   if(($total_point>90) && ($total_point<=100))
                                   {
                                           $grade_point = 10;
                                           $gg ='A1' ;
                                   }
                                   if(($total_point>80) && ($total_point<=90))
                                   {
                                           $grade_point = 9;
                                           $gg ='A2' ;
                                   }
                                   if(($total_point>70) && ($total_point<=80))
                                   {
                                           $grade_point = 8;
                                           $gg ='B1' ;
                                   }
                                   if(($total_point>60) && ($total_point<=70))
                                   {
                                           $grade_point= 7;
                                           $gg ='B2' ;
                                   }
                                   if(($total_point>50) && ($total_point<=60))
                                   {
                                           $grade_point = 6;
                                           $gg ='C1' ;
                                   }
                                   if(($total_point>40) && ($total_point<=50))
                                   {
                                           $grade_point = 5;
                                           $gg ='C2' ;
                                   }
                                   if(($total_point>32) && ($total_point<=40))
                                   {
                                          $grade_point = 4;
                                          $gg ='D' ;
                                   }
                                   if(($total_point>20) && ($total_point<=32))
                                   {
                                           $grade_point = 3;
                                           $gg ='E1' ;
                                   }
                                    if(($total_point>=0) && ($total_point<=20))
                                   {
                                           $grade_point = 2;
                                           $gg ='E2' ;
                                   }
                                   
   
   
   
   
        echo'<td align="center">'.$grade_point.'</td>';    
      $cgpa=$grade_point;
      $total_cgpa=$total_cgpa+$cgpa;
     $total_per=$total_cgpa/9;
     $calculata_cgpa=round($total_per,2);
     
        //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade_point = 0;
                                   if(($total_per>9) && ($total_per<=10))
                                   {
                                           $grade_point = 10;
                                           $gg_cpga ='A1' ;
                                   }
                                   if(($total_per>8) && ($total_per<=9))
                                   {
                                           $grade_point = 9;
                                           $gg_cpga ='A2' ;
                                   }
                                   if(($total_per>7) && ($total_per<=8))
                                   {
                                           $grade_point = 8;
                                           $gg_cpga ='B1' ;
                                   }
                                   if(($total_per>6) && ($total_per<=7))
                                   {
                                           $grade_point= 7;
                                           $gg_cpga ='B2' ;
                                   }
                                   if(($total_per>5) && ($total_per<=6))
                                   {
                                           $grade_point = 6;
                                           $gg_cpga ='C1' ;
                                   }
                                   if(($total_per>4) && ($total_per<=5))
                                   {
                                           $grade_point = 5;
                                           $gg_cpga ='C2' ;
                                   }
                                   if(($total_per>3) && ($total_per<=4))
                                   {
                                          $grade_point = 4;
                                          $gg_cpga ='D' ;
                                   }
                                   if(($total_per>2) && ($total_per<=3))
                                   {
                                           $grade_point = 3;
                                           $gg_cpga ='E1' ;
                                   }
                                    if(($total_per>=0) && ($total_per<=2))
                                   {
                                           $grade_point = 2;
                                           $gg_cpga ='E2' ;
                                   }
                                   
   
     
    
}

echo'   </tbody></table>';
    

echo'<table  class="table table-bordered table-striped" width="100%" valign="top" align="center" style="border:0px solid black;border-collapse:collapse;"><tbody>
 <tr align="center">
            <td align="left" style="font-size:20px" colspan="5"><b>Attendance</b> : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;90/90</td>
            <td colspan="" align="center" style="font-size:19px">&nbsp;&nbsp;&nbsp;&nbsp;210/210&nbsp;&nbsp;</td>
                    <td colspan="5" align="center" style="font-size:12px"><b>Eetremely regular student. keep it up !</b></td>
            <td style="font-size:18px;">&nbsp;&nbsp;&nbsp;&nbsp;CGPA&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="font-size:18px">&nbsp;&nbsp;&nbsp;'.$calculata_cgpa.'&nbsp;&nbsp;</td>
          
          
        </tr>
 <tr align="center" style="border:0px solid black;border-collapse:collapse;">
           
            <td colspan="11" align="left"><font style="font-size:11px;"><b>Note:A1=91-100%;A2=81-90%;B1=71-80%;B2=61-70%;C1=51-60%;C2=41-50%;D=33-40%;E1=21-32%;E2=0-20% AND BELOW</b></td>
            
           
            <td colspan="" style="font-size:14px"><b>Overall Grade</b></td>
            <td colspan="" style="font-size:20px"><b>'.$gg_cpga.'</b></td>
                 
        </tr></tbody></table>';


// to ptint the scholastic area************************************************************************
 echo'
<table  class="table table-bordered table-striped"  width="100%" style="border:0px solid black;border-collapse:collapse;" cellpadding="2">
<thead >                            
<tr><th colspan="3" align="center" width="40%" style="background-color:#CCCCCC;-webkit-print-color-adjust: exact" ><font style="font-size:16px;">Co-SCHOLASTIC AREA</font></th>
</tr>
<tr><th align="center" width=""><font style="font-size:17px;">ACTIVITY</font></th><th align="center" style="width:5%;"><font style="font-size:16px;">GRADE</th></font><th align="center" ><font style="font-size:17px;">INDICATOR NAME</font></th></tr>
  </thead>
<tbody>';
 $ctr=0;
 $coscho_area="SELECT * FROM cce_coscho_fields ";
$exe_coscho=mysql_query($coscho_area);
while($coscho_area_name=mysql_fetch_array($exe_coscho))
{   $coscho_name=$coscho_area_name[2];
       $coscho_id=$coscho_area_name[0];
       $cosho=$coscho_area_name['cosho_area_id'];
       $ctr++;
    echo'<tr><td width="28%" height="30"> <b size="4"style="font-size:16px">'.$coscho_name.'</b></font></td>';
    $get_grade="SELECT * FROM cce_coscho_indicator_marks_table WHERE student_id=$student_id  AND coscho_id=  $coscho_id  AND session_id=$session_id";
    $exe_grade= mysql_query($get_grade);
    while($fetch_grade=  mysql_fetch_array($exe_grade))
    {             $grade=$fetch_grade['grade'];
    
   $coscho_indicator_name="SELECT *
            FROM cce_coscho_indicator_table  WHERE coscho_id=$coscho_id  AND  grade='$grade '";
    $exe_indicator=mysql_query($coscho_indicator_name);
    while($fetch_indicator=  mysql_fetch_array($exe_indicator))
    {             $indicato_name=$fetch_indicator[2];

       echo'<td align="center" width="3%" ><b style="font-size:18px">'.$fetch_indicator['grade'].'</b></td><td width="70%"><b style="font-size:12px" >'.$name_student.'</b><font  style="font-size:16px">   '. $indicato_name.'</font></td></tr>';
    }
}}
echo'</tbody></table>';




// to print the bottom part signature and self awerness........................
echo'<table width="100%" height="" style="border:0px solid black;border-collapse:collapse;"><tbody>    
    <tr align="center" style="border:1px solid black; border-collapse:collapse;">
            <td colspan="13" style="background-color:#CCCCCC;-webkit-print-color-adjust: exact"><font  style="font-size:16px">
<b>SELF AWARENESS</b></font></td>
        </tr>
         <tr align="center" style="border:0px solid black;border-collapse:collapse;">
            <td colspan="2" celpadding="10%"><font size="4"style="font-size:17px" ><b>My Goals</b></td>
             <td colspan="11" ><font size="4" style="font-size:17px"><b>My Interests and Hobbies</b></td>
        </tr>
         <tr align="center" style="border:0px solid black;border-collapse:collapse;">
            <td colspan="2" align="left"><font size="3"  style="font-size:16px">I will become an electrical engineer some day.</td>
             <td colspan="11" align="left"><font size="3"  style="font-size:16px">My favourite hobby is playing snooker. I enjoy working on the computer</td>
        </tr>
        <tr align="center" style="border:0px;" style="border:0px solid black;border-collapse:collapse;">
         <td colspan="13" align="left" width="100%" ><font size="3"><b  style="font-size:18px" width="100%">CONGRATULATIONS !</b> <b  style="font-size:16px" > Your ward is promoted to </b><b  style="margin-left:20px; width:100%;"  style="font-size:20px"><font size="4">Class:6 A</font></b><b  style="margin-left:40px; width:100%;"  style="font-size:22px"><font size="4">New Session beings on .......</b></td>

            </tbody></table>';

echo'<table  width="100%"  style="border:0px;" width=""></tbody>
       </tr>
       <tr align="center" height="100px;" >
         <td  valign="bottom"  width="50%"  style="border:0px;" style="font-size:25px" align="left"><b>DATE:</b></td>
           <td  valign="bottom"  width="50%"  style="border:0px;" style="font-size:25px" ><b style="margin-left:-460px; width:100%;">Class Teacher\'s signature</b></td>
           <td valign="bottom"  width="0%"  style="border:0px;" style="font-size:25px"><b style="margin-left:-150px; width:100%;">Principal\'s signature</b></td>
       </tr>
      
         ';
    echo' </tbody></table> ';
    

}



///********************** class id = 28**************************************************
else if(($class_id==27)||($class_id==28)||($class_id==29)||($class_id==30)||($class_id==31)||($class_id==32)||($class_id==33)||($class_id==34)||($class_id==35))
{  $jke=1;


   ?> 
    <style>
table,th,td,tr
{
border:1px solid black;
font-size: 19px;
}
</style>

    <?php
    
   echo '<p style="z-index:1; position:absolute; top:5px; left:87px"><font style="font-size:38px;"><strong>VIDYA BAL BHAWAN SR. SEC. SCHOOL </strong></font></p>';
    echo'
<table  width="100%" style="border:0px">
<tbody ><tr><td>
<table  width="100%" style="border:0px">
<tbody style="border:0px">
<tr style="border:0px">
<td align="left" width="25%" style="border:0px"></td>
</tr>
<tr style="border:0px">';echo'<td align="left" style="border:0px"position:absolute; top:0px; ><img src="../images/logo/School.jpg" width="32%"  ></td>';
echo'<br><div ><td  valign="top" width="150%" style="margin-left:-2px; width:100%;border:0px">
<font style="margin-left:0px; width:100%; font-size-size:12px;"" align="center"><br>
      Kondli Gharoli Road, Mayur Vihar III, Delhi -110096 </font>
     <br>
    <b style="margin-left:-22px; width:100%;"><font style="font-size:24px;">Record of Academic Performance(2014-2015)</b>  </font> </td>
</tr>
</tbody>
</table>';




//get the name of the student on that id
$subject_id=0;
$get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB,`Blood Group`
FROM student_user
WHERE sId = ".$_GET['student_id']."";
$exe_s_name=mysql_query($get_s_name);
$fetch_s_name=mysql_fetch_array($exe_s_name);
$name_student=$fetch_s_name[0];
$blood=$fetch_s_name['Blood Group'];

// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.*
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.sId = ".$_GET['student_id']."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 $class_name=$fetch_cls_name['class'];
        $section_name=$fetch_cls_name['section'];
         
echo '
         
<div class="row-fluid">
<div class="widget  span12 clearfix">
<div class="widget-header">
<table id=8   valign="top" class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<tbody  width="100%">
<tr width="40%"  style="border:0px solid black;border-collapse:collapse;">
<td align="left" width="40%"  style="border:0px solid black;border-collapse:collapse;">Student Name: <b style="font-size:16px"> '.$name_student.'</b></font></td>
<td align="left" width="25%"  style="border:0px solid black;border-collapse:collapse;">Admission No.:<b style="font-size:16px" > '.$fetch_s_name[3].'</b></font></td>
<td align="left"  style="border:0px solid black;border-collapse:collapse;">Examination Roll.No:<b style="font-size:16px"></b></td>
</tr>
<tr width="20%"  style="border:0px solid black;border-collapse:collapse;">
<td align="left" width="40%"  style="border:0px solid black;border-collapse:collapse;">Fathers Name:<b style="font-size:16px"> '.$fetch_s_name[1].'</b></font></td>
<td align="left" width="25%"  style="border:0px solid black;border-collapse:collapse;">Class & Section:<b style="font-size:16px" > '.$class_name.'       '.$section_name.' </b></font></td> 
<td align="left"   style="border:0px solid black;border-collapse:collapse;">D.O.B.- <b style="font-size:16px">'.date("jS F Y",strtotime($fetch_s_name[4])).' <b></b></b></font></td>
</tr>
<tr  width="40%"   style="border:0px solid black;border-collapse:collapse;">
<td align="left" width="40%"  style="border:0px solid black;border-collapse:collapse;">Mothers Name: <b style="font-size:16px"> '.$fetch_s_name[2].'</b></font></td >
<td align="left" width="25%"  style="border:0px solid black;border-collapse:collapse;">Blood Group:<b style="font-size:16px"> '.$blood.'</b></td> 
    <td  align="left"   style="border:0px solid black;border-collapse:collapse;"><font style="font-size:20px">Height: <b  width:100%;" style="font-size:14px"><font style="margin-left:40px; width:100%;" style="font-size:12px"></b><font size=""  style="font-size:20px">Weight:<b></td>
</tr>
</tbody></table>  							
       </div><!-- End widget-header -->	          							
<div class="widget-content" align="center">						
<!-- Table widget -->

<table  class="table table-bordered table-striped" width="100%" valign="top" align="center" style="border:0px solid black;border-collapse:collapse;">
<thead >                                          
<tr">
   <th width="16%" style="font-size:16px" align="left">SCHOLASTIC AREA</th> 
   <th colspan="4" align="center"  style="font-size:16px;width:28%;">TERM-I</th>
   <th colspan="4" align="center"  style="font-size:16px;width:28%;">TERM-II</th>
   <th colspan="4" align="center"  style="font-size:16px;width:28%;">FINAL ASSESSMENT</th>
</tr>
<tr height="60" style="background-color:#CCCCCC;-webkit-print-color-adjust: exact;">
   <th style="font-size:20px;width:16%" >&nbsp;&nbsp;&nbsp;&nbsp;SUBJECT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
   <th align="center" style="font-size:18px;width:7%;">FA1<br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;">FA2<br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;">SA1<br>(30%)</th>
   <th align="center" style="font-size:18px;width:7%;"> Total<br>(50%)</th>
   <th align="center" style="font-size:18px;width:7%;">FA3<br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;"> FA4 <br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;">SA2<br>(30%)</th>
   <th align="center" style="font-size:18px;width:7%;"> Total<br>(50%)</th>
   <th align="center" style="font-size:18px;width:7%;">FA<br>(40%)</th>
   <th align="center" style="font-size:18px;width:7%;">SA<br>(60%)</th>
   <th align="center" style="font-size:18px;width:7%;">Overall<br>(FA+SA)<br  style="font-size:18px">100%</th>
   <th align="center" style="font-size:17px;width:7%;">Grade Point</th>
</tr> 															
  </thead> ';
  
$total_max_sa_marks=0;
$total_max_fa_marks=0;
           $total_max_marks=0;
$marks_fa=array();
$marks_sa=array();
$sid=array();
$marks_total=0;
$get_marks=0;
$ctr=0; $sum_total_fa_get_marks=0;
    $final_grade=array(); 
    $sum_sa=0;

  $total_fas_marks=0;
     $get_subject = "
SELECT DISTINCT subject.subject, subject.subject_id, `cce_marks_scho_type_skills_table`.sts_id,cce_subject_type_skills_id_table.class_id
FROM subject
INNER JOIN `cce_subject_type_skills_id_table` 
ON subject.subject_id =`cce_subject_type_skills_id_table` .subject_id

INNER JOIN `cce_marks_scho_type_skills_table`
ON `cce_marks_scho_type_skills_table`.sts_id=cce_subject_type_skills_id_table.sts_id

INNER JOIN  cce_term_test_table
ON cce_term_test_table.id=cce_marks_scho_type_skills_table.term_test_id

 
INNER JOIN class
ON class.sId=`cce_marks_scho_type_skills_table` .student_id
WHERE `cce_marks_scho_type_skills_table` .student_id=".$_GET['student_id']."  AND cce_term_test_table.class_id=$class_id
                                                                  ";
$exe_subject = mysql_query($get_subject);
$j=0;
while($fetch_subject = mysql_fetch_array($exe_subject))
{         
 $sts_id=$fetch_subject['sts_id'];
$sub_id=$fetch_subject['subject_id'];
//$term_dd=$fetch_subject['term_id'];

 echo '
<tr>
<td style="font-size:16px"><b>'.$fetch_subject['subject'].'</td> ';
  
    $get_term_id="SELECT   term_id  FROM  cce_term_table   ";
    $exe_term=mysql_query($get_term_id);
    while($fetch_term_id=mysql_fetch_array($exe_term))
    {           $term=$fetch_term_id[0];
    
$get_marks="SELECT cce_marks_scho_type_skills_table.* ,class.classId 
          FROM  `cce_marks_scho_type_skills_table` 
          
 INNER JOIN cce_term_test_table
                  ON cce_marks_scho_type_skills_table.term_test_id=cce_term_test_table.id

 INNER JOIN cce_term_table
                  ON cce_term_table.term_id=cce_term_test_table.term_id
           INNER JOIN class 
         ON class.sId=cce_marks_scho_type_skills_table.student_id
       WHERE  cce_marks_scho_type_skills_table.sts_id=$sts_id  AND cce_marks_scho_type_skills_table.student_id= ".$_GET['student_id']." AND cce_term_table.term_id=$term  AND cce_term_test_table.class_id=$class_id   AND cce_marks_scho_type_skills_table.session_id=$session_id"   ;
 $exe_marks=mysql_query($get_marks);
 while($fetch_marks=mysql_fetch_array($exe_marks))
 {
 $marks=$fetch_marks['marks'];
   $term_test_id=$fetch_marks[1];
  $cid=$fetch_marks['classId'];
   $get_test_id="SELECT cce_test_table.* FROM cce_test_table
                  INNER JOIN cce_term_test_table
                  ON cce_term_test_table.test_id=cce_test_table.test_id	
                  INNER JOIN cce_term_table
                  ON cce_term_table.term_id=cce_term_test_table.term_id
                
                  WHERE cce_term_test_table.id=$term_test_id  AND cce_term_test_table.class_id=$cid   AND cce_term_table.term_id=$term AND cce_term_test_table.class_id=$class_id";
              
 $exe_get_id=  mysql_query($get_test_id);
 while($fetch_test_id=mysql_fetch_array($exe_get_id))
 {   $test_id=$fetch_test_id['test_id'];
      $ctr++;
  $max_marks=$fetch_test_id['max_marks'];
$get_marks=$get_marks+$marks;
if(($test_id==5)||($test_id==6))
   {   
                                             
                                                            $total_max_fa_marks=$total_max_fa_marks+$max_marks;
                                                         
                                                             $marks_grade= ($marks *100)/$max_marks ;



                                  }
                                    if(($test_id == 7))
                                  {
                                   $total_max_sa_marks=$total_max_sa_marks+$max_marks;
                                       $marks_grade = $marks *100/$max_marks ;


                                  }     
 }


    //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($marks_grade>90) && ($marks_grade<=100))
                                   {
                                           $grade1 = 10;
                                           $g ='A1' ;
                                   }
                                   if(($marks_grade>80) && ($marks_grade<=90))
                                   {
                                           $grade1 = 9;
                                           $g ='A2' ;
                                   }
                                   if(($marks_grade>70) && ($marks_grade<=80))
                                   {
                                           $grade1 = 8;
                                           $g ='B1' ;
                                   }
                                   if(($marks_grade>60) && ($marks_grade<=70))
                                   {
                                           $grade1= 7;
                                           $g ='B2' ;
                                   }
                                   if(($marks_grade>50) && ($marks_grade<=60))
                                   {
                                           $grade1 = 6;
                                           $g ='C1' ;
                                   }
                                   if(($marks_grade>40) && ($marks_grade<=50))
                                   {
                                           $grade1 = 5;
                                           $g ='C2' ;
                                   }
                                   if(($marks_grade>32) && ($marks_grade<=40))
                                   {
                                          $grade1 = 4;
                                          $g ='D' ;
                                   }
                                        if(($marks_grade>20) && ($marks_grade<=32))
                                   {
                                           $grade1 = 3;
                                           $g ='E1' ;
                                   }
                                    if(($marks_grade>=0) && ($marks_grade<=32))
                                   {
                                           $grade1 = 2;
                                           $g ='E2' ;
                                   }


echo'<td align="center">'.$g.'</td>';
 $marks_total=$get_marks;
 if($jke<4)
{
 $total_max_marks=$total_max_marks+$max_marks;
}
$jke++;
$total_max_marks;
$total=($get_marks*100)/$total_max_marks;

$grade1_sum1 = 0;

                              

                                 //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($total>90) && ($total<=100))
                                   {
                                           $grade1 = 10;
                                           $gg ='A1' ;
                                   }
                                   if(($total>80) && ($total<=90))
                                   {
                                           $grade1 = 9;
                                           $gg ='A2' ;
                                   }
                                   if(($total>70) && ($total<=80))
                                   {
                                           $grade1 = 8;
                                           $gg ='B1' ;
                                   }
                                   if(($total>60) && ($total<=70))
                                   {
                                           $grade1= 7;
                                           $gg ='B2' ;
                                   }
                                   if(($total>50) && ($total<=60))
                                   {
                                           $grade1 = 6;
                                           $gg ='C1' ;
                                   }
                                   if(($total>40) && ($total<=50))
                                   {
                                           $grade1 = 5;
                                           $gg ='C2' ;
                                   }
                                   if(($total>32) && ($total<=40))
                                   {
                                          $grade1 = 4;
                                          $gg ='D' ;
                                   }
                                   if(($total>20) && ($total<=32))
                                   {
                                           $grade1 = 3;
                                           $gg ='E1' ;
                                   }
                                    if(($total>=0) && ($total<=20))
                                   {
                                           $grade1 = 2;
                                           $gg ='E2' ;
                                   }
                                   
         
                                   
 }   //echo   $final_grade=$gg;        
$sum_total_fa_get_marks=0;
$sum_total_sa_get_marks=0;
     echo'<td align="center">'.$gg.'</td>';
    }
         $get_test_id="SELECT * FROM cce_test_table ";
     $exe_test_id=mysql_query($get_test_id);
     while($fetch_test_id=mysql_fetch_array($exe_test_id))
     {  
            $fa_id=$fetch_test_id[0];
      if(($fa_id==5)||($fa_id==6)||($fa_id==8)||($fa_id==9))
   {    
                  $get_fa_marks="SELECT DISTINCT  SUM(marks) AS marks  FROM  `cce_marks_scho_type_skills_table` 
                                         
                                  INNER JOIN  `cce_term_test_table` 
                                  ON `cce_term_test_table` .id=cce_marks_scho_type_skills_table.term_test_id

                                   INNER  JOIN  `cce_test_table` 
                                   ON `cce_test_table`.test_id=cce_term_test_table.test_id 
                                   
                                  INNER JOIN cce_subject_type_skills_id_table
                                  ON cce_subject_type_skills_id_table.sts_id=cce_marks_scho_type_skills_table.sts_id
                                WHERE cce_test_table.test_id=$fa_id AND cce_marks_scho_type_skills_table.student_id=$student_id "
                         . "AND cce_subject_type_skills_id_table.subject_id=$sub_id AND cce_term_test_table.class_id=$class_id   AND cce_marks_scho_type_skills_table.session_id=$session_id";
                          $fa_total_marks=mysql_query($get_fa_marks);
                         while($fetch_fa_marks=mysql_fetch_array($fa_total_marks))
                         {
                          
                             $sum_fa=$fetch_fa_marks['marks'];
                         '<br>'. $sum_total_fa_get_marks=$sum_total_fa_get_marks+$sum_fa.'';
                         }


   
     }
   

//     
    }
    
     //$get_fa=($sum_total_fa_get_marks*100)/100;
                                 //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($sum_total_fa_get_marks>90) && ($sum_total_fa_get_marks<=100))
                                   {
                                           $grade1 = 10;
                                           $gggfa ='A1' ;
                                   }
                                   if(($sum_total_fa_get_marks>80) && ($sum_total_fa_get_marks<=90))
                                   {
                                           $grade1 = 9;
                                           $gggfa ='A2' ;
                                   }
                                   if(($sum_total_fa_get_marks>70) && ($sum_total_fa_get_marks<=80))
                                   {
                                           $grade1 = 8;
                                           $gggfa ='B1' ;
                                   }
                                   if(($sum_total_fa_get_marks>60) && ($sum_total_fa_get_marks<=70))
                                   {
                                           $grade1= 7;
                                           $gggfa ='B2' ;
                                   }
                                   if(($sum_total_fa_get_marks>50) && ($sum_total_fa_get_marks<=60))
                                   {
                                           $grade1 = 6;
                                           $gggfa ='C1' ;
                                   }
                                   if(($sum_total_fa_get_marks>40) && ($sum_total_fa_get_marks<=50))
                                   {
                                           $grade1 = 5;
                                           $gggfa ='C2' ;
                                   }
                                   if(($sum_total_fa_get_marks>32) && ($sum_total_fa_get_marks<=40))
                                   {
                                          $grade1 = 4;
                                          $gggfa ='D' ;
                                   }
                                   if(($sum_total_fa_get_marks>20) && ($sum_total_fa_get_marks<=32))
                                   {
                                           $grade1 = 3;
                                           $gggfa ='E1' ;
                                   }
                                    if(($sum_total_fa_get_marks>=0) && ($sum_total_fa_get_marks<=20))
                                   {
                                           $grade1 = 2;
                                           $gggfa ='E2' ;
                                   }
    
    
   echo'<td align="center">'.$gggfa.'</td>';     
     
          
   /////////////////////total sa//////////////////////////////////////////////////
     $get_test_sa_id="SELECT * FROM cce_test_table ";
     $exe_test_sa_id=mysql_query($get_test_sa_id);
     while($fetch_test_sa_id=mysql_fetch_array($exe_test_sa_id))
     {  
            $sa_id=$fetch_test_sa_id[0];
      if(($sa_id==7)||($sa_id==10))
       {    
                   $get_sa_marks="SELECT DISTINCT  SUM(marks) AS marks  FROM  `cce_marks_scho_type_skills_table` 
                                         
                                  INNER JOIN  `cce_term_test_table` 
                                  ON `cce_term_test_table` .id=cce_marks_scho_type_skills_table.term_test_id

                                   INNER  JOIN  `cce_test_table` 
                                   ON `cce_test_table`.test_id=cce_term_test_table.test_id 
                                   
                                  INNER JOIN cce_subject_type_skills_id_table
                                  ON cce_subject_type_skills_id_table.sts_id=cce_marks_scho_type_skills_table.sts_id
                                WHERE cce_test_table.test_id=$sa_id AND cce_marks_scho_type_skills_table.student_id=$student_id "
                         . "AND cce_subject_type_skills_id_table.subject_id=$sub_id AND cce_term_test_table.class_id=$class_id  AND cce_marks_scho_type_skills_table.session_id=$session_id";
                          $sa_total_marks=mysql_query($get_sa_marks);
                         while($fetch_sa_marks=mysql_fetch_array($sa_total_marks))
                         {
                          
                              $sum_sa=$fetch_sa_marks['marks'];
                         '<br>'. $sum_total_sa_get_marks=$sum_total_sa_get_marks+$sum_sa.'';
                         }


   
     }
   

//     
 
    }
        $get_sa=($sum_total_sa_get_marks*100)/200;
      //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($get_sa>90) && ($get_sa<=100))
                                   {
                                           $grade1 = 10;
                                           $gggsa  ='A1' ;
                                   }
                                   if(($get_sa>80) && ($get_sa<=90))
                                   {
                                           $grade1 = 9;
                                           $gggsa  ='A2' ;
                                   }
                                   if(($get_sa>70) && ($get_sa<=80))
                                   {
                                           $grade1 = 8;
                                           $gggsa  ='B1' ;
                                   }
                                   if(($get_sa>60) && ($get_sa<=70))
                                   {
                                           $grade1= 7;
                                           $gggsa  ='B2' ;
                                   }
                                   if(($get_sa>50) && ($get_sa<=60))
                                   {
                                           $grade1 = 6;
                                           $gggsa  ='C1' ;
                                   }
                                   if(($get_sa>40) && ($get_sa<=50))
                                   {
                                           $grade1 = 5;
                                           $gggsa  ='C2' ;
                                   }
                                   if(($get_sa>32) && ($get_sa<=40))
                                   {
                                          $grade1 = 4;
                                          $gggsa  ='D' ;
                                   }
                                   if(($get_sa>20) && ($get_sa<=32))
                                   {
                                           $grade1 = 3;
                                           $gggsa  ='E1' ;
                                   }
                                    if(($get_sa>=0) && ($get_sa<=20))
                                   {
                                           $grade1 = 2;
                                           $gggsa ='E2' ;
                                   }
    

    
    
   echo'<td align="center">'.$gggsa .'</td>';     
     $sum_fa_sa_total=$sum_total_sa_get_marks+$sum_total_fa_get_marks;
     
     $grand_total=($sum_fa_sa_total*100)/300;
      //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade_point = 0;
                                   if(($grand_total>90) && ($grand_total<=100))
                                   {
                                           $point = 10;
                                           $grand_total_grade ='A1' ;
                                   }
                                   if(($grand_total>80) && ($grand_total<=90))
                                   {
                                           $point = 9;
                                           $grand_total_grade ='A2' ;
                                   }
                                   if(($grand_total>70) && ($grand_total<=80))
                                   {
                                           $point = 8;
                                           $grand_total_grade ='B1' ;
                                   }
                                   if(($grand_total>60) && ($grand_total<=70))
                                   {
                                           $point= 7;
                                           $grand_total_grade ='B2' ;
                                   }
                                   if(($grand_total>50) && ($grand_total<=60))
                                   {
                                           $point = 6;
                                           $grand_total_grade ='C1' ;
                                   }
                                   if(($grand_total>40) && ($grand_total<=50))
                                   {
                                           $point = 5;
                                           $grand_total_grade ='C2' ;
                                   }
                                   if(($grand_total>32) && ($grand_total<=40))
                                   {
                                          $point = 4;
                                          $grand_total_grade ='D' ;
                                   }
                                   if(($grand_total>20) && ($grand_total<=32))
                                   {
                                           $point = 3;
                                           $grand_total_grade ='E1' ;
                                   }
                                    if(($grand_total>=0) && ($grand_total<=20))
                                   {
                                           $point = 2;
                                           $grand_total_grade ='E2' ;
                                   }
                                   
   echo'<td align="center">'.$grand_total_grade.'</td>';       
      
  $total_overall=$total_overall+$sum_fa_sa_total;
   
  $total_overall_per=($total_overall*100)/2100;
  
   $total_point=($sum_fa_sa_total*100)/300;
                                 //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade_point = 0;
                                   if(($total_point>90) && ($total_point<=100))
                                   {
                                           $grade_point = 10;
                                           $gg ='A1' ;
                                   }
                                   if(($total_point>80) && ($total_point<=90))
                                   {
                                           $grade_point = 9;
                                           $gg ='A2' ;
                                   }
                                   if(($total_point>70) && ($total_point<=80))
                                   {
                                           $grade_point = 8;
                                           $gg ='B1' ;
                                   }
                                   if(($total_point>60) && ($total_point<=70))
                                   {
                                           $grade_point= 7;
                                           $gg ='B2' ;
                                   }
                                   if(($total_point>50) && ($total_point<=60))
                                   {
                                           $grade_point = 6;
                                           $gg ='C1' ;
                                   }
                                   if(($total_point>40) && ($total_point<=50))
                                   {
                                           $grade_point = 5;
                                           $gg ='C2' ;
                                   }
                                   if(($total_point>32) && ($total_point<=40))
                                   {
                                          $grade_point = 4;
                                          $gg ='D' ;
                                   }
                                   if(($total_point>20) && ($total_point<=32))
                                   {
                                           $grade_point = 3;
                                           $gg ='E1' ;
                                   }
                                    if(($total_point>=0) && ($total_point<=20))
                                   {
                                           $grade_point = 2;
                                           $gg ='E2' ;
                                   }
                                   
   
   
   
   
        echo'<td align="center">'.$grade_point.'</td>';    
      $cgpa=$grade_point;
      $total_cgpa=$total_cgpa+$cgpa;
     $total_per=$total_cgpa/9;
     $calculata_cgpa=round($total_per,2);
     
        //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade_point = 0;
                                   if(($total_per>9) && ($total_per<=10))
                                   {
                                           $grade_point = 10;
                                           $gg_cpga ='A1' ;
                                   }
                                   if(($total_per>8) && ($total_per<=9))
                                   {
                                           $grade_point = 9;
                                           $gg_cpga ='A2' ;
                                   }
                                   if(($total_per>7) && ($total_per<=8))
                                   {
                                           $grade_point = 8;
                                           $gg_cpga ='B1' ;
                                   }
                                   if(($total_per>6) && ($total_per<=7))
                                   {
                                           $grade_point= 7;
                                           $gg_cpga ='B2' ;
                                   }
                                   if(($total_per>5) && ($total_per<=6))
                                   {
                                           $grade_point = 6;
                                           $gg_cpga ='C1' ;
                                   }
                                   if(($total_per>4) && ($total_per<=5))
                                   {
                                           $grade_point = 5;
                                           $gg_cpga ='C2' ;
                                   }
                                   if(($total_per>3) && ($total_per<=4))
                                   {
                                          $grade_point = 4;
                                          $gg_cpga ='D' ;
                                   }
                                   if(($total_per>2) && ($total_per<=3))
                                   {
                                           $grade_point = 3;
                                           $gg_cpga ='E1' ;
                                   }
                                    if(($total_per>=0) && ($total_per<=2))
                                   {
                                           $grade_point = 2;
                                           $gg_cpga ='E2' ;
                                   }
    
     
     
}

echo'   </tbody></table>';
    

echo'<table  class="table table-bordered table-striped" width="100%" valign="top" align="center" style="border:0px solid black;border-collapse:collapse;"><tbody>
 <tr align="center">
            <td align="left" style="font-size:20px" colspan="5"><b>Attendance</b> : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;90/90</td>
            <td colspan="" align="center" style="font-size:19px">&nbsp;&nbsp;&nbsp;&nbsp;210/210&nbsp;&nbsp;</td>
                    <td colspan="5" align="center" style="font-size:12px"><b>Eetremely regular student. keep it up !</b></td>
            <td style="font-size:18px;">&nbsp;&nbsp;&nbsp;&nbsp;CGPA&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="font-size:18px">&nbsp;&nbsp;&nbsp;'.$calculata_cgpa.'&nbsp;&nbsp;</td>
          
          
        </tr>
 <tr align="center" style="border:0px solid black;border-collapse:collapse;">
           
            <td colspan="11" align="left"><font style="font-size:11px;"><b>Note:A1=91-100%;A2=81-90%;B1=71-80%;B2=61-70%;C1=51-60%;C2=41-50%;D=33-40%;E1=21-32%;E2=0-20% AND BELOW</b></td>
            
           
            <td colspan="" style="font-size:14px"><b>Overall Grade</b></td>
            <td colspan="" style="font-size:20px"><b>'.$gg_cpga.'</b></td>
                 
        </tr></tbody></table>';


// to ptint the scholastic area************************************************************************
 echo'
<table  class="table table-bordered table-striped"  width="100%" style="border:0px solid black;border-collapse:collapse;" cellpadding="2">
<thead >                            
<tr><th colspan="3" align="center" width="40%" style="background-color:#CCCCCC;-webkit-print-color-adjust: exact" ><font style="font-size:16px;">Co-SCHOLASTIC AREA</font></th>
</tr>
<tr><th align="center" width=""><font style="font-size:17px;">ACTIVITY</font></th><th align="center" style="width:5%;"><font style="font-size:16px;">GRADE</th></font><th align="center" ><font style="font-size:17px;">INDICATOR NAME</font></th></tr>
  </thead>
<tbody>';
 $ctr=0;
 $coscho_area="SELECT * FROM cce_coscho_fields ";
$exe_coscho=mysql_query($coscho_area);
while($coscho_area_name=mysql_fetch_array($exe_coscho))
{   $coscho_name=$coscho_area_name[2];
       $coscho_id=$coscho_area_name[0];
       $cosho=$coscho_area_name['cosho_area_id'];
       $ctr++;
    echo'<tr><td width="28%" height="30"> <b size="4"style="font-size:16px">'.$coscho_name.'</b></font></td>';
    $get_grade="SELECT * FROM cce_coscho_indicator_marks_table WHERE student_id=$student_id  AND coscho_id=  $coscho_id  AND session_id=$session_id";
    $exe_grade= mysql_query($get_grade);
    while($fetch_grade=  mysql_fetch_array($exe_grade))
    {             $grade=$fetch_grade['grade'];
    
   $coscho_indicator_name="SELECT *
            FROM cce_coscho_indicator_table  WHERE coscho_id=$coscho_id  AND  grade='$grade '";
    $exe_indicator=mysql_query($coscho_indicator_name);
    while($fetch_indicator=  mysql_fetch_array($exe_indicator))
    {             $indicato_name=$fetch_indicator[2];

       echo'<td align="center" width="3%" ><b style="font-size:18px">'.$fetch_indicator['grade'].'</b></td><td width="70%"><b style="font-size:12px" >'.$name_student.'</b><font  style="font-size:16px">   '. $indicato_name.'</font></td></tr>';
    }
}}
echo'</tbody></table>';




// to print the bottom part signature and self awerness........................
echo'<table width="100%" height="" style="border:0px solid black;border-collapse:collapse;"><tbody>    
    <tr align="center" style="border:1px solid black; border-collapse:collapse;">
            <td colspan="13" style="background-color:#CCCCCC;-webkit-print-color-adjust: exact"><font  style="font-size:16px">
<b>SELF AWARENESS</b></font></td>
        </tr>
         <tr align="center" style="border:0px solid black;border-collapse:collapse;">
            <td colspan="2" celpadding="10%"><font size="4"style="font-size:17px" ><b>My Goals</b></td>
             <td colspan="11" ><font size="4" style="font-size:17px"><b>My Interests and Hobbies</b></td>
        </tr>
         <tr align="center" style="border:0px solid black;border-collapse:collapse;">
            <td colspan="2" align="left"><font size="3"  style="font-size:16px">I will become an electrical engineer some day.</td>
             <td colspan="11" align="left"><font size="3"  style="font-size:16px">My favourite hobby is playing snooker. I enjoy working on the computer</td>
        </tr>
        <tr align="center" style="border:0px;" style="border:0px solid black;border-collapse:collapse;">
         <td colspan="13" align="left" width="100%" ><font size="3"><b  style="font-size:18px" width="100%">CONGRATULATIONS !</b> <b  style="font-size:16px" > Your ward is promoted to </b><b  style="margin-left:20px; width:100%;"  style="font-size:20px"><font size="4">Class:</font></b><b  style="margin-left:40px; width:100%;"  style="font-size:22px"><font size="4">New Session beings on .......</b></td>

            </tbody></table>';

echo'<table  width="100%"  style="border:0px;" width=""></tbody>
       </tr>
       <tr align="center" height="100px;" >
         <td  valign="bottom"  width="50%"  style="border:0px;" style="font-size:25px" align="left"><b>DATE:</b></td>
           <td  valign="bottom"  width="50%"  style="border:0px;" style="font-size:25px" ><b style="margin-left:-460px; width:100%;">Class Teacher\'s signature</b></td>
           <td valign="bottom"  width="0%"  style="border:0px;" style="font-size:25px"><b style="margin-left:-150px; width:100%;">Principal\'s signature</b></td>
       </tr>
      
         ';
    echo' </tbody></table> ';
 
}

//****************************************** when class id =41 ************************************************



else if(($class_id==36)||($class_id==37)||($class_id==38)||($class_id==39)||($class_id==40)||($class_id==41)||($class_id==42)||($class_id==43)||($class_id==44)||($class_id==45))
{      ?> 
    <style>
table,th,td,tr
{
border:1px solid black;
font-size: 19px;
}
</style>

    <?php
    
  echo '<p style="z-index:1; position:absolute; top:5px; left:87px"><font style="font-size:38px;"><strong>VIDYA BAL BHAWAN SR. SEC. SCHOOL </strong></font></p>';
    echo'
<table  width="100%" style="border:0px">
<tbody ><tr><td>
<table  width="100%" style="border:0px">
<tbody style="border:0px">
<tr style="border:0px">
<td align="left" width="25%" style="border:0px"></td>
</tr>
<tr style="border:0px">';echo'<td align="left" style="border:0px"position:absolute; top:0px; ><img src="../images/logo/School.jpg" width="32%"  ></td>';
echo'<br><div ><td  valign="top" width="150%" style="margin-left:-2px; width:100%;border:0px">
<font style="margin-left:0px; width:100%; font-size-size:12px;"" align="center"><br>
      Kondli Gharoli Road, Mayur Vihar III, Delhi -110096 </font>
     <br>
    <b style="margin-left:-22px; width:100%;"><font style="font-size:24px;">Record of Academic Performance(2014-2015)</b>  </font> </td>
</tr>
</tbody>
</table>';




//get the name of the student on that id
$subject_id=0;
$get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB,`Blood Group`
FROM student_user
WHERE sId = ".$_GET['student_id']."";
$exe_s_name=mysql_query($get_s_name);
$fetch_s_name=mysql_fetch_array($exe_s_name);
$name_student=$fetch_s_name[0];
$blood=$fetch_s_name['Blood Group'];

// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.*
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.sId = ".$_GET['student_id']."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 $class_name=$fetch_cls_name['class'];
        $section_name=$fetch_cls_name['section'];
         
echo '
         
<div class="row-fluid">
<div class="widget  span12 clearfix">
<div class="widget-header">
<table id=8   valign="top" class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
<tbody  width="100%">
<tr width="40%"  style="border:0px solid black;border-collapse:collapse;">
<td align="left" width="40%"  style="border:0px solid black;border-collapse:collapse;">Student Name: <b style="font-size:16px"> '.$name_student.'</b></font></td>
<td align="left" width="25%"  style="border:0px solid black;border-collapse:collapse;">Admission No.:<b style="font-size:16px" > '.$fetch_s_name[3].'</b></font></td>
<td align="left"  style="border:0px solid black;border-collapse:collapse;">Examination Roll.No:<b style="font-size:16px"></b></td>
</tr>
<tr width="20%"  style="border:0px solid black;border-collapse:collapse;">
<td align="left" width="40%"  style="border:0px solid black;border-collapse:collapse;">Fathers Name:<b style="font-size:16px"> '.$fetch_s_name[1].'</b></font></td>
<td align="left" width="25%"  style="border:0px solid black;border-collapse:collapse;">Class & Section:<b style="font-size:16px" > '.$class_name.'       '.$section_name.' </b></font></td> 
<td align="left"   style="border:0px solid black;border-collapse:collapse;">D.O.B.- <b style="font-size:16px">'.date("jS F Y",strtotime($fetch_s_name[4])).' <b></b></b></font></td>
</tr>
<tr  width="40%"   style="border:0px solid black;border-collapse:collapse;">
<td align="left" width="40%"  style="border:0px solid black;border-collapse:collapse;">Mothers Name: <b style="font-size:16px"> '.$fetch_s_name[2].'</b></font></td >
<td align="left" width="25%"  style="border:0px solid black;border-collapse:collapse;">Blood Group:<b style="font-size:16px"> '.$blood.'</b></td> 
    <td  align="left"   style="border:0px solid black;border-collapse:collapse;"><font style="font-size:20px">Height: <b  width:100%;" style="font-size:14px"><font style="margin-left:40px; width:100%;" style="font-size:12px"></b><font size=""  style="font-size:20px">Weight:<b></td>
</tr>
</tbody></table>  							
      
<table  class="table table-bordered table-striped" width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;" id=9>
<thead >                                          
<tr>
   <th width="16%" style="font-size:16px" align="left">SCHOLASTIC AREA</th> 
   <th colspan="4" align="center"  style="font-size:16px;width:28%;">TERM-I</th>
   <th colspan="4" align="center"  style="font-size:16px;width:28%;">TERM-II</th>
   <th colspan="4" align="center"  style="font-size:16px;width:28%;">FINAL ASSESSMENT</th>
</tr>
<tr height="60" style="background-color:#CCCCCC;-webkit-print-color-adjust: exact;">
   <th style="font-size:20px;width:16%" >&nbsp;&nbsp;&nbsp;&nbsp;SUBJECT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
   <th align="center" style="font-size:18px;width:7%;">FA1<br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;">FA2<br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;">SA1<br>(30%)</th>
   <th align="center" style="font-size:18px;width:7%;"> Total<br>(50%)</th>
   <th align="center" style="font-size:18px;width:7%;">FA3<br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;"> FA4 <br>(10%)</th>
   <th align="center" style="font-size:18px;width:7%;">SA2<br>(60%)</th>
   <th align="center" style="font-size:18px;width:7%;"> Total<br>(50%)</th>
   <th align="center" style="font-size:18px;width:7%;">FA<br>(40%)</th>
   <th align="center" style="font-size:18px;width:7%;">SA<br>(60%)</th>
   <th align="center" style="font-size:18px;width:7%;">Overall<br>(FA+SA)<br  style="font-size:18px">100%</th>
   <th align="center" style="font-size:17px;width:7%;">Grade Point</th>
</tr> 															
  </thead> ';
  
$total_max_sa_marks=0;
$total_max_fa_marks=0;
           $total_max_marks=0;
$marks_fa=array();
$marks_sa=array();
$sid=array();
$marks_total=0;
$get_marks=0;
$ctr=0; $sum_total_fa_get_marks=0;
    $final_grade=array(); 
    $sum_sa=0;

  $total_fas_marks=0;
     $get_subject = "
SELECT DISTINCT subject.subject, subject.subject_id, `cce_marks_scho_type_skills_table`.sts_id,cce_subject_type_skills_id_table.class_id
FROM subject
INNER JOIN `cce_subject_type_skills_id_table` 
ON subject.subject_id =`cce_subject_type_skills_id_table` .subject_id

INNER JOIN `cce_marks_scho_type_skills_table`
ON `cce_marks_scho_type_skills_table`.sts_id=cce_subject_type_skills_id_table.sts_id

INNER JOIN  cce_term_test_table
ON cce_term_test_table.id=cce_marks_scho_type_skills_table.term_test_id

 
INNER JOIN class
ON class.sId=`cce_marks_scho_type_skills_table` .student_id
WHERE `cce_marks_scho_type_skills_table` .student_id=".$_GET['student_id']."  AND  cce_term_test_table.class_id=$class_id
                                                                  ";
$exe_subject = mysql_query($get_subject);
$j=0;
while($fetch_subject = mysql_fetch_array($exe_subject))
{         
 $sts_id=$fetch_subject['sts_id'];
$sub_id=$fetch_subject['subject_id'];
//$term_dd=$fetch_subject['term_id'];

 echo '
<tr>
<td style="font-size:16px"><b>'.$fetch_subject['subject'].'</td> ';
  
    $get_term_id="SELECT   term_id  FROM  cce_term_table   ";
    $exe_term=mysql_query($get_term_id);
    while($fetch_term_id=mysql_fetch_array($exe_term))
    {           $term=$fetch_term_id[0];
    
$get_marks="SELECT cce_marks_scho_type_skills_table.* ,class.classId 
          FROM  `cce_marks_scho_type_skills_table` 
          
 INNER JOIN cce_term_test_table
                  ON cce_marks_scho_type_skills_table.term_test_id=cce_term_test_table.id

 INNER JOIN cce_term_table
                  ON cce_term_table.term_id=cce_term_test_table.term_id
           INNER JOIN class 
         ON class.sId=cce_marks_scho_type_skills_table.student_id
       WHERE  cce_marks_scho_type_skills_table.sts_id=$sts_id  AND cce_marks_scho_type_skills_table.student_id= ".$_GET['student_id']." AND cce_term_table.term_id=$term   AND cce_term_test_table.class_id=$class_id  AND cce_marks_scho_type_skills_table.session_id=$session_id"   ;
 $exe_marks=mysql_query($get_marks);
 while($fetch_marks=mysql_fetch_array($exe_marks))
 {
 $marks=$fetch_marks['marks'];
   $term_test_id=$fetch_marks[1];
  $cid=$fetch_marks['classId'];
   $get_test_id="SELECT cce_test_table.* FROM cce_test_table
                  INNER JOIN cce_term_test_table
                  ON cce_term_test_table.test_id=cce_test_table.test_id	
                  INNER JOIN cce_term_table
                  ON cce_term_table.term_id=cce_term_test_table.term_id
                
                  WHERE cce_term_test_table.id=$term_test_id  AND cce_term_test_table.class_id=$cid   AND cce_term_table.term_id=$term ";
              
 $exe_get_id=  mysql_query($get_test_id);
 while($fetch_test_id=mysql_fetch_array($exe_get_id))
 {   $test_id=$fetch_test_id['test_id'];
      $ctr++;
  $max_marks=$fetch_test_id['max_marks'];
$get_marks=$get_marks+$marks;
if(($test_id==5)||($test_id==6))
   {   
                                             
                                                            $total_max_fa_marks=$total_max_fa_marks+$max_marks;
                                                         
                                                             $marks_grade= ($marks *100)/$max_marks ;



                                  }
                                    if(($test_id == 7))
                                  {
                                   $total_max_sa_marks=$total_max_sa_marks+$max_marks;
                                       $marks_grade = $marks *100/$max_marks ;


                                  }     
 }


    //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($marks_grade>90) && ($marks_grade<=100))
                                   {
                                           $grade1 = 10;
                                           $g ='A1' ;
                                   }
                                   if(($marks_grade>80) && ($marks_grade<=90))
                                   {
                                           $grade1 = 9;
                                           $g ='A2' ;
                                   }
                                   if(($marks_grade>70) && ($marks_grade<=80))
                                   {
                                           $grade1 = 8;
                                           $g ='B1' ;
                                   }
                                   if(($marks_grade>60) && ($marks_grade<=70))
                                   {
                                           $grade1= 7;
                                           $g ='B2' ;
                                   }
                                   if(($marks_grade>50) && ($marks_grade<=60))
                                   {
                                           $grade1 = 6;
                                           $g ='C1' ;
                                   }
                                   if(($marks_grade>40) && ($marks_grade<=50))
                                   {
                                           $grade1 = 5;
                                           $g ='C2' ;
                                   }
                                   if(($marks_grade>32) && ($marks_grade<=40))
                                   {
                                          $grade1 = 4;
                                          $g ='D' ;
                                   }
                                        if(($marks_grade>20) && ($marks_grade<=32))
                                   {
                                           $grade1 = 3;
                                           $g ='E1' ;
                                   }
                                    if(($marks_grade>=0) && ($marks_grade<=32))
                                   {
                                           $grade1 = 2;
                                           $g ='E2' ;
                                   }


echo'<td align="center">'.$g.'</td>';
 $marks_total=$get_marks;
 if($jke<4)
{
 $total_max_marks=$total_max_marks+$max_marks;
}
$jke++;
$total_max_marks;
$total=($get_marks*100)/$total_max_marks;

$grade1_sum1 = 0;

                              

                                 //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($total>90) && ($total<=100))
                                   {
                                           $grade1 = 10;
                                           $gg ='A1' ;
                                   }
                                   if(($total>80) && ($total<=90))
                                   {
                                           $grade1 = 9;
                                           $gg ='A2' ;
                                   }
                                   if(($total>70) && ($total<=80))
                                   {
                                           $grade1 = 8;
                                           $gg ='B1' ;
                                   }
                                   if(($total>60) && ($total<=70))
                                   {
                                           $grade1= 7;
                                           $gg ='B2' ;
                                   }
                                   if(($total>50) && ($total<=60))
                                   {
                                           $grade1 = 6;
                                           $gg ='C1' ;
                                   }
                                   if(($total>40) && ($total<=50))
                                   {
                                           $grade1 = 5;
                                           $gg ='C2' ;
                                   }
                                   if(($total>32) && ($total<=40))
                                   {
                                          $grade1 = 4;
                                          $gg ='D' ;
                                   }
                                   if(($total>20) && ($total<=32))
                                   {
                                           $grade1 = 3;
                                           $gg ='E1' ;
                                   }
                                    if(($total>=0) && ($total<=20))
                                   {
                                           $grade1 = 2;
                                           $gg ='E2' ;
                                   }
                                   
         
                                   
 }   //echo   $final_grade=$gg;        
$sum_total_fa_get_marks=0;
$sum_total_sa_get_marks=0;
     echo'<td align="center">'.$gg.'</td>';
    }
         $get_test_id="SELECT * FROM cce_test_table ";
     $exe_test_id=mysql_query($get_test_id);
     while($fetch_test_id=mysql_fetch_array($exe_test_id))
     {  
            $fa_id=$fetch_test_id[0];
      if(($fa_id==5)||($fa_id==6)||($fa_id==8)||($fa_id==9))
   {    
                  $get_fa_marks="SELECT DISTINCT  SUM(marks) AS marks  FROM  `cce_marks_scho_type_skills_table` 
                                         
                                  INNER JOIN  `cce_term_test_table` 
                                  ON `cce_term_test_table` .id=cce_marks_scho_type_skills_table.term_test_id

                                   INNER  JOIN  `cce_test_table` 
                                   ON `cce_test_table`.test_id=cce_term_test_table.test_id 
                                   
                                  INNER JOIN cce_subject_type_skills_id_table
                                  ON cce_subject_type_skills_id_table.sts_id=cce_marks_scho_type_skills_table.sts_id
                                WHERE cce_test_table.test_id=$fa_id AND cce_marks_scho_type_skills_table.student_id=$student_id "
                         . "AND cce_subject_type_skills_id_table.subject_id=$sub_id AND cce_term_test_table.class_id=$class_id   AND cce_marks_scho_type_skills_table.session_id=$session_id";
                          $fa_total_marks=mysql_query($get_fa_marks);
                         while($fetch_fa_marks=mysql_fetch_array($fa_total_marks))
                         {
                          
                             $sum_fa=$fetch_fa_marks['marks'];
                         '<br>'. $sum_total_fa_get_marks=$sum_total_fa_get_marks+$sum_fa.'';
                         }


   
     }
   

//     
    }
    
     //$get_fa=($sum_total_fa_get_marks*100)/100;
                                 //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($sum_total_fa_get_marks>90) && ($sum_total_fa_get_marks<=100))
                                   {
                                           $grade1 = 10;
                                           $gggfa ='A1' ;
                                   }
                                   if(($sum_total_fa_get_marks>80) && ($sum_total_fa_get_marks<=90))
                                   {
                                           $grade1 = 9;
                                           $gggfa ='A2' ;
                                   }
                                   if(($sum_total_fa_get_marks>70) && ($sum_total_fa_get_marks<=80))
                                   {
                                           $grade1 = 8;
                                           $gggfa ='B1' ;
                                   }
                                   if(($sum_total_fa_get_marks>60) && ($sum_total_fa_get_marks<=70))
                                   {
                                           $grade1= 7;
                                           $gggfa ='B2' ;
                                   }
                                   if(($sum_total_fa_get_marks>50) && ($sum_total_fa_get_marks<=60))
                                   {
                                           $grade1 = 6;
                                           $gggfa ='C1' ;
                                   }
                                   if(($sum_total_fa_get_marks>40) && ($sum_total_fa_get_marks<=50))
                                   {
                                           $grade1 = 5;
                                           $gggfa ='C2' ;
                                   }
                                   if(($sum_total_fa_get_marks>32) && ($sum_total_fa_get_marks<=40))
                                   {
                                          $grade1 = 4;
                                          $gggfa ='D' ;
                                   }
                                   if(($sum_total_fa_get_marks>20) && ($sum_total_fa_get_marks<=32))
                                   {
                                           $grade1 = 3;
                                           $gggfa ='E1' ;
                                   }
                                    if(($sum_total_fa_get_marks>=0) && ($sum_total_fa_get_marks<=20))
                                   {
                                           $grade1 = 2;
                                           $gggfa ='E2' ;
                                   }
    
    
   echo'<td align="center">'.$gggfa.'</td>';     
     
          
   /////////////////////total sa//////////////////////////////////////////////////
     $get_test_sa_id="SELECT * FROM cce_test_table ";
     $exe_test_sa_id=mysql_query($get_test_sa_id);
     while($fetch_test_sa_id=mysql_fetch_array($exe_test_sa_id))
     {  
            $sa_id=$fetch_test_sa_id[0];
      if(($sa_id==7)||($sa_id==10))
       {    
                   $get_sa_marks="SELECT DISTINCT  SUM(marks) AS marks  FROM  `cce_marks_scho_type_skills_table` 
                                         
                                  INNER JOIN  `cce_term_test_table` 
                                  ON `cce_term_test_table` .id=cce_marks_scho_type_skills_table.term_test_id

                                   INNER  JOIN  `cce_test_table` 
                                   ON `cce_test_table`.test_id=cce_term_test_table.test_id 
                                   
                                  INNER JOIN cce_subject_type_skills_id_table
                                  ON cce_subject_type_skills_id_table.sts_id=cce_marks_scho_type_skills_table.sts_id
                                WHERE cce_test_table.test_id=$sa_id AND cce_marks_scho_type_skills_table.student_id=$student_id "
                         . "AND cce_subject_type_skills_id_table.subject_id=$sub_id AND cce_term_test_table.class_id=$class_id  AND cce_marks_scho_type_skills_table.session_id=$session_id";
                          $sa_total_marks=mysql_query($get_sa_marks);
                         while($fetch_sa_marks=mysql_fetch_array($sa_total_marks))
                         {
                          
                              $sum_sa=$fetch_sa_marks['marks'];
                         '<br>'. $sum_total_sa_get_marks=$sum_total_sa_get_marks+$sum_sa.'';
                         }


   
     }
   

//     
 
    }
        $get_sa=($sum_total_sa_get_marks*100)/200;
      //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade1 = 0;
                                   if(($get_sa>90) && ($get_sa<=100))
                                   {
                                           $grade1 = 10;
                                           $gggsa  ='A1' ;
                                   }
                                   if(($get_sa>80) && ($get_sa<=90))
                                   {
                                           $grade1 = 9;
                                           $gggsa  ='A2' ;
                                   }
                                   if(($get_sa>70) && ($get_sa<=80))
                                   {
                                           $grade1 = 8;
                                           $gggsa  ='B1' ;
                                   }
                                   if(($get_sa>60) && ($get_sa<=70))
                                   {
                                           $grade1= 7;
                                           $gggsa  ='B2' ;
                                   }
                                   if(($get_sa>50) && ($get_sa<=60))
                                   {
                                           $grade1 = 6;
                                           $gggsa  ='C1' ;
                                   }
                                   if(($get_sa>40) && ($get_sa<=50))
                                   {
                                           $grade1 = 5;
                                           $gggsa  ='C2' ;
                                   }
                                   if(($get_sa>32) && ($get_sa<=40))
                                   {
                                          $grade1 = 4;
                                          $gggsa  ='D' ;
                                   }
                                   if(($get_sa>20) && ($get_sa<=32))
                                   {
                                           $grade1 = 3;
                                           $gggsa  ='E1' ;
                                   }
                                    if(($get_sa>=0) && ($get_sa<=20))
                                   {
                                           $grade1 = 2;
                                           $gggsa ='E2' ;
                                   }
    

    
    
   echo'<td align="center">'.$gggsa .'</td>';     
     $sum_fa_sa_total=$sum_total_sa_get_marks+$sum_total_fa_get_marks;
     
     $grand_total=($sum_fa_sa_total*100)/300;
      //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade_point = 0;
                                   if(($grand_total>90) && ($grand_total<=100))
                                   {
                                           $point = 10;
                                           $grand_total_grade ='A1' ;
                                   }
                                   if(($grand_total>80) && ($grand_total<=90))
                                   {
                                           $point = 9;
                                           $grand_total_grade ='A2' ;
                                   }
                                   if(($grand_total>70) && ($grand_total<=80))
                                   {
                                           $point = 8;
                                           $grand_total_grade ='B1' ;
                                   }
                                   if(($grand_total>60) && ($grand_total<=70))
                                   {
                                           $point= 7;
                                           $grand_total_grade ='B2' ;
                                   }
                                   if(($grand_total>50) && ($grand_total<=60))
                                   {
                                           $point = 6;
                                           $grand_total_grade ='C1' ;
                                   }
                                   if(($grand_total>40) && ($grand_total<=50))
                                   {
                                           $point = 5;
                                           $grand_total_grade ='C2' ;
                                   }
                                   if(($grand_total>32) && ($grand_total<=40))
                                   {
                                          $point = 4;
                                          $grand_total_grade ='D' ;
                                   }
                                   if(($grand_total>20) && ($grand_total<=32))
                                   {
                                           $point = 3;
                                           $grand_total_grade ='E1' ;
                                   }
                                    if(($grand_total>=0) && ($grand_total<=20))
                                   {
                                           $point = 2;
                                           $grand_total_grade ='E2' ;
                                   }
                                   
   echo'<td align="center">'.$grand_total_grade.'</td>';       
      
  $total_overall=$total_overall+$sum_fa_sa_total;
   
  $total_overall_per=($total_overall*100)/2100;
  
   $total_point=($sum_fa_sa_total*100)/300;
                                 //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade_point = 0;
                                   if(($total_point>90) && ($total_point<=100))
                                   {
                                           $grade_point = 10;
                                           $gg ='A1' ;
                                   }
                                   if(($total_point>80) && ($total_point<=90))
                                   {
                                           $grade_point = 9;
                                           $gg ='A2' ;
                                   }
                                   if(($total_point>70) && ($total_point<=80))
                                   {
                                           $grade_point = 8;
                                           $gg ='B1' ;
                                   }
                                   if(($total_point>60) && ($total_point<=70))
                                   {
                                           $grade_point= 7;
                                           $gg ='B2' ;
                                   }
                                   if(($total_point>50) && ($total_point<=60))
                                   {
                                           $grade_point = 6;
                                           $gg ='C1' ;
                                   }
                                   if(($total_point>40) && ($total_point<=50))
                                   {
                                           $grade_point = 5;
                                           $gg ='C2' ;
                                   }
                                   if(($total_point>32) && ($total_point<=40))
                                   {
                                          $grade_point = 4;
                                          $gg ='D' ;
                                   }
                                   if(($total_point>20) && ($total_point<=32))
                                   {
                                           $grade_point = 3;
                                           $gg ='E1' ;
                                   }
                                    if(($total_point>=0) && ($total_point<=20))
                                   {
                                           $grade_point = 2;
                                           $gg ='E2' ;
                                   }
                                   
   
   
   
   
        echo'<td align="center">'.$grade_point.'</td>';    
   $cgpa=$grade_point;
      $total_cgpa=$total_cgpa+$cgpa;
     $total_per=$total_cgpa/8;
     $calculata_cgpa=round($total_per,2);
     
        //grade for FA1,FA2,FA3,FA4,SA1,SA2
                                  $grade_point = 0;
                                   if(($total_per>9) && ($total_per<=10))
                                   {
                                           $grade_point = 10;
                                           $gg_cpga ='A1' ;
                                   }
                                   if(($total_per>8) && ($total_per<=9))
                                   {
                                           $grade_point = 9;
                                           $gg_cpga ='A2' ;
                                   }
                                   if(($total_per>7) && ($total_per<=8))
                                   {
                                           $grade_point = 8;
                                           $gg_cpga ='B1' ;
                                   }
                                   if(($total_per>6) && ($total_per<=7))
                                   {
                                           $grade_point= 7;
                                           $gg_cpga ='B2' ;
                                   }
                                   if(($total_per>5) && ($total_per<=6))
                                   {
                                           $grade_point = 6;
                                           $gg_cpga ='C1' ;
                                   }
                                   if(($total_per>4) && ($total_per<=5))
                                   {
                                           $grade_point = 5;
                                           $gg_cpga ='C2' ;
                                   }
                                   if(($total_per>3) && ($total_per<=4))
                                   {
                                          $grade_point = 4;
                                          $gg_cpga ='D' ;
                                   }
                                   if(($total_per>2) && ($total_per<=3))
                                   {
                                           $grade_point = 3;
                                           $gg_cpga ='E1' ;
                                   }
                                    if(($total_per>=0) && ($total_per<=2))
                                   {
                                           $grade_point = 2;
                                           $gg_cpga ='E2' ;
                                   }
    
   
     
     
}

echo'   </tbody></table>';
    

echo'<table  class="table table-bordered table-striped" width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;"><tbody>
 <tr align="center">
            <td align="left" style="font-size:20px" colspan="5"><b>Attendance</b> : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;90/90</td>
            <td colspan="" align="center" style="font-size:19px">&nbsp;&nbsp;&nbsp;&nbsp;210/210&nbsp;&nbsp;</td>
                    <td colspan="5" align="center" style="font-size:12px"><b>Eetremely regular student. keep it up !</b></td>
            <td style="font-size:18px;">&nbsp;&nbsp;&nbsp;&nbsp;CGPA&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="font-size:18px">&nbsp;&nbsp;&nbsp;'.$calculata_cgpa.'&nbsp;&nbsp;</td>
          
          
        </tr>
 <tr align="center" style="border:1px solid black;border-collapse:collapse;">
           
            <td colspan="11" align="left"><font style="font-size:11px;"><b>Note:A1=91-100%;A2=81-90%;B1=71-80%;B2=61-70%;C1=51-60%;C2=41-50%;D=33-40%;E1=21-32%;E2=0-20% AND BELOW</b></td>
            
           
            <td colspan="" style="font-size:14px"><b>Overall Grade</b></td>
            <td colspan="" style="font-size:20px"><b>'.$gg_cpga.'</b></td>
                 
        </tr></tbody></table>';


// to ptint the scholastic area************************************************************************
 echo'
<table  class="table table-bordered table-striped"  width="100%" style="border:1px solid black;border-collapse:collapse;" cellpadding="2">
<thead >                            
<tr><th colspan="3" align="center" width="40%" style="background-color:#CCCCCC;-webkit-print-color-adjust: exact" ><font style="font-size:16px;">Co-SCHOLASTIC AREA</font></th>
</tr>
<tr><th align="center" width=""><font style="font-size:17px;">ACTIVITY</font></th><th align="center" style="width:5%;"><font style="font-size:16px;">GRADE</th></font><th align="center" ><font style="font-size:17px;">INDICATOR NAME</font></th></tr>
  </thead>
<tbody>';
 $ctr=0;
 $coscho_area="SELECT * FROM cce_coscho_fields ";
$exe_coscho=mysql_query($coscho_area);
while($coscho_area_name=mysql_fetch_array($exe_coscho))
{   $coscho_name=$coscho_area_name[2];
       $coscho_id=$coscho_area_name[0];
       $cosho=$coscho_area_name['cosho_area_id'];
       $ctr++;
    echo'<tr><td width="28%" height="30"> <b size="4"style="font-size:16px">'.$coscho_name.'</b></font></td>';
    $get_grade="SELECT * FROM cce_coscho_indicator_marks_table WHERE student_id=$student_id  AND coscho_id=  $coscho_id  AND session_id=$session_id";
    $exe_grade= mysql_query($get_grade);
    while($fetch_grade=  mysql_fetch_array($exe_grade))
    {             $grade=$fetch_grade['grade'];
    
   $coscho_indicator_name="SELECT *
            FROM cce_coscho_indicator_table  WHERE coscho_id=$coscho_id  AND  grade='$grade '";
    $exe_indicator=mysql_query($coscho_indicator_name);
    while($fetch_indicator=  mysql_fetch_array($exe_indicator))
    {             $indicato_name=$fetch_indicator[2];

       echo'<td align="center" width="3%" ><b style="font-size:18px">'.$fetch_indicator['grade'].'</b></td><td width="70%"><b style="font-size:12px" >'.$name_student.'</b><font  style="font-size:16px">   '. $indicato_name.'</font></td></tr>';
    }
}}
echo'</tbody></table>';




// to print the bottom part signature and self awerness........................
echo'<table width="100%" height="" style="border:1px solid black;border-collapse:collapse;"><tbody>    
    <tr align="center" style="border:1px solid black; border-collapse:collapse;">
            <td colspan="13" style="background-color:#CCCCCC;-webkit-print-color-adjust: exact"><font  style="font-size:16px">
<b>SELF AWARENESS</b></font></td>
        </tr>
         <tr align="center" style="border:0px solid black;border-collapse:collapse;">
            <td colspan="2" celpadding="10%"><font size="4"style="font-size:17px" ><b>My Goals</b></td>
             <td colspan="11" ><font size="4" style="font-size:17px"><b>My Interests and Hobbies</b></td>
        </tr>
         <tr align="center" style="border:0px solid black;border-collapse:collapse;">
            <td colspan="2" align="left"><font size="3"  style="font-size:16px">I will become an electrical engineer some day.</td>
             <td colspan="11" align="left"><font size="3"  style="font-size:16px">My favourite hobby is playing snooker. I enjoy working on the computer</td>
        </tr>
        <tr align="center" style="border:0px;" style="border:0px solid black;border-collapse:collapse;">
         <td colspan="13" align="left" width="100%" ><font size="3"><b  style="font-size:18px" width="100%">CONGRATULATIONS !</b> <b  style="font-size:16px" > Your ward is promoted to </b><b  style="margin-left:20px; width:100%;"  style="font-size:20px"><font size="4">Class:</font></b><b  style="margin-left:40px; width:100%;"  style="font-size:22px"><font size="4">New Session beings on .......</b></td>

            </tbody></table>';

echo'<table  width="100%"  style="border:0px;" width=""></tbody>
       </tr>
       <tr align="center"  height="160px;" >
         <td  valign="bottom"  width="50%"  style="border:0px;" style="font-size:25px" align="left"><b>DATE:</b></td>
           <td  valign="bottom"  width="50%"  style="border:0px;" style="font-size:25px" ><b style="margin-left:-460px; width:100%;">Class Teacher\'s signature</b></td>
           <td valign="bottom"  width="0%"  style="border:0px;" style="font-size:25px"><b style="margin-left:-150px; width:100%;">Principal\'s signature</b></td>
       </tr>
      
         ';
    echo' </tbody></table> ';
    

}


else if (($class_id==1)||($class_id==2)||($class_id==3)||($class_id==4)||($class_id==5)||($class_id==6))
    {

?>
    <style>
table,th,td,tr
{
border:1px solid black;
font-size: 22px;
}
</style>
    <?php
 $jke=1;
//get the name of the student on that id
$subject_id=0;
$get_s_name=
"SELECT Name,`Father's Name`,`Mother's Name`,admission_no,DOB,`Blood Group`
FROM student_user
WHERE sId = ".$_GET['student_id']."";
$exe_s_name=mysql_query($get_s_name);
$fetch_s_name=mysql_fetch_array($exe_s_name);
$name_student=$fetch_s_name[0];
$blood=$fetch_s_name['Blood Group'];
$marks_term=0;
// qurey to get the class full details.
     $get_class_name = 
	 "SELECT class_index.*
	 FROM class 
	 INNER JOIN class_index
	 ON class_index.cId=class.classId
	 WHERE class.sId = ".$_GET['student_id']."";
	 $exe_class_name=mysql_query($get_class_name);
	 $fetch_cls_name=mysql_fetch_array($exe_class_name);
	 $class_name=$fetch_cls_name['class'];
          $section_name=$fetch_cls_name['section'];
  echo '<p style="z-index:1; position:absolute; top:0px; left:72px"><font style="font-size:32px;"><strong>VIDYA BAL BHAWAN SR. SEC. SCHOOL </strong></font></p>';
    echo'
<table  width="100%" style="border:0px">
<tbody ><tr><td>
<table  width="100%" style="border:0px">
<tbody style="border:0px">
<tr style="border:0px">
<td align="left" width="25%" style="border:0px"></td>
</tr>
<tr style="border:0px">';echo'<td align="left" style="border:0px"position:absolute; top:0px; ><img src="../images/logo/School.jpg" width="30%"  ></td>';
echo'<div ><td  valign="top" width="100%" style="margin-left:-2px; width:100%;border:0px"><br>
<font style="margin-left:-10px;font-size:18px"  >
      Kondli Gharoli Road, Mayur vihar III, Delhi -110096 </font>
     <br>
    <b style="margin-left:-10px; width:100%;"><font style="font-size:24px;">Half  yearly Assessment(2014-2015)</b>  </font> </td>
</tr>
</tbody>
</table>';

echo '
<body aline="center">          
<div class="row-fluid">
<div class="widget  span12 clearfix">
<div class="widget-header">
<table  width="100%" id=8  height=80 valign="top" style="border:1px solid black;border-collapse:collapse;" >
<tbody >
<tr width="30%" style="border:0px solid black;border-collapse:collapse;">
<td align="left" style="border:0px solid black;border-collapse:collapse;"><font size="3">Student Name: </font><b  style="font-size:14px">'.$name_student.'</b></td>
<td align="left"  style="border:0px solid black;border-collapse:collapse;"><font size="3">Class :</font><b  style="font-size:14px" width="25%"> Pre-Primary      '.$section_name.' </b><font size="3">    Admission No.:<b style="margin-left:10px; width:100%;"  style="font-size:14px" >'.$fetch_s_name[3].'    </b></font></td>
<td align="left" style="border:0px solid black;border-collapse:collapse;"><font size="3"> Roll.No:<b></td>
</tr>
<tr width="30%" style="border:0px solid black;border-collapse:collapse;">
<td align="left" style="border:0px solid black;border-collapse:collapse;" ><font size="3">Fathers Name: <b style="font-size:14px">'.$fetch_s_name[1].'</b></font></td>
<td align="left"width="30" style="border:0px solid black;border-collapse:collapse;"><font size="3">Mothers Name:<b style="font-size:14px"  width="25%">'.$fetch_s_name[2].' </b></font></td> 
<td align="left" style="border:0px solid black;border-collapse:collapse;"><font size="3">D.O.B.-<b style="font-size:14px" >'.date("jS F Y",strtotime($fetch_s_name[4])).' <b></b></b></font></td>
</tr>

</tbody></table>  							
       </div><!-- End widget-header -->	          							

<table  class="table table-bordered table-striped" width="100%"  height="350" style="border:1px solid black;border-collapse:collapse;" >
<thead >                                          
<tr><tr style="background-color:#CCCCCC;-webkit-print-color-adjust: exact;"><th colspan="7" align="left" style="font-size:16px"><i>My teacher says...</i></th></tr>
<th rowspan="3"><font size="" style="font-size:16px">SUBJECT</font></th>
<th colspan="5" align="center"><font size="4" style="font-size:16px"><i>Scholastic Area</i></font></th>
<th width="35%"><font size="" style="font-size:16px"> GRADE</font></th>										
 </tr>     																	
  </thead>
  
'; 
$max=0;
      $array_i=0;
        $array_j=0;
    $sum_marks=0;
    $k=1;
    $n=2;
    $x=0;
    $y=0;
    $c=0;
    $d=0;
    
    $array_1=array();
    $array_2=array();
$array_ass_1=array();
$array_ass_2=array(); 
$ct=0;
$total_marks=0;
$ass1_marks=array();
$marks_term=0;
$total_max_marks=0;
 $marks_total=0;
$get_visitor_year="SELECT DISTINCT subject.subject , subject.subject_id
               FROM  `subject`
                INNER JOIN grade_cce_assign_subject_activity
               ON subject.subject_id=grade_cce_assign_subject_activity.subject_id
               

";
$execute_visitor_year=mysql_query($get_visitor_year);
while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
{       
                $subject=$fetch_visitor_year['subject'];
                $id=$fetch_visitor_year['subject_id'];
                $test_id=$fetch_visitor_year['test_id'];
                
//       echo' <tr ><td width="10%" ><font size="" style="font-size:18px" ><b>'.$subject.'</b></font>';     
          
                     $marks_term=0;
            $get_test_id="SELECT  DISTINCT cce_test_table.test_id  ,cce_test_table.max_marks FROM  cce_test_table  "
                         . "INNER JOIN  grade_cce_marks_activity"
                          . " ON grade_cce_marks_activity.test_id=cce_test_table.test_id";
                    $exe_test_table=mysql_query($get_test_id);
                  while($fetch_test_id=mysql_fetch_array($exe_test_table))
                   {   
                       $test_id=$fetch_test_id['test_id'];
                                                                                                     
if(($test_id==11)||($test_id==12)||($test_id==13)||($test_id==14))
{         
  
       $get_subject="SELECT  subject. *,grade_cce_activity_name.*,grade_cce_marks_activity. marks
               FROM  `subject`
               INNER JOIN grade_cce_assign_subject_activity
               ON subject.subject_id=grade_cce_assign_subject_activity.subject_id
               INNER JOIN grade_cce_activity_name
               ON grade_cce_activity_name.id=grade_cce_assign_subject_activity.activity_id
               INNER JOIN class_index
               ON class_index.level=grade_cce_assign_subject_activity.level
               INNER JOIN grade_cce_marks_activity
               ON grade_cce_marks_activity.activity_id=grade_cce_assign_subject_activity.activity_id
               AND  grade_cce_marks_activity.subject_id=grade_cce_assign_subject_activity.subject_id
              WHERE class_index.cId=$class_id  AND grade_cce_assign_subject_activity.subject_id=$id
             AND grade_cce_marks_activity.student_id=$student_id  AND grade_cce_marks_activity.test_id=$test_id
                   ";
$execute_subject=mysql_query($get_subject);
mysql_query("SET NAMES 'utf8_bin'");
while($fetch_subject=mysql_fetch_array($execute_subject))//looping for getting year
{   
          $subject=$fetch_subject['subject'];
          $activity=$fetch_subject['activity_name'];
          $activity_id=$fetch_subject['id'];
          $subject_id=$fetch_subject['subject_id'];
         $marks=$fetch_subject['marks'];
         
         if($test_id==15)
         {
               $array_1[$x]=$fetch_subject['marks'];
              $x++;
         }
         elseif($test_id==16)
         {
               
             $array_2[$y]=$fetch_subject['marks'];
             $y++;
         }
              elseif($test_id==17)
         {
               $array_3[$c]=$fetch_subject['marks'];
              $c++;
         }
         elseif($test_id==18)
         {
               
             $array_4[$d]=$fetch_subject['marks'];
             $d++;
         }  
         
               
}          
$val_1=array();
$val_2=array();
$val_3=array();
$val_4=array();
for($z=0; $z<$x; $z++)//ASS--1
{
      
    $val_1[$z]=$array_1[$z] ;
     
}
//
for($z=0; $z<$y; $z++)//ASS--2
{
     
    $val_2[$z]=$array_2[$z];
          
}
for($z=0; $z<$c; $z++)//ASS--3
{
      
    $val_3[$z]=$array_3[$z] ;
     
}
//
for($z=0; $z<$d; $z++)//ASS--4
{
     
    $val_4[$z]=$array_4[$z];
          
}

for($w=0; $w<$z; $w++)//SUM OF ASS
{
      
      
$sum_total = $val_1[$w] + $val_2[$w]+$val_3[$w] + $val_4[$w];
                        
}
              
}
}}

///////////////////////*************************************************************************************************************
$r_next=0;

$get_visitor_year="SELECT DISTINCT subject.subject , subject.subject_id
               FROM  `subject`
                INNER JOIN grade_cce_assign_subject_activity
               ON subject.subject_id=grade_cce_assign_subject_activity.subject_id
               

";
$execute_visitor_year=mysql_query($get_visitor_year);
while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
{       
                $subject=$fetch_visitor_year['subject'];
                $id=$fetch_visitor_year['subject_id'];
                $test_id=$fetch_visitor_year['test_id'];
                
       echo' <tr ><td width="10%" ><font size="" style="font-size:18px" ><b>'.$subject.'</b></font>';     
          
                     $marks_term=0;
            $get_test_id="SELECT  DISTINCT cce_test_table.test_id  ,cce_test_table.max_marks FROM  cce_test_table  "
                         . "INNER JOIN  grade_cce_marks_activity"
                          . " ON grade_cce_marks_activity.test_id=cce_test_table.test_id";
                    $exe_test_table=mysql_query($get_test_id);
                  while($fetch_test_id=mysql_fetch_array($exe_test_table))
                   {   
                       $test_id=$fetch_test_id['test_id'];
                                                                                                     
if(($test_id==15)||($test_id==16)||($test_id==17)||($test_id==18))
{         
  
       $get_subject="SELECT  subject. *,grade_cce_activity_name.*,grade_cce_marks_activity. marks
               FROM  `subject`
               INNER JOIN grade_cce_assign_subject_activity
               ON subject.subject_id=grade_cce_assign_subject_activity.subject_id
               INNER JOIN grade_cce_activity_name
               ON grade_cce_activity_name.id=grade_cce_assign_subject_activity.activity_id
               INNER JOIN class_index
               ON class_index.level=grade_cce_assign_subject_activity.level
               INNER JOIN grade_cce_marks_activity
               ON grade_cce_marks_activity.activity_id=grade_cce_assign_subject_activity.activity_id
               AND  grade_cce_marks_activity.subject_id=grade_cce_assign_subject_activity.subject_id
              WHERE class_index.cId=$class_id  AND grade_cce_assign_subject_activity.subject_id=$id
             AND grade_cce_marks_activity.student_id=$student_id  AND grade_cce_marks_activity.test_id=$test_id
                   ";
$execute_subject=mysql_query($get_subject);
mysql_query("SET NAMES 'utf8_bin'");
while($fetch_subject=mysql_fetch_array($execute_subject))//looping for getting year
{   
          $subject=$fetch_subject['subject'];
          $activity=$fetch_subject['activity_name'];
          $activity_id=$fetch_subject['id'];
          $subject_id=$fetch_subject['subject_id'];
         $marks=$fetch_subject['marks'];
         
         if($test_id==15)
         {
               $array_1[$x]=$fetch_subject['marks'];
              $x++;
         }
         elseif($test_id==16)
         {
               
             $array_2[$y]=$fetch_subject['marks'];
             $y++;
         }
         elseif($test_id==17)
         {
               $array_3[$c]=$fetch_subject['marks'];
              $c++;
         }
         elseif($test_id==18)
         {
               
             $array_4[$d]=$fetch_subject['marks'];
             $d++;
         }  
         
            if($test_id==15)     
            {
                 
                   $sum_total_next = $val_1[$r_next] + $val_2[$r_next]+$val_3[$r_next] + $val_4[$r_next];    //sum of asses
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                    $max_marks=40;
                    
//                   if($jke<4)
//{
// $total_max_marks=$total_max_marks+$max_marks;
//}
//$jke++;
// $total_max_marks;
//$total=($get_marks*100)/$total_max_marks;
 
                    
               $total_max_marks=$total_max_marks+$max_marks;
               $marks_term=$get_marks_terms+$get_marks_terms  ;
             $total_marks=($marks_term*100)/$total_max_marks;
             $sum_total_next;
                         $grade1 = 0;
                                   if(($sum_total_next>34) && ($sum_total_next<=40))
                                   {
                                           $grade1 = 10;
                                           $g ='A+' ;
                                   }
                                   if(($sum_total_next>29) && ($sum_total_next<=34))
                                   {
                                           $grade1 = 9;
                                           $g ='A' ;
                                   }
                                   if(($sum_total_next>24) && ($sum_total_next<=28))
                                   {
                                           $grade1 = 8;
                                           $g ='B+' ;
                                   }
                                   if(($sum_total_next>19) && ($sum_total_next<=24))
                                   {
                                           $grade1= 7;
                                           $g ='B' ;
                                   }
                                   if(($sum_total_next>14) && ($sum_total_next<=19))
                                   {
                                           $grade1 = 6;
                                           $g ='C+' ;
                                   }
                                   if(($sum_total_next>9) && ($sum_total_next<=14))
                                   {
                                           $grade1 = 5;
                                           $g ='C' ;
                                   }
                                   if(($sum_total_next>=0) && ($sum_total_next<=9))
                                   {
                                          $grade1 = 4;
                                          $g ='D' ;
                                   }
                                   


                
  echo'<td  width="15%" height="7%" align="center" style="font-size:14px" valign="top">'. $g.' <br><p position:absolute; top:12px; style="margin-top:22px;font-size:14px" ></p>'.$activity.'</td>'; 
   $r_next++;
    }
    
}          


                  

       $total_marks="SELECT  SUM(marks) AS marks
               FROM  `grade_cce_marks_activity`
               
              WHERE  student_id=$student_id  AND subject_id=$subject_id  AND test_id >=11 AND  test_id <=14 ";
             $exe_query=mysql_query($total_marks);
             while($fetch_query=  mysql_fetch_array($exe_query))
             {
             
                 
           $total=$fetch_query[0];
             
//echo $tot_mark=($total*100)/200;
                                     $grade1 = 0;
                                   if(($total>174) && ($total<=200))
                                   {
                                           $grade1 = 10;
                                           $g ='A+' ;
                                   }
                                   if(($total>149) && ($total<=174))
                                   {
                                           $grade1 = 9;
                                           $g ='A' ;
                                   }
                                   if(($total>124) && ($total<=149))
                                   {
                                           $grade1 = 8;
                                           $g ='B+' ;
                                   }
                                   if(($total>99) && ($total<=124))
                                   {
                                           $grade1= 7;
                                           $g ='B' ;
                                   }
                                   if(($total>74) && ($total<=99))
                                   {
                                           $grade1 = 6;
                                           $g ='C+' ;
                                   }
                                   if(($total>49) && ($total<=74))
                                   {
                                           $grade1 = 5;
                                           $g ='C' ;
                                   }
                                   if(($total>=0) && ($total<=49))
                                   {
                                          $grade1 = 4;
                                          $g ='D' ;
                                   }
                                   

                                        
                                        if($test_id==15)
                                        {
                                                                                                       
           echo'<td valign="top" width="35%"><b  style="margin-left:40px; width:100%;" style="font-size:14px"><font size="3">'.$g.'</font></b></td>';
                                        }
                                                            
                                                                                            
                                                                       
                                                                                            
            }   
}
}}


//********************************************************************************************************************************

echo '
</tbody></table>';

$co_curricular = array();

//query to get the activity
   $get_co_curricular_act = "SELECT  DISTINCT id,co_activity_name FROM cce_co_scho_activity_table WHERE term=2 AND level=0";
   $exe_co_curricular_act = mysql_query($get_co_curricular_act);
   while($fetch_co_curricular_act = mysql_fetch_array($exe_co_curricular_act))
   {
     $activity = $fetch_co_curricular_act['co_activity_name'];
      //query to get the skill from cce_co_scho_area_table table
    $get_co_curricular_skill = "SELECT id,co_skill FROM cce_co_scho_area_table WHERE co_activity_id=".$fetch_co_curricular_act['id']." AND level=0 ";
       $exe_co_curricular_skill = mysql_query($get_co_curricular_skill);
       $num_rows = mysql_num_rows($exe_co_curricular_skill);
       if($num_rows>0)
       {
       while($fetch_co_curricular_skill = mysql_fetch_array($exe_co_curricular_skill))
             {
           $skill = $fetch_co_curricular_skill['co_skill'];
              //query to get the marks from cce_co_scho_marks_table table
             $get_co_curricular_marks = "SELECT grade FROM cce_co_scho_marks_table WHERE student_id = $student_id AND class_id = $class_id AND term_id = 2 AND activity_id = ".$fetch_co_curricular_act['id']." AND skill_id =".$fetch_co_curricular_skill['id']." ";
             
               $exe_co_curricular_marks = mysql_query($get_co_curricular_marks);
               $fetch_co_curricular_marks = mysql_fetch_array($exe_co_curricular_marks);
               $fetch_co_curricular_marks['grade'];
            $co_curricular[$activity][$skill]['g'] = $fetch_co_curricular_marks['grade'];
          
            }    
       }
       else
       {
           //query to get the marks from cce_co_scho_marks_table table
            $get_co_curricular_marks0 = "SELECT grade FROM cce_co_scho_marks_table WHERE student_id = $student_id AND class_id = $class_id AND term_id = 2 AND activity_id = ".$fetch_co_curricular_act['id']." AND skill_id = 0 ";
            
             $exe_co_curricular_marks0 = mysql_query($get_co_curricular_marks0);
             $fetch_co_curricular_marks0 = mysql_fetch_array($exe_co_curricular_marks0);
             
            $co_curricular[$activity][$activity]['g'] = $fetch_co_curricular_marks0['grade'];
       }
           
 }
  $co_curricular[$activity][$activity]['g'];




 
 echo'<table class="table table-bordered table-striped"  width="100%" valign="top" align="center" style="border:1px solid black;border-collapse:collapse;">
        <thead>
              <tr style="border:1px solid black;">
                <th align="center"  style="border:1px solid black;" width="50%" colspan="7"><b  style="font-size:16px">Co-Curricular Activities</b></th><th align="center" style="border:1px solid black;"  colspan="7"><b  style="font-size:16px" >Personality  Development</b></th></tr>
            <tr height="30"><th colspan="3"  style="font-size:14px">ACTIVITY</th><th  style="font-size:14px" width="4%">GRADE</th><th colspan="3"  style="font-size:14px" width="18%">REMARKS</th><th colspan="3"  style="font-size:14px" width="25%">ACTIVITY</th><th  style="font-size:14px" width="4%">GRADE</th><th colspan="3"  style="font-size:14px">REMARKS</th></tr>
                  </thead>';
echo' <tr height="30"><td colspan="" rowspan="3" style="font-size:14px" width="8%"><b>GAMES</td><td style="font-size:14px" colspan="2" width="10%">TeamSpirit</td><td  style="font-size:16px" align="center"><b>'.$co_curricular['GAMES']['Team Spirit']['g'].'</b></td><td colspan="3"></td><td style="font-size:14px" align="left" colspan="3"><b>Attitude</td><td style="font-size:16px" align="center"><b>'.$co_curricular['Attitude']['Attitude']['g'].'</b></td><td colspan="3""></td></tr>        ';
echo' <tr height="30"><td style="font-size:14px" colspan="2">Discipline</td><td style="font-size:16px" align="center"><b>'.$co_curricular['GAMES']['Discipline']['g'].'</b></td><td colspan="3"></td><td style="font-size:14px" align="left" colspan="3"><b>Care of Belongings</td><td style="font-size:16px" align="center"><b>'.$co_curricular['Care of Belongings']['Care of Belongings']['g'].'</b></td><td colspan="3""></td></tr>        ';
echo' <tr height="30"><td style="font-size:14px" colspan="2">Enthusiasm</td><td style="font-size:16px" align="center"><b>'.$co_curricular['GAMES']['Enthusiasm']['g'].'</b></td><td colspan="3"></td><td style="font-size:14px" align="left" colspan="3"><b>Regularity & Punctuality</td><td style="font-size:16px" align="center"><b>'.$co_curricular['Regularity & Punctuality']['Regularity & Punctuality']['g'].'</b></td><td colspan="3""></td></tr>        ';
echo' <tr height="30"><td  rowspan="2" style="font-size:14px"><b>GENERAL AWARENESS</td><td style="font-size:14px" colspan="2">Awareness</td><td  style="font-size:16px" align="center"><b>'.$co_curricular['GENERAL AWARENESS']['Awareness']['g'].'</b></td><td colspan="3"></td><td style="font-size:14px" align="left" colspan="3"><b>Sharing & Caring</td><td style="font-size:16px" align="center"><b>'.$co_curricular['Sharing & Caring']['Sharing & Caring']['g'].'</b></td><td colspan="3""></td></tr>        ';
echo' <tr height="30"><td style="font-size:14px" colspan="2">Promptness</td><td  style="font-size:16px" align="center"><b>'.$co_curricular['GENERAL AWARENESS']['Promptness']['g'].'</b></td><td colspan="3"></td><td style="font-size:14px" align="left"colspan="3"><b>Confidence</td><td style="font-size:16px" align="center"><b>'.$co_curricular['Confidence']['Confidence']['g'].'</b></td><td colspan="3""></td></tr>        ';
echo' <tr height="30"><td colspan="" rowspan="3" style="font-size:14px"><b>MUSIC& DANCE</td><td style="font-size:14px" colspan="2">Melody</td><td style="font-size:16px" align="center"><b>'.$co_curricular['MUSIC & DANCE']['Melody']['g'].'</b></td><td colspan="3"></td><td style="font-size:14px" align="left" colspan="3"><b>Discipline</td><td style="font-size:16px" align="center"><b>'.$co_curricular['Discipline']['Discipline']['g'].'</b></td><td colspan="3""></td></tr>        ';
echo' <tr height="30"><td style="font-size:14px" colspan="2">Rhythm</td><td style="font-size:16px" align="center"><b>'.$co_curricular['MUSIC & DANCE']['Rhythm']['g'].'</b></td><td colspan="3"></td><td style="font-size:14px" align="left" colspan="3"><b>Neatness</td> <td style="font-size:16px" align="center"><b>'.$co_curricular['Neatness']['Neatness']['g'].'</b></td><td colspan="3""></td>       ';
echo' <tr height="30"><td style="font-size:14px" colspan="2">Interest</td><td style="font-size:16px" align="center"><b>'.$co_curricular['MUSIC & DANCE']['Interest']['g'].'</b></td><td colspan="3"></td><td style="font-size:14px" align="left" colspan="3"><b>Imaginative Skills</td><td style="font-size:16px" align="center"><b>'.$co_curricular['Imaginative Skills']['Imaginative Skills']['g'].'</b></td><td colspan="3""></td></tr>        ';
  


echo'
    <table align="left" id=77  width="100%"  style="border:1px solid black;border-collapse:collapse;" ><tbody>
<td style="float:left" valign="bottom"><font size="4"><b>Attendance:</font><b  style="margin-left:100px; width:100%;"><font size="4">Blood Group:</b></font><b  style="margin-left:100px; width:100%;"><font size="4">Height:</b></font><b  style="margin-left:100px; width:100%;"><font size="4">Weight:</b></font></td></b></tr>
<td  style="float:left" valign="bottom" height="70"><b><font size="4"><b >Parent\'s signature</b><font size="4"><b  style="margin-left:60px; width:100%;">Class Teacher\'s signature</font></b><b  style="margin-left:90px; width:100%;"><font size="4">Principal\'s signature</b></font></td><tr style="background-color:#CCCCCC;-webkit-print-color-adjust: exact;"><td align="left"   valign="top" style="font-size:14px" ><i><b>Out Standing  ( A+)</b><b style="margin-left:40px; width:100%;">Excellent ,Keep it up !  ( A)</b><b style="margin-left:30px; width:100%;" style="font-size:14px">Very Good   (B)</b><b style="margin-left:40px; width:100%;" style="font-size:14px">Good   (C)</b><b style="margin-left:50px; width:100%;" style="font-size:14px">Not Satisfactory   (D)</b></i></td></tr>
</tbody></table></table>

';
 
      
echo'</tbody></table>';





    }

?>