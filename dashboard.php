<?php
require_once('include/functions_dashboard.php');
require_once('config/config.php');
require_once('include/session.php');
require_once('include/userdetail.php');
require_once('include/check.php');
require_once('track_user.php');
logged_in();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <title>School Management System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon.ico"/> 

        <!-- CSS Stylesheet-->
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap-responsive.css" />
        <link type="text/css" rel="stylesheet" href="css/zice.style.css"/>

		
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->  
		
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.ui.min.js"></script> 
		<script type="text/javascript" src="components/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="components/ui/timepicker.js"></script>
        <script type="text/javascript" src="components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="components/form/form.js"></script>
        <script type="text/javascript" src="components/elfinder/js/elfinder.full.js"></script>
        <script type="text/javascript" src="components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="components/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="components/jscrollpane/jscrollpane.min.js"></script>
        <script type="text/javascript" src="components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="components/chosen/chosen.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="components/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="components/flot/flot.js"></script>
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>       
		<script type="text/javascript" src="components/Jcrop/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="components/smartWizard/jquery.smartWizard.min.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/zice.custom.js"></script>
		  <script type="text/javascript" src="js/zice.custom.js"></script>
          <script type="text/javascript" src="users/js/manage_users.js"></script>

		<script type="text/javascript">
		$(function() {		
		// Calendar 
			var date = new Date();
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();		
			$('#calendar').fullCalendar({
				header: {
					left: 'title',
					center: 'prev,next  ',
					right: 'today month,basicWeek,agendaDay'
				},
			  buttonText: {
					prev: 'Previous',
					next: 'Next '
				},
				editable: true,
				refetchEvents :'refetchEvents',
				selectable: true,
				selectHelper: true,
				dayClick: function(date, allDay, jsEvent, view) {
				var nDate=$.fullCalendar.formatDate( date, 'd' );
				var dDate=$.fullCalendar.formatDate( date, 'dddd ' );
				var fullDate=$.fullCalendar.formatDate( date, ' MMMM , yyyy' );
				var date_add_event=$.fullCalendar.formatDate( date,'yyyy-MM-dd');
				var date_delete_event=$.fullCalendar.formatDate( date,'yyyy-MM-dd');
				<?php
				if($_SESSION['priv']==1)
				{
					echo '
				window.location.href = "view_calender.php?date="+date_add_event;	
				';}
				else
				{
					
				}
				
				?>
				
				
				$('#calendar .fc-header-title h2').html('<div class="dateBox" ><div class="nD">'+nDate+'</div><div class="dD" >'+fullDate+'</div><div></div><div class="clear"></div>');
				},events: [
					
						<?php
						$query=
						"SELECT  `message`,`date_id`
                        FROM  `reminder_calendar` ";
						$exe_rem=mysql_query($query);
					
					while($fetch=mysql_fetch_array($exe_rem))
						{
							//get date on the date id
							$get_date=
							"SELECT date
							FROM dates_d
							WHERE date_id = ".$fetch[1]."";
						    $exe_date=mysql_query($get_date);
							$fetch_date=mysql_fetch_array($exe_date);
							$date=$fetch_date[0];
							$data=$fetch[0];
							$date_format=explode("-",$date);
							$y=$date_format[0];
							$m=$date_format[1];
							$d=$date_format[2];
								if($data == " ")
						{
						$cm = ' ';
						}
						else
						{
							$cm =',';
						
						}
						
						
						
						echo"
						{
						title: '".$data."',";
							echo "
						start: new Date($y,$m-1,$d)
					
						";
					
						
						echo "}".$cm."";
						
						}
						
						?>
						
					 
				]
			});  
		}); 
		</script>

		</head>        
        <body>        
<div id="header">
<?php

 top_header();//function for calling header in function_admin.php

?>
</div>


<div id="left_menu">
<?php

 left_menu();//function for calling left menu in function_admin.php

?>
 </div>

<div id="content" >
<div class="inner">
      
<?php
 top_menu();//function for calling top menu in function_admin.php
?>



<?php
    dashboard();//function for calling dashboard in function_admin.php
?>




<?php
 footer(); //function for calling footer in function_admin.php
?>       

 <script type="text/javascript">
$("#dash").addClass("select");
</script>  
        </body>
      </html>