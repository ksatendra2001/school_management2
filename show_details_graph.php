<?php
require_once('include/functions_dashboard.php');
require_once('config/config.php');
require_once('include/session.php');
require_once('include/userdetail.php');
require_once('include/check.php');
require_once('include/grades_cce.php');

logged_in();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <title>School Management System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon.ico"/> 

        <!-- CSS Stylesheet-->
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap-responsive.css" />
        <link type="text/css" rel="stylesheet" href="css/zice.style.css"/>

		
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->  
		<script type="text/javascript" src="grades_cce/js/cce.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.ui.min.js"></script> 
		<script type="text/javascript" src="components/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="components/ui/timepicker.js"></script>
        <script type="text/javascript" src="components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="components/form/form.js"></script>
        <script type="text/javascript" src="components/elfinder/js/elfinder.full.js"></script>
        <script type="text/javascript" src="components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="components/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="components/jscrollpane/jscrollpane.min.js"></script>
        <script type="text/javascript" src="components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="components/chosen/chosen.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="components/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="components/flot/flot.js"></script>
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>       
		<script type="text/javascript" src="components/Jcrop/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="components/smartWizard/jquery.smartWizard.min.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/zice.custom.js"></script>
		 

		<script type="text/javascript">
		$(function() {		
		// Calendar 
			var date = new Date();
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();		
			$('#calendar').fullCalendar({
				header: {
					left: 'title',
					center: 'prev,next  ',
					right: 'today month,basicWeek,agendaDay'
				},
			  buttonText: {
					prev: 'Previous',
					next: 'Next '
				},
				editable: true,
				refetchEvents :'refetchEvents',
				selectable: true,
				selectHelper: true,
				dayClick: function(date, allDay, jsEvent, view) {
				var nDate=$.fullCalendar.formatDate( date, 'd' );
				var dDate=$.fullCalendar.formatDate( date, 'dddd ' );
				var fullDate=$.fullCalendar.formatDate( date, ' MMMM , yyyy' );
				$('#calendar .fc-header-title h2').html('<div class="dateBox"><div class="nD">'+nDate+'</div><div class="dD">'+dDate+'<div class="fullD">'+fullDate+'</div><div></div><div class="clear"></div>');
				},
				events: [
					{
						title: 'Project-1(Resources R1)',
						start: new Date(y, m, 1)
					},
					{
						title: 'Project-2(Resources R2)',
						start: new Date(y, m, d-5),
						end: new Date(y, m, d-2)
					},
					
					
					{
						title: 'Meeting For Project 3(Planning resources-3)',
						start: new Date(y, m, d, 10, 30),
						allDay: false
					},
					{
						title: 'Project-1 Submission(Releasing Resouces of Project-1)',
						start: new Date(y, m, d, 12, 0),
						end: new Date(y, m, d, 14, 0),
						allDay: false
					},
					{
						title: 'Project-3(Resouces R3)',
						start: new Date(y, m, d+1, 19, 0),
						end: new Date(y, m, d+1, 22, 30),
						allDay: false
					}
				]
			});  
		}); 
		</script>
        
        
        
        <script type="text/javascript">
            $(document).ready(function(){
                // Smart Wizard     	
                $('#wizardvalidate').smartWizard({transitionEffect:'slideleft',onLeaveStep:leaveAStepCallback,onFinish:onFinishCallback,enableFinishButton:true});
        
              function leaveAStepCallback(obj){
					var step_num= obj.attr('rel');
					return validateSteps(step_num);
              }
              
              function onFinishCallback(){
				   if(validateAllSteps()){
						 $('form').submit();
				   }
              }
                    
           });
               
            function validateAllSteps(){
               var isStepValid = true;
               if(validateStep1() == false){
                 isStepValid = false;
                 $('#wizardvalidate').smartWizard('setError',{stepnum:1,iserror:true});         
               }else{
                 $('#wizardvalidate').smartWizard('setError',{stepnum:1,iserror:false});
               }
           		// add more if you want to validateStep 2
               if(validateStep2() == false){
                 isStepValid = false;
                 $('#wizardvalidate').smartWizard('setError',{stepnum:3,iserror:true});         
               }else{
                 $('#wizardvalidate').smartWizard('setError',{stepnum:3,iserror:false});
               }
               
               if(validateStep3() == false){
                 isStepValid = false;
                 $('#wizardvalidate').smartWizard('setError',{stepnum:3,iserror:true});         
               }else{
                 $('#wizardvalidate').smartWizard('setError',{stepnum:3,iserror:false});
               }
               
               if(!isStepValid){
                  $('#wizardvalidate').smartWizard('showMessage','Please correct the errors in the steps and continue');
               }
                      
               return isStepValid;
            } 	
                
                
                function validateSteps(step){
                  var isStepValid = true;
					  // validate step 1
					  if(step == 1){
							if(validateStep1() == false ){
							  isStepValid = false; 
							  $('#wizardvalidate').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
							  $('#wizardvalidate').smartWizard('setError',{stepnum:step,iserror:true});         
							}else{
							  $('#wizardvalidate').smartWizard('setError',{stepnum:step,iserror:false});
							}
					  }
        
					  // validate step 2
					  if(step == 2){
							if(validateStep2() == false ){
							  isStepValid = false; 
							  $('#wizardvalidate').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
							  $('#wizardvalidate').smartWizard('setError',{stepnum:step,iserror:true});         
							}else{
							  $('#wizardvalidate').smartWizard('setError',{stepnum:step,iserror:false});
							}
					  }
					  
					  // validate step3
					  if(step == 3){
							if(validateStep3() == false ){
							  isStepValid = false; 
							  $('#wizardvalidate').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
							  $('#wizardvalidate').smartWizard('setError',{stepnum:step,iserror:true});         
							}else{
							  $('#wizardvalidate').smartWizard('setError',{stepnum:step,iserror:false});
							}
					  }
              
              return isStepValid;
            }
                
                function validateStep1(){
               var isValid = true; 
               
               // validate password
               var pw = $('#password').val();
               if(!pw && pw.length <= 0){
                 isValid = false;
                 $('#msg_password').html('Please fill password').show();         
               }else{
                 $('#msg_password').html('').hide();
               }
               
               // validate confirm password
               var cpw = $('#cpassword').val();
               if(!cpw && cpw.length <= 0){
                 isValid = false;
                 $('#msg_cpassword').html('Please fill confirm password').show();         
               }else{
                 $('#msg_cpassword').html('').hide();
               }  
               
               // validate password match
               if(pw && pw.length > 0 && cpw && cpw.length > 0){
                 if(pw != cpw){
                   isValid = false;
                   $('#msg_cpassword').html('Password mismatch').show();            
                 }else{
                   $('#msg_cpassword').html('').hide();
                 }
               }
               
               // Validate Username
               var un = $('#username').val();
               if(!un && un.length <= 0){
                 isValid = false;
                 $('#msg_username').html('Please fill username').show();
               }else{
                $('#msg_username').html('').hide(); 
               }
              return isValid;
            }
        
        
            function validateStep2(){
              var isValid = true;    
               // Validate firstname
               var fname = $('#firstname').val();
               if(!fname && fname.length <= 0){
                 isValid = false;
                 $('#msg_firstname').html('Please fill firstname').show();
               }else{
                 $('#msg_username').html('').hide();
               }
               
               // validate lastname
               var lname = $('#lastname').val();
               if(!lname && lname.length <= 0){
                 isValid = false;
                 $('#msg_lastname').html('Please fill lastname').show();         
               }else{
                 $('#msg_lastname').html('').hide();
               }
        
               // validate gender
               var gender = $('#gender').val();
               if(!gender && gender.length <= 0){
                 isValid = false;
                 $('#msg_gender').html('Please choose gender').show();         
               }else{
                 $('#msg_gender').html('').hide();
               }
        
                return isValid;
            }
            
            function validateStep3(){
              var isValid = true;    
              //validate email  email
              var email = $('#email').val();
               if(email && email.length > 0){
                 if(!isValidEmailAddress(email)){
                   isValid = false;
                   $('#msg_email').html('Email is invalid').show();           
                 }else{
                  $('#msg_email').html('').hide();
                 }
               }else{
                 isValid = false;
                 $('#msg_email').html('Please enter email').show();
               }       
              return isValid;
            }
            
            // Email Validation
            function isValidEmailAddress(emailAddress) {
              var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
              return pattern.test(emailAddress);
            } 
                
                
        </script>


		</head>        
        <body>        
<div id="header">
<?php

 top_header();//function for calling header in function_admin.php

?>
</div>


<div id="left_menu">
<?php

 left_menu();//function for calling left menu in function_admin.php

?>
 </div>

<div id="content">
<div class="inner">
      
<?php
 top_menu();//function for calling top menu in function_admin.php
?>



<?php
    show_graph_admin();//function for calling dashboard in function_admin.php
?>




<?php
 footer(); //function for calling footer in function_admin.php
?>       

 <script type="text/javascript">
$("#grades_cce").addClass("select");
</script>  
        </body>
      </html>