<?php
require_once('include/session.php');
require_once('include/check.php');
require_once('config/config.php');
require_once('include/userdetail.php');
require_once('include/functions_dashboard.php');
require_once('include/grade_functions.php');


logged_in();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <title>School Management System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="grades_cce/images/favicon.ico"/> 

        <!-- CSS Stylesheet-->
        <link type="text/css" rel="stylesheet" href="grades_cce/components/bootstrap/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="grades_cce/components/bootstrap/bootstrap-responsive.css" />
        <link type="text/css" rel="stylesheet" href="grades_cce/css/zice.style.css"/>

		
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->  
		<script type="text/javascript" src="grades_cce/grades_cce/js/cce.js"></script>
        <script type="text/javascript" src="grades_cce/js/jquery.min.js"></script>
        <script type="text/javascript" src="grades_cce/components/ui/jquery.ui.min.js"></script> 
	
        <script type="text/javascript" src="grades_cce/components/ui/timepicker.js"></script>
        <script type="text/javascript" src="grades_cce/components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="grades_cce/components/form/form.js"></script>
 
        <script type="text/javascript" src="grades_cce/components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="grades_cce/components/fancybox/jquery.fancybox.js"></script>
       
        <script type="text/javascript" src="grades_cce/components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="grades_cce/components/chosen/chosen.js"></script>
        <script type="text/javascript" src="grades_cce/components/validationEngine/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="grades_cce/components/validationEngine/jquery.validationEngine-en.js"></script>
      
        <script type="text/javascript" src="grades_cce/components/uploadify/uploadify.js"></script>       
		<script type="text/javascript" src="grades_cce/components/Jcrop/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="grades_cce/components/smartWizard/jquery.smartWizard.min.js"></script>
      
        <script type="text/javascript" src="grades_cce/js/zice.custom.js"></script>
		 

	


		</head>        
        <body>        
<div id="header">
<?php

 top_header();//function for calling header in function_admin.php

?>
</div>


<div id="left_menu">
<?php

 left_menu();//function for calling left menu in function_admin.php

?>
 </div>

<div id="content">
<div class="inner">
      
<?php
 top_menu();//function for calling top menu in function_admin.php
?>



<?php



  view_report_coscho();//function for calling dashboard in function_admin.php from grade_cce/grade_cce.php
  
  
  
  
  
  
 // show_students_for_grades();//function for calling dashboard in function_admin.php
 // students_co_scholastic_report();//function for calling dashboard in function_admin.php from grade_cce/grade_cce.php
?>




<?php
 footer(); //function for calling footer in function_admin.php
?>       

 <script type="text/javascript">
$("#grades_cce").addClass("select");
</script>  
        </body>
      </html>