<?php
require_once('include/functions_dashboard.php');
require_once('config/config.php');
require_once('include/session.php');
require_once('include/check.php');
require_once('include/fee_functions.php');

logged_in();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <title>School Management System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon.ico"/> 

        <!-- CSS Stylesheet-->
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap-responsive.css" />
        <link type="text/css" rel="stylesheet" href="css/zice.style.css"/>

		
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->  
      <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.ui.min.js"></script> 
		<script type="text/javascript" src="components/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="components/ui/timepicker.js"></script>
        <script type="text/javascript" src="components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="components/form/form.js"></script>
        <script type="text/javascript" src="components/elfinder/js/elfinder.full.js"></script>
        <script type="text/javascript" src="components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="components/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="components/jscrollpane/jscrollpane.min.js"></script>
        <script type="text/javascript" src="components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="components/chosen/chosen.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="components/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="components/flot/flot.js"></script>
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>       
		<script type="text/javascript" src="components/Jcrop/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="components/smartWizard/jquery.smartWizard.min.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/zice.custom.js"></script>
		<script type="text/javascript" src="fee/js/fee.js"></script>
        
        	<script type="text/javascript">
            $(document).ready(function(){
                // Smart Wizard     	
                $('#wizardvalidate').smartWizard({transitionEffect:'slideleft',onLeaveStep:leaveAStepCallback,onFinish:onFinishCallback,enableFinishButton:true});
        
              function leaveAStepCallback(obj){
					var step_num= obj.attr('rel');
					return validateSteps(step_num);
              }
              
              function onFinishCallback(){
				  $('#form1').submit();
				   if(validateAllSteps()){
						 $('#form1').submit();
				   }
              }
                    
           });
               
            function validateAllSteps(){
               var isStepValid = true;
               if(validateStep1() == false){
                 isStepValid = false;
                 $('#wizardvalidate').smartWizard('setError',{stepnum:1,iserror:true});         
               }else{
                 $('#wizardvalidate').smartWizard('setError',{stepnum:1,iserror:false});
               }
           		// add more if you want to validateStep 2
               if(validateStep2() == false){
                 isStepValid = false;
                 $('#wizardvalidate').smartWizard('setError',{stepnum:3,iserror:true});         
               }else{
                 $('#wizardvalidate').smartWizard('setError',{stepnum:3,iserror:false});
               }
               
               if(validateStep3() == false){
                 isStepValid = false;
                 $('#wizardvalidate').smartWizard('setError',{stepnum:3,iserror:true});         
               }else{
                 $('#wizardvalidate').smartWizard('setError',{stepnum:3,iserror:false});
               }
               
               if(!isStepValid){
                  $('#wizardvalidate').smartWizard('showMessage','Please correct the errors in the steps and continue');
               }
                      
               return isStepValid;
            } 	
                
                
                function validateSteps(step){
                  var isStepValid = true;
					  // validate step 1
					  if(step == 1){
							
					  }
        
					  // validate step 2
					  if(step == 2){
							
					  }
					  
					  // validate step3
					  if(step == 3){
							
					  }
              
              return isStepValid;
            }
                
               function validateStep1(){
				  var isvalid=true;
				  var dur = $('#select_w_box_chzn').val()
				  alert(dur);
				  if(dur == 0 || dur =='')
				  {
					  isvalid = false ;
					  alert('select duration');
				  }
				 
              return true;
            }
             
             function validateStep2(){
              return true;
            }
			
			function validateStep3(){
				
              return true;
            }
			
			
			
            
            // Email Validation
            function isValidEmailAddress(emailAddress) {
              var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
              return pattern.test(emailAddress);
            } 
        </script>
		

		</head>        
        <body>        
<div id="header">
<?php

 top_header();//function for calling header in function_admin.php

?>
</div>


<div id="left_menu">
<?php

 left_menu();//function for calling left menu in function_admin.php

?>
 </div>

<div id="content" >
<div class="inner">
      
<?php
 top_menu();//function for calling top menu in function_admin.php
?>



<?php
     fee_details();//function for calling dashboard in attendance_function.php
?>




<?php
 footer(); //function for calling footer in function_admin.php
?>       
<script type="text/javascript">
$("#fee").addClass("select");
</script>
</body>
      </html>