<?php
require_once('include/functions_dashboard.php');
require_once('config/config.php');
require_once('include/session.php');
require_once('include/check.php');

require_once('include/security_functions.php');



logged_in();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <title>School Management System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon.ico"/> 

        <!-- CSS Stylesheet-->
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap-responsive.css" />
        <link type="text/css" rel="stylesheet" href="css/zice.style.css"/>

		
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->  
		
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.ui.min.js"></script> 
        
        <script type="text/javascript" src="components/form/form.js"></script>
      
        <script type="text/javascript" src="components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="components/fancybox/jquery.fancybox.js"></script>
       
        <script type="text/javascript" src="components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="components/chosen/chosen.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js"></script>
       
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>       
		<script type="text/javascript" src="components/Jcrop/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="components/smartWizard/jquery.smartWizard.min.js"></script>

       
      
<script type="text/javascript">
    
  webcam.set_api_url( 'security/camTest/test.php' );
  webcam.set_quality( 90 ); // JPEG quality (1 - 100)
  webcam.set_shutter_sound( true ); // play shutter click sound
  webcam.set_hook( 'onComplete', 'my_completion_handler' );

  function take_snapshot()
  {
   // take snapshot and upload to server
   document.getElementById('upload_results').innerHTML =
    '<h1>Uploading...</h1>';
   webcam.snap();
  }

  function my_completion_handler(msg)
  {
      //alert(msg);
   // extract URL out of PHP output
   if (msg.match(/(http\:\/\/\S+)/))
   {
    // show JPEG image in page
    document.getElementById('upload_results').innerHTML =
     '<h1>Upload Successful!</h1>';
    // reset camera for another shot
    webcam.reset();
   }
   else
   {
       document.getElementById('upload_results').innerHTML =
     '<h1>Upload Successful!</h1>';
     
     document.getElementById('image_field').value = msg;
     
    alert("Image Uploaded Successfully!!!")
    webcam.reset();
    }
  }
  
// Form validationEngine
$(document).ready(function(){	
	// Main menu 
	$('ul#main_menu ul').hide();
	$('ul#main_menu li ').hover( function () {
		  var parent = $(this).parents('ul').attr('id');
		   var parents = $(this).find('ul');
		  $('#' + parent + ' ul:visible').hide();
		   $(parents).show();
		  $('#' + parent + ' ul:visible li:first').append('<div class="arr"><span></span></div>');
		  $('#' + parent + ' ul:visible ').live({  mouseleave: function(){  $(this).hide(); } });
	});
	// Fancybox 
	$(".pop_box").fancybox({ 'showCloseButton': false, 'hideOnOverlayClick'	:	false });	
	$(".pop_img").fancybox({  'showCloseButton': true,'centerOnScroll' : true, 'overlayOpacity' : 0.8,'padding' : 0 });
	$('.albumImage').dblclick(function(){
		  $("a[rel=glr]").fancybox({  'showCloseButton': true,'centerOnScroll' : true, 'overlayOpacity' : 0.8,'padding' : 0 });
		  $(this).find('a').trigger('click');
	})
	// NewsUpdate
	$('#news_update').vTicker({ 
		speed: 1000,
		pause: 5000,
		animation: 'fade',
		mousePause: true,
		showItems: 3
	});
	// Tabs
	$("ul.tabs li").fadeIn(400); 
	$("ul.tabs li:first").addClass("active").fadeIn(400); 
	$(".tab_content:first").fadeIn(); 
	$("ul.tabs li").live('click',function() {
		  $("ul.tabs li").removeClass("active");						   
		  $(this).addClass("active");  
		  var activeTab = $(this).find("a").attr("href"); 
		  $('.tab_content').fadeOut();		
		  $(activeTab).delay(400).fadeIn();		
		  ResetForm();
		  return false;
	});
  //DataTable
  $('#dataTable').dataTable( {
	  "sDom": "<'row-fluid tb-head'<'span6'f><'span6'<'pull-right'l>>r>t<'row-fluid tb-foot'<'span4'i><'span8'p>>",
	  "bJQueryUI": false,
	  "iDisplayLength": 10,
	  "sPaginationType": "bootstrap",
	  "oLanguage": {
		  "sLengthMenu": "_MENU_",
		  "sSearch": "Search"
	  }
  });
	$('.data_table3').dataTable({
	  "sDom": "<'row-fluid tb-head'<'span6'f><'span6'<'pull-right'Cl>>r>t<'row-fluid tb-foot'<'span4'i><'span8'p>>",
	  "bJQueryUI": false,
	  "iDisplayLength": 10,
	  "sPaginationType": "bootstrap",
	  "oLanguage": {
		  "sLengthMenu": "_MENU_",
		  "sSearch": "Search"
	  }
	});
	
	// WYSIWYG Editor
	$("#editor,#editor2").cleditor();	
	// Form validationEngine
	$('form#validation').validationEngine();		
	$('form#validation_demo').validationEngine();	
	// Input filter
	$('.numericonly input').autotab_magic().autotab_filter('numeric');
	$('.textonly input').autotab_magic().autotab_filter('text');
	$('.alphaonly input').autotab_magic().autotab_filter('alpha');
	$('.regexonly input').autotab_magic().autotab_filter({ format: 'custom', pattern: '[^0-9\.]' });
	$('.alluppercase input').autotab_magic().autotab_filter({ format: 'alphanumeric', uppercase: true });
	// Tags Input
	$('#tags_input').tagsInput({width:'auto'});
});	

</script>
		
	

		</head>        
        <body>        
<div id="header">
<?php

 top_header();//function for calling header in function_admin.php

?>
</div>


<div id="left_menu">
<?php

 left_menu();//function for calling left menu in function_admin.php

?>
 </div>

<div id="content" >
<div class="inner">
      
<?php
 top_menu();//function for calling top menu in function_admin.php
?>



<?php
         security_visitor();	//This is to generate the dashboard depending on the security privilege; filename=>include/security_function
?>




<?php
 footer(); //function for calling footer in function_admin.php
?>       

<script type="text/javascript">
$("#security").addClass("select");
</script>
        
        </body>
        </html>