<?php

require_once('../include/functions_dashboard.php');
require_once('../config/config.php');
require_once('../include/session.php');
require_once('../include/check.php');
require_once('../include/userdetail.php');
require_once('../include/transport_functions.php');


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Paweł 'kilab' Balicki - kilab.pl" />
<title>BookCrust Admin panel</title>
<link rel="stylesheet" type="text/css" href="../css/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../css/navi.css" media="screen" />
<script type="text/javascript" src="../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../library/js/retrieve_book_dashboard.js"></script>
<script type="text/javascript" src="../library/js/button_approve.js"></script>
<script type="text/javascript"  src="../library/js/class_for_issue.js"></script>
<script type="text/javascript"  src="../library/js/student_details.js"></script>

<script type="text/javascript">
$(function(){
	$(".box .h_title").not(this).next("ul").hide("normal");
	$(".box .h_title").not(this).next("#home").show("normal");
	$(".box").children(".h_title").click( function() { $(this).next("ul").slideToggle(); });
});
</script>
</head>
<body>
<div class="wrap">
	<?php
      top_menu_header();		//This function is used to generate the top menu ; filename => include/function_menu.php
    ?>
    
    <div id="content">
    <?php
    left_menu();// This function is used to generate the left menu ; filename=> include/function_menu.php
    ?>
    </div>
   
    <div id="main">
    <?php
	
	transport_student_view();	//This is to generate the dashboard depending on the user privilege; filename=>include/transport_functions.php
	
	?>
    </div>
  
    <?php
    footer();			//This function will generate the footer ; filename => include/function_menu.php
    ?>
</div>

</body>
</html>
