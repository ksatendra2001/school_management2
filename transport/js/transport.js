// JavaScript Document
// 
function allocate_transport_admission(val)
{
	
var xmlhttp;
if(val == "")

{
document.getElementById("generate_allocate").innerHTML = "";
return;	
}


if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("generate_allocate").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","transport/allocate_transport_admission.php?data="+val,true);
xmlhttp.send();


}
//function to get destination on the selected route
function get_destination()
{
val = $("#select_route").val();	

route=$("#select_route option:selected").attr("id");
//get destinations on that main roite

$.ajax({
	type:'GET',
	url:"transport/show_destination.php",
	data:{route:route,route_id:val},
	success: function(data){
	document.getElementById("destination").innerHTML=data;
	}
});	
	
}
//function to get destination on the selected route
function get_destinations()
{
val = $("#select_route").val();	

route=$("#select_route option:selected").attr("id");
//get destinations on that main roite

$.ajax({
	type:'GET',
	url:"transport/show_destinations.php",
	data:{route:route,route_id:val},
	success: function(data){
	document.getElementById("destinations").innerHTML=data;
	}
});	
	
}

//function to get destination on the selected route
function get_edit_destination()
{
val = $("#select_route").val();	

route=$("#select_route option:selected").attr("id");
//get destinations on that main roite

$.ajax({
	type:'GET',
	url:"transport/show_edit_destination.php",
	data:{route:route,route_id:val},
	success: function(data){
	document.getElementById("destination_edit").innerHTML=data;
	}
});	
	
}




//function to show fare
function show_fare()
{
 
value=$("#sel_dest").val();


$.ajax({
	type:'GET',
	url:"transport/show_fare.php",
	data:{route:value},
	success: function(data){
	document.getElementById("fare").value=data;
	}
});		
	
}

function set_main_route()
{


if(document.getElementById("set_main").checked==true)	
{
	
document.getElementById("m_route").style.display="none";	
destination=$("#destination").val();
cost=$("#cost").val();

$.ajax({
	type:'GET',
	url:"transport/add_routes.php",
	data:{dest:destination,cost:cost,main_route:""},
	success: function(data){
			if(data == 0)
		{
		window.location.href="admin_transport_set_routes.php?error";	
			
		}
		else
		{
		window.location.href="admin_transport_set_routes.php?done";
		}
	}
});

}	
else
{
	
document.getElementById("m_route").style.display="block";	
destination=$("#destination").val();
cost=$("#cost").val();
main_route=$("#main_route").val();
$.ajax({
	type:'GET',
	url:"transport/add_routes.php",
	data:{dest:destination,cost:cost,main_route:main_route},
	success: function(data){
		if(data == 0)
		{
		window.location.href="admin_transport_set_routes.php?error";	
			
		}
		else
		{
		window.location.href="admin_transport_set_routes.php?done";
		}
	}
});	
}
	
}
function show_checkbox()
{

if(document.getElementById("set_main").checked==false)
{
	
document.getElementById("m_route").style.display="block";	

}
else
{
document.getElementById("m_route").style.display="none";		
}
	
}
function text_editable_transport(id,vehicle_type,route_id,license_no,no_of_trips,row_id)
{
	//This function will convert the table into editable text boxes
	var element = document.getElementById(row_id);
	element.innerHTML = '<td>'+row_id+'</td><td ><input id="'+row_id+'vehicle_type" type="text" value="'+vehicle_type+'" style="width:130px" /></td><td><input id="'+row_id+'route_id" type="text" value="'+route_id+'"  style="width:100px" /></td><td><input  id="'+row_id+'license_no"  type="text" value="'+license_no+'"  style="width:150px" /></td><td><input id="'+row_id+'no_of_trips" type="text" value="'+no_of_trips+'"  style="width:70px" /></td><td><img src="images/icon/color_18/checkmark2.png" onclick="vehicle_editing('+row_id+','+id+');" /></a></td>';

}

function vehicle_editing(row_id,id)
{
	var vehicle_type_id = row_id+"vehicle_type";
	var vehicle_type_value = document.getElementById(vehicle_type_id).value;
	
	var route_id_id = row_id+"route_id";
	var route_id_value = document.getElementById(route_id_id).value;
	
	var vehicle_license_id = row_id+"license_no";
	var vehicle_license_value = document.getElementById(vehicle_license_id).value;
	
	var no_of_trips_id = row_id+"no_of_trips";
	var no_of_trips_value = document.getElementById(no_of_trips_id).value;
	
	var url = "vehicle_type="+vehicle_type_value+"&route_id="+route_id_value+"&vehicle_license="+vehicle_license_value+"&no_of_trips="+no_of_trips_value+"&id="+id;
	
	
	window.location.href = "transport/vehicle_editing.php?"+url;	
}

function validate()
{

	if(  add_transport.vehicle_type.value=='' || add_transport.route_id.value=='' || add_transport.license_no.value=='' || add_transport.no_trips.value=='' )
  
    {
	alert('You Have Added Incomplete Vehicle Details!!')
	return true;
	}	
}



function show_data_month()
{     
		year = $("#year").val();
		month = $("#month").val();
		
		$.ajax({
		type: 'GET',
		url: 'transport/show_data_month.php',
		data: {year:year,month:month},
		success: function(data) {
				$('#data_month').html(data);
		}
		});
	
}




function edit_transport_route(rid,dest,cost,destn)
{     
$('#cost_'+rid+'_'+dest).html("<input type='text' value='"+cost+"' id='new_cost'>");
$('#dest_'+rid+'_'+dest).html("<input type='text' value='"+destn+"' id='new_destn'>");
$('#edit_'+rid+'_'+dest).html("<input type='button' onclick='save_destination("+rid+",\""+dest+"\","+cost+", \""+destn+"\")' value='SAVE'>");
}




function save_destination(rid,destt,cost,destn)
{
	var new_cost = $("#new_cost").val();
	var new_destn = $("#new_destn").val();
		$.ajax({
		type: 'GET',
		url: 'transport/edit_transport_route.php',
		data: {rid:rid,dest:new_destn,cost:new_cost,destn:destn},
		success: function(data) {
				$('#cost_'+rid+'_'+destt).html("<input type='text' value='"+new_cost+"' disabled>");
				$('#dest_'+rid+'_'+destt).html("<input type='text' value='"+new_destn+"' disabled>");
if(data=='1')
		$('#edit_'+rid+'_'+destt).html("<label style='color:green;'>Saved</label>");

else if(data=='0')
		$('#edit_'+rid+'_'+destt).html("<label style='color:red;'>Not Saved</label>");

		}
		});
	
}





function delete_transport_route(rid,dest,cost,destn)
{
$('#cost_'+rid+'_'+dest).html("<input type='text' value='"+cost+"' id='new_cost' disabled>");
$('#dest_'+rid+'_'+dest).html("<input type='text' value='"+destn+"' id='new_destn' disabled>");
$('#edit_'+rid+'_'+dest).html("<input type='button' onclick='delete_destination("+rid+",\""+dest+"\","+cost+", \""+destn+"\")' value='CONFIRM ?'>");
}





function delete_destination(rid,destt,cost,destn)
{
	var new_cost = $("#new_cost").val();
	var new_destn = $("#new_destn").val();

	$.ajax({
		type: 'GET',
		url: 'transport/delete_transport_route.php',
		data: {rid:rid,destn:destn},
		success: function(data) {
				$('#cost_'+rid+'_'+destt).html("<input type='text' value='"+new_cost+"' disabled>");
				$('#dest_'+rid+'_'+destt).html("<input type='text' value='"+new_destn+"' disabled>");
if(data=='1')
		$('#edit_'+rid+'_'+destt).html("<label style='color:green;'>Deleted</label>");

else if(data=='0')
		$('#edit_'+rid+'_'+destt).html("<label style='color:red;'>Not Deleted</label>");

		}
		});
	
}






