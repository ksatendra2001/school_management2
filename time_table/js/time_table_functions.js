function delete_class_time_table(cid)
{
	var message = window.confirm("Are You Sure to Delete (Time Table)?");
	if(message == true)
	{
             window.location.href ="time_table/delete_class_time_table.php?cid="+cid;
	}
        else
        {
             return false;	
        }
}
// JavaScript Document
function generate_time_table(cid)
{
    
    $.ajax({
        type:'GET',
        url:'time_table/generate_time_table.php',
        
        data:{cid:cid},
        success: function(data)
        {
           $('#cl_ti_ta_div').empty();
            $('#cl_ti_ta_div').append(data);
           // alert(data);
        }
    })
}
function on_change_time_slot()
{
	
	//This function is called on an onchange event of a time slot
	//This function will generate day list for the user
	
	$('#time_table_container').append('<div class="section"><label>Day</label><div><select id="day" name="day" onchange="on_change_day();"><option value="NULL">Day</option><option value="0">Monday</option><option value="1">Tuesday</option><option value="2">Wednesday</option><option value="3">Thursday</option><option value="4">Friday</option><option value="5">Saturday</option></select></div></div>');
}

function on_change_day()
{
	//This function is called when day is changed
	//This function will populate a select box in which we have to select the subjects taught in the class, the subjects are taken from the teachers table in which a mapping is done of the teachers and the subjects taught in a class.
	time_slot=$('#time_slot').val();
	
	day=$('#day').val();
	
	class_id = $('#class_id').val();
	
	$.ajax({
		type: 'GET',
		url: 'time_table/get_subjects.php',
		data: {time_slot:time_slot, day:day, class_id:class_id},
		success: function(data) {
				$('#time_table_container').append(data);
		}
		});
}

function on_change_subject()
{
	//This function is called when a subject value is changed
	//This function will populate the teachers associated with the selected subject and the class, the values are taken from the teachers table
	
	time_slot=$('#time_slot').val();
	
	day=$('#day').val();
	
	class_id = $('#class_id').val();
	
	subject_id = $('#subject').val();
	
	$.ajax({
		type: 'GET',
		url: 'time_table/get_teacher.php',
		data: {time_slot:time_slot, day:day, class_id:class_id, subject_id:subject_id},
		success: function(data) {
				$('#time_table_container').append(data);
		}
		});
}

function on_change_teacher()
{
	//This function is called when teacher value is changed
	//Now we have to introduce a submit button and the submit buton will submit all the values in the database
	
	$('#time_table_container').append('<div class="section"><label><button type="button" class="add" onClick="submit_time_table_details();">ADD</button></label><div class="section"><button type="button" class="cancel">Cancel</button></div></div>');
}

function submit_time_table_details()
{
	//This function will submit all the details and insert the values in the database
	
	time_slot=$('#time_slot').val();
	
	day=$('#day').val();
	
	class_id = $('#class_id').val();
	
	subject_id = $('#subject').val();
	
	teacher_id = $('#teachers').val();
	
	$.ajax({
		type: 'GET',
		url: 'time_table/add_value_time_table.php',
		data: {time_slot:time_slot, day:day, class_id:class_id, subject_id:subject_id, teacher_id:teacher_id},
		success: function(data) {
				window.location.href = "admin_make_entry.php?class_id="+class_id;
				//$('#time_table_container').append('<tr>'+data+'</tr>');
		}
		});
	
}
function edit_time_no_sub_tea(day,cid,id)
{
	

	$.ajax({
		type: 'GET',
		url: 'time_table/new_addition_time.php',
		data: {class_id:cid,id:id,day:day},
		success: function(data) {
			
					$("#val_"+day+"_"+id).replaceWith(data);
		}
		});
/*document.getElementById("val_"+day+"_"+id).innerHTML ='<input type="text" value="'+subject+'" style="width:70px" id="edit_sub_'+day+'_'+id+'"/><br><input type="text" value="'+teacher+'" style="width:70px" id="edit_tea_'+day+'_'+id+'"/><br><img src="images/icon/color_18/checkmark2.png" onclick="save_time('+day+','+id+','+time_id+','+cid+','+tid+');"/>';*/

}
function edit_time(day,cid,id,tid,sid,time_id)
{
	

	$.ajax({
		type: 'GET',
		url: 'time_table/subject_for_edit.php',
		data: {class_id:cid,tid:tid,sid:sid,time_id:time_id,id:id,day:day},
		success: function(data) {
			
					$("#val_"+day+"_"+id).replaceWith(data);
		}
		});
/*document.getElementById("val_"+day+"_"+id).innerHTML ='<input type="text" value="'+subject+'" style="width:70px" id="edit_sub_'+day+'_'+id+'"/><br><input type="text" value="'+teacher+'" style="width:70px" id="edit_tea_'+day+'_'+id+'"/><br><img src="images/icon/color_18/checkmark2.png" onclick="save_time('+day+','+id+','+time_id+','+cid+','+tid+');"/>';*/

}
function save_time(time_id,cid)
{
	
var tea_id = $("#teacher_edit").val();	
var sub_id = $("#subject_edit").val();


	
	$.ajax({
		type: 'GET',
		url: 'time_table/edit_time_table.php',
		data: {teacher_id:tea_id,subject_id:sub_id,time:time_id,clas_id:cid},
		success: function(data) {
			
					window.location.href = "admin_view_time.php?class_id="+cid;
		}
		});


}
function delete_time(time_id,cid)
{

var tea_id = $("#teacher_edit").val();	
var sub_id = $("#subject_edit").val();


	
	$.ajax({
		type: 'GET',
		url: 'time_table/delete_time_table.php',
		data: {teacher_id:tea_id,subject_id:sub_id,time:time_id,clas_id:cid},
		success: function(data) {
			
					window.location.href = "admin_view_time.php?class_id="+cid;
		}
		});


}
function save_time_new(time_s_id,cid,day)
{
	
var tea_id = $("#teacher_edit").val();	
var sub_id = $("#subject_edit").val();
//for inserting if no time id available

$.ajax({
		type: 'GET',
		url: 'time_table/add_value_time_table.php',
		data: {teacher_id:tea_id,subject_id:sub_id,time_s_id:time_s_id,class_id:cid,day:day},
		success: function(data) {
			
					window.location.href = "admin_view_time.php?class_id="+cid;
		}
		});
	

}