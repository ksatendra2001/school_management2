<?php
require_once('../config/config.php');
require_once('../include/session.php');
require_once('../include/userdetail.php');
require_once('../include/check.php');


$time_slot = $_GET['time_slot'];
$day = $_GET['day'];
$class_id = $_GET['class_id'];

//First check whether there is any entry in the table for the class_id, day and time slot, if entry is found then show the details of the subject and the teacher, else generate a select box containing subjects taking details from the teachers table

//Query to get details from the time_table table
$query_time_table = "
SELECT * 
FROM `time_table`
WHERE `time_slot_id` = '$time_slot' AND `day` = '$day' AND `class_id` = '$class_id'
";

$execute_time_table = mysql_query($query_time_table);

$time_table = mysql_fetch_array($execute_time_table);

if(mysql_num_rows($execute_time_table) == 0)
{
	//No Entry is made, generate a select box of the subjects available for that class
	$query_subjects = "
		SELECT subject.subject, subject.subject_id
		FROM subject
		
		INNER JOIN teachers
		ON teachers.subject_id = subject.subject_id
		
		INNER JOIN class_index
		ON class_index.class_name = teachers.class
		
		WHERE class_index.cId = $class_id
	";
	$execute_subjects = mysql_query($query_subjects);
	
	if(mysql_num_rows($execute_subjects) == 0)
	{
		//No relationship of teachers and subjects in defined in the teachers table
		echo '
			  <div class="section">
				<label>No Subject-Teacher Relationship defined for this class, please go to Add subject-teacher relationship in the teachers section and make changes</label>
				</div>
			
		';
	}
	else
	{
	
		  echo '
		       <div class="section">
			  <label>Subjects</label>
			  <div><select name="subject" id="subject" onchange="on_change_subject();">
				  <option value="NULL">Select</option>
			
				  
			  ';
			  
			  while($subjects = mysql_fetch_array($execute_subjects))
			  {
				  echo '<option value="'.$subjects['subject_id'].'">'.$subjects['subject'].'</option>';
			  }
		  
		  echo'	
		  </select>
			 </div>
			 </div>
		 ';
	}

}

else
{
	//If there is an entry for that time slot then populate the table with the details
	
	//Get the subject
	$query_subject = "
		SELECT subject
		FROM subject
		WHERE subject_id = ".$time_table['subject_id']."
	";
	$execute_subject = mysql_query($query_subject);
	$subject = mysql_fetch_array($execute_subject);
	$subject = $subject[0];
	
	//Get the teacher
	$query_teacher = "
		SELECT Name
		FROM users
		WHERE uId = ".$time_table['teacher_id']."
	";
	$execute_teacher = mysql_query($query_teacher);
	$teacher = mysql_fetch_array($execute_teacher);
	$teacher = $teacher[0];
	
	echo '<div class="section">
		
			<label>Subject</label>
			<td>'.$subject.'</td>
		</tr>
		<tr>
			<td><strong>Teacher</strong></td>
			<td>'.$teacher.'</td>
		</tr></div>
	';
	
}


?>