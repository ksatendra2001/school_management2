<?php
function view_time_slots()
{
	$query_get_time_slots = "
		SELECT *
		FROM `time_slots`
	";
	
	$execute_get_time_slots = mysql_query($query_get_time_slots);
	
	echo '	
			<div class="full_w">
				<div class="h_title">View Time slots</div>';
				
				if(isset($_GET['timeSlotAdded']))
				{
					echo '<h2 align="center" style="color:GREEN">Time Slot Added Successfully </h2>';
				}
				
				
	echo'			
				<a href="time_table_add_time_slots.php" ><button type="button" class="add" >Add Time Slot</button></a>
				
									<table >
																<tr>
																	<th width="75px">Sno.</th>
																	<th align="center">Slot Name</th>
																	<th align="center">Slot Start</th>
																	<th align="center">Slot End</th>
																</tr>';
														$sno = 0;	
														while($get_time_slots = mysql_fetch_array($execute_get_time_slots))
														{
															echo '<tr>';
																echo '<td>'.++$sno.'</td>';
																echo '<td>'.$get_time_slots['time_slot_name'].'</td>';
																echo '<td>'.$get_time_slots['time_slot_start'].'</td>';
																echo '<td>'.$get_time_slots['time_slot_end'].'</td>';
															echo '</tr>';
														}
echo'													
										</table>
														 
				
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>';
}

function add_time_slots()
{
	echo '	
			<div class="full_w">
				<div class="h_title">Add Time slots</div>
				<div class="entry">
					<div class="sep"></div>
				</div>
				
									<table >
									<form action="time_table/add_time_slot.php" method="post">
										<tr>
											<td><strong>Time Slot Name</strong></td><td><input type="text" name="time_slot_name" /></td>
										</tr>
										<tr>
											<td><strong>Time Slot Start</strong></td><td><input type="text" name="time_slot_start" /></td>
										</tr>
										<tr>
											<td><strong>Time Slot End</strong></td><td><input type="text" name="time_slot_end" /></td>
										</tr>	
										<tr>
											<td><button type="submit" class="add">ADD</button></td><td><button type="button" class="cancel">Cancel</button></td>
										</tr>
									</form>			
									</table>
														
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>';
}

function view_classes()
{
	$disp='';
$q="
SELECT `cId`,`class_name` 
FROM class_index 
ORDER BY class, section ASC
";

$q_res=mysql_query($q);
echo '<h2 align="center">';
echo 'Kindly select class';
echo '</h2>';
echo "<br>";
echo "<br>";
echo "<br>";
while($res=mysql_fetch_array($q_res))
{
	$disp.='<tr class="first" id="added_rows">
<td>
			
			<a href="time_table_view_time_table.php?class_id='.$res['cId'].'"><span  class="button approve" id="btn">'.$res[1].'</span></a>
			</td>
				
	</tr>';
}


echo $disp;
}


function return_day($day_code)
{
	//This function will return the day corresponding to the day Code passed as an argument
	switch($day_code)
	{
		case 0 : return 'Monday'; break;
		case 1 : return 'Tuesday'; break;
		case 2 : return 'Wednesday'; break;
		case 3 : return 'Thursday'; break;
		case 4 : return 'Friday'; break;
		case 5 : return 'Saturday'; break;		
	}
	
}

function view_time_table()
{
	
	
	
	$class_id = $_GET['class_id'];
	
	$query_get_time_slot = "
		SELECT `time_slot_id`, `time_slot_name`
		FROM `time_slots`
	";
	$execute_get_time_slot = mysql_query($query_get_time_slot);
	
	$day = 0;		//Set to zero as the week starts with Monday
	
	//Now Store all the time_slot ID and Name in an array
	$ctr_time_slot = 0;
	$array_time_slot_id = array();		//Array to store the time_slot_id
	$array_time_slot_name = array();	//Array to store the time_slot_name
	
	while($get_time_slot = mysql_fetch_array($execute_get_time_slot))
	{
		$array_time_slot_id[$ctr_time_slot] = $get_time_slot['time_slot_id'];
		$array_time_slot_name[$ctr_time_slot] = $get_time_slot['time_slot_name'];
		++$ctr_time_slot;
	}
	
	echo '	
			<div class="full_w">
				<div class="h_title">Time table</div>
				<div class="entry">
				<a href="time_table_make_entry.php?class_id='.$class_id.'"><button type="button" class="add">Make an Entry</button></a>
					<div class="sep"></div>
				</div>
				
									<table >
										<tr>
											<th>Day &darr;</th>';
											//Loop to get the time_slot_name in place
											for($i=0;$i<$ctr_time_slot;$i++)
											{
												echo '<th>'.$array_time_slot_name[$i].'</th>';
											}
				echo'							
										</tr>';
								//End of the header part
								
								//Now loop to generate the days and also get the details of the subject and teacher
								
								for($day=0;$day<=5;$day++)
								{
									//Loop will generate days, 0->Monday, 1-> Tuesday etc...
									echo '<tr>';
									
										echo '<td>'.return_day($day).'</td>';
										
							//Loop to get the details from the time_table table for the corresponding day, time_slot_id, class_id
							
										for($ctr = 0;$ctr < $ctr_time_slot; $ctr++)
										{
											$query_get_details_time_table = "
												SELECT subject.subject, users.Name
												FROM subject
												
												INNER JOIN time_table
												ON time_table.subject_id = subject.subject_id
												
												INNER JOIN users
												ON users.uId = time_table.teacher_id
												
												WHERE time_table.class_id = '$class_id' AND time_table.day = '$day' AND time_table.time_slot_id = '$array_time_slot_id[$ctr]' AND time_table.session_id = ".$_SESSION['current_session_id']."
											";
											
											$execute_get_details_time_table = mysql_query($query_get_details_time_table);
											$get_details_time_table = mysql_fetch_array($execute_get_details_time_table);
											$subject = $get_details_time_table[0];
											$teacher = $get_details_time_table[1];
											
											echo "<td align=\"center\"><strong>$subject</strong><br>($teacher)</td>";
											
										}
									
									echo '</tr>';
								}
										
										
				echo'						
									</table>
														
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>';
}

function make_entry()
{
	$class_id = $_GET['class_id'];
	
	$query_get_class = "
		SELECT `class_name`
		FROM class_index
		WHERE `cId` = $class_id
	";
	
	$execute_get_class = mysql_query($query_get_class);
	$get_class = mysql_fetch_array($execute_get_class);
	$class_name = $get_class[0];
	
	//Query to get the time slots
	$query_time_slots = "
		SELECT `time_slot_id`,`time_slot_name`
		FROM time_slots
	";
	$execute_time_slots = mysql_query($query_time_slots);
	
	echo '	
			<div class="full_w">
				<div class="h_title">Make an Entry</div>
				<div class="entry">
					<h2>Class : '.$class_name.'</h2>
					<div class="sep"></div>
					
				</div>
						<table id="time_table_container" style="width:400px">
						<input type="hidden" id="class_id" name="class_id" value="'.$class_id.'" />
							<tr>
								<td><strong>Time Slots</strong></td>
								<td>
								<select id="time_slot" name="time_slot" onchange="on_change_time_slot();" >
								<option>Time Slot</option>
								';
								while($time_slots = mysql_fetch_array($execute_time_slots))
								{
									echo '<option value="'.$time_slots['time_slot_id'].'" >'.$time_slots['time_slot_name'].'</option>';
								}
								
				echo'			
								</select>	
								</td>
							</tr>
						</table>
														
				<div class="entry">
		
				<div id="txtHint"></div>
				<div id="Hint"></div>	
			
			</div>';
}

?>