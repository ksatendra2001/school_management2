//Author By:Manish
//function show delete asset details
	function delete_confirm(id,session_id)
	{ 	
	var r = window.confirm("Are you sure?");
		if(r == true)
		{
		$.ajax({
		type: 'GET',
		url: 'finance/accounts_delete_asset.php',
		data: {id:id,session_id:session_id},
		success: function(data) {
		window.location.href ="accounts_finance_view_asset_details.php?session_id="+session_id;
								}
				});
	}
	else
	{
	return false;	
	}
	}
//function to delete liability
	function delete_liability_confirm(id,session_id)
	{
	var r = window.confirm("Are you sure?");
	if(r == true)
		{
		$.ajax({
		type: 'GET',
		url: 'finance/accounts_delete_liability.php',
		data: {id:id,session_id:session_id},
		success: function(data) {
		window.location.href ="accounts_finance_view_liability.php?session_id="+session_id;
								}
				});
	}
	else
	{
	return false;	
	}
	}
	//function to delete category
	function delete_category(cid,session)
	{
	var r_del = window.confirm("Are you sure?");
	if(r_del == true)
		{
		$.ajax({
		type: 'GET',
		url: 'finance/account_delete_category.php',
		data: {id:cid,session:session},
		success: function(data) {
		window.location.href ="accounts_finance_view_category.php?session="+session ;
								}
				});
		}
	else
	{
	return false;	
	}
	}

	//editing transaction details
	function edit_transactions(category,expenses,income,date,id,from_date,to_date)
	{
	var edit=document.getElementById(id).innerHTML="<td><input type='text' value='"+category+"' id='category"+id+"' style='width:50px'></td><td><input type='text' value='"+expenses+"' id='expenses"+id+"' style='width:50px'></td><td><input type='text' value='"+income+"' id='income"+id+"' style='width:50px'></td><td><input type='text' value='"+date+"' id='date"+id+"' style='width:100px'></td><td id='button"+id+"'><button class='uibutton submit' onclick='change_transaction("+id+",\""+from_date+"\",\""+to_date+"\")'>Save?</button></td>'";
	}
	
	//function to edit transaction
	function change_transaction(id,from_date,to_date)
	{	
	var new_category=document.getElementById("category"+id).value;
	var new_expenses=document.getElementById("expenses"+id).value;
	var new_income=document.getElementById("income"+id).value;
	var new_date=document.getElementById("date"+id).value;
	$.ajax({
	type: 'GET',
	url: 'finance/accounts_update_transactions.php',
	data: {category:new_category,expenses:new_expenses,income:new_income,date:new_date,id:id},
	success: function(data) {
	if(data)
		{
	//$("#button"+id).html(data);
		window.location.href ="accounts_finance_edit_transaction_details.php?from="+from_date+"&to="+to_date;
		}
							}
	});	/*window.location.href="finance/accounts_update_transactions.php?category="+new_category+"&expenses="+new_expenses+"&income="+new_income+"&date="+new_date+"&id="+id;*/
	}
	//function to print donation	
	function print_donation(id)
	{ 
	window.location.href="finance/examples/print_donation.php?id="+id;
	
	/*var divElements = document.getElementById("print_donation").innerHTML;
	//Get the HTML of whole page
	var oldPage = document.body.innerHTML;
	//Reset the page's HTML with div's HTML only
	document.body.innerHTML = 
	"<html><head><title></title></head><body>" + 
	divElements + "</body>";
	//Print Page
	window.print();
	//Restore orignal HTML
	document.body.innerHTML = oldPage;*/
	}
	//editing transaction details
	function edit_measure(measure_name,value,date,id,session_id)
	{
	var edit=document.getElementById(id).innerHTML="<td><input type='text' value='"+measure_name+"' id='measure_name"+id+"' style='width:50px'></td><td><input type='text' value='"+value+"' id='value"+id+"' style='width:50px'></td><td><input type='text' value='"+date+"' id='date"+id+"' style='width:100px'></td><td id='button"+id+"'><button class='uibutton submit' onclick='change_measure("+id+","+session_id+")'>Save?</button></td>'";
	}
	
	//function to edit measure
	function change_measure(id,session_id)
	{
	var new_measure=document.getElementById("measure_name"+id).value;
	var new_value=document.getElementById("value"+id).value;
	var new_date=document.getElementById("date"+id).value;
	$.ajax({
	type: 'GET',
	url: 'finance/accounts_update_measure_details.php',
	data: {measure_name:new_measure,value:new_value,date:new_date,id:id},
	success: function(data) {
	if(data)
		{
	//$("#button"+id).html(data);
		window.location.href ="accounts_finance_view_measure_details.php?session_id="+session_id+"&&id="+id+"";
		}
							}
	});	/*window.location.href="finance/accounts_update_transactions.php?category="+new_category+"&expenses="+new_expenses+"&income="+new_income+"&date="+new_date+"&id="+id;*/
	}
	
	//editing donation details
	function edit_donation(donor_name,description,amount,date,id,session_id)
	{
	
	var edit=document.getElementById(id).innerHTML="<td><input type='text' value='"+donor_name+"' id='donor_name"+id+"' style='width:100px'></td><td><input type='text' value='"+description+"' id='description"+id+"' style='width:100px'></td><td><input type='text' value='"+amount+"' id='amount"+id+"' style='width:100px'></td><td><input type='text' value='"+date+"' id='date"+id+"' style='width:100px'></td><td id='button"+id+"'><button class='uibutton submit' onclick='change_donation("+id+","+session_id+")'>Save?</button></td>'";
	}
	
	//function to edit donation details
	function change_donation(id,session_id)
	{
	var new_donor_name=document.getElementById("donor_name"+id).value;
	var new_description=document.getElementById("description"+id).value;
	var new_amount=document.getElementById("amount"+id).value;
	var new_date=document.getElementById("date"+id).value;
	$.ajax({
	type: 'GET',
	url: 'finance/accounts_update_donation.php',
	data: {donor_name:new_donor_name,description:new_description,amount:new_amount,date:new_date,id:id},
	success: function(data) {
	if(data == 1)
	{
	window.location.href ="accounts_finance_view_donation.php?session="+session_id;
	}
							}
	});
	}
	
	//function to delete donation
	function delete_donation(id,session_id)
	{
	var r_del = window.confirm("Are you sure?");
	if(r_del == true)
		{
		$.ajax({
		type: 'GET',
		url: 'finance/accounts_delete_donation.php',
		data: {id:id,session_id:session_id},
		success: function(data) {
		window.location.href ="accounts_finance_view_donation.php?session="+session_id ;
								}
		});
	}
	else
	{
	return false;	
	}
	}

/*
//function to show month
function show_data_month()
{
		year = $("#year").val();
		month = $("#month").val();
		
		$.ajax({
		type: 'GET',
		url: 'finance/show_data_month.php',
		data: {year:year,month:month},
		success: function(data) {
				$('#data_month').html(data);
		}
		});
	
}
*/
//function to delete deduction
	function delete_deduction_confirm(id,year)
	{
	var r_del = window.confirm("Are you sure?");
	if(r_del == true)
		{
		$.ajax({
		type: 'GET',
		url: 'finance/accounts_delete_deduction_type.php',
		data: {id:id,year:year},
		success: function(data) {
		window.location.href ="accounts_finance_pay_view_deduction_type.php?id="+id+"&&year="+year+"";
								}
			});
		}
	else
	{
	return false;	
	}
	}
	//function show deduction
	function show_deduction_type(uId,year,month)
	{
	$.ajax({
			type: 'GET',
			url: 'finance/show_deduction_type.php',
			data: {uId:uId,year:year,month:month},
			success: function(data) {
			$('#data_deduction').html(data);
									}
			});
	
	}

	//function show deduction
	function show_deduction_amount(id,i,year)
	{
	value=$("#max_val").val();
	document.getElementById("check_"+id).innerHTML="<input type='text' name='amount"+id+"' style='width:100px'>";
	/*	
	$.ajax({
	type: 'GET',
	url: 'finance/show_deduction_amount.php',
	data: {id:id},
	success: function(data) {
	  $('#data_deduction_amount').html(data);
	}
	});*/
	}

	//editing deduction details
	function edit_deduction(did,i,uId,amount,deduction_type,year)
	{
	var edit=document.getElementById("edit_"+i+"_"+uId).innerHTML="<input type='text' value='"+amount+"' id='amount_"+i+"_"+uId+"' style='width:50px' onblur='change_deduction("+did+","+i+","+uId+","+year+")'/>";
	var edit=document.getElementById("edit_ded_"+i+"_"+uId).innerHTML="<input type='text' value='"+deduction_type+"' id='ded_"+i+"_"+uId+"'  style='width:140px' onblur='change_deduction("+did+","+i+","+uId+","+year+")'/>"; 
	}
	  //function to edit deduction details
	function change_deduction(did,i,uId,year)
	{  
	var new_amount=document.getElementById("amount_"+i+"_"+uId).value;
	var new_deduction_type=document.getElementById("ded_"+i+"_"+uId).value;
	$.ajax({
			type: 'GET',
			url: 'finance/accounts_update_deduction_details.php',
			data: {amount_:new_amount,ded:new_deduction_type,uId:uId,did:did,year:year},
			success: function(data) {
			window.location.href ="accounts_finance_pay_view_employee_deduction_details.php?id="+uId+"&&year="+year+"";
									}
			});
	}
	
	//function to approved salary
	function approve_salary(id,year,month,name)
	{
	var r_del = window.confirm("Are you sure for Approved Salary?");
	if(r_del == true)
		{
		$.ajax({
				type: 'GET',
				url: 'finance/accounts_update_employee_deduction.php',
				data: {id:id,year:year,month:month,name:name},
				success: function(data) {
				window.location.href ="accounts_finance_pay_status.php?emp_id="+id+"&&year="+year+"&&month="+month+"&&name="+name+"";
										}
				})	
	}
	else
	{
	return false;	
	}
	
	}
	//function to print the approved salary
	function print_approved_salary(id,year,month,name)
	{  
	window.location.href="finance/examples/print_approved_salary.php?id="+id+"&&year="+year+"&&month="+month+"&&name="+name+"";
	}
	//function to print salary
	function print_salary(id,year,month,name)
	{   
	    
	window.location.href="finance/examples/print_salary.php?id="+id+"&&year="+year+"&&month="+month+"&&name="+name+"";
	
	}
	//function to reject salary
	function reject_salary(id,year,month,name)
	{	
	var r_del = window.confirm("Are you sure for Reject Salary?");
	if(r_del == true)
	{
		$.ajax({
			type: 'GET',
			url: 'finance/accounts_update_reject_employee_deduction.php',
			data: {id:id,year:year,month:month,name:name},
			success: function(data) {
			window.location.href ="accounts_finance_pay_employee_details.php?emp_id="+id+"&&year="+year+"&&month="+month+"&&name="+name+"";
									}
			})	
	}
	else
	{
	return false;	
	}
	
	}
	//function show deduction
	function show_deduction_year(year)
	{     
	$.ajax({
			type: 'GET',
			url: 'finance/accounts_year_deduction_type.php',
			data: {year:year},
			success: function(data) {
			$('#data_deduction_year').html(data);
									}
			});
	}
	
	
	//function to delete deduction
	function delete_deduction_details_confirm(id_ded,id,amount,year)
	{ 
	var r_del = window.confirm("Are you sure?");
	if(r_del == true)
		{
		$.ajax({
				type: 'GET',
	
				url: 'finance/accounts_delete_employee_deduction_details.php',
				data: {id_ded:id_ded,id:id,year:year},
				success: function(data) {
				window.location.href ="accounts_finance_pay_view_employee_deduction_details.php?year="+year+"";
										}
				});
	
		}
	else
	{
	return false;	
	}
	}
	//function show deduction
	function show_pf_details(year)
	{
	uId = $("#uid").val();
	$.ajax({
			type: 'GET',
			url: 'finance/show_employee_pf_details.php',
			data: {uId:uId,year:year},
			success: function(data) {
		 	 $('#data_pf').html(data);
									}
			});
	
	}
	//function show deduction
	function show_tax_details(year)
	{
	uId = $("#uid").val();
	$.ajax({
			type: 'GET',
			url: 'finance/show_employee_tax_details.php',
			data: {uId:uId,year:year},
			success: function(data) {
		  	$('#data_tax').html(data);
									}
		});
	
	}

/*
function reject_salary(id,year,month)
{
	
var r_del = window.confirm("Are you sure for Reject Salary?");
if(r_del == true)
{

$.ajax({
		type: 'GET',
		
		url: 'finance/accounts_type.php',
		data: {id:id,year:year,month:month},
		success: function(data) {
				window.location.href ="accounts_finance_pay_employee_details.php?id="+id+"&&year="+year+"&&month="+month+"";
		}
		})	

}
else
{
return false;	
}
	
}*/