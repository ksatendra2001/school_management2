<?php

require_once('tcpdf_include.php');
require_once('../../config/config.php');


//============================================================+
// File name   : example_048.php
// Begin       : 2009-03-20
// Last Update : 2013-05-14
//
// Description : Example 048 for TCPDF class
//               HTML tables and table headers
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: HTML tables and table headers
 * @author Nicola Asuni
 * @since 2009-03-20
 */

// Include the main TCPDF library (search for installation path).


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('By navikaran');
$pdf->SetTitle('School Name');



// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'c', 8);

// add a page
$pdf->AddPage();

$pdf->Write(5, 'Donation Receipt', '', 0, 'C', true, 0, false, false, 0);

$pdf->SetFont('helvetica', '', 12);

// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------

// NON-BREAKING ROWS (nobr="true")
//get the content from the qurey
$donation_details="SELECT *
FROM `transaction_donation_details` 
WHERE `id`=".$_GET['id']."  ";

$exe_donation_details=mysql_query($donation_details);
$fetch_donation_details=mysql_fetch_array($exe_donation_details);
$tbl = <<<EOD
<table border="1" cellpadding="2" cellspacing="2" align="left">
 <tr nobr="true">
  <th  colspan="2" align="center">Donor Name:$fetch_donation_details[donor_name]</th>
 </tr>
  <tr nobr="true">
  <td>Transaction Id</td>
  <td>$fetch_donation_details[id]</td>
 
 </tr>
 <tr nobr="true">
  <td>Description</td>
  <td>$fetch_donation_details[description]</td>
 
 </tr>
 <tr nobr="true">
  <td>Amount</td>
  <td>$fetch_donation_details[amount]</td>
  
 </tr>
 <tr nobr="true">
  <td>Date & Time</td>
 <td>$fetch_donation_details[date]</td>
 </tr>
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

// -----------------------------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_048.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+


?>