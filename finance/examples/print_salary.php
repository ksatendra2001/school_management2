<?php

require_once('tcpdf_include.php');
require_once('../../config/config.php');
$deduction_cal=0;
		$name=$_GET['name'];
		$year=$_GET['year'];
		$month=$_GET['month'];
		
		
	$total_salary=0;
	$total_measure=0;
	
	
	$date="SELECT `month`,`year`,month_of_year
	       FROM `dates_d`
		   WHERE `month_of_year`=".$month."";
   $exe_date=mysql_query($date);
   $fetch_date=mysql_fetch_array($exe_date);
   $month_name=$fetch_date['month'];	
	
	  //view details of `employee_salary_details` table 						
	$view_salary_calculate=" SELECT * 
	                         FROM `employee_salary_details`
							 WHERE `emp_id`=".$_GET['id']." AND `year`=".$year." AND `month_of_year`=".$month."
							 ";
	$view_result=mysql_query($view_salary_calculate);
	$fetch_result=mysql_fetch_array($view_result) ;   
	$status=$fetch_result['status'];
	
	
	$get_mea=
			"SELECT  DISTINCT measures
			FROM salary_calculator
			
			 ORDER BY id DESC";
			$exe_mea_name=mysql_query($get_mea);
			$k=0;
			$mea=array();
			while($fetch_mea_name=mysql_fetch_array($exe_mea_name))
			{
				 $mea[$k]=$fetch_mea_name['measures'];
			echo '';	
				
				$k++;
			}
	$total_salary=$total_salary+$fetch_result['basic_salary'];
	 $total_earning=$total_salary+$total_measure;
	 
//============================================================+
// File name   : example_048.php
// Begin       : 2009-03-20
// Last Update : 2013-05-14
//
// Description : Example 048 for TCPDF class
//               HTML tables and table headers
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: HTML tables and table headers
 * @author Nicola Asuni
 * @since 2009-03-20
 */

// Include the main TCPDF library (search for installation path).


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('By navikaran');
$pdf->SetTitle('School Name');



// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}
$deduction_cal=0;
$total_salary=0;
// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'C', 12);

// add a page
$pdf->AddPage();

$pdf->Write(3, 'Status:Pending'  , '3', '', 'L', true, 3, false, false, 0);
$pdf->Write(3, 'Year:  '.$year.''  , '3', '', 'L', true, 3, false, false, 0);
$pdf->Write(3, 'Month:  '.$month_name.''  , '3', '', 'L', true, 3, false, false, 0);
$pdf->Write(3, 'Employee Id:'.$_GET['id'].''  , '3', '', 'C', true, 3, false, false, 0);
$pdf->Write(3, 'Name: '.$name.''  , '3', '', 'C', true, 3, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$q=3;
$i;
$tbl='';
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------

// NON-BREAKING ROWS (nobr="true")
//get the content from the qurey
/*$donation_details="SELECT *
FROM `transaction_donation_details` 
WHERE `id`=".$_GET['id']."  ";

$exe_donation_details=mysql_query($donation_details);
$fetch_donation_details=mysql_fetch_array($exe_donation_details);*/

$tbl.='
<table border="1" cellpadding="2" cellspacing="2" align="left">
 <tr nobr="true">
  <th style="color:red"><b>Earning</b></th>
  <th style="color:red"><b>Amount</b></th>
 </tr>
  <tr nobr="true">
  <td>Basic salary</td>
  <td>'.$fetch_result['basic_salary'].'</td>
 
 </tr>';

 for($i=0;$i<$k;$i++)
	{
	$mea_name=$mea[$i];
	if($fetch_result[$q]==0)
		{
			
			
		}
		else if($mea[$i]=='PF')
		{
	$pf=$fetch_result[$q];
		}
		else
		{
	
	 $tbl.='
		 <tr nobr="true">
  <td>'.$mea_name.'</td>
   <td>'.$fetch_result[$q].'</td>
  
		</tr>
		';
	 $total_measure=$total_measure+$fetch_result[$q];
		}
		$q++;
	}
	 $total_salary=$total_salary+$fetch_result['basic_salary'];
	 $total_earning=$total_salary+$total_measure;

		$tbl.='
 
 <tr nobr="true">
  <td><h4>Total earning</h4></td>
  <td><b>'.$total_earning.'</b></td>
  
 </tr>
 
 <tr nobr="true">
  <td><label style="color:red">Deductions</label></td>
 
 </tr>';
  $get_deduction_details="SELECT * 
	                        FROM `employee_deduction_details`
							WHERE `emp_id`=".$_GET['id']." AND `year`=".$year." ";
		$exe_deducation_details=mysql_query($get_deduction_details);
		while($fetch_deduction_details=mysql_fetch_array($exe_deducation_details))
		      {
				  $deduction=$fetch_deduction_details['amount'];
				  $deduction_did=$fetch_deduction_details['did'];
		           
				   //query to get deduction type
				   $get_deduction_type="SELECT *
				                        FROM `finance_deduction_details`
										WHERE `id`=".$deduction_did." AND `year`=".$year."";
										
					$exe_deduction_type=mysql_query($get_deduction_type);
					while($fetch_deduction_type=mysql_fetch_array($exe_deduction_type))
					{
						$deduction_type=$fetch_deduction_type['deduction_type'];
 
 $tbl.='
 <tr nobr="true">
 <td>'.$deduction_type.'</td>
<td>'.$deduction.'</td>	

 </tr>';
 
 $deduction_cal=$deduction_cal+$deduction;
					}
			  }
			   $deduction_cal+=$pf;
	  $total_net_salary=$total_earning-$deduction_cal;			  
			  $tbl.='
 <tr nobr="true">
  <td>PF</td>
  <td>'.$pf.'</td>
  </tr>
  <tr nobr="true">
 <td><h4>Deductions amount</h4></td>
 
 <td><b>'.$deduction_cal.'</b></td>
 </tr>
  <tr nobr="true">
  <td><h3 style="color:green">Net Salary</h3></td>
  <td><h3 style="color:green">'.$total_net_salary.'</h3></td>
  </tr>
</table>
';
	
	
		  
		
	

$pdf->writeHTML($tbl, true, true, true, true, '');

// -----------------------------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_048.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+


?>