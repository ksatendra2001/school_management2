// JavaScript Document
function showEditButton(Id)
{
	
	var ele = document.getElementById(Id);
	ele.style.visibility = "visible";
}
//-------------------------------------Code for hiding the edit symbol-------------------
function hideEditButton(Id)
{
	var ele = document.getElementById(Id);
	ele.style.visibility = "hidden";
}

//-----------------------------------code for hiding edit symbol ends here-------------------

//-------------------------------------Code for changing the edit symbol-------------------
function changeButton(Id)
{
	var ele = document.getElementById(Id);
	ele.src = "img/edit-over.gif";
}
//-----------------------------------code for changing edit symbol ends here-------------------

//------------------------------------code to redirect to edit test details-------------------
function redirectEditTestDetails(Id)
{
	var url = "edit_test_details.php?id="+Id;
	window.location.href = url;
}

//----------------------------------- code to redirect to edit test details Ends here-------------

//-------------------------------code for Inserting marks start here-------------------
function getValueInsert(Id)			//Called on onBlur of <input> when an Insert is required
{
	//Function to get the value for Insert operation on grades
	var name = document.getElementById(Id).name;
	var value = document.getElementById(Id).value;
	if(value == '')
	{
		document.getElementById(Id).value.innerHTML="";
		return;
	}
	else
	{
		//there is a value which has to be Inserted
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		 	 xmlhttp=new XMLHttpRequest();
		  }
		  else
		  {// code for IE6, IE5
		  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		  xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				if(xmlhttp.responseText == 1)
				{
					//Marks Inserted successfully
					var IMGID = Id+'IMG';
					
					document.getElementById(IMGID).src = "images/icon/color_18/checkmark2.png";
					document.getElementById(IMGID).style.visibility = "visible";
				}
				else
				{
					//Error in Inserting marks
					var IMGID = Id+'IMG';
					document.getElementById(IMGID).src = "images/icon/color_18/close.png";
					document.getElementById(IMGID).style.visibility = "visible";
				}
			}
		  else
		  {
			  	var IMGID = Id+'IMG';
				document.getElementById(IMGID).src = "img/blueloading.gif";
				document.getElementById(IMGID).style.visibility = "visible";
		  }
		  }
		xmlhttp.open("GET","grades/grades_insert.php?marks="+value+"&"+name,true);
		xmlhttp.send();
		
		
	}
}
//--------------------------------Code for inserting marks ends here-------------------------------


//-------------------------------Code for updating Marks start here--------------------------------
var prevValueUpdate;
//variable to store value on onfocus during an update event, this is the value already present

var curValueUpdate;
//variable to store value on onblur during an update event, this is the current value after update

function getValueUpdate(Id)		//Called on onfocus of <input> when an update is required
{
	//Function to get the values for update operation on grades
	var valueUpdate = document.getElementById(Id).value;
	preValueUpdate = valueUpdate;		//Store the previous value in a global variable
}

function setValueUpdate(Id)
{
	//function to set the value to update in the table
	var name = document.getElementById(Id).name;
	var curValueUpdate = document.getElementById(Id).value;
	if(curValueUpdate == preValueUpdate)
	{
		document.getElementById(Id).value.innerHTML="";
		return;
	}
	else
	{
		//Update required
		//Ajax call to insert data	  
		  if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		 	 xmlhttp=new XMLHttpRequest();
		  }
		  else
		  {// code for IE6, IE5
		  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		  xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				if(xmlhttp.responseText == 1)
				{
					//Marks update successfully
					var IMGID = Id+'IMG';
					document.getElementById(IMGID).src = "images/icon/color_18/checkmark2.png";
					document.getElementById(IMGID).style.visibility = "visible";
				}
				else
				{
					//Error in updating marks
					var IMGID = Id+'IMG';
					document.getElementById(IMGID).src = "images/icon/color_18/close.png";
					document.getElementById(IMGID).style.visibility = "visible";
				}
			}
		  else
		  {
			  	var IMGID = Id+'IMG';
				document.getElementById(IMGID).src = "img/blueloading.gif";
				document.getElementById(IMGID).style.visibility = "visible";
		  }
		  }
		xmlhttp.open("GET","grades/grades_update.php?marks="+curValueUpdate+"&"+name,true);
		xmlhttp.send();
		  
	}
}

//------------------------------------Code for updating marks Ends here---------------------------------
