function print_pass()
{

 var divElements = document.getElementById("print_pass").innerHTML;
 
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML = 
              '<html style="background-color:white"><head><title></title></head><body>' + 
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
	
	
}
function show_data_year(year)
{
		
		
		$.ajax({
		type: 'GET',
		url: 'security/show_data_year.php',
		data: {year:year},
		success: function(data) {
				$('#data_year').html(data);
		}
		});
	
}
function show_data_month()
{
		year = $("#year").val();
		month = $("#month").val();
		
		$.ajax({
		type: 'GET',
		url: 'security/show_data_month.php',
		data: {year:year,month:month},
		success: function(data) {
				$('#data_month').html(data);
		}
		});
	
}
function show_data_weekly()
{
		year = $("#year").val();
		month = $("#month").val();
		from = $("#from_date").val();
		to = $("#to_date").val();
		$.ajax({
		type: 'GET',
		url: 'security/show_data_weekly.php',
		data: {year:year,month:month,from:from,to:to},
		success: function(data) {
				$('#data_week').html(data);
		}
		});
	
}
function show_data_day()
{
		day = $("#date_day").val();
		if(day == "")
		{
		return;	
			
		}
		else
		{
		$.ajax({
		type: 'GET',
		url: 'security/show_data_day.php',
		data: {full_date:day},
		success: function(data) {
				$('#data_day').html(data);
		}
		});
		}
}