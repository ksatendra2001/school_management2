<?php

function create_test()

{
echo' 
<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Create Test</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />';
	
                                                                        if(isset($_GET['err']))		//Test Added
                                                                                {
                        echo '<h5 align="center" >Please Fill All Details</h5>';
                                                                                }

	
			
				echo'	<form action="grades/test_added.php" method="post">
					<div class="entry">
					<div class="sep"></div>
				</div>
			  <div class="section ">
				<label> Test ID</label>   
				<div>
				<input type="text" name="test_id"/>
				</div>
			   </div>
			   
			   <div class="section ">
				<label> Test Name</label>   
				<div>
				<input type="text" name="test_name"/>
				</div>
			   </div>
				
				<div class="section ">
				<label> Maximum Marks</label>   
				<div>
				<input type="text" name="max_marks" />
				</div>
			   </div>
				
				<div class="section ">
				<label>Test Date</label>   
				<div>
				<input type="text" name="test_date"  id="datePicker"  class="datepicker"/>
				</div>
			   </div>
			   
			<br></br>
      <div class="section last">	
	<button class="uibutton icon  add " type="submit">Add Test</button> 
	<button class="uibutton icon special cancel " type="reset">Cancel</button> 	
		</form>
		</div>
		</div>
		</div>
		</div>
		
				';
	
}

function view_tests_grades()
{
	//Query to get the current session details
	
	
	
	
	//Query to display details of the tests
	$queryTestDetails = "
	SELECT `Test_id`, `Test Name`, `Max Marks`, 
	EXTRACT(YEAR FROM `Time stamp`) as Year,
	EXTRACT(MONTH FROM `Time stamp`) as Month,
	EXTRACT(DAY FROM `Time stamp`) as Day,
	EXTRACT(HOUR FROM `Time stamp`) as Hour,
	EXTRACT(MINUTE FROM `Time stamp`) as Minute,
	EXTRACT(SECOND FROM `Time stamp`) as Second,
	Id, `Test Date`
	FROM `test_index`
	  WHERE session_id=".$_SESSION['current_session_id']."
	";
	$getTestDetails = mysql_query($queryTestDetails);
	$Sno=0;
	echo '
	<div class="row-fluid">                            
                         <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Test</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
				';
	
            if(isset($_GET['added']))		//Test Added
            {
                    echo '<span class="system positive" style="" id="sub_pos"><h3 align="center">&nbsp;Test added successfully</h3></span><br/>';
            }

            if(isset($_GET['deleted']))		//Test Deleted
            {
                    echo '<span class="system positive" style="" id="sub_pos">&nbsp;Test deleted successfully</span><br/>';
            }

            if(isset($_GET['noDelete']))		//error in deleting test
            {
                    echo '<span class="system negative" style="" id="cls_neg">&nbsp;Error: deleting test, please try again</span><br/>';
            }

            if(isset($_GET['updated']))		//Test updated
            {
                    echo '<span class="system positive" style="" id="sub_pos">&nbsp;Test updated successfully</span><br/>';
            }

            if(isset($_GET['noUpdate']))		//error in updating test
            {
                    echo '<span class="system negative" style="" id="cls_neg">&nbsp;(no changes made)Error: updating test, please try again</span><br/>';
            }			


            echo '<!--[if !IE]>start table_wrapper<![endif]-->

                    <form action="view_tests_grades.php" method="post">
                    <div class="entry">
                    <div class="sep"></div>
            </div>

					
  <div id="dataTable_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row-fluid tb-head"><div class="span6"><div class="dataTables_filter" id="dataTable_filter"></div></div></div></div><table class="table table-bordered table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
                                    <thead align="left">
                                         <tr role="row">
    <th style="width: 56px;">SNo.</th>
    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Test Id: activate to sort column descending" style="width:10px;">Test Id</th>
    <th class="sorting" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Test Names: activate to sort column ascending" style="width: 90px;">Test Name</th>
    <th class="sorting" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Max Marks: activate to sort column ascending" style="width: 100px;">Max Marks</th>

    <th class="sorting" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Test Date: activate to sort column ascending" style="width: 100px;">Test Date</th>
    <th class="sorting" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Creation Time: activate to sort column ascending" style="width: 50px;">Creation Time<span style="font-size : 09px;color:#736F6E;">(HH:MM:SS)</span></th>
    <th class="sorting" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Creation Date: activate to sort column ascending" style="width: 80px;">Creation Date<span style="font-size : 09px;color:#736F6E;">(DD/MM/YYYY)</span></th></tr>

</thead>
';

    while($testDetails = mysql_fetch_array($getTestDetails))
    {
            $test_id = $testDetails['Test_id'];		//Contains the test Id
            $test_name = $testDetails['Test Name'];	//Contains the test name
            $max_marks = $testDetails['Max Marks'];	//Contains the Max Marks
            //$created_by = $testDetails[3];	//name of user who created
            $year = $testDetails[4];		//Year of creation
            $month = $testDetails[5];		//Month of creation
            $day = $testDetails[6];			//Day of creation
            $hour = $testDetails[7];		//Hour of creation
            $minute = $testDetails[8];		//Minute of creation
            $second = $testDetails[9];		//Second of creation
            $Id = $testDetails[10];			//Unique Id of the table
            $test_date = $testDetails['Test Date'];	//Test date

            $creation_date = $day.'/'.$month.'/'.$year;
            $creation_time = $hour.':'.$minute.':'.$second;
            ++$Sno;
            echo'
            <tr onmouseover="showEditButton('.$Sno.');" onmouseout="hideEditButton('.$Sno.');">
                    <td>'.$Sno.'</td>
                    <td>'.$test_id.'</td>
                    <td>'.$test_name.'</td>
                    <td>'.$max_marks.'</td>
                    
                    <td>'.$test_date.'</td>
                    <td>'.$creation_time.'</td>
                    <td>'.$creation_date.'<a href="#" ><img id="'.$Sno.'" style="visibility:hidden;" src="img/edit.gif" align="right" onmouseover="changeButton('.$Sno.')" onclick="redirectEditTestDetails('.$Id.');" /></a></td>

            </tr>


            ';}
            echo '</div></table>
            </form>
            </div>
            </div>
            </div>
            </div>
';	
											
						
	
}





function edit_test()
{
	$rowId = $_GET['id'];
	
	  //Query to get the current session details
	 
	//Query to get the details for that test table Id
	$queryTestDetails = "
	SELECT `Test_id`,`Test Name`,`Max Marks`
	FROM `test_index`
	WHERE `Id` = '$rowId  AND session_id=".$_SESSION['current_session_id']."'
	";
	$getTestDetails = mysql_query($queryTestDetails);
	$testDetails = mysql_fetch_array($getTestDetails);
	$testId = $testDetails[0];
	$testName = $testDetails[1];
	$maxMarks = $testDetails[2];
	
	echo '

					
						<div class="full_w">
				<div class="h_title">Edit Test details</div>
				<table>
					<div class="entry">
					<div class="sep"></div>
				</div>
							';	
						
						echo '<tr><td>
											<a href="grades/test_deleted.php?rowid='.$rowId.'" class="button add_new"><span><span>Delete Test</span></span></a></td></tr>
										';
											
											echo '<!--[if !IE]>start table_wrapper<![endif]-->

											
												
												<form action="grades/test_edited.php" class="search_form general_form" method="post" enctype="multipart/form-data">
													<!--[if !IE]>start fieldset<![endif]-->
													
														<!--[if !IE]>start forms<![endif]-->
													<tr><td>
														<input class="text" name="rowid" type="hidden" value="'.$rowId.'" />
														
														
															<label><h3>Test ID</h3></label>	
														
																<input class="text" name="test_id" type="text" value="'.$testId.'" />
														</td></tr>
														
													<tr><td>
															<label><h3>Test Name</h3></label>	
														
																<input class="text" name="test_name" type="text" value="'.$testName.'" />
															</td></tr>
														
														
													<tr><td>
															<label><h3>Maximum Marks</h3></label>	
															
																<span class="input_wrapper"><input class="text" name="max_marks" type="text" value="'.$maxMarks.'" /></span>
														</td></tr>
														
														<!--[if !IE]>start row<![endif]-->
														
															
																<tr><td>
																
																<button class="button approve id="btn"">UPDATE TEST
			<button class="button approve id="btn"">Cancel		
																</td></tr>

														
														<!--[if !IE]>end row<![endif]-->

														</div>
														<!--[if !IE]>end forms<![endif]-->
														
													</fieldset>
													<!--[if !IE]>end fieldset<![endif]-->
									
												</form>
												<!--[if !IE]>end forms<![endif]-->	
												
												

													<div class="table_wrapper_inner">

													<table cellpadding="0" cellspacing="0" width="100%">

														<tbody><tr>
					<!--Header of the table-->
															
															</tr>';
											
											echo '<!--[if !IE]>end table_wrapper<![endif]--></tbody></table>';
			
	
}

function classes_view()
{

	echo '
	<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Classes</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
				

<div class="entry">
<div class="sep"></div>
</div>

';	



        //Queries to get data from the class table

$queryClass = "SELECT `class_name`,`section`,`class`,`cId` FROM `class_index`  ORDER BY class+0 ASC,section ASC";
$getClass = mysql_query($queryClass);




                                                echo '<!--[if !IE]>start table_wrapper<![endif]-->

											
                                    <div id="dataTable_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row-fluid tb-head"><div class="span6"><div class="dataTables_filter" id="dataTable_filter"></div></div></div></div><table class="table table-bordered table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
                                        <thead>
                                            <tr role="row">
											<th style="width: 56px;">SNo.</th>
											<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Class Name: activate to sort column descending" style="width: 254px;">Class Name</th>
											<th class="sorting" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Total number of students: activate to sort column ascending" style="width: 382px;">Total number of students</th>
											<th class="sorting" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Operations: activate to sort column ascending" style="width: 347px;">Operations</th></tr>
                                        </thead>
                                        <tbody align="center">
';
                                                $Sno = 0;
                                                while($classes = mysql_fetch_array($getClass))
                                                {
                                                        $class_name = $classes[0];

                                                        $id = $classes[3];
                                                        $queryNoOfStudents = "SELECT COUNT(*) FROM `class` WHERE `classId` = '$id' ";
                                                        $getNoOfStudents = mysql_query($queryNoOfStudents);
                                                        $noOfStudents = mysql_fetch_array($getNoOfStudents);
                                                        $noOfStudents = $noOfStudents[0];
                                                        echo '<tr><td>'.++$Sno.'</td>
                                                        <td>'.$class_name.'</td>
                                                        <td>'.$noOfStudents.'</td>
                                                        <td><a href="insert_test_name_wise.php?classId='.$id.'" >Insert Marks</a></td>

                                                        </tr>';
                                                }
                            echo '</div>
                            </tbody>
                            </table>
                            </div>
                            </div>
                            </div>
                            </div>';

											
}

function view_tests_name_for_class()
{
	$classId = $_GET['classId'];
	//Query to get the name of the class
	$queryClassName = "SELECT `class_name` FROM `class_index` WHERE `cId` = '$classId'";
	$getClassName = mysql_query($queryClassName);
	$className = mysql_fetch_array($getClassName);
	$className = $className[0];
	//-------End query to extract the name of the class -------
	

	
	//Query to display details of the tests
	$queryTestDetails = "
	SELECT `Test_id`, `Test Name`, `Max Marks`, users.Name, 
	EXTRACT(YEAR FROM `Time stamp`) as Year,
	EXTRACT(MONTH FROM `Time stamp`) as Month,
	EXTRACT(DAY FROM `Time stamp`) as Day,
	EXTRACT(HOUR FROM `Time stamp`) as Hour,
	EXTRACT(MINUTE FROM `Time stamp`) as Minute,
	EXTRACT(SECOND FROM `Time stamp`) as Second,
	`Id`, `Test Date`
	
	FROM `test_index`
	
	INNER JOIN `users`
	ON users.uId = test_index.Created_by  WHERE session_id=".$_SESSION['current_session_id']."
	
	";
	
	$getTestDetails = mysql_query($queryTestDetails);
	$Sno=0;
	
	
	echo '
						<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i><b>Available tests for class : '.$className.'</b></span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	
					<div class="entry">
					<div class="sep"></div>
				</div>
	';		
						
											
											echo '
							<div id="dataTable_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row-fluid tb-head"><div class="span6"><div class="dataTables_filter" id="dataTable_filter"></div></div></div></div><table class="table table-bordered table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
                                        <thead>
                                            <tr role="row">
											<th style="width: 56px;">SNo.</th>
											<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Test Id: activate to sort column descending" style="width:10px;">Test Id</th>
											<th class="sorting" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Test Names: activate to sort column ascending" style="width: 90px;">Test Name</th>
											<th class="sorting" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Max Marks: activate to sort column ascending" style="width: 100px;">Max Marks</th>
											<th class="sorting" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Created by: activate to sort column ascending" style="width: 100px;">Created by</th>
											<th class="sorting" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Test Date: activate to sort column ascending" style="width: 100px;">Test Date</th>
											<th class="sorting" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Creation Time: activate to sort column ascending" style="width: 50px;">Creation Time<span style="font-size : 09px;color:#736F6E;">(HH:MM:SS)</span></th>
											<th class="sorting" role="columnheader" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Creation Date: activate to sort column ascending" style="width: 80px;">Creation Date<span style="font-size : 09px;color:#736F6E;">(DD/MM/YYYY)</span></th></tr>
                                       
									    </thead>
										<tbody align="center">';
											
											while($testDetails = mysql_fetch_array($getTestDetails))
											{
												$test_id = $testDetails[0];		//Contains the test Id
												$test_name = $testDetails[1];	//Contains the test name
												$max_marks = $testDetails[2];	//Contains the Max Marks
												$created_by = $testDetails[3];	//name of user who created
												$year = $testDetails[4];		//Year of creation
												$month = $testDetails[5];		//Month of creation
												$day = $testDetails[6];			//Day of creation
												$hour = $testDetails[7];		//Hour of creation
												$minute = $testDetails[8];		//Minute of creation
												$second = $testDetails[9];		//Second of creation
												$id = $testDetails[10];			//Unique test Id
												$test_date = $testDetails[11];	//Test date
												
												$creation_date = $day.'/'.$month.'/'.$year;
												$creation_time = $hour.':'.$minute.':'.$second;
												
												echo'
												<tr>
													<td>'.++$Sno.'</td>
													<td>'.$test_id.'</td>
													<td><a href="insert_value_test_name_wise.php?classId='.$classId.'&testId='.$id.'&testName='.$test_name.'">'.$test_name.'</a></td>
													<td>'.$max_marks.'</td>
													<td>'.$created_by.'</td>
													<td>'.$test_date.'</td>
													<td>'.$creation_time.'</td>
													<td>'.$creation_date.'</td>
												</tr>
												';
											}
											
											echo '
											</tbody>
											</table>
											</div>
												</div>
												</div>
												</div>';
											
}





function insert_value_test_name_wise()
{
	
	$classId = $_GET['classId'];
	$testId = $_GET['testId'];
	$testName = $_GET['testName'];
	
	$queryClassName = "SELECT `class_name` FROM `class_index` WHERE `cId` = '$classId'";
	$getClassName = mysql_query($queryClassName);
	$className = mysql_fetch_array($getClassName);
	$className = $className[0];
	
	echo '
	<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i><b>Class : '.$className.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Test Name : '.$testName.'</b></span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
		
		
					<div class="entry">
					<div class="sep"></div>
				</div>
											
											';
												
											
											$queryTeacherSubject = "
											SELECT Name, subject.subject
											FROM users
											INNER JOIN teachers
											ON teachers.tId = users.uId
											JOIN subject
											ON subject.subject_id = teachers.subject_id
											WHERE teachers.class = '$className'
											";
											$getTeacherSubject = mysql_query($queryTeacherSubject);
											
						
											
											echo '<!--[if !IE]>start table_wrapper<![endif]-->


										<table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sno.</th>
                                                <th>Teacher Name</th>
                                                <th>Subject</th>
                                            </tr>
                                        </thead>
										<tbody align="center">
													';
											$Sno = 0;			//For generating serial numbers
											while($teacherSubject = mysql_fetch_array($getTeacherSubject))
											{
												$teacherName = $teacherSubject[0];
												$subjectName = $teacherSubject[1];
												
												echo'
												<tr>
													<td>'.++$Sno.'</td>
													<td>'.$teacherName.'</td>
													<td>'.$subjectName.'</td>												
												</tr>
												';
											}
													
													
											echo'	</tbody>
													</table>
													';
													
							//------------End of the section to display the subject and teacher---------
/*
Now to insert/update data into the marks table, we have to first get the current marks table and then enter 
the following details into the table
sId - Id of the student which will come from student_user table, this is to identify the student
tId - Id of the teacher which will come from users table, this is to tel which teacher is associated with it
cId - Id of the class which will come from class_index table, this will tell the class of the student
test_id - id of the test which will come from test_index table, this will tell the test Id
subject_id - id of the subject which will come from subject table, this will tell for which subject the marks ar 
			given
			
There will be 2 javascript function calls:
one will be to update the marks when the above details are given and to do that we will make a select query if it returns a value we will put it in the textbox and will associate a update function call with the text box

The other one will be when the data will be entered for the first time to do this again we will make a select query and if it doesnot return anything then we will associate a insert function call with the text box

*/
							
							//Query to get the teacher Id, subject Id and Subject name
							$queryTeacherSubjectIdName = "
							SELECT teachers.tId,teachers.subject_id,subject.subject
							FROM teachers
							INNER JOIN subject
							ON subject.subject_id = teachers.subject_id
							WHERE `class` = '$className'
							";
							$getTeacherSubjectIdName = mysql_query($queryTeacherSubjectIdName);
							$result_count = mysql_num_rows($getTeacherSubjectIdName); //it will result the number of rows returned
							$teacherIdMarks = array();			//teacher Id for marks operations
							$subjectIdMarks = array();			//Subject Id for marks operations
							$subjectNameMarks = array();		//Subject name for marks operations
							
							for($i=0;$i<$result_count;$i++)
							{
								$teacherSubjectIdName = mysql_fetch_array($getTeacherSubjectIdName);
								$teacherIdMarks[$i] = $teacherSubjectIdName[0];		//Teacher Id
								$subjectIdMarks[$i] = $teacherSubjectIdName[1];		//Subject Id
								$subjectNameMarks[$i] = $teacherSubjectIdName[2];	//Subject Name
							}
							
							
							//Query to get the students id and names for marks
							$queryStudent = "
							SELECT student_user.sId, `Name`
							FROM student_user
							INNER JOIN class
							ON student_user.sId = class.sId
							WHERE class.classId = '$classId'
							ORDER BY `Name` ASC
							";
							
							$getStudents = mysql_query($queryStudent);
							$uniqueId = 0;					//use to generate unique ID for each <td>
							
											echo'	<table class="table table-bordered table-striped">
														
									<!--Header of the table-->
									 <thead>
                                            <tr style="text-align: center">
											<th style="width:90px">Student Name</th>';
											for($i=0;$i<$result_count;$i++)
											{
												echo '<th style="width:70px; ">'.$subjectNameMarks[$i].'</th>';
											}
								echo   '</tr>
								 </thead>
								 <tbody align="center">';
								while($students = mysql_fetch_array($getStudents))
								{
									/*Now use a for loop to generate TD containing a text 
										with Test Id, Teacher Id, subject Id, classId
									*/
									$studentId = $students[0];
									$studentName = $students[1];
									
									echo '<tr>';
										echo '<td>'.$studentName.'</td>';
										for($i=0;$i<$result_count;$i++)
										{
											/*
											$uniqueId : generates a unique Id for a <td>
											$studentId : contains the student Id and it changes every <tr>
											$teacherIdMarks : contains the teacher Id and changes every<td>
											$classId : contains the class Id and is constant throughout
											$testId : contains the test Id and is constant
											$subjectIdMarks : contains the subject Id and changes every<td>
											*/
											$queryMarks = "SELECT `mId`,`marks` FROM marks WHERE sId = '$studentId' AND tId = '$teacherIdMarks[$i]' AND cId = '$classId' AND test_id = '$testId' AND subject_id = '$subjectIdMarks[$i]'";
											$getMarks = mysql_query($queryMarks);
											$count_no_rows = mysql_num_rows($getMarks);
											
											if($count_no_rows == 0)
											{
												++$uniqueId;
												//For the insert operation
												echo '<td><img id="'.$uniqueId.'IMG" style="visibility:hidden" /><input id="'.$uniqueId.'" style="width :60px" type="text" name="studentId='.$studentId.'&teacherId='.$teacherIdMarks[$i].'&classId='.$classId.'&testId='.$testId.'&subjectId='.$subjectIdMarks[$i].'" onBlur="getValueInsert('.$uniqueId.')" /></td>';
											}
											else
											{
												//For the update operation
												$marks = mysql_fetch_array($getMarks);
												$mId = $marks[0];
												$marksValue = $marks[1];
												$uniqueId++;
												echo '<td><img id="'.$uniqueId.'IMG" style="visibility:hidden" /><input id="'.$uniqueId.'" style="width :60px" type="text" name="studentId='.$studentId.'&teacherId='.$teacherIdMarks[$i].'&classId='.$classId.'&testId='.$testId.'&subjectId='.$subjectIdMarks[$i].'&mId='.$mId.'" value="'.$marksValue.'" onfocus="getValueUpdate('.$uniqueId.')" onblur="setValueUpdate('.$uniqueId.')" /></td>';
											}
										}
									echo '</tr>';
								}
											
											echo '
											</tbody>
											</table>';
											echo'
						
	
	</div>
												</div>
												</div>
												</div>
												';
}





?>