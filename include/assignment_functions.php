<?php


///////////////////////////////////////////////////// function to upload assignments ////////////////////////////////////////////////////////////

function teacher_upload_assignments()
{
echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Upload Assignments </span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
';	
    //query to get class             			 		
   
	 $query="SELECT DISTINCT class_index.class_name ,class_index.cId
            FROM  class_index
            
          INNER JOIN teachers
          ON teachers.class=class_index.class_name
            WHERE teachers.tId=".$_SESSION['user_id']." ";
	 $fetch=mysql_query($query) ;		
	
 echo'	
   <form id="validation_demo" name="notes" action="assignments/upload_assignments.php" method="post" enctype="multipart/form-data"> 
         			 
  <input type="hidden" name="tid" value="'.$_SESSION['user_id'].'">
 
  <div class="section ">
  <label>Title<small></small></label>   
  <div> 
 <input type="text" class="validate[required] medium" name="title" />
 </div>
 </div>	
<div class="section ">		 	
  <label>Class<small></small></label>   
  <div>
   <select name="class"   class="Select"  onchange="show_sub(this.value);">
   <option value="">Select Class</option>
  
';
                                while($cls=mysql_fetch_array($fetch))
                                                {
                                                        $cls_name=$cls['class_name'];
                                                echo '																																																																								
                                                <option value="'.$cls_name.'">'.$cls_name.'</option>';
                                                } 
                                                echo'</select>
                                                </div>
                                                </div>
<span id="subject" ></span>

<div class="section">
<label>Description<small></small></label>
<div>
<textarea rows="8" cols="50"  id="" name="description" class="" ></textarea>
</div>
</div>                                               
<div class="section ">
<label>Upload Notes<small></small></label>   
 <div>                         
<input type="file" name="file" class="fileupload validate[required]" />
</div>
</div>	
<div class="section last">
 <div><button class="uibutton submit" rel="1" type="submit"  >Save</button>
 <button class="uibutton special"type="reset"  >Reset</button>   
</div>
</div>											
</form>
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';							
}






/////////////////////////////////////////////////////// function to view assignments ////////////////////////////////////////////////////////////

function  teacher_view_assignments()
{
	
	echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Notes</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
';
	$tId=$_SESSION['user_id'];
	

				  $query = "SELECT class_index.class_name, subject.subject, notes.Title, notes.Description, notes.Date,    notes.Time, notes.notes, notes.tId, notes.nId
									FROM notes
									INNER JOIN session_table
									ON notes.session_id = session_table.sId
									
									INNER JOIN subject
									ON notes.sub_Id = subject.subject_Id
									
									INNER JOIN class_index
									ON notes.cId = class_index.cId
									
									WHERE  session_table.current_session=1 AND notes.tId=".$_SESSION['user_id']."";      
				   $execute=mysql_query($query);
				   $sno =0 ;
								 
echo '
	          
	<table  class="table table-bordered table-striped" id="dataTable">
	<thead>
    <tr>		   
	<th>S.No</th>
	<th>Classes</th>
	<th>Subject</th>
	<th>Title</th>	
	<th>Description</th>
	<th>Date</th>	
	<th>Time</th>
	<th>Download</th>
	<th >Actions</th>						
    </tr>
    </thead>
	<tbody align="center">	 		
';				 
	while($new=mysql_fetch_array($execute))
	{
		++$sno;    
   echo'	
		  <tr >
		  <td >'.$sno.'</td>
		  <td>'.$new['class_name'].'</td>
		  <td>'.$new['subject'].'</td>
		  <td>'.$new['Title'].'</td>
		  <td>'.$new['Description'].'</td>
		  <td>'.$new['Date'].'</td>
		  <td>'.$new['Time'].'</td>
		  
		  <td><a href="notes/note_dump/'.$new['notes'].'">DOWNLOAD</a></td>
		  <td>
<a href="teacher_notes_edit_notes.php?id='.$_SESSION['user_id'].'&name='.$_SESSION['username'].'&dbid='.$new['nId'].'" class="table-icon edit" title="Edit"><img src="images/icon/icon_edit.png"></a>';
					
 echo'<a href="notes/deleting_notes.php?id='.$_SESSION['user_id'].'&dbid='.$new['nId'].'" class="table-icon delete" title="Delete"><img src="images/icon/icon_delete.png"></a>';
 echo'
	  </td>
	  </tr>																			
	 
  ';
  }
  
	echo' </tbody>
	</table>
	
 <a href="teacher_upload_assignment.php?id='.$_SESSION['user_id'].'&name='.$_SESSION['username'].'"class="uibutton icon add " >Add</a> 
								 									
	</div>';                                           
echo'						
  </div><!-- row-fluid column-->
  </div><!--  end widget-content -->
  </div><!-- widget  span12 clearfix-->
  </div><!-- row-fluid -->
';							

	}






/////////////////////////////////////////////////////// function to edit assignment /////////////////////////////////////////////////////////////

function teacher_edit_assignment()
{
	
	echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Notes</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
';
		
    //query to get class             			 		
   $dbid=$_GET['dbid'];
	$query="SELECT  `class` , `Id`
            FROM  `teachers` 
            WHERE `tId`=".$_SESSION['user_id']." ";
	 $fetch=mysql_query($query);

	
       $queryedit="SELECT notes.Description, notes.Title, subject.subject, class_index.class_name 
            FROM notes
			
			INNER JOIN subject
			ON notes.sub_id=subject.subject_id
			
			INNER JOIN class_index
			ON notes.cId=class_index.cId
			 
			WHERE notes.nId=$dbid";

       $execute_edit=mysql_query($queryedit);
       $executeedit=mysql_fetch_array($execute_edit);
	   $title=$executeedit['Title'];
	   $description=$executeedit['Description'];
	   $class=$executeedit['class_name'];
	   $subject=$executeedit['subject'];
	
	
	
		
 echo'	
   <form id="validation_demo" action="notes/editing_notes.php" method="post" enctype="multipart/form-data">              			 
  <div class="section ">
  <label>Title<small></small></label>   
  <div> 
 <input type="text" class="validate[required] medium" name="title" value="'.$title.'" />
 </div>
 </div>	
 <input type="hidden" id="'.$_GET['id'].'">
	<div class="section ">		 	
  <label>Class<small></small></label>   
  <div>
   <select name="class"  data-placeholder="'.$class.'" onchange="show_sub_id(this.value);">
   <option value="">Select Class</option>
   ';
  while($cls=mysql_fetch_array($fetch))
	{
		$cls_name=$cls['class'];
		echo'
																		
	<option value="'.$cls_name.'">'.$cls_name.'</option>';
 }   echo'</select>
 </div>
 </div>
 <span id="subject"></span>
 
   <div class="section">
<label>Description<small></small></label>	
<div>	
<textarea rows="8" cols="50"  id="" name="description" >'.$description.'</textarea>
</div>
</div>  
                                                  
<div class="section ">
<label>Upload Notes<small></small></label>   
 <div>                         
<input type="file" class="fileupload validate[required]" name="file" />

</div>
</div>	
 <input type="hidden" name="session_id" value="'.$_SESSION['current_session_id'].'"/> 
  <input type="hidden" name="user_id" value="'.$_SESSION['user_id'].'"/> 
   <input type="hidden" name="id_db" value="'.$dbid.'"/> 
 <div class="section last">
 <div><button class="uibutton submit"  type="submit"  >Update</button>
 <button class="uibutton special"type="reset"  >Reset</button> 
  
';	                                         
echo'						
		  </div><!-- row-fluid column-->
		  </div><!--  end widget-content -->
		  </div><!-- widget  span12 clearfix-->
		  </div><!-- row-fluid -->
';								
	}

?>
