<?php

function admin_show_charts()
{
	
 echo '<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Adminssions/yr</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">';
								echo "
								<script type='text/javascript'>
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'column',
                margin: [ 50, 50, 100, 80]
            },
            title: {
                text: 'Total Admissions per year in school'
            },
            xAxis: {
                categories: [
                   '1998',
				   '1999',
				   '2000',
				   '2001',
				   '2002',
				   '2003',
				   '2004',
				   '2005',
				   '2006',
				   '2007',
				   '2008',
				   '2009',
				   '2010',
				   '2011',
				   '2012',
				   '2013'
                ],
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total admissions'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        'Admission: '+ Highcharts.numberFormat(this.y, 1) +
                        ' students';
                }
            },
                series: [{
                name: 'Population',
                data: [300,400,450,480,550,600,595,610,600,700,698,709,800,850,1200],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    x: -3,
                    y: 10,
                    formatter: function() {
                        return this.y;
                    },
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    });
    
});
		</script>";
		echo '
		<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>

								
								
								
								</div>
								</div>
								</div>';	
	
	
	
}





?>