<?php
/*This script is to get all the user details and store it in variables.
$user_id => user id (lId from login table)
$username => user name (username from login table)
$priv => privilege from login table (0-Manager, 1-Admin, 2-teacher, 3-students, 4-parents)
$name => name of the user (either from student_user or users table)
*/
$user_id = $_SESSION['user_id'];
$username = $_SESSION['username'];
$priv = $_SESSION['priv'];

if($priv == 3 || $priv == 4)		//The user is a student or a parent (get details from student_user table)
{
	$query = "SELECT `Name` FROM `student_user` WHERE `sId` = '$user_id'";
	$result = mysql_query($query) or die("Query failed");
	$details = mysql_fetch_array($result);
	$name = $details[0];
}

else		//The user can be a Manager, Admin or a teacher (get details from users table)
{
	$query = "SELECT `Name` FROM `users` WHERE `uId` = '$user_id'";
	$result = mysql_query($query);
	$details = mysql_fetch_array($result);
	$name = $details[0];
}
$_SESSION['name'] = $name;

$query_current_session = "SELECT `sId` FROM `session_table` WHERE `current_session` = 1";
$execute = mysql_query($query_current_session);
$current_session = mysql_fetch_array($execute);
$current_session = $current_session[0];

$_SESSION['current_session_id'] = $current_session;
?>