<?php
require_once('include/session.php');
require_once('include/check.php');
require_once('config/config.php');
require_once('include/userdetail.php');
require_once('include/functions_dashboard.php');
require_once('include/grade_functions.php');


?>
<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <title>School Management System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon.ico"/> 

        <!-- CSS Stylesheet-->
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="components/bootstrap/bootstrap-responsive.css" />
        <link type="text/css" rel="stylesheet" href="css/zice.style.css"/>

		
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->  
		
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.ui.min.js"></script> 
		<script type="text/javascript" src="components/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="components/ui/timepicker.js"></script>
        <script type="text/javascript" src="components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="components/form/form.js"></script>
        <script type="text/javascript" src="components/elfinder/js/elfinder.full.js"></script>
        <script type="text/javascript" src="components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="components/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="components/jscrollpane/jscrollpane.min.js"></script>
        <script type="text/javascript" src="components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="components/chosen/chosen.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="components/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="components/flot/flot.js"></script>
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>       
		<script type="text/javascript" src="components/Jcrop/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="components/smartWizard/jquery.smartWizard.min.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/zice.custom.js"></script>
        <script type="text/javascript" src="attendance/js/attendance_js.js"></script>
        <script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
        <script type="text/javascript" src="grades/js/grades_js.js"></script>
		
		
		<script type="text/javascript">
		$(function() {		
		// Calendar 
			var date = new Date();
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();		
			$('#calendar').fullCalendar({
				header: {
					left: 'title',
					center: 'prev,next  ',
					right: 'today month,basicWeek,agendaDay'
				},
			  buttonText: {
					prev: 'Previous',
					next: 'Next '
				},
				editable: true,
				refetchEvents :'refetchEvents',
				selectable: true,
				selectHelper: true,
				dayClick: function(date, allDay, jsEvent, view) {
				var nDate=$.fullCalendar.formatDate( date, 'd' );
				var dDate=$.fullCalendar.formatDate( date, 'dddd ' );
				var fullDate=$.fullCalendar.formatDate( date, ' MMMM , yyyy' );
				$('#calendar .fc-header-title h2').html('<div class="dateBox"><div class="nD">'+nDate+'</div><div class="dD">'+dDate+'<div class="fullD">'+fullDate+'</div><div></div><div class="clear"></div>');
				},
				events: [
					{
						title: 'Project-1(Resources R1)',
						start: new Date(y, m, 1)
					},
					{
						title: 'Project-2(Resources R2)',
						start: new Date(y, m, d-5),
						end: new Date(y, m, d-2)
					},
					
					
					{
						title: 'Meeting For Project 3(Planning resources-3)',
						start: new Date(y, m, d, 10, 30),
						allDay: false
					},
					{
						title: 'Project-1 Submission(Releasing Resouces of Project-1)',
						start: new Date(y, m, d, 12, 0),
						end: new Date(y, m, d, 14, 0),
						allDay: false
					},
					{
						title: 'Project-3(Resouces R3)',
						start: new Date(y, m, d+1, 19, 0),
						end: new Date(y, m, d+1, 22, 30),
						allDay: false
					}
				]
			});  
		}); 
		</script>

		</head>        
        <body>        
<div id="header">
<?php

 top_header();//function for calling header in function_admin.php

?>
</div>


<div id="left_menu">
<?php

 left_menu();//function for calling left menu in function_admin.php

?>
 </div>

<div id="content" >
<div class="inner">
      
<?php
 top_menu();//function for calling top menu in function_admin.php
?>



<?php
   insert_value_test_name_wise();	//This is to generate the dashboard depending on the user privilege; filename=>include/grade_functions.php
?>




<?php
 footer(); //function for calling footer in function_admin.php
?>       

<script type="text/javascript">
$("#grades").addClass("select");
</script>
        
        </body>
        </html>