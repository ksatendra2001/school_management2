<?php

function department_details() 
{                                  //shows details of department


 $id=$_SESSION['user_id'];   //id of dept user.
  
  
$query= "SELECT department.dept_id, department.dept_code, department.dept_name, department.dept_description, DAY( department.timestamp ) , MONTH( department.timestamp ) , YEAR( department.timestamp ), users.name, users.image 
           FROM department
		   INNER JOIN users
		   ON department.uId = users.uId
		   WHERE users.uId = ".$id."";
  
  $fetch=mysql_query($query);
  $execute=mysql_fetch_array($fetch);              //fetches dept details from department table.
 
echo'
	<!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Department Details </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <div class="boxtitle">'.$execute['dept_name'].'</div>
									<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto">


<table class="table table-bordered table-striped">
                                       
                                        <tbody align="left">
                                            <tr class="odd gradeX">
                                                 <td><b><b/></td>
                                                <td><img src="dump/'.$execute['image'].'"</td>
                                               
                                            </tr>
											
											 <tr class="even gradeC">
                                                
                                               <td><b>Department Head<b/></td>
											    <td>'.$execute['name'].'</td>
                                            </tr>
											
                                            <tr class="even gradeC">
                                                 <td><b>Department Code<b/></td>
                                                <td>'.$execute['dept_code'].'</td>
                                               
                                            </tr>
                                            <tr class="odd gradeA">
                                                <td><b>Department Description<b/></td>
                                                <td>'.$execute['dept_description'].'</td>
                                               
                                            </tr>
                                           
										     <tr class="odd gradeA">
                                                <td><b>Created On<b/></td>
                                                <td>'.$execute['4'].' - '.$execute['5'].' - '. $execute['6'].'</td>
                                               
                                            </tr>
                                  
                                        </tbody>
                                    </table>
									
									</div>
                                   </div></div></div>
									
';									
}

function user_item_cluster()
{
	 $id=$_SESSION['user_id'];
  
                                                               //fetches department name
$query= "SELECT  department.dept_name, department.dept_id
           FROM department
		   INNER JOIN users
		   ON department.uId = users.uId
		   WHERE users.uId = ".$id."";
$fetch=mysql_query($query);
$execute=mysql_fetch_array($fetch);
  
                                                                 //fetches cluster in inventory
$query_cluster="  SELECT DISTINCT item_cluster.cluster_id , item_cluster.cluster_name, item_cluster.cluster_type,   item_cluster.cluster_description
                    FROM item_cluster
					
					INNER JOIN items
					ON item_cluster.cluster_id = items.cluster_id
					
					
					
					INNER JOIN item_code_mapping
					ON item_code_mapping.item_id = items.item_id
					
					WHERE  item_code_mapping.item_code NOT IN
					 (
					   SELECT item_code
					   FROM dept_items_mapping
					 )
												 
				  
					";  
$execute_cluster=mysql_query($query_cluster);
			                                                    //fetches cluster in dept
$query_dept_cluster=" SELECT DISTINCT item_cluster.cluster_id , item_cluster.cluster_name, item_cluster.cluster_type, item_cluster.cluster_description
                    FROM item_cluster
					
					INNER JOIN items
					ON item_cluster.cluster_id = items.cluster_id
					
					INNER JOIN dept_items_mapping
					ON items.item_id = dept_items_mapping.item_id
					
					WHERE dept_items_mapping.dept_id = ".$execute['dept_id']."
                    ";	
$fetch_dept_cluster=mysql_query($query_dept_cluster);
	
	
	echo'
	                      <!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                               
                                
                                <div class="widget-content">
                                    <div class="boxtitle">'.$execute['dept_name'].'</div>
									<div id="container" style="min-width: 400px; height:100%; margin: 0 auto">
	
	
	<div class="demo">
                                    <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
                                        <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                                            <li class="ui-state-default ui-corner-top"><a href="#tabs-2">Inventory</a></li>
                                            <li class="ui-state-default ui-corner-top ui-state-hover ui-tabs-selected ui-state-active"><a href="#tabs-1">Department</a></li>   ';  /*tab names*/   echo'
                                           
                                        </ul>
    
	
	
	  ';   /*Cluster Department*/ 
	  echo'                     
	      <div id="tabs-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
                                         
                    
                    	
                            
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Select Cluster in Department </span>
                                </div><!-- End widget-header -->	
                                
                              <div class="row-fluid">
                    
                                <div class="widget-content">
                                          
                                          <div id="UITab" style="position:relative;">
                                         
                                          <div class="tab_container" >
                    
                                              <div id="tab1" class="tab_content"> 
                                                   
                                                  <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                    <thead align="center">
                                                      <tr>
                                                <th>Cluster</th>
                                                <th>Description</th>
                                                <th>Type</th>
                                            </tr>
                                        </thead>
                                        <tbody align="center">';
					while($execute_dept_cluster=mysql_fetch_array($fetch_dept_cluster))					
                                     {                                                   //fetches cluster in dept
									 echo'                                             
										    <tr class="odd gradeX">
                                                <td><a href="user_view_item_department.php?id='.$execute_dept_cluster['cluster_id'].'&name='.$execute_dept_cluster['cluster_name'].'"><span style="color:blue">'.$execute_dept_cluster['cluster_name'].'</span></a></td>
                                                <td>'.$execute_dept_cluster['cluster_description'].'</td>
                                             '; 
											  if($execute_dept_cluster['cluster_type'] == 1)
											  {
											   echo'<td>Static Items</td>';
											  }
											  
											  else if($execute_dept_cluster['cluster_type'] == 0)
											  {
											   echo'<td>Frequently Used Items Items</td>';
											  }
                                           echo'
                                            </tr>
									'; }
                                    echo'       
                                        </tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div></div></div></div>
										
										
        
     ';   /*Cluster Inventory*/   echo'        
	                              <div id="tabs-2" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
                                                     
                     <div class="widget-header">
                                    <span><i class="icon-home"></i> Select Cluster in Inventory </span>
                                </div><!-- End widget-header -->
                    		
                            
                             <div class="row-fluid">
                    
                                <div class="widget-content">
                                 
                                                   
                                                  <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                    <thead align="center">
                                                      <tr>
                                                <th>Cluster</th>
                                                <th>Description</th>
                                                <th>Type</th>
                                            </tr>
                                        </thead>
										  <tbody align="center">';
										
	while($fetch_cluster=mysql_fetch_array($execute_cluster))
  {	                                                            //fetches clusters in inventory
	echo'										
	                                           <tr class="odd gradeX"> 
				
 
 				
                                                <td><a href="user_view_item_inventory.php?id='.$fetch_cluster['cluster_id'].'&name='.$fetch_cluster['cluster_name'].'"><span style="color:blue">'.$fetch_cluster['cluster_name'].'</span></a></td>
                                                <td>'.$fetch_cluster['cluster_description'].'</td>
                                             '; 
											  if($fetch_cluster['cluster_type'] == 1)
											  {
											   echo'<td>Static Items</td>';
											  }
											  
											  else if($fetch_cluster['cluster_type'] == 0)
											  {
											   echo'<td>Frequently Used Items Items</td>';
											  }
                                           echo'
                                            </tr> '; }
                                      echo'    
                                        </tbody>
                                    </table>
                                        
                                    </div>  </div>  </div>    
                                   

';
}


function user_view_item_inventory()
{
 $id=$_SESSION['user_id'];
  
                                                                   //fetches items in selected cluster
$query= "SELECT item_cluster.cluster_name, items.item_name, items.date_of_purchase, items.vendor_id, items.warranty, items.warranty_voids_on, items.quantity_left, items.cost,items.item_id
           FROM items
		   INNER JOIN item_cluster
		   ON items.cluster_id = item_cluster.cluster_id
		   WHERE item_cluster.cluster_id = ".$_GET['id']."";
  
  $execute=mysql_query($query);
 
 
echo'
	<!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Item Details </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <div class="boxtitle">'.$_GET['name'].'</div>
									<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto">

<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                              	
                                
                               <div class="widget-content">
                                          
                                          <div id="UITab" style="position:relative;">
                                         
                                          <div class="tab_container" >
                    
                                              <div id="tab1" class="tab_content"> 
                                                   
                                                  <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                    <thead align="center">
                                                      <tr>
                                                <th>Item</th>
                                                <th>Date of Purchase</th>
                                                <th>Vendor\'s Name</th>
												<th>Warranty</th>
                                                <th>Warranty Voids On</th>
                                                <th>Availability</th>
												<th>Cost</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody align="center">  ';
					while($fetch=mysql_fetch_array($execute))					  //fetches items in selected cluster
                                       { echo'  <tr class="odd gradeX">
                                                <td>'.$fetch['item_name'].'</td>
                                                <td>'.$fetch['date_of_purchase'].'</td>';
             $sql_vendor="SELECT vendor_name
			              FROM vendor_details
						  WHERE vendor_id= ".$fetch['vendor_id']." ";
			$execute_vendor=mysql_query($sql_vendor);
			$fetch_vendor=mysql_fetch_array($execute_vendor);			  
								  echo        ' <td>'.$fetch_vendor['vendor_name'].'</td>';
									if ($fetch['warranty'] == 1)
										{	
										echo	'<td>In Warranty</td>';    
										}
									else if ($fetch['warranty'] == 0)	
									     {	
										echo'	<td><span style="color:red;"  >Out Of Warranty</span></td>';    
										}
                                     echo'
									            <td>'.$fetch['warranty_voids_on'].'</td>
                                                <td><span style="color:blue"><a href="user_item_availability_inventory.php?id='.$fetch['item_id'].'&name='.$fetch['item_name'].'">'.$fetch['quantity_left'].'</a></span></td>
												<td>'.$fetch['cost'].'</td>
                                               
                                            </tr>   ';   }
											
                            echo'               
                                        </tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div>

									</div>
                                   </div></div></div>
									
';									
}

function user_item_availibility_inventory()
{
	                                                               //fetches department name
$query_dept= "SELECT  department.dept_name, department.dept_id
           FROM department
		   INNER JOIN users
		   ON department.uId = users.uId
		   WHERE users.uId = ".$_SESSION['user_id']."";
$fetch_dept=mysql_query($query_dept);
$execute_dept=mysql_fetch_array($fetch_dept);
	
	
	     $query="SELECT item_code
            FROM item_code_mapping
		    WHERE item_code NOT IN 
							  (
							  	SELECT item_code
								FROM dept_items_mapping
								WHERE item_id = ".$_GET['id']."
								
							  )
			AND item_id = ".$_GET['id']."				  
		   
		   ";
	 $execute=mysql_query($query);
 
 $no=1;
echo'
	<!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Item Details </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <div class="boxtitle">'.$_GET['name'].'</div>
									<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto">

<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                              	
                                
                               <div class="widget-content">
                                          
                                          <div id="UITab" style="position:relative;">
                                         
                                          <div class="tab_container" >
                    
                                              <div id="tab1" class="tab_content"> 
                                                   
                                                  <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                    <thead align="center">
                                                      <tr>
                                                <th>S.No.</th>
                                                <th>Item Code</th>
                                             </tr>
                                        </thead>
                                        <tbody align="center">  ';
					while($fetch=mysql_fetch_array($execute))					  //fetches items in selected cluster
                                       { echo'  <tr class="odd gradeX">
                                                <td>'.$no++.'</td>
                                                <td>'.$fetch['item_code'].'</td>
                                                </tr>   ';   }
											
                            echo'               
                                        </tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div>

									</div>
                                   </div></div></div>
									
';									   
		   
		   
		   
		   



} 

function user_view_item_department()
{
 $id=$_SESSION['user_id'];
  
                                                                   //fetches items in selected cluster
$query= "SELECT COUNT(dept_items_mapping.item_code) AS total, item_cluster.cluster_name, items.item_name, items.date_of_purchase, items.vendor_id, items.warranty, items.warranty_voids_on,  items.cost,items.item_id
           FROM items
		   
		   INNER JOIN item_cluster
		   ON items.cluster_id = item_cluster.cluster_id
		   
		   INNER JOIN dept_items_mapping
		   ON items.item_id = dept_items_mapping.item_id
		   
		   WHERE items.item_id = ".$_GET['id']."";
  
  $execute=mysql_query($query);
 
 
echo'
	<!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Item Details </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <div class="boxtitle">'.$_GET['name'].'</div>
									<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto">

<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                              	
                                
                               <div class="widget-content">
                                          
                                          <div id="UITab" style="position:relative;">
                                         
                                          <div class="tab_container" >
                    
                                              <div id="tab1" class="tab_content"> 
                                                   
                                                  <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                    <thead align="center">
                                                      <tr>
                                                <th>Item</th>
                                                <th>Date of Purchase</th>
                                                <th>Vendor\'s Name</th>
												<th>Warranty</th>
                                                <th>Warranty Voids On</th>
                                                <th>Availability</th>
												<th>Cost</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody align="center">  ';
					while($fetch=mysql_fetch_array($execute))					  //fetches items in selected cluster
                                       { echo'  <tr class="odd gradeX">
                                                <td>'.$fetch['item_name'].'</td>
                                                <td>'.$fetch['date_of_purchase'].'</td>';
             $sql_vendor="SELECT vendor_name
			              FROM vendor_details
						  WHERE vendor_id= ".$fetch['vendor_id']." ";
			$execute_vendor=mysql_query($sql_vendor);
			$fetch_vendor=mysql_fetch_array($execute_vendor);			  
								  echo        ' <td>'.$fetch_vendor['vendor_name'].'</td>';
									if ($fetch['warranty'] == 1)
										{	
										echo	'<td>In Warranty</td>';    
										}
									else if ($fetch['warranty'] == 0)	
									     {	
										echo'	<td><span style="color:red;"  >Out Of Warranty</span></td>';    
										}
                                     echo'
									            <td>'.$fetch['warranty_voids_on'].'</td>
                                                <td><span style="color:blue"><a href="user_item_availability_department.php?id='.$fetch['item_id'].'&name='.$fetch['item_name'].'">'.$fetch['total'].'</a></span></td>
												<td>'.$fetch['cost'].'</td>
                                               
                                            </tr>   ';   }
											
                            echo'               
                                        </tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div>

									</div>
                                   </div></div></div>
									
';									
}





	
	
function user_item_availibility_department()
{
	                                                               //fetches department name
$query_dept= "SELECT  department.dept_name, department.dept_id
           FROM department
		   INNER JOIN users
		   ON department.uId = users.uId
		   WHERE users.uId = ".$_SESSION['user_id']."";
$fetch_dept=mysql_query($query_dept);
$execute_dept=mysql_fetch_array($fetch_dept);
	
	
	     $query="SELECT item_code
                 FROM dept_items_mapping
		         WHERE  item_id = ".$_GET['id']."				  
		   
		   ";
	 $execute=mysql_query($query);
 
 $no=1;
echo'
	<!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Item Details </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <div class="boxtitle">'.$_GET['name'].'</div>
									<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto">

<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                              	
                                
                               <div class="widget-content">
                                          
                                          <div id="UITab" style="position:relative;">
                                         
                                          <div class="tab_container" >
                    
                                              <div id="tab1" class="tab_content"> 
                                                   
                                                  <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                    <thead align="center">
                                                      <tr>
                                                <th>S.No.</th>
                                                <th>Item Code</th>
                                             </tr>
                                        </thead>
                                        <tbody align="center">  ';
					while($fetch=mysql_fetch_array($execute))					  //fetches items in selected cluster
                                       { echo'  <tr class="odd gradeX">
                                                <td>'.$no++.'</td>
                                                <td>'.$fetch['item_code'].'</td>
                                                </tr>   ';   }
											
                            echo'               
                                        </tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div>

									</div>
                                   </div></div></div>
									
';									   
		   
		   
		   
		   



} 	



function user_item()
{
	 $id=$_SESSION['user_id'];
  
                                                               //fetches department name
$query= "SELECT  department.dept_name, department.dept_id
           FROM department
		   INNER JOIN users
		   ON department.uId = users.uId
		   WHERE users.uId = ".$id."";
$fetch=mysql_query($query);
$execute=mysql_fetch_array($fetch);
  
                                                                                //fetches inventory item detail
$query_Inventoryitemdetail = "SELECT DISTINCT item_code_mapping.item_code, items.item_name, items.item_details, items.item_id
                              FROM items
							  
							  INNER JOIN item_code_mapping
							  ON item_code_mapping.item_id = items.item_id
							 	
							  WHERE item_code_mapping.item_code NOT IN 
							  (
							  	SELECT item_code
								FROM dept_items_mapping
							  )
							  ";
$execute_Inventoryitemdetail = mysql_query($query_Inventoryitemdetail);
							
	
	
	                                                                          //fetches department item detail    
$query_departmentitemdetail = "SELECT items.item_name, items.item_details, items.item_id,dept_items_mapping.item_code
                                FROM items
								
                                INNER JOIN dept_items_mapping 
								ON dept_items_mapping.item_id = items.item_id
								
								WHERE dept_items_mapping.dept_id = ".$execute['dept_id']."
								ORDER BY items.item_name ASC 

							";
$execute_departmentitemdetail = mysql_query($query_departmentitemdetail);	
	
	echo'
	                      <!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                               
                                
                                <div class="widget-content">
                                    <div class="boxtitle">'.$execute['dept_name'].'</div>
									<div id="container" style="min-width: 400px; height:100%; margin: 0 auto">
	
	
	<div class="demo">
                                    <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
                                        <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                                            <li class="ui-state-default ui-corner-top"><a href="#tabs-2">Inventory</a></li>
                                            <li class="ui-state-default ui-corner-top ui-state-hover ui-tabs-selected ui-state-active"><a href="#tabs-1">Department</a></li>   ';  /*tab names*/   echo'
                                           
                                        </ul>
    
	
	
	  ';   /*Cluster Department*/ 
	  echo'                     
	      <div id="tabs-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
                                        
										   <div class="widget-header">
                                    <span><i class="icon-home"></i> Item Details Department </span>
                                </div><!-- End widget-header -->	
                                
                              <div class="row-fluid">
                    
                                <div class="widget-content">
                                          
                                          <div id="UITab" style="position:relative;">
                                         
                                          <div class="tab_container" >
                    
                                              <div id="tab1" class="tab_content"> 
                                                   
                                                  <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                    <thead align="center">
                                                      <tr>
                                                <th>Item Name</th>
												<th>Item Code</th>
                                                <th>Detail</th>
												<th>Request Service/Dump</th>
                                            </tr>
                                        </thead>
                                        <tbody align="center">';
				$ctr = 123456;						
					while($fetch_departmentitemdetail=mysql_fetch_array($execute_departmentitemdetail))					
                                     {                             //fetches department item detail     
									 echo'    
										    <tr class="odd gradeX">
                                                <td><a href="user_view_item_department.php?id='.$fetch_departmentitemdetail['item_id'].'&name='.$fetch_departmentitemdetail['item_name'].'"><span style="color:blue">'.$fetch_departmentitemdetail['item_name'].'</span></a></td>
												<td>'.$fetch_departmentitemdetail['item_code'].'</td>
                                                <td>'.$fetch_departmentitemdetail['item_details'].'</td>
                                             	<td id="'.++$ctr.'">';
												//select status to fetch request/cancel button
    		 $sql_status="SELECT MAX(user_request_status.status_code) AS status_code, user_request.status_id, user_request.request_id
			 FROM user_request_status
			
			 INNER JOIN user_request
			 ON user_request.status_id = user_request_status.status_id
			 
			 WHERE user_request.item_code= '".$fetch_departmentitemdetail['item_code']."'
			  AND user_request.user_id = $id
	         ";                                        
	  $execute_status=mysql_query($sql_status);
	  $fetch_status=mysql_fetch_array($execute_status);							
										
				
				        if(($fetch_status['status_code'] == NULL) || ($fetch_status['status_code'] == '3'))
		                 {	
			              echo'
													 <span class="tip" onClick="request_service(1,\''.$fetch_departmentitemdetail['item_code'].'\','.$ctr.');" title="service request"><img src="../images/icon/color_18/hammer.png" class="iconBox color"  alt="hammer" style="width:18px" height="18px"/></span>
													 <span class="tip" onClick="request_dumping(1,\''.$fetch_departmentitemdetail['item_code'].'\','.$ctr.');"  title="Dumping Request"><img src="../images/icon/color_18/trash_can.png" class="iconBox color" alt="trash_can" style="width:18px" height="18px onClick="request_dumping(1,\''.$fetch_departmentitemdetail['item_code'].'\','.$ctr.');">';
													 
		                   }   //closed if
		  
		 				 else if ( $fetch_status['status_code'] == 0 && $fetch_status['request_id'] == 2)
						  {	
						   echo'
													 <input type="button" class="uibutton special" value="Cancel service request" onClick="request_service(0,'.$fetch_status['status_id'].','.$ctr.');"></td>';
		                   }  
						    else if ( $fetch_status['status_code'] == 1 && $fetch_status['request_id'] == 2)
		                   {
		                    echo'
					                     REQUEST ACCEPTED 
										 <br/>
										 <input type="button" value="Acknowledge" id="ack" onclick="acknowledge_request('.$ctr.','.$fetch_status['status_id'].');" ></td>';                        
														 
		  
		  
		  }
	    						
		  else if ( $fetch_status['status_code'] == 2 && $fetch_status['request_id'] == 2)
		  {
		                     echo'         REQUEST ACKNOWLEDGED </td>';									    
		  }
						    //closed else if
						  
						   else if(($fetch_status['status_code'] == 0) && ($fetch_status['request_id'] == 3))
						   {	
						   echo'
													 <input type="button" class="uibutton special" value="Cancel dumping request" onClick="request_dumping(0,'.$fetch_status['status_id'].','.$ctr.');"></td>';
		                   }   //closed else if 
	
	      else if ( $fetch_status['status_code'] == 1 && $fetch_status['request_id'] == 3)
		  {
		            echo'
					                     REQUEST ACCEPTED 
										 <br/>
										 <input type="button" value="Acknowledge" id="ack" onclick="acknowledge_request('.$ctr.','.$fetch_status['status_id'].');" ></td>';                        
														 
		  
		  
		  }
	    						
		  else if ( $fetch_status['status_code'] == 2 && $fetch_status['request_id'] == 3)
		  {
		            echo'         REQUEST ACKNOWLEDGED </td>';									    
		  }
	
	
	
	
                              echo'    </tr> ';
	               
					}   //closed while loop
                              echo'       
                                        </tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div>  </div>  </div>
										
										</div>
        
     ';   /*Cluster Inventory*/   echo'        
	                             <div id="tabs-2" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
                                           <div class="widget-header">
                                    <span><i class="icon-home"></i> Item Details Inventory </span>
                                </div><!-- End widget-header -->
                    		
                            
                             <div class="row-fluid">
                    
                                <div class="widget-content">
                                 
                                                   
                                                  <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                    <thead align="center">
                                                      <tr>
                                                <th>Item Name</th>
												 <th>Item Code</th>
                                                <th>Detail</th>
												<th>Request Item</th>
                                            </tr>
                                        </thead>
                                        <tbody align="center">';
		$ctr = 123456;								
	while($fetch_Inventoryitemdetail=mysql_fetch_array($execute_Inventoryitemdetail))
    {	
		echo'										
	                                           <tr class="odd gradeX"> 
				                                <td><a href="user_view_item_inventory.php?id='.$fetch_Inventoryitemdetail['item_id'].'&name='.$fetch_Inventoryitemdetail['item_name'].'"><span style="color:blue">'.$fetch_Inventoryitemdetail['item_name'].'</a></span></td>
                                                <td>'.$fetch_Inventoryitemdetail['item_code'].'</td>
										        <td>'.$fetch_Inventoryitemdetail['item_details'].'</td>
												<td id="'.++$ctr.'">';
     
	 
 $sql_status="SELECT MAX(user_request_status.status_code) AS status_code, user_request.status_id
			 FROM user_request_status
			
			 INNER JOIN user_request
			 ON user_request.status_id = user_request_status.status_id
			 
			 WHERE user_request.item_code= '".$fetch_Inventoryitemdetail['item_code']."' 
	         ";                                        
	  $execute_status=mysql_query($sql_status);
	  $fetch_status=mysql_fetch_array($execute_status);
	 
	 
	
	
	
		  if(($fetch_status['status_code'] == NULL) || ($fetch_status['status_code'] == '3'))
		  {	
			  echo'
													  <input type="button" class="uibutton submit_form" value="Request Item" onClick="change_button(1,\''.$fetch_Inventoryitemdetail['item_code'].'\','.$ctr.');"></td>';
		  }
		  
		  else if ( $fetch_status['status_code'] == 0)
		  {	
				  echo'
													 <input type="button" class="uibutton special" value="Cancel Item" onClick="change_button(0,'.$fetch_status['status_id'].','.$ctr.');"></td>';
		  }
		  
		   else if ( $fetch_status['status_code'] == 1)
		  {
		            echo'
					                     REQUEST ACCEPTED 
										 <br/>
										 <input type="button" value="Acknowledge" id="ack" onclick="acknowledge_request('.$ctr.','.$fetch_status['status_id'].');" ></td>';                        
														 
		  
		  
		  }
	    						
		  else if ( $fetch_status['status_code'] == 2)
		  {
		            echo'         REQUEST ACKNOWLEDGED </td>';									    
		  }
	
echo'                                            </tr> ';
	}
                                      echo'    
                                        </tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div>
                                        </div>
                                        
                                    </div>
                                    </div>
									
';
}



function user_view_item_details()
{
 
  
                                                                   //fetches items in selected cluster
$query= "SELECT  item_name,date_of_purchase,vendor_id, warranty, warranty_voids_on, quantity_left,cost
           FROM items
		   
		   WHERE item_id = ".$_GET['id']."";
  
  $execute=mysql_query($query);
 
 
echo'
	<!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Item Details </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <div class="boxtitle"></div>
									<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto">

<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                              	
                                
                               <div class="widget-content">
                                          
                                          <div id="UITab" style="position:relative;">
                                         
                                          <div class="tab_container" >
                    
                                              <div id="tab1" class="tab_content"> 
                                                   
                                                  <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                    <thead align="center">
                                                      <tr>
                                                <th>Item</th>
                                                <th>Date of Purchase</th>
                                                <th>Vendor\'s Name</th>
												<th>Warranty</th>
                                                <th>Warranty Voids On</th>
                                                <th>Quantity</th>
												<th>Cost</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody align="center">  ';
					while($fetch=mysql_fetch_array($execute))					  //fetches items in selected cluster
                                       { echo'  <tr class="odd gradeX">
                                                <td>'.$fetch['item_name'].'</td>
                                                <td>'.$fetch['date_of_purchase'].'</td>';
             $sql_vendor="SELECT vendor_name
			              FROM vendor_details
						  WHERE vendor_id= ".$fetch['vendor_id']." ";
			$execute_vendor=mysql_query($sql_vendor);
			$fetch_vendor=mysql_fetch_array($execute_vendor);			  
								  echo        ' <td>'.$fetch_vendor['vendor_name'].'</td>';
									if ($fetch['warranty'] == 1)
										{	
										echo	'<td>In Warranty</td>';    
										}
									else if ($fetch['warranty'] == 0)	
									     {	
										echo'	<td><span style="color:red;"  >Out Of Warranty</span></td>';    
										}
                                     echo'
									            <td>'.$fetch['warranty_voids_on'].'</td>
                                                <td>'.$fetch['quantity_left'].'</td>
												<td>'.$fetch['cost'].'</td>
                                               
                                            </tr>   ';   }
											
                            echo'               
                                        </tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div>

									</div>
                                   </div></div></div>
									
';									
}


function generate_request()
{
		 $id=$_SESSION['user_id'];
  
                                                               //fetches department name
$query= "SELECT  department.dept_name, department.dept_id
		 FROM department
		 INNER JOIN users
		 ON department.uId = users.uId
		 WHERE users.uId = ".$id."";
$fetch=mysql_query($query);
$execute=mysql_fetch_array($fetch);
  
                                                                                //fetches inventory item detail
$query_Inventoryitemdetail = " SELECT items.item_name, items.item_details, items.item_id, item_code_mapping.item_code
                              FROM items
							  
							  INNER JOIN item_code_mapping 
							  ON item_code_mapping.item_id = items.item_id
							  
							  WHERE item_code_mapping.item_code NOT IN 
							  (
							  	SELECT item_code
								FROM dept_items_mapping
								WHERE dept_id = ".$execute['dept_id']."
							  )
							  ORDER BY items.item_name ASC
				            ";
$execute_Inventoryitemdetail = mysql_query($query_Inventoryitemdetail);
	
	echo' 
	                     <!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Generate Request </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <div class="boxtitle">'.$execute['dept_name'].'</div>
									<div id="container" style="min-width: 400px; height:100%; margin: 0 auto">

  	           
                                          <div class="row-fluid">
                    
                                <div class="widget-content">
                                          
                                          <div id="UITab" style="position:relative;">
                                         
                                          <div class="tab_container" >
                    
                                              <div id="tab1" class="tab_content"> 
                                                   
                                                  <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                    <thead align="center">
                                                      <tr>
                                                <th>Item Name</th>
												<th>Item Code</th>
                                                <th>Detail</th>
												<th>Request Item</th>
                                            </tr>
                                        </thead>
                                        <tbody align="center">';
		
		$ctr = 123456;								
	while($fetch_Inventoryitemdetail=mysql_fetch_array($execute_Inventoryitemdetail))
    {	                                                                             //fetches inventory item detail
		echo'										
	                                           <tr class="odd gradeX"> 
				                                <td>'.$fetch_Inventoryitemdetail['item_name'].'</td>
												<td>'.$fetch_Inventoryitemdetail['item_code'].'</td>
                                                <td>'.$fetch_Inventoryitemdetail['item_details'].'</td>
												<td id="'.++$ctr.'">';
     
	                                                                       //selects status to fetch request/cancel button
 $sql_status="SELECT user_request_status.status_code, user_request.status_id
			 FROM user_request_status
			
			 INNER JOIN user_request
			 ON user_request.status_id = user_request_status.status_id
			 
			 WHERE user_request.item_code= '".$fetch_Inventoryitemdetail['item_code']."' 
			 AND user_request.user_id = $id
			 
			 ORDER BY user_request_status.timestamp DESC
	         ";                                        
$execute_status=mysql_query($sql_status);
$fetch_status=mysql_fetch_array($execute_status);
	 
	
	
		  if(($fetch_status['status_code'] == NULL) || ($fetch_status['status_code'] == '3'))
		  {	
			        echo'
													  <input type="button" class="uibutton submit_form" value="Request Item" onClick="change_button(1,\''.$fetch_Inventoryitemdetail['item_code'].'\','.$ctr.');">';
		  }
		  
		  else if ( $fetch_status['status_code'] == 0)
		  {	
				    echo'
													 <input type="button" class="uibutton special" value="Cancel Item" onClick="change_button(0,'.$fetch_status['status_id'].','.$ctr.');"></td>';
		  }
		  
	      else if ( $fetch_status['status_code'] == 1)
		  {
		            echo'
					                     REQUEST ACCEPTED 
										 <br/>
										 <input type="button" value="Acknowledge" id="ack" onclick="acknowledge_request('.$ctr.','.$fetch_status['status_id'].');" ></td>';                        
														 
		  
		  
		  }
	    						
		  else if ( $fetch_status['status_code'] == 2)
		  {
		            echo'         REQUEST ACKNOWLEDGED </td>';									    
		  }
	  
	 
	             
	  
echo'                                            </tr> ';
	}
                                      echo'    
                                        </tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div>
                                        </div>
                                        
                                    </div>
                                    </div>
									 </div></div></div>
';
}

function view_status()
{
	
	   
$id=$_SESSION['user_id'];
 $no=1; 
                                                                     //fetches dept name
$query= "SELECT  department.dept_name 
           FROM department
		   INNER JOIN users
		   ON department.uId = users.uId
		   WHERE users.uId = ".$id."";
  
  $fetch=mysql_query($query);
  $execute=mysql_fetch_array($fetch);
  
                                         //fetches request table
    $sql_code="SELECT  DISTINCT user_request.item_code,  user_request.request_id, user_request.status_id
	      FROM user_request
		 
		  WHERE   user_request.user_id = '".$id."'
		  
		  ORDER BY user_request.timestamp DESC
		";
$execute_status=mysql_query($sql_code);




 
echo'
	<!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Request Status </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <div class="boxtitle">'.$execute['dept_name'].'</strong></div>
									<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto">

<div class="row-fluid">
                    
                                <div class="widget-content">
                                          
                                          <div id="UITab" style="position:relative;">
                                         
                                          <div class="tab_container" >
                    
                                              <div id="tab1" class="tab_content"> 
                                                   
                                                  <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                    <thead align="center">
                                                      <tr>';
                                 
				                     echo'      <th>S.No.</th>
											    <th>Item Name</th>
												<th>Item Code</th>
											    <th>Request Id</th>
                                                <th>Status</th> 
										       
                                        </tr>
                                        </thead>
                                        <tbody align="center"> ';
									
                            while($fetch_status=mysql_fetch_array($execute_status))
							 {                                       //fetches status_code
							   $query_status="SELECT MAX(status_code) AS status_code
                                              From user_request_status
			                                  WHERE status_id = ".$fetch_status['status_id']."
											  ORDER BY timestamp DESC  
											  ";
                               $execute_query_status=mysql_query($query_status);
							    $fetch_query_status=mysql_fetch_array($execute_query_status);  
								      //fetches item name
	$sql="SELECT DISTINCT items.item_name
          FROM items
	  
	      INNER JOIN item_code_mapping
	      ON items.item_id = item_code_mapping.item_id
	  
	      WHERE item_code_mapping.item_code = '".$fetch_status['item_code']."'
          ";
$sql_item_query=mysql_query($sql);
$sql_item_execute=mysql_fetch_array($sql_item_query);  

							
				                     echo'      <tr class="odd gradeX">
                                                <td>'.$no.'</td> ';
												
				
										
										echo'		
                                                <td>'.$sql_item_execute['item_name'].'</td>
												<td>'.$fetch_status['item_code'].'</td>  '; 
                     if($fetch_status['request_id'] == 0)
					                              echo  '<td><span style="color:red;"  >New Item</span></td> ';
		             
		             if($fetch_status['request_id'] == 2)
					                              echo  '<td><span style="color:orange;"  >Service</span></td> ';
		             if($fetch_status['request_id'] == 3)
					                              echo  '<td><span style="color:green;"  >Dumping</span></td> ';
												  
												  
					if($fetch_query_status['status_code'] == 0)
											        echo'<td> REQUEST PENDING </td>';
					if($fetch_query_status['status_code'] == 1)						
												    echo'<td id='.$no.'> REQUEST ACCEPTED 
													     <br/>
													      <input type="button" value="Acknowledge" id="ack" onclick="acknowledge_request('.$no.','.$fetch_status['status_id'].');" >                        
														  </td>
													
													';
					if($fetch_query_status['status_code'] == 2)						
												    echo'<td> REQUEST ACKNOWLEDGED </td>';
                                         echo'   </tr>    '; ++$no;	 }
							        echo'				
                                         
                                     </tbody>
                                                  </table>
                                              </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->


                    </div>	
									</div>
                                   </div></div></div>
									
';		
						
}



function pending_request()
{
$id=$_SESSION['user_id'];
 $no=1; 
                                                                     //fetches dept name
$query= "SELECT  department.dept_name 
           FROM department
		   INNER JOIN users
		   ON department.uId = users.uId
		   WHERE users.uId = ".$id."";
  
  $fetch=mysql_query($query);
  $execute=mysql_fetch_array($fetch);
  
                                         //fetches request table
   $sql="SELECT   DISTINCT items.item_name,  user_request.request_id, user_request.item_code
                      FROM user_request
                                  
                                  INNER JOIN item_code_mapping
                                  ON item_code_mapping.item_code = user_request.item_code
                                  
                                  INNER JOIN items
                                  ON items.item_id = item_code_mapping.item_id
                                  
                                  WHERE user_request.user_id = ".$id."
                                  ORDER BY user_request.timestamp ASC

";
	$execute_status=mysql_query($sql);
	 
 
echo'
	<!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Pending Requests </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <div class="boxtitle">'.$execute['dept_name'].'</strong></div>
									<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto">

<div class="row-fluid">
                    
                                <div class="widget-content">
                                          
                                          <div id="UITab" style="position:relative;">
                                         
                                          <div class="tab_container" >
                    
                                              <div id="tab1" class="tab_content"> 
                                                   
                                                  <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                    <thead align="center">
                                                      <tr>
                                               <th>S.No.</th>
											   <th>Item Name</th>
											    <th>Item Code</th>
											    <th>Request Id</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody align="center"> ';
				while($fetch_status=mysql_fetch_array($execute_status))
				  {						                                     //fetches request table
                                     echo'      <tr class="odd gradeX">
                                                <td>'.$no++.'</td>
                                                <td>'.$fetch_status['item_name'].'</td>
   											    <td>'.$fetch_status['item_code'].'</td>';
                   if($fetch_status['request_id'] == 0)                 
									echo'       <td><span style="color:red;"  >New Item</span></td> ';
			       
				   if($fetch_status['request_id'] == 2)                 
									echo'       <td><span style="color:orange;"  >Service</span></td> ';
				   if($fetch_status['request_id'] == 3)                 
									echo'       <td><span style="color:green;"  >Dumping</span></td> ';									
                                         echo'   </tr>    ';  }
							        echo'				
                                         
                                     
                                                      </tr>';
												
													  echo '
												   
                                                    </tbody>
                                                  </table>
                                              </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->


                    </div>	
									</div>
                                   </div></div></div>
								   
									
';									
}




function admin_create_cluster()
{
	
echo'
              <div class="row-fluid">
                    
                    		<!-- Widget -->
                            <div class="widget  span12 clearfix">
                                <div class="widget-header">
                                    <span> CLUSTERS</span>
                                </div><!-- End widget-header -->	
                                <div class="widget-content">
                                
                                <div id="UITab" class="clearfix" style="position:relative;">
                                    <ul class="tabs">
                                        <li style="display: list-item;" class="active"><a href="#tab1"> Create Cluster </a></li>';  /*cluster creation page tab*/ echo'
                                        <li style="display: list-item;" class=""><a href="#tab2"> View Clusters </a></li> '; /*cluster view page tab*/ echo'           
                                    </ul>
        
                                <div class="tab_container">
        
                                  <div id="tab1" class="tab_content" style="display: block;"> 
                                      <div class="load_page">
                                        <div class="formEl_b">
                                       <div class="formEl_b">';
				if (isset($_GET['cluster_created']))
				{
					echo "<h3 align=\"center\">CLUSTER ADDED SUCCESSFULLY!</h3>";
				}
				if(isset($_GET['error_creating_cluster']))
				{
					echo "<h3 align=\"center\">ERROR ADDING CLUSTER!!</h3>";
				}   	
                                  echo'
								        <form id="" method="post" action="processing/creating_cluster.php"> 
                                        <fieldset>
                                        <legend>Create Cluster</legend>
                                              <div class="section ">
                                              <label> Cluster Name</label>   
                                              <div> 
                                              <input type="text" class="" name="cluster_name" id="cluster_name">
                                              </div>
                                             </div>
											 
                                              <div class="section ">
                                              <label> Cluster Description</label>   
                                              <div> 
                                              <textarea rows="5" column="80" class="" name="cluster_description" id="cluster_description"></textarea>
                                              </div>
                                             </div>
											  
                                             
                                    <div class="section">
                                      <label>Cluster Type</label>   
                                      <div>
                                          <select  name="cluster_type" id="cluster_type" class="" style="display: none; padding: 6px;">
                                          <option value="">SELECT</option> 
                                          <option value="0">Frequently Used Items</option> 
                                          <option value="1">Static Items</option> 
                                          </select>
                                           
                                </div>
                                </div>
                                            
                                              <div class="section last">
                                              <div>
                                                <input type="submit" value="Create Cluster" class="btn submit_form">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a class="btn" onclick="ResetForm()" title="Reset  Form">Cancel</a>
                                             </div>
                                             </div>
                                        </fieldset>
                                        </form>
        
                                        </div>
                                        </div>
        
                                      </div>	
                                  </div><!--tab1-->
                                  
                                  
                                  <div id="tab2" class="tab_content" style="display: none;"> 
                                      <div class="load_page">';
                                      
                                                  
     

  
                                                                 //fetches cluster in inventory
$query_cluster="  SELECT  cluster_id, cluster_name, cluster_type, cluster_description
                    FROM item_cluster
					";  
$execute_cluster=mysql_query($query_cluster);
			                                                  
$sno=1;
	
	
	echo'
	                      <!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                               
                                
                                <div class="widget-content">
                                   
									<div id="container" style="min-width: 400px; height:100%; margin: 0 auto">
	
	
	<div class="demo">';
           
                    	if (isset($_GET['deleted_cluster']))
				{
					echo "<h3 align=\"center\">CLUSTER DELETED SUCCESSFULLY!</h3>";
				}
				if(isset($_GET['error_deleting_cluster']))
				{
					echo "<h3 align=\"center\">ERROR DELETING CLUSTER!!</h3>";
				}   	
                    		
                       echo'     
                             <div class="row-fluid">
                    
                                <div class="widget-content">
                                 
                                                   
                                                  <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                    <thead align="center">
                                                      <tr>
                                                <th>Cluster</th>
                                                <th>Description</th>
                                                <th>Type</th>
												<th>Actions</th>
                                            </tr>
                                        </thead>
										  <tbody align="center">';
										
	while($fetch_cluster=mysql_fetch_array($execute_cluster))
  {	$d=$sno;
                                                           //fetches cluster in inventory
	echo'										
	                                           <tr class="odd gradeX" id='.$d.'> 
				
 
 				
                                                <td>'.$fetch_cluster['cluster_name'].'</td>
                                                <td>'.$fetch_cluster['cluster_description'].'</td>
                                             '; 
											  if($fetch_cluster['cluster_type'] == 1)
											  {
											   echo'<td>Static Items</td>';
											  }
											  
											  else if($fetch_cluster['cluster_type'] == 0)
											  {
											   echo'<td>Frequently Used Items</td>';
											  }
                                           echo'
                                        
											<td >
                                                        
                                                     <span class="tip" onclick="edit_cluster(\''.$fetch_cluster[0].'\',\''.$fetch_cluster[1].'\',\''.$fetch_cluster[2].'\',\''.$fetch_cluster[3].'\',\''.$sno++.'\')" ><img src="../images/icon/icon_edit.png" ></span> 
       <span class="tip"><a href="processing/deleting_cluster.php?id='.$fetch_cluster['cluster_id'].'" class="Delete" name="delete name" title="Delete"><img src="../images/icon/icon_delete.png"></a></span> 
                                                         </td>'; }
                                      echo'      </tr>   
                                        </tbody>
                                    </table>
                                        
                                    </div>  </div>  </div>    	';
}



function reset_password()
{
	
	 $id=$_SESSION['user_id'];
  
                                                               //fetches department name
$query= "SELECT  department.dept_name, department.dept_id
           FROM department
		   INNER JOIN users
		   ON department.uId = users.uId
		   WHERE users.uId = ".$id."";
$fetch=mysql_query($query);
$execute=mysql_fetch_array($fetch);
  
	
echo '	<!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> CHANGE PASSWORD</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <div class="boxtitle">'.$execute['dept_name'].' </div>
									<div  align="center" id="container" style="min-width: 400px; height: 400px; margin: 0 auto">';
                                 
				 	if (isset($_GET['password_changed']))
				{
					echo "<h4 align=\"center\">PASSWORD CHANGED SUCCESSFULLY!</h4>";
				}
				if(isset($_GET['error_changing_password']))
				{
					echo "<h4 align=\"center\">ERROR!!</h4>";
				}   	
								 
								 echo'
								 <div class="">
                                     <form name="password" id="validation_demo" action="processing/resetting_password.php" method="post">       
                                       <table>
                                        <tr>
										<td><h4>OLD PASSWORD &nbsp;<span style="color:red;">*</span></h4></td>
										<td><input type="password"  name="old_password" class="validate[required]" id="old_password"></td>
                                         </tr>     
                                         <tr>   
                                          <td> <h4>NEW PASSWORD&nbsp;<span style="color:red;">*</span></h4></td>   
										  <td><input type="password"  name="new_password" class="validate[required]" id="new_password"></td>
                                          </tr>
										  <tr>   
                                          <td> <h4>CONFIRM NEW PASSWORD&nbsp;<span style="color:red;">*</span>&nbsp;&nbsp;&nbsp;&nbsp;</h4></td>    
										  <td><input type="password" class="validate[required,equals[new_password]]" name="passwordCon" id="passwordCon"></td>
										  </tr>
                                           <tr>          
                                           <td><input type="hidden" value="'.$id.'" name="id"></td> 
										   <td> <input type="submit" value="submit" onClick="validate_password();">&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="cancel"></td>
                                            </table>
											  </form>
											  </div>
  </div></div></div></div>
                                          ';
}

?>
