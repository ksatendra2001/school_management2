<?php
//function to call admin_create_session.php
function admin_create_session()
{
echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>Create Session</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	
	';		
	
	if (isset($_GET['Successfully_Insert']))
		  {
			  echo "<h4 style=\"color:green\"align=\"center\">Successfully Added!</h4>";
		  }
	if(isset($_GET['error_adding_session']))
				{
					echo "<h5 style=\"color:red\"align=\"center\">SESSION ALREADY EXIST!!</h5>";
				}
	
	  echo'
	  <form id="validation_demo" action="session/admin_insert_session_details.php" method="post">              			 
  <div class="section ">
  <label> Session Name<small></small></label>   
   <div> 
  <input type="text" class="validate[required] small" name="session_name" />
    </div>
    </div>	
	<div class="section">
	<label>Start Date<small></small></label>   
	<div><input type="text"  id="date_day" class=" birthday  small " name="start_date" required="required"/></div>
	 </div>	
	 <div class="section">
	 <label> End Date </label>   
	 <div><input type="text"  id="datepick" class="datepicker  small" readonly="readonly" name="end_date"  />
	 </div>
     </div>
	 <div class="section ">
 
   <label>Current Session <small>(Make this session as the current session)</small></label>   
 <div> 
  <div>
 <input type="radio" name="current_session" id="radio-1" value="1"  class="ck"/>
  <label for="radio-1">YES</label>
  </div>
  <div>
  <input type="radio" name="current_session" id="radio-2" value="0"  class="ck"  />
  <label for="radio-2" >NO</label>
    </div>
    </div>
    </div>

<div class="section last">
 <div><button class="uibutton submit" rel="1" type="submit"  >Create Session</button>
 <button class="uibutton special"type="reset"  >Reset</button>   
</div>
</div>									
</form>  
';
echo'                              
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';			
	
}
//function to call admin_view_session
function  admin_view_session()
{
echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View Session Details</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	
	';		
if (isset($_GET['update_successfully']))
		  {
			  echo "<h4 style=\"color:green\"align=\"center\">Successfully Update!</h4>";
		  }
		  
		  if (isset($_GET['deleted_session']))
		  {
			  echo "<h4 style=\"color:green\"align=\"center\">Successfully Deleted!</h4>";
		  }
//query to get session details fropm session tables
$get_session= 
"SELECT `session_name`,`session_start_date`,`session_end_date`,`current_session`,`sId`
 FROM `session_table` ";
$exe_session = mysql_query($get_session);
 
$sno=0;
echo'
 <table  class="table table-bordered table-striped" id="dataTable" >                      					
					       <thead>
						    <tr>
							<th>S.No</th>
							<th>Session Name</th>
							<th>Session Start date</th>
							<th>Session End date</th>
							<th>Actions</th>
						    </tr>
					        </thead>
						<tbody align="center">';
						$c = '';
					while($fetch_session=mysql_fetch_array($exe_session))
				   {      $sid=$fetch_session['sId'];
					   if($fetch_session[3] == 1)
					   {
						$c = '<b style="color:green">current session</b>' ;   
					   }
					   else
					   {
						$c = "";   
					   }
					 
						  ++$sno;    
					 echo'	
					        <tr>
							<td>'.$sno.'</td>
							<td>'.$fetch_session['session_name'].'<br>'.$c.'</td>
							<td>'.$fetch_session['session_start_date'].'</td>
							<td>'.$fetch_session['session_end_date'].'</td>
							<td>
							
<span class="tip"><a href="admin_edit_session.php?sid='.$sid.'" class="table-icon edit" title="Edit"> <img src="images/icon/icon_edit.png"/></a></span>
								
<span class="tip"><a href="session/delete_session.php?sid='.$sid.'" class="table-icon delete" title="Delete"><img src="images/icon/icon_delete.png"/></a></span>
					     
							</td>
						    </tr>
					';
					}
					

echo'   </tbody>
        </table>                           
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';				
}



//function to call admin_edit_session.php
function  admin_edit_session()
{
	echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>Edit Session Details</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	
	';		
	
	
	
	//query to get session details fropm session tables
 $get_session= 
  "SELECT `session_name`,`session_start_date`,`session_end_date`,`current_session`
   FROM `session_table`
   WHERE `sId`=".$_GET['sid']." ";
$exe_session = mysql_query($get_session);
$fetch_session=mysql_fetch_array($exe_session);
$sname=$fetch_session['session_name'];
$start_date=$fetch_session['session_start_date'];
$end_date=$fetch_session['session_end_date'];
$current_session=$fetch_session['current_session'];

echo'
	<form action="session/update_session.php" method="post" enctype="multipart/form-data">	
	<input type="hidden" name="sid" value="'.$_GET['sid'].'"/>			
    <div class="section ">
    <label> Session Name<small></small></label>   
    <div> 
    <input type="text" class="validate[required] small" name="session_name" value="'.$sname.'" />
    </div>
    </div>	
	<div class="section">
	<label>Start Date<small></small></label>   
	<div><input type="text"  id="date_day" class=" birthday  small " name="start_date" required="required" value="'.$start_date.'"/></div>
	 </div>	
	 <div class="section">
	 <label> End Date </label>   
    <div>
    <input type="text"  id="datepick" class="datepicker  small" readonly="readonly" name="end_date" value="'.$end_date.'" />
	 </div>
     </div>
	 <div class="section ">
    
	 ';
	 if($current_session==1)
	 {
	 echo' 
	 <div>
 <input type="text" value="Current session" readonly="readonly" class="small"/>
 <input type="hidden" value="1" name="current_session"/>
  </div>
   </div>
   ';
	 }
   if($current_session==0)
   {
  echo'
   <label>Current Session <small>(Make this session as the current session)</small></label>  
	 <div> 
  <div>
   <input type="radio" name="current_session" id="radio-1" value="1"  class="ck"/>
  <label for="radio-1">YES</label>
  </div>
  <div>
  <input type="radio" name="current_session" id="radio-2" value="0"  class="ck"   checked="checked"/>
  <label for="radio-2" >NO</label>
  
    </div>
	</div>
	
   ';
   }
   echo' 
   </div>
   <div class="section last">
 <div><button class="uibutton submit" type="submit"  >Update Session</button>
 <button class="uibutton special"type="reset" >Reset</button>   
</div>
</div>									
</form> ';
	
echo'                          
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';				

	
}

//function to call manager_view_session.php
function  manager_view_session()
{
	
	
echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>View Session Details</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	
	';		
if (isset($_GET['update_successfully']))
		  {
			  echo "<h4 style=\"color:green\"align=\"center\">Successfully Update!</h4>";
		  }
//query to get session details fropm session tables
$get_session= 
"SELECT `session_name`,`session_start_date`,`session_end_date`,`current_session`,`sId`
 FROM `session_table` ";
$exe_session = mysql_query($get_session);
 
$sno=0;
echo'
 <table  class="table table-bordered table-striped" id="dataTable" >                      					
					       <thead>
						    <tr>
							<th>S.No</th>
							<th>Session Name</th>
							<th>Session Start date</th>
							<th>Session End date</th>
							
						    </tr>
					        </thead>
						<tbody align="center">';
						$c = '';
					while($fetch_session=mysql_fetch_array($exe_session))
				   {      $sid=$fetch_session['sId'];
					   if($fetch_session[3] == 1)
					   {
						$c = '<b style="color:green">current session</b>' ;   
					   }
					   else
					   {
						$c = "";   
					   }
					 
						  ++$sno;    
					 echo'	
					        <tr>
							<td>'.$sno.'</td>
							<td>'.$fetch_session['session_name'].'<br>'.$c.'</td>
							<td>'.$fetch_session['session_start_date'].'</td>
							<td>'.$fetch_session['session_end_date'].'</td>
							
						    </tr>
					';
					}
					

echo'   </tbody>
        </table>                           
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';					
	
	
	
}
?>