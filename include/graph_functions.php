<?php

function Show_graph()		
{
	 echo '<div class="row-fluid">					
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Graph for Admission</span>
      </div><!-- End widget-header -->	
      <div class="widget-content"><br />
      <div class="row-fluid"> 		  
	';			
	
	
	  $time_offset ="525"; // Change this to your time zone
	    $time_a = ($time_offset * 120);
	    $today = date("jS F Y"); 
	  echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	
	
	
	
	
	echo"

		<script type='text/javascript'>
		
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'column',
                margin: [ 50, 50, 100, 80]
            },
            title: {
                text: 'Admission Graph 2013'
            },
            xAxis: {
                categories: [
                   '1990',
				   '1991',
				   '1992',
				   '1993',
				   '1994',
                    '1995',
                    '1996',
                    '1997',
                    '1998',
                    '1999',
                    '2000',
                    '2001',
                    '2002',
                    '2003',
                    '2004',
                    '2005',
                    '2006',
                    '2007',
                    '2008',
                    '2009',
                    '2010',
					'2011',
					'2012',
					'2013',
                ],
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'No Of Student'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        'Student Admission: '+ Highcharts.numberFormat(this.y, 1) +
                        ' ';
                }
            },
                series: [{
                name: 'Admission',
                data: [200, 250, 310, 400, 480, 580, 650,  690,
                      760, 870,  970, 1020, 1100, 1190, 1270,1350,1490,1600,1650,1700,1800,1870,1950,2050],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    x: -3,
                    y: 200,
                    formatter: function() {
                        return this.y;
                    },
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    });
    
});
		</script>
	";

echo'
<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>

';
	
	
	
	
	
	
	
	 echo'	                                  
     </div><!-- row-fluid column-->
     </div><!--  end widget-content -->
     </div><!-- widget  span12 clearfix-->
     </div><!-- row-fluid -->
';			
	
	
	
	
	
	
	
	
	
	
	
	
	}

?>