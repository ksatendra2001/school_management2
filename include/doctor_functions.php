<?php
//priv admin
//function to call By doctor_add_doctor_details.php
function doctor_add_details()//add doctor details
{  //Author :Manish mishra
	//query to get doctor details
	 $query_get_doctor_details = "
		SELECT users.Name, users.uId,users.Email,users.image
		FROM users
		
		INNER JOIN `login`
		ON login.lId = users.uId
		
		WHERE login.privilege = 8
		
		ORDER BY users.uId ASC
		";	
	$execute_doctor_details = mysql_query($query_get_doctor_details);
	
	echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Doctor Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
     ';	
	 if(isset($_GET['update_successfully']))
		{
		echo "<h4 style=\"color:green\" align=\"center\">Successfully Update</h4>" ;
		}	
	echo'						
    <a href="doctor_add_new_doctor_details.php">
    <button class="uibutton icon special add "  rel="1" type="submit" >Add New Doctor</button></a>
	';
	echo' 		                                                                         	 
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
    <tr>		   
	<th>SNo.</th>
	<th>Name</th>
	<th>Email</th>	
	<th>Image</th>	
					
    </tr>
    </thead>	 	
	<tbody align="center">  ';
		  $sno = 0;
	while($get_doctor = mysql_fetch_array($execute_doctor_details))
		{  
		  $sno++;
		  $uid=$get_doctor['uId'];
		  $image=$get_doctor['image'];
		  echo '<tr>					    
		  <td>'.$sno.'</td>
		  <td><a href="doctor_admin_view_doctor_details.php?doc_id='.$uid.'">'.$get_doctor['Name'].'</td>
		  <td>'.$get_doctor['Email'].'</td>
		  <td><image src="user_image/'.$image.'"width="110px" height="110px"/></td>
		  </tr>';	
		 }
	echo'
	</tbody>
	</table>								
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';						
}
//function to called By doctor_add_new_doctor_details.php
function doctor_add_new_details()//Add New Doctor Details
{    //Author :Manish mishra
	echo 
	'<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Add New Doctor</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	if(isset($_GET['added']))
	 {
	  echo "<h3 style=\"color:green\" align=\"center\">Successfully Added</h3>" ;
	 }
	if(isset($_GET['usernameExists']))
	 {
	  echo "<h2 style=\"color:red\" align=\"center\">Username Already exists, please try again</h2>";
	 }
	echo'	
	<form id="validation_demo" action="doctor/add_new_doctor.php" method="post" enctype="multipart/form-data">
	<div class="section ">
	<label> Name<small></small></label>   
	<div> 
	<input type="text" class="validate[required] medium" name="name"/>
	</div>
	</div>	
	<div class="section">
	<label> Login  Account  <small></small></label>
	<div>
	<input type="text" onKeyUp="check_doctor();" onblur="check_doctor();"  name="username" id="username"   class="validate[required,minSize[3],maxSize[20],] medium"  /> <label id="show_availability"></label></td>
	<label>Username</label>                                             
	</div>
	<div>
	<input type="password"  class="validate[required,minSize[3]] medium"  name="password" id="password"  />
	<label>Password</label>
	</div>
	<div>
	<input type="password" class="validate[required,equals[password]] medium"  name="confirm_password" id="passwordCon"  />
	<label>Confirm Password</label>                                                    
	</div>
	</div>						
	<div class="section ">
	<label> Email<small></small></label>   
	<div> 
	<input type="text" class="validate[required,custom[email]] medium" name="email_id" id="e_required">
	</div>
	</div>				
	<div class="section  numericonly ">
	<label>Contact No<small></small></label>   
	<div> 
	<input type="text" class="validate[required] medium" name="phone_no" maxlength="12"/>
	</div>
	</div>		
	<div class="section">
	<label>Address <small></small></label>   
	<div > <textarea name="local_address" id="Textareaelastic"  class="medium"  cols="" rows=""  ></textarea>
	</div>                                              
	</div>		   
	<div class="section">
	<label>Date of Birth<small></small></label>   
	<div><input type="text"  id="birthday" class=" birthday  medium " name="dob"  />
	</div>
	</div>
	<div class="section">
	<label>Gender <small></small></label>   
	<div>
	<div class="radiorounded">
	<input type="radio" name="gender" id="radio-1" value="male" />
	<label for="radio-1" title="Male"></label>
	</div>
	<div class="radiorounded">
	<input type="radio" name="gender" id="radio-2" value="female"/>
	<label for="radio-2"  title="Female"></label>
	</div>
	</div>
	</div>
	<div class="section ">
	<label> Image <small></small></label>   
	<div> 
	<input type="file" name="image" class="fileupload" />
	</div>
	</div>
	<br></br>
	<tr><div class="section last">	
	<button class="uibutton icon  add " type="submit">Add Doctor</button> 
	<button class="uibutton icon special cancel " type="reset">Reset</button> 							  	  	  
	</form>';							
	echo'				  
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';						
}
//function to called By  doctor_add_information.php
function doctor_add_information()//Add information of Doctor
{   
    //Author :Manish mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Add Information</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';
	/*if(($_GET['doc_id']==""))//check the doctor id 
		 {
			header('location:doctor_health_select_student_details.php');
		 }	*/
		 
	if(isset($_GET['added_successfully']))
		{
		echo "<h4 style=\"color:green\" align=\"center\">Successfully Added</h4>" ;
		}	
	echo'
	<form id="validation_demo" action="doctor/add_doctor_information.php" method="get" >
	<div class="section ">
	<label>Qualification<small></small></label>   
	<div> 
	<input type="text" class="validate[required] medium" name="qualification"/>
	</div>
	</div>	
	<div class="section ">
	<label>Specialist<small></small></label>   
	<div> 
	<input type="text" class="validate[required] medium" name="specialist"/>
	</div>
	</div>	
	<div class="section ">
	<label>Experience<small></small></label>   
	<div> 
	<input type="text" class="medium" name="experience"/>
	</div>
	</div>	
	<tr><div class="section last">	
	<button class="uibutton btn-btn " type="submit">Submit</button> 
	<button class="uibutton btn-btn special cancel" type="reset">Reset</button> 	
	</form>
	';
	echo'	
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';				
}
//function to called By doctor_admin_view_doctor_details.php
function doctor_admin_view_information()//view doctor information
{      
     //Author :Manish mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i> Manager Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';
	$user_id = $_GET['doc_id'];
	$privilege = $_SESSION['priv'];
	
	if(($_GET['doc_id']==""))//check the doctor id 
		 {
			header('location:doctor_add_new_doctor_details.php');
		 }
	else		
	//Query to fetch the login details of the user
	$query_login_details ="SELECT `username`,`privilege`
	                       FROM `login` 
						   WHERE `lId`=".$user_id." ";
	$execute_login_details=mysql_query($query_login_details);
	$login_details = mysql_fetch_array($execute_login_details);
	$username = $login_details['username'];
	$privilege = $login_details['privilege'];
	
	//query to get doctor information details
	 $get_information="SELECT *
	                   FROM doctor_info_details
					   WHERE doc_id=".$user_id."";
	$exe_get_information=mysql_query($get_information);
	$fetch_information=mysql_fetch_array($exe_get_information);
	$qualification=$fetch_information['qualification'];	
	$specialist=$fetch_information['specialist'];
	$experience=$fetch_information['experience'];			   
	
    //Query to fetch personal details of the Doctor
	$query_personal_details = 
	        mysql_query("SELECT `Name`,`DOB`,`Email`,`Local Address`,`Phone No`,`sex`,`image`,`uId` 
	                    FROM `users`
						WHERE `uId` = '$user_id' ");
	$personal_details = mysql_fetch_array($query_personal_details);
	$uid = $personal_details['uId'];
	$name = $personal_details['Name'];
	$dob = $personal_details['DOB'];
	$email = $personal_details['Email'];
	$local_address = $personal_details['Local Address'];
	$phone_no = $personal_details['Phone No'];
	$gender = $personal_details['sex'];
	$image = $personal_details['image'];
	
	if(isset($_GET['update_successfully']))
		{
		echo "<h3 style=\"color:green\" align=\"center\">Successfully Update</h3>" ;
		}
	echo '	
	<h2>Welcome</h2> 	
	</div>
	<image src="user_image/'.$image.'"  width="200px" height="200px" align="left" />
	<table class="table table-bordered table-striped"><tr>
	<td><strong>uId</strong></td><td>'.$uid.'</td></tr><tr>
	<td><strong>Name</strong></td><td>'.$name.'</td></tr><tr>
	<td><strong>Username</strong></td><td>'.$username.'</td></tr><tr>
	<td><strong>Qualification</strong></td><td>'.$qualification.'</td></tr><tr>
	<td><strong>Specialist</strong></td><td>'.$specialist.'</td></tr><tr>
	<td><strong>Experience</strong></td><td>'.$experience.'</td></tr><tr>
	<td><strong>Date of Birth</strong></td><td>'.$dob.'</td></tr><tr>
	<td><strong>Email address</strong></td><td>'.$email.'</td></tr>		<tr>
	<td><strong>Gender</strong></td><td>'.$gender.'</td></tr><tr>
	<td><strong>Local Address</strong></td><td>'.$local_address.'</td></tr><tr>
	<td><strong>Phone Number</strong></td><td>'.$phone_no.'</td></tr>
	</table>
	<a href="doctor_edit_details.php?doc_id='.$uid.'">
	<button type="submit" class="uibutton icon special add">Edit Details</button></a>
	';
	echo'					  
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	';			
}
//function to called By doctor_edit_details.php
function doctor_admin_edit_details()//Edit the doctor details
{   
     //Author :Manish mishra
	echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Edit Doctor Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';
	$user_id=$_GET['doc_id'];
	if(($_GET['doc_id']==""))//check the doctor id 
		 {
			header('location:doctor_edit_details.php');
		 }
	else
		{		
	//Query to get the username of the teacher
	$query_username = "
			SELECT username
			FROM login
			WHERE lId = $user_id AND privilege = 8
		   ";
	$execute_username = mysql_query($query_username);
	$username = mysql_fetch_array($execute_username);
	$username = $username[0];
	
	//Query to get the details of the user
	$query_get_details_user = "
			SELECT `Name`, `DOB`, `Email`, `Local Address`, `Phone No`, `sex`, `image`
			FROM users
			WHERE uId = $user_id";
	$execute_get_details_user = mysql_query($query_get_details_user);
	$get_details_user = mysql_fetch_array($execute_get_details_user);	
	$name = $get_details_user['Name'];
	$dob = $get_details_user['DOB'];
	$email = $get_details_user['Email'];
	$local_address = $get_details_user['Local Address'];
	$phone_no = $get_details_user['Phone No'];
    $gender = $get_details_user['sex'];
	$image = $get_details_user['image'];
	
	//query to get doctor information details
	 $get_information="
			SELECT *
			FROM doctor_info_details
			WHERE doc_id=".$user_id."";
	$exe_get_information=mysql_query($get_information);
	$fetch_information=mysql_fetch_array($exe_get_information);
	$qualification=$fetch_information['qualification'];	
	$specialist=$fetch_information['specialist'];
	$experience=$fetch_information['experience'];	
	
	echo'
	<form action="doctor/update_doctor_details.php" method="post" enctype="multipart/form-data">				
	<div class="section ">			
	<div> 
	<input type="hidden" value="'.$user_id.'" name="user_id"/>
	</div>
	</div>					
	<div class="section ">
	<label> Name</label>   
	<div> 
	<input input id="name" name="name" type="text" value="'.$name.'"class="validate[required] medium"/>
	</div>
	</div>	
	<div class="section">
	<label>birthday</label>   
	<div><input type="text" value="'.$dob.'" id="birthday" class=" birthday medium " name="dob"  />
	</div>
	</div>
	<div class="section ">
	<label> Email</label>   
	<div> 
	<input type="text" id="e_required " name="email_id" onblur="checkemail(this.value)"
	value="'.$email.'"class="validate[required,custom[email]] medium ">
	</div>
	</div>
	<div class="section">
	<label>Address</label>   
	<div > <textarea name="local_address" id="Textareaelastic"  class="medium"  cols="" rows=""  >'.$local_address.'</textarea>
	</div>                                              
	</div>
	<div class="section numericonly">
	<label>Contact NO.</label>   
	<div> 
	<input name="phone_no" type="text" value="'.$phone_no.'" class="medium" maxlength="12">
	</div>
	</div>
	<div class="section ">
	<label>Qualification</label>   
	<div> 
	<input type="text" class="validate[required] medium" value="'.$qualification.'" name="qualification"/>
	</div>
	</div>	
	<div class="section ">
	<label>Specialist</label>   
	<div> 
	<input type="text" class="validate[required] medium"  value="'.$specialist.'" name="specialist"/>
	</div>
	</div>	
	<div class="section ">
	<label>Experience</label>   
	<div> 
	<input type="text" class="medium" value="'.$experience.'" name="experience"/>
	</div>
	</div>	
	<div class="section">
	<label>Gender </label>   
	<div>
	';

	if($gender=='male')
		{
		
		echo '<div class="radiorounded">
		<input type="radio" name="gender" id="radio-1" value="male" checked="checked"/>
		<label for="radio-1" title="Male"></label></div>
		
		<br><div class="radiorounded">
		<input type="radio" name="gender" id="radio-2" value="female" />
		<label for="radio-2"  title="Female"></label>
		</div>
		';
		}
	else if($gender=='female')
		{	
	
		echo '
		<div class="radiorounded">
		<input type="radio" name="gender" id="radio-1" value="male" />
		<label for="radio-1" title="Male"></label></div>
		<br>
		<div class="radiorounded">
		<input type="radio" name="gender" id="radio-2" value="female" checked="checked"/>
		<label for="radio-2"  title="Female"></label>
		</div>';	
		}			
	echo' 
	</div>
	</div>
	<div class="section last">	
	<button class="uibutton icon btn-btn special" type="submit">Update </button> 			
	</form>	';
	echo'			  
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
	}//end of else	
}
//priv doctor
//function to called By doctor_student_health_information_details.php
function doctor_student_health_information()//select the class
{ 
    //Author :Manish mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Class Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	if(isset($_GET['Successfully_added']))
	{
	echo "<h3 style=\"color:green\" align=\"center\">Successfully Added</h3>" ;
	}		
	//query to get class name
	$get_class="SELECT `cId`,`class_name` 
	            FROM class_index 
	            ORDER BY class+0 ASC, section ASC";
	$exe_class=mysql_query($get_class);
	
	echo' <div class="span4"><h3 style="color:">Kindly select class</h3></div>';
	echo '</h2>';
	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo '<ol class="rounded-list">';
	
	while($res=mysql_fetch_array($exe_class))//looping for getting  class 
    	 {	 
		$class=$res['class_name'];
		echo '<tr class="row-fluid" id="added_rows">
		<td><div class="row-fluid">
		<div class="span6">							                                                              
		<li><a href="doctor_health_select_student_details.php?&class_id='.$res['cId'].'">'.$res['class_name'].'</a>            </div>
		</div><!-- end row-fluid  -->
		</td>
		</tr>';	                                                          	                                 			 
    	 }		 		
	echo'						
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';		
	
}
//function to called BY doctor_health_select_student_details.php
function doctor_student_cheakup_details()//student health check entry
{    
    //Author :Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Checkup Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	$class_id=$_GET['class_id'];
	//query to get student name,class
	$get_student="
			SELECT  DISTINCT class.classId,class.sId,student_user.sId,student_user.Name
			FROM class
			
			INNER JOIN student_user
			ON class.sId=student_user.sId
			
			WHERE class.classId=".$_GET['class_id']."  AND class.session_id=".$_SESSION['current_session_id']."";
	
	$exe_student=mysql_query($get_student);				 
	echo' 
	<form action="doctor/add_student_cheackup_details.php" method="post" id="demovalidation">
	<input type="hidden" name="class_id" value="'.$class_id.'"/>
	<div class="section ">
	<label>Select Student<small></small></label><div>
	<select  data-placeholder="Select Student..." class="chzn-select" tabindex="2" name="id" >        
	<option value=""></option>'; 	
								
	while($fetch_student=mysql_fetch_array($exe_student))//while for use select student name
		{
		$s_name=$fetch_student['Name'];	
		$s_id=$fetch_student['sId'];
		
		echo'
		<option value="'.$s_id.'">'.$s_name.'</option>';	
		}			  					   
	echo' 
	</select>
	</div> 	
	</div>
	<div class="section ">
	<label>Select Blood Group<small></small></label>        
	<div>
	<select  data-placeholder="Select Group..." class="chzn-select" tabindex="2" name="blood" >        
	<option value=""></option> 								
	<option value="A+">A+</option> 
	<option value="O+">O+</option> 
	<option value="B+">B+</option> 
	<option value="AB+">AB+</option> 
	<option value="A-">A-</option> 
	<option value="O-">O-</option> 
	<option value="B-">B-</option>
	<option value="AB-">AB-</option>	  
	';		  					   
	echo' 
	</select>
	</div> 	
	</div>
	<div class="section regexonly">
	<label>Eye Test <small></small></label>   
	<div>
	<label>Left</label>
	<input type="text" name="left"  maxlength="3" class="validate[required]" required="required"/><b>/6</b></div>
	<div>
	<label>Right</label>
	<input type="text" name="right"maxlength="3" class="validate[required] " required="required"/><b>/6</b> 
	</div>
	</div>
	<div class="section">
	<label>Dental Test<small></small></label>   
	<div><textarea name="dental" id="dental"   class="validate[required] small" required="required" cols="" rows=""></textarea></div>   
	</div>
	<div class="section">
	<label>ENT Test<small></small></label>   
	<div > <textarea name="ENT" id="ent"   class="validate[required] small"  required="required" cols="" rows=""></textarea></div>
	</div>
	<div class="section">
	<label>Diseases <small></small></label>   
	<div> <input type="text" name="diseases" /></div>
	</div>
	<div class="section">
	<label>Remarks<small></small></label>   
	<div > <textarea name="remarks" id="ent"   class="validate[required] small"  required="required" cols="" rows=""></textarea></div>
	</div>
	<div class="section last">
	<div><button class="uibutton" title="Submit" type="submit" rel="1" >submit</button></div>
	</div>
	</form> 
	  ';	
	echo'						
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';				
}

//function to called By doctor_health_view_cheakup_details.php
function doctor_student_view_cheakup_details()//view Student checkup Details
{
	//Author  : Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Class Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
		
	//query to get class name
	$get_class="SELECT DISTINCT `cId`,`class_name`
	            FROM class_index 
	            ORDER BY class+0, section ASC";
	$q_res=mysql_query($get_class);
	echo' <div class="span4"><h3 style="color:">Kindly select class</h3></div>';
	echo '</h2>';
	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo '<ol class="rounded-list">';
	
	while($res=mysql_fetch_array($q_res))//looping for getting  class id,class
		{	
		$class=$res['class_name'];
		echo '<tr class="row-fluid" id="added_rows">
		<td><div class="row-fluid">
		<div class="span6">							                                                              
		<li><a href="doctor_view_cheakup_student_details.php?&class_id='.$res['cId'].'">'.$res['class_name'].'Class</a>                                                               	                                 
		</div>
		</div><!-- end row-fluid  -->
		</td>
		</tr>';	 	 
		}		 		
	echo'						
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';					
}
//function to called By doctor_view_checkup_student_details.php
function doctor_view_cheakup_student_details()//view checkup details
{
	//Author  : Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Checkup Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	if(isset($_GET['update_successfully']))
		{
		echo "<h4 style=\"color:green\" align=\"center\">Update Successfully</h4>" ;
		}	
	if(($_GET['class_id']==""))//check the doctor id 
		 {
			header('location:doctor_health_view_cheakup_details.php');
		 }
		 
		 
	$class_id=$_GET['class_id'];	
	//query to get student name
	$get_student="
	              SELECT  DISTINCT class.classId,class.sId,student_user.sId,student_user.Name
	              FROM class
				  
	              INNER JOIN student_user
	              ON class.sId=student_user.sId
				  
	              WHERE class.classId=".$_GET['class_id']." class.session_id=".$_SESSION['current_session_id']."";
	
	$exe_student=mysql_query($get_student);				 
	echo' 
	<form >
	<div class="section ">
	<label>Select Student<small></small></label>        
	<div>
	<select  data-placeholder="Select Student..." class="chzn-select" tabindex="2" name="id" id="sel_id">        
	<option value=""></option>'; 								
	while($fetch_student=mysql_fetch_array($exe_student))//while for use select student name
		{
		$s_name=$fetch_student['Name'];	
		$s_id=$fetch_student['sId'];
		echo'
		<option value="'.$s_id.'">'.$s_name.'</option>';	
		}						 
	echo' 
	</select>
	</div> 	
	</div>
	<div class="section last">
	<div><button class="uibutton" type="button" onclick="show_cheakup_details('.$class_id.');">
	View</button></div>
	</div>
	</form>';
	echo'	
	<span id="data_cheakup"></span>					
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';		
 }
 //function to called by doctor_view_checkup_student_details.php
 function student_view_cheakup_details()//show the student checkup details priv:student
 {
	 //Author  : Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Checkup Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';
	$flag=1;	
	$s_id=$_SESSION['user_id']; 
	//query to get checkup details
	 $get_doctor_id="
			SELECT DISTINCT doc_id
			FROM student_health_info_details
			WHERE sid=".$s_id."";
	$exe_doctor_id=mysql_query($get_doctor_id);	 
	while($fetch_doc_id=mysql_fetch_array($exe_doctor_id))
	     {
		 $doc_id=$fetch_doc_id['doc_id']; 
		//query to get doctor details 
		 $get_doctor_details="
				SELECT *
				FROM doctor_info_details
				WHERE doc_id=".$doc_id."";
		$exe_doctor_details=mysql_query($get_doctor_details);
		$fetch_doctor_details=mysql_fetch_array($exe_doctor_details);
		$qualification=$fetch_doctor_details['qualification'];
		$specialist=$fetch_doctor_details['specialist'];
		$experience=$fetch_doctor_details['experience'];
		//query to get doctor name
		$get_doctor_name="
				SELECT Name
				FROM users
				WHERE uId=".$doc_id."";
		$exe_get_doctor_name=mysql_query($get_doctor_name);
		$fetch_doc_name=mysql_fetch_array($exe_get_doctor_name);
		$doc_name=$fetch_doc_name['Name'];		
		echo'
		<div style="float:left">
		<h4 style="color:green" >Doctor Name:'.$doc_name.'</h4>
		<h4 style="color:green" >Qualification:'.$qualification.'</h4>
		<h4 style="color:green" >Specialist:'.$specialist.'</h4>
		</div>';
		 
		echo'
		<table  class="table table-bordered table-striped" >
		<thead>
		<tr>
		<td><h5 style="color:brown" align="center">Eye Details<h5></td>
		<td><h5 style="color:brown" align="center">Dental Details</h5></td>
		<td><h5 style="color:brown" align="center">ENT Details</h5></td>
		<td><h5 style="color:brown" align="center">Diseases</h5></td>
		<td><h5 style="color:brown" align="center">Remarks</h5></td>
		<td><h5 style="color:brown" align="center">Date</h5></td>
		<td><h5 style="color:brown" align="center">Time</h5></td>
		<tbody align="center">
		';
		$b='';
		//query to get checkup details
		$get_checkup_details="
				SELECT * 
				FROM student_health_info_details
				WHERE sid=".$s_id." AND doc_id=".$doc_id." ";
		$exe_checkup_details=mysql_query($get_checkup_details);
	while($fetch_checkup_details=mysql_fetch_array($exe_checkup_details))	//loop for get checkup details			  				
	   {
		
			$eye_details=$fetch_checkup_details['eye_details'];
			$dental_details=$fetch_checkup_details['dental_details'];
			$ent_details=$fetch_checkup_details['ent_details'];
		
			if($fetch_checkup_details['blood_group'] == "")
				{
				
				}
			else
				{
				$b=$fetch_checkup_details['blood_group'];	
				}
			$diseases=$fetch_checkup_details['diseases'];
			$remarks=$fetch_checkup_details['remarks'];
			$date=$fetch_checkup_details['date'];
			$time=$fetch_checkup_details['time'];
			$eye_array=explode("|",$eye_details);
			$left=$eye_array[0];
			$right=$eye_array[1];
			echo'<tr>
			<td>Left: <b>'.$left.'</b><b>/6</b>Right: <b>'.$right.'</b><b>/6</b></td>
			<td>'.$dental_details.'</td>
			<td>'.$ent_details.'</td>
			<td>'.$diseases.'</td>
			<td>'.$remarks.'</td>
			<td>'.$date.'</td>
			<td>'.$time.'</td>
			';	
			}//end of inner while
	
	echo' 
	 <h4 style="color:red" align="center">Blood Group:'.$b.'</h4></div>';	
	
	echo'</tbody>
	     <table><hr></hr>';
		   
	 }//end of outer while
	echo'			
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';		 	 
}
 //function to called by doctor_view_parent_student_health_details.php
 function parent_view_cheakup_student_details()//show student checkup details priv:parent
 {
	 //Author  : Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Checkup Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	$p_id=$_SESSION['user_id']; 
	//query to get parent_id
	 $get_pid="SELECT lId,privilege
	     FROM login
		 WHERE lId=".$p_id." AND privilege=4 ";
	$exe_get_pid=mysql_query($get_pid);
	$fetch_get_pid=mysql_fetch_array($exe_get_pid);	 
		
		//query to get sid
		 $sid="SELECT lId,privilege
		     FROM login
		     WHERE lId=".$p_id." AND privilege=3 ";
	  $exe_sid=mysql_query($sid);
	  $fetch_sid=mysql_fetch_array($exe_sid);
	  $sid=$fetch_sid['lId'];		 
	//query to get checkup details
	 $get_doctor_id="
			SELECT DISTINCT doc_id
			FROM student_health_info_details
			WHERE sid=". $sid."";
	$exe_doctor_id=mysql_query($get_doctor_id);	 
	while($fetch_doc_id=mysql_fetch_array($exe_doctor_id))
	     {
			 $doc_id=$fetch_doc_id['doc_id'];
			 
		//query to get doctor details 
		 $get_doctor_details="
				SELECT *
				FROM doctor_info_details
				WHERE doc_id=".$doc_id."";
		$exe_doctor_details=mysql_query($get_doctor_details);
		$fetch_doctor_details=mysql_fetch_array($exe_doctor_details);
		$qualification=$fetch_doctor_details['qualification'];
		$specialist=$fetch_doctor_details['specialist'];
		$experience=$fetch_doctor_details['experience'];
	
		//query to get doctor name
		$get_doctor_name="
				SELECT Name
				FROM users
				WHERE uId=".$doc_id."";
		$exe_get_doctor_name=mysql_query($get_doctor_name);
		$fetch_doc_name=mysql_fetch_array($exe_get_doctor_name);
		$doc_name=$fetch_doc_name['Name'];	
		echo'
		<div style="float:left">
		<h4 style="color:green" >Doctor Name:'.$doc_name.'</h4>
		<h4 style="color:green" >Qualification:'.$qualification.'</h4>
		<h4 style="color:green" >Specialist:'.$specialist.'</h4>
		</div>';	
		echo'
		<table  class="table table-bordered table-striped" >
		<thead>
		<tr>
		<td><h5 style="color:brown" align="center">Eye Details<h5></td>
		<td><h5 style="color:brown" align="center">Dental Details</h5></td>
		<td><h5 style="color:brown" align="center">ENT Details</h5></td>
		<td><h5 style="color:brown" align="center">Diseases</h5></td>
		<td><h5 style="color:brown" align="center">Remarks</h5></td>
		<td><h5 style="color:brown" align="center">Date</h5></td>
		<td><h5 style="color:brown" align="center">Time</h5></td>
		<tbody align="center">
		';
		$b='';
		//query to get checkup details
		$get_checkup_details="
				SELECT * 
				FROM student_health_info_details
				WHERE sid=".$sid." AND doc_id=".$doc_id." ";
		$exe_checkup_details=mysql_query($get_checkup_details);
		while($fetch_checkup_details=mysql_fetch_array($exe_checkup_details))	//loop for get checkup details			  				
		   {
				$eye_details=$fetch_checkup_details['eye_details'];
				$dental_details=$fetch_checkup_details['dental_details'];
				$ent_details=$fetch_checkup_details['ent_details'];
				if($fetch_checkup_details['blood_group'] == "")
					{	
					}
				else
					{
					$b=$fetch_checkup_details['blood_group'];	
					}
				$diseases=$fetch_checkup_details['diseases'];
				$remarks=$fetch_checkup_details['remarks'];
				$date=$fetch_checkup_details['date'];
				$time=$fetch_checkup_details['time'];
				$eye_array=explode("|",$eye_details);
				$left=$eye_array[0];
				$right=$eye_array[1];
				echo'<tr>
				<td>Left: <b>'.$left.'</b><b>  /6  </b>Right: <b>'.$right.'</b><b>/6</b></td>
				<td>'.$dental_details.'</td>
				<td>'.$ent_details.'</td>
				<td>'.$diseases.'</td>
				<td>'.$remarks.'</td>
				<td>'.$date.'</td>
				<td>'.$time.'</td>
				';	
			}//end of inner while
	echo' <h4 style="color:red" align="center">Blood Group:'.$b.'</h4></div>';	
	echo'</tbody>
	     <table><hr></hr>';
 		}//end of outer while
	echo'			
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';				 
}

//function to called by doctor_manager_session.php   priv:manager & Admin
function doctor_manager_session()//seslct session
{ 
    //Author :Manish Mishra
	echo  '<div class="row-fluid">                           
	<div class="span12  widget clearfix">
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Session</span>
	</div><!-- End widget-header -->		  
	<div class="widget-content"><br />
	<div class="row-fluid"> 			
	';	
	
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	

	echo'
	<form action=" doctor_manager_class_student_health_details.php" method="get" name="session" >							
	<label><h4 align="center">select Session<h4><small></label>   
	<div align="center">											
	<select  data-placeholder="Choose  Session..." class="chzn-select" tabindex="2" name="session">        
	<option value=""></option>'; 								
	//query to get month from `fee_generated_sessions`  table
	 $get_session="
		SELECT `sId`,`session_name`
		FROM `session_table` ";
	$get_session_exe=mysql_query($get_session);
	while($get_session_fetch=mysql_fetch_array($get_session_exe))
	     {
			$sid=$get_session_fetch['sId'];	
			$session_name=$get_session_fetch['session_name'];
			echo'
	        <option value="'.$sid.'">'. $session_name.'</option>';
	      } 							 
	echo'
	</select>       
	</div>									                           	       				
	<br></br>			   																
	<div class="section last" align="center">
	<button  class="uibutton submit_form" >Go</button>
	</div>
	</form>
	';
	//query to get session name and session_id	
	echo'                                       
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';				
}
//function to called by doctor_manager_class_student_health_details.php  
function  manager_class_cheakup_student_details()//select class
{
    //Author  : Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Class Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	if($_GET['session']=="")//check the session_id 
		 {
	header('location:doctor_manager_session.php');
		 }
	$session_id=$_GET['session'];	
	//query to get class name
	$get_class="SELECT DISTINCT `cId`,`class_name`
	            FROM class_index 
	               ORDER BY class+0 ASC,section ASC";
	$q_res=mysql_query($get_class);
	echo' <div class="span4"><h3 style="color:">Kindly select class</h3></div>';
	echo '</h2>';
	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo '<ol class="rounded-list">';
	
	while($res=mysql_fetch_array($q_res))//looping for getting  class id,class
		{	
		$class=$res['class_name'];
		echo '<tr class="row-fluid" id="added_rows">
		<td><div class="row-fluid">
		<div class="span6">							                                                              
		<li><a href="doctor_manager_student_health.php?&class_id='.$res['cId'].'&session_id='.$session_id.'">'.$res['class_name'].'</a>                                                          	                                 
		</div>
		</div><!-- end row-fluid  -->
		</td>
		</tr>';	 	 
		}		 		
	echo'						
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';							
}
//function to calle by doctor_manager_student_health.php
function manager_cheakup_student_details()//student checkup details
{
 	//Author  : Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Checkup Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	if(($_GET['class_id']==""))//check the doctor id 
		 {
			header('location:doctor_manager_student_health.php');
		 }
    $session_id=$_GET['session_id'];	 
	$class_id=$_GET['class_id'];	
	//query to get student name
	$get_student="
		SELECT  DISTINCT class.classId,class.sId,student_user.sId,student_user.Name
		FROM class
		
		INNER JOIN student_user
		ON class.sId=student_user.sId
		
		WHERE class.classId=".$_GET['class_id']." AND class.session_id=".$session_id."";
	
	$exe_student=mysql_query($get_student);				 
	echo' 
	<form action="doctor_manager_view_checkup_details.php" method="get">
	<input type="hidden" name="class_id" value="'.$class_id.'" /> 
	<input type="hidden" name="session_id" value="'.$session_id.'"/> 
	<div class="section ">
	<label>Select Student<small></small></label>        
	<div>
	<select  data-placeholder="Select Student..." class="chzn-select" tabindex="2" name="id" id="sel_id">    
	<option value=""></option>'; 								
	while($fetch_student=mysql_fetch_array($exe_student))//while for use select student name
		{
		$s_name=$fetch_student['Name'];	
		$s_id=$fetch_student['sId'];
		echo'
		<option value="'.$s_id.'">'.$s_name.'</option>';	
		}						 
	echo' 
	</select>
	</div> 	
	</div>
	<div class="section last">
	<div><button class="uibutton" type="submit">View</button></div>
	</div>
	</form>';
	echo'		
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';		 		
}
//function to called by doctor_manager_view_checkup_details.php
function  manager_view_cheakup_details()//view checkup details
{
	//Author  : Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Checkup Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	$session_id=$_GET['session_id'];
	$class_id=$_GET['class_id']; 
	$sid=$_GET['id']; 	 
	//query to get session 
	$get_session="
			SELECT sId,session_name
			FROM session_table
			WHERE sId=".$session_id."";	
	$exe_session=mysql_query($get_session);
	$fetch_session=mysql_fetch_array($exe_session);
	$session_name=$fetch_session['session_name'];
	echo' <h5 align="center">'.$session_name.'</h5>';
	if($sid=="")//check the student_id 
		 {
			header('location:doctor_manager_student_health.php?class_id='.$class_id.'&session_id='.$session_id.'');
		 }
	//query to get student name
	$get_student_details="
			SELECT Name
			FROM student_user
			WHERE sId=".$sid." ";
	$exe_get_name=mysql_query($get_student_details);
	$fetch_nmae=mysql_fetch_array($exe_get_name);
	$name=$fetch_nmae['Name'];
	//query to get class
	$get_calss="
			SELECT class_name
			FROM class_index
			WHERE cId=".$class_id."";
	$exe_class=mysql_query($get_calss);
	$fetch_class=mysql_fetch_array($exe_class);		
	$class_name=$fetch_class['class_name'];		
	echo' <div style="float:left">
	 <h5 style="color:green" align="left">Roll No.:'.$sid.'</h5>
	 <h5 style="color:green" align="left">Name:'.$name.'</h5>
	 <h5 style="color:green" align="left">Class:'.$class_name.'</h5>
	 </div>';	
     //query to get checkup details
	 $get_doctor_id="
			SELECT DISTINCT doc_id
			FROM student_health_info_details
			WHERE sid=". $sid." AND session_id=".$session_id."";
	$exe_doctor_id=mysql_query($get_doctor_id);	 
	
	while($fetch_doc_id=mysql_fetch_array($exe_doctor_id))
	     {
			 $doc_id=$fetch_doc_id['doc_id'];
		//query to get doctor details 
		  $get_doctor_details="
				SELECT *
				FROM doctor_info_details
				WHERE doc_id=".$doc_id." ";
		$exe_doctor_details=mysql_query($get_doctor_details);
		$fetch_doctor_details=mysql_fetch_array($exe_doctor_details);
		$qualification=$fetch_doctor_details['qualification'];
		$specialist=$fetch_doctor_details['specialist'];
		$experience=$fetch_doctor_details['experience'];
		//query to get doctor name
		$get_doctor_name="
				SELECT Name
				FROM users
				WHERE uId=".$doc_id."";
		$exe_get_doctor_name=mysql_query($get_doctor_name);
		$fetch_doc_name=mysql_fetch_array($exe_get_doctor_name);
		$doc_name=$fetch_doc_name['Name'];	
		echo'
		<div style="float:right">
		<h5 style="color:green" >Doctor Name:'.$doc_name.'</h5>
		<h5 style="color:green" >Qualification:'.$qualification.'</h5>
		<h5 style="color:green" >Specialist:'.$specialist.'</h5>
		</div>';
		
		echo'
		<table  class="table table-bordered table-striped">
		<thead>
		<tr>
		<td><h5 style="color:brown" align="center">Eye Details<h5></td>
		<td><h5 style="color:brown" align="center">Dental Details</h5></td>
		<td><h5 style="color:brown" align="center">ENT Details</h5></td>
		<td><h5 style="color:brown" align="center">Diseases</h5></td>
		<td><h5 style="color:brown" align="center">Remarks</h5></td>
		<td><h5 style="color:brown" align="center">Date</h5></td>
		<td><h5 style="color:brown" align="center">Time</h5></td>
		<tbody align="center">
		';
		$b='';
		//query to get checkup details
		 $get_checkup_details="
				SELECT * 
				FROM student_health_info_details
				WHERE sid=".$sid." AND doc_id=".$doc_id." AND session_id=".$session_id." ";
		$exe_checkup_details=mysql_query($get_checkup_details);
		while($fetch_checkup_details=mysql_fetch_array($exe_checkup_details))	//loop for get checkup details			  				
		   {  
		  
			$eye_details=$fetch_checkup_details['eye_details'];
			$dental_details=$fetch_checkup_details['dental_details'];
			$ent_details=$fetch_checkup_details['ent_details'];
			if($fetch_checkup_details['blood_group'] == "")
			{	
			}
			else
			{
			$b=$fetch_checkup_details['blood_group'];	
			}
			$diseases=$fetch_checkup_details['diseases'];
			$remarks=$fetch_checkup_details['remarks'];
			$date=$fetch_checkup_details['date'];
			$time=$fetch_checkup_details['time'];
			$eye_array=explode("|",$eye_details);
			$left=$eye_array[0];
			$right=$eye_array[1];
			echo'<tr>
			<td>Left: <b>'.$left.'</b><b>  /6  </b>Right: <b>'.$right.'</b><b>/6</b></td>
			<td>'.$dental_details.'</td>
			<td>'.$ent_details.'</td>
			<td>'.$diseases.'</td>
			<td>'.$remarks.'</td>
			<td>'.$date.'</td>
			<td>'.$time.'</td>
			';	
		}//end of inner while
	echo' <h5 style="color:red" align="center">Blood Group:'.$b.'</h5></div>';	
	echo'</tbody>
	     <table><hr></hr>';
   }//end of outer while
	echo'			
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';				 	 		
}
//function to called by doctor_admin_session.php
function admin_session()//select session
{
    //Author :Manish Mishra
	echo  '<div class="row-fluid">                           
	<div class="span12  widget clearfix">
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Session</span>
	</div><!-- End widget-header -->		  
	<div class="widget-content"><br />
	<div class="row-fluid"> 			
	';	
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo'
	<form action="doctor_admin_class_student_checkup_details.php" method="get" name="session" >							
	<label><h4 align="center">select Session<h4><small></label>   
	<div align="center">											
	<select  data-placeholder="Choose  Session..." class="chzn-select" tabindex="2" name="session">        
	<option value=""></option>'; 								
	//query to get month from `fee_generated_sessions`  table
	 $get_session="
		SELECT `sId`,`session_name`
		FROM `session_table` ";
	$get_session_exe=mysql_query($get_session);
	while($get_session_fetch=mysql_fetch_array($get_session_exe))
	     {
			$sid=$get_session_fetch['sId'];	
			$session_name=$get_session_fetch['session_name'];
			echo'
	        <option value="'.$sid.'">'. $session_name.'</option>';
	      } 							 
	echo'
	</select>       
	</div>									                           	       				
	<br></br>			   																
	<div class="section last" align="center">
	<button  class="uibutton submit_form" >Go</button>
	</div>
	</form>
	';
	//query to get session name and session_id	
	echo'                                       
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';						
}
//function to called by doctor_admin_class_student_checkup_details.php
function  admin_student_class_details()//select class
{
	//Author  : Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Class Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	if($_GET['session']=="")//check the session_id 
		 {
			header('location:doctor_admin_select_session.php');
		 }
	$session_id=$_GET['session'];	
	//query to get class name
	$get_class="SELECT DISTINCT `cId`,`class_name`
	            FROM class_index 
	               ORDER BY class+0 ASC,section ASC";
	$q_res=mysql_query($get_class);
	echo' <div class="span4"><h3 style="color:">Kindly select class</h3></div>';
	echo '</h2>';
	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo '<ol class="rounded-list">';
	
	while($res=mysql_fetch_array($q_res))//looping for getting  class id,class
		{	
		$class=$res['class_name'];
		echo '<tr class="row-fluid" id="added_rows">
		<td><div class="row-fluid">
		<div class="span6">							                                                              
		<li><a href="doctor_admin_view_student_checkup_details.php?&class_id='.$res['cId'].'&session_id='.$session_id.'">'.$res['class_name'].'</a>                                                          	                                 
		</div>
		</div><!-- end row-fluid  -->
		</td>
		</tr>';	 	 
		}		 		
	echo'						
	
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';				
	}
//function to called by doctor_admin_view_student_checkup_details.php
function admin_view_cheakup_student_details()//show student checkup details
{
	
	//Author  : Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Checkup Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	$class_id=$_GET['class_id'];
	$session_id=$_GET['session_id'];
	//query to get session 
	$get_session="
			SELECT sId,session_name
			FROM session_table
			WHERE sId=".$session_id."";	
	$exe_session=mysql_query($get_session);
	$fetch_session=mysql_fetch_array($exe_session);
	$session_name=$fetch_session['session_name'];
	echo' <h5 style="color:green" align="center">Session:'.$session_name.'</h5>';
	//query to get class
	$get_calss="
			SELECT class_name
			FROM class_index
			WHERE cId=".$class_id."";
	$exe_class=mysql_query($get_calss);
	$fetch_class=mysql_fetch_array($exe_class);		
	$class_name=$fetch_class['class_name'];	
	  echo' <div style="float:left">
	  <h5 style="color:green" align="left">Class:'.$class_name.'</h5>
	  </div>';	
       echo'
		<table  class="table table-bordered table-striped">
		<thead>
		<tr>
		<td><h5 style="color:brown" align="center">S.No.<h5></td>
		<td><h5 style="color:brown" align="center">Roll No.<h5></td>
		<td><h5 style="color:brown" align="center">Name<h5></td>
		
		<td><h5 style="color:brown" align="center">Doctor Name<h5></td>
	     
		<td><h5 style="color:brown" align="center">Eye Details<h5></td>
		<td><h5 style="color:brown" align="center">Dental Details</h5></td>
		<td><h5 style="color:brown" align="center">ENT Details</h5></td>
		<td><h5 style="color:brown" align="center">Diseases</h5></td>
		<td><h5 style="color:brown" align="center">Remarks</h5></td>
		<td><h5 style="color:brown" align="center">Date</h5></td>
		<td><h5 style="color:brown" align="center">Time</h5></td></tr>
		<tbody align="center">
		';
        $sno=0;
	   //query to get checkup details
	   $get_checkup_details="
		  SELECT * 
		  FROM student_health_info_details
		  
		  INNER JOIN class
		  ON class.sId=student_health_info_details.sid  
		  
		  WHERE student_health_info_details.session_id=".$session_id." AND class.classId=".$class_id."
		  ORDER BY student_health_info_details.sid ASC ";
		$exe_checkup_details=mysql_query($get_checkup_details);
		while($fetch_checkup_details=mysql_fetch_array($exe_checkup_details))
			{
				 $sno++;
			$sid=$fetch_checkup_details['sid'];
			//query to get student name
					$get_student_details="
							SELECT Name
							FROM student_user
							WHERE sId=".$sid." ";
					$exe_get_name=mysql_query($get_student_details);
					$fetch_name=mysql_fetch_array($exe_get_name);
					$name=$fetch_name['Name'];
			
			$doctor_id=$fetch_checkup_details['doc_id'];
			$eye_details=$fetch_checkup_details['eye_details'];
			$dental_details=$fetch_checkup_details['dental_details'];
			$ent_details=$fetch_checkup_details['ent_details'];
			$blood=$fetch_checkup_details['blood_group'];	
				
			$diseases=$fetch_checkup_details['diseases'];
			$remarks=$fetch_checkup_details['remarks'];
			$date=$fetch_checkup_details['date'];
			$time=$fetch_checkup_details['time'];
			$eye_array=explode("|",$eye_details);
			$left=$eye_array[0];
			$right=$eye_array[1];
			
			echo '  
					<td>'.$sno.'</td>
					<td>'.$sid.'</td>
					<td>'.$name.'</td>
					';
					
			//query to get doctor name
			$get_doctor_name="
					SELECT  Name
					FROM users
					WHERE uId=".$doctor_id."";
			$exe_get_doctor_name=mysql_query($get_doctor_name);
			while($fetch_doc_name=mysql_fetch_array($exe_get_doctor_name))
				  {  
				   $doc_name=$fetch_doc_name['Name'];
				   	
				  echo'<td>'.$doc_name.'</td>
					<td>Left: <b>'.$left.'</b><b>  /6  </b>Right: <b>'.$right.'</b><b>/6</b></td>
					<td>'.$dental_details.'</td> 
					<td>'.$blood.'</td> 
					<td>'.$diseases.'</td> 
					<td>'.$remarks.'</td>   
					<td>'.$date.'</td> 
					<td>'.$time.'</td></tr>
					';
			  }	
		}//end outer while
			echo'</tbody>
    			<table>';
				echo'<div class="section last" align="right">
		   <a href="doctor/download_excel_admin.php?class_id='.$class_id.'&session='.$session_id.'" class="uibutton normal">Download Excel</a>
		     </div>';
				
echo'						
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';					
}

//function to called by doctor_view_all_student_checkup_details.php
function doctor_view_class()//show class
{
//Author : Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Class Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	
	//query to get class name
	$get_class="SELECT DISTINCT `cId`,`class_name`
	            FROM class_index 
	            ORDER BY class+0, section ASC";
	$q_res=mysql_query($get_class);
	echo' <div class="span4"><h3 style="color:">Kindly select class</h3></div>';
	echo '</h2>';
	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo '<ol class="rounded-list">';
	
	while($res=mysql_fetch_array($q_res))//looping for getting  class id,class
		{	
		$class=$res['class_name'];
		echo '<tr class="row-fluid" id="added_rows">
		<td><div class="row-fluid">
		<div class="span6">							                                                              
		<li><a href=doctor_view_all_checkup_details.php?&class_id='.$res['cId'].'>'.$res['class_name'].'</a>                                                    	                                 
		</div>
		</div><!-- end row-fluid  -->
		</td>
		</tr>';	 	 
		}		 		
	echo'						
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';					
}

//function to called by doctor_view_all_checkup_details.php
function  doctor_view_all_checkup_details()//show_all student checkup details
{
	
	//Author : Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Checkup Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
		
		$class_id=$_GET['class_id'];
		$doc_id=$_SESSION['user_id'];
		//query to get class name
		$cless_name="SELECT class_name
		             FROM `class_index` 
		  WHERE cId=".$class_id."";
		$exe_class_name=mysql_query($cless_name);
		$fetch_class=mysql_fetch_array($exe_class_name);
		$class_name=$fetch_class['class_name'];			 
		
		//query to get doctor name
		$get_doctor_name="
				SELECT  `Name`
				FROM users
				WHERE uId=".$doc_id."";
		$exe_get_doctor_name=mysql_query($get_doctor_name);
		$fetch_name=mysql_fetch_array($exe_get_doctor_name);
		$doc_name=$fetch_name['Name'];
        echo' <div style="float:left">
		<h4 style="color:green" align="left">Name:'.$doc_name.'</h4>
	    <h4 style="color:green" align="left">Class:'.$class_name.'</h4>
	    </div>';
		//get all the sessions
		$get_sessions=
			"SELECT DISTINCT session_table.sId,session_table.session_name
			FROM session_table
			
			INNER JOIN student_health_info_details
			ON student_health_info_details.session_id = session_table.sId
			
			INNER JOIN student_user
			ON student_user.sId =  student_health_info_details.sid
			
			INNER JOIN class
			ON class.sId = student_user.sId
			
			WHERE student_health_info_details.doc_id = ".$doc_id."
			
			AND class.classId = ".$class_id." ";
		$exe_sessions=mysql_query($get_sessions);
		while($sessions_fetch=mysql_fetch_array($exe_sessions))
			{ 
			$sno=0;
			
			
			echo'
			<table  class="table table-bordered table-striped" >
			<thead>';	
			if($sessions_fetch[1]=="")
			{	
			}
			else
			{
			$session_name=$sessions_fetch[1];
			echo '
			<tr>
			<th><h5 style="color:brown" align="center">S.No.<h5></th>
			<th><h5 style="color:brown" align="center">student Id<h5></th>
			<th><h5 style="color:brown" align="center">Name<h5></th>
			<th><h5 style="color:brown" align="center">Eye Details<h5></th>
			<th><h5 style="color:brown" align="center">Dental Details</h5></th>
			<th><h5 style="color:brown" align="center">ENT Details</h5></th>
			<th><h5 style="color:brown" align="center">Blood Group</h5></th>
			<th><h5 style="color:brown" align="center">Diseases</h5></th>
			<th><h5 style="color:brown" align="center">Remarks</h5></th>
			<th><h5 style="color:brown" align="center">Date</h5></th>
			<th><h5 style="color:brown" align="center">Time</h5></th>
			</tr>
			</thead>';
			}
			echo '
			<tbody align="center">
			';
		//get values
		 $get_values=
			"SELECT * , student_user.Name,student_user.sId
			FROM  student_health_info_details
			
			INNER JOIN student_user
			ON student_user.sId =  student_health_info_details.sid
			
			INNER JOIN class
			ON class.sId = student_user.sId
			
			WHERE class.classId = ".$class_id."
			AND  student_health_info_details.doc_id = ".$doc_id."
			AND  student_health_info_details.session_id = ".$sessions_fetch[0]."
			 ORDER BY student_health_info_details.sid ASC";
		$exe_values=mysql_query($get_values);
		while($fetch_val=mysql_fetch_array($exe_values))
			{    
			  $sno++;
			echo'<tr>';
			if($fetch_val['sId']=="")
			{
				
			}
			else
			{
			//$class_name=$fetch_val['class_name'];
			$sid=$fetch_val['sId'];
			$name=$fetch_val['Name'];
			$eye_details=$fetch_val['eye_details'];
			$dental_details=$fetch_val['dental_details'];
			$ent_details=$fetch_val['ent_details'];
			if($fetch_val['blood_group'] == "")
			{
			echo '<td>no data avai</td>';	 
			}
			else
			{
			$b=$fetch_val['blood_group'];	
			}
			$diseases=$fetch_val['diseases'];
			$remarks=$fetch_val['remarks'];
			$date=$fetch_val['date'];
			$time=$fetch_val['time'];
			$eye_array=explode("|",$eye_details);
			$left=$eye_array[0];
			$right=$eye_array[1];
			echo '
			<td>'.$sno.'</td>	
			<td>'.$sid.'</td>
			<td>'.$name.'</td>
		    <td>Left: <b>'.$left.'</b><b>  /6  </b>Right: <b>'.$right.'</b><b>/6</b></td>
			<td>'.$dental_details.'</td>
			<td>'.$ent_details.'</td>
			<td>'.$b.'</td>
			<td>'.$diseases.'</td>
			<td>'.$remarks.'</td>
			<td>'.$date.'</td>
			<td>'.$time.'</td>';	
			}
		    }
			
			echo '</tr>';	
			echo' <h4 align="center" style="color:green">'.$session_name.'</h4></div> ';
			echo '
		    </tbody>
		    </table>';	
		echo'<div class="section last" align="right">
		   <a href="doctor/download_excel_doc.php?class_id='.$class_id.'&&doc_id='.$doc_id.'&&session='.$sessions_fetch[0].'" class="uibutton normal"> Download Excel</a>
		     </div>';
		}//end of while
		
	  echo'						
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	  </div><!-- row-fluid -->
	  ';			
}
//function to called by doctor_add_all_check_details.php
function doctor_add_all_checkup_details()
{
	//Author  : Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Class Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';	
	if(isset($_GET['Successfully']))
		{
		echo "<h4 style=\"color:green\" align=\"center\">Successfully Added</h4>" ;
		}	
	//query to get class name
	$get_class="SELECT DISTINCT `cId`,`class_name`
	            FROM class_index 
	            ORDER BY class+0, section ASC";
	$q_res=mysql_query($get_class);
	echo' <div class="span4"><h3 style="color:">Kindly select class</h3></div>';
	echo '</h2>';
	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo '<ol class="rounded-list">';
	
	while($res=mysql_fetch_array($q_res))//looping for getting  class id,class
		{	
		$class=$res['class_name'];
		echo '<tr class="row-fluid" id="added_rows">
		<td><div class="row-fluid">
		<div class="span6">							                                                              
		<li><a href=doctor_add_class_student_checkup_details.php?&class_id='.$res['cId'].'>'.$res['class_name'].'</a>                                               	                                 
		</div>
		</div><!-- end row-fluid  -->
		</td>
		</tr>';	 	 
		}		 		
	echo'						
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';					
}

//function to claaed by doctor_add_class_student_checkup_details.php
function doctor_insert_checkup_details()//add all checkup details
{
	//Author  : Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Checkup Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';		
	$class_id=$_GET['class_id'];
	//query to get class name
	$cless_name="
		SELECT class_name
		FROM `class_index` 
		WHERE cId=".$class_id."";
	$exe_class_name=mysql_query($cless_name);
	$fetch_class=mysql_fetch_array($exe_class_name);
	$class_name=$fetch_class['class_name'];			
	echo' <h4 align="center" style="color:green">Class:'.$class_name.'</h4></div> ';
	echo'<form action="doctor/add_all_checkup_details.php" method="post" >';
	//query to get sname
	$get_student_name="
		SELECT class_index.class_name,student_user.Name,class.sId
		FROM class_index
		
		INNER JOIN class
		ON class.classId=class_index.cId
		
		INNER JOIN student_user
		ON class.sId=student_user.sId
		
		WHERE class_index.cId=".$class_id." AND class.session_id=".$_SESSION['current_session_id']."";
	$exe_student_name=mysql_query($get_student_name);
	$id_s='';
	$l=0;
	while($fetch_student_name=mysql_fetch_array($exe_student_name))
		{    
			$sid=$fetch_student_name['sId'];
			$sname=$fetch_student_name['Name'];
			echo '<table  class="table table-bordered table-striped" >
			<thead>
			<tr>
			<td><h5 style="color:brown" align="center">student Id</h5></td>
			<td><h5 style="color:brown" align="center">Name</h5></td>
			<td><h5 style="color:brown" align="center">Eye Details</h5></td>
			<th><h5 style="color:brown" align="center">Dental Details</h5></th>
			<th><h5 style="color:brown" align="center">ENT Details</h5></th>
			</thead>
			</tr>
			</thead>';
			echo '
			<tbody align="center">';
			echo'
			<tr>
			<td>'.$sid.'</td>
			<td>'.$sname.'</td>
			<input type="hidden" value="'.$sid.'"/ name="sid">
			<input type="hidden" value="'.$class_id.'"/ name="class_id">
			<td>
			LEFT<input type="text" name="left_'.$sid.'" /><br>
			RIGHT<input type="text" name="right_'.$sid.'" /></td>
			<td><textarea rows="3" columns="30" name="dental_'.$sid.'"></textarea></td>
			<td><textarea rows="3" columns="30" name="ent_'.$sid.'"></textarea></td>
			</tr>
			<tr><td></td><td></td>
			<th><h5 style="color:brown" align="center">Blood Group</h5></th>
			<th><h5 style="color:brown" align="center">Diseases</h5></th>
			<th><h5 style="color:brown" align="center">Remarks</h5></th>
			</tr><td></td><td></td>
			<td><select  data-placeholder="Select Blood..." class="chzn-select" tabindex="2" name="blood_'.$sid.'" >        
			<option value=""></option> 								
			<option value="A+">A+</option> 
			<option value="O+">O+</option> 
			<option value="B+">B+</option> 
			<option value="AB+">AB+</option> 
			<option value="A-">A-</option> 
			<option value="O-">O-</option> 
			<option value="B-">B-</option>
			<option value="AB-">AB-</option></td>
			<td><input type="text" name="remarks_'.$sid.'" /></td>
			<td><input type="text" name="diseases_'.$sid.'" /></td>
			</tr>
			'; 
			$id_s.=" ".$sid;
			$l++;
		}
	     echo '<input type="hidden" value="'.$l.'" name="max_val"/>';
	     echo '<input type="hidden" value="'.$id_s.'" name="id_stu"/>';
		echo'
		</tbody>
		</table>
		<div class="section last" align="center">
		<button class="uibutton submit" rel="1" type="submit">Submit</button>
		</div>
		</form>
	';
	echo'						
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';			
	
}

//function to called by doctor_edit_checkup_details.php
function  doctor_edit_cheakup_student_details()//Edit checkup details
{
    //Author  : Manish Mishra
	echo '<div class="row-fluid">                            
	<div class="span12  widget clearfix">                            
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Edit Checkup Details</span>
	</div><!-- End widget-header -->		
	<div class="widget-content"><br />
	';			
	$id=$_GET['id'];
	$class_id=$_GET['class_id'];
	$doc_id=$_GET['doc_id'];
	$sid=$_GET['sid'];
	$session_id=$_GET['session_id'];
	echo'<form action="doctor/update_checkup_details.php" method="post" >';
	//get values
	$get_values=
		"SELECT * , student_user.Name,student_user.sId
		FROM  student_health_info_details
		
		INNER JOIN student_user
		ON student_user.sId =  student_health_info_details.sid
		
		INNER JOIN class
		ON class.sId = student_user.sId
		
		WHERE class.classId = ".$class_id."
		AND  student_health_info_details.doc_id = ".$doc_id."
		AND  student_health_info_details.session_id = ".$session_id."
		AND  student_health_info_details.sid = ".$sid."
		AND  student_health_info_details.id = ".$id."
		";
		$exe_values=mysql_query($get_values);
		while($fetch_val=mysql_fetch_array($exe_values))
			{    
			$id=$fetch_val['id'];
			$sid=$fetch_val['sId'];
			$name=$fetch_val['Name'];
			$eye_details=$fetch_val['eye_details'];
			$dental_details=$fetch_val['dental_details'];
			$ent_details=$fetch_val['ent_details'];
			$blood=$fetch_val['blood_group'];	
			$diseases=$fetch_val['diseases'];
			$remarks=$fetch_val['remarks'];
			$date=$fetch_val['date'];
			$time=$fetch_val['time'];
			$eye_array=explode("|",$eye_details);
			$left=$eye_array[0];
			$right=$eye_array[1];
			echo '<table  class="table table-bordered table-striped" >
			<thead>
			<tr>
			<td><h5 style="color:brown" align="center">student Id</h5></td>
			<td><h5 style="color:brown" align="center">Name</h5></td>
			<td><h5 style="color:brown" align="center">Eye Details</h5></td>
			<th><h5 style="color:brown" align="center">Dental Details</h5></th>
			<th><h5 style="color:brown" align="center">ENT Details</h5></th>
			</thead>
			</tr>
			</thead>';
			echo '
			<tbody align="center">';
			echo'
			<tr>
			<td>'.$sid.'</td>
			<td>'.$name.'</td>
			<input type="hidden" value="'.$sid.'"/ name="sid">
			<input type="hidden" value="'.$id.'"/ name="id">
			<input type="hidden" value="'.$class_id.'"/ name="class_id">
			<input type="hidden" value="'.$session_id.'"/ name="session_id">
			<input type="hidden" value="'.$doc_id.'"/ name="doc_id">
			<td>
			LEFT<input type="text"  value="'.$left.'"name="left" /><br>
			RIGHT<input type="text" value="'.$right.'" name="right" /></td>
			<td><textarea rows="3" columns="30"   name="dental">'.$dental_details.'</textarea></td>
			<td><textarea rows="3" columns="30"   name="ent">'.$ent_details.'</textarea></td>
			</tr>
			<tr><td></td><td></td>
			<th><h5 style="color:brown" align="center">Blood Group</h5></th>
			<th><h5 style="color:brown" align="center">Diseases</h5></th>
			<th><h5 style="color:brown" align="center">Remarks</h5></th>
			</tr><td></td><td></td>
	        <td><select  data-placeholder="Select Blood..." class="chzn-select" tabindex="2"  name="blood" >
			<option value="'.$blood.'">'.$blood.'</option> 								
			<option value="A+">A+</option> 
			<option value="O+">O+</option> 
			<option value="B+">B+</option> 
			<option value="AB+">AB+</option> 
			<option value="A-">A-</option> 
			<option value="O-">O-</option> 
			<option value="B-">B-</option>
			<option value="AB-">AB-</option></td>
			<td><input type="text" value="'.$diseases.'" name="diseases" /></td>
			<td><input type="text" value="'.$remarks.'" name="remarks" /></td>
			</tr>
			'; 	
		   }//end of while   
	echo'
	</tbody>
	</table>
	<div class="section last" align="center">
	<button class="uibutton submit" rel="1" type="submit">Submit</button>
	</div>
	</form>
	';	
     echo'						
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';			
}
?>