<?php
//function show hostel details
function view_hostel_details()
{
	//Author : Anil Kumar*
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Hostel Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<table  class="table table-bordered table-striped" id="dataTable" >';
		if(isset($_GET['edit']))
		{
			echo '<p style="color:GREEN" align="center"><b>HOSTEL DETAILS UPDATED SUSSESSFULLY</b></p>';	
		}
	if(isset($_GET['delete']))
		{
			echo '<p style="color:RED" align="center"><b>HOSTEL DETAILS DELETED SUSSESSFULLY</b></p>';	
		}
	echo '<thead>
	<tr>
	<th>Hostel Name</th>
	<th>Type</th>
	<th>Address</th>
	<th>Action</th>
	</tr>
	</thead>
	<tbody align="center">
	';
	$sn=1;	
	$hostel="SELECT *
	         FROM hostel_details
			 ";
	$exe_hostel=mysql_query($hostel);
	while($fetch=mysql_fetch_array($exe_hostel))
		{
			echo '<tr id="'.$fetch[0].'">
			<td><a href="hostel_view_room_details.php?hid='.$fetch[0].'">'.$fetch[1].'</a></td>
			<td>'.$fetch[2].'</td>
			<td>'.$fetch[3].'</td>
			<td class=" ">
			<span class=""><a href="#" onclick="edit_hostel_details('.$fetch[0].',\''.$fetch[1].'\',\''.$fetch[2].'\',
			\''.$fetch[3].'\')"; title="Edit">
			<img src="images/icon/icon_edit.png"></a></span> 
			<span class="tip"><a href="hostel/delete_hostel_details.php?id='.$fetch[0].'" original-title="Delete">
			<img src="images/icon/icon_delete.png"></a></span> 
			</td>
			</tr>';
			$sn++;
		}
	echo'
	<a class="btn btn-inverse" href="hostel_add_new_hostel_details.php"><i class="icon-info-sign icon-white">
	</i>Add New Hostel</a>
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
	}

//function add new hostel details
function add_new_hostel_details()
{
	//Author : Anil Kumar*
	echo'  <div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ADD NEW HOSTEL</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	//if mysql query affected rows then get message enquiry successfull
	if(isset($_GET['add']))
		{
			echo '<p style="color:GREEN" align="center"><b>HOSTEL DETAILS ADDED SUSSESSFULLY</b></p>';	
		}
	if(isset($_GET['delete']))
		{
			echo '<p style="color:RED" align="center"><b>HOSTEL DETAILS DELETED SUSSESSFULLY</b></p>';	
		}
	echo'  
	<div class="load_page">
	<div class="formEl_b">	
	<form id="validation_demo" action="hostel/insert_new_hostel.php" method="get">
	<div class="section ">
	<label>Hostel Name<small></small></label>   
	<div> 
	<input type="text" class="validate[required] " name="h_name" maxlength="180" >
	</div>
	</div>
	<div class="section ">
	<label> Type <small></small></label>   
	<div> 
	<select name="h_type">
	<option>Common Hostel</option>
	<option>Boy`s Hostel</option>
	<option>Girl`s Hostel</option>
	</select>
	</div>
	</div>
	<div class="section ">
	<label>Address<small></small></label>   
	<div> 
	<input type="text" name="info">
	</div>
	</div>
	<div class="section last">
	<div>
	<button class="btn btn-small btn-info"><i class="icon-info-sign icon-white">
	</i>SUBMIT</button>
	<a class="btn btn-small btn-info" href="hostel_view_hostel_details.php"><i class="icon-info-sign icon-white">
	</i>BACK</a>
	</div>
	</div>
	</form>
	</div>								
	</div>
	</div>
	</div>	
	</div><!--tab1--> ';
}

//function select hostel
function select_hostel()
{
	//Author : Anil Kumar*
	//query get hostel name and details
	$hostel_name="SELECT *
	              FROM hostel_details";
	$exe_hostel_name=mysql_query($hostel_name);
	echo'  <div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ADD NEW ROOM DETAILS</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	
	echo'  
	<div class="load_page">
	<div class="formEl_b">	
	<form id="validation_demo" action="hostel_view_room_details.php" method="get">
	<div class="section ">
	<label>SELECT HOSTEL NAME <small></small></label>   
	<div> 
	<select name="hid">
	<option>SELECT HOSTEL</option>';
	while($fetch_hostel=mysql_fetch_array($exe_hostel_name))
		{
			echo '
			<option value="'.$fetch_hostel[0].'">'.$fetch_hostel[1].'</option>';
		}
	echo '</select>
	</div>
	</div>
	
	<div class="section last">
	<div>
	<button class="btn btn-small btn-info"><i class="icon-info-sign icon-white">
	</i>SUBMIT</button>
	</div>
	</div>
	</form>
	</div>								
	</div>
	</div>
	</div>	
	</div><!--tab1--> ';
}
//function show room details
function view_room_details()
{
	//Author : Anil Kumar*
	//get hostel id
	$hid=$_GET['hid'];
	//query get hostel name and type 
	$name="SELECT *
	       FROM hostel_details
		   WHERE ID=".$hid."";
	$exe_name=mysql_query($name);
	$fetch_h_name=mysql_fetch_array($exe_name);
	$h_name=$fetch_h_name[1];
	$type=$fetch_h_name[2];
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ROOM DETAILS</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<table  class="table table-bordered table-striped" id="dataTable" >';
	if(isset($_GET['error']))
	{
		echo '<p style="color:RED" align="center"><b> UPDATION ERROR</b></p>';	
	}
	if(isset($_GET['allocate']))
	{
		echo '<p style="color:RED" align="center"><b>DELETE STUDENT ALLOCATION BEFORE UPDATION</b></p>';	
	}
	if(isset($_GET['update']))
		{
			echo '<p style="color:GREEN" align="center"><b>ROOM DETAILS UPDATED SUSSESSFULLY</b></p>';	
		}
	echo '<thead>
	<tr>
	<th>S. no.</th>
	<th>Room No.</th>
	<th>Number of Bed/Room</th>
	<th>Availability</th>
	<th>Rent</th>
	<th>Action</th>
	</tr>
	</thead>
	<tbody align="center">
	';	
	$sn=1;
	$availability=0;
	//query get room details from hostel_room_details table
	$room="SELECT *
	       FROM hostel_room_details
		   WHERE h_id=".$hid."";
	$exe_room=mysql_query($room);
	while($fetch_room=mysql_fetch_array($exe_room))
		{
			$bed=$fetch_room[3];
			//query get availabilite details of room allocation
			$hostel="SELECT COUNT(roomid) AS room
			         FROM hostel_room_allocation
					 WHERE roomid=".$fetch_room[0]."";
			$exe_hostel=mysql_query($hostel);
			$fetch_hostel=mysql_fetch_array($exe_hostel);
			//calculate availability
			$availability=$bed-$fetch_hostel[0];
			echo '<tr>
			<td>'.$sn++.'</td>
			<td><a href="admin_show_left_hostel_room.php?room_id='.$fetch_room[0].'&&hostel_id='.$hid.'">'.$fetch_room[2].'</a></td>
			<td>'.$fetch_room[3].'</td>
			<td>'.$availability.'</td>
			<td>'.$fetch_room[4].'</td>
			<td class=" ">
			<span class=""><a href="hostel_room_details_update.php?rid='.$fetch_room[0].'&hid='.$hid.'"  title="Edit">
			<img src="images/icon/icon_edit.png"></a></span> 
			<span class="tip"><a href="hostel/delete_room.php?id='.$fetch_room[0].'&hid='.$hid.'" original-title="Delete">
			<img src="images/icon/icon_delete.png"></a></span> 
			</td>
			</tr>';
			
		}
	echo'
	<a class="btn btn-inverse" href="hostel_add_new_room_details.php?hid='.$hid.'"><i class="icon-info-sign icon-white">
	</i>Add New Room</a>
	<a class="btn btn-inverse" href="hostel_select_classes.php?hid='.$hid.'"><i class="icon-info-signicon-white">
	</i>Room Allocation</a>
	<h5 style="color:green" align="center">Hostel Name : '.$h_name.'</h5>
	<h5 style="color:green" align="center">Type : '.$type.'</h5>
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}

//function add new room details
function add_new_room_details()
{
	//Author : Anil Kumar*
	$hid=$_GET['hid'];
	//query get hostel name and details
	$hostel_name="SELECT *
	              FROM hostel_details";
	$exe_hostel_name=mysql_query($hostel_name);
	echo'  <div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ADD NEW ROOM DETAILS</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	//if mysql query affected rows then get message enquiry successfull
	if(isset($_GET['exit']))
		{
			$exist_room=$_GET['exit_room'];
			echo '<p style="color:red" align="center"><b>ROOM NO. ('.$exist_room.') ALREADY EXIST</b></p>';	
		}
	if(isset($_GET['select']))
		{
			echo '<p style="color:red" align="center"><b>SELECT HOSTEL NAME PROPERLY</b></p>';	
		}
	if(isset($_GET['add']))
		{
			echo '<p style="color:GREEN" align="center"><b>ROOM DETAILS ADDED SUSSESSFULLY</b></p>';	
		}
	if(isset($_GET['delete']))
		{
			echo '<p style="color:RED" align="center"><b>ROOM DETAILS DELETED SUSSESSFULLY</b></p>';	
		}
	echo'  
	<div class="load_page">
	<div class="formEl_b">	
	<form id="validation_demo" action="hostel/insert_new_room.php?hid='.$hid.'" method="get">
	<div class="section ">
	<label>SELECT HOSTEL NAME <small></small></label>   
	<div> 
	<select name="h_name">
	<option>SELECT HOSTEL</option>';
	while($fetch_hostel=mysql_fetch_array($exe_hostel_name))
		{
			echo '
			<option value="'.$fetch_hostel[0].'">'.$fetch_hostel[1].'</option>';
		}
	echo '</select>
	</div>
	</div>
	<div class="section ">
	<label>Enter Room no.<small></small></label>   
	<div> 
	<input type="text" class="validate[required] " name="room">
	</div>
	</div>
	<div class="section ">
	<label>Number of Bed/room<small></small></label>   
	<div> 
	<input type="text" class="validate[required] " name="bed">
	</div>
	</div>
	<div class="section ">
	<label>Rent<small></small></label>   
	<div> 
	<input type="text" class="validate[required] " name="rent">
	</div>
	</div>
	<div class="section ">
	<label>how many room?<small></small></label>   
	<div> 
	<input type="text" class="validate[required] " name="hmr">
	</div>
	</div>
	<div class="section last">
	<div>
	<button class="btn btn-inverse"><i class="icon-info-sign icon-white">
	</i>Submit</button>
	<a class="btn btn-inverse" href="hostel_view_room_details.php?hid='.$hid.'"><i class="icon-info-sign icon-white">
	</i>Go Back</a>
	</div>
	</div>
	</form>
	</div>								
	</div>
	</div>
	</div>	
	</div><!--tab1--> ';
}
//function add new room details
function hostel_room_details_update()
{
	//Author : Anil Kumar*
	$rid=$_GET['rid'];
	$hid=$_GET['hid'];
	//query get room details and update
	$hostel_name="SELECT *
	              FROM hostel_room_details
				  WHERE `id`=".$rid."";
	$exe_hostel_name=mysql_query($hostel_name);
	echo'  <div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ADD NEW ROOM DETAILS</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	echo'  
	<div class="load_page">
	<div class="formEl_b">	
	<form id="validation_demo" action="hostel/update_room_details.php?rid='.$rid.'&hid='.$hid.'" method="get">';
    while($fetch_room=mysql_fetch_array($exe_hostel_name))
		{
	        echo '
			<div class="section ">
			<input type="hidden" value="'.$rid.'" class="validate[required] " name="rid">
			</div>
			<div class="section ">
			<input type="hidden" value="'.$hid.'" class="validate[required] " name="hid">
			</div>
			<div class="section ">
			<label>Room no.<small></small></label>   
			<div> 
			<input type="text" value="'.$fetch_room[2].'" class="validate[required] " name="room">
			</div>
			</div>
			<div class="section ">
			<label>Number of Bed/room<small></small></label>   
			<div> 
			<input type="text" value="'.$fetch_room[3].'" class="validate[required] " name="bed">
			</div>
			</div>
			<div class="section ">
			<label>Rent<small></small></label>   
			<div> 
			<input type="text" value="'.$fetch_room[4].'" class="validate[required] " name="rent">
			</div>
			</div>';
		}
	echo '<div class="section last">
	<div>
	<button class="btn btn-inverse"><i class="icon-info-sign icon-white">
	</i>Submit</button>
	<a class="btn btn-inverse" href="hostel_view_room_details.php?hid='.$hid.'"><i class="icon-info-sign icon-white">
	</i>Go Back</a>
	</div>
	</div>
	</form>
	</div>								
	</div>
	</div>
	</div>	
	</div><!--tab1--> ';
}

//function show all classes
function select_class()
{
	//Author : Anil Kumar*
	//get hostel id
	$hid=$_GET['hid'];
	echo'  <div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ADD NEW HOSTEL</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<h5 align="right"><a href="hostel_view_room_details.php?hid='.$hid.'" 
	class="btn btn-inverse">Go Back </a></h5>';
	//query to get all class
	$query_class="SELECT *
				 FROM class_index
				  ";
	$execute_class=mysql_query($query_class);
	echo '
	<div class="section last">
	<label>SELECT CLASS</label>
	<div > <ol class="rounded-list">';				  
	while($fetch_class=mysql_fetch_array($execute_class))
		{ 
			echo' <li><a href="hostel_show_student_details.php?hid='.$hid.'&cid='.$fetch_class['cId'].'
			">'.$fetch_class['class_name'].'</a></li> ';	
		}
	echo'
	</ol>
	</div>
	</div>
	</div>
	</div>	
	</div><!--tab1--> '; 	
}

//function show all student of select class and allocate hostel of any student
function hostel_show_student_details()
{
	//Author : Anil Kumar*
	//get hostel id
	$hid=$_GET['hid'];
	//get class id from select_class() function
	$cid=$_GET['cid'];
	$class="SELECT class_name
	        FROM class_index
			WHERE cId=".$cid."";
	$execlass=mysql_query($class);
	$fetch_c=mysql_fetch_array($execlass);
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Hostel Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<table  class="table table-bordered table-striped" id="dataTable" >
	<h5 align="right"><a href="hostel_view_room_details.php?hid='.$hid.'" 
	class="btn btn-inverse">Go Back </a></h5>
	<thead>
	<tr>
	<th>S. No.</th>
	<th>Admission No.</th>
	<th>Student Name</th>
	<th>Father`s Name</th>
	<th>Class</th>
	</tr>
	</thead>
	<tbody align="center">
	';
	if(isset($_GET['allot']))
		{
			echo '<h5 style="color:green" align="center"><b>Hostel Room Allocation Successfull</b></h5>';	
		}
	$sn=1;	
	//query get student name and addmissin no from student_user table
	$hostel="SELECT *
	         FROM student_user
			 
			 INNER JOIN class
			 ON class.sId=student_user.sId
			 
			 WHERE class.classId=".$cid."
	         
			 ";
	$exe_hostel=mysql_query($hostel);
	while($fetch=mysql_fetch_array($exe_hostel))
		{
			echo '<tr id="'.$fetch[0].'">
			<td>'.$sn.'</td>
			<td><a href="hostel_room_details_allocation.php?hid='.$hid.'&sid='.$fetch[0].'&addmission='.$fetch[10].'&cid='.$cid.'">'.$fetch[10].'</a></td>
			<td>'.$fetch[1].'</td>
			<td>'.$fetch[5].'</td>
			<td>'.$fetch_c[0].'</td>';
			/*<td class=" ">
			<span class=""><a href="#" onclick="edit_hostel_details('.$fetch[0].',\''.$fetch[1].'\',\''.$fetch[2].'\',
			\''.$fetch[3].'\')"; title="Edit">
			<img src="images/icon/icon_edit.png"></a></span> 
			<span class="tip"><a href="hostel/delete_hostel_details.php?id='.$fetch[0].'" original-title="Delete">
			<img src="images/icon/icon_delete.png"></a></span> 
			</td>*/
			echo '</tr>';
			$sn++;
		}
	echo'
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';		
}

//function show hostel name and select hostel name and allocate room
function hostel_select_allocation()
{
	//Author : Anil Kumar*
	
	//query get hostel name and details
	$hostel_name="SELECT *
	              FROM hostel_details";
	$exe_hostel_name=mysql_query($hostel_name);
	echo'  <div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ADD NEW ROOM DETAILS</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	if(isset($_GET['select']))
		{
			echo '<p style="color:RED" align="center"><b>SELECT HOSTEL PROPERLY</b></p>';	
		}
	echo'  
	<div class="load_page">
	<div class="formEl_b">	
	<form id="validation_demo" action="hostel_select_classes.php" method="get">
	<div class="section ">
	<label>SELECT HOSTEL NAME <small></small></label>   
	<div> 
	<select name="hid">
	<option>SELECT HOSTEL</option>';
	while($fetch_hostel=mysql_fetch_array($exe_hostel_name))
		{
			echo '
			<option value="'.$fetch_hostel[0].'">'.$fetch_hostel[1].'</option>';
		}
	echo '</select>
	</div>
	</div>   
	<div class="section last">
	<div>
	<button class="btn btn-inverse"><i class="icon-info-sign icon-white">
	</i>SUBMIT</button>
	</div>
	</div>
	</form>
	</div>								
	</div>
	</div>
	</div>	
	</div><!--tab1--> ';
}
//function show room details allocation of student
//function show room details
function hostel_room_details_allocation()
{
	//Author : Anil Kumar*
	//get hostel id
	$hid=$_GET['hid'];
	$sid=$_GET['sid'];
	$addmission=$_GET['addmission'];
	$cid=$_GET['cid'];
	//if condition check if hostel name is blank then go preveious page
	if($hid=="SELECT HOSTEL")
		{
			header("location:hostel_select_allocation.php?select&sid='.$sid.'&addmission='.$addmission.'&cid='.$cid.'");
		}
	//query get hostel name and type 
	$name="SELECT *
	       FROM hostel_details
		   WHERE id=".$hid."";
	$exe_name=mysql_query($name);
	$fetch_h_name=mysql_fetch_array($exe_name);
	$h_name=$fetch_h_name[1];
	$type=$fetch_h_name[2];
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ROOM DETAILS</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<table  class="table table-bordered table-striped" id="dataTable" >
	<h5 align="right"><a href="hostel_view_room_details.php?hid='.$hid.'" 
	class="btn btn-inverse">Go Back </a></h5>
	<thead>
	<tr>
	<th>S. no.</th>
	<th>Room No.</th>
	<th>Number of Bed/Room</th>
	<th>Availability</th>
	<th>Rent</th>
	<th>Action</th>
	</tr>
	</thead>
	<tbody align="center">
	';	
	$sn=1;
	//query get room details from hostel_room_details table
	$room="SELECT *
	       FROM hostel_room_details
		   WHERE h_id=".$hid."";
	$exe_room=mysql_query($room);
	while($fetch_room=mysql_fetch_array($exe_room))
		{
			$bed=$fetch_room[3];
			//query get availabilite details of room allocation
			$hostel="SELECT COUNT(roomid) AS room
			         FROM hostel_room_allocation
					 WHERE roomid=".$fetch_room[0]."";
			$exe_hostel=mysql_query($hostel);
			$fetch_hostel=mysql_fetch_array($exe_hostel);
			//calculate availability
			$availability=$bed-$fetch_hostel[0];
			echo '<tr>
			<td>'.$sn++.'</td>
			<td>'.$fetch_room[2].'</td>
			<td>'.$fetch_room[3].'</td>
			<td>'.$availability.'</td>
			<td>'.$fetch_room[4].'</td>';
			//if availability == 0 then room nt allotted
			if($availability==0)
				{
					echo '<td style="color:red">Full</a></td></tr>';
				}
			else
				{
					echo '<td><a href="hostel/insert_room_allocation.php?sid='.$sid.'&hid='.$hid.'&add='.$addmission.'
					&room='.$fetch_room[2].'&bed='.$fetch_room[3].'&rent='.$fetch_room[4].'&cid='.$cid.'">Allocation</a></td>
					</tr>';
				}
		}
	echo'
	<h5 style="color:green" align="center">Hostel Name : '.$h_name.'</h5>
	<h5 style="color:green" align="center">Type : '.$type.'</h5>
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}

//function show session and duration
//select session and duration and view all allocated students
function hostel_view_room_allocation_class_wise()
{
	//Author : Anil Kumar*

    //query get session name and id from session table
	$session_name="SELECT sId, session_name
	               FROM session_table
				   ORDER BY session_name DESC";
	$exe_session=mysql_query($session_name);
	echo'  <div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ADD NEW ROOM DETAILS</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	if(isset($_GET['blanks']))
		{
			echo '<p style="color:RED" align="center"><b>SELECT All DETAILS PROPERLY</b></p>';	
		}
	echo'  
	<div class="load_page">
	<div class="formEl_b">	
	<form id="validation_demo" action="hostel_view_room_allocation_details.php" method="get">
	<div class="section ">
	<label>Slect Session <small></small></label>   
	<div> 
	<select name="session">
	<option value="">SELECT SESSION</option>';
	while($fetch_session=mysql_fetch_array($exe_session))
		{
			echo '
			<option value="'.$fetch_session[0].'">'.$fetch_session[1].'</option>';
		}
	echo '</select>
	</div>
	</div>';
	echo '<div class="section ">
	<label>Slect Level<small></small></label> 
	<div> 
	<select name="level" id="level" onchange="show_room_allocation_details(this.value)">
	<option value="">SELECT LEVEL</option>
	<option value="1">All School</option>  
	<option value="2">According to Class</option> 
	<option value="3">According to Hostel</option> 
	</select>
	</div>
	</div>
	<div class="section" id="2" style="display:none">
	<label>SELECT CLASS</label>
	<div>
	<select name="class">';
	//get class name and id from class index table
	$get_name_class=
				   "SELECT cId,class , class_name
				    FROM class_index";
	$exe_class=mysql_query($get_name_class);
	while($fetch_class=mysql_fetch_array($exe_class))
		{
			echo '
			<option value="'.$fetch_class[0].'">'.$fetch_class[2].'</option>
			';
		} 
	echo '</select>
	</div>
	</div> ';
	echo '	<div class="section" id="3" style="display:none">
	<label>SELECT HOSTEL</label>
	<div>
	<select name="h_name">';
	//query get hostel name and details
	$hostel_name="SELECT *
	              FROM hostel_details";
	$exe_hostel_name=mysql_query($hostel_name);
	while($fetch_room=mysql_fetch_array($exe_hostel_name))
		{
			echo '
			<option value="'.$fetch_room[0].'">'.$fetch_room[1].'</option>
			';
		} 
	echo '</select>
	</div>
	</div> ';   
	echo '<div class="section last">
	<div>
	<button class="btn btn-inverse"><i class="icon-info-sign icon-white">
	</i>SUBMIT</button>
	</div>
	</div>
	</form>
	</div>								
	</div>
	</div>
	</div>	
	</div><!--tab1--> ';
}
//function show room allocation details of student---==-
function hostel_view_room_allocation_details()
{
	//Author : Anil Kumar*
    //get session and class details
	$session_id=$_GET['session'];
	$level=$_GET['level'];
	//condition check select box select properly or not
	if(($session_id=="")||($level==""))
		{
			header('location:hostel_view_room_allocation_class_wise.php?blanks');	
		}
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ROOM ALLOCATION DETAILS</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th>S. no.</th>
	<th>Admission No.</th>
	<th>Student Name</th>
	<th>Class Name</th>
	<th>Hostel Name</th>
	<th>Room No.</th>
	<th>Bed/Student</th>
	<th>Rent</th>
	</tr>
	</thead>
	<tbody align="center">
	';
	//check condition of level 
	if($level==1)
		{	
			$sn=1;
			//query get room allocation details
			$room_allocation="SELECT *
				   FROM hostel_room_allocation
					WHERE session_id=".$session_id."";
			$exe_room=mysql_query($room_allocation);
			while($fetch_room=mysql_fetch_array($exe_room))
				{
					//get student name from student user
					$student_name="SELECT Name
								   FROM student_user
								   WHERE sId=".$fetch_room[6]."";
					$exe_name=mysql_query($student_name);
					$fetch_name=mysql_fetch_array($exe_name);
					//query get hoste name from hostel detalls
					$h_name="SELECT hostel_name
								   FROM hostel_details
								   WHERE id=".$fetch_room[1]."";
					$exe_h_name=mysql_query($h_name);
					$fetch_h_name=mysql_fetch_array($exe_h_name);
					//query get class of student
					$class="SELECT class_index.class_name
							FROM class_index
							INNER JOIN class
							ON class.classId=class_index.cId
							WHERE class.sId=".$fetch_room[6]."";
					$exe_class=mysql_query($class);
					$fetch_class=mysql_fetch_array($exe_class);
					echo '<tr>
					<td>'.$sn++.'</td>
					<td>'.$fetch_room[7].'</td>
					<td>'.$fetch_name[0].'</td>
					<td>'.$fetch_class[0].'</td>
					<td>'.$fetch_h_name[0].'</td>
					<td>'.$fetch_room[3].'</td>
					<td>'.$fetch_room[4].'</td>
					<td>'.$fetch_room[5].'</td>
					</tr>';
				
				}
		}
		else if($level==2)
			{
				$cid=$_GET['class'];
							$sn=1;
			//query get room allocation details
			$room_allocation="SELECT *
				   FROM hostel_room_allocation
				    WHERE session_id=".$session_id."
					AND cid=".$cid."";
			$exe_room=mysql_query($room_allocation);
			while($fetch_room=mysql_fetch_array($exe_room))
				{
					//get student name from student user
					$student_name="SELECT Name
								   FROM student_user
								   WHERE sId=".$fetch_room[6]."";
					$exe_name=mysql_query($student_name);
					$fetch_name=mysql_fetch_array($exe_name);
					//query get hoste name from hostel detalls
					$h_name="SELECT hostel_name
								   FROM hostel_details
								   WHERE id=".$fetch_room[1]."";
					$exe_h_name=mysql_query($h_name);
					$fetch_h_name=mysql_fetch_array($exe_h_name);
					//query get class of student
					$class="SELECT class_index.class_name
							FROM class_index
							INNER JOIN class
							ON class.classId=class_index.cId
							WHERE class.sId=".$fetch_room[6]."";
					$exe_class=mysql_query($class);
					$fetch_class=mysql_fetch_array($exe_class);
					echo '<tr>
					<td>'.$sn++.'</td>
					<td>'.$fetch_room[7].'</td>
					<td>'.$fetch_name[0].'</td>
					<td>'.$fetch_class[0].'</td>
					<td>'.$fetch_h_name[0].'</td>
					<td>'.$fetch_room[3].'</td>
					<td>'.$fetch_room[4].'</td>
					<td>'.$fetch_room[5].'</td>
					</tr>';
				}
				
			}
			else if($level==3)
			{
				
			 $hid=$_GET['h_name'];
			 $sn=1;
			//query get room allocation details
			$room_allocation="SELECT *
				   FROM hostel_room_allocation
				    WHERE session_id=".$session_id."
					AND hid=".$hid."";
			$exe_room=mysql_query($room_allocation);
			while($fetch_room=mysql_fetch_array($exe_room))
				{
					//get student name from student user
					$student_name="SELECT Name
								   FROM student_user
								   WHERE sId=".$fetch_room[6]."";
					$exe_name=mysql_query($student_name);
					$fetch_name=mysql_fetch_array($exe_name);
					//query get hoste name from hostel detalls
					$h_name="SELECT hostel_name
								   FROM hostel_details
								   WHERE id=".$fetch_room[1]."";
					$exe_h_name=mysql_query($h_name);
					$fetch_h_name=mysql_fetch_array($exe_h_name);
					//query get class of student
					$class="SELECT class_index.class_name
							FROM class_index
							INNER JOIN class
							ON class.classId=class_index.cId
							WHERE class.sId=".$fetch_room[6]."";
					$exe_class=mysql_query($class);
					$fetch_class=mysql_fetch_array($exe_class);
					echo '<tr>
					<td>'.$sn++.'</td>
					<td>'.$fetch_room[7].'</td>
					<td>'.$fetch_name[0].'</td>
					<td>'.$fetch_class[0].'</td>
					<td>'.$fetch_h_name[0].'</td>
					<td>'.$fetch_room[3].'</td>
					<td>'.$fetch_room[4].'</td>
					<td>'.$fetch_room[5].'</td>
					</tr>';
				}
				
			}
	echo'
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}
//function show session and duration
//select session and duration and view all fee paid students
function select_session_duration()
{
     //Author : Anil Kumar*
    //query get session name and id from session table
	$session_name="SELECT sId, session_name
	               FROM session_table
				   ORDER BY session_name DESC";
	$exe_session=mysql_query($session_name);
		//query get hostel name and details
	$hostel_name="SELECT *
	              FROM hostel_details";
	$exe_hostel_name=mysql_query($hostel_name);

	echo'  <div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ADD NEW ROOM DETAILS</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	if(isset($_GET['select']))
		{
			echo '<p style="color:RED" align="center"><b>SELECT All DETAILS PROPERLY</b></p>';	
		}
	echo'  
	<div class="load_page">
	<div class="formEl_b">	
	<form id="validation_demo" action="hostel_fee_paid_students_details.php" method="get">
	<div class="section ">
	<label>Slect Session <small></small></label>   
	<div> 
	<select name="session">
	<option value="">SELECT SESSION</option>';
	while($fetch_session=mysql_fetch_array($exe_session))
		{
			echo '
			<option value="'.$fetch_session[0].'">'.$fetch_session[1].'</option>';
		}
	echo '</select>
	</div>
	</div>
	<div class="section ">
	<label>Slect Hostel Name<small></small></label> 
	<div> 
	<select name="hid">
	<option value="">SELECT HOSTEL</option>';
	while($fetch_hostel=mysql_fetch_array($exe_hostel_name))
		{
			echo '
			<option value="'.$fetch_hostel[0].'">'.$fetch_hostel[1].'</option>';
		}
	echo '</select>
	</div>
	</div>     
	<div class="section ">
	<label>Slect Session <small></small></label>   
	<div> 
	<select name="duration">
	<option value="">SELECT DURATION</option>';
	//query get duratio9n from fee generated session
    $duration="SELECT *
	           FROM fee_generated_sessions";
    $exe_duration=mysql_query($duration);
	while($fetch_duration=mysql_fetch_array($exe_duration))
		{
			echo '
			<option value="'.$fetch_duration[0].'">'.$fetch_duration[2].'</option>';
		}
	echo '</select>
	</div>
	</div> 
		<div class="section ">
	<label>Slect Level<small></small></label> 
	<div> 
	<select name="level" id="level" onchange="show_room_level_details(this.value)">
	<option value="">SELECT LEVEL</option>
	<option value="1">All School</option>  
	<option value="2">According to Class</option> 
	<option value="3">According to Room</option> 
	</select>
	</div>
	</div>
	<div class="section" id="2" style="display:none">
	<label>SELECT CLASS</label>
	<div>
	<select name="class">';
	//get class name and id from class index table
	$get_name_class=
				   "SELECT cId,class , class_name
				    FROM class_index";
	$exe_class=mysql_query($get_name_class);
	while($fetch_class=mysql_fetch_array($exe_class))
		{
			echo '
			<option value="'.$fetch_class[0].'">'.$fetch_class[2].'</option>
			';
		} 
	echo '</select>
	</div>
	</div> ';
	echo '	<div class="section" id="3" style="display:none">
	<label>SELECT ROOM</label>
	<div>
	<select name="room">';
	//get room no  and id from class index table
	$get_room=
			   "SELECT id, room_no
				FROM hostel_room_details";
	$exe_room=mysql_query($get_room);
	while($fetch_room=mysql_fetch_array($exe_room))
		{
			echo '
			<option value="'.$fetch_room[0].'">'.$fetch_room[1].'</option>
			';
		} 
	echo '</select>
	</div>
	</div> ';   
	echo '<div class="section last">
	<div>
	<button class="btn btn-inverse"><i class="icon-info-sign icon-white">
	</i>SUBMIT</button>
	</div>
	</div>
	</form>
	</div>								
	</div>
	</div>
	</div>	
	</div><!--tab1--> ';
}
//function show fee_paid students details
function hostel_fee_paid_students_details_view()
{
	//Author : Anil Kumar*
	
    //get duration and session
	$session_id=$_GET['session']; 
	$duration_id=$_GET['duration'];
	$hid=$_GET['hid'];
	$level=$_GET['level'];
	//check any select box is blanks or not
	if(($session_id=="")||($duration_id=="")||($hid=="")||($level==""))
		{
			header("location:hostel_fee_paid_session_duration.php?select");
		}
	//get hostel name
	$hostel_name="SELECT *
	              FROM hostel_details
				  WHERE id=$hid";
	$exe_hostel_name=mysql_query($hostel_name);
	$fetch_h_name=mysql_fetch_array($exe_hostel_name);
	//query get duration
	$du="SELECT * 
	     FROM fee_generated_sessions
		 WHERE Id=".$duration_id."";
    $exe_du=mysql_query($du);
	$fetch_du_name=mysql_fetch_array($exe_du);
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><h5 style="color:green" align="center">'.$fetch_h_name[1].'</h5></span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<table  class="table table-bordered table-striped" id="dataTable" >';
	echo ' <h5 style="color:green" align="center">Duration : '.$fetch_du_name[2].'</h5>';
	echo '<thead>
	<tr>
	<th>S. no.</th>
	<th>Admission No.</th>
	<th>Student Name</th>
	<th>Class Name</th>
	<th>Last Date</th>
	<th>Paid On Date</th>
	<th>Room No.</th>
	<th>Rent</th>
	</tr>
	</thead>
	<tbody align="center">
	';
		//if condition check level is school or class or room
	//$level==1 then show school details
	if($level==1)
		{	
			$sn=1;
			//query get hostel fee paid details student 
			$room_allocation="SELECT DISTINCT hostel_room_allocation.id, hostel_room_allocation.hid,
									  hostel_room_allocation.roomid,
									  hostel_room_allocation.room, hostel_room_allocation.bed, hostel_room_allocation.rent,
									  hostel_room_allocation.sid, hostel_room_allocation.admission_no,
									   hostel_room_allocation.session_id,
									  fee_details.paid_on,
									  fee_generated_sessions.last_date, fee_generated_sessions.duration
							 FROM hostel_room_allocation
							 
							 INNER JOIN fee_details
							 ON fee_details.student_id=hostel_room_allocation.sid
							 
							 INNER JOIN fee_generated_sessions
							 ON fee_details.fee_generated_session_id=fee_generated_sessions.Id
							 
							 WHERE fee_details.paid=1
							 
							 AND fee_details.fee_generated_session_id=".$duration_id."
							 
							 AND hostel_room_allocation.session_id=".$session_id."
							 
							 AND hostel_room_allocation.hid=".$hid."
							 
							 AND fee_generated_sessions.Id=".$duration_id."";
		}
		//if condition check level is school or class or room
	//$level==1 then show school details
	else if($level==2)
		{	
		    $cid=$_GET['class'];
			$sn=1;
			//query get hostel fee paid details student 
			$room_allocation="SELECT DISTINCT hostel_room_allocation.id, hostel_room_allocation.hid,
									  hostel_room_allocation.roomid,
									  hostel_room_allocation.room, hostel_room_allocation.bed, hostel_room_allocation.rent,
									  hostel_room_allocation.sid, hostel_room_allocation.admission_no,
									   hostel_room_allocation.session_id,
									  fee_details.paid_on,
									  fee_generated_sessions.last_date, fee_generated_sessions.duration
							 FROM hostel_room_allocation
							 
							 INNER JOIN fee_details
							 ON fee_details.student_id=hostel_room_allocation.sid
							 
							 INNER JOIN class_index
							 ON class_index.cId=hostel_room_allocation.cid
							 
							 INNER JOIN fee_generated_sessions
							 ON fee_details.fee_generated_session_id=fee_generated_sessions.Id
							 
							 WHERE fee_details.paid=1
							 
							 AND fee_details.fee_generated_session_id=".$duration_id."
							 
							 AND hostel_room_allocation.session_id=".$session_id."
							 
							 AND hostel_room_allocation.hid=".$hid."
							 
							 AND fee_generated_sessions.Id=".$duration_id."
							 
							 AND class_index.cId=".$cid."";
		}
	//if condition check level is school or class or room
	//$level==1 then show school details
	else if($level==3)
		{
			$roomid=$_GET['room'];	
			$sn=1;
			//query get hostel fee paid details student 
			$room_allocation="SELECT DISTINCT hostel_room_allocation.id, hostel_room_allocation.hid,
									  hostel_room_allocation.roomid,
									  hostel_room_allocation.room, hostel_room_allocation.bed, hostel_room_allocation.rent,
									  hostel_room_allocation.sid, hostel_room_allocation.admission_no,
									   hostel_room_allocation.session_id,
									  fee_details.paid_on,
									  fee_generated_sessions.last_date, fee_generated_sessions.duration
							 FROM hostel_room_allocation
							 
							 INNER JOIN fee_details
							 ON fee_details.student_id=hostel_room_allocation.sid
							 
							 INNER JOIN fee_generated_sessions
							 ON fee_details.fee_generated_session_id=fee_generated_sessions.Id
							 
							 WHERE fee_details.paid=1
							 
							 AND fee_details.fee_generated_session_id=".$duration_id."
							 
							 AND hostel_room_allocation.session_id=".$session_id."
							 
							 AND hostel_room_allocation.hid=".$hid."
							 
							 AND hostel_room_allocation.roomid=".$roomid."
							 
							 AND fee_generated_sessions.Id=".$duration_id."";
		}
	$exe_room=mysql_query($room_allocation);
	while($fetch_room=mysql_fetch_array($exe_room))
		{
			//query get date from dates_d
			$last_date="SELECT date FROM dates_d
			            WHERE date_id=".$fetch_room['last_date']."";
			$exe_date=mysql_query($last_date);
			$fetch_last_date=mysql_fetch_array($exe_date);
			 $date_last=$fetch_last_date[0];
			 $last_date=date('d-m-Y',strtotime($date_last));
			//query get date from dates_d
			$paid_date="SELECT date FROM dates_d
			            WHERE date_id=".$fetch_room['paid_on']."";
			$exe_date=mysql_query($paid_date);
			$fetch_paid_date=mysql_fetch_array($exe_date);
			$paid_date=date('d-m-Y',strtotime($fetch_paid_date[0]));
			//get student name from student user
		    $student_name="SELECT Name
			               FROM student_user
						   WHERE sId=".$fetch_room[6]."";
			$exe_name=mysql_query($student_name);
			$fetch_name=mysql_fetch_array($exe_name);
		    //query get hoste name from hostel detalls
			$h_name="SELECT hostel_name
			               FROM hostel_details
						   WHERE id=".$fetch_room[1]."";
			$exe_h_name=mysql_query($h_name);
			$fetch_h_name=mysql_fetch_array($exe_h_name);
			//query get class of student
			$class="SELECT class_index.class_name
			        FROM class_index
					INNER JOIN class
					ON class.classId=class_index.cId
					WHERE class.sId=".$fetch_room[6]."";
			$exe_class=mysql_query($class);
			$fetch_class=mysql_fetch_array($exe_class);
			echo '
			<tr>
			<td>'.$sn++.'</td>
			<td>'.$fetch_room[7].'</td>
			<td>'.$fetch_name[0].'</td>
			<td>'.$fetch_class[0].'</td>
			<td>'.$last_date.'</td>
			<td>'.$paid_date.'</td>
			<td>'.$fetch_room[3].'</td>
			<td>'.$fetch_room[5].'</td>
			</tr>';
		
		}
	echo'
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}
//function show session and duration
//select session and duration and view all fee paid students
function hostel_fee_not_paid_students()
{
	//Author : Anil Kumar*
    //query get session name and id from session table
	$session_name="SELECT sId, session_name
	               FROM session_table
				   ORDER BY session_name DESC";
	$exe_session=mysql_query($session_name);
		//query get hostel name and details
	$hostel_name="SELECT *
	              FROM hostel_details";
	$exe_hostel_name=mysql_query($hostel_name);

	echo'  <div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ADD NEW ROOM DETAILS</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	if(isset($_GET['select']))
		{
			echo '<p style="color:RED" align="center"><b>SELECT ALL DETAILS PROPERLY</b></p>';	
		}
	echo'  
	<div class="load_page">
	<div class="formEl_b">	
	<form id="validation_demo" action="hostel_fee_not_paid_students_view.php" method="get">
	<div class="section ">
	<label>Slect Session Name <small></small></label>   
	<div> 
	<select name="session">
	<option value="">SELECT SESSION</option>';
	while($fetch_session=mysql_fetch_array($exe_session))
		{
			echo '
			<option value="'.$fetch_session[0].'">'.$fetch_session[1].'</option>';
		}
	echo '</select>
	</div>
	</div>
	<div class="section ">
	<label>Slect Hostel Name<small></small></label> 
	<div> 
	<select name="hid">
	<option value="">SELECT HOSTEL</option>';
	while($fetch_hostel=mysql_fetch_array($exe_hostel_name))
		{
			echo '
			<option value="'.$fetch_hostel[0].'">'.$fetch_hostel[1].'</option>';
		}
	echo '</select>
	</div>
	</div>    
	<div class="section ">
	<label>Slect Duration <small></small></label>   
	<div> 
	<select name="duration">
	<option value="">SELECT DURATION</option>';
	//query get duratio9n from fee generated session
    $duration="SELECT *
	           FROM fee_generated_sessions";
    $exe_duration=mysql_query($duration);
	while($fetch_duration=mysql_fetch_array($exe_duration))
		{
			echo '
			<option value="'.$fetch_duration[0].'">'.$fetch_duration[2].'</option>';
		}
	echo '</select>
	</div>
	</div> 
	<div class="section ">
	<label>Slect Level<small></small></label> 
	<div> 
	<select name="level" id="level" onchange="show_room_level_details(this.value)">
	<option value="">SELECT LEVEL</option>
	<option value="1">All School</option>  
	<option value="2">According to Class</option> 
	<option value="3">According to Room</option> 
	</select>
	</div>
	</div>
	<div class="section" id="2" style="display:none">
	<label>SELECT CLASS</label>
	<div>
	<select name="class">';
	//get class name and id from class index table
	$get_name_class=
				   "SELECT cId,class , class_name
				    FROM class_index";
	$exe_class=mysql_query($get_name_class);
	while($fetch_class=mysql_fetch_array($exe_class))
		{
			echo '
			<option value="'.$fetch_class[0].'">'.$fetch_class[2].'</option>
			';
		} 
	echo '</select>
	</div>
	</div> ';
	echo '	<div class="section" id="3" style="display:none">
	<label>SELECT ROOM</label>
	<div>
	<select name="room">';
	//get room no  and id from class index table
	$get_room=
			   "SELECT id, room_no
				FROM hostel_room_details";
	$exe_room=mysql_query($get_room);
	while($fetch_room=mysql_fetch_array($exe_room))
		{
			echo '
			<option value="'.$fetch_room[0].'">'.$fetch_room[1].'</option>
			';
		} 
	echo '</select>
	</div>
	</div> ';
	echo '<div class="section last">
	<div>
	<button class="btn btn-inverse"><i class="icon-info-sign icon-white">
	</i>SUBMIT</button>
	</div>
	</div>
	</form>
	</div>								
	</div>
	</div>
	</div>	
	</div><!--tab1--> ';
}
//function show fee_paid students details
function hostel_fee_not_paid_students_view()
{
	//Author : Anil Kumar*
    //get duration and session
	$session_id=$_GET['session']; 
	$duration_id=$_GET['duration'];
	$hid=$_GET['hid'];
	$level=$_GET['level'];
	if(($session_id=="")||($duration_id=="")||($hid=="")||($level==""))
		{
			header("location:hostel_fee_not_paid_students.php?select");
		}
	//query get duration
	$du="SELECT * 
	     FROM fee_generated_sessions
		 WHERE Id=".$duration_id."";
    $exe_du=mysql_query($du);
	$fetch_du_name=mysql_fetch_array($exe_du);
	//get hostel name
	$hostel_name="SELECT *
	              FROM hostel_details
				  WHERE id=$hid";
	$exe_hostel_name=mysql_query($hostel_name);
	$fetch_h_name=mysql_fetch_array($exe_hostel_name);
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><h5 style="color:green" align="center">'.$fetch_h_name[1].'</h5></span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<table  class="table table-bordered table-striped" id="dataTable" >';
	echo ' <h5 style="color:green" align="center">Duration : '.$fetch_du_name[2].'</h5>';
	echo '<thead>
	<tr>
	<th>S. no.</th>
	<th>Admission No.</th>
	<th>Student Name</th>
	<th>Class Name</th>
	<th>Last Date</th>
	<th>Room No.</th>
	<th>Rent</th>
	</tr>
	</thead>
	<tbody align="center">
	';	
	//if condition check level is school or class or room
	//$level==1 then show school details
	if($level==1)
		{
			$sn=1;
			//query get hostel fee paid details student 
			$room_allocation="SELECT DISTINCT hostel_room_allocation.id, hostel_room_allocation.hid,
			                  hostel_room_allocation.roomid,
							  hostel_room_allocation.room, hostel_room_allocation.bed, hostel_room_allocation.rent,
							  hostel_room_allocation.sid, hostel_room_allocation.session_id, fee_details.paid_on, 
							  fee_generated_sessions.last_date, fee_generated_sessions.duration
							 FROM hostel_room_allocation
							 
							 INNER JOIN fee_details
							 ON fee_details.student_id=hostel_room_allocation.sid
							 
							 INNER JOIN fee_generated_sessions
							 ON fee_details.fee_generated_session_id=fee_generated_sessions.Id
							 
							 WHERE fee_details.paid=0
							 
							 AND fee_details.fee_generated_session_id=".$duration_id."
							 
							 AND hostel_room_allocation.session_id=".$session_id."
							 
							 AND hostel_room_allocation.hid=".$hid."
							 
							 AND fee_generated_sessions.Id=".$duration_id."";
		}
	//if condition check level is school or class or room
	//$level==1 then show school details
	else if($level==2)
		{
			$cid=$_GET['class'];
			$sn=1;
			//query get hostel fee paid details student 
			 $room_allocation="SELECT DISTINCT hostel_room_allocation.id, hostel_room_allocation.hid,
			                  hostel_room_allocation.roomid,
							  hostel_room_allocation.room, hostel_room_allocation.bed, hostel_room_allocation.rent,
							  hostel_room_allocation.sid, hostel_room_allocation.session_id, fee_details.paid_on, 
							  fee_generated_sessions.last_date, fee_generated_sessions.duration
							 FROM hostel_room_allocation
							 
							 INNER JOIN fee_details
							 ON fee_details.student_id=hostel_room_allocation.sid
							 
							 INNER JOIN class_index
							 ON class_index.cId=hostel_room_allocation.cid
							 
							 INNER JOIN fee_generated_sessions
							 ON fee_details.fee_generated_session_id=fee_generated_sessions.Id
							 
							 WHERE fee_details.paid=0
							 
							 AND fee_details.fee_generated_session_id=".$duration_id."
							 
							 AND hostel_room_allocation.session_id=".$session_id."
							 
							 AND hostel_room_allocation.hid=".$hid."
							 
							 AND fee_generated_sessions.Id=".$duration_id."
							 
							  AND class_index.cId=".$cid."";
		}
	//if condition check level is school or class or room
	//$level==3 then show room defaulter details
	else if($level==3)
		{
			$room_id=$_GET['room'];
			$sn=1;
			//query get hostel fee paid details student 
			$room_allocation="SELECT DISTINCT hostel_room_allocation.id, hostel_room_allocation.hid,
			                  hostel_room_allocation.roomid,
							  hostel_room_allocation.room, hostel_room_allocation.bed, hostel_room_allocation.rent,
							  hostel_room_allocation.sid, hostel_room_allocation.session_id, fee_details.paid_on, 
							  fee_generated_sessions.last_date, fee_generated_sessions.duration
							 FROM hostel_room_allocation
							 
							 INNER JOIN fee_details
							 ON fee_details.student_id=hostel_room_allocation.sid
							 
							 INNER JOIN fee_generated_sessions
							 ON fee_details.fee_generated_session_id=fee_generated_sessions.Id
							 
							 WHERE fee_details.paid=0
							 
							 AND fee_details.fee_generated_session_id=".$duration_id."
							 
							 AND hostel_room_allocation.session_id=".$session_id."
							 
							 AND hostel_room_allocation.hid=".$hid."
							 
							 AND hostel_room_allocation.roomid=".$room_id."
							 
							 AND fee_generated_sessions.Id=".$duration_id."";
		}
					 
	$exe_room=mysql_query($room_allocation);
	while($fetch_room=mysql_fetch_array($exe_room))
		{
			//query get date from dates_d
			$last_date="SELECT date FROM dates_d
			            WHERE date_id=".$fetch_room['last_date']."";
			$exe_date=mysql_query($last_date);
			$fetch_last_date=mysql_fetch_array($exe_date);
			 $date_last=$fetch_last_date[0];
			 $last_date=date('d-m-Y',strtotime($date_last));
		/*	//query get date from dates_d
			$paid_date="SELECT date FROM dates_d
			            WHERE date_id=".$fetch_room['paid_on']."";
			$exe_date=mysql_query($paid_date);
			$fetch_paid_date=mysql_fetch_array($exe_date);
			$paid_date=date('d-m-Y',strtotime($fetch_paid_date[0]));*/
			//get student name from student user
		    $student_name="SELECT Name
			               FROM student_user
						   WHERE sId=".$fetch_room[6]."";
			$exe_name=mysql_query($student_name);
			$fetch_name=mysql_fetch_array($exe_name);
		    //query get hoste name from hostel detalls
			$h_name="SELECT hostel_name
			               FROM hostel_details
						   WHERE id=".$fetch_room[1]."";
			$exe_h_name=mysql_query($h_name);
			$fetch_h_name=mysql_fetch_array($exe_h_name);
			//query get class of student
			$class="SELECT class_index.class_name
			        FROM class_index
					INNER JOIN class
					ON class.classId=class_index.cId
					WHERE class.sId=".$fetch_room[6]."";
			$exe_class=mysql_query($class);
			$fetch_class=mysql_fetch_array($exe_class);
			echo '
			<tr>
			<td>'.$sn++.'</td>
			<td>'.$fetch_room[7].'</td>
			<td>'.$fetch_name[0].'</td>
			<td>'.$fetch_class[0].'</td>
			<td>'.$last_date.'</td>
			
			<td>'.$fetch_room['room'].'</td>
			<td>'.$fetch_room[5].'</td>
			</tr>';
		
		}
	echo'
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}
//function show all classes
function hostel_select_class_defaulter()
{	//Author : Anil Kumar*
	//get hostel id
	$hid=$_GET['hid'];
	echo'  <div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ADD NEW HOSTEL</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	//query to get all class
	$query_class="SELECT *
				 FROM class_index
				  ";
	$execute_class=mysql_query($query_class);
	echo '
	<div class="section last">
	<label>SELECT CLASS</label>
	<div > <ol class="rounded-list">';				  
	while($fetch_class=mysql_fetch_array($execute_class))
		{ 
			echo' <li><a href="hostel_show_student_details.php?hid='.$hid.'&cid='.$fetch_class['cId'].'
			">'.$fetch_class['class_name'].'</a></li> ';	
		}
	echo'
	</ol>
	</div>
	</div>
	</div>
	</div>	
	</div><!--tab1--> '; 	
}

//function show fee_paid students details
function hostel_show_defaulter_class()
{
	//Author : Anil Kumar*
    //get duration and session
	$session_id=$_GET['session']; 
	$duration_id=$_GET['duration'];
	$hid=$_GET['hid'];
	//get hostel name
	$hostel_name="SELECT *
	              FROM hostel_details
				  WHERE id=$hid";
	$exe_hostel_name=mysql_query($hostel_name);
	$fetch_h_name=mysql_fetch_array($exe_hostel_name);
	echo'
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>'.$fetch_h_name[1].'</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th>S. no.</th>
	<th>Admission No.</th>
	<th>Student Name</th>
	<th>Class Name</th>
	<th>Last Date</th>
	<th>Room No.</th>
	<th>Rent</th>
	</tr>
	</thead>
	<tbody align="center">
	';	
	$sn=1;
	//query get hostel fee paid details student 
	$room_allocation="SELECT DISTINCT hostel_room_allocation.id, hostel_room_allocation.hid, hostel_room_allocation.roomid,
	                  hostel_room_allocation.room, hostel_room_allocation.bed, hostel_room_allocation.rent,
					  hostel_room_allocation.sid, hostel_room_allocation.session_id, fee_details.paid_on, 
					  fee_generated_sessions.last_date, fee_generated_sessions.duration
					 FROM hostel_room_allocation
					 
					 INNER JOIN fee_details
					 ON fee_details.student_id=hostel_room_allocation.sid
					 
					 INNER JOIN fee_generated_sessions
					 ON fee_details.fee_generated_session_id=fee_generated_sessions.Id
					 
					 WHERE fee_details.paid=0
					 
					 AND fee_details.fee_generated_session_id=".$duration_id."
					 
					 AND hostel_room_allocation.session_id=".$session_id."
					 
					 AND hostel_room_allocation.hid=".$hid."
					 
					 AND fee_generated_sessions.Id=".$duration_id."";
					 
	$exe_room=mysql_query($room_allocation);
	while($fetch_room=mysql_fetch_array($exe_room))
		{
			//query get date from dates_d
			$last_date="SELECT date FROM dates_d
			            WHERE date_id=".$fetch_room['last_date']."";
			$exe_date=mysql_query($last_date);
			$fetch_last_date=mysql_fetch_array($exe_date);
			 $date_last=$fetch_last_date[0];
			 $last_date=date('d-m-Y',strtotime($date_last));
		/*	//query get date from dates_d
			$paid_date="SELECT date FROM dates_d
			            WHERE date_id=".$fetch_room['paid_on']."";
			$exe_date=mysql_query($paid_date);
			$fetch_paid_date=mysql_fetch_array($exe_date);
			$paid_date=date('d-m-Y',strtotime($fetch_paid_date[0]));*/
			//get student name from student user
		    $student_name="SELECT Name
			               FROM student_user
						   WHERE sId=".$fetch_room[6]."";
			$exe_name=mysql_query($student_name);
			$fetch_name=mysql_fetch_array($exe_name);
		    //query get hoste name from hostel detalls
			$h_name="SELECT hostel_name
			               FROM hostel_details
						   WHERE id=".$fetch_room[1]."";
			$exe_h_name=mysql_query($h_name);
			$fetch_h_name=mysql_fetch_array($exe_h_name);
			//query get class of student
			$class="SELECT class_index.class_name
			        FROM class_index
					INNER JOIN class
					ON class.classId=class_index.cId
					WHERE class.sId=".$fetch_room[6]."";
			$exe_class=mysql_query($class);
			$fetch_class=mysql_fetch_array($exe_class);
			echo '
			<tr>
			<td>'.$sn++.'</td>
			<td>'.$fetch_room[7].'</td>
			<td>'.$fetch_name[0].'</td>
			<td>'.$fetch_class[0].'</td>
			<td>'.$last_date.'</td>
			
			<td>'.$fetch_room[4].'</td>
			<td>'.$fetch_room[5].'</td>
			</tr>';
		
		}
	echo'
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
}

//function add and show hostel fee generation details
function hostel_add_fee_generation_details()
{	
	//Author : Anil Kumar*
	echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span> Fee Generation Details </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly add   <span class="netip"><a  class="red" > fee generation 
	details  </a></span>
	</div>
	';		  
	if (isset($_GET['added']))
		{
			echo '<h4 align="center"><span style="color:green">FEE GENERATION DETAILS ADDED SUCCESSFULLY!</h4></span>';
		}
	else if	(isset($_GET['error']))
		{
			echo "<h4 align=\"center\">ERROR!</h4>";
		}	
	echo'       
	<form id="demo"  action="hostel/insert_hostel_fee_generation_details.php" method="post"> 
	<div class="section">
	<label>Type</label>   
	<div> 
	<select  name="type" class=" medium" />
	<option value="monthly">Monthly</option>
	<option value="quarterly">Quarterly</option>
	<option value="half-yearly">Half-yearly</option>
	<option value="yearly">Yearly</option>
	</select>
	</div>
	</div>
	<div class="section">
	<label>Duration<small>Monthwise</small></label>   
	<div> 
	<input type="text" name="duration" class=" small" />
	</div>
	</div>';
	/*		<div class="section">
	<label>Last DATE<small>Fee Submission</small></label>   
	<div><input type="text" id="datepick" class="datepicker hasDatepicker"  name="last_date">
	</div>
	</div>*/
	echo '		 
	<div class="section">
	<label>Last DATE<small>Fee collection date</small></label>    
	<div><input type="text"  id="datepick" class="datepicker"  name="last_date"  /></div>
	</div>
	<div class="section">
	<label>Fine<small>daywise</small></label>   
	<div> 
	<input type="text" name="fine" class=" small" />
	</div>
	</div>';
	echo '		
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form">
	<a class="btn" onclick="ResetForm()" title="Reset  Form">Clear Form</a>
	</div>
	</div>	 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
	$sno=0;
	echo' 
	<div class="row-fluid">
	<!-- Table widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>View Fee Discount Credentials </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	if (isset($_GET['deleted']))
		{
			echo '<h4 align="center"><span style="color:green">FEE GENERATION DETAIL DELETED SUCCESSFULLY!</h4></span>';
		}	
	else if (isset($_GET['editted']))
		{
			echo '<h4 align="center"><span style="color:green">FEE GENERATION DETAIL EDITED SUCCESSFULLY!</h4></span>';
		}	
	else if (isset($_GET['error']))
		{
			echo '<h4 align="center"><span style="color:red">ERROR!</h4></span>';
		}						
	echo'         
	<table class="table table-bordered table-striped" id="dataTable">
	<thead>';
	echo'   
	<tr>
	<th width="5%">S.No.</th>
	<th width="22%">TYPE</th>
	<th width="22%">DURATION</th>
	<th width="22%">LAST DATE<br/><small>(Fee submission)</small></th>
	<th width="22%">FINE/perday in Rs.</th>
	<th width="22%">SESSION</th>
	<th width="7%">ACTION</th>
	</tr>
	</thead>
	<tbody align="center">';
	$query = "SELECT * , session_name
			FROM hostel_fee_generated_sessions
			INNER JOIN session_table
			ON hostel_fee_generated_sessions.session_id = session_table.sId
			ORDER BY last_date ASC
			";
	$execute = mysql_query($query) ;
	while($fetch = mysql_fetch_array($execute))
		{            
			++$sno;           
			echo'     <tr class="odd gradeX">
			<td>'.$sno.'</td>
			<td>'.$fetch['type'].'</td>
			<td>'.$fetch['duration'].'</td>';
			$sql_date="SELECT date
			FROM dates_d
			WHERE date_id = ".$fetch['last_date']."
			";
			$execute_date = mysql_query($sql_date);
			$fetch_date = mysql_fetch_array($execute_date);		  
			echo'		
			<td>'.$fetch_date['date'].'</td>
			<td>'.$fetch['fine'].'</td>
			<td>'.$fetch['session_name'].'</td>
			<td class=" ">
			<span class="tip"><a href="hostel_edit_fee_generation_details.php?id='.$fetch['Id'].'" original-title="Edit">
			<img src="images/icon/icon_edit.png"></a></span> 
			<span class="tip"><a href="hostel/delete_fee_generation_details.php?id='.$fetch['Id'].'" original-title="Delete">
			<img src="images/icon/icon_delete.png"></a></span> 
			</td>
			</tr>';
		}
	echo' </tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';	
}

//function edit hostel fee generation details and update
function hostel_edit_fee_generation_details()
{
	//Author : Anil Kumar*	
	echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span> Fee Generation Details </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly add   <span class="netip"><a  class="red" > fee generation 
	details  </a></span>
	</div>
	';
	//query get details from hostel fee generated sessions where id=get id		  
	$sql="SELECT *
		FROM hostel_fee_generated_sessions
		WHERE id = ".$_GET['id']."
		";
	$execute=mysql_query($sql);
	$fetch=mysql_fetch_array($execute);	 	
	$date_id = $fetch['last_date'];
	$sql_date="SELECT date
				FROM dates_d
				WHERE date_id = ".$fetch['last_date']."
				";
	$execute_date=mysql_query($sql_date);
	$fetch_date=mysql_fetch_array($execute_date);	
	echo'        
	<form id="demo"  action="hostel/edit_fee_generation_details.php?id='.$_GET['id'].'" method="post"> 
	<div class="section">
	<label>Type</label>   
	<div> ';
	/*<input type="text" name="type" value="'.$fetch['type'].'" class=" medium" />*/
	echo '<select  name="type" class="chzn-select medium" data-placeholder="'.$fetch['type'].'">
	<option value=""></option>
	<option value="monthly">monthly</option>
	<option value="quarterly">quarterly</option>
	<option value="half-yearly">half-yearly</option>
	<option value="yearly">yearly</option>
	</select>
	</div>
	</div>
	<div class="section">
	<label>Duration<small>Monthwise</small></label>   
	<div> 
	<input type="text" name="duration" value="'.$fetch['duration'].'" class=" medium" />
	</div>
	</div>
	<div class="section">
	<label>Last Date<small>Fee Submission</small></label>   
	<div><input type="text" id="datepick" value="'.$fetch_date['date'].'" class="datepicker"  name="last_date" />
	</div>
	</div>
	<div class="section">
	<label>Duration<small>Monthwise</small></label>   
	<div> 
	<input type="text" name="fine" value="'.$fetch['fine'].'" class=" medium" />
	</div>
	</div>
	<div class="section last">
	<div>
	<input type="submit" value="submit" class="btn submit_form">
	<a class="btn" onclick="ResetForm()" title="Reset  Form">Clear Form</a>
	</div>
	</div>	 
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';
}
//function to show hostel room availablity
function show_left_room_hostel()
{
	//Author : Anil Kumar*
	$room_id=$_GET['room_id'];
	$hid=$_GET['hostel_id'];
	echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span> Fee Generation Details </span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';	
	//query to check beds which are occupied in that room
	if (isset($_GET['allot']))
	  {
		  echo '<h4 align="center"><span style="color:green">ROOM ALLOTTED SUCCESSFULLY!</h4></span>';
	  }	
	$get_total_beds=
				"SELECT `bed/student`,room_no
				FROM  hostel_room_details
				WHERE id = ".$room_id."";
	$exe_total_beds=mysql_query($get_total_beds);
	$fetch_total_beds=mysql_fetch_array($exe_total_beds);
	$total_beds=$fetch_total_beds[0];	
	$student_name=array();
	$student_ad=array();
	//query get availabilite details of room allocation
	$hostel="SELECT id,student_user.Name , student_user.admission_no, class_index.class_name
				 FROM hostel_room_allocation
				 INNER JOIN student_user
				 ON student_user.sId = hostel_room_allocation.sid
				 INNER JOIN class_index
				 ON hostel_room_allocation.cid = class_index.cId
				 WHERE roomid=".$room_id."
				";
	$exe_hostel=mysql_query($hostel);
	$f=0;
	while($fetch_hostel=mysql_fetch_array($exe_hostel))
	{
	$student_name[$f]=$fetch_hostel[1];	
	$student_ad[$f]=$fetch_hostel[2];
	$student_class[$f]=$fetch_hostel[3];
	$f++;	
	}
	
	$left=$total_beds-$f;
	
	echo '<h5>Total Beds in Room('.$fetch_total_beds[1].')==>'.$total_beds.'</h5><br>
	
	<h5 align="left">Total Left Beds in Room('.$fetch_total_beds[1].')==>'.$left.'</h5>
	<h5 align="right"><a class="btn btn-inverse" href="hostel_view_room_details.php?hid='.$hid.'">Go Back</a></h5>
	<hr>';
	
	for($i=0;$i<$f;$i++)
	{
	//get the students name in that room id
	echo '<img src="images/bed_allocated.png" height="100" width="150"/>
	<h5 align="center">'.$student_name[$i].'-----'.$student_ad[$i].'</h5>
	<h5 align="center">Class Name:'.$student_class[$i].'</h5>
	<h5 align="right"><a href="hostel/delete_allocation.php?rid='.$room_id.'&hid='.$hid.'&sid='.$student_ad[$i].'" 
	class="btn btn-inverse">Delete Allocation </a></h5><br><hr>';
	/*<a href="hostel_edit_bed_allocation.php"><button class="btn btn-success">Edit </a></button><br><hr>';*/
	/*<button class="btn btn-inverse" href="#">Inverse</button>*/
	}
	for($j=0;$j<$left;$j++)
	{
	
	echo '<img src="images/bed_left.png" height="100" width="150"/>
	<h5 align="center">No data available</h5>
	<h5 align="right"><a href="hostel_edit_bed_allocation.php?rid='.$room_id.'&hid='.$hid.'" class="btn btn-inverse"> 
	New   Allocation </a></h5><br><hr>';	
	echo ' 
									 
	
	
	<div class="section" style="display:none" id="allocate_bed_'.$j.'" style="background-color:green" style="padding:30px">
	<label>SELECT CLASS</label>
	<div>
	<select name="class" id="class" onchange="show_class_students(this.value)">';
	//get class name and id from class index table
	$get_name_class=
				 "SELECT cId,class , class_name
				  FROM class_index";
	$exe_class=mysql_query($get_name_class);
	while($fetch_class=mysql_fetch_array($exe_class))
	  {
		  echo '
		  <option value="'.$fetch_class[0].'">'.$fetch_class[2].'</option>
		  ';
	  } 
	echo '</select>
	</div>
	</div> 											
	<br><hr>';
	}
	  
	
	echo 
	'</div>
	</div>
	</div>';

}
//function show session and duration
//select session and duration and view all allocated students
function hostel_edit_bed_allocation()
{
     $rid=$_GET['rid'];
	 $hid=$_GET['hid'];
    //query get session name and id from session table
	$session_name="SELECT sId, session_name
	               FROM session_table
				   ORDER BY session_name DESC";
	$exe_session=mysql_query($session_name);
	echo'  <div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ADD NEW ROOM DETAILS</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	if(isset($_GET['select']))
		{
			echo '<p style="color:RED" align="center"><b>SELECT All DETAILS PROPERLY</b></p>';	
		}
	echo'  
	<div class="load_page">
	<div class="formEl_b">	
	<form id="validation_demo" action="hostel_select_students_new_allocation.php" method="get">';
	echo '
	<div class="section">
	<label>SELECT CLASS</label>
	<div>
	<select name="class" id="class" >';
	//get class name and id from class index table
	$get_name_class=
				   "SELECT cId,class , class_name
				    FROM class_index";
	$exe_class=mysql_query($get_name_class);
	while($fetch_class=mysql_fetch_array($exe_class))
		{
			echo '
			<option value="'.$fetch_class[0].'">'.$fetch_class[2].'</option>
			';
		} 
	echo '</select>
	</div>
	</div> ';
    echo ' <input type="hidden" name="rid" value="'.$rid.'">
        <input type="hidden" name="hid" value="'.$hid.'">';
	echo '<div class="section last">
	<div>
	<button class="btn btn-small btn-info"><i class="icon-info-sign icon-white">
	</i>SUBMIT</button>
	</div>
	</div>
	</form>
	</div>								
	</div>
	</div>
	</div>	
	</div><!--tab1--> ';
}
//select session and duration and view all allocated students
function hostel_select_students_new_allocation()
{
	//Author : Anil Kumar*
      $rid=$_GET['rid'];
	 $hid=$_GET['hid'];
	 $cid=$_GET['class'];
    //query get session name and id from session table
	$session_name="SELECT student_user.admission_no, student_user.Name
	                FROM student_user
					
					INNER JOIN class
					ON class.sid=student_user.sId
					
					WHERE class.classId=".$cid."";
	$exe_session=mysql_query($session_name);
	echo'  <div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>ADD NEW ROOM DETAILS</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">';
	if(isset($_GET['select']))
		{
			echo '<p style="color:RED" align="center"><b>SELECT All DETAILS PROPERLY</b></p>';	
		}
	echo'  
	<div class="load_page">
	<div class="formEl_b">	
	<form id="validation_demo" action="hostel/new_room_allocation_bed.php?rid='.$rid.'&hid='.$hid.'&cid='.$cid.'" 
	method="get">';
	echo '
	<div class="section">
	<label>SELECT CLASS</label>
	<div>
	<select name="add" id="class" >';
	//get class name and id from class index table

	while($fetch_student=mysql_fetch_array($exe_session))
		{
			echo '
			<option value="'.$fetch_student[0].'">'.$fetch_student[1].'</option>
			';
		} 
	echo '</select>
	</div>
	</div> ';
    echo ' <input type="hidden" name="rid" value="'.$rid.'">
	      <input type="hidden" name="cid" value="'.$cid.'">
          <input type="hidden" name="hid" value="'.$hid.'">';
	echo '<div class="section last">
	<div>
	<button class="btn btn-small btn-info"><i class="icon-info-sign icon-white">
	</i>SUBMIT</button>
	</div>
	</div>
	</form>
	</div>								
	</div>
	</div>
	</div>	
	</div><!--tab1--> ';
}

?>