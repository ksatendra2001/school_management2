<?php


function admin_create_cluster()
{
	
echo'
              <div class="row-fluid">
                    
                    		<!-- Widget -->
                            <div class="widget  span12 clearfix">
                                <div class="widget-header">
                                    <span> CLUSTERS</span>
                                </div><!-- End widget-header -->	
                                <div class="widget-content">
                                
                                <div id="UITab" class="clearfix" style="position:relative;">
                                    <ul class="tabs">
                                        <li style="display: list-item;" class="active"><a href="#tab1"> Create Cluster </a></li>';  /*cluster creation page tab*/ echo'
                                        <li style="display: list-item;" class=""><a href="#tab2"> View Clusters </a></li> '; /*cluster view page tab*/ echo'           
                                    </ul>
        
                                <div class="tab_container">
        
                                  <div id="tab1" class="tab_content" style="display: block;"> 
                                      <div class="load_page">
                                        <div class="formEl_b">
                                       <div class="formEl_b">';
				if (isset($_GET['cluster_created']))
				{
					echo "<h3 align=\"center\">CLUSTER ADDED SUCCESSFULLY!</h3>";
				}
				if(isset($_GET['error_creating_cluster']))
				{
					echo "<h3 align=\"center\">ERROR ADDING CLUSTER!!</h3>";
				}   	
                                  echo'
								        <form id="" method="post" action="processing/creating_cluster.php"> 
                                        <fieldset>
                                        <legend>Create Cluster</legend>
                                              <div class="section ">
                                              <label> Cluster Name</label>   
                                              <div> 
                                              <input type="text" class="" name="cluster_name" id="cluster_name">
                                              </div>
                                             </div>
											 
                                              <div class="section ">
                                              <label> Cluster Description</label>   
                                              <div> 
                                              <textarea rows="5" column="80" class="" name="cluster_description" id="cluster_description"></textarea>
                                              </div>
                                             </div>
											  
                                             
                                    <div class="section">
                                      <label>Cluster Type</label>   
                                      <div>
                                          <select  name="cluster_type" id="cluster_type" class="" style="display: none; padding: 6px;">
                                          <option value="">SELECT</option> 
                                          <option value="0">Frequently Used Items</option> 
                                          <option value="1">Static Items</option> 
                                          </select>
                                           
                                </div>
                                </div>
                                            
                                              <div class="section last">
                                              <div>
                                                <input type="submit" value="Create Cluster" class="btn submit_form">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a class="btn" onclick="ResetForm()" title="Reset  Form">Cancel</a>
                                             </div>
                                             </div>
                                        </fieldset>
                                        </form>
        
                                        </div>
                                        </div>
        
                                      </div>	
                                  </div><!--tab1-->
                                  
                                  
                                  <div id="tab2" class="tab_content" style="display: none;"> 
                                      <div class="load_page">';
                                      
                                                  
     

  
                                                                 //fetches cluster in inventory
$query_cluster="  SELECT  cluster_id, cluster_name, cluster_type, cluster_description
                    FROM item_cluster
					";  
$execute_cluster=mysql_query($query_cluster);
			                                                  
$sno=1;
	
	
	echo'
	                      <!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                               
                                
                                <div class="widget-content">
                                   
									<div id="container" style="min-width: 400px; height:100%; margin: 0 auto">
	
	
	<div class="demo">';
           
                    	if (isset($_GET['deleted_cluster']))
				{
					echo "<h3 align=\"center\">CLUSTER DELETED SUCCESSFULLY!</h3>";
				}
				if(isset($_GET['error_deleting_cluster']))
				{
					echo "<h3 align=\"center\">ERROR DELETING CLUSTER!!</h3>";
				}   	
                    		
                       echo'     
                             <div class="row-fluid">
                    
                                <div class="widget-content">
                                 
                                                   
                                                  <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                    <thead align="center">
                                                      <tr>
                                                <th>Cluster</th>
                                                <th>Description</th>
                                                <th>Type</th>
												<th>Actions</th>
                                            </tr>
                                        </thead>
										  <tbody align="center">';
										
	while($fetch_cluster=mysql_fetch_array($execute_cluster))
  {	$d=$sno;
  
	echo'										
	                                           <tr class="odd gradeX" id='.$d.'> 
				
 
 				
                                                <td><a href="admin_show_items_cluster.php?cluster_id='.$fetch_cluster[0].'">'.$fetch_cluster['cluster_name'].'</a></td>
                                                <td>'.$fetch_cluster['cluster_description'].'</td>
                                             '; 
											  if($fetch_cluster['cluster_type'] == 1)
											  {
											   echo'<td>Static Items</td>';
											  }
											  
											  else if($fetch_cluster['cluster_type'] == 0)
											  {
											   echo'<td>Frequently Used Items</td>';
											  }
                                           echo'
                                        
											<td >
                                                        
                                                     <span class="tip" onclick="edit_cluster(\''.$fetch_cluster[0].'\',\''.$fetch_cluster[1].'\',\''.$fetch_cluster[2].'\',\''.$fetch_cluster[3].'\',\''.$sno++.'\')" ><img src="../images/icon/icon_edit.png" ></span> 
       <span class="tip"><a href="processing/deleting_cluster.php?id='.$fetch_cluster['cluster_id'].'" class="Delete" name="delete name" title="Delete"><img src="../images/icon/icon_delete.png"></a></span> 
                                                         </td>'; }
                                      echo'      </tr>   
                                        </tbody>
                                    </table>
                                        
                                    </div>  </div>  </div>    	';
}
//to show item  name of that cluster
function admin_show_items_cluster()
{
	$get_cluster_id=$_GET['cluster_id'];
	//query to get items which are not in dept
	   $get_stock_items=
	   "SELECT *
	   FROM items 
	   WHERE cluster_id=".$get_cluster_id."
	  ";
	   $exe_get_stock_items=mysql_query($get_stock_items);
  
	 echo '<div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i> Items in stock</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                    <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead>
										<tr>
                                        <th>Item name</th>
                                                <th>Date of purchase</th>
                                                <th>Vendor name</th>
												<th>Receipt</th>
												<th>Warranty voids on</th>
												<th>Warranty</th>
												 <th>Item cluster name</th>
												 
												 <th>Total Items</th>
												 <th>Quantity(In stock)</th>
												 <th>Cost per unit</th>
												 
												 </tr>
												 <tbody class="align-center">';
												 while($fetch_stock_items=mysql_fetch_array($exe_get_stock_items))
												 {
													
											    //get vendor name for that id
												$get_vendor_name=
												"SELECT vendor_name
												FROM vendor_details 
												WHERE vendor_id=".$fetch_stock_items['vendor_id']."";
												$exe_vendor_name=mysql_query($get_vendor_name);
												$fetch_vendor_name=mysql_fetch_array($exe_vendor_name);
												$name_vendor=$fetch_vendor_name[0];
												//get receipt for view from receipt_dertails
												$get_receipt=
												"SELECT file_name
												FROM receipt_details
												WHERE receipt_id=".$fetch_stock_items['receipt_id']."";
												$get_file=mysql_query($get_receipt);
												$fetch_file=mysql_fetch_array($get_file);
												$name_file=$fetch_file[0];
												//get cluster name acc to id
												$get_cluster_name=
												"SELECT cluster_name
												FROM item_cluster
												WHERE cluster_id=".$fetch_stock_items['cluster_id']."";
												$exe_cluster_name=mysql_query($get_cluster_name);
												$fetch_cluster_name=mysql_fetch_array($exe_cluster_name);
												$cluster_name=$fetch_cluster_name[0];
												//to get quantity left from the overall 
													 
													 echo'
												 <tr>
												 <td><a href="admin_show_code_for_item.php?item_id='.$fetch_stock_items[0].'">'.$fetch_stock_items['item_name'].'</a></td>
												 <td>'.$fetch_stock_items['date_of_purchase'].'</td>
												 <td>'.$name_vendor.'</td>
												 <td><a href="files/'.$name_file.'">view</a></td>
												 <td>'.$fetch_stock_items['warranty_voids_on'].'</td>
												 <td>';
												   date_default_timezone_set('Asia/Kolkata');
									  
									  
									              $today_time = date("Y-m-d");
									  
												 
												 if($fetch_stock_items['warranty_voids_on']<$today_time)
												 
												 {
													 echo '<i style="color:red">Warranty over</i>';
													 
													 
												 }
												 else
												 
												 {
													 echo '<i style="color:green">In Warranty</i>';
													 
													 
												 }
												 
												 echo '</td>
												 <td>'.$cluster_name.'</td>
												 <td>'.$fetch_stock_items['quantity'].'</td>
												 <td>
												 ';
												 if($fetch_stock_items['quantity_left']==0)
												 {
													echo '<i><b>NULL</b></i>'; 
													 
												 }
												 
												
												 else
												 {
													echo $fetch_stock_items['quantity_left']; 
													 
												 }
												
												 echo '</td>
												 <td>'.$fetch_stock_items['cost'].'</td>
												 
												 
									 	
												 
												 
												 
												 
												 
												 </tr>';
												 }
												 echo '
                                        </tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div><!-- row-fluid -->';

	
	
	
}


?>