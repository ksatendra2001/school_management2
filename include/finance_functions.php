<?php

//function to called by accounts_finance_manage_category.php
function accounts_category_details()//enter the category details
{        
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Category Details</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';
	
	if (isset($_GET['Successfully_Insert']))
	  {
	   echo "<h4 style=\"color:green\"align=\"center\">Successfully Added!</h4>";
	  }
    if(isset($_GET['error_adding']))
	 {
	   echo "<h5 style=\"color:red\"align=\"center\">Category Already Exist !!</h5>";
	  }
	if (isset($_GET['type_select']))
	 {
	  echo "<h5 style=\"color:red\"align=\"center\">Please type select !</h5>";
	 }		
	  echo'
	<form id="validation_demo" action="finance/account_finance_insert_category.php" method="post">        			 
	<div class="section ">
	<label>Category Name<small></small></label>   
	<div> 
	<input type="text" class="validate[required] small" name="category_name" />
	</div>
	</div>	
	<div class="section ">
	<label>Type <small></small></label>   
	<div> 
	<div>
	<input type="radio" name="type" id="radio-1" value="1"  class="ck"/>
	<label for="radio-1">Income</label>
	</div>
	<div>
	<input type="radio" name="type" id="radio-2" value="0"  class="ck"  />
	<label for="radio-2" >Expense</label>
	</div>
	</div>
	</div>
	<div class="section last">
	<div><button class="uibutton submit" rel="1" type="submit"  >Add</button>
	<button class="uibutton special"type="reset"  >Reset</button>   
	</div>
	</div>									
	</form>  
	';			 	 																
	echo'	    
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 	
}

//function to called by accounts_session_category_details.php
function  session_category_details()//select session details
{   
    //Author By:Manish Mishra
	echo  '<div class="row-fluid">                           
	<div class="span12  widget clearfix">
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Session</span>
	</div><!-- End widget-header -->		  
	<div class="widget-content"><br />
	<div class="row-fluid"> 			
	';	
	
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	

	echo'
	<form action="accounts_finance_view_category.php" method="get" name="session" >							
	<label><h4 align="center">select Session<h4><small></label>   
	<div align="center">											
	<select  data-placeholder="Choose  Session..." class="chzn-select" tabindex="2" name="session">        
	<option value=""></option>'; 								
	//query to get month from `fee_generated_sessions`  table
	 $get_session="
		SELECT `sId`,`session_name`
		FROM `session_table` ";
	$get_session_exe=mysql_query($get_session);
	while($get_session_fetch=mysql_fetch_array($get_session_exe))
	     {
			$sid=$get_session_fetch['sId'];	
			$session_name=$get_session_fetch['session_name'];
			echo'
	        <option value="'.$sid.'">'. $session_name.'</option>';
	      } 							 
	echo'
	</select>       
	</div>									                           	       				
	<br></br>			   																
	<div class="section last" align="center">
	<button  class="uibutton submit_form" >Go</button>
	</div>
	</form>
	';
	//query to get session name and session_id	
	echo'                                       
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';		
	
}
//function to called By accounts_finance_category.php
function accounts_view_category_details()//view category details
{   
     //Author By:Manish Mishra
	echo '<div class="row-fluid">                          
	<div class="span12  widget clearfix">
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Category Details</span>
	</div><!-- End widget-header -->		  
	<div class="widget-content"><br />
	<div class="row-fluid"> 	  	
	';	
	$session=$_GET['session'];
	
	if(isset($_GET['update_successfully']))
	   {
	    echo "<h4 style=\"color:green\"align=\"center\">Successfully Update!</h4>";
	   }
	if(isset($_GET['deleted_category']))
	   {
	    echo "<h4 style=\"color:green\"align=\"center\">Successfully Deleted!</h4>";
	   }
	//query to get category ,id from `transactions_credentials` 
	 $get_category= 
		"SELECT `category`,`id`
		FROM `transactions_credentials` 
		WHERE `session_id`=".$session." ";
	$exe_category= mysql_query($get_category);	 
	$sno=0;
	echo'
	<table  class="table table-bordered table-striped" id="dataTable" >                      					
	<thead>
	<tr>
	<th>S.No</th>
	<th>Category Name</th>
	<th>Actions</th>
	</tr>
	</thead>
	<tbody align="center">';
	;
	while($fetch_category=mysql_fetch_array($exe_category))
	    {      
		$cid=$fetch_category['id'];
		++$sno;    
		echo'	
		<tr>
		<td>'.$sno.'</td>
		<td>'.$fetch_category['category'].'<br></td>
		<td>			
		<span class="tip"><a href="accounts_finance_edit_category.php?cid='.$cid.'&session='.$session.'" 
		class="table-icon edit" title="Edit">
		<img src="images/icon/icon_edit.png"/></a></span>
		<span class="tip" onclick="delete_category('.$cid.','.$session.');"><a class="table-icon delete" title="Delete">
		<img src="images/icon/icon_delete.png"/></a></span>
		</td>
		</tr>
		';
	   }				
	echo'  
	</tbody>
	</table>                           
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';				
}
//function to called By accounts_fimance_edit_category.php
function accounts_edit_category_details()//edit the category detailed
{
	//Author By: Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i> Edit Category Details</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';
	if(isset($_GET['Successfully_update']))
	  {
	    echo "<h4 style=\"color:green\"align=\"center\">Successfully Updated!</h4>";
	  }
	$session=$_GET['session'];
	
	//query to get category details
	$get_category= 
		"SELECT `category`,`type`
		FROM `transactions_credentials` 
		WHERE `id`=".$_GET['cid']." AND `session_id`=".$session."
		";
	$exe_category= mysql_query($get_category);
	$fetch_category=mysql_fetch_array($exe_category);
	$category=$fetch_category['category'];
	$type=$fetch_category['type'];
	
	echo'
	<form id="validation_demo" action="finance/account_edit_category.php" method="post"> 
	<input type="hidden" value="'.$_GET['cid'].'" name="cid"/> 
	<input type="hidden" value="'.$session.'" name="session"/>            			 
	<div class="section ">
	<label>Category Name<small></small></label>   
	<div> 
	<input type="text" class="validate[required] small"  value='.$category.' name="category_name" />
	</div>
	</div>	';
	 if($type==1)
	  {
		echo' <div class="section ">
		<label>Type <small></small></label>   
		<div> 
		<div>
		<input type="radio" name="type" id="radio-1" value="1"  class="ck" checked="checked"/>
		<label for="radio-1">Income</label>
		</div>
		<div>
		<input type="radio" name="type" id="radio-2" value="0"  class="ck"  />
		<label for="radio-2" >Expense</label>
		</div>
		</div>
		</div>
		';
      }
	if($type==0)
	  {
		echo' <div class="section ">
		<label>Type <small></small></label>   
		<div> 
		<div>
		<input type="radio" name="type" id="radio-1" value="1"  class="ck"/>
		<label for="radio-1">Income</label>
		</div>
		<div>
		<input type="radio" name="type" id="radio-2" value="0"  class="ck" checked="checked" />
		<label for="radio-2" >Expense</label>
		</div>
		</div>
		</div>
		';
	 }
	echo'
	<div class="section last">
	<div><button class="uibutton submit" rel="1" type="submit"  >Add</button>
	<button class="uibutton special"type="reset"  >Reset</button>   
	</div>
	</div>									
	</form>  
	';		 	 																
	echo'	    
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 	
	
}

//function to called By accounts_finance_transation_details.php
function  accounts_transation_details()//select the income &expence details
{      
	//Author By: Manish
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Add Expense & Income Details</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';
	  
	echo'
	<div class="span4">
	<a href="accounts_finance_create_expense.php"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	<em><b style="color:green">Create Expense</b></em> </div>
	<div class="breaks"><span></span></div>
	
	<a href="accounts_finance_create_income.php"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>
	<em><b style="color:green">Create Income</b></em> </div>
	<div class="breaks"><span></span></div>
	</div><!-- span4 column-->								
	';		 	 																
	echo'	    
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 		
}
//function to called By accounts_finance_create expense .php
function create_expense()//enter the expense details
{   
    //Author By: Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Create Expense</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	if (isset($_GET['Successfully_Insert']))
	{
	echo "<h4 style=\"color:green\"align=\"center\">Successfully Added!</h4>";
	}
	if (isset($_GET['insetion_error']))
	{
	echo "<h4 style=\"color:red\"align=\"center\">please try again!</h4>";
	}
	echo' 
	<form  action="finance/account_add_expense_income.php" method="post">
	<div class="section ">
	<label>Select Category<small></small></label>        
	<div>
	<select  data-placeholder="Choose  Category..." class="chzn-select" tabindex="2" name="id" >        
	<option value=""></option>'; 										
	//query to get year
	$get_category="
		SELECT `category`,`id`,`type`
		FROM `transactions_credentials`
		WHERE `type`=0
		";
	$execute_category=mysql_query($get_category);
	while($fetch_category=mysql_fetch_array($execute_category))//looping for getting year
		{   
			$category=$fetch_category['category'];
			$cid=$fetch_category['id'];
			$type=$fetch_category['type'];
			
			echo'
			<option value="'.$cid.'">'. $category.'</option>';
		}				 
	echo' 
	</select>
	</div> 	
	</div>
	<div>
	<input type="hidden" name="type" value="'.$type.'" />
	</div>       
	<div class="section">
	<label>Select Date</label>   
	<div>
	<input type="text"  class=" birthday  small" name="date" id="datePicker"/>
	</div> 	
	</div>
	<div class="section  regexonly ">
	<label>Amount</label>      
	<div> 
	<input name="amount" id="amount" align="center" type="text"  class="validate required small" maxlength="20">
	</div>
	</div>
	<div class="section last">
	<div><button  class="uibutton submit_form" value="submit" >Go</button>
	</div>
	</div>
	</form>   
	';	
	echo'	    
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 				
}
//function to called By accounts_finanace_create_income.php
function  create_income()//enter the income details
{   
     //Author By: Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Create Income</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	if (isset($_GET['Successfully_Insert']))
	   {
	    echo "<h4 style=\"color:green\"align=\"center\">Successfully Added!</h4>";
	   }
	if (isset($_GET['error']))
	  {
	    echo "<h4 style=\"color:red\"align=\"center\">please try again!</h4>";
	  }
	echo' 
	<form  action="finance/account_add_expense_income.php" method="post">
	<div class="section ">
	<label>Select Category<small></small></label>        
	<div>
	<select  data-placeholder="Choose  Category..." class="chzn-select" tabindex="2" name="id" >        
	<option value=""></option>'; 								
	//query to get year
	$get_category="
		SELECT `category`,`id`,`type`
		FROM `transactions_credentials`
		WHERE `type`=1";
	$execute_category=mysql_query($get_category);
	while($fetch_category=mysql_fetch_array($execute_category))//looping for getting year
	   {   
		$category=$fetch_category['category'];
		$cid=$fetch_category['id'];
		$type=$fetch_category['type'];
		echo'
		<option value="'.$cid.'">'. $category.'</option>';
	   }					 
	echo' 
	</select>
	</div> 	
	</div>
	<div>
	<input type="hidden" name="type" value="'.$type.'" />
	</div>       
	<div class="section">
	<label>Select Date</label>   
	<div>
	<input type="text"  class=" birthday  small" name="date" id="datePicker"/>
	</div> 	
	</div>
	<div class="section  regexonly ">
	<label>Amount</label>      
	<div> 
	<input name="amount" id="amount" align="center" type="text"  class="validate required small" maxlength="20">
	</div>
	</div>
	<div class="section last">
	<div><button  class="uibutton submit_form" value="submit" >Go</button>
	</div>
	</div>
	</form>   ';	
	 echo'	    
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 				
}	
//function to called By accounts_finance_view_report.php
function  finance_transaction_view_report()//view the transaction report by date
 {   
	//Author By: Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Date</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	<br>
	<br>';
	echo '
	<form action="accounts_finance_view_transation_details.php" method="get">
	<div class="section">
	<label>From </label>   
	<div>
	<input type="text"  class=" birthday  small" name="date_from" id="datePicker1"/>
	</div> 	
	</div>	
	<div class="section">
	<label>To </label>   
	<div>
	<input type="text"  class=" birthday  small" name="date_to" id="datePicker2"/>
	</div> 	
	</div>	
	<div class="section last">
	<div><button  class="uibutton submit_form" value="submit" >Go</button>
	</div>
	</div>
	</form>
	'; 
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 		  
}

//function to called accounts_finance_view_transation_details.php	
function finance_transaction_view()//show the transation details
{
	//Author By: Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Transaction Details</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';
	echo'
	<ul class="uibutton-group">
	<li><a href="accounts_finance_edit_transaction_details.php?from='.$_GET['date_from'].'
	&to='.$_GET['date_to'].'" class="uibutton  icon edit  special" >Edit Details</a></li>  
	</ul>
	';
	if(($_GET['date_from']=="")|| ($_GET['date_to']==""))
	 {
	  header('location:accounts_finance_view_report.php');
	 }
	else
	$income_donation=0;
	$total_income=0;
	$total_expenses=0;
	$total_donation=0;
	//query to get donation details
	$get_donation="
		SELECT `measure_name`  
		FROM transaction_measure_details
		";
	$exe_donation=mysql_query($get_donation);
	$fetch_donation=mysql_fetch_array($exe_donation);
	
	//query to get measure details and donation details
	$get_id="
		SELECT transaction_measure_details.id,transaction_measure_details.value,
		transaction_donation_details.amount,
		transaction_donation_details.donor_name,
		transaction_donation_details.date,
		transaction_donation_details.description,
		transaction_donation_details.id
		FROM  transaction_donation_details
		
		INNER JOIN transaction_measure_details
		ON transaction_donation_details.measure_id=transaction_measure_details.id
		
		WHERE transaction_donation_details.date  BETWEEN '".$_GET['date_from']."' AND '".$_GET['date_to']."'";
	$exe_id=mysql_query($get_id);
	while($fetch_id=mysql_fetch_array($exe_id))
	    {
		$did=$fetch_id['id'];
		$income_amount=$fetch_id['amount'];
		$value=$fetch_id['value'];
		$tax_donation=$income_amount*$value/100;
		$total_income=$total_income+$income_amount;
		$total_expenses=$total_expenses+$tax_donation;
		$total_donation=$total_income-$total_expenses;
	   }
	echo'	
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th><h5>S.No.</h5></th>
	<th><h5>Particulars</h5></th>		
	<th><h5>Expenses(Rs.)</h5></th>	
	<th><h5>Income(Rs.)</h5></th>								
	</tr>
	</thead>	   	
	<tbody align="center"> 
	'; 
	$from_date=$_GET['date_from'];
	$to_date=$_GET['date_to'];
	$total_expense=0;
	$total_income=0;
	$total=0;
	$sno=0;	
	//query to get all transactions details
	$query_get_details_transactions = "
	SELECT id , category
	FROM transactions_credentials
	";
	$execute_get_details_transactions = mysql_query($query_get_details_transactions);
	$exp=array();
	$inc=array();
	$count=0;
	while($get_details_transactions = mysql_fetch_array($execute_get_details_transactions))
	    {      
		  $id=$get_details_transactions['id']; 
		  
		//query to sum of expenses and income				     
		$get_amount="
			SELECT  SUM(expenses), SUM(income)
			FROM transations_details
			WHERE `cId`=".$get_details_transactions[0]." AND `date` BETWEEN '".$from_date."' AND '".$to_date."' ";
		$exe_amount=mysql_query($get_amount);
		$fetch_amount=mysql_fetch_array($exe_amount);
		++$sno; 
		if(($fetch_amount[0]=="")||($fetch_amount[1]==""))
		{
			$expense_amount=0;
			$income_amount=0;
		}
		else
		{
			$expense_amount=$fetch_amount[0];	
			$income_amount=$fetch_amount[1];	
		}
		$exp[$count] = $expense_amount;
		$inc[$count] = $income_amount;
		
		$total_expense=$total_expense+$expense_amount;
		$total_income=$total_income+$income_amount;	
			
		echo'	
		<tr>
		<td><b>'.$sno.'</td>
		<td><a href="accounts_finance_view_particulars_details.php?id='.$id.'&from='.$from_date.'
		&to='.$to_date.'"<b>'.$get_details_transactions[1].'</td> </a>
		<td><b>'.$expense_amount.'</td>
		<td><b>'.$income_amount.'</td>
		</tr>';
		$count++;
	}//while loop close
	echo'<tr>
	<td><b>'.++$sno.'</td>
	<td><a href="accounts_finance_view_donation_details.php?from='.$_GET['date_from'].'
	&to='.$_GET['date_to'].'">'.$fetch_donation['measure_name'].'</a></td>
	<td></td>
	<td><b>'.$total_donation.'</b></td>
	</tr>';
	$total_income=$total_income+$total_donation;//clculate total income
	if($total_expense>$total_income)
	  {
	   $total=$total_expense-$total_income;
	  }
	else if($total_income>$total_expense)
	   {
	   $total=$total_income-$total_expense ;
	   }	
	echo'
	</tbody>
	</table>
	<h4 align="left" style="color:green"> Grand Total: '.$total.'</h4>
	';
	//show the graph 	
	echo "<script type='text/javascript'>
	$(function () {
	var chart;
	$(document).ready(function() {
	chart = new Highcharts.Chart({
	chart: {
	renderTo: 'container',
	type: 'column'
	},
	title: {
	text: 'Finance Transactions '
	},
	xAxis: {
	categories: [ 
	";
	//get all the category;
	$get_data="
		SELECT DISTINCT transactions_credentials.category,transactions_credentials.id
		FROM transactions_credentials
		";
	$exe_data=mysql_query($get_data);
	echo '"Donation",';
	while($fetch_data=mysql_fetch_array($exe_data))
	   {
	
	     echo "'$fetch_data[0]'".",";	
	   }
	echo "
	]
	},
	tooltip: {
	formatter: function() {
	return ''+
	this.series.name +': '+ this.y +'';
	}
	},
	credits: {
	enabled: false
	},
	series: [
	";
	$s=0;
	echo "{
	name: 'expense',
	data: [";
	echo $s.",";
	for($i=0;$i<$count;$i++)
	{
	if($i+1 == $count)
	{
	  $d="";	
	}
	else
	{
	  $d=",";	
	}
	echo -$exp[$i].$d;
	}
	echo "
	]
	},
	{
	name: 'income',
	data: [";
	echo $total_donation.",";
	for($j=0;$j<$count;$j++)
	{
	if($j+1 == $count)
	{
	  $f="";	
	}
	else
	{
	  $f=",";	
	}
	echo $inc[$j].$f;
	}
	echo "
	]
	},
	";
	echo "
	]
	});
	});
	});
	</script>";
	echo '
	<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 	
	
}
//function to called By accounts_finance_session.php
function accounts_view_particulars_details()//view the paricular details
{
	//Author By:Manish Mishra
	
	//query to get category details
	$get_details="SELECT *
	FROM `transations_details`
	WHERE `cId`=".$_GET['id']." AND date BETWEEN '".$_GET['from']."' AND '".$_GET['to']."'";
	$exe_details=mysql_query($get_details);
	
	//query to get category
	$get_category="SELECT `category`
	FROM `transactions_credentials`
	WHERE `id`=".$_GET['id']."
	";
	$execute_category=mysql_query($get_category);  
	$fetch_category=mysql_fetch_array($execute_category);
				  
	/*echo' 
		<table  class="table table-bordered table-striped" id="dataTable" >
     <tbody>
	<tr><td><h5><strong style="color:green">Particular</h5></td>
	<td>'.$fetch_category['category'].'</td>
    </tr>
		<tr><td><h5><strong style="color:green">Expense</h5></td>
	<td>'.$fetch_details['expenses'].'</td>
    </tr>
    <tr><td><h5><strong style="color:green">Income</h5></td>
	<td>'.$fetch_details['income'].'</td>
	</tr>
	<tr><td><h5><strong style="color:green">date</h5></td>
	<td>'.$fetch_details['date'].'</td>
	</tr>';
	*/
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View '.$fetch_category['category'].' Details</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';
	$total_expense=0;
	$total_income=0;
	echo'	
	<table  class="table table-bordered table-striped"  id="dataTable">
	<thead>
	<tr>
	<th><h5><strong style="color:brown">Expense(Rs.)</h5></th>	
	<th><h5><strong style="color:brown">Income(Rs.)</h5></th>
	<th><h5><strong style="color:brown">date</h5></th>								
	</tr>
	</thead>	   	
	<tbody align="center"> 
	'; 
	while($fetch_details=mysql_fetch_array($exe_details))
	    {
		echo'<tr>
		<td>'.$fetch_details['expenses'].'</td>
		<td>'.$fetch_details['income'].'</td>
		<td>'.$fetch_details['date'].'</td>
		</tr>
		';
		$total_expense=$total_expense+$fetch_details['expenses'];
		$total_income=$total_income+$fetch_details['income'];
	   }//while loop close		 
	if($total_expense>$total_income)
		{
		$total=$total_expense-$total_income;
		}
	else if($total_income>$total_expense)
		{
		$total=$total_income-$total_expense ;
		}
	echo'
	</tbody>
	</table>';	            
	echo'
	<h4 align="center" style="color:brown">Total Expenses: '. $total_expense.'</h4> 
	<h4 align="center" style="color:green">Total Income: '. $total_income.'</h4> ';   
	if($total_expense>$total_income)
	  {
	   $total=$total_expense-$total_income;
	   echo'<h4 align="left" style="color:brown">Total: '. $total.'</h4>';
	  }
	else if($total_income>$total_expense)
	       {
	        $total=$total_income-$total_expense ;
	        echo'<h4 align="left" style="color:green">Total:'. $total.'</h4> ';
	       }                 
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 			
}
//function to called By accounts_finance_edit_transaction_details.php
function accounts_edit_transactions_details()//edit the transaction details
{
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Edit Transaction Details</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';
	$from_date=$_GET['from'];
	$to_date=$_GET['to'];	
	//query to category details		
	$get_category="
		SELECT `category`,`id`
		FROM `transactions_credentials`
		";
	$execute_category=mysql_query($get_category);  
	echo'	
	<table  class="table table-bordered table-striped"  >
	<thead>
	<tr>
	<th><h5><strong style="color:brown">Category</h5></th>		
	<th><h5><strong style="color:brown">Expense(Rs.)</h5></th>	
	<th><h5><strong style="color:brown">Income(Rs.)</h5></th>
	<th><h5><strong style="color:brown">Date</h5></th>
	<th><h5><strong style="color:brown">Action</h5></th>								
	</tr>
	</thead>	   	
	<tbody align="center"> 
	'; 	
	while($fetch_category=mysql_fetch_array($execute_category))
	   {
		$id=$fetch_category['id'];
		//query to get category details
		$get_details="
			SELECT *
			FROM `transations_details`
			WHERE `cId`=".$id." AND `date` BETWEEN '".$from_date."' AND '".$to_date."'";
		$exe_details=mysql_query($get_details);
			while($fetch_details=mysql_fetch_array($exe_details))
			    {
				$id=$fetch_details['id'];
				
				echo'<tr id="'.$fetch_details['id'].'">
				<td>'.$fetch_category['category'].'</td>
				<td>'.$fetch_details['expenses'].'</td>
				<td>'.$fetch_details['income'].'</td>
				<td>'.$fetch_details['date'].'</td>
				<td >
				<span>
				<a href="#" class="table-icon edit" title="Edit" onclick="edit_transactions(\''.$fetch_category['category'].'\',
				'.$fetch_details['expenses'].','.$fetch_details['income'].',
				\''.$fetch_details['date'].'\','.$fetch_details['id'].',\''.$from_date.'\',\''.$to_date.'\')">
				<img src="images/icon/icon_edit.png"/></a></span>				
				</td>
				</tr>
				';
			   }//inner loop close
	    }//while loop close
	echo'
	</tbody>
	</table>';
	echo'	    
	
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 		
}
//function to called By accounts_finance_session.php
function session_details()//select the session details
{   
    //Author By:Manish Mishra
	echo  '<div class="row-fluid">                           
	<div class="span12  widget clearfix">
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Session</span>
	</div><!-- End widget-header -->		  
	<div class="widget-content"><br />
	<div class="row-fluid"> 			
	';	
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo'
	<form action="accounts_finance_view_asset_liability.php" method="get" name="session" >							
	<label><h4 align="center">select Session<h4><small></label>   
	<div align="center">											
	<select  data-placeholder="Choose  Session..." class="chzn-select" tabindex="2" name="session">        
	<option value=""></option>'; 	
								
	//query to get month from `session_table`  table
	$get_session="
		SELECT `sId`,`session_name`
		FROM `session_table` ";
	$get_session_exe=mysql_query($get_session);
	while($get_session_fetch=mysql_fetch_array($get_session_exe))
	    {
		$sid=$get_session_fetch['sId'];	
		$session_name=$get_session_fetch['session_name'];
		echo'
		<option value="'.$sid.'">'. $session_name.'</option>';
	    }    
	echo' 
	</select>       
	</div>									                           	       				
	<br></br>			   																
	<div class="section last" align="center">
	<button  class="uibutton submit_form" >Go</button>
	</div>
	</form>
	';
	echo'                                       
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';					
}
//function to called By accounts_finance_asset_liability.php
function create_asset_liability()//create asset &liability
{
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Create Asset & Liability</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';
	$session_id=$_GET['session'];		
	echo'
	<div class="span4">
	<a href="accounts_finance_create_asset.php?session_id='.$session_id.'"><div class="shoutcutBox">
    <span class="ico color chat-exclamation"></span><em><b style="color:green">Asset & Liability</b></em> </div>  
	';
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 			
}
//function to called By accounts_finance_create_asset.php
function create_asset()//create asset details
{    
    //Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Create & View  </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	$session_id=$_GET['session_id'];
/*	echo'
	<div class="span4">
    <a href="accounts_finance_create_form_asset.php?session_id='.$session_id.'"><div class="shoutcutBox">
	 <span class="ico color chat-exclamation"></span><em><b style="color:green">Create Asset & Liability</b></em> </div>';*/
	echo'<div class="breaks"><span></span></div>
	<a href="accounts_finance_view_asset_liability.php?session_id='.$session_id.'"><div class="shoutcutBox">
	<span class="ico color chat-exclamation"></span>  <em><b style="color:green">View Asset & Liability</b></em> </div>
	<div class="breaks"><span></span></div>
	</div><!-- span4 column-->                                        
	';	
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 			
}

//function to called By  accounts_finance_year_deduction_details.php
function finance_session_asset_liability()//select the session details
{
	//Author By:Manish Mishra
	echo  '<div class="row-fluid">                           
	<div class="span12  widget clearfix">
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Session</span>
	</div><!-- End widget-header -->		  
	<div class="widget-content"><br />
	<div class="row-fluid"> 			
	';	
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo'
	<form action="accounts_finance_create_form_asset.php" method="get" name="session" >						  
	<label><h4 align="center">select Session<h4><small></label>   
	<div align="center">											
	<select  data-placeholder="Choose  Session..." class="chzn-select" tabindex="2" name="session">        
	<option value=""></option>'; 								
	//query to get month from `fee_generated_sessions`  table
	$get_session="
		SELECT `sId`,`session_name`
		FROM `session_table` ";
	$get_session_exe=mysql_query($get_session);
	while($get_session_fetch=mysql_fetch_array($get_session_exe))
	   {
		$sid=$get_session_fetch['sId'];	
		$session_name=$get_session_fetch['session_name'];
		echo'
		<option value="'.$sid.'">'. $session_name.'</option>';
	   } 				   
	echo' 
	</select>       
	</div>									                           	       				
	<br></br>			   																
	<div class="section last" align="center">
	<button  class="uibutton submit_form" >Go</button>
	</div>
	</form>
	';
	echo'                                       
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';						
	
}
//function to called By accounts_finance_create_form_asset.php
function create_asset_form()//enter the asset details
{
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Add Asset & Libility </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';
	$session=$_GET['session'];
	if($session=="")
	{
		header('location:accounts_finance_session_asset_liabilty.php');
		}
	if (isset($_GET['Successfully_Insert']))
	   {
	    echo "<h5 style=\"color:green\"align=\"center\">Successfully Added !</h5>";
	   }	
	if (isset($_GET['error_adding']))
	   {
	    echo "<h5 style=\"color:red\"align=\"center\">Already Exists Please try again !</h5>";
	   }	
	echo'
	<form id="validation_demo" action="finance/accounts_add_asset_liability.php" method="get"> 
	<input type="hidden" name="id" />  
	<input type="hidden" name="session" value="'.$session.'" />           			 
	<div class="section ">
	<label>Title<small></small></label>   
	<div> 
	<input type="text" class="validate[required] small" name="title" />
	</div>
	</div>	
	<div class="section  regexonly ">
	<label>Amount</label>      
	<div> 
	<input name="amount" id="amount" type="text"  class="validate[required] small" maxlength="20">
	</div>
	</div>
	<div class="section ">
	<label>Type <small></small></label>   
	<div> 
	<div>
	<input type="radio" name="type" id="radio-1" value="1"  class="ck"/>
	<label for="radio-1">Asset</label>
	</div>
	<div>
	<input type="radio" name="type" id="radio-2" value="0"  class="ck"  />
	<label for="radio-2" >Liability</label>
	</div>
	</div>
	</div>
	<div class="section last">
	<div><button class="uibutton submit" rel="1" type="submit"  >Add</button>
	<button class="uibutton special"type="reset"  >Reset</button>   
	</div>
	</div>									
	</form>  
	';
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 				
}

//function to called By  accounts_finance_view_asset_liability.php
function  view_asset_liability_details()//show the asset & liability Details
{
     //Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Asset & Libility </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	if(($_GET['session'] == ""))
	  {
	   header('location:accounts_finance_session.php');
	  }
	else
	   $session_id=$_GET['session'];
	echo'
	<div class="span4">
	<a href="accounts_finance_view_asset_details.php?session_id='.$session_id.'"><div class="shoutcutBox">
	<span class="ico color chat-exclamation"></span><em><b style="color:red">View Asset</b> </em> </div>
	<div class="breaks"><span></span></div>
	<a href="accounts_finance_view_liability.php?session_id='.$session_id.'"><div class="shoutcutBox">
    <span class="ico color chat-exclamation"></span>  <em><b style="color:red">View Liability</b></em> </div>
	<div class="breaks"><span></span></div>
	</div><!-- span4 column-->                                        
	';	
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 					
}
//function to called By  accounts_finance_view_asset_details.php
function view_asset_details()//view Asset details
{
	//Author By:Manish Mishra	
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Asset  </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';		
	if (isset($_GET['Successfully_update']))
	   {
	    echo "<h5 style=\"color:green\"align=\"center\">Successfully Updated !</h5>";
	   }		
	$session_id=$_GET['session_id'];
	echo'	
	<table  class="table table-bordered table-striped"  id="dataTable">
	<thead>
	<tr>
	<th><h5>S.No.</h5></th>
	<th><h5>Asset</h5></th>		
	<th><h5>Amount(Rs.)</h5></th>	
	<th><h5>Date</h5></th>
	<th><h5>Action</h5></th>								
	</tr>
	</thead>	   	
	<tbody align="center"> 
	'; 
	$sno=0;	
	//query to get all transactions details
	$query_get_asset_details= "
		SELECT *
		FROM `asset_liability_details` 
		WHERE `type`=1 AND `session_id`=".$session_id."
		";
	$execute_get_asset_details = mysql_query($query_get_asset_details);
	while($get_asset_details = mysql_fetch_array($execute_get_asset_details))
		{           
		$id=$get_asset_details['id'];
		++$sno;
		if($get_asset_details['active/inactive'] == 1)
			{
			echo'	
			<tr style="color:orange">
			<td><b>'.$sno.'</td>
			<td><a style="color:orange" href="accounts_finance_show_asset_details.php?id='.$id.'&session_id='.$session_id.'">
			<b>'.$get_asset_details['title'].'(solved)</td></a>
			<td><b>'.$get_asset_details['amount'].'</td>
			<td><b>'.$get_asset_details['date'].'</td>
			<td>
			<span class="tip"><a href="accounts_finance_edit_asset_details.php?id='.$id.'
			&session_id='.$_GET['session_id'].'" class="table-icon edit" title="Edit">
			<img src="images/icon/icon_edit.png"/></a></span>
			<span class="tip" onclick="delete_confirm('.$id.','.$_GET['session_id'].');">
			<a class="table-icon delete" title="Delete" >
			<img src="images/icon/icon_delete.png"/></a></span>
			</td>
			</tr>';	 
		   }//close if condition
		else
		  {
			echo'	
			<tr>
			<td><b>'.$sno.'</td>
			<td><a href="accounts_finance_show_asset_details.php?id='.$id.'&session_id='.$session_id.'">
			<b>'.$get_asset_details['title'].'</td></a>
			<td><b>'.$get_asset_details['amount'].'</td>
			<td><b>'.$get_asset_details['date'].'</td>
			<td>
			<span class="tip"><a href="accounts_finance_edit_asset_details.php?id='.$id.'
			&session_id='.$_GET['session_id'].'" class="table-icon edit" title="Edit">
			<img src="images/icon/icon_edit.png"/></a></span>
			<span class="tip" onclick="delete_confirm('.$id.','.$_GET['session_id'].');">
			<a class="table-icon delete" title="Delete">
			<img src="images/icon/icon_delete.png"/></a></span>
			</td>
			</tr>';
		 }//close if condition
  }//close while condition
	echo'
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 		
}

//function to called By accounts_finance_view_liability.php
function view_liability_details()//view liability details
{	
    //Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Liability  </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	$session_id=$_GET['session_id'];
	if(isset($_GET['Successfully_update']))
	   {
	    echo "<h5 style=\"color:green\"align=\"center\">Successfully Updated !</h5>";
	   }				
	echo'	
	<table  class="table table-bordered table-striped"  id="dataTable">
	<thead>
	<tr>
	<th><h5>S.No.</h5></th>
	<th><h5>Liability</h5></th>		
	<th><h5>Amount(Rs.)</h5></th>	
	<th><h5>Date</h5></th>
	<th><h5>Action</h5></th>								
	</tr>
	</thead>	   	
	<tbody align="center"> 
	'; 
	$sno=0;	
	 //query to get all transactions details
	 $query_get_liability_details= "
			SELECT *
			FROM `asset_liability_details` 
			WHERE `type`=0 AND `session_id`=".$session_id."
	         ";
	$execute_get_liability_details = mysql_query($query_get_liability_details);
	while($get_liability_details = mysql_fetch_array($execute_get_liability_details))
	    {      
		$id=$get_liability_details['id'];    
	    ++$sno; 
			if($get_liability_details['active/inactive'] == 1)
			{
				echo'	
				<tr style="color:orange">
				<td><b>'.$sno.'</td>
				<td><a  style="color:orange" href="accounts_finance_view_liability_details.php?id='.$id.'
				&session_id='.$session_id.'"><b>'.$get_liability_details['title'].'(Solved)</a></td>
				<td><b>'.$get_liability_details['amount'].'</td>
				<td><b>'.$get_liability_details['date'].'</td>
				<td>
				<span class="tip"><a href="accounts_finance_edit_liability.php?id='.$id.'
				&session_id='.$session_id.' " class="table-icon edit" title="Edit">
				<img src="images/icon/icon_edit.png"/></a></span>
				<span class="tip" onclick="delete_liability_confirm('.$id.','.$_GET['session_id'].');">
				<a class="table-icon delete" title="Delete">
				<img src="images/icon/icon_delete.png"/></a></span>
				</td>
				</tr>';
			}//close if condition
			else
			{
				echo'	
				<tr>
				<td><b>'.$sno.'</td>
				<td><a href="accounts_finance_view_liability_details.php?id='.$id.'
				&session_id='.$session_id.'"<b>'.$get_liability_details['title'].'</td></a>
				<td><b>'.$get_liability_details['amount'].'</td>
				<td><b>'.$get_liability_details['date'].'</td>
				<td>
				<span class="tip"><a href="accounts_finance_edit_liability.php?id='.$id.'
				&session_id='.$_GET['session_id'].'" class="table-icon edit" title="Edit">
				<img src="images/icon/icon_edit.png"/></a></span>
				<span class="tip" onclick="delete_liability_confirm('.$id.','.$_GET['session_id'].');">
				<a class="table-icon delete" title="Delete">
				<img src="images/icon/icon_delete.png"/></a></span>
				</td>
				</tr>';
			}//close else
	   }//close while
	
	echo'
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 		
}
//function to called By accounts_finance_edit_asset_details.php
function edit_asset_details()//edit asset details
{	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Edit Asset </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';		
	$id=$_GET['id'];
	$session_id=$_GET['session_id'];
	//query to get all transactions details
	$get_asset_details= "
		SELECT *
		FROM `asset_liability_details` 
		WHERE `id`=".$id." AND `type`=1 AND `session_id`=".$session_id."
		";
	$exe_asset_details=mysql_query($get_asset_details);
	$fetch_asset_details=mysql_fetch_array($exe_asset_details);
	$title=$fetch_asset_details['title'];
	$amount=$fetch_asset_details['amount'];
	$date=$fetch_asset_details['date'];
	echo'
	<form id="validation_demo" action="finance/account_edit_asset_details.php" method="get"> 
	<input type="hidden" name="id" value="'.$id.'" />   
	<div>
	<input type="hidden" value="'.$session_id.'" name="session_id"> 
	</div>         			 
	<div class="section ">
	<label>Title<small></small></label>   
	<div> 
	<input type="text" class="validate[required] small" value="'.$title.'" name="title" />
	</div>
	</div>	
	<div class="section  regexonly">
	<label>Amount</label>      
	<div> 
	<input name="amount" id="amount" type="text" value="'.$amount.'" class="validate[required] small" maxlength="20">
	</div>
	</div>
	<div class="section">
	<label>Select Date</label>   
	<div>
	<input type="text"  class=" birthday  small" name="date"  value="'.$date.'"id="datePicker"/>
	</div> 	
	</div>	
	<div class="section ">
	<label>Inactive This Asset? <small></small></label>   
	<div> 
	<div>
	<input type="radio" name="solve" id="radio-1" value="1"  class="ck"/>
	<label for="radio-1">Yes</label>
	</div>
	<div>
	<input type="radio" name="solve" id="radio-2" value="0"  class="ck" />
	<label for="radio-2" >No</label>
	</div>
	</div>
	</div>';
	echo'
	<div class="section last">
	<div><button class="uibutton submit" rel="1" type="submit">Save</button>
	<button class="uibutton special"type="reset" >Reset</button>   
	</div>
	</div>									
	</form>  
	';
	echo'	
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 			
}

//function to called by accounts_finance_edit_liability.php
function edit_liability_details()//edit the liability Details
{
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Edit Liability </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	$session_id=$_GET['session_id'];		
	$id=$_GET['id'];
	//query to get all transactions details
	$get_asset_details= "
		SELECT `title`,`amount`,`date`,`type`
		FROM `asset_liability_details` 
		WHERE `id`=".$id." AND `type`=0 AND `session_id`=".$session_id."
		";
	$exe_asset_details=mysql_query($get_asset_details);
	$fetch_asset_details=mysql_fetch_array($exe_asset_details);
	$title=$fetch_asset_details['title'];
	$amount=$fetch_asset_details['amount'];
	$date=$fetch_asset_details['date'];
	echo'
	<form id="validation_demo" action="finance/accounts_edit_liability_details.php" method="get"> 
	<input type="hidden" name="id" value="'.$id.'" />    
	<input type="hidden" value="'.$session_id.'" name="session_id"/>         			 
	<div class="section ">
	<label>Title<small></small></label>   
	<div> 
	<input type="text" class="validate[required] small" value="'.$title.'" name="title" />
	</div>
	</div>	
	<div class="section  regexonly ">
	<label>Amount</label>      
	<div> 
	<input name="amount" id="amount" type="text" value="'.$amount.'" class="validate[required] small" maxlength="20">
	</div>
	</div>
	<div class="section">
	<label>Select Date</label>   
	<div>
	<input type="text"  class=" birthday  small" name="date"  value="'.$date.'"id="datePicker"/>
	</div> 	
	</div>	
	<div class="section ">
	<label>Inactive This Asset? <small></small></label>   
	<div> 
	<div>
	<input type="radio" name="solve" id="radio-1" value="1"  class="ck"/>
	<label for="radio-1">Yes</label>
	</div>
	<div>
	<input type="radio" name="solve" id="radio-2" value="0"  class="ck" />
	<label for="radio-2" >No</label>
	</div>
	</div>
	</div>';
	echo'
	<div class="section last">
	<div><button class="uibutton submit" rel="1" type="submit">Save</button>
	<button class="uibutton special"type="reset" >Reset</button>   
	</div>
	</div>									
	</form>  
	';
	echo'	
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	'; 	
}
//function to called By accounts_finance_donation details
function accounts_donation_details()//select donation details
{    
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Donation Details </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	echo'
	<div class="span4">
	<a href="accounts_finance_add_donation.php"><div class="shoutcutBox"> <span class="ico color chat-exclamation">
	</span><em><b style="color:green">Add Donation details</b></em> </div>';
	echo '<div class="breaks"><span></span></div>
	<a href="accounts_finance_add_measure_donation.php"><div class="shoutcutBox">
	<span class="ico color chat-exclamation"> </span>  <em><b style="color:green">Add Measure</b></em> </div>
	<div class="breaks"><span></span></div>
	</div><!-- span4 column-->                                        
	';	
	echo'	
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';
	}

//function to called By accounts_finance_add_donation.php
function  accounts_add_donation()//add donation details
{  
    //Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i> Donation Form</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	if (isset($_GET['Successfully_Insert']))
	    {
	     echo "<h4 style=\"color:green\"align=\"center\">Successfully Added!</h4>";
	    }
	//queryt to get id from transaction_measure_id
	$get_id="
		SELECT *
		FROM   `transaction_measure_details` 
		";
	$exe_id=mysql_query($get_id);
	$fetch_id=mysql_fetch_array($exe_id);
	$id=$fetch_id['id'];	
	echo'
	<form id="validation_demo" action="finance/accounts_add_donation_details.php" method="get">
	<input type="hidden" value="'.$id.'" name="id"/>  
	<div class="section ">
	<label>Donor Name<small></small></label>   
	<div> 
	<input type="text" class="validate[required] small" name="donor_name" />
	</div>
	</div>	
	<div class="section ">
	<label>Description<small></small></label>   
	<div> 
	<input type="text" class="validate[required] small" name="description" />
	</div>
	</div>	
	<div class="section  regexonly">
	<label>Amount</label>      
	<div> 
	<input name="amount" id="amount" type="text" class="validate[required] small" maxlength="20">
	</div>
	</div>
	<div class="section last">
	<div><button class="uibutton submit" rel="1" type="submit"  >Add</button>
	<button class="uibutton special"type="reset"  >Reset</button>   
	</div>
	</div>									
	</form>  
	';
	echo'	
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';	
}

//function to called By accounts_finance_donation_receipts.php
function accounts_donation_receipts()//print the donaton receipts
{   
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Print Receipts</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	<div id="print_donation">
	';		
	//query to get donation details 
	$donation_details="
		SELECT *
		FROM `transaction_donation_details` 
		WHERE `id`=".$_GET['id']."  ";	
	$exe_donation_details=mysql_query($donation_details);
	$fetch_donation_details=mysql_fetch_array($exe_donation_details);
	echo' 
	<table  class="table table-bordered table-striped"  >
	<tbody>
	<tr><td ><h5><strong style="color:green">Transaction Id</h5></td>
	<td>'.$fetch_donation_details['id'].'</td>
	</tr>
	<tr><td><b><strong style="color:green">Donor Name<b></td>
	<td>'.$fetch_donation_details['donor_name'].'</td>
	</tr>
	<tr><td><b><strong style="color:green">Description </b></td>
	<td>'.$fetch_donation_details['description'].'</td>								
	</tr>
	<tr><td><b><strong style="color:green">Amount</b></td>
	<td>'.$fetch_donation_details['amount'].'</td>
	<tr><td><b><strong style="color:green">Date<b></td>
	<td>'.$fetch_donation_details['date'].'</td>
	</tr>
	';	
	echo'
	</tbody>
	</table> 
	</div>
	';  
	//get count on that pass id
	echo'
	<button  class="btn btn-"  onclick="print_donation('.$_GET['id'].')" >Print</button>	 
	';
	echo'	
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';	
}
//function to called By accounts_finance_add_measure_donation.php
function accounts_add_measure_donation()
{  
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Add Measure </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	if(isset($_GET['error_adding']))
		{
		echo "<h5 style=\"color:red\"align=\"center\">Category Already Exist !!</h5>";
		}
	if(isset($_GET['adding_measure']))
		{
		echo "<h5 style=\"color:green\"align=\"center\">Successfully Added !!</h5>";
		}
	echo'	
	<form id="validation_demo" action="finance/accounts_add_donation_measure_value.php" method="get"> 
	<div class="section">
	<label>Category Name<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] small	" name="measure">
	</div>
	</div>																		
	<div class="section regexonly">
	<label>Value(%)<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] small" name="value">
	</div>
	</div>																																																				
	<div class="section last">
	<div><button class="uibutton submit"  rel="1" type="submit" >submit</button>			 
	<button class="uibutton special"type="reset" >Reset</button> 				          		  			
	</div>	
	</div>
	</form>
	';					
	echo'	
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';	
	}

//function to called By  accounts_finance_session_donation.php
function accounts_session_donation()
{  
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Session</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';		
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo'
	<form action="accounts_finance_view_donation.php" method="get" name="session" >		  
	<label><h4 align="center">select Session<h4><small></label>   
	<div align="center">											
	<select  data-placeholder="Choose  Session..." class="chzn-select" tabindex="2" name="session">        
	<option value=""></option>';								
	//query to get month from `session_table`  table
	$get_session="
		SELECT `sId`,`session_name`
		FROM `session_table` ";
	$get_session_exe=mysql_query($get_session);
	while($get_session_fetch=mysql_fetch_array($get_session_exe))
		{
		$sid=$get_session_fetch['sId'];	
		$session_name=$get_session_fetch['session_name'];
		echo'
		<option value="'.$sid.'">'. $session_name.'</option>';
		} 
					 
	echo'
	</select>       
	</div>									                           	       				
	<br></br>			   																
	<div class="section last" align="center">
	<button  class="uibutton submit_form" >Go</button>
	</div>
	</form>
	';	
	echo'	
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';			
}

//function to called By accounts_finance_view_donation.php
function accounts_view_donation_details()
{  //Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Donation</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	if(($_GET['session'] == ""))
		{
		header('location:accounts_finance_session_donation.php');
		}
	else
	$session_id=$_GET['session'];
	echo'
	<ul class="uibutton-group">
	<li><a href="accounts_finance_view_measure_details.php?session_id='.$session_id.'"
	 class="uibutton  icon  special" >View Measure Details</a></li>  
	</ul>
	';
	echo'	
	<table class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th><h5><strong style="color:brown">Donor Name</h5></th>	
	<th><h5><strong style="color:brown">Description</h5></th>		
	<th><h5><strong style="color:brown">Amount(Rs.)</h5></th>	
	<th><h5><strong style="color:brown">Date/Time</h5></th>	
	<th><h5><strong style="color:brown">Action</h5></th>								
	</tr>
	</thead>	   	
	<tbody align="center"> 
	'; 
	//query to get donation details
	$donation_details="
		SELECT *
		FROM `transaction_donation_details` 
		WHERE `session_id`=".$session_id."";
	$exe_donation_details=mysql_query($donation_details);
	while($fetch_donation_details=mysql_fetch_array($exe_donation_details))
		{      
		 $id=$fetch_donation_details['id'];
		echo'
		<tr id="'.$id.'">
		<td>'.$fetch_donation_details['donor_name'].'</td>
		<td>'.$fetch_donation_details['description'].'</td>
		<td>'.$fetch_donation_details['amount'].'</td>
		<td>'.$fetch_donation_details['date'].'</td>
		<td>
		<span>
		<a class="table-icon edit" title="Edit" onclick="edit_donation(\''.$fetch_donation_details['donor_name'].'\',\''.$fetch_donation_details['description'].'\','.$fetch_donation_details['amount'].',\''.$fetch_donation_details['date'].'\','.$fetch_donation_details['id'].','.$session_id.');">
		<img src="images/icon/icon_edit.png"/></a></span>  
		<span class="tip" onclick="delete_donation('.$id.','.$_GET['session'].');"><a class="table-icon delete" title="Delete">
		<img src="images/icon/icon_delete.png"/></a></span>
		</td>
		</tr>
		';	
		}//close while loop
	echo'	
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';			
}
//function to called By accounts_finance_view_measure_details.php
function accounts_view_measure_details()//view measure details
{	 //Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Measure Details </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	$session_id=$_GET['session_id'];	
	echo'	
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th><h5><strong style="color:brown">Measure Name</h5></th>
	<th><h5><strong style="color:brown">Value(%)</h5></th>	
	<th><h5><strong style="color:brown">Date/Time</h5></th>	
	<th><h5><strong style="color:brown">Action</h5></th>								
	</tr>
	</thead>	   	
	<tbody align="center"> 
	'; 
	//query to get measure details
	$get_measure="
		SELECT *
		FROM   `transaction_measure_details`
		WHERE `session_id`=".$session_id." ";
	$exe_measure=mysql_query($get_measure);
	while($fetch_measure=mysql_fetch_array($exe_measure))
		{       
		echo '<tr id="'.$fetch_measure['id'].'">
		<td>'.$fetch_measure['measure_name'].'</td>
		<td>'.$fetch_measure['value'].'</td>
		<td>'.$fetch_measure['date'].'</td>
		<td><span>
		<a href="#" class="table-icon edit" title="Edit" onclick="edit_measure(\''.$fetch_measure['measure_name'].'\',
		'.$fetch_measure['value'].',\''.$fetch_measure['date'].'\','.$fetch_measure['id'].','.$session_id.')">
		<img src="images/icon/icon_edit.png"/></a></span>
		</td>
		</tr>';
		}
	echo'	
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';			
}
//function to called by accounts_finance_view_donation_details.php
function view_donation_details()//view donation details
{
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Donation Details </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';
	echo'	
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th><h5><strong style="color:brown">Transaction Id</h5></th>
	<th><h5><strong style="color:brown">Donor Name</h5></th>
	<th><h5><strong style="color:brown">Description</h5></th>	
	<th><h5><strong style="color:brown">income</h5></th>
	<th><h5><strong style="color:brown">Expenses</h5></th>	
	<th><h5><strong style="color:brown">Date/Time</h5></th>			  
	</tr>
	</thead>	   	
	<tbody align="center"> 
	'; 
	$from=$_GET['from'];
	$to=$_GET['to'];
	$income_donation=0;
	$total_income=0;
	$total_expenses=0;
	$total=0;
	//query to get details of donation
	$get_id="
	SELECT transaction_measure_details.id,transaction_measure_details.value,
	transaction_donation_details.amount,transaction_donation_details.donor_name,
	transaction_donation_details.date,transaction_donation_details.description,
	transaction_donation_details.id
	FROM  transaction_donation_details
	
	INNER JOIN transaction_measure_details
	ON transaction_donation_details.measure_id=transaction_measure_details.id
	
	WHERE transaction_donation_details.date BETWEEN '".$from."' AND '".$to."' ";
	
	$exe_id=mysql_query($get_id);
	while($fetch_id=mysql_fetch_array($exe_id))
		{     
		$tid=$fetch_id['id'];
		$income_amount=$fetch_id['amount'];
		$value=$fetch_id['value'];
		$tax_donation=$income_amount*$value/100;
		echo'<tr>
		<td>'.$fetch_id['id'].'</td>
		<td>'.$fetch_id['donor_name'].'</td>
		<td>'.$fetch_id['description'].'</td>
		<td>'.$income_amount.'</td>
		<td>'.$tax_donation.'</td>
		<td>'.$fetch_id['date'].'</td>';
		$total_income=$total_income+$income_amount;
		$total_expenses=$total_expenses+$tax_donation;
		$total=$total_income-$total_expenses;
		}//close while 
	echo'	
	</tbody>
	</table>
	<h5 align="center" style="color:brown">Total Income: '.$total_income.'</h5>
	<h5 align="center" style="color:brown">Total Expenses: '.$total_expenses.'</h5>
	<h4 align="left" style="color:green">Total: '.$total.'</h4>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';	
}
//function to called By accounts_finance_view_liability_details.php
function accounts_view_liability_details()//view liability details
{  
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Liability Details </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	//query to get all transactions details
	$query_get_liability_details= "
		SELECT *
		FROM `asset_liability_details` 
		WHERE `type`=0 AND `session_id`=".$_GET['session_id']." AND `id`=".$_GET['id']."
		";
	$execute_get_liability_details = mysql_query($query_get_liability_details);
	$fetch_get_liability_details=mysql_fetch_array($execute_get_liability_details);
	$title=$fetch_get_liability_details['title'];
	$amount=$fetch_get_liability_details['amount'];
	$date=$fetch_get_liability_details['date']; 
	echo'	
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	</thead>	   	
	<tbody align="center"> 
	<tr><th><h5><strong style="color:brown">Title</h5></th>
	<td>'.$fetch_get_liability_details['title'].'</td>
	</tr>
	<tr><th><h5><strong style="color:brown">Amount</h5></th>
	<td>'.$fetch_get_liability_details['amount'].'</td>
	</tr>
	<tr><th><h5><strong style="color:brown">Date & Time</h5></th>	
	<td>'.$fetch_get_liability_details['date'].'</td>
	</tr>
	';
	echo'
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';		
}
//function to called By accounts_finance_show_asset_details.php
function accounts_show_asset_details()//view asset details
{
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Asset Details </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	//query to get all transactions details
	$query_get_asset_details= "
		SELECT *
		FROM `asset_liability_details` 
		WHERE `type`=1 AND `session_id`=".$_GET['session_id']." AND `id`=".$_GET['id']."
		";
	$execute_get_asset_details = mysql_query($query_get_asset_details);
	$fetch_get_asset_details=mysql_fetch_array($execute_get_asset_details);
	$title=$fetch_get_asset_details['title'];
	$amount=$fetch_get_asset_details['amount'];
	$date=$fetch_get_asset_details['date']; 
	echo'	
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	</thead>	   	
	<tbody align="center"> 
	<tr><th><h5><strong style="color:brown">Title</h5></th>
	<td>'.$fetch_get_asset_details['title'].'</td>
	</tr>
	<tr><th><h5><strong style="color:brown">Amount</h5></th>
	<td>'.$fetch_get_asset_details['amount'].'</td>
	</tr>
	<tr><th><h5><strong style="color:brown">Date & Time</h5></th>	
	<td>'.$fetch_get_asset_details['date'].'</td>
	</tr>
	';
	echo'</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';			  
}
//function to called By accounts_finance_session_pay_details.php
function  accounts_year_pay_details()//select year 
{
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select session </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';		
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo' 
	<form action="accounts_finance_pay_employee_details.php" method="get" name="year">
	<div class="section ">
	<label>Select Year<small></small></label>           
	<div >
	<select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" name="year" >        
	<option value=""></option>'; 								
	//query to get year		
	$get_year="
		SELECT DISTINCT `year`
	    FROM  `dates_d`
		";
	$execute_year=mysql_query($get_year);
	while($fetch_year=mysql_fetch_array($execute_year))//looping for getting year
		{   
		$year=$fetch_year['year'];
		echo'
		<option value="'.$fetch_year['year'].'">'. $year.'</option>';
		}
		echo'  </select> 
		</div>
		</div> 
		';				
		echo' 
		<div class="section ">
		<label>Select Month<small></small></label>              
		<div >
		<select  data-placeholder="Choose  month..." class="chzn-select" tabindex="2" id="month" name=month>        
		<option value=""></option>'; 								
		//query to get month and month of year		
		$get_month="
			SELECT DISTINCT `month` ,`month_of_year`
			FROM  `dates_d`
			";
		$execute_month=mysql_query($get_month);
	while($fetch_month=mysql_fetch_array($execute_month))//looping for getting month
		{   
		$month=$fetch_month['month'];
		echo'
		<option value="'.$fetch_month[1].'">'. $month.'</option>';
		}
	echo'
	</select> 
	</div> 
	</div> 
	<div class="section last">
	<div><button class="uibutton submit_form" rel="1"  value="submit" >Go</button>
	</div></div>
	</form>  
	';			
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';			
}
//function to called By accounts_finance_pay_employee_details.php
function  accounts_employee_pay_details()//generate empolyee payslip
{
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Employee Payslip</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	$year=$_GET['year'];
	$month=$_GET['month'];
	//query to get month name
	$date="
		SELECT `month`,`year`,month_of_year
		FROM `dates_d`
		WHERE `month_of_year`=".$month."";
	$exe_date=mysql_query($date);
	$fetch_date=mysql_fetch_array($exe_date);
	$month_name=$fetch_date['month'];	
	if(($_GET['year'] == ""))
		{
		header('location:accounts_finance_year_pay_details.php');
		}
		if(($_GET['month']==""))
		{
		header('location:accounts_finance_year_pay_details.php');
		}
	$deduction_cal=array();
	$count=0;
	echo'<h5 style="color:brown" align="center"> '.$year.'   '.$month_name.'</h5>                                                                        	 
	<table  class="table table-bordered table-striped" id="dataTable">
	<thead align="center">
	<tr>
	<th align="center" style="color:brown" width="20%"><b>Employee Id</th>
	<th align="center" style="color:brown"><b>Employee Name</th>
	<th align="center" style="color:brown"><b>Net Salary</th>
	<th align="center" style="color:brown"><b>Status</th>
	</tr>
	</thead>	 	
	<tbody align="center"> 
	';
	$pf="";
	$approved_amount=0;
	$total_net_salary=0;
	$net=array();
	 //query to gat date
	$get_date="
		SELECT `year`,`emp_id`,`month_of_year`
		FROM `employee_salary_details` 
		WHERE `year`=".$year." AND `month_of_year`=".$month."";
	$exe_date=mysql_query($get_date);
	while($fetch_date=mysql_fetch_array($exe_date))
		{    
		//get name fom users
		$get_name="
			SELECT name
			FROM users
			WHERE uId = ".$fetch_date['emp_id']."";
		$exe_name=mysql_query($get_name);
		$fetch_name=mysql_fetch_array($exe_name);
		$name=$fetch_name[0];
		$emp_id=$fetch_date['emp_id'];
		//query to gate name ,id
		$get_emp_name="
			SELECT DISTINCT *
			FROM employee_salary_details
			WHERE `emp_id`=".$fetch_date['emp_id']." AND `year`=".$year." AND `month_of_year`=".$month." 
			";
		$exe_name=mysql_query($get_emp_name); 
		$fetch_name=mysql_fetch_array($exe_name);
		$uId=$emp_id;
		/*	//get pf
		$get_pf=
		"SELECT PF
		FROM emplo";*/
		$pf=$fetch_name['PF'];
		//get the deduction on the id
		$get_deduction="
			SELECT SUM(amount)
			FROM employee_deduction_details
			WHERE emp_id =".$fetch_date['emp_id']." AND year=".$year."";
		$exe_deduction=mysql_query($get_deduction);
		$fetch_deduction=mysql_fetch_array($exe_deduction);
		$ded_sum=$fetch_deduction[0];//sum of deduction
		$net_pf=$fetch_name['net_salary']-$pf;
		$sal_net=$net_pf-($ded_sum+$pf);//calculate net salary
		//condition fo checking status
		if($fetch_name['status']==0)
			{
		if($sal_net < 0)
			{
			$sal_net="NA";	
			}
		echo'<tr>
		<td>'.$uId.'</td>
		<td>'.$name.'</td>
		';
		if($sal_net=="NA")
			{
			echo' <td>'.$sal_net.'</td>
			<td>Pending</td>';  
			}
		else
			{
			echo '
			<td>'.$sal_net.'</td>
			<td><a href="accounts_finance_pay_status.php?emp_id='.$uId.'&name='.$name.'&year='.$year.'&month='.$month.'">
			Pending</a></td>
			</tr>
			';
			}
		$total_net_salary=$total_net_salary+$sal_net;//calculate net salary	
		}
		else if($fetch_name['status']==1)
			{
			if($sal_net < 0)
			{
				$sal_net="NA";	
			}
		echo'<tr>
		<td>'.$uId.'</td>
		<td>'.$name.'</td>
		';
		if($sal_net=="NA")
			{
			echo' <td>'.$sal_net.'</td>
			<td>Pending</td>';  
			}
		else
			{
			echo '
			<td>'.$sal_net.'</td>
			<td><a href="accounts_finance_pay_status.php?emp_id='.$uId.'&name='.$name.'&year='.$year.'&month='.$month.'">
			Approved</a></td>
			</tr>
		     ';
		     }
		$total_net_salary=$total_net_salary+$sal_net;			
	}
		if($fetch_name['status']==1)
		{
		$approved_amount=$approved_amount+$sal_net;
		}
 	}//END OF WHILE
	//end of tax calculation	
	echo'
	</tbody>
	</table>
	<h4 style="color:green" >Total Net Salary='.$total_net_salary.'</h4>
	<h4 style="color:red" >Total Approved Salary='. $approved_amount.'</h4>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';		
}
//function to called By  accounts_finance_pay_status
function accounts_pay_status()//show the payslip
{  
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Employee Salary</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	$pf="";
	$deduction_cal=0;
	$name=$_GET['name'];
	$year=$_GET['year'];
	$month=$_GET['month'];
	$total_salary=0;
	$total_measure=0;
	$date="
		SELECT `month`,`year`,month_of_year
		FROM `dates_d`
		WHERE `month_of_year`=".$month."";
	$exe_date=mysql_query($date);
	$fetch_date=mysql_fetch_array($exe_date);
	$month_name=$fetch_date['month'];
		
	//view details of `employee_salary_details` table 						
	$view_salary_calculate="
		SELECT DISTINCT * 
		FROM `employee_salary_details`
		WHERE `emp_id`=".$_GET['emp_id']." AND `year`=".$year." AND `month_of_year`=".$month."
		";
	$view_result=mysql_query($view_salary_calculate);
	$fetch_result=mysql_fetch_array($view_result) ;   
	$status=$fetch_result['status'];
	if($status==1)
		{   
		echo'<h4 style="color:red" align="center">STATUS:APPROVED</h4>';
		}
	else
		{
		echo'<h4 style="color:red" align="center">STATUS:PENDING</h4>';
		}	
	echo '<h5 style="color:brown">Emp Id:  '.$fetch_result['emp_id'].'</h5>
	<h5 style="color:brown">Name:  '.$name.'</h5>
	<h5 style="color:brown" align="center"> '.$year.'   '.$month_name.'</h5>
	';
	
	echo' 	                                                                         	 
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>   
	';
	$get_mea="
		SELECT  DISTINCT measures
		FROM salary_calculator
		ORDER BY id DESC";
	$exe_mea_name=mysql_query($get_mea);
	$k=0;
	$mea=array();
	while($fetch_mea_name=mysql_fetch_array($exe_mea_name))
		{     
		$mea[$k]=$fetch_mea_name['measures'];
		echo '';	
		$k++;
		}
	echo '	
	</tr>
	</thead>	 	
	<tbody align="left"> 
	';	
	echo'<tr style="color:green"><td><b>Earnings</td>
	<td align="center"><b>Amount</td></tr>
	<tr align="center"> <th>Basic Salary</th>
	<td>'.$fetch_result['basic_salary'].'</td></tr></tr>	
	';
	$q=3;
	for($i=0;$i<$k;$i++)
		{
		if($fetch_result[$q]==0)
		{
		}
		else if($mea[$i]=='PF')
		{
		$pf=$fetch_result[$q];
		}
		else
		{
		echo '
		<tr align="center"><th>'.$mea[$i].'</th>
		<td>'.$fetch_result[$q].'</td>
		';
		$total_measure=$total_measure+$fetch_result[$q];
		}
		++$q;
		}
		$total_salary=$total_salary+$fetch_result['basic_salary'];
		$total_earning=$total_salary+$total_measure;
	echo '<tr align="center"><td><b>Total Salary</td>
	<td><b>Rs.'.$total_earning.'</td></tr>
	<tr style="color:green"><td><b>Deductions</td>
	<td></td></tr> ';
	//query to select deduction details
	$get_deduction_details="
		SELECT * 
		FROM `employee_deduction_details`
		WHERE `emp_id`=".$_GET['emp_id']." AND `year`=".$year."";
	$exe_deducation_details=mysql_query($get_deduction_details);
		while($fetch_deduction_details=mysql_fetch_array($exe_deducation_details))
		      {
				$deduction=$fetch_deduction_details['amount'];
				$deduction_did=$fetch_deduction_details['did'];
				
				//query to get deduction type
				$get_deduction_type="
					SELECT *
					FROM `finance_deduction_details`
					WHERE `id`=".$deduction_did." AND `year`=".$year."";
				$exe_deduction_type=mysql_query($get_deduction_type);
				while($fetch_deduction_type=mysql_fetch_array($exe_deduction_type))
					{  
					echo'<tr align="center">
					<td><b>'.$deduction_type=$fetch_deduction_type['deduction_type'].'</td>
					<td>'.$deduction.'</td>	
					</tr>				
					'; 
				$deduction_cal=$deduction_cal+$deduction;
					}  //end of inner while    
				}//end of while
				$deduction_cal+=$pf;
				$total_net_salary=$total_earning-$deduction_cal;			
	echo'
	<tr align="center"><td><b>PF</td><td>'.$pf.'</td></tr>
	<tr align="center"><td><b>Deductions amount</b></td>
	<td><b>Rs.'.$deduction_cal.'</td></tr>
	<tr style="color:green"><td><b>Total</td>
	<td></td></tr> 
	<tr align="center"><td><h5>Net Salary</td>
	<td><b>Rs.'.$total_net_salary.'</td></tr>	
	';	  		  	  
	echo'
	</tbody>
	</table>';	
	if($status==0)
	{
	echo'				  
	<div class="section last">	 
	<div><span class="tip"><button class="btn btn-" value="submit" onclick="approve_salary('.$_GET['emp_id'].',
	'.$year=$_GET['year'].',\''.$month=$_GET['month'].'\',\''.$name.'\')">Approve</button>		
	<button class="btn" value="submit" onclick="print_salary('.$_GET['emp_id'].','.$year=$_GET['year'].',
	\''.$month=$_GET['month'].'\',\''.$name.'\')">Print</button>';
	}
	else
	{
	echo'	
	<button class="btn"value="submit" onclick="print_approved_salary('.$_GET['emp_id'].','.$year=$_GET['year'].',
	\''.$month=$_GET['month'].'\',\''.$name.'\')">Print</button> 
	<button class="btn" value="submit"onclick="reject_salary('.$_GET['emp_id'].','.$year=$_GET['year'].',
	'.$month=$_GET['month'].',\''.$name.'\')">Reject</button>
	       
            <a href="accounts_finance_pay_employee_details.php?emp_id='.$_GET['emp_id'].'&year='.$year=$_GET['year'].'&month=
	'.$month=$_GET['month'].'&name=\''.$name.'\'"<button class=" btn"  rel="1">Back</button></a>			 
				
             ';
	
	
	}
	echo'
	</div>	
	</div>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';				
}
//function to called By accounts_finance_deduction_year.php
function accounts_deduction_year()//select year
{    
    //Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Year</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';
	
	if(isset($_GET['error_adding']))
		{
		echo "<h5 style=\"color:red\"align=\"center\">Category Already Exist !!</h5>";
		}
	if(isset($_GET['adding_deduction']))
		{
		echo "<h5 style=\"color:green\"align=\"center\">Successfully Added !!</h5>";
		}
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo' 
	<form action="finance/accounts_add_deduction_type.php" method="get" name="year">
	<div class="section ">
	<label>Select Year<small></small></label>           
	<div >
	<select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2"
	 id="year" name="year" onchange="show_deduction_year(this.value);">        
	<option value=""></option>'; 								
	//query to get year		
	$get_year="
		SELECT DISTINCT `year`
		FROM  `dates_d`
		";
	$execute_year=mysql_query($get_year);
	while($fetch_year=mysql_fetch_array($execute_year))//looping for getting year
		{   
		$year=$fetch_year['year'];
		echo'
		<option value="'.$fetch_year['year'].'">'. $year.'</option>';
		}
	echo'
	</select> 
	</div>
	</div> 
	<span id="data_deduction_year"></span>
	</form>
	';				
	echo' 				
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';				
}
//function to called By accounts_finance_pay_deduction_type.php
function  accounts_pay_deduction_type()//
{
/*echo '<div class="row-fluid">			  
     <div class="span12  widget clearfix">	
    <div class="widget-header">
    <span><i class="icon-align-center"></i>Add Deduction Type</span>
    </div><!-- End widget-header -->
    <div class="widget-content">
	
           						';		
	if(isset($_GET['error_adding']))
				{
					echo "<h5 style=\"color:red\"align=\"center\">Category Already Exist !!</h5>";
				}
				if(isset($_GET['adding_deduction']))
				{
					echo "<h5 style=\"color:green\"align=\"center\">Successfully Added !!</h5>";
				}
							
	echo'	
	      <form id="validation_demo" action="finance/accounts_add_deduction_type.php" method="get" name="deduction"> 
			 <div class="section ">
			 <label>Deduction Name<small></small></label>   
			 <div> 
			<input type="text"  class="validate[required] small	" name="deduction">
			</div>
			 </div>																		
			 																																																		
             <div class="section last" >
             <div><button class="uibutton submit"  rel="1" type="submit" >submit</button>			 
				
             </div>	
			 </div>
			 </form>
	';				
	
	
echo'
 <a href="accounts_finance_pay_view_deduction_type.php"><button class="uibutton special">View</button></a> 				 </a></div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
';		*/
	
}
//function to called By accounts_finance_view_deduction_year.php
function finance_view_deduction_year()//select year
{
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select Year </span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	if(isset($_GET['error_adding']))
		{
	echo "<h5 style=\"color:red\"align=\"center\">Category Already Exist !!</h5>";
		}
	if(isset($_GET['adding_deduction']))
		{
	echo "<h5 style=\"color:green\"align=\"center\">Successfully Added !!</h5>";
		}
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo' 
	<form action="accounts_finance_pay_view_deduction_type.php" method="get" name="year">
	<div class="section ">
	<label>Select Year<small></small></label>           
	<div >
	<select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" 
	name="year" onchange="show_deduction_year(this.value);">        
	<option value=""></option>'; 								
	//query to get year		
	$get_year="
		SELECT DISTINCT `year`
		FROM  `dates_d`
		";
	$execute_year=mysql_query($get_year);
	while($fetch_year=mysql_fetch_array($execute_year))//looping for getting year
		{   
		$year=$fetch_year['year'];
		echo'
		<option value="'.$fetch_year['year'].'">'. $year.'</option>';
		}				 
	echo'
	</select> 
	</div>
	</div> 
	<span id="data_deduction_view"></span>
	<button  class="uibutton special ">View</button>
	</form>
	';					
	//<a href="accounts_finance_pay_view_deduction_type.php"><button  class="uibutton special ">View</button></a> 
	echo' 
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';				
}
//function to called by accounts_finance_pay_view_deduction_type.php
function accounts_pay_view_deduction_type()//view deduction type
{
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View Deduction Details</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	echo'	
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th><h5><strong style="color:brown">S.No.</h5></th>	
	<th><h5><strong style="color:brown">Deduction Name</h5></th>	
	<th><h5><strong style="color:brown">Action</h5></th>			  
	</tr>
	</thead>	   	
	<tbody align="center"> 
	'; 
	if(($_GET['year'] == ""))
	{
		header('location:accounts_finance_view_year_deduction_type.php');
	}
	$year=$_GET['year'];
	$sno=0;
	//query to get deduction name
	$get_deduction="
		SELECT *
		FROM `finance_deduction_details` 
		WHERE `year`=".$_GET['year']." ";
	$exe_get_deduction=mysql_query($get_deduction);
	while( $fetch_deduction=mysql_fetch_array($exe_get_deduction))
		{    
		$deduction=$fetch_deduction['deduction_type'];
		$id=$fetch_deduction['id'];
		$sno++;
		echo'
		<tr>
		<td>'.$sno.'</td>
		<td>'.$deduction.'</td>
		<td>
	
		<span class="tip"><a href="accounts_finance_pay_edit_deduction_type.php?id='.$id.'&year='.$_GET['year'].'" 
		class="table-icon edit" title="Edit">
		<img src="images/icon/icon_edit.png"/></a></span>
		<span class="tip" onclick="delete_deduction_confirm('.$id.',year='.$year.');">
		<a class="table-icon delete" title="Delete">
		<img src="images/icon/icon_delete.png"/></a></span>
		</td>    
		</tr>
		';
	    }//end while
	echo'
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';			
}
//function to called By accounts_finance_pay_edit_deduction_type.php
function  accounts_pay_edit_deduction_type()//edit the deduction type
{
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Edit Deduction Details</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	$year=$_GET['year'];
	//query to get deduction details
	$get_deduction="
		SELECT *
		FROM `finance_deduction_details` 
		WHERE `id`=".$_GET['id']." AND year=".$year."";
	$exe_get_deduction=mysql_query($get_deduction);
	$fetch_deduction=mysql_fetch_array($exe_get_deduction);
	$deduction_name=$fetch_deduction['deduction_type'];
	echo'	
	<form id="validation_demo" action="finance/accounts_edit_deduction_type.php" method="get" name="deduction"> 
	<input type="hidden" name="deduction" value="'.$fetch_deduction['deduction_type'].'"/>
	<input type="hidden" name="id" value="'.$_GET['id'].'"/>
	<input type="hidden" name="year" value="'.$year.'"/>
	<div class="section ">
	<label>Deduction Name<small></small></label>   
	<div> 
	<input type="text"  class="validate[required] small	" name="deduction" value='.$deduction_name.'>
	</div>
	</div>																																																																
	<div class="section last">
	<div><button class="uibutton submit"  rel="1" type="submit" >submit</button>			 	  
	</div>	
	</div>
	</form>
	';						
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';			
}
//function to call accounts_finance_pay_deduction_details.php

//function accounts_deduction_year_details()
//{
/*echo '<div class="row-fluid">			  
     <div class="span12  widget clearfix">	
    <div class="widget-header">
    <span><i class="icon-align-center"></i>Select year & Month</span>
    </div><!-- End widget-header -->
    <div class="widget-content">
	';		
	
	$time_offset ="525"; // Change this to your time zone
 $time_a = ($time_offset * 120);
 $today = date("jS F Y");	 
echo '<h5 style="color:green" align="center">'.$today.'</h5>';	

echo' 
		<form action="accounts_finance_pay_deduction_details.php" method="get" name="year">
		<div class="section ">
        <label>Select Year<small></small></label>           
		 <div >
		 <select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" name="year" >        
		 <option value=""></option>'; 								
			//query to get year		
		$get_year="SELECT DISTINCT `year`
	                       FROM  `dates_d`
				   ";
	$execute_year=mysql_query($get_year);
	while($fetch_year=mysql_fetch_array($execute_year))//looping for getting year
	      {   
			 $year=$fetch_year['year'];
	
	echo'
		     <option value="'.$fetch_year['year'].'">'. $year.'</option>';
		  }
				  					   
	 echo'  </select> 
	        </div>
		   </div> 
				   ';				
			echo' 
		 <div class="section ">
         <label>Select Month<small></small></label>              
		 <div >
		 <select  data-placeholder="Choose  month..." class="chzn-select" tabindex="2" id="month" name=month>        
			    <option value=""></option>'; 								
			//query to get month and month of year		
		$get_month="SELECT DISTINCT `month` ,`month_of_year`
	                       FROM  `dates_d`
				   ";
	$execute_month=mysql_query($get_month);
	while($fetch_month=mysql_fetch_array($execute_month))//looping for getting month
	      {   
			 $month=$fetch_month['month'];
	
	echo'
		     <option value="'.$fetch_month[1].'">'. $month.'</option>';
		  }
				  					   
echo'    </select> 
	     </div> 
		 </div> 	
         <div class="section last">
         <div><button class="uibutton submit_form" rel="1"  value="submit" >Go</button>
		 </div></div>
		 </form>  
		 
	  ';			
echo'
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
';			*/	
//}


//function to called By accounts_finance_year_deduction_details.php
function finance_add_deduction_year_details()//select year
{   
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select year</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';		
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo' 
	<form action="accounts_finance_pay_deduction_details.php" method="get" name="year">
	<div class="section ">
	<label>Select Year<small></small></label>           
	<div >
	<select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" name="year" >        
	<option value=""></option>'; 								
	//query to get year		
	$get_year="
		SELECT DISTINCT `year`
		FROM  `dates_d`
		";
	$execute_year=mysql_query($get_year);
	while($fetch_year=mysql_fetch_array($execute_year))//looping for getting year
		{   
		$year=$fetch_year['year'];
		echo'
		<option value="'.$fetch_year['year'].'">'. $year.'</option>';
		}   
	echo'
	</select> 
	</div>
	</div> 
	';							  					   
	echo'   	
	<div class="section last">
	<div><button class="uibutton submit_form" rel="1"  value="submit" >Go</button>
	</div></div>
	</form>   
	';			
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';			
}

//function to called By accounts_finance_pay_deduction_details.php
function accounts_pay_deduction()//show the pay deduction
{    
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Add Deducttion Details</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	$year=$_GET['year'];
	if($year=="")
	{
		header('location:accounts_finance_year_deduction_details.php');
		}
	if(isset($_GET['error_adding']))
		{
		echo "<h5 style=\"color:red\"align=\"center\">Category Already Exist !!</h5>";
		}
	if(isset($_GET['adding_details']))
		{
		echo "<h5 style=\"color:green\"align=\"center\">Successfully Added !!</h5>";
		}
	echo'	
	<form action="finance/accounts_add_deduction_details.php" method="get" >
	<input type="hidden" name="year" value="'.$year.'" />
	<div class="section">
	<label>Employee Name<small></small></label>   
	<div>
	<select data-placeholder="Select a Employee..." class="chzn-select" tabindex="2"
	name="uId"  onchange="show_deduction_type(this.value,'.$year.');">
	<option value=""></option>
	';	
	//get the employee name from users table
	$get_user_name_query="SELECT `Name`,`uId` FROM `users`";
	$get_name_query_res=mysql_query($get_user_name_query);
	while($fetch_name=mysql_fetch_array($get_name_query_res))
		{  
		$name=$fetch_name[0];
		$uid=$fetch_name['uId'];
		echo '
		<option value="'.$uid.'">'.$name.'(id='.$uid.')</option>';
		}		
	echo'</select>       
	</div>
	</div>         	   
	<span id="data_deduction"></span>
	</div>
	</div>	
	</form>        										
	';	
	echo'</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';					
}
//function to called By accounts_finance_view_year_employee_deduction_details.php
function finance_year_employee_deduction_details()//select year
{
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select year</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';		
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo' 
	<form action="accounts_finance_pay_view_employee_deduction_details.php" method="get" name="year">
	<div class="section ">
	<label>Select Year<small></small></label>           
	<div >
	<select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" name="year" >        
	<option value=""></option>'; 								
	//query to get year		
	$get_year="
		SELECT DISTINCT `year`
		FROM  `dates_d`
		";
	$execute_year=mysql_query($get_year);
	while($fetch_year=mysql_fetch_array($execute_year))//looping for getting year
		{   
		$year=$fetch_year['year'];
		echo'
		<option value="'.$fetch_year['year'].'">'. $year.'</option>';
	}
	echo'
	</select> 
	</div>
	</div> 
	';							  					   
	echo'   	
	<div class="section last">
	<div><button class="uibutton submit_form" rel="1"  value="submit" >View</button>
	</div></div>
	</form>   
	';			
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';					
}
//function to called By accounts_finance_pay_view_employee_deduction_details.php
function accounts_view_employee_deduction_details()//view employee deduction details
{  
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Employee Deduction Details</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';	
	echo'<h3 style="color:green" align="center">'.$_GET['year'].'</h3>';
	echo'	
	<table  class="table table-bordered table-striped" id="dataTable" >
	<thead>
	<tr>
	<th><h5><strong style="color:brown">S.No.</h5></th>	
	<th><h5><strong style="color:brown"> Employee Id</h5></th>	
	<th><h5><strong style="color:brown"> Employee Name</h5></th>	
	<th><h5><strong style="color:brown"> Amount</h5></th>	
	<th><h5><strong style="color:brown">Deduction Type</h5></th>	
	<th><h5><strong style="color:brown">Action</h5></th>			
	</tr>
	</thead>	   	
	<tbody align="center"> 
	'; 
	$year=$_GET['year'];
	$sno=0;
	$ded=array();
	$id_ded=array();
	$amount=array();
	//query to get deduction details
	$get_details="
		SELECT DISTINCT users.uId,users.Name
		FROM employee_deduction_details
		
		INNER JOIN users
		ON employee_deduction_details.emp_id=users.uId
		
		INNER JOIN finance_deduction_details
		ON finance_deduction_details.id=employee_deduction_details.did 
		
		WHERE  employee_deduction_details.year=".$_GET['year']."
		";
	$k=0;
	$exe_details=mysql_query($get_details);
	while($fetch_details=mysql_fetch_array($exe_details))
		{ 
		$sno++;
		echo'
		<tr id="'.$fetch_details['uId'].'">
		<td>'.$sno.'</td>
		<td>'.$fetch_details[0].'
		<td>'.$fetch_details[1].'</td>
		';	
		//query to get the employee deduction details
		$get_details_next=
		"SELECT employee_deduction_details.amount,finance_deduction_details.deduction_type,finance_deduction_details.id
		FROM employee_deduction_details
		
		INNER JOIN finance_deduction_details
		ON finance_deduction_details.id = employee_deduction_details.did
		
		WHERE employee_deduction_details.emp_id=".$fetch_details[0]." AND employee_deduction_details.year=".$year."";
		$exe_next_details=mysql_query($get_details_next);
		echo '<td>';
		$c=0;
		while($fetch_next_details=mysql_fetch_array($exe_next_details))
			{
			//end		
			$id_ded[$c]=$fetch_next_details[2];	
			$amount[$c]=$fetch_next_details[0];
			$ded[$c]=$fetch_next_details[1];
			echo '<span id="edit_'.$c.'_'.$fetch_details[0].'"><b>'.$fetch_next_details[0]."</b></span><br></br>";
			$c++;	
			}
		echo '</td>';
		echo '<td>';
		for($i=0;$i<$c;$i++)
		{
		
			echo '<span id="edit_ded_'.$i.'_'.$fetch_details[0].'"><b>'.$ded[$i]."</b></span><br></br>";
		}
		echo '</td>';
		echo '<td>';
		for($i=0;$i<$c;$i++)
		{
		echo '<a class="table-icon edit" title="Edit" onclick="edit_deduction('.$id_ded[$i].','.$i.',
		'.$fetch_details[0].','.$amount[$i].',\''.$ded[$i].'\','.$year.')">
		<img src="images/icon/icon_edit.png"/></a>
		<a class="table-icon delete" title="Delete" onclick="delete_deduction_details_confirm('.$id_ded[$i].',
		'.$fetch_details[0].','.$amount[$i].','.$year.');">
		<img src="images/icon/icon_delete.png"/></a>		
		<br></br>';
		}
		echo '</td>';
		echo'
		</tr>
		';
	}//end of while
	echo '
	</tbody>
	</table>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';				
}
//function to called By accounts_finance_pf_year.php
function pf_year_employee_details()//select year
{
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select year</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';		
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo' 
	<form action="accounts_finance_pay_view_employee_pf_details.php" method="get" name="year">
	<div class="section ">
	<label>Select Year<small></small></label>           
	<div >
	<select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" name="year" >        
	<option value=""></option>'; 								
	//query to get year		
	$get_year="
		SELECT DISTINCT `year`
		FROM  `dates_d`
		";
	$execute_year=mysql_query($get_year);
	while($fetch_year=mysql_fetch_array($execute_year))//looping for getting year
		{   
		$year=$fetch_year['year'];
		echo'
		<option value="'.$fetch_year['year'].'">'. $year.'</option>';
		}
	echo'
	</select> 
	</div>
	</div> 
	';							  					   
	echo'   	
	<div class="section last">
	<div><button class="uibutton submit_form" rel="1"  value="submit" >Go</button>
	</div></div>
	</form>   
	';			
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';		
}
//function to callED bY accounts_finance_pay_view_employee_pf_details.php
function view_employee_pf_details()//show Pf details
{   
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View PF Details</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';			
	$year=$_GET['year'];
	echo'<h4 style="color:red" align="center">Year :'.$_GET['year'].'</h4>';
	echo'	
	<form>
	<input type="hidden" name="year" value="'.$year.'" />
	<div class="section">
	<label>Employee Name<small></small></label>   
	<div>
	<select data-placeholder="Select a Employee..." class="chzn-select" tabindex="2" name="uId" id="uid">
	<option value=""></option>
	';	
	//get the employee name from users table
	$get_user_name_query="SELECT `Name`,`uId` FROM `users`";
	$get_name_query_res=mysql_query($get_user_name_query);
	while($fetch_name=mysql_fetch_array($get_name_query_res))
		{  
		$name=$fetch_name[0];
		$uid=$fetch_name['uId'];
		echo '
		<option value="'.$uid.'">'.$name.'(id='.$uid.')</option>';
		}		
	echo'</select>       
	</div>
	</div>         	   
	<div class="section last">
	<div><button class="uibutton submit"  rel="1" type="button" onclick="show_pf_details('.$year.');" >View</button>	
	</div>
	</div>	
	</form>        										
	';	
	echo'
	<span id="data_pf"></span>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';		
}
//function to called by  accounts_finance_year_tax.php
function accounts_year_tax()//select year
{  
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Select year</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';		
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo' 
	<form action="accounts_finance_pay_view_employee_tax_details.php" method="get" name="year">
	<div class="section ">
	<label>Select Year<small></small></label>           
	<div >
	<select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" name="year" >        
	<option value=""></option>'; 								
	//query to get year		
	$get_year="
		SELECT DISTINCT `year`
		FROM  `dates_d`
		";
	$execute_year=mysql_query($get_year);
	while($fetch_year=mysql_fetch_array($execute_year))//looping for getting year
		{   
		$year=$fetch_year['year'];
		echo'
		<option value="'.$fetch_year['year'].'">'. $year.'</option>';
		}
	echo'
	</select> 
	</div>
	</div> 
	';							  					   
	echo'   	
	<div class="section last">
	<div><button class="uibutton submit_form" rel="1"  value="submit" >Go</button>
	</div></div>
	</form>   
	';			
	echo'
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';		
}
//function to called By accounts_finance_pay_view_employee_tax_details.php
function accounts_view_tax_details()//show tax details
{    
	//Author By:Manish Mishra
	echo '<div class="row-fluid">			  
	<div class="span12  widget clearfix">	
	<div class="widget-header">
	<span><i class="icon-align-center"></i>View PF Details</span>
	</div><!-- End widget-header -->
	<div class="widget-content">
	';			
	$year=$_GET['year'];
	echo'<h4 style="color:red" align="center">Year :'.$_GET['year'].'</h4>';
	echo'	
	<form>
	<input type="hidden" name="year" value="'.$year.'" />
	<div class="section">
	<label>Employee Name<small></small></label>   
	<div>
	<select data-placeholder="Select a Employee..." class="chzn-select" tabindex="2" name="uId" id="uid">
	<option value=""></option>
	';	
	//get the employee name from users table
	$get_user_name_query="SELECT `Name`,`uId` FROM `users`";
	$get_name_query_res=mysql_query($get_user_name_query);
	while($fetch_name=mysql_fetch_array($get_name_query_res))
		{  
		$name=$fetch_name[0];
		$uid=$fetch_name['uId'];
		echo '
		<option value="'.$uid.'">'.$name.'(id='.$uid.')</option>';
		}		
	echo'</select>       
	</div>
	</div>         	   
	<div class="section last" align="left">
	<div><button class="uibutton submit"  rel="1" type="button" onclick="show_tax_details('.$year.');" >View</button>	
	</div>
	</div>	
	</form>        										
	';	
	echo'
	<span id="data_tax"></span>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->	
	';			
}
?>