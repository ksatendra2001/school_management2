<?php
//In this all functions for vendors are declared
function admin_create_vendors()
{
		
	echo '	<!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Create Vendor </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
								<form id="validation_demo" action="processing/vendor_created.php" method="post"> 

                                    <div class="section">
                                                <label>Vendors name</label>   
                                                <div> <input type="text"  class="validate[required] medium" id="vendor_name" name="vendor_name" onblur="show_names_vendor();"/></div>
</div>

                                    <div class="section">
                                                <label>Vendors code</label>   
                                                <div> <input type="text"  class="validate[required] medium" name="vendor_code" id="show_codes"/></div>
</div>
  <div class="section">
                                                 <label>Vendors address</label>   
                                                <div> <textarea id="Textareaelastic"  class="large"  cols="" rows="" name="vendor_address"></textarea></div>   
                                                
                                             </div>
											  <div class="section">
                                    <label>Phone 
									</label>
                                    <div class="section numericonly">

                                        <input type="text" name="phone_no" class="medium"   />
                                        <span class="f_help"></span>
                                    </div>
                                </div>
								 <div class="section ">
                                              <label> Email Id</label>   
                                              <div> 
                                              <input type="text" class="validate[required,custom[email]]  large" name="e_required" id="e_required">
                                              </div>
                                              </div>
			  <div class="section">
                                    <label>Fax No
									</label>
                                    <div>
                                        <input  type="text"  name="fax_no"/>
                                        <span class="f_help"></span>
                                    </div>
                                </div>
  <div class="section">
                                    <label>Bank name
									</label>
                                    <div>
                                        <input  type="text"  name="bank_name"/>
                                        <span class="f_help"></span>
                                    </div>
                                </div>
								
								 <div class="section">
                                    <label>Account name
									</label>
                                    <div>
                                        <input  type="text"  name="acc_name"/>
                                        <span class="f_help"></span>
                                    </div>
                                </div>
								 <div class="section">
                                    <label>Account number
									</label>
                                    <div>
                                        <input  type="text"   name="acc_number"/>
                                        <span class="f_help"></span>
                                    </div>
                                </div>
								 <div class="section">
                                    <label>Bank branch
									</label>
                                    <div>
                                        <input  type="text"  name="bank_branch"/>
                                        <span class="f_help"></span>
                                    </div>
                                </div>
								 <div class="section">
                                    <label>Pan number
									</label>
                                    <div>
                                        <input  type="text"   name="pan_no"/>
                                        <span class="f_help"></span>
                                    </div>
                                </div>
								 <div class="section">
                                    <label>Contact person
									</label>
                                    <div>
                                        <input  type="text"  class="validate[required]" name="contact_person"/>
                                        <span class="f_help"></span>
                                    </div>
                                </div>
								 <div class="section ">
                                              <label>Vendor website</label>   
                                              <div> 
                                              <input type="text" class=" large" name="url_required" id="url_required"  value="http://">
                                              </div>
                                              </div>

								  <div class="section last">
                                              <div>
                                                <a class="btn submit_form" >Submit</a>
                                                <a class="btn" onClick="ResetForm()" title="Reset  Form">Clear Form</a>
                                             </div>
                                             </div>

								

									
						</form>		
                                   </div></div></div>
                                          ';
	
}
//function to view vendors
function admin_view_vendors()
{
	echo '
                    <div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-table"></i>View vendor</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
                                          <!-- Table UITab -->
                                          <div id="UITab" style="position:relative;">
										     <table class="table table-bordered table-striped data_table3 "  id="data_table3">
                                                  <thead class="align-center">
                                            <tr>
                                                <th>Vendor name</th>
                                                <th>Address</th>
                                                <th>Phone no.</th>
												<th>Email id</th>
												<th>Contact person</th>
												 <th>Vendor website</th>
												 <th>Action</th>

     
													 </tr>
													   </thead>
   <tbody align="center">
                                       ';
									   //query to get details of vendor for view
									   $query_vendor_details=
									   "SELECT * 
									   FROM `vendor_details`";
									   $exe_vendor_details=mysql_query($query_vendor_details);
									   while($fetch_vendor_details=mysql_fetch_array($exe_vendor_details))
									   {
									   
									   echo '<tr>
									 

									   
                                    <td><a href="admin_view_det_for_vendor.php?vendor_id='.$fetch_vendor_details[0].'&vendor_name='.$fetch_vendor_details['vendor_name'].'">'.$fetch_vendor_details['vendor_name'].'</a></td>
                                     
									  
									 <td>'.$fetch_vendor_details['address'].'</td>
									 <td>'.$fetch_vendor_details['phone_no'].'</td>
									 <td>'.$fetch_vendor_details['email_id'].'</td>
									 <td>'.$fetch_vendor_details['contact_person'].'</td>
									 <td><a href="'.$fetch_vendor_details['vendor_website'].'">'.$fetch_vendor_details['vendor_website'].'</a></td>
									 	 <td>
      <span class="tip" ><a href="admin_edit_vendor_details.php?id_vendor='.$fetch_vendor_details[0].'"  title="Edit" ><img src="../images/icon/icon_edit.png" ></a></span> 
                                                          
                                                         
	<span class="tip" ><a href="processing/admin_delete_vendor_details.php?id_vendor='.$fetch_vendor_details[0].'" data-name="delete name" title="Delete"  ><img src="../images/icon/icon_delete.png" ></a></span> </td>
									 </tr>';
									   }
 echo '
 	</tbody>
													  
													  </table>
                                              </div><!--tab1-->
											   </div>
                                      </div><!-- End UITab -->
                                      <div class="clearfix"></div>
                  
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div><!-- row-fluid -->


';
	
}
//function to view vendors other details
function  admin_view_det_for_vendor()
{
	$id_vendor=$_GET['vendor_id'];
	$name_vendor=$_GET['vendor_name'];
	//query to get imp details for that vendor
	$sql_vendor_acc_details=
	"SELECT * 
	FROM vendor_details
	WHERE vendor_id=".$id_vendor."";
	$exe_acc_details=mysql_query($sql_vendor_acc_details);
	
echo '	  <div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span>Vendor info ('.$name_vendor.')</span>
                                </div><!-- End widget-header -->	
                              

                                <div class="widget-content">
                                 <table  class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Fax number</th>
                                                <th>Bank name</th>
                                                <th>Account name</th>
												<th>Bank Branch</th>
												 <th>Pan number</th>
												 <th>Action</th>
                                            </tr>
											
                                        </thead>
                                        <tbody align="center">
										';
										while($fetch_acc_details=mysql_fetch_array($exe_acc_details))
										{
										
                                            echo '<tr class="odd gradeX">
												<td>'.$fetch_acc_details['fax'].'</td>
												<td>'.$fetch_acc_details['bank_name'].'</td>
												<td>'.$fetch_acc_details['account_name'].'</td>
												<td>'.$fetch_acc_details['bank_branch'].'</td>
												<td>'.$fetch_acc_details['pan_no'].'</td>
												 <td>
        <span class="tip" ><a href="admin_edit_vendor_acc_details.php?id_vendor='.$id_vendor.'&name_vendor='.$name_vendor.'"  title="Edit" ><img src="../images/icon/icon_edit.png" ></a></span> 
		                      
									</td>
                                                          
                                                         </td>

												</tr>
												';
										}
										echo '
                                        </tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div>';
					
					//for showing items of that vendor
					echo ' <div class="row-fluid">
                    
                    		<!-- Table widget -->
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span>Items of ('.$name_vendor.')</span>
                                </div><!-- End widget-header -->	
                              

                                <div class="widget-content">
                                    <table  class="table table-bordered table-striped">
                                        <thead>
                                          <th>Item name</th>
										  <th>Date of purchase</th>
										  <th>Quantity</th>
										  <th>Total cost</th>
											
                                        </thead>
                                        <tbody align="center">
										';
										//get items of that vendor acc ot the id
										$get_item_vendor=
										"SELECT item_name , date_of_purchase , quantity ,cost
										FROM items
										WHERE vendor_id=".$id_vendor."";
										$exe_item_vendor=mysql_query($get_item_vendor);
										
										while($fetch_item_vendor=mysql_fetch_array($exe_item_vendor))
										{
										
                                            echo '<tr class="odd gradeX">
												<td>'.$fetch_item_vendor['item_name'].'</td>
												<td>'.$fetch_item_vendor[1].'</td>
												<td>'.$fetch_item_vendor[2].'</td>
												<td>'.$fetch_item_vendor[3].'</td>
												

												</tr>
												';
										}
										echo '
                                        </tbody>
                                    </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div>';

	
	
}
//function edit vendor details
function admin_edit_vendor()
{
	
	$vendor_id=$_GET['id_vendor'];
	//query to get details of vendor for editing
	$sql_query_for_edit_vendor=
	"SELECT * 
	FROM vendor_details
	WHERE vendor_id=".$vendor_id."";
	$exe_query_for_vendor=mysql_query($sql_query_for_edit_vendor);
	$fetch_details_for_vendor=mysql_fetch_array($exe_query_for_vendor);
	
	
	$name_vendor=$fetch_details_for_vendor['vendor_name'];	
	$add_vendor=$fetch_details_for_vendor['address'];
	$phone_no=$fetch_details_for_vendor['phone_no'];
	$email_id=$fetch_details_for_vendor['email_id'];
	$contact_person=$fetch_details_for_vendor['contact_person'];
	$vendor_website=$fetch_details_for_vendor['vendor_website'];
	
	echo '	<!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Create Vendor </span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
								<form id="validation_demo" action="processing/vendor_details_edited.php" method="get"> 
                                 <input type="hidden" value="'.$vendor_id.'" name="vendor_id">
                                    <div class="section">
                                                <label>Vendors name</label>   
                                                <div> <input type="text"  class="validate[required] medium" name="vendor_name_edit" value="'.$name_vendor.'"/></div>
</div>
  <div class="section">
                                                 <label>Vendors address</label>   
                                                <div > <input id="tags_input" type="text" class="tags" value="'.$add_vendor.'" name="add_vendor_edit"/></div>   
                                                
                                             </div>
											  <div class="section">
                                    <label>Phone 
									</label>
                                    <div>
                                        <input type="text" name="phone_no_edit" class="validate[required,maxSize[10],] medium" value="'.$phone_no.'" />
                                        <span class="f_help"></span>
                                    </div>
                                </div>
								 <div class="section ">
                                              <label> Email Id</label>   
                                              <div> 
                                              <input type="text" class="validate[required,custom[email]]  large" name="email_id_edit" id="e_required" value="'.$email_id.'">
                                              </div>
                                              </div>
			  
								 <div class="section">
                                    <label>Contact person
									</label>
                                    <div>
                                        <input  type="text"  class="validate[required]" name="contact_person_edit" value="'.$contact_person.'"/>
                                        <span class="f_help"></span>
                                    </div>
                                </div>
								 <div class="section ">
                                              <label>Vendor website</label>   
                                              <div> 
                                              <input type="text" class="validate[required,custom[url]]  large" name="url_required_edit" id="url_required"  value="'.$vendor_website.'">
                                              </div>
                                              </div>

								  <div class="section last">
                                              <div>
                                                <a class="btn submit_form" >Submit</a>
                                                <a class="btn" onClick="ResetForm()" title="Reset  Form">Clear Form</a>
                                             </div>
                                             </div>

								

									
						</form>		
                                   </div></div></div>
                                          ';
	
	
	
}
//function to edit acc details of venodr
function edit_vendor_acc_details()
{
	
	//get id,name from admin accounts page for editing account info of vendor
		$id_for_acc_vendor=$_GET['id_vendor'];
		$name_vendor_editing=$_GET['name_vendor'];
		
		//query to fetch details from vendor_detials for editing its account information
		$sql_query_get_acc_details=
		"SELECT * 
		FROM vendor_details
		WHERE vendor_id=".$id_for_acc_vendor." ";
		$exe_acc_detials_vendor=mysql_query($sql_query_get_acc_details);
		$fetch_acc_details_vendor=mysql_fetch_array($exe_acc_detials_vendor);
	
	echo '	<!-- Dashboard  widget -->
<div class="row-fluid">
                            <div class="widget  span12 clearfix">
                            
                                <div class="widget-header">
                                    <span>Edit account details ('.$name_vendor_editing.')</span>
                                </div><!-- End widget-header -->	
                                
                                <div class="widget-content">
								<form id="validation_demo" action="processing/vendor_edit_acc_details.php" method="get"> 
                             <input type="hidden" value="'.$id_for_acc_vendor.'" name="id_vendor_acc"/>                                  
			  <div class="section">
                                    <label>Fax No
									</label>
                                    <div>
                                        <input  type="text"  class="validate[required]" name="fax_no_edit" value="'.$fetch_acc_details_vendor['fax'].'"/>
                                        <span class="f_help"></span>
                                    </div>
                                </div>
  <div class="section">
                                    <label>Bank name
									</label>
                                    <div>
                                        <input  type="text"  class="validate[required]" name="bank_name_edit" value="'.$fetch_acc_details_vendor['bank_name'].'"/>
                                        <span class="f_help"></span>
                                    </div>
                                </div>
								
								 <div class="section">
                                    <label>Account name
									</label>
                                    <div>
                                        <input  type="text"  class="validate[required]" name="acc_name_edit" value="'.$fetch_acc_details_vendor['account_name'].'"/>
                                        <span class="f_help"></span>
                                    </div>
                                </div>
								 <div class="section">
                                    <label>Account number
									</label>
                                    <div>
                                        <input  type="text"  class="validate[required]" name="acc_no_edit" value="'.$fetch_acc_details_vendor['account_no'].'"/>
                                        <span class="f_help"></span>
                                    </div>
                                </div>
								 <div class="section">
                                    <label>Bank branch
									</label>
                                    <div>
                                        <input  type="text"  class="validate[required]" name="bank_branch_edit" value="'.$fetch_acc_details_vendor['bank_branch'].'"/>
                                        <span class="f_help"></span>
                                    </div>
                                </div>
								 <div class="section">
                                    <label>Pan number
									</label>
                                    <div>
                                        <input  type="text"  class="validate[required]" name="pan_no_edit" value="'.$fetch_acc_details_vendor['pan_no'].'"/>
                                        <span class="f_help"></span>
                                    </div>
                                </div>
								

								  <div class="section last">
                                              <div>
                                                <a class="btn submit_form" >Submit</a>
                                                <a class="btn" onClick="ResetForm()" title="Reset  Form">Clear Form</a>
                                             </div>
                                             </div>

								

									
						</form>		
                                   </div></div></div>
                                          ';
	
	
	
	
}

?>




