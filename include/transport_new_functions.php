<?php
function admin_set_routes()
{
echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">

<div class="widget-header">
	<span><i class="icon-home"></i>Set routes</span>
</div><!-- End widget-header -->	

<div class="widget-content">
';	
if(isset($_GET['done']))
{
	echo '<h5 align="center">Route added</h5>';
	
}
if(isset($_GET['error']))
{
echo '<h5 align="center" style="color:red">Error!(destination or route exists)</h5>';	
	
}
echo '
<form>
<div class="section">
<label>Destination</label>
<div>
<input type="text" name="destination" id="destination"/>
</div>
</div>
<div class="section">
<label>Cost<small>month wise</small></label>
<div>
<input type="text" name="cost" id="cost"/>
</div>
</div>
<div class="section">
<label>Set this as main route?</label>
<div>
<input type="checkbox" name="set_main" id="set_main" onClick="show_checkbox();" checked="checked"/>
</div>
</div>
<div class="section" style="display:none" id="m_route">
<label>Main route</label>
<div>
<select class="chzn-select"  data-placeholder="Select Route" name="main_route" tabindex="1" id="main_route">
<option value=""></option>
';
//get all the routes
$get_route_data=
"SELECT DISTINCT routes_data.main_route,route_id
FROM routes_data
";
$exe_route_data=mysql_query($get_route_data);
$q=0;
$destination=array();
$route_id=array();
$main_route=array();
$cost=array();
while($fetch_exe_data=mysql_fetch_array($exe_route_data))
{

$main_route[$q]=$fetch_exe_data['main_route'];
$route_id[$q]=$fetch_exe_data[1];	
echo '<option value="'.$fetch_exe_data[0].'">'.$fetch_exe_data[0].'</option>';	
$q++;	
}
echo '
</select>
</div>
</div>

<div class="section last">
<div>
<button class="uibutton normal" onClick="set_main_route();" type="button">Save</button>
</div>
</div>
</form>
  <table  class="table table-bordered table-striped" id="dataTable" >
                              
					  
				 
				       <thead> 
				   <tr>
				   <th>Main Route</th>
				   <th>Cost</th>
				   <th>Destination</th>
				   <th>Action</th>
				   
				   </thead>
				   <tbody align="center">
				   ';
				   //get details on the main route
				   for($i=0;$i<$q;$i++)
				   {
					     echo '<tr>
				   <td><b>'.strtoupper($main_route[$i]).'</b></td>';
				   
				   $get_des_details=
				   "SELECT *
				   FROM destination_route
				   WHERE rId = ".$route_id[$i]."";
				   $exe_des_details=mysql_query($get_des_details);
				echo '<td>';
				$c=0;
				$t=0;
				   while($fetch_des_details=mysql_fetch_array($exe_des_details))
				   {
					    
				$destination[$c]=$fetch_des_details['d_name'];
				
					echo '<span id="cost_'.$route_id[$i].'_'.preg_replace('/[^A-Za-z0-9\-]/', '', "$destination[$c]").'">'.$fetch_des_details['cost'].'</span><hr>
					';
					   array_push($cost,$fetch_des_details['cost']);
				/*echo '
					<td>
								<a onclick="text_editable_transport()" class="table-icon edit" title="Edit"><img src="images/icon/icon_edit.png"></a>
								
							    <a class="table-icon delete" title="Delete"><img src="images/icon/icon_delete.png"></a>
					  		</td>
							
							
					';   */
					
					   
					$c++;
				   }
				   echo '
				   </td>
				  <td>';
				  
				  for($k=0;$k<$c;$k++)
				  {
					  
					  echo '<span id="dest_'.$route_id[$i].'_'.preg_replace('/[^A-Za-z0-9\-]/', '', "$destination[$k]").'">'.$destination[$k].'</span><hr>';
					  $t++;
				  }
				  echo '
				  </td>

				<td>';
				  
				  for($j=0;$j<$t;$j++)
				  {
					  
  echo '<span id="edit_'.$route_id[$i].'_'.preg_replace('/[^A-Za-z0-9\-]/', '', "$destination[$j]").'">

<img src="images/icon/icon_edit.png" onclick="edit_transport_route('.$route_id[$i].',\''.preg_replace('/[^A-Za-z0-9\-]/', '', "$destination[$j]").'\','.$cost[$j].', \''.$destination[$j].'\');">
&nbsp;&nbsp;&nbsp;

<img src="images/icon/icon_delete.png" onclick="delete_transport_route('.$route_id[$i].',\''.preg_replace('/[^A-Za-z0-9\-]/', '', "$destination[$j]").'\','.$cost[$j].', \''.$destination[$j].'\');">

</span><hr>';
					  
				  }
				  echo '
				  </td>
				   
				   </tr>';
				   }
				   echo '
				   
				 
				   </tbody>
				   </table>
';
	
echo '
</div>
</div>
</div>';	
	



}
function admin_add_vehicle()
{
//echo '<div align="right"><img src="images/bus.png" height="50px" width="150px"/></div>';	
echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">

<div class="widget-header">
	<span><i><img src="images/icon/gray_shadow18/location.png"/></i>Add new vehicle</span>
</div><!-- End widget-header -->	

<div class="widget-content">
';	
if(isset($_GET['done']))
{
echo '<h5 style="color:green" align="center">Vehicle added</h5>';		
}
if(isset($_GET['update']))
{
echo '<h5 style="color:green" align="center">Vehicle Eddited Successfuly</h5>';		
}
if(isset($_GET['delete']))
{
echo '<h5 style="color:red" align="center">Vehicle Deleted Successfuly</h5>';		
}

echo '
<form action="transport/add_new_vehicle.php" method="post">
<div class="section">
<label>Vehicle type</label>
<div>
<input type="text" name="vehicle_type" id="vehicle_type" required="required"/>
</div>
</div>
<div class="section">
<label>Vehicle number</label>
<div>
<input type="tel" name="vehicle_no" id="vehicle_no" required="required"/>
</div>
</div>
<div class="section">
<label>Route</label>
<div>
<select name="route_sel" class="chzn-select" data-placeholder="select route">
<option value=""></option>
';
//get all the routes
           $get_route_details=
						"SELECT *
						FROM routes_data";
$exe_route_details=mysql_query($get_route_details);
while($fetch_details=mysql_fetch_array($exe_route_details))
	{
		echo '<option value="'.$fetch_details[0].'">'.$fetch_details['main_route'].'</option>';	
	}
echo '
</select>
</div>
</div>
<div class="section">
<label>No of seats</label>
<div>
<input type="tel" name="no_seats" required="required"/>
</div>
</div>
<div class="section">
<label>status</label>
<div>
<input type="radio" name="status" value="1"><b>Active</b><br><br>
<input type="radio" name="status" value="0"><b>Inactive</b> 
</div>
</div>
<div class="section last">
<div>
<button class="uibutton normal">Save</button>
</div>
</div>
</form>';
//start table show details
echo '<table  class="table table-bordered table-striped" id="dataTable" >
				       <thead> 
				   <tr>
				   <th>Vehicle Number</th>
				   <th>Route</th>
				   <th>No of seats</th>
				   <th>Available Seats</th>
				   <th>Status</th>
				   <th>Action</th>
				   </thead>
				   <tbody align="center">
				   ';
//get all the values
 $get_vehicle_details=
"SELECT *
FROM vehicle_details";
$exe_details=mysql_query($get_vehicle_details);
while($fetch_details=mysql_fetch_array($exe_details))
{
//get route on the route id
   $get_route=
"SELECT *
FROM routes_data
where route_id=".$fetch_details[2]."";
$exe_data=mysql_query($get_route);
$fetch_data=mysql_fetch_array($exe_data);	
//$seats_left=$fetch_details[5]-$fetch_details[6];

//count assign transport student
 $count="SELECT COUNT(vehicle_no1) AS route
        FROM user_vehicle_details
		WHERE vehicle_no1=".$fetch_details[0]."";
$exe_count=mysql_query($count);
$fetch_count=mysql_fetch_array($exe_count);
$seats_left=$fetch_details[5]-$fetch_count[0];
    
echo '<tr>
<td>'.$fetch_details[3].'</td>
<td>'.$fetch_data['main_route'].'</td>
<td>'.$fetch_details[5].'</td>
<td>'.$seats_left.'</td>
<td>'
;
if($fetch_details[7] == 1)
{
echo 'Active';	
}
else
{
echo 'Inactive';	
}

echo '</td>
<td>
								<a href="edit_vehicle.php?vehicle_id='.$fetch_details[0].'&&main_route='.$fetch_data['main_route'].'&&seats_left='.$seats_left.'" class="table-icon edit" title="Edit"><img src="images/icon/icon_edit.png"></a>
								
							    <a href="transport/delete_vehicle_new.php?id='.$fetch_details[0].'" class="table-icon delete" title="Delete"><img src="images/icon/icon_delete.png"></a>
							</td>

</tr>';	
	
}
echo '
</tbody>
</table>
</div>
</div>
</div>';	
	
}
//function to edi vehicle details
function edit_vehicle()
{
$vec_id=$_GET['vehicle_id'];	
$route=$_GET['main_route'];
//get details on that vechicle id
$get_details=
"SELECT *
FROM vehicle_details
WHERE vehicle_id=".$vec_id."";
$exex_v_details=mysql_query($get_details);
$fetch_details=mysql_fetch_array($exex_v_details);

$v_type=$fetch_details['vehicle_type'];
$v_no=$fetch_details[3];
$no_seats=$_GET['seats_left'];
$st=$fetch_details['status'];
$seat=$fetch_details['no_seats'];
$route_id=$fetch_details['route_id'];
echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">

<div class="widget-header">
	<span><i><img src="images/icon/gray_shadow18/location.png"/></i>Add new vehicle</span>
</div><!-- End widget-header -->	

<div class="widget-content">
';	
echo '
<form action="transport/update_edit_vehicle.php" method="post">
<input type="hidden" name="id" value="'.$vec_id.'"/>
<input type="hidden" name="route_id" value="'.$route_id.'"/>
<div class="section">
<label>Vehicle type</label>
<div>
<input type="tel" name="vehicle_type" id="vehicle_type" required="required" value="'.$v_type.'"/>
</div>
</div>
<div class="section">
<label>Vehicle number</label>
<div>
<input type="tel" name="vehicle_no" id="vehicle_no" required="required" value="'.$v_no.'"/>
</div>
</div>
<div class="section">
<label>Route</label>
<div>
<select name="route_sel" class="chzn-select" data-placeholder="select route">
<option value="">'.$route.'</option>
';
//get all the routes
$get_route_details=
"SELECT *
FROM routes_data
WHERE route_id <>".$route_id."";
$exe_route_details=mysql_query($get_route_details);
while($fetch_details=mysql_fetch_array($exe_route_details))
{  $rid=$fetch_details['route_id'];
echo '<option value="'.$fetch_details[0].'">'.$fetch_details['main_route'].'</option>';	
}
echo '
<input type="hidden" name="rid" value="'.$rid.'"/>

</select>
</div>
</div>
<div class="section">
<label>No of seats</label>
<div>
<input type="tel" name="seats" required="required" value="'.$seat.'"/>
</div>
</div>
<div class="section">
<label>status</label>
<div>
';
if($st==1)
{
echo '
<input type="radio" name="status" value="1" checked="checked"><b>Active</b><br><br>
<input type="radio" name="status" value="0"><b>Inactive</b> 
';
}
else
{
echo '
<input type="radio" name="status" value="1"><b>Active</b><br><br>
<input type="radio" name="status" value="0" checked="checked"><b>Inactive</b> 
';
}
	
	

echo '
</div>
</div>
<div class="section last">
<div>
<button class="uibutton normal">Save</button>
</div>
</div>
</form>
';
echo 
'</div>
</div>
</div>';		
}
//function to allocate transport to user
function allocate_transport_admission()
{
    //Author by: Anil sir
	echo'
	<div class="row-fluid">
	<!-- Widget -->
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span>Allocata Transport</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<!-- title box -->
	<div class="boxtitle"><i class="icon-hdd"></i> Kindly Enter  <span class="netip"><a  class="red" > Student\'s Admission
	 Number  </a></span>
	</div>
	';		  
	if(isset($_GET['added']))
		{
			echo '<h4 align="center"><span style="color:green">Fee Generated Successfully!</span></h4>';
		}
	if(isset($_GET['error']))
		{
			echo '<h4 align="center"><span style="color:red">Error!</span></h4>';
		}
	echo'        
	<div class="section">
	<label>Admission No.</label>   
	<div> 
	<input type="text" name="admission_no" id="admission_no"  class=" medium" onkeyup="allocate_transport_admission(this.value)" autofocus/>
	</div>
	</div>
	<div id="generate_allocate">
	</div>
	</form>
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->';
    

}
//function to allocate transport to user
function allocate_transport()
{
	$q="SELECT `cId`,`class_name`, `index` FROM class_index ORDER BY  index+0 ASC ";
	$q_res=mysql_query($q);
	echo '<div class="row-fluid">
	<div class="span12  widget clearfix">
	<div class="widget-header">
	<span><i class="icon-align-center"></i>Transport (Kindly select class)</span>
	</div><!-- End widget-header -->	
	<div class="widget-content"><br />
	';



	if (isset($_GET['assignedRoute']))
		{
			echo "<h5 align=\"center\" style='color:green'>User Added to the specified route successfully!!</h5>";
		}
	if(isset($_GET['error_adding_vehicle']))
		{
			echo "<h5 align=\"center\" style='color:red'>Error!!</h5>";
		}
	if(isset($_GET['duplicate']))
		{
			echo "<h5 align=\"center\" style='color:red'>User Already Allocated!!</h5>";
		}
	

echo'<a href="show_students_class_transport.php?class_id=0"><button class="uibutton confirm" type="button">ALL SCHOOL TRANSPORT VIEW</button></a>';


	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");
	echo '<h5 style="color:grey" align="center">'.$today.'</h5>';
	$disp='';
	$q="SELECT  *  FROM class_index ORDER BY order_by  ASC";
	$q_res=mysql_query($q);
	echo '<ol class="rounded-list">';
	while($res=mysql_fetch_array($q_res))
		{
			echo '<tr class="row-fluid" id="added_rows">
			<td><div class="row-fluid">
			<div class="span6">	
			';
			echo'						                                                              
			<li><a href="show_students_class_transport.php?class_id='.$res['cId'].'"><span class="button approve" 
			id="btn">'.$res[1].'</span></a>                                                                     
			</div>
			</div><!-- end row-fluid  -->
			</td>
			</tr>';	 	 
		}
	echo'	
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';	
}
//function to show students of the class
function show_students_class()
{
$class_id=$_GET['class_id'];
//get students of tha6t class

if($class_id==0)
{
$get_stu_class=
"SELECT *
FROM student_user
INNER JOIN class
ON class.sId = student_user.sId
WHERE  class.session_id=".$_SESSION['current_session_id']."";	
}
else
{
$get_stu_class=
"SELECT *
FROM student_user
INNER JOIN class
ON class.sId = student_user.sId
WHERE class.classId=".$class_id." AND class.session_id=".$_SESSION['current_session_id']."";	
}

$exe_stu_class=mysql_query($get_stu_class);
echo '	 
<div class="row-fluid">
<div class="widget  span12 clearfix">
<div class="widget-header">
<span><i><img src="images/icon/gray_shadow18/location.png"/></i>Allocate Transport</span>
</div><!-- End widget-header -->	
<div class="widget-content">';

if(isset($_GET['dallocate']))
    {
            $name=$_GET['name'];
            echo "<h5 align=\"center\" style='color:red'>De-Allocate Transport of <b style='color:green'>(".$name." )</b></h5>";
    }
echo'<a href="admin_allocate_transport.php"><button class="uibutton confirm" type="button">Back</button></a>';
echo'
<table  class="table table-bordered table-striped" id="dataTable" >
                              
				   <thead> 
				   <tr>
				   <th>Student Name</th>
				   <th>Admission No.</th>
				   
				   <th>Action</th>
				   </thead>
				   <tbody align="center">
';	
while($fetch_stu_class=mysql_fetch_array($exe_stu_class))
{
echo '<tr>
<td>'.$fetch_stu_class['Name'].'</td>
<td>'.$fetch_stu_class['admission_no'].'</td>';
//  **************************   FEE_DETAILS ************* CONNECT  FEE  AND TRANSPORT ******************************
 //duration name
date_default_timezone_set('Asia/Kolkata');
$date=date('d-m-Y');
$month=explode("-",$date);

$get_fee="SELECT * FROM fee_details
         WHERE student_id=".$fetch_stu_class['sId']."
             AND fee_generated_session_id=".$month[1]."
                 AND session_id=".$_SESSION['current_session_id']."";
$exe_fee=mysql_query($get_fee);
$fetch_fee=mysql_fetch_array($exe_fee);

// GET TRANSPORT COST FROM DESTINATION ROUTE
     $student_id=$fetch_stu_class['sId'];
   $get_last_asign_transport_id="SELECT MAX(Id) FROM user_vehicle_details
                                 WHERE uId=".$student_id." AND session_id=".$_SESSION['current_session_id']."";
   $exe_last=mysql_query($get_last_asign_transport_id);
   $fetch_last_tr_id=mysql_fetch_array($exe_last);
   $max_id=$fetch_last_tr_id[0];

    $get_last_asign_transport_id1="SELECT status FROM user_vehicle_details
                                 WHERE Id=".$max_id." AND session_id=".$_SESSION['current_session_id']."";
   $exe_last1=mysql_query($get_last_asign_transport_id1);
   if(!$exe_last1)
   {
       $status=0;
   }
   else 
   {
        $fetch_last_tr_id1=mysql_fetch_array($exe_last1);
        $status=$fetch_last_tr_id1[0];
   }
    if($status ==0 )
    {
    echo '<td style="color:green"><a class="btn btn-success" href="show_details_root_student.php?student_id='.$fetch_stu_class[0].'&&name_stu='.$fetch_stu_class['Name'].'">Allocate</a></td>
    </tr>';	
    }
    elseif($status ==1 )
    {
         echo '<td style="color:red"><a class="btn btn-danger" href="transport/dallocate_vehicle_student.php?student_id='.$fetch_stu_class[0].'&&name_stu='.$fetch_stu_class['Name'].'&&class_id='.$class_id.'">Dallocate</a></td>
         </tr>';
    }
}
echo 
'</tbody>
</table>
</div>
</div>
</div>';		
}
function show_transport_details()
{
$sid=$_GET['student_id'];
$name_stu=$_GET['name_stu'];	

echo '	
<div class="row-fluid">
<div class="widget  span12 clearfix">
<div class="widget-header">
<span><i><img src="images/icon/gray_shadow18/location.png"/></i>Allocate Transport</span>
</div><!-- End widget-header -->	
<div class="widget-content"> ';
//get class for the following sid
$get_class=
"SELECT class_index.class_name,class_index.cId
FROM class_index
INNER JOIN class
ON class.classId = class_index.cId
WHERE class.sId = ".$sid."
";	
$exe_name_class=mysql_query($get_class);
$fetch_names=mysql_fetch_array($exe_name_class);
$class_id=$fetch_names[1];
echo'<a href="show_students_class_transport.php?class_id='.$class_id.'"><button class="uibutton confirm" type="button">Back</button></a>';
if(isset($_GET['assignedRoute']))
		{
			echo "<h5 align=\"center\" style='color:green'>Allocate Transport</h5>";
		}	
	if(isset($_GET['alreadyassigned']))
		{
			echo "<h5 align=\"center\" style='color:red'>Already Assigned!!</h5>";
		}	
			
	if(isset($_GET['notavailable']))
		{
			echo "<h5 align=\"center\" style='color:red'>Seat not Available in this vehicle!!</h5>";
		}	
				
			
echo 
'<form action="transport/allocate_vehicle_student.php" method="get">
<input type="hidden" name="sid" value="'.$sid.'"/>
<input type="hidden" name="sname" value="'.$name_stu.'"/>
<div class="section">
<label>Name</label>
<div>
<h5>'.$name_stu.'</h5>
</div>
</div>

<div class="section">
<label>Route</label>
<div>
<select class="chzn-select" name="route_sel" id="select_route" onchange="get_destination(this.val);" data-placeholder="Select route">
<option value=""></option>
';
//get all the routes from routes_data
$get_routes=
"SELECT main_route,route_id
FROM routes_data";
$exe_routes=mysql_query($get_routes);
while($fetch_routes=mysql_fetch_array($exe_routes))
{
echo '<option value="'.$fetch_routes[1].'" id="'.$fetch_routes[0].'">'.$fetch_routes[0].'</option>';	
	
}
echo '
</select>
</div>
</div>
<span id="destination"></span>

</form>';
echo 
'</div>
</div>
</div>';	
}

//manish codding start
//function to called by admin_view_new_tranport_details.php
function view_transport_details()
{
	echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>View Transport Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';		
	if(isset($_GET['delete_d']))
		{
			echo "<h5 align=\"center\" style='color:green'>Delete Allocated Student</h5>";
		}	
	echo'<table  class="table table-bordered table-striped" id="dataTable" >
                              
				   <thead style="color:brown"> 
				   <tr>
				   <th>Serial No.</th>
				   <th>Vehicle Type</th>
				    <th>Main Route</th>
					<th>Total Seat</th>
					<th>Available Seat</th>
				  
				   </thead>
				   <tbody align="center">';	
				   
	  		$sno=0;	   
		//get all the values
 $get_vehicle_details=
"SELECT *
FROM vehicle_details";
$exe_details=mysql_query($get_vehicle_details);
while($fetch_details=mysql_fetch_array($exe_details))
{      $sno++;
//get route on the route id
   $get_route=
"SELECT *
FROM routes_data
where route_id=".$fetch_details[2]."";
$exe_data=mysql_query($get_route);
$fetch_data=mysql_fetch_array($exe_data);	

//count assign transport student
 $count="SELECT COUNT(vehicle_no1) AS route
        FROM user_vehicle_details
		WHERE vehicle_no1=".$fetch_details[0]."";
$exe_count=mysql_query($count);
$fetch_count=mysql_fetch_array($exe_count);
$seats_left=$fetch_details[5]-$fetch_count[0];
    
echo '<tr>
<td>'.$sno.'</td>
<td>'.$fetch_details[1].'</td>
<td><a href="admin_view_student_new_transport_details.php?rid='.$fetch_data[0].'&vehicle_id='.$fetch_details[0].'&license_no='.$fetch_details[3].'">'.$fetch_data['main_route'].'</td></a>
<td>'.$fetch_details[5].'</td>
<td>'.$seats_left.'</td>
'
;		   				
}
     echo'
	 </tbody>
	 </table>	
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';		
}


function view_transport_student_all_details()
{
    echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>View Transport Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';	
	if(isset($_GET['update']))
		{
			echo "<h5 align=\"center\" style='color:green'>Update successfully</h5>";
		}	
	if(isset($_GET['error']))
		{
			echo "<h5 align=\"center\" style='color:red'>Please Try again!!</h5>";
		}	
			


    
 
	$sr=0;
	
	$session_id=$_SESSION['current_session_id'];
	//$vehicle_id=$_GET['vehicle_id'];
	//$license_no=$_GET['license_no'];
	echo'<table  class="table table-bordered table-striped" id="dataTable" >
                              
				   <thead style="color:brown"> 
				   <tr> <th>S. No.</th>
				   <th>Admission No.</th>
				   <th>Name</th>
				    <th>class</th>
					
					<th>Destination</th>
					<th>Fare</th>
					<th>Vehicle name.</th>
                                        <th>Destination.</th>
					<th>Action</th>
				   </thead>
				   <tbody align="center">';		
	
	//query to get details
   $queryStudentNameClass = "
		SELECT student_user.Name, student_user.admission_no,
		 class_index.class_name, student_user.sId,vehicle_details.vehicle_id,vehicle_details.license_no,user_vehicle_details.route_id
		FROM student_user
		
		INNER JOIN user_vehicle_details
		ON student_user.sId = user_vehicle_details.uId
		
		INNER JOIN class
		ON student_user.sId = class.sId
		
		INNER JOIN class_index
		ON class.classId = class_index.cId
		
		INNER JOIN vehicle_details 
		ON vehicle_details.vehicle_id= user_vehicle_details.vehicle_no1
		
		WHERE   class.session_id=$session_id
		";
	  $total=0;              
	$exe_details=mysql_query($queryStudentNameClass);
	while($fetch_details=mysql_fetch_array($exe_details))
	             
			{       
            $sr++;
                                                                              $sid=$fetch_details['sId'];
                                                                                 $vid=$fetch_details['vehicle_id'];
                                                                                 $lic_no=$fetch_details['license_no'];
                                                                                 $route_id=$fetch_details['route_id'];
                                                                                 
					
                                                                                 $get_cost="SELECT  destination_route.*,routes_data.*
				            FROM destination_route
							
							INNER JOIN user_vehicle_details 
							ON user_vehicle_details.dId=destination_route.dId
                                                                                                                                                                           INNER JOIN routes_data 
							ON routes_data.route_id=destination_route.rId
                                                                                                                                                                             

					     WHERE user_vehicle_details.uId=$sid AND destination_route.rId=$route_id AND routes_data.route_id=$route_id
										";
								
				$exe_cost=mysql_query($get_cost);
				$fetch_cost=mysql_fetch_array($exe_cost);
				 $did=$fetch_cost['dId'];
                                  $cost=$fetch_cost['cost'];
                                   $d_name=$fetch_cost['d_name'];
                                 

								 
	                                                                  echo'<tr>  <td>'.$sr.'</td>
			                       <td>'.$fetch_details[1].'</td>
				 <td>'.$fetch_details[0].'</td>
				 <td>'.$fetch_details[2].'</td>
				 
				 <td>'.$fetch_cost['d_name'].'</td>
				 <td>'.$fetch_cost['cost'].'</td>
				  <td>'.$fetch_cost['main_route'].'</td>
                                       <td>'.$fetch_cost['d_name'].'</td>
				<td><a href="admin_transport_edit_allocate_student.php?sid='.$sid.'&vid='.$vid
				  .'&route_id='.$route_id.'&did='.$did.'" class="table-icon edit" title="Edit">
			  <img src="images/icon/icon_edit.png"></a>
			<a href="transport/delete_allocated_student.php?sid='.$sid.'&vid='.$vid
				  .'&rid='.$route_id.'&did='.$did.'" class="table-icon delete" title="Delete">
			<img src="images/icon/icon_delete.png"></a></td>	   </tr>';
			    $total=$total+ $fetch_cost['cost'];
			}
	 echo'
	 </tbody>
    </table>	
	<h5 align="left" style="color:red">Total:&nbsp;'.$total.'</h5>
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';		
	   
    
    
    
    
    
    
    
    
}



//function to called by admin_view_student_new_transport_details.php
function view_student_transport_details()
{
	echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>View Transport Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';	
	
	
	$sr=0;
	
	$session_id=$_SESSION['current_session_id'];
	$vehicle_id=$_GET['vehicle_id'];
	$license_no=$_GET['license_no'];
        	echo'<a href="transport/admin_print_transport_details.php?rid='.$_GET['rid'].'&vehicle_id='.$vehicle_id.'&license_no='.$license_no.'"><button class="uibutton confirm" type="button">Print</button></a>';

        
	echo'<table  class="table table-bordered table-striped" id="dataTable"  >
                              
				   <thead style="color:brown"> 
				   <tr> <th>S. No.</th>
				   <th>Admission No.</th>
				   <th>Name</th>
				    <th>class</th>
					
					<th>Destination</th>
					<th>Fare</th>
					<th>License No.</th>
					<th>Action</th>
				   </thead>
				   <tbody align="center">';		
	
	//query to get details
  $queryStudentNameClass = "
		SELECT student_user.Name, student_user.admission_no,
		 class_index.class_name, student_user.sId,vehicle_details.vehicle_id,vehicle_details.license_no
		FROM student_user
		
		INNER JOIN user_vehicle_details
		ON student_user.sId = user_vehicle_details.uId
		
		INNER JOIN class
		ON student_user.sId = class.sId
		
		INNER JOIN class_index
		ON class.classId = class_index.cId
		
		INNER JOIN vehicle_details 
		ON vehicle_details.vehicle_id= user_vehicle_details.vehicle_no1
		
		WHERE vehicle_details.route_id = ".$_GET['rid']." AND vehicle_details.vehicle_id=".$vehicle_id." AND vehicle_details.license_no=".$license_no." AND class.session_id=".$session_id." AND student_user.sId NOT IN

(
SELECT student_id FROM struck_off_student
)
		";
	  $total=0;              
	$exe_details=mysql_query($queryStudentNameClass);
	while($fetch_details=mysql_fetch_array($exe_details))
	             
			{       
            $sr++;
                                                                              $sid=$fetch_details['sId'];
					
				 $get_cost="SELECT *
				            FROM destination_route
							
							INNER JOIN user_vehicle_details 
							ON user_vehicle_details.dId=destination_route.dId	
					     WHERE user_vehicle_details.uId=".$fetch_details[3]."
					 AND user_vehicle_details.route_id=".$_GET['rid']."
					";
							
				$exe_cost=mysql_query($get_cost);
				$fetch_cost=mysql_fetch_array($exe_cost);
				 $did=$fetch_cost['dId'];
				 
	        echo'<tr>  <td>'.$sr.'</td>
			     <td>'.$fetch_details[1].'</td>
				 <td>'.$fetch_details[0].'</td>
				 <td>'.$fetch_details[2].'</td>
				 
				 <td>'.$fetch_cost['d_name'].'</td>
				 <td>'.$fetch_cost['cost'].'</td>
				  <td>'.$fetch_details['license_no'].'</td>
				  <td><a href="admin_transport_edit_allocate_student.php?sid='.$sid.'&vid='.$vehicle_id
				  .'&route_id='.$_GET['rid'].'&did='.$did.'" class="table-icon edit" title="Edit">
			  <img src="images/icon/icon_edit.png"></a>
				</td>
			   </tr>';
			    $total=$total+ $fetch_cost['cost'];
			}
	 echo'
	 </tbody>
    </table>	
	<h5 align="left" style="color:red">Total:&nbsp;'.$total.'</h5>
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';		
	
}

//priv student
function view_student_transport_vehicle_details()
{
	
echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>View Transport Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';	
	//$vehicle_id=$_GET['vehicle_id'];
	///$license_no=$_GET['license_no'];
	echo'<table  class="table table-bordered table-striped" id="dataTable" >
                              
				   <thead style="color:brown"> 
				   <tr>
				   <th>Admission No.</th>
				   <th>Name</th>
				    <th>class</th>
					
					<th>Destination</th>
					<th>Fare</th>
					<th>Vehicle Type</th>
					<th>Vehicle License No</th>
				   </thead>
				   <tbody align="center">';		
	
	//query to get details
   $queryStudentNameClass = "
		SELECT student_user.Name, student_user.admission_no,
		 class_index.class_name, student_user.sId,vehicle_details.vehicle_id
		FROM student_user
		
		INNER JOIN user_vehicle_details
		ON student_user.sId = user_vehicle_details.uId
		
		INNER JOIN class
		ON student_user.sId = class.sId
		
		INNER JOIN class_index
		ON class.classId = class_index.cId
		
		INNER JOIN vehicle_details 
		ON vehicle_details.vehicle_id= user_vehicle_details.vehicle_no1
		
		WHERE user_vehicle_details.uId=".$_SESSION['user_id']." AND user_vehicle_details.priv=3
		";
	                
	$exe_details=mysql_query($queryStudentNameClass);
	while($fetch_details=mysql_fetch_array($exe_details))
	             
			{       $sid=$fetch_details['sId'];
					
				 $get_cost="SELECT *,vehicle_details.vehicle_type ,vehicle_details.license_no
				            FROM destination_route
							
							INNER JOIN user_vehicle_details 
							ON user_vehicle_details.dId=destination_route.dId	
							AND user_vehicle_details.route_id=destination_route.rId
							
							INNER JOIN vehicle_details 
							ON vehicle_details.route_id=user_vehicle_details.route_id	
							
					    WHERE user_vehicle_details.uId=".$_SESSION['user_id']."
					";
							
				$exe_cost=mysql_query($get_cost);
				$fetch_cost=mysql_fetch_array($exe_cost);
				 
				 
	        echo'<tr>
			     <td>'.$fetch_details[1].'</td>
				 <td>'.$fetch_details[0].'</td>
				 <td>'.$fetch_details[2].'</td>
				 
				 <td>'.$fetch_cost['d_name'].'</td>
				 <td>'.$fetch_cost['cost'].'</td>
				 <td>'.$fetch_cost['vehicle_type'].'</td>
				  <td>'.$fetch_cost['license_no'].'</td>
			   </tr>
			';
			}
	 echo'
	 </tbody>
    </table>	
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';			
}

function driver_maintenance_transport_details()
{
	
	echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>All Visitor Details</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid"> 	  	
	';	
	echo'
	<div class="span4">
    <a href="admin_add_transport_maintenance_details.php"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span> <em><b style="color:green">Add Maintenance Details</b></em> </div>
    <div class="breaks"><span></span></div>
	
     <a href="admin_transport_add_driver_details.php"><div class="shoutcutBox"> <span class="ico color chat-exclamation"></span>  <em><b style="color:green">Add Driver Details</b></em> </div>
    <div class="breaks"><span></span></div>
   </div><!-- span4 column-->
                                          
';
		 echo'  
		 </tbody>
		 </table>                        
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->

	   </div><!-- row-fluid -->
';				
	
}

//maintenance details...
function maintenance_transport_details()
{
echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>Transport Maintenance Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';	
	
		
	if(isset($_GET['done']))
		{
			echo "<h5 align=\"center\" style='color:green'>Added Successfully!!</h5>";
		}	
		
		if(isset($_GET['error']))
		{
			echo "<h5 align=\"center\" style='color:red'>Please select vehicle!!</h5>";
		}	
	//get all the routes from vehicle details
 $get_routes=
"SELECT  `vehicle_id`,`license_no`
FROM  vehicle_details";
$exe_routes=mysql_query($get_routes);
	echo '
<form id="validation_demo" action="transport/add_transport_maintenance_details.php" method="post">
<div class="section">
<label>Select Vehicle No.</label>
<div>
<select class="chzn-select" data-placeholder="Select Vehicle" name="vehicle_id" >
<option value=""></option>';

while($fetch_routes=mysql_fetch_array($exe_routes))
{
echo '<option value="'.$fetch_routes[0].'">'.$fetch_routes['license_no'].'</option>';		
}
echo '
</select>
</div>
</div>
<div class="section">
<label>Maintenance Type</label>
<div>
<input type="text" name="type" class="validate[required] medium" autocomplete="off"/>
</div>
</div>
<div class="section numericonly">
<label>Amount</label>
<div>
<input type="text" name="amount" class="validate[required] medium" autocomplete="off"/>
</div>
</div>
 <div class="section">
<label>Date </label>
<div>
<input type="tel" class="birthday " id="from_date" name="date" autocomplete="off"/>
</div>
</div>
<div class="section last">
 <div><button class="uibutton submit" rel="1" type="submit"  >Save</button>
 <button class="uibutton special"type="reset"  >Reset</button>
 <a href="admin_transport_maintenance_session.php"><button class="uibutton confirm" type="button">View</button></a>
 
</div>
</div>	
</form>';

 echo'
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';			
}

function  session_maintenance_transport_details()
{	
echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>Select session</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		  <div class="row-fluid">';		
	
	//get time 
	$time_offset ="525"; // Change this to your time zone
	$time_a = ($time_offset * 120);
	$today = date("jS F Y");	 
	echo '<h5 style="color:green" align="center">'.$today.'</h5>';	
	echo'
	<form action="admin_view_transport_maintain_details.php" method="get" name="session" >							
	<label><h4 align="center">select Session<h4><small></label>   
	<div align="center">											
	<select  data-placeholder="Choose  Session..." class="chzn-select" tabindex="2" name="session">        
	<option value=""></option>'; 								
	//query to get month from `fee_generated_sessions`  table
	 $get_session="
		SELECT `sId`,`session_name`
		FROM `session_table` ";
	$get_session_exe=mysql_query($get_session);
	while($get_session_fetch=mysql_fetch_array($get_session_exe))
	     {
			$sid=$get_session_fetch['sId'];	
			$session_name=$get_session_fetch['session_name'];
			echo'
	        <option value="'.$sid.'">'. $session_name.'</option>';
	      } 							 
	echo'
	</select>       
	</div>									                           	       				
	<br></br>			   																
	<div class="section last" align="center">
	<button  class="uibutton submit_form" >Go</button>
	</div>
	</form>
	';
	
  echo'                        
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	   </div><!-- row-fluid -->
';						
}
//view maintenance details
function  view_maintenance_transport_details()
{
echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>View Transport Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';	
	$session_id=$_GET['session'];
	
	if($session_id=="")
	{
		header('location:admin_transport_maintenance_session.php');
	}
	echo' <a href="admin_transport_maintain_details_monthly.php"><button class="uibutton special" type="button">View Monthly Details</button></a>';
	
	
	echo'<table  class="table table-bordered table-striped" id="dataTable" >
                              
				   <thead style="color:brown"> 
				   <tr>
				   <th>Vehicle License No</th>
				   <th>Vehicle Type</th>
				    <th>Maintenance Type</th>
					<th>Amount</th>
					<th>Date</th>
				   </thead>
				   <tbody align="center">';	
			$total=0;	
			
//query to get maintence details			   
	 $get_cost="SELECT *,vehicle_details.vehicle_type ,vehicle_details.license_no,transport_maintenance_details.type,
	 transport_maintenance_details.amount,transport_maintenance_details.date
				            FROM transport_maintenance_details
							
							INNER JOIN vehicle_details
							ON vehicle_details.vehicle_id=transport_maintenance_details.vehicle_id
							WHERE session_id=".$session_id."";
													
	$exe_cost=mysql_query($get_cost);
	while($fetch_cost=mysql_fetch_array($exe_cost))						
	      {    $date=date('d-m-Y',strtotime($fetch_cost['date']));
			 echo'
			 <tr>
			 <td>'.$fetch_cost['license_no'].'</td>
			 <td>'.$fetch_cost['vehicle_type'].'</td>
			 <td>'.$fetch_cost['type'].'</td>
			 <td>'.$fetch_cost['amount'].'</td>
			 <td>'. $date.'</td> ';
			 
			 $total=$total+ $fetch_cost['amount'];
			 }		 
echo'
</tbody>
</table>
<h5 align="center" style="color:red">Total Rs.:&nbsp;'.$total.'</h5>
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';		
}
//view monthly details
function monthly_maintenance_transport_details()
{
echo '<div class="row-fluid">                          
          <div class="span12  widget clearfix">
	      <div class="widget-header">
	      <span><i class="icon-align-center"></i>Month Wise Visitor Details</span>
	      </div><!-- End widget-header -->		  
	      <div class="widget-content"><br />
		   	';	
		echo' 
		<form>
		<div class="section ">
        <label>Select Year<small></small></label>           
		 <div >
		 <select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year"  >        
		 <option value=""></option>'; 								
			//query to get year		
		$get_visitor_year="SELECT DISTINCT `year`
	                       FROM  `dates_d`
				   ";
	$execute_visitor_year=mysql_query($get_visitor_year);
	while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
	      {   
			 $fetch_year=$fetch_visitor_year['year'];
	
	echo'
		     <option value="'.$fetch_year.'">'. $fetch_year.'</option>';
		  }
				  					   
	 echo'  </select> 
	        </div>
		   </div> 
				   ';				
			echo' 
		 <div class="section ">
         <label>Select Month<small></small></label>              
		 <div >
		 <select  data-placeholder="Choose  month..." class="chzn-select" tabindex="2" id="month" >        
			    <option value=""></option>'; 								
			//query to get month and month of year		
		$get_visitor_month="SELECT DISTINCT `month` ,`month_of_year`
	                       FROM  `dates_d`
				   ";
	$execute_visitor_month=mysql_query($get_visitor_month);
	while($fetch_visitor_month=mysql_fetch_array($execute_visitor_month))//looping for getting month
	      {   
			 $fetch_month=$fetch_visitor_month['month'];
	
	echo'
		     <option value="'.$fetch_visitor_month[1].'">'. $fetch_month.'</option>';
		  }
				  					   
echo'    </select> 
	     </div> 
		 </div> 
		</form>  ';			
		 echo'  
		 <br>	
         <div class="section last" align="center">
         <div><button class="uibutton submit" rel="1" type="button" onclick="show_data_month()" >Go</button></div></div>
	  <span id="data_month"></span>
	  </div><!-- row-fluid column-->
	  </div><!--  end widget-content -->
	  </div><!-- widget  span12 clearfix-->
	 ';				
}

//add driver details
function add_driver_transport_details()
{
echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>Driver Transport</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';	
	echo'<a href="admin_transport_add_driver.php"><button class="uibutton icon add" type="button">Add Driver Details</button></a>
	<a href="admin_transport_view_driver_details.php"><button class="uibutton confirm" type="button">View Driver Details</button></a>
<a href="admin_view_transport_driver_allocate.php"><button class="uibutton special"type="button">View Allocate Driver</button></a>	
	';
		
	if(isset($_GET['add']))
		{
			echo "<h5 align=\"center\" style='color:green'>Allocate Successfully!!</h5>";
		}	
		if(isset($_GET['err']))
		{
			echo "<h5 align=\"center\" style='color:red'>Please Select Details!!</h5>";
		}	
			if(isset($_GET['exist']))
		{
			echo "<h5 align=\"center\" style='color:red'>Already Allocated!!</h5>";
		}	
		
	
echo '
<form id="validation_demo" action="transport/add_transport_driver_details.php" method="post">
<div class="section">
<label>Select Vehicle No.</label>
<div>
<select class="chzn-select" data-placeholder="Select Vehicle" name="vehicle_id" >
<option value=""></option>';
  $get_routes=
	  "SELECT  *
	  FROM  vehicle_details";
	  $exe_routes=mysql_query($get_routes);
		 

	
	  while($fetch_routes=mysql_fetch_array($exe_routes))
	     {     $get_details="SELECT v_id,d_id
             FROM transport_driver_details
			 WHERE v_id = ".$fetch_routes['vehicle_id']."
			";		 
        $exe_details=mysql_query($get_details);
		$fetch_details=mysql_fetch_array($exe_details);
		/*{
		$vid=$fetch_details['v_id'];
		$did=$fetch_details['d_id'];
			 */
		    $v_id=$fetch_routes[0];
		   if($fetch_details)
		  {
	echo '<option value="'.$fetch_routes[0].'" style="color:red"><b>'.$fetch_routes['license_no'].'('.$fetch_routes['vehicle_type'].')</b></option>';	
		  }
		  
		  else
		  {
			echo '<option value="'.$fetch_routes[0].'" ><b>'.$fetch_routes['license_no'].'('.$fetch_routes['vehicle_type'].')</b></option>';  
			  
		  }
		  
		 
		}
echo '
</select>
</div>
</div>
<div class="section">
<label>Driver Name</label>
<div>
<select class="chzn-select" data-placeholder="Select Driver" name="driver_id" >
<option value=""></option>';
	
	
	//query to get maintence details			   
	 $get_cost="SELECT *
	            FROM driver_details ";
													
	$exe_cost=mysql_query($get_cost);
	
	
	while($fetch_cost=mysql_fetch_array($exe_cost))		
	   {    
	     $get_details_did="SELECT *
             FROM transport_driver_details
			  WHERE d_id=".$fetch_cost['id']."";	 
$exe_details_did=mysql_query($get_details_did);
$fetch_details_did=mysql_fetch_array($exe_details_did);
		
		
		if($fetch_details_did)
		{
	 echo '<option value="'.$fetch_cost[0].'" style="color:red"><b>'.$fetch_cost['d_name'].'('.$fetch_cost[2].')</b></option>';		
			
		}
		else
		{
		  
 echo '<option value="'.$fetch_cost[0].'"><b>'.$fetch_cost['d_name'].'('.$fetch_cost[2].')</b></option>';		
	   }
	   }
		
echo '
</select>
</div>
</div></br>
<div class="section last">
 <div><button class="uibutton submit" rel="1" type="submit"  >Save</button>

 
</div>
</div>	
</form>';

 echo'
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';		

}

//add driver 
function add_driver_details()
{
echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>Add Driver Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<a href="admin_transport_add_driver_details.php"><button class="uibutton icon answer">Back</button></a>';	
	
	if(isset($_GET['exist']))
		{
			echo "<h5 align=\"center\" style='color:red'>License No. Already Exist!!</h5>";
		}
	if(isset($_GET['done']))
		{
			echo "<h5 align=\"center\" style='color:green'>Added Successfully!!</h5>";
		}	
		
		if(isset($_GET['error']))
		{
			echo "<h5 align=\"center\" style='color:red'>Please Try Again!!</h5>";
		}
	echo'	
   <form id="validation_demo" action="transport/add_driver_details.php" method="post">              			 
  <div class="section ">
  <label>Name<small></small></label>   
   <div> 
  <input type="text" class="validate[required] medium" name="name" autocomplete="off" />
   </div>
   </div>	
    <div class="section ">
  <label>Driver License No.<small></small></label>   
   <div> 
 <input type="text" class="validate[required] medium" name="license_no" autocomplete="off" />
   </div>
   </div>
 <div class="section ">
  <label>Experience<small></small></label>   
   <div> 
 <input type="text" class=" medium" name="experience" autocomplete="off"/>
   </div>
   </div>
 <div class="section numericonly">
 <label>Contact<small></small></label>   
 <div> 
 <input  type="text" name="contact" class="validate[required] medium" maxlength="11" autocomplete="off" >
 </div>
 </div>
 <div class="section">
 <label> Address<small></small></label>
 <div >
 <textarea name="address"  class="medium"  cols="" rows=""></textarea>
 </div>                                                  
 </div>
<div class="section ">
  <label>Conductor Name<small></small></label>   

   <div> 
  <input type="text" class="validate[required] medium" name="c_name" autocomplete="off" />
   </div>
   </div>	
<div class="section numericonly">

 <label>Conductor Contact<small></small></label>   
 <div> 
 <input  type="text" name="c_con" class="validate[required] medium" maxlength="11" autocomplete="off" >
 </div>
</div>
<div class="section ">
  <label>Incharge Name<small></small></label>   
   <div> 
  <input type="text" class="validate[required] medium" name="i_name" autocomplete="off" />
   </div>
   </div>	
<div class="section numericonly">
 <label>Incharge Contact<small></small></label>   
 <div> 
 <input  type="text" name="i_con" class="validate[required] medium" maxlength="11" autocomplete="off" >
 </div>
 </div>
<div class="section last">
 <div><button class="uibutton submit" rel="1" type="submit"  >Save</button>
 <button class="uibutton special" type="reset"  >Reset</button> 
</div>
</div>									
</form>     
';
 echo'
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';		
	
}
//view allocate driver details
function view_allocate_driver_details()
{
	
echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>View Allocate  Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<a href="admin_transport_add_driver_details.php"><button class="uibutton icon answer">Back</button></a>';	
     if(isset($_GET['update_dd']))
		{
			echo "<h5 align=\"center\" style='color:green'>Successfully Updated!!</h5>";
		}
		 if(isset($_GET['update_not']))
		{
			echo "<h5 align=\"center\" style='color:red'>  NOT Updated!!</h5>";
		}
		if(isset($_GET['delete_d']))
		{
			echo "<h5 align=\"center\" style='color:red'>Successfully Deleted!!</h5>";
		}
	echo'<table  class="table table-bordered table-striped" id="dataTable" >
                              
				   <thead style="color:brown"> 
				   <tr>
				   <th>Name</th>
				   <th>Driver License No</th>
				    <th>Vehicle Type</th>
					<th>Route</th>
					<th>Vehicle License No.</th>
					<th>Action</th>
				   </thead>
				   <tbody align="center">';	


$get_details="SELECT transport_driver_details.*,vehicle_details.*,driver_details.*,routes_data.*
             FROM  transport_driver_details
			 
			 INNER JOIN vehicle_details
			 ON  transport_driver_details.v_id=vehicle_details.vehicle_id
			  
			 INNER JOIN driver_details
			 ON  transport_driver_details.d_id=driver_details.id
			 
			 INNER JOIN routes_data
			 ON  routes_data.route_id=vehicle_details.route_id ";

      $exe_details=mysql_query($get_details);
	  while($fetch_details=mysql_fetch_array($exe_details))
	     {    $did=$fetch_details['d_id'];
			  $vid=$fetch_details['vehicle_id'];
		   echo'   
		       <tr>
			   <td>'.$fetch_details['d_name'].'</td>
			   <td>'.$fetch_details['d_license_no'].'</td>
			   <td>'.$fetch_details['vehicle_type'].'</td>
			   <td>'.$fetch_details['main_route'].'</td>
			   <td>'.$fetch_details['license_no'].'</td>
			   
	<td><a href="admin_transport_edit_allocate_driver_details.php?did='.$did.'&vid='.$vid.'" class="table-icon edit" title="Edit">
			  <img src="images/icon/icon_edit.png"></a>
			<a href="transport/delete_allocate_driver_details.php?did='.$did.'&vid='.$vid.'" class="table-icon delete" title="Delete">
			<img src="images/icon/icon_delete.png"></a></td></td>
			   </tr>';	 			 	 
	   }     
echo'
</tbody>
</table>
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';				
}

//edit allocate driver details
function edit_allocate_driver_details()
{
	
echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>Driver Transport</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';	
		
$did=$_GET['did'];	
$vid=$_GET['vid'];	

 $get_vehicle_details="SELECT *
                      FROM  vehicle_details 
					  WHERE vehicle_id=".$vid."";

$exe_vehicle_details=mysql_query($get_vehicle_details);
$fetch_vehicle_details=mysql_fetch_array($exe_vehicle_details);
$vehicle_type=$fetch_vehicle_details['vehicle_type'];
$license_no=$fetch_vehicle_details['license_no'];
$vehicle_id=$fetch_vehicle_details['vehicle_id'];	
	
//get driver details
$get_driver_details="SELECT *
                      FROM  driver_details 
					  WHERE id=".$did."";

$exe_driver_details=mysql_query($get_driver_details);
$fetch_driver_details=mysql_fetch_array($exe_driver_details);
$dname=$fetch_driver_details['d_name'];
$dlicense_no=$fetch_driver_details['d_license_no'];
$driver_id=$fetch_driver_details['id'];	
	
echo '
<form id="validation_demo" action="transport/update_allocate_driver_details.php" method="post">
<div class="section">
<label>Select Vehicle No.</label>
<div>
<select class="chzn-select" data-placeholder=" Select Vehicle" name="vehicle_id" >
<option value="'.$vehicle_id.'" style="color:red">'.$license_no.'('.$vehicle_type.')</option>';


//get all the routes from vehicle details
 $get_routes=
"SELECT  *
FROM  vehicle_details
WHERE vehicle_id <> ".$vehicle_id."";
$exe_routes=mysql_query($get_routes);
while($fetch_routes=mysql_fetch_array($exe_routes))
   {     

     $v_id=$fetch_routes[0];
	 
	 
	 $get_details_did="SELECT *
             FROM transport_driver_details
			  WHERE v_id=".$v_id."";	 
$exe_details_did=mysql_query($get_details_did);
$fetch_details_did=mysql_fetch_array($exe_details_did);
		
	if($fetch_details_did)
	{
echo '<option value="'.$fetch_routes[0].'" style="color:red">'.$fetch_routes['license_no'].'('.$fetch_routes['vehicle_type'].')</option>';	
      
    }
	
	else
	{
		echo '<option value="'.$fetch_routes[0].'">'.$fetch_routes['license_no'].'('.$fetch_routes['vehicle_type'].')</option>';	
      
		}
	
	
	
	
	}
echo '
</select>
</div>
</div>
<div class="section">
<label>Driver Name</label>
<div>
<select class="chzn-select" data-placeholder="Select Driver" name="driver_id" >
<option value="'.$driver_id.'" style="color:red">'.$dname.'('.$dlicense_no.')</option>';

//query to get maintence details			   
	 $get_cost="SELECT *
	            FROM driver_details 
				WHERE id <> ".$driver_id."";
													
	$exe_cost=mysql_query($get_cost);
	while($fetch_cost=mysql_fetch_array($exe_cost))		
	{
		$d_id=$fetch_cost[0];	
		
		 $get_details="SELECT v_id,d_id
             FROM transport_driver_details
			 WHERE d_id = ".$d_id."
			";		 
        $exe_details=mysql_query($get_details);
		$fetch_details=mysql_fetch_array($exe_details);
		if($fetch_details)
		{
      echo '<option value="'.$fetch_cost[0].'" style="color:red">'.$fetch_cost['d_name'].'('.$fetch_cost[2].')</option>';			  
   }
   
   else{
	    echo '<option value="'.$fetch_cost[0].'">'.$fetch_cost['d_name'].'('.$fetch_cost[2].')</option>';	
	   
	   
	   }
   
   
   
	}
echo '
</select>
</div>
</div></br>
<div class="section last">
 <div><button class="uibutton submit" rel="1" type="submit"  >Save</button>
</div>
</div>	
</form>';

 echo'
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';		
	
}
//view driver details
function view_driver_details()
{
	
echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>View Transport Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	<a href="admin_transport_add_driver_details.php"><button class="uibutton icon answer">Back</button></a>';	
if(isset($_GET['update']))
		{
			echo "<h5 align=\"center\" style='color:green'>Successfully Updated!!</h5>";
		}
		
		if(isset($_GET['delete']))
		{
			echo "<h5 align=\"center\" style='color:red'>Successfully Deleted!!</h5>";
		}
	echo'<table  class="table table-bordered table-striped" id="dataTable" >
                              
				   <thead style="color:brown"> 
				   <tr>
				   <th>Name</th>
				   <th>Driver License No</th>
				    <th>Experience</th>
					<th>Contact</th>
					<th>Address</th>
					<th>Action</th>
				   </thead>
				   <tbody align="center">';			
//query to get maintence details			   
	 $get_cost="SELECT *
	            FROM driver_details";
													
	$exe_cost=mysql_query($get_cost);
	while($fetch_cost=mysql_fetch_array($exe_cost))						
	      {   $did=$fetch_cost['id'];
		     
			 echo'
			 <tr>
			 <td>'.$fetch_cost['d_name'].'</td>
			 <td>'.$fetch_cost['d_license_no'].'</td>
			 <td>'.$fetch_cost['experience'].'</td>
			 <td>'.$fetch_cost['contact'].'</td>
			  <td>'.$fetch_cost['address'].'</td>
			  <td><a href="admin_transport_edit_driver_details.php?did='.$did.'" class="table-icon edit" title="Edit">
			  <img src="images/icon/icon_edit.png"></a>
			<a href="transport/delete_driver_details.php?did='.$did.'" class="table-icon delete" title="Delete">
			<img src="images/icon/icon_delete.png"></a></td>';
		}		 
echo'
</tbody>
</table>
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';			
}

//edit driver details
function  edit_driver_details()
{
	
echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>Add Driver Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';	
	$did=$_GET['did'];	
	//query to get maintence details			   
	 $get_cost="SELECT *
	            FROM driver_details
				WHERE id=".$did."";
													
	$exe_cost=mysql_query($get_cost);
	$fetch_cost=mysql_fetch_array($exe_cost);
	         $dname=$fetch_cost['d_name'];
			 $d_license=$fetch_cost['d_license_no'];
			 $experience=$fetch_cost['experience'];
			 $contact=$fetch_cost['contact'];
			 $address=$fetch_cost['address']	;
		
	echo'	
   <form id="validation_demo" action="transport/update_driver_details.php" method="post">  
     <input type="hidden" name="did" value="'.$did.'"/>            			 
  <div class="section ">
  <label>Name<small></small></label>   
   <div> 
  <input type="text" class="validate[required] medium" name="name" autocomplete="off" value="'.$dname.'"/>
   </div>
   </div>	
    <div class="section ">
  <label>Driver License No.<small></small></label>   
   <div> 
 <input type="text" class="validate[required] medium" name="license_no" autocomplete="off" value="'.$d_license.'"/>
   </div>
   </div>
 <div class="section ">
  <label>Experience<small></small></label>   
   <div> 
 <input type="text" class=" medium" name="experience" autocomplete="off" value="'.$experience.'"/>
   </div>
   </div>
 <div class="section numericonly">
 <label>Contact<small></small></label>   
 <div> 
 <input  type="text" name="contact" class="validate[required] medium" maxlength="11" autocomplete="off" value="'.$contact.'">
 </div>
 </div>
 <div class="section">
 <label> Address<small></small></label>
 <div >
 <textarea name="address"  class="medium"  cols="" rows="">'.$address.'</textarea>
 </div>                                                  
 </div>

<div class="section last">
 <div><button class="uibutton submit" rel="1" type="submit"  >Save</button>
 
</div>
</div>									
</form>  ';
 echo'
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';			
}
//edit allocate student details
function edit_allocate_student_details()
{
echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>Edit Allocated Student</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';	
	
	
	
	$did=$_GET['did'];	
	$rid=$_GET['route_id'];	
	$vid=$_GET['vid'];		
	$sid=$_GET['sid'];		
	
/*if(isset($_GET['assignedRoute']))
		{
			echo "<h5 align=\"center\" style='color:green'>Allocate Transport</h5>";
		}	
	if(isset($_GET['alreadyassigned']))
		{
			echo "<h5 align=\"center\" style='color:red'>Already Assigned!!</h5>";
		}	
			
	if(isset($_GET['notavailable']))
		{
			echo "<h5 align=\"center\" style='color:red'>Seat not Available in this vehicle!!</h5>";
		}	
*/		

	if(isset($_GET['update']))
		{
			echo "<h5 align=\"center\" style='color:green'>Updated Successfully!!</h5>";
		}	

if(isset($_GET['error']))
		{
			echo "<h5 align=\"center\" style='color:red'>Not Updated!!</h5>";
		}			
$get_name="SELECT * 
          FROM student_user
		  WHERE sId=".$sid."";
$exe_name=mysql_query($get_name);
$fetch_name=mysql_fetch_array($exe_name);
$name_stu=$fetch_name['Name'];


//get all the routes from routes_data
$get_routes_id=
"SELECT main_route,route_id
FROM routes_data
WHERE route_id=".$rid." ";
$exe_routes_id=mysql_query($get_routes_id);
$fetch_routes_id=mysql_fetch_array($exe_routes_id);
$route_id=$fetch_routes_id['route_id'];	
$route_name=$fetch_routes_id['main_route'];	

//get all the routes from routes_data
$get_routes=
"SELECT DISTINCT main_route,route_id
FROM routes_data
";
$exe_routes=mysql_query($get_routes);
echo 
'<form action="transport/update_allocate_vehicle_student.php" method="get">
<input type="hidden" name="sid" value="'.$sid.'"/>
<input type="hidden" name="sname" value="'.$name_stu.'"/>
<div class="section">
<label>Name</label>
<div>
<h5>'.$name_stu.'</h5>
</div>
</div>

<div class="section">
<label>Route</label>
<div>
<select class="chzn-select" name="route_sel" id="select_route" onchange="get_edit_destination(this.val);" data-placeholder="Select route">
<option value=""></option>
';

while($fetch_routes=mysql_fetch_array($exe_routes))
{
echo '<option value="'.$fetch_routes[1].'" id="'.$fetch_routes[0].'">'.$fetch_routes[0].'</option>';	
	
}
echo '
</select>
</div>
</div>
<span id="destination_edit"></span>

</form>';
	

 echo'
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';		

}


//manager.....................
function  manager_view_transport_details()
{
	
echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>View Transport Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';		
	if(isset($_GET['delete_d']))
		{
			echo "<h5 align=\"center\" style='color:green'>Delete Allocated Student</h5>";
		}	
	echo'<table  class="table table-bordered table-striped" id="dataTable" >
                              
				   <thead style="color:brown"> 
				   <tr>
				   <th>Serial No.</th>
				   <th>Vehicle Type</th>
				    <th>Main Route</th>
					<th>Total Seat</th>
					<th>Available Seat</th>
				  
				   </thead>
				   <tbody align="center">';	
				   
	  		$sno=0;	   
		//get all the values
 $get_vehicle_details=
"SELECT *
FROM vehicle_details";
$exe_details=mysql_query($get_vehicle_details);
while($fetch_details=mysql_fetch_array($exe_details))
{      $sno++;
//get route on the route id
   $get_route=
"SELECT *
FROM routes_data
where route_id=".$fetch_details[2]."";
$exe_data=mysql_query($get_route);
$fetch_data=mysql_fetch_array($exe_data);	

//count assign transport student
 $count="SELECT COUNT(vehicle_no1) AS route
        FROM user_vehicle_details
		WHERE vehicle_no1=".$fetch_details[0]."";
$exe_count=mysql_query($count);
$fetch_count=mysql_fetch_array($exe_count);
$seats_left=$fetch_details[5]-$fetch_count[0];
    
echo '<tr>
<td>'.$sno.'</td>
<td>'.$fetch_details[1].'</td>
<td><a href="manager_view_student_new_transport_details.php?rid='.$fetch_data[0].'&vehicle_id='.$fetch_details[0].'&license_no='.$fetch_details[3].'">'.$fetch_data['main_route'].'</td></a>
<td>'.$fetch_details[5].'</td>
<td>'.$seats_left.'</td>
'
;		   				
}
     echo'
	 </tbody>
	 </table>	
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';			

	
}
function   manager_view_stu_transport_details()
{
echo '	 
	<div class="row-fluid">
	<div class="widget  span12 clearfix">
	<div class="widget-header">
	<span><i class="icon-home"></i>View Transport Details</span>
	</div><!-- End widget-header -->	
	<div class="widget-content">
	';	
	
	$vehicle_id=$_GET['vehicle_id'];
	$license_no=$_GET['license_no'];
	echo'<table  class="table table-bordered table-striped" id="dataTable" >
                              
				   <thead style="color:brown"> 
				   <tr>
				   <th>Admission No.</th>
				   <th>Name</th>
				    <th>class</th>
					
					<th>Destination</th>
					<th>Fare</th>
					<th>License No.</th>
				
				   </thead>
				   <tbody align="center">';		
	
	//query to get details
  $queryStudentNameClass = "
		SELECT student_user.Name, student_user.admission_no,
		 class_index.class_name, student_user.sId,vehicle_details.vehicle_id,vehicle_details.license_no
		FROM student_user
		
		INNER JOIN user_vehicle_details
		ON student_user.sId = user_vehicle_details.uId
		
		INNER JOIN class
		ON student_user.sId = class.sId
		
		INNER JOIN class_index
		ON class.classId = class_index.cId
		
		INNER JOIN vehicle_details 
		ON vehicle_details.vehicle_id= user_vehicle_details.vehicle_no1
		
		WHERE vehicle_details.route_id = ".$_GET['rid']." AND vehicle_details.vehicle_id=".$vehicle_id." AND vehicle_details.license_no=".$license_no."
		";
	  $total=0;              
	$exe_details=mysql_query($queryStudentNameClass);
	while($fetch_details=mysql_fetch_array($exe_details))
	             
			{       $sid=$fetch_details['sId'];
					
				 $get_cost="SELECT *
				            FROM destination_route
							
							INNER JOIN user_vehicle_details 
							ON user_vehicle_details.dId=destination_route.dId	
					     WHERE user_vehicle_details.uId=".$fetch_details[3]."
					 AND user_vehicle_details.route_id=".$_GET['rid']."
					";
							
				$exe_cost=mysql_query($get_cost);
				$fetch_cost=mysql_fetch_array($exe_cost);
				 $did=$fetch_cost['dId'];
				 
	        echo'<tr>
			     <td>'.$fetch_details[1].'</td>
				 <td>'.$fetch_details[0].'</td>
				 <td>'.$fetch_details[2].'</td>
				 
				 <td>'.$fetch_cost['d_name'].'</td>
				 <td>'.$fetch_cost['cost'].'</td>
				  <td>'.$fetch_details['license_no'].'</td>
				  
			   </tr>';
			    $total=$total+ $fetch_cost['cost'];
			}
	 echo'
	 </tbody>
    </table>	
	<h5 align="left" style="color:red">Total:&nbsp;'.$total.'</h5>
	</div><!-- row-fluid column-->
	</div><!--  end widget-content -->
	</div><!-- widget  span12 clearfix-->
	</div><!-- row-fluid -->
	';			
	
	
	
	
	
	
}
?>
