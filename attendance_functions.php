<?php

//function to call teacher_attendance_weekly
function teacher_attendance_weekly()
{

echo '<div class="row-fluid">	  
<div class="span12  widget clearfix"> 
<div class="widget-header">
<span><i class="icon-align-center"></i>Weekly Attendance</span>
</div><!-- End widget-header -->
<div class="widget-content">
        '; 
        //query to get year and date id
          $get_visitor_year="SELECT DISTINCT `year`
                               FROM  `dates_d`
                                          ";
        echo' 
                <form action="teacher_attendance_weekly_class.php" method="get" class="demo_validation">
                <div class="section">
        <label>Select Year<small></small></label>           
                 <div >
                 <select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" name="year" >        
                 <option value=""></option>'; 								
                        //query to get year		

        $execute_visitor_year=mysql_query($get_visitor_year);
        while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
              {   
                         $fetch_year=$fetch_visitor_year['year'];		

        echo'
                     <option value="'.$fetch_year.'">'. $fetch_year.'</option>';
                  }

         echo'  </select> 
                </div>
                   </div> 
                                   ';	
                                  $get_visitor_month="SELECT DISTINCT `month`,`month_of_year`
                                           FROM  `dates_d`
                                                       WHERE `year`=". $fetch_year." ";			
                        echo' 
                 <div class="section ">
         <label>Select Month<small></small></label>              
                 <div >
                 <select  data-placeholder="Choose  month..." class="chzn-select" tabindex="2" id="month" name="month">        
                            <option value=""></option>'; 								
                        //query to get month and month of year		

        $execute_visitor_month=mysql_query($get_visitor_month);
        while($fetch_visitor_month=mysql_fetch_array($execute_visitor_month))//looping for getting month
              {   

                         $fetch_month=$fetch_visitor_month['month'];
                         $fetch_month_year=$fetch_visitor_month['month_of_year'];

        echo'
                     <option value="'.$fetch_month_year.'">'. $fetch_month.'</option>';
                  }

echo'    </select> 
             </div>  
                 </div> 	  
<div class="section numericonly ">
  <label>FROM <small></small></label>   
  <div> 
  <input name="from_date" id="from_date" align="center" type="text"  class="validate required medium" maxlength="2">
  </div>
  </div>
  <div class="section numericonly">
  <label>TO<small></small></label>   
  <div> 
   <input name="to_date" id="to_date" align="center" type="text" class=" validate required medium" maxlength="2">
  </div>
  </div> 
 <div class="section last">
 <div><button  class="uibutton submit_form" value="submit" >Go</button>
  </div>
  </div>		 
</form> 
'; 	 

        echo'	                   
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
';




        }

//function to select year for manager
function select_year()
{	
$id_sel=$_GET['id_sel'];
echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select year</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
          <ol class="rectangle-list">';
          //get all the years
          $get_year=
          "SELECT DISTINCT(year)
          FROM dates_d";
          $exe_years=mysql_query($get_year);
          while($fetch_years=mysql_fetch_array($exe_years))
          {
                 echo '<li><a href="select_month_for_year.php?year='.$fetch_years[0].'&&id_sel='.$id_sel.'">'.$fetch_years[0].'</a></li>'; 

          }
          echo'
          </ol>

          </div>
          </div>
          </div>';	
          //href="manager_view_absent_graph.php?year='.$fetch_years[0].'
}
//select month for the following year
function select_month_for_year()
{
$id_sel=$_GET['id_sel'];	
echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select year</span>
      </div><!-- End widget-header -->
      <div class="widget-content">	

         <ol class="rounded-list">';
          //select all the distinct years
          $get_months=
          "SELECT DISTINCT month, month_of_year
          FROM dates_d
          WHERE year=".$_GET['year']."";
          $exe_get_months=mysql_query($get_months);
          while($fetch_all_months=mysql_fetch_array($exe_get_months))
          {
                  if($id_sel==1)
                  {
                        echo '<li><a href="manager_view_absent_first_graph.php?year='.$_GET['year'].'&&month='.$fetch_all_months[1].'">'.$fetch_all_months[0].'</a></li>';   

                  }
                  else
                  {
                 echo '<li><a href="manager_view_absent_graph.php?year='.$_GET['year'].'&&month='.$fetch_all_months[1].'">'.$fetch_all_months[0].'</a></li>';   
                  }

          }
          echo '
          </ol></div>
          </div>
          </div>
                ';


}
//function to show graph to manager for the foll dates
function attendance_date_manager_graph()
{
$year=$_GET['year'];
$month=$_GET['month'];
echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Absent details</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';


echo "<script type='text/javascript'>
$(function () {
    var chart;
    $(document).ready(function() {

        var colors = Highcharts.getOptions().colors,
            categories = [
                        ";
                        $did=array();
                        $date=array();
                        $day=array();
                        $count=0;
                        //get all the dates between the years
                         $get_dates=
                        "SELECT date , date_id ,day
                        FROM dates_d
                        WHERE month_of_year=".$month."
                        AND year=".$year."
                        LIMIT 15,30";
                        $exe_dates=mysql_query($get_dates);
                        while($fetch_dates=mysql_fetch_array($exe_dates))
                        {
                        $date[$count]=$fetch_dates[0];	
                        $did[$count] = $fetch_dates[1];
                        $day[$count]=$fetch_dates[2];	
                        echo "'$fetch_dates[0]',";	
                        $count++;
                        }

                        echo "],
            name = 'Absent Record',
            data = [
                                ";
                                //get all the absenties list on the followin date id
                                for($i=0;$i<$count;$i++)
                                {
                            $get_id=
                                "SELECT COUNT(sId)
                                FROM attendance_date_comment
                                WHERE dId =".$did[$i]."";
                                $exe_get_count=mysql_query($get_id);
                                $fetch_data_stu=mysql_fetch_array($exe_get_count);
                                $total_count_sid=$fetch_data_stu[0];



                                echo "
                                {
                    y: ".$total_count_sid.",
                    color: colors[2],
                    drilldown: {
                        name: '(".$day[$i].")".$date[$i]."',
                        categories: [
                                                ";
                                                $cid=array();
                                                $val=0;
                                                //get all the classes
                                                $get_classes=
                                                "SELECT class_name,cId
                                                FROM class_index";
                                                $exe_classes=mysql_query($get_classes);
                                                while($fetch_classes=mysql_fetch_array($exe_classes))
                                                {
                                                $cid[$val] = $fetch_classes[1];	
                                                echo "'$fetch_classes[0]',";	
                                                $val++;	
                                                }

                                                echo "],
                        data: [";
                                                for($j=0;$j<$val;$j++)
                                                {
                                                 $get_absent_details=
                                                "SELECT COUNT(attendance_date_comment.sId)
                                                FROM attendance_date_comment
                                                INNER JOIN class
                                                ON class.sId = attendance_date_comment.sId
                                                WHERE attendance_date_comment.dId = ".$did[$i]."
                                                AND class.classId = ".$cid[$j]."";
                                                $exe_absent_details=mysql_query($get_absent_details);
                                                while($fetch_absent_details=mysql_fetch_array($exe_absent_details))
                                                {
                                                echo $fetch_absent_details[0].",";
                                                }
                                                }
                                                echo "],
                        color: colors[3]
                    }
                },


                                ";
        }
                                echo "];

        function setChart(name, categories, data, color) {
            chart.xAxis[0].setCategories(categories);
            chart.series[0].remove();
            chart.addSeries({
                name: name,
                data: data,
                color: color || 'white'
            });
        }

        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'column'
            },
            title: {
                text: 'Absentees record of past 15 days'
            },
            subtitle: {
                text: 'Click the columns to view details of the day.'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Number of students absent'
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var drilldown = this.drilldown;
                                if (drilldown) { // drill down
                                    setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                } else { // restore
                                    setChart(name, categories, data);
                                }
                            }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y +'';
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var point = this.point,
                        s = this.x +':<b>'+ this.y +' student(s) absent</b><br/>';
                    if (point.drilldown) {
                        s += 'Click to view '+ point.category +' details';
                    } else {
                        s += 'Click to return';
                    }
                    return s;
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }],
            exporting: {
                enabled: false
            }
        });
    });

});
                </script>
";
echo '

<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
</div>
</div>
</div>';


}
//att for first 15 days
//function to show graph to manager for the foll dates
function attendance_date_manager_graph_first()
{
$year=$_GET['year'];
$month=$_GET['month'];
echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Absent details</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';


echo "<script type='text/javascript'>
$(function () {
    var chart;
    $(document).ready(function() {

        var colors = Highcharts.getOptions().colors,
            categories = [
                        ";
                        $did=array();
                        $date=array();
                        $day=array();
                        $count=0;
                        //get all the dates between the years
                         $get_dates=
                        "SELECT date , date_id ,day
                        FROM dates_d
                        WHERE month_of_year=".$month."
                        AND year=".$year."
                        LIMIT 0,15";
                        $exe_dates=mysql_query($get_dates);
                        while($fetch_dates=mysql_fetch_array($exe_dates))
                        {
                        $date[$count]=$fetch_dates[0];	
                        $did[$count] = $fetch_dates[1];
                        $day[$count]=$fetch_dates[2];	
                        echo "'$fetch_dates[0]',";	
                        $count++;
                        }

                        echo "],
            name = 'Absent Record',
            data = [
                                ";
                                //get all the absenties list on the followin date id
                                for($i=0;$i<$count;$i++)
                                {
                            $get_id=
                                "SELECT COUNT(sId)
                                FROM attendance_date_comment
                                WHERE dId =".$did[$i]."";
                                $exe_get_count=mysql_query($get_id);
                                $fetch_data_stu=mysql_fetch_array($exe_get_count);
                                $total_count_sid=$fetch_data_stu[0];



                                echo "
                                {
                    y: ".$total_count_sid.",
                    color: colors[2],
                    drilldown: {
                        name: '(".$day[$i].")".$date[$i]."',
                        categories: [
                                                ";
                                                $cid=array();
                                                $val=0;
                                                //get all the classes
                                                $get_classes=
                                                "SELECT class_name,cId
                                                FROM class_index";
                                                $exe_classes=mysql_query($get_classes);
                                                while($fetch_classes=mysql_fetch_array($exe_classes))
                                                {
                                                $cid[$val] = $fetch_classes[1];	
                                                echo "'$fetch_classes[0]',";	
                                                $val++;	
                                                }

                                                echo "],
                        data: [";
                                                for($j=0;$j<$val;$j++)
                                                {
                                                 $get_absent_details=
                                                "SELECT COUNT(attendance_date_comment.sId)
                                                FROM attendance_date_comment
                                                INNER JOIN class
                                                ON class.sId = attendance_date_comment.sId
                                                WHERE attendance_date_comment.dId = ".$did[$i]."
                                                AND class.classId = ".$cid[$j]."";
                                                $exe_absent_details=mysql_query($get_absent_details);
                                                while($fetch_absent_details=mysql_fetch_array($exe_absent_details))
                                                {
                                                echo $fetch_absent_details[0].",";
                                                }
                                                }
                                                echo "],
                        color: colors[3]
                    }
                },


                                ";
        }
                                echo "];

        function setChart(name, categories, data, color) {
            chart.xAxis[0].setCategories(categories);
            chart.series[0].remove();
            chart.addSeries({
                name: name,
                data: data,
                color: color || 'white'
            });
        }

        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'column'
            },
            title: {
                text: 'Absentees record of past 15 days'
            },
            subtitle: {
                text: 'Click the columns to view details of the day.'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Number of students absent'
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var drilldown = this.drilldown;

                                if (drilldown) { // drill down
                                    setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                } else { // restore
                                    setChart(name, categories, data);
                                }
                            }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y +'';
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var point = this.point,
                        s = this.x +':<b>'+ this.y +' student(s) absent</b><br/>';
                    if (point.drilldown) {
                        s += 'Click to view '+ point.category +' details';
                    } else {
                        s += 'Click to return';
                    }
                    return s;
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }],
            exporting: {
                enabled: false
            }
        });
    });

});
                </script>
";
echo '

<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
</div>
</div>
</div>';


}

//end of function
//function to view graph to manager of absenties 
//calculates monthwise
function attendance_graph_manager()
{
$year=$_GET['year'];	
$month=array();	
$cid=array();

//get all the dId from attendance_date_comment
$get_did=
"SELECT *
FROM attendance_date_comment";
$exe_did=mysql_query($get_did);

echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Graph</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
          ';
          echo "<script type='text/javascript'>
$(function () {
    var chart;
    $(document).ready(function() {

        var colors = Highcharts.getOptions().colors,
            categories = [
                        ";
                //get month an year
                $id=array();

                $get_values_date_d=
                "SELECT DISTINCT month , year 
                FROM dates_d
                WHERE year = ".$year."";
                $exe_month_year=mysql_query($get_values_date_d);
                $c = 0;
                while($fetch_values=mysql_fetch_array($exe_month_year))
                {

                $month[$c]=$fetch_values[0];
                $exp=str_split($fetch_values[0]);	

      echo "'$exp[0]$exp[1]$exp[2]($fetch_values[1])',";  		
          $c++;

                }echo "
                ],
            name = 'Attendance Record',
            data = [

                        ";
                        for($i=0;$i<$c;$i++)
                        {
                        //get all the date_id's of the following month of the foll year

                        //get the absent dates on the folloeing id of the month
                        $get_absent_count=
                        "SELECT COUNT(DISTINCT sId)
                        FROM attendance_date_comment

                        INNER JOIN dates_d
                        ON dates_d.date_id = attendance_date_comment.dId

                        WHERE dates_d.month = '$month[$i]'
                        AND dates_d.year=".$year."
                        ";
                        $exe_count=mysql_query($get_absent_count);
                        $fetch_absent_count=mysql_fetch_array($exe_count);

                        if($fetch_absent_count[0] == "")
                        {
                        $count = 0;	
                        }
                        else
                        {
                        $count=$fetch_absent_count[0];	
                        }

                                if($i+1 == $c)
                                {
                                $d="";	
                                }
                                else
                                {
                                $d=",";	
                                }
                        echo "
                        {
                    y: ".$count.",
                    color: colors[0],
                    drilldown: {
                        name: '$month[$i]',
                        categories: [
                                                ";

                                                //fetch all the classes
                                                 $select_classes=
                                                "SELECT class_name , cId
                                                FROM class_index

                                                ";
                                                $exe_classes=mysql_query($select_classes);

                                                $cls=0;
                                                while($fetch_class_name=mysql_fetch_array($exe_classes))
                                                {
                                                $cid[$cls]=$fetch_class_name[1];	
                                                echo "'$fetch_class_name[0]',";	
                                                        $cls++;
                                                }
                                                echo "

                                                ],
                        data: [
                                                ";
                                                $line=0;
                                                for($j=0;$j<$cls;$j++)
                                                {
                                                //get all the sid for that class
                                                        //get all the sid's
                                        /*	$get_sid=
                                                "SELECT student_user.sId
                                                FROM student_user
                                                INNER JOIN class
                                                ON class.sId = student_user.sId
                                                WHERE class.classId = $cid[$j]
                                                ";
                                                $exe_get_sid=mysql_query($get_sid);
                                                while($fetch_sid=mysql_fetch_array($exe_get_sid))
                                                {*/
                                                //select stduent name for that class
                                        $get_stu_list=
                                                "SELECT COUNT( DISTINCT attendance_date_comment.sId) 
                                                FROM attendance_date_comment


                                            INNER JOIN class
                                                ON class.sId = attendance_date_comment.sId

                                                INNER JOIN dates_d
                                                ON dates_d.month = '".$month[$i]."'

                                                AND class.classId = ".$cid[$j]."";	
                                                $exe_stu_list=mysql_query($get_stu_list);
                                                $fetch_stu_list=mysql_fetch_array($exe_stu_list);
                                                if($fetch_stu_list[0] == "")
                                                {
                                                $data=0;	
                                                }
                                                else
                                                {
                                                $data=$fetch_stu_list[0];	
                                                }
                                                echo $data.",";	

                                                }

                                                echo "
                                                ],
                        color: colors[1]
                    }
                }".$d." 
                                ";

                        }
                        echo "
                ];

        function setChart(name, categories, data, color) {
            chart.xAxis[0].setCategories(categories);
            chart.series[0].remove();
            chart.addSeries({
                name: name,
                data: data,
                color: color || 'white'
            });
        }

        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'column'
            },
            title: {
                text: 'List of absenties for the following months '
            },
            subtitle: {
                text: 'Click to view classwise.'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Total absent'
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var drilldown = this.drilldown;
                                if (drilldown) { // drill down
                                    setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                } else { // restore
                                    setChart(name, categories, data);
                                }
                            }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y +'';
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var point = this.point,
                        s = this.x +':<b>'+ this.y +'student absent</b><br/>';
                    if (point.drilldown) {
                        s += 'Click to view '+ point.category +' class wise absenties';
                    } else {
                        s += 'Click to return';
                    }
                    return s;
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }],
            exporting: {
                enabled: false
            }
        });
    });

});
                </script>
";
echo '
<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>';
          echo '</div>
          </div>
          </div>';


}

//function to that user can select particular dates in which he wants to see values
function admin_att_select_dates()
{

echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select dates</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
          <a href="select_class_for_dates.php"><button class="uibutton normal">View classwise</button></a>
<br>
<br>
<br>
        '; 
        echo '
<form>
<div class="section">
<label>FROM</label>
<div>
<input type="tel" class="birthday" id="from_date"/>
</div>
</div>
<div class="section">
<label>To</label>
<div>
<input type="tel" class="birthday" id="to_date"/>
</div>
</div>

<div class="section last">
<div>
<button class="uibutton submit" type="button" onclick="show_data_dates()">Go</button>
</div>
</div>
</form>	
<div id="show_data_for_dates"></div>

        ';

        echo '</div>
        </div>
        </div>';	

}
//function to view attendance classwise of the following class
function admin_att_select_dates_class()
{
echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select class</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
          <ol class="rounded-list">
          ';
          $get_class=
          "SELECT *
          FROM class_index
          ORDER BY class+0 ASC,section ASC";
          $exe_get_class=mysql_query($get_class);
          while($fetch_classes=mysql_fetch_array($exe_get_class))
          {
                echo '<li><a href="admin_class_selected.php?id_class='.$fetch_classes[0].'">'.$fetch_classes[1].'</a></li>';

          }
          echo '

          </div>
          </div>
          </div>
          ';	


}
function admin_class_selected()
{
$class_id = $_GET['id_class'];
//get name of the class on the id
$get_name_class=
"SELECT class_name
FROM class_index
WHERE cId = ".$class_id."";
$exe_class_name=mysql_query($get_name_class);	
$fetch_class_name=mysql_fetch_array($exe_class_name);
$name_class=$fetch_class_name[0];
echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select class</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
          <h5>Class:'.$name_class.'</h5>
          <form>
<div class="section">
<label>FROM</label>
<div>
<input type="tel" class="birthday" id="from_date_cls"/>
</div>
</div>

<div class="section">
<label>To</label>
<div>
<input type="tel" class="birthday" id="to_date_cls"/>
</div>
</div>
<input type="hidden" value="'.$class_id.'" id="class_id"/>
<div class="section last">
<div>
<button class="uibutton submit" type="button" onclick="show_data_dates_cls()">Go</button>
</div>
</div>
</form>	
<div id="show_data_for_dates_cls"></div>
          </div>
          </div>
          </div>';	
}
//function to call teacher_attendance_weekly_calss
function teacher_attendance_weekly_class()
{

echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select Class</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
        '; 


                   /*	$year_name="SELECT DISTINCT`year`,`month`
                                              FROM `dates_d`
                                                          WHERE `month`=".$_GET['month']." AND`year`=".$_GET['year']."  ";
                                $exe_year_name=mysql_query($year_name);
                                $fetch_year_name=mysql_fetch_array($exe_year_name);


                                */
                                //query to get month name
                                $get_month_name="SELECT `month`
                                                 FROM `dates_d`
                                                                 WHERE `month_of_year`=".$_GET['month']." ";
                                        $exe_month_name=mysql_query($get_month_name);
                                        $fetch_month=mysql_fetch_array($exe_month_name);
                                        $month_name=$fetch_month['month'];			 

  if(($_GET['year'] == "")||($_GET['month'] == ""))
 {

        header('location:teacher_attendance_weekly.php');

  }
  else

                                  echo '<h5 style="color:green"align="center"><td >'.$_GET['year'].'</td></h5>';	
                                  echo '<h5 style="color:green"align="center"><td >'.$fetch_month['month'].'</td></h5>';		
                echo'<h3 style="color:geeen" align="left">Kindly select class</h3>';

                        $teacher_id = $_SESSION['user_id'];
        //Query to get the class of the teacher
          $select_uid="SELECT `class`
                      FROM `teachers`
                                  WHERE `tId`=$teacher_id  ORDER BY class+0 ASC";
                                  $exe_teacher_id=mysql_query($select_uid);
                                  while($fetch_class=mysql_fetch_array( $exe_teacher_id))
                                  { 
                                         $select_classid="SELECT cId ,class_name
                                                FROM class_index
                                         WHERE `class_name`='".$fetch_class['class']."' ORDER BY class+0 ASC,section ASC";
                                         $exe_class_id=mysql_query($select_classid);
                                         $res=mysql_fetch_array($exe_class_id);
                                          $class_name=$res['class_name'];
        echo '				                                                              
  <li><a class="uibutton large"href="teacher_view_attendance_weekly.php?year='.$_GET['year'].'&&month='.$_GET['month'].'&&from_date='.$_GET['from_date'].'&&to_date='.$_GET['to_date'].'&&class_id='.$res['cId'].'">'.$class_name.'</a>                                                                    	                                 
          ';

}		 																
echo'	                       
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 					
}
//function to call teacher_attendance_class
function teacher_view_attendance_weekly()
{
        echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>View Attendance</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
        '; 
$class_id=$_GET['class_id'];
$get_year=$_GET['year'];
$month=$_GET['month'];
$from=$_GET['from_date'];
$to=$_GET['to_date'];
                  //query to get month name
                                $get_month_name="SELECT `month`
                                                 FROM `dates_d`
                                                                 WHERE `month_of_year`=".$_GET['month']." ";
                                        $exe_month_name=mysql_query($get_month_name);
                                        $fetch_month=mysql_fetch_array($exe_month_name);
                                        $month_name=$fetch_month['month'];			 

  if(($_GET['year'] == "")||($_GET['month'] == ""))
 {

        header('location:admin_attendance_weekly.php');

  }
  else
  //query to get class name
  $q="SELECT `cId`,`class_name` FROM class_index 
                             WHERE `cId`=".$_GET['class_id']."";
                 $q_res=mysql_query($q);
                         $res=mysql_fetch_array($q_res);
            $class=$res['class_name'];


 echo '<h5 style="color:green"align="center"><td >'.$_GET['year'].'</td></h5>';	
 echo '<h6 style="color:green"align="center"><td >'.$fetch_month['month'].' (from '.$_GET['from_date'].' to '.$_GET['to_date'].')</td></h6>'; 
echo ' <h5 style="color:green"align="left"><td >CLASS '.$class.'</td></h5>';		

//get all the date and extract years
                echo'
        <table  class="table table-bordered table-striped"  >
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Admission No.</th>
                        <th>Status</th>		
                        <th>Number of days absent</th>								
                    </tr>
                    </thead>	   	
        <tbody align="center"> 

        '; 	
//query to get details student adn class table
 $query_get_details_students = "
                                                        SELECT DISTINCT student_user.Name, student_user.sId,student_user.admission_no
                                                        FROM student_user

                                                        INNER JOIN class
                                                        ON class.sId = student_user.sId

                                                        INNER JOIN attendance_date_comment
                                                        ON attendance_date_comment.sId = student_user.sId

                                                        WHERE class.classId = ".$_GET['class_id']." 
                                                ";
                               $execute_get_details_students = mysql_query($query_get_details_students);
                                           while($get_details_students = mysql_fetch_array($execute_get_details_students))
                                               {     
                                                     $admission_no=$get_details_students['admission_no'];
                                                   $sid=$get_details_students['sId'];


                                          //get date id on the date and year
                                        $get_date_id_from = 
                                          "SELECT date_id
                                          FROM dates_d
                                          WHERE date = '".$get_year."-".$month."-".$from."'";
                                          $exe_date_from_date=mysql_query($get_date_id_from);
                                          $fetch_date_id_from=mysql_fetch_array($exe_date_from_date);
                                          //for to date id
                                                $get_date_id_to = 
                                          "SELECT date_id
                                          FROM dates_d
                                          WHERE date = '".$get_year."-".$month."-".$to."'";
                                          $exe_date_to_date=mysql_query($get_date_id_to);
                                          $fetch_date_id_to=mysql_fetch_array($exe_date_to_date);
                                          //get students absent on the following date id's
                                          $get_list=
                                          "SELECT COUNT(sId)
                                          FROM attendance_date_comment
                                          WHERE dId BETWEEN '".$fetch_date_id_from[0]."' AND '".$fetch_date_id_to[0]."'
                                          AND sId = ".$sid."";
                                          $exe_get_list=mysql_query($get_list);
                                          $fetch_data_from_list=mysql_fetch_array($exe_get_list);


                                        /*  //query to get timestamp
                                            $get_day=
                                                  "SELECT `timestamp`
                                                  FROM   `attendance_date_comment`
                                                  ";
                                                  $exe_day=mysql_query($get_day);
                                                  while($fetch_day=mysql_fetch_array($exe_day))
                                                  {
                                                  $date= $fetch_day[0];
                                                  $date_array = explode("-",$date);
                                                  $day=$date_array[2];
                                                  $day_array_font=explode(" ",$day);
                                                  $split=$day_array_font[1];

echo $get_details=
"SELECT COUNT(sId)
FROM  `attendance_date_comment`
WHERE `timestamp` > '".$get_year."-".$month."-".$from." ".$split."' AND `timestamp` < '".$get_year."-".$month."-".$to." ".$split."'
AND `sId`=".$sid."";					   
$execute_attendance=mysql_query($get_details);
$fetch_sid=mysql_fetch_array($execute_attendance);	
                                                  */






                                  echo '<tr>
<td><a href="teacher_view_absent_attendance_weekly.php?sid='.$sid.'&class='.$class.'&year='.$_GET['year'].'&month='.$month.'&from='.$_GET['from_date'].'&to='.$_GET['to_date'].'">'.$get_details_students['Name'].'</a></td>

                                        <td>'.$get_details_students['admission_no'].'</td>';	
                                                  if($fetch_data_from_list[0] > 0)
                                                  {
                                                  echo '<td><label style="color:red">Absent</label></td>';
                                                  }
                                                  else
                                                  {
                                                  echo '<td><label style="color:green">Present</label></td>';	
                                                  }
                                                  echo '<td>'.$fetch_data_from_list[0].'</td>';

                                                  echo '</tr>';
               }			


        //query to get sid from student_user	
                /*	 $get_details=
                                "SELECT COUNT(sId)
                                FROM attendance_date_comment
                                INNER JOIN dates_d
                                ON dates_d.date_id = attendance_date_comment.dId
                                WHERE dates_d.year=".$get_year."
                                AND dates_d.month_of_year=".$month."
                                AND attendance_date_comment.sId = ".$sid." ";
                                $exe_get_details=mysql_query($get_details);
                                while($fetch_get_details=mysql_fetch_array($exe_get_details))
                                                {						
                                                 echo '<tr>
                                                           <td>'.$get_details_students['Name'].'</td>
                                                             ';							
                                                        if($fetch_get_details[0] > 0)
                                                        {
                                                        echo '<td><label style="color:red">Absent</label></td>';
                                                        }
                                                        else
                                                        {
                                                        echo '<td><label style="color:green">Present</label></td>';	
                                                        }
                                                        echo '<td>'.$fetch_get_details[0].'</td>';

                                                    echo '</tr>';
                                                        }			*/



echo' </tbody>
         </table>   	                       
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 	


}



//function to call teacher_view_absent_attendance_weekly
function teacher_view_absent_weekly()
{

echo '<div class="row-fluid">			  
<div class="span12  widget clearfix">  
<div class="widget-header">
<span><i class="icon-align-center"></i>Attendance</span>
</div><!-- End widget-header -->
<div class="widget-content">
        '; 

        //query to get name
         $query_get_details_students = "
                                                        SELECT DISTINCT Name
                                                        FROM student_user
                                                    WHERE sId=".$_GET['sid']."
                                                  ";

         $exe_details_students=mysql_query($query_get_details_students);
         $fetch_details_students=mysql_fetch_array($exe_details_students);
         $sname=$fetch_details_students['Name'];

         echo '<h5 style="color:green"align="left"><td >Name -'.$sname.'</td></h5>';


         echo'
        <table  class="table table-bordered table-striped"  >
                    <thead>
                    <tr>
                        <th><h5 style="color:red">Absent Date</h5></th>						
                    </tr>
                    </thead>	   	
        <tbody align="center"> 
        '; 		





                                          //get date id on the date and year
                                        $get_date_id_from = 
                                          "SELECT date_id
                                          FROM dates_d
                                          WHERE date ='".$_GET['year']."-".$_GET['month']."-".$_GET['from']."'";
                                          $exe_date_from_date=mysql_query($get_date_id_from);
                                          $fetch_date_id_from=mysql_fetch_array($exe_date_from_date);

                                          //for to date id
                                                $get_date_id_to = 
                                          "SELECT date_id
                                          FROM dates_d
                                          WHERE date = '".$_GET['year']."-".$_GET['month']."-".$_GET['to']."'";
                                          $exe_date_to_date=mysql_query($get_date_id_to);
                                          $fetch_date_id_to=mysql_fetch_array($exe_date_to_date);


                                          //get students absent on the following date id's
                                          $get_list=
                                          "SELECT  dates_d.date,attendance_date_comment.sId
                                          FROM dates_d

                                          INNER JOIN attendance_date_comment
                                          ON  attendance_date_comment.dId=dates_d.date_id

                                          WHERE date_id BETWEEN '".$fetch_date_id_from[0]."' AND '".$fetch_date_id_to[0]."'
                                          AND attendance_date_comment.sId=".$_GET['sid']."
                                          ";
                                          $exe_get_list=mysql_query($get_list);
                                         while( $fetch_data_from_list=mysql_fetch_array($exe_get_list))
                                         {
                                                 echo '<tr>
                                          <td><b>'. $fetch_data_from_list[0].'</b></td>	
                                          </tr>';	 

                                                 }






        echo'	
       </tbody>
           </table>                
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
';	


}
//function to call teacher_attendance_monthly
function teacher_attendance_monthly()
{


echo '<div class="row-fluid">		  
 <div class="span12  widget clearfix">
 <div class="widget-header">
  <span><i class="icon-align-center"></i>Select  year & Month</span>
  </div><!-- End widget-header -->
   <div class="widget-content">
        ';
        //query to get year and date id
          $get_visitor_year="SELECT DISTINCT `year`
                               FROM  `dates_d`
                                          ";
        echo' 
                <form action="teacher_attendance_class_monthly.php" method="get" >
                <div class="section">
        <label>Select Year<small></small></label>           
                 <div >
                 <select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" name="year" >        
                 <option value=""></option>'; 								
                        //query to get year		

        $execute_visitor_year=mysql_query($get_visitor_year);
        while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
              {   
                         $fetch_year=$fetch_visitor_year['year'];		

        echo'
                     <option value="'.$fetch_year.'">'. $fetch_year.'</option>';
                  }

         echo'  </select> 
                </div>
                   </div> 
                                   ';	
                                  $get_visitor_month="SELECT DISTINCT `month`,`month_of_year`
                                           FROM  `dates_d`
                                                       WHERE `year`=". $fetch_year." ";			
                        echo' 
                 <div class="section ">
         <label>Select Month<small></small></label>              
                 <div >
                 <select  data-placeholder="Choose  month..." class="chzn-select" tabindex="2" id="month" name="month">        
                            <option value=""></option>'; 								
                        //query to get month and month of year		

        $execute_visitor_month=mysql_query($get_visitor_month);
        while($fetch_visitor_month=mysql_fetch_array($execute_visitor_month))//looping for getting month
              {   

                         $fetch_month=$fetch_visitor_month['month'];
                         $fetch_month_year=$fetch_visitor_month['month_of_year'];

        echo'
                     <option value="'.$fetch_month_year.'">'. $fetch_month.'</option>';
                  }

echo'    </select> 
             </div>  
                 </div> 
                 <div class="section last">
                  <div><button  class="uibutton submit_form" value="submit">Go</button>
                  </div>
                  </div>		 
                </form> '; 	 																
echo'	    
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 		

}

//function to call teacher_attendance_class_monthly
function teacher_attendance_class_monthly()
{


        echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select Class</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
        '; 


                   /*	$year_name="SELECT DISTINCT`year`,`month`
                                              FROM `dates_d`
                                                          WHERE `month`=".$_GET['month']." AND`year`=".$_GET['year']."  ";
                                $exe_year_name=mysql_query($year_name);
                                $fetch_year_name=mysql_fetch_array($exe_year_name);


                                */
                                //query to get month name
                                $get_month_name="SELECT `month`
                                                 FROM `dates_d`
                                 WHERE `month_of_year`=".$_GET['month']." ";
                                        $exe_month_name=mysql_query($get_month_name);
                                        $fetch_month=mysql_fetch_array($exe_month_name);
                                        $month_name=$fetch_month['month'];			 

  if(($_GET['year'] == "")||($_GET['month'] == ""))
 {

        header('location:teacher_attendance_monthly.php');

  }
  else
  {
        echo'<h3 style="color:" align="left">Kindly select class</h3>';
                                  echo '<h5 style="color:green"align="center"><td >'.$_GET['year'].'</td></h5>';	
                                  echo '<h5 style="color:green"align="center"><td >'.$fetch_month['month'].'</td></h5>';		


                        $teacher_id = $_SESSION['user_id'];
        //Query to get the class of the teacher
          $select_uid="SELECT DISTINCT`class`
                      FROM `teachers`
                                  WHERE `tId`=$teacher_id ORDER BY class +0 ASC ";
                                  $exe_teacher_id=mysql_query($select_uid);
                                  while($fetch_class=mysql_fetch_array( $exe_teacher_id))
                                  { 
                                            $select_classid="SELECT cId ,class_name
                                                                             FROM class_index
                                         WHERE `class_name`='".$fetch_class['class']."' ORDER BY class +0 ASC,section ASC ";
                                         $exe_class_id=mysql_query($select_classid);
                                         $res=mysql_fetch_array($exe_class_id);
                                          $class_name=$res['class_name'];
        echo '				                                                              
 <a  href="teacher_view_attendance_monthly.php?year='.$_GET['year'].'&&month='.$_GET['month'].'&&class_id='.$res['cId'].'">    <div class="shoutcutBox"> <span class="ico color chat-exclamation"></span> <em><b style="color:green">'.$class_name.' </span></em> </div></a>                                                     	                                 
           ';	 
} 
		 																
echo'	                       
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 			

  }
}

//function to call teacher_view_attendance_monthly
function  teacher_view_attendance_monthly()
{

echo '<div class="row-fluid">			  
<div class="span12  widget clearfix">  
<div class="widget-header">
<span><i class="icon-align-center"></i>Attendance</span>
</div><!-- End widget-header -->
<div class="widget-content">
        '; 
        //query to get class details
         $q="SELECT `cId`,`class_name` FROM class_index
            WHERE `cId`=".$_GET['class_id']." 
                             ORDER BY class, section ASC";
                 $q_res=mysql_query($q);
                         $res=mysql_fetch_array($q_res);
                         $class=$res['class_name'];
                         //query to get month	 
        $get_month_name="SELECT `month`
                                                 FROM `dates_d`
                                                                 WHERE `month_of_year`=".$_GET['month']." ";
                                        $exe_month_name=mysql_query($get_month_name);
                                        $fetch_month=mysql_fetch_array($exe_month_name);
                                        $month_name=$fetch_month['month'];			 

        $class_id=$_GET['class_id'];
                  //get date id's of the follwing year and month
        $get_id=
        "SELECT date_id
        FROM dates_d
        WHERE year = ".$_GET['year']."
        AND month_of_year = ".$_GET['month']."";
        $exe_id=mysql_query($get_id);
        $days_total=0;
        while($fetch_date_id = mysql_fetch_array($exe_id))
        {
        //get all the date id's from the working days table to calculate total number of working days
        $get_working_id=
        "SELECT Date_working_id
        FROM working_days
        WHERE Date_working_id = ".$fetch_date_id[0]."";
        $exe_working_id=mysql_query($get_working_id);
        $fetch_working_id=mysql_fetch_array($exe_working_id);	
        if($fetch_working_id[0] == $fetch_date_id[0])
        {	

        $days_total++;
        }
        } 
        echo '<h5 align="left">Total no. of working days: '.$days_total.'</h5>';
        echo '<h5 style="color:green"align="center">'.$_GET['year'].'</h5>';
        echo '<h5 style="color:green"align="center">'.$month_name.'</h5>';
        echo '<h5 style="color:green"align="left" ><b>Class Name:</b>'. $class.'</h5>';

                        echo'
        <table  class="table table-bordered table-striped"  >
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Admission No.</th>
                        <th>Status</th>		
                        <th>Number of days absent</th>		
                        <th>Percentage(%)</th>						
                    </tr>
                    </thead>	   	
        <tbody align="center"> 
        '; 	
        $percentage=0;
         $query_get_details_students = "
                                                        SELECT DISTINCT student_user.Name, student_user.sId,student_user.admission_no
                                                        FROM student_user

                                                        INNER JOIN class
                                                        ON class.sId = student_user.sId


                                                        WHERE class.classId = ".$class_id." 
                                                ";
                               $execute_get_details_students = mysql_query($query_get_details_students);
                                           while($get_details_students = mysql_fetch_array($execute_get_details_students))
                                           {      $sid=$get_details_students['sId'];

        //query to get sid from student_user

                                $year = $_GET['year'];
                                $month = $_GET['month'];

                         $get_details=
                                "SELECT COUNT(sId)
                                FROM attendance_date_comment
                                INNER JOIN dates_d
                                ON dates_d.date_id = attendance_date_comment.dId
                                WHERE dates_d.year=".$year."
                                AND dates_d.month_of_year=".$month."
                                AND attendance_date_comment.sId = ".$sid." ";

                                $exe_get_details=mysql_query($get_details);
                                while($fetch_get_details=mysql_fetch_array($exe_get_details))
                                                {	

                                                         echo '<tr>
 <td><a href="teacher_view_attendance_absent_date_monthly.php?sid='.$sid.'&&class='.$class.'&&year='.$year.'&&month='.$month.'">'.$get_details_students['Name'].'</a></td>
                                                                   <td>'.$get_details_students['admission_no'].'</td>
                                                             ';							
                                                        if($fetch_get_details[0] > 0)
                                                        {
                                                        echo '<td><label style="color:red">Absent</label></td>';
                                                        }
                                                        else
                                                        {
                                                        echo '<td><label style="color:green">Present</label></td>';	
                                                        }
                                                        echo '<td>'.$fetch_get_details[0].'</td>';

                                                        $left_present=$days_total - $fetch_get_details[0];
                                                        if($left_present == 0)
                                                        {

                                                        $percentage=0;	
                                                        }
                                                        else
                                                        {
                                                                if($days_total!=0)
                                                                {
                                                        $percentage = $left_present/$days_total*100;
                                                                }


                                                        }settype($percentage, "integer");
                                                        echo '<td>'.$percentage.'.0</td>';

                                                        echo '</tr>';

                                                        }			

                                           }

echo'	
       </tbody>
           </table>                
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
';	


}
//function to call teacher_view_attendance_absent_date_monthly
function teacher_view_absent_monthly()
{
        echo '<div class="row-fluid">			  
<div class="span12  widget clearfix">  
<div class="widget-header">
<span><i class="icon-align-center"></i>Attendance</span>
</div><!-- End widget-header -->
<div class="widget-content">
        '; 

        //query to get name
         $query_get_details_students = "
                                                        SELECT DISTINCT Name
                                                        FROM student_user
                                                    WHERE sId=".$_GET['sid']."
                                                  ";

         $exe_details_students=mysql_query($query_get_details_students);
         $fetch_details_students=mysql_fetch_array($exe_details_students);
         $sname=$fetch_details_students['Name'];

         echo '<h5 style="color:green"align="left"><td >Name -'.$sname.'</td></h5>';


         echo'
        <table  class="table table-bordered table-striped"  >
                    <thead>
                    <tr>
                        <th><h5 style="color:red">Absent Date</h5></th>						
                    </tr>
                    </thead>	   	
        <tbody align="center"> 
        '; 		


        //query to get date 
         $get_absent_details=" SELECT  dates_d.date,attendance_date_comment.sId
                                    FROM dates_d
                                                        INNER JOIN attendance_date_comment 
                                                ON attendance_date_comment.dId  = dates_d.date_id
                                                        WHERE attendance_date_comment.sId=".$_GET['sid']."
                                                        AND  dates_d.year=".$_GET['year']."
                                                AND dates_d.month_of_year=".$_GET['month']."
                                                     ";
                $exe_absent_details=mysql_query($get_absent_details);
                while($fetch_absent_details=mysql_fetch_array($exe_absent_details))
                        {
                                 echo '<tr>
                                          <td><b>'.$fetch_absent_details[0].'</b></td>	
                                          </tr>';	
                        }			


        echo'	
       </tbody>
           </table>                
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
';	

}

//function to call teacher_attendance_select_date
function teacher_attendance_select_date()
{

        echo '<div class="row-fluid">			  
<div class="span12  widget clearfix">  
<div class="widget-header">
<span><i class="icon-align-center"></i>Select Date</span>
</div><!-- End widget-header -->
<div class="widget-content">
        '; 


         echo'                      
        <form action="teacher_attendance_select_class.php" method="get" > 
           <div class="section">
           <label>Select Date</label>   
           <div>
           <input type="text"  class=" birthday  small " name="date" id="datePicker"/></div>
           </div> 
           <div class="section last">
           <div><button  class="uibutton submit_form" type="submit">Go</button>
                </div>
           </div>		  
      </form>

';					 
        echo'	
       </tbody>
           </table>                
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
';	

}
//function to call teacher_attendance_select_class
function teacher_attendance_select_class()
{

        echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select Class</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
        '; 


  if(($_GET['date'] == ""))
 {

        header('location:teacher_attendance_select_date.php');

  }
  else

echo '<h5 style="color:green"align="center" ><b>Date:</b>'. $_GET['date'].'</h5>';
      echo'<h3 style="color:" align="left">Kindly select class</h3>';

                        $teacher_id = $_SESSION['user_id'];
        //Query to get the class of the teacher
          $select_uid="SELECT DISTINCT`class`
                      FROM `teachers`
                                  WHERE `tId`=$teacher_id";
                                  $exe_teacher_id=mysql_query($select_uid);
                                  while($fetch_class=mysql_fetch_array( $exe_teacher_id))
                                  { 
                                         $select_classid="SELECT cId ,class_name
                         FROM class_index
                                         WHERE `class_name`='".$fetch_class['class']."' ";
                                         $exe_class_id=mysql_query($select_classid);
                                         $res=mysql_fetch_array($exe_class_id);
                    $class_name=$res['class_name'];

                 echo '				                                                              
  <li><a class="uibutton large"href="teacher_attendance_view_select_date.php?date='.$_GET['date'].'&&class_id='.$res['cId'].'">'.$class_name.'</a>                                                                    	                                 
          ';	 	 
}		 																
echo'	                       
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 			
}


//function to call teacher_attendance_view_select_date
function teacher_view_attendance_select_date()
{

        echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Attendance</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
        '; 
                echo '<h5 style="color:green"align="center" ><b>Date:</b>'. $_GET['date'].'</h5>';


                                echo'
        <table  class="table table-bordered table-striped"  >
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Admission No.</th>
                        <th>Status</th>		
                        <th>Number of days absent</th>								
                    </tr>
                    </thead>	   	
        <tbody align="center"> 
        '; 	
         $query_get_details_students = "
                                                        SELECT DISTINCT student_user.Name, student_user.sId,student_user.admission_no
                                                        FROM student_user

                                                        INNER JOIN class
                                                        ON class.sId = student_user.sId

                                                        INNER JOIN attendance_date_comment
                                                        ON attendance_date_comment.sId = student_user.sId

                                                        WHERE class.classId = ".$_GET['class_id']." 
                                                ";
                               $execute_get_details_students = mysql_query($query_get_details_students);
                                           while($get_details_students = mysql_fetch_array($execute_get_details_students))
                                           {      $sid=$get_details_students['sId'];

        //query to get sid from student_user


                 $get_details=
                                "SELECT COUNT(sId)
                                FROM attendance_date_comment
                                INNER JOIN dates_d
                                ON dates_d.date_id = attendance_date_comment.dId
                                WHERE dates_d.date='".$_GET['date']."'

                                AND attendance_date_comment.sId = ".$sid." ";

                                $exe_get_details=mysql_query($get_details);
                                while($fetch_get_details=mysql_fetch_array($exe_get_details))
                                                {	

                                                         echo '<tr>
                                  <td>'.$get_details_students['Name'].'</td>
                                                                   <td>'.$get_details_students['admission_no'].'</td>
                                                             ';							
                                                        if($fetch_get_details[0] > 0)
                                                        {
                                                        echo '<td><label style="color:red">Absent</label></td>';
                                                        }
                                                        else
                                                        {
                                                        echo '<td><label style="color:green">Present</label></td>';	
                                                        }
                                                        echo '<td>'.$fetch_get_details[0].'</td>';

                                                        echo '</tr>';

                                                        }			

                                           }




echo'	
</tbody>
</table>                       
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 	


}
//manish coding end
//manish functions
//function to call admin_attendance_monthly	 
function  admin_attendance_monthly()
 {


echo '<div class="row-fluid">

 <div class="span12  widget clearfix">

 <div class="widget-header">
  <span><i class="icon-align-center"></i>Select  year & Month</span>
  </div><!-- End widget-header -->
   <div class="widget-content">


        ';
        //query to get year and date id
          $get_visitor_year="SELECT DISTINCT `year`
                               FROM  `dates_d`  WHERE  date_id=3287
                                          ";
        echo' 
                <form action="admin_attendance_class.php" method="get" >
                <div class="section">
        <label>Select Year<small></small></label>           
                 <div >
                 <select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" name="year" >        
                 <option value=""></option>'; 								
                        //query to get year		

        $execute_visitor_year=mysql_query($get_visitor_year);
        while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
              {   
                         $fetch_year=$fetch_visitor_year['year'];		

        echo'
                     <option value="'.$fetch_year.'">'. $fetch_year.'</option>';
                  }

         echo'  </select> 
                </div>
                   </div> 
                                   ';	
                                  $get_visitor_month="SELECT DISTINCT `month`,`month_of_year`
                                           FROM  `dates_d`
                                                       WHERE `year`=". $fetch_year." ";			
                        echo' 
                 <div class="section ">
         <label>Select Month<small></small></label>              
                 <div >
                 <select  data-placeholder="Choose  month..." class="chzn-select" tabindex="2" id="month" name="month">        
                            <option value=""></option>'; 								
                        //query to get month and month of year		

        $execute_visitor_month=mysql_query($get_visitor_month);
        while($fetch_visitor_month=mysql_fetch_array($execute_visitor_month))//looping for getting month
              {   

                         $fetch_month=$fetch_visitor_month['month'];
                         $fetch_month_year=$fetch_visitor_month['month_of_year'];




        echo'
                     <option value="'.$fetch_month_year.'">'. $fetch_month.'</option>';
                  }

echo'    </select> 
             </div>  
                 </div> 
                 <div class="section last">
                  <div><button  class="uibutton submit_form" value="submit">Go</button>
                  </div>
                  </div>		 

                </form> '; 



echo'	

 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 	

} 
//functio to view messagelog to admin
function view_message_log()
{
echo '<div class="row-fluid">

 <div class="span12  widget clearfix">

 <div class="widget-header">
  <span><i class="icon-align-center"></i>Message Log(search accordingly)</span>
  </div><!-- End widget-header -->
   <div class="widget-content">
        ';
//get all the valus to view in the message log
$get_details_message=
"SELECT student_user.Name , student_user.admission_no , dates_d.date,message_log.message
FROM student_user

INNER JOIN message_log
ON message_log.student_id = student_user.sId

INNER JOIN dates_d
ON dates_d.date_id = message_log.date_id ";

$exe_details_message=mysql_query($get_details_message);
echo ' <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead>
                                                <tr>
                                                <th>Student Name</th>
                                                <th>Admission No.</th>
                                                <th>Date</th>
                                                <th>Message</th>
                                                </thead>
                                                <tbody align="center">
                                                ';
while($fetch_details_message=mysql_fetch_array($exe_details_message))
{
echo '<tr>
<td>'.$fetch_details_message[0].'</td>
<td>'.$fetch_details_message[1].'</td>
<td>'.date('jS F Y',strtotime($fetch_details_message[2])).'</td>
<td>'.$fetch_details_message[3].'</td>
</tr>';	

}

echo '
</tbody>
</table>
</div>
</div>
</div>';	


}
function view_class_for_attendance_for_selected_date()
{
        echo '<iframe id="I2" style="display:none"></iframe>';
        //Function to generate list of students in the particular class for the present date.
        //Class id to be taken from GET

        $classId = $_GET['classId'];

        $selected_date = $_GET['date'];

 $date_in_format= date('Y-m-d',strtotime($selected_date));

        $no=1;

                        $no=1;


        $queryDateId = "
SELECT `date_id`
FROM `dates_d`
WHERE `date` ='".$date_in_format."'
";


$getDateId = mysql_query($queryDateId);
$dateId = mysql_fetch_array($getDateId);
$dateId = $dateId[0];

        $queryClassName = "
        SELECT `class_name`
        FROM `class_index`
        WHERE `cId` = '$classId'
        ";
        $getClassName = mysql_query($queryClassName);
        $className = mysql_fetch_array($getClassName);
        $className = $className[0];		//Name of the class



        //Query to get the name of students and there SID for attendance
        $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$_GET['classId']."'
                AND session_id='".$_SESSION['current_session_id']."'";
        $exe = mysql_query($query);

        echo '


                                <div class="row-fluid">

 <div class="span12  widget clearfix">

 <div class="widget-header">
  <span><i class="icon-align-center"></i>Select  year & Month</span>
  </div><!-- End widget-header -->
   <div class="widget-content">
        <h5 align="center">Class : '.$className.'</h5>
                                                                                        <h5 align="center">Date : '.$date_in_format.'</h5>									
                                                                                        <h5 align="center">Kindly select the Students who are absent</h5>
                          <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead>
                                                <tr>


                                                        <th scope="col">S.no</th>
                                                        <th scope="col">Name</th>
                                                        <th scope="col">Admission no</th>
                                                        <th scope="col">Action</th>


                                                </tr>
                                        </thead>

                                        <tbody align="center">
                                        '; while($fe=mysql_fetch_array($exe)){
                                                 echo'
                                                <tr id="txtboxes_view_return">
                                                        <td class="align-center">'.$no.'</td></td>
                                                        <td class="align-center"> '.$fe['Name'].'</td>
                                                        <td class="align-center">'.$fe['admission_no'].' </td>';

                                                        $sql="SELECT Id
                                                        FROM attendance_date_comment 
                                                        WHERE dId='".$dateId."' 
                                                        AND sId='".$fe['sId']."'";

                        $query_exe=mysql_query($sql);
                        $query_fetch=mysql_fetch_array($query_exe);
                                                        if(isset($query_fetch['Id']))
                                                        {
                                                                echo '<td class="align-center">Student Absent</td>';	
                                                        }
                                                        else 
                                                        {
                                                                echo' <td class="align-center" id="'.$fe['sId'].'"><a style="cursor:pointer" onclick="mark_absent_selected_date('.$fe['sId'].','.$dateId.');">Absent</a>
                                                ';}
                                                echo'

                                                </tr>
                                                ';++$no; }echo'
                                        </tbody>
                                </table>

                        </div>
                        </div>

                        </div>';	




}


function admin_attendance_class()
{

echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select Class</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
        '; 


                   /*	$year_name="SELECT DISTINCT`year`,`month`
                                              FROM `dates_d`
                                                          WHERE `month`=".$_GET['month']." AND`year`=".$_GET['year']."  ";
                                $exe_year_name=mysql_query($year_name);
                                $fetch_year_name=mysql_fetch_array($exe_year_name);


                                */
                                //query to get month name
                                $get_month_name="SELECT `month`
                                                 FROM `dates_d`
                                 WHERE `month_of_year`=".$_GET['month']." ";
                                        $exe_month_name=mysql_query($get_month_name);
                                        $fetch_month=mysql_fetch_array($exe_month_name);
                                        $month_name=$fetch_month['month'];			 

  if(($_GET['year'] == "")||($_GET['month'] == ""))
 {

        header('location:admin_attendance_monthly.php');

  }
  else
                                  echo '<h5 style="color:green"align="center"><td >'.$_GET['year'].'</td></h5>';	
                                  echo '<h5 style="color:green"align="center"><td >'.$fetch_month['month'].'</td></h5>';		





                 $disp='';
                 $q="SELECT `cId`,`class_name` FROM class_index 
                           ORDER BY class+0 ASC,section ASC";
                 $q_res=mysql_query($q);

          echo' <div class="span4"><h3 style="color:">Kindly select class</h3></div>';
          echo '</h2>';
          echo "<br>";
          echo "<br>";
          echo "<br>";
           echo '<ol class="rounded-list">';
while($res=mysql_fetch_array($q_res))//looping for getting  class id
{	$class=$res['class_name'];
        echo '<tr class="row-fluid" id="added_rows">
          <td><div class="row-fluid">
          <div class="span6">							                                                              
  <li><a href="admin_attendance_view_monthly.php?year='.$_GET['year'].'&&month='.$_GET['month'].'&&class_id='.$res['cId'].'">'.$res['class_name'].'</a>                                                                    	                                 
           </div>
           </div><!-- end row-fluid  -->
       </td>
       </tr>';	 	 
}		 																
echo'	                       
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 		
}

//function to call admin_attendance_view_monthly
function attendance_view_monthly()
{						 
echo '<div class="row-fluid">

<div class="span12  widget clearfix">

<div class="widget-header">
<span><i class="icon-align-center"></i>Attendance</span>
</div><!-- End widget-header -->
 <div class="widget-content">

        '; 
        //get date id's of the follwing year and month
        $get_id=
        "SELECT date_id
        FROM dates_d
        WHERE year = ".$_GET['year']."
        AND month_of_year = ".$_GET['month']."";
        $exe_id=mysql_query($get_id);
        $days_total=0;
        while($fetch_date_id = mysql_fetch_array($exe_id))
        {
        //get all the date id's from the working days table to calculate total number of working days
        $get_working_id=
        "SELECT Date_working_id
        FROM working_days
        WHERE Date_working_id = ".$fetch_date_id[0]."";
        $exe_working_id=mysql_query($get_working_id);
        $fetch_working_id=mysql_fetch_array($exe_working_id);	
        if($fetch_working_id[0] == $fetch_date_id[0])
        {	

        $days_total++;
        }
        }


         $q="SELECT `cId`,`class_name` FROM class_index
            WHERE `cId`=".$_GET['class_id']." 
                             ORDER BY class, section ASC";
                 $q_res=mysql_query($q);
                         $res=mysql_fetch_array($q_res);
                         $class=$res['class_name'];
                         //query to get month

        $get_month_name="SELECT `month`
                                                 FROM `dates_d`
                                         WHERE `month_of_year`=".$_GET['month']." ";
                                        $exe_month_name=mysql_query($get_month_name);
                                        $fetch_month=mysql_fetch_array($exe_month_name);
                                        $month_name=$fetch_month['month'];			 

        $class_id=$_GET['class_id'];
        echo '<h5 align="left">Total no. of working days: '.$days_total.'</h5>';
        echo '<h5 style="color:green"align="center">'.$_GET['year'].'</h5>';
        echo '<h5 style="color:green"align="center">'.$month_name.'</h5>';
        echo '<h5 style="color:green"align="left" ><b>Class Name:</b>'. $class.'</h5>';

                        echo'
        <table  class="table table-bordered table-striped" id="dataTable" >
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>		
                        <th>Number of days absent</th>	
                        <th>Percentage(%)</th>							
                    </tr>
                    </thead>	   	
        <tbody align="center"> 
        '; 	
         $query_get_details_students = "
                                                        SELECT DISTINCT student_user.Name, student_user.sId
                                                        FROM student_user

                                                        INNER JOIN class
                                                        ON class.sId = student_user.sId



                                                        WHERE class.classId = ".$class_id." 
                                                ";
                               $execute_get_details_students = mysql_query($query_get_details_students);
                                           while($get_details_students = mysql_fetch_array($execute_get_details_students))
                                           {      $sid=$get_details_students['sId'];

        //query to get sid from student_user

                                $year = $_GET['year'];
                                $month = $_GET['month'];

                         $get_details=
                                "SELECT COUNT(sId)
                                FROM attendance_date_comment
                                INNER JOIN dates_d
                                ON dates_d.date_id = attendance_date_comment.dId
                                WHERE dates_d.year=".$year."
                                AND dates_d.month_of_year=".$month."
                                AND attendance_date_comment.sId = ".$sid." ";

                                $exe_get_details=mysql_query($get_details);
                                while($fetch_get_details=mysql_fetch_array($exe_get_details))
                                                {	

                                                         echo '<tr>
                                                               <td>'.$get_details_students['Name'].'</td>
                                                             ';							
                                                        if($fetch_get_details[0] > 0)
                                                        {
                                                        echo '<td><label style="color:red">Absent</label></td>';
                                                        }
                                                        else
                                                        {
                                                        echo '<td><label style="color:green">Present</label></td>';	
                                                        }
                                                        echo '<td>'.$fetch_get_details[0].'</td>
                                                        ';
                                                        $left_present=$days_total - $fetch_get_details[0];
                                                        if($left_present == 0)
                                                        {
                                                        $percentage = 0;	
                                                        }
                                                        else
                                                        {  
                                                        if($days_total!=0)
                                                        {
                                                        $percentage = $left_present/$days_total*100;
                                                        }
                                                        }
                                                        settype($percentage, "integer");
                                                        echo '<td>'.$percentage.'.0</td>';

                                                        echo '</tr>';

                                                        }			

                                           }

echo'	
       </tbody>
           </table>                
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 		
}


//end of manish functions
//function to send leave by student
function student_send_leave()
{

        echo '<div class="row-fluid">

                                <!-- Table widget -->
                            <div class="widget  span12 clearfix">

                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Send leave application </span>
                                </div><!-- End widget-header -->	

                                <div class="widget-content">

                                                                <form id="validation_demo" action="attendance/insert_leave_app.php" method="post">

<input type="hidden" value="'.$_SESSION['user_id'].'" name="user_id"/>
                                                                 <div class="section" >
                                    <label>Number of days for leave</label>   
                                   <div> <input type="text" name="no_days" class="validate[required]" onblur="show_for_date(this.value)"/></div>
                                           </div>



                                                                 <div class="section" id="on_date" style="display:none"><label>On?</label><div><input type="text"  class=" birthday  small " name="date"/></div></div>

                                                                 <div class="section" id="for_to_date" style="display:none">
                                                                 <label>From?</label><div><input type="text"  class=" birthday  small " name="from_date"/></div> 
                                                                 <label>To?</label><div><input type="text"   class=" birthday  small " name="to_date"/></div></div>


                                                                 <div class="section" >
                                    <label>Subject</label>   
                                   <div> <input type="text" name="sub_leave" class="validate[required]"placeholder="Subject"/></div>
                                           </div>

                                     <div class="section" >
                                    <label>Leave description</label>   
                                   <div><textarea name="des_leave" placeholder="Brief application"></textarea></div>
                                           </div>
                                                                                    <div class="section last">

                                                                                    <div>
                                                <button class="btn submit_form" type="submit">Send</button>
                                                <a class="btn" onClick="ResetForm()" title="Reset  Form">Clear Form</a>
                                             </div>
                                             </div>



                                                                </form>

                                                                </div>
                                                                </div>
                                                                </div>

                                                                ';


}
//show leave applications to the class teacher
function show_leave_to_teacher()
{
        echo '<div class="row-fluid">

                                <!-- Table widget -->
                            <div class="widget  span12 clearfix">

                                <div class="widget-header">
                                    <span><i class="icon-home"></i>Leave applications </span>
                                </div><!-- End widget-header -->	

                                <div class="widget-content">
                                                                  <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead>
                                            <tr>
                                                                                        <th>Student Name</th>
                                                                                        <th>Subject</th>
                                                                                        <th>Application</th>
                                                                                        <th>Leave for(days)</th>
                                                                                        <th>From</th>
                                                                                        <th>To</th>
                                                                                        <th>On</th>
                                                                                        <th>Action</th>
                                                                                        </tr>
                                                                                        <tbody align="center">
';

                                                                 $get_applications=
                                                                "SELECT * 
                                                                FROM leave_application
                                                                WHERE pending = 1";
                                                                $exe_application_query=mysql_query($get_applications);
                                                                while($fetch_applications=mysql_fetch_array($exe_application_query))
                                                                {
                                                                        //get name of the student acc to that id
                                                                        $get_student_name=
                                                                        "SELECT Name
                                                                        FROM student_user
                                                                        WHERE sId=".$fetch_applications[1]."";
                                                                        $exe_name=mysql_query($get_student_name);
                                                                        $fetch_name_student=mysql_fetch_array($exe_name);
                                                                        echo '<tr>
                                                                        <td>'.$fetch_name_student[0].'</td>
                                                                        <td>'.$fetch_applications[2].'</td>
                                                                        <td><textarea disabled="disabled">'.$fetch_applications[3].'</textarea></td>
                                                                        <td>'.$fetch_applications[4].'</td>
                                                                        <td>
                                                                        ';
                                                                        if($fetch_applications[5] == '0000-00-00')
                                                                        {
                                                                        echo "";	

                                                                        }
                                                                        else
                                                                        {
                                                                        echo $fetch_applications[5];
                                                                        }

                                                                        echo '

                                                                        </td>
                                                                        <td>';
                                                                        if($fetch_applications[6] == '0000-00-00')
                                                                        {
                                                                        echo "";	

                                                                        }
                                                                        else
                                                                        {
                                                                        echo $fetch_applications[6];
                                                                        }
                                                                        echo '
                                                                        </td>
                                                                        <td>';
                                                                        if($fetch_applications[7] == '0000-00-00')
                                                                        {
                                                                        echo "";	

                                                                        }
                                                                        else
                                                                        {
                                                                        echo $fetch_applications[7];
                                                                        }
                                                                        echo '
                                                                        </td>
                                                                        <td id="'.$fetch_applications[0].'"><button class="uibutton submit" onclick="approve_app('.$fetch_applications[0].',1)">approve</button>
                                                                            <button class="uibutton special" onclick="approve_app('.$fetch_applications[0].',0)">Deny</button></td>
                                                                        </tr>
                                                                        ';


                                                                }




                                                                echo '
                                                                </tbody>
                                                                </table>

                                                                </div>
                                                                </div>
                                                                </div>';


}
//function to view leave status
function student_view_leave_status()
{
        echo '<div class="row-fluid">

                                <!-- Table widget -->
                            <div class="widget  span12 clearfix">

                                <div class="widget-header">
                                    <span><i class="icon-home"></i>'.$_SESSION['name'].'( Applications status)</span>
                                </div><!-- End widget-header -->	

                                <div class="widget-content">
                                                                  <table  class="table table-bordered table-striped" id="dataTable" >
                                        <thead align="center">
                                            <tr>

                                                                                        <th>Subject</th>
                                                                                        <th>Application</th>
                                                                                        <th>Leave for(days)</th>
                                                                                        <th>Status</th>
                                                                                        </tr>
                                                                                        <tbody align="center">';
//query to get leave applications which are approved or denied
$get_leave_status=
"SELECT *
FROM leave_application
WHERE sId=".$_SESSION['user_id']."
AND pending=0";
$exe_leave_app=mysql_query($get_leave_status);
while($fetch_leaves_student=mysql_fetch_array($exe_leave_app))
{

echo '<tr>
<td>'.$fetch_leaves_student['subject'].'</td>
<td>'.$fetch_leaves_student['description'].'</td>
<td>'.$fetch_leaves_student['num_days'].'</td>
<td>
';

if($fetch_leaves_student['approved/disapproved']==1)
{
echo '<b style="color:green">Application Approved</b>';	

}
else
{
echo '<b style="color:red">Application Denied</b>';		
}
echo '
</td>
</tr>
';



}
echo '</tbody>
</table>
</div>
</div>
</div>';


}
function student_view_month_att()
{



echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select dates</span>
      </div><!-- End widget-header -->
      <div class="widget-content">

<br>
<br>
<br>
        '; 
        echo '
<form>
<div class="section">
<label>FROM</label>
<div>
<input type="tel" class="birthday" id="from_date"/>
</div>
</div>
<div class="section">
<label>To</label>
<div>
<input type="tel" class="birthday" id="to_date"/>
</div>
</div>

<div class="section last">
<div>
<button class="uibutton submit" type="button" onclick="show_data_dates_parent_stu()">Go</button>
</div>
</div>
</form>	
<div id="show_data_for_dates_p_s"></div>

        ';

        echo '</div>
        </div>
        </div>';

}
function attendance_archive_student()
{
        $month=$_GET['month'];
        $student_id = $_SESSION['user_id'];	

        //Query to get the class of the user
        $query_class_student = "
                SELECT class_index.class_name
                FROM class_index

                INNER JOIN class
                ON class.classId = class_index.cId

                WHERE class.sId = $student_id
        ";

        $execute_class_student = mysql_query($query_class_student);
        $class_student = mysql_fetch_array($execute_class_student);
        $class_name = $class_student[0];

        //Query to get the attendance details of the student
        $query_get_attendance_details = "
                SELECT day(dates_d.date), month(dates_d.date), year(dates_d.date), attendance_date_comment.comment
                FROM dates_d

                INNER JOIN attendance_date_comment
                ON attendance_date_comment.dId = dates_d.date_id

                WHERE attendance_date_comment.sId = $student_id

                AND dates_d.month = '$month'

                ORDER BY attendance_date_comment.dId DESC
        ";
        $execute_get_attendance_details = mysql_query($query_get_attendance_details);

        echo '
         <div class="row-fluid">

                                <!-- Table widget -->
                            <div class="widget  span12 clearfix">

                                <div class="widget-header">
                                    <span><i class="icon-bookmark"></i>Attendance Status (For <b>'.$month.'</b>)</span>
                                </div><!-- End widget-header -->	

                                <div class="widget-content">
                                    <table  class="table table-bordered table-striped" id="dataTable" >


                                <h5 style="color:grey" align="center">Student : '.$_SESSION['name'].'</h5>
                                <h5 style="color:grey" align="center">Class : '.$class_name.'</h5>							


                                        <thead align="center">
                                                <tr>


                                                        <th scope="col">S.no</th>
                                                        <th scope="col">Date</th>
                                                        <th scope="col">Reason given</th>
                                                </tr>
                                        </thead>

                                        <tbody align="center">
                                        '; 
                                        $sno = 0;
                                        while($get_attendance_details = mysql_fetch_array($execute_get_attendance_details))
                                        {
                                                echo '<tr>';
                                                //Query to display all the attendance details

                                                echo '<td align="center">'.++$sno.'</td>';
                                                echo '<td align="center">'.$get_attendance_details[0].'/'.$get_attendance_details[1].'/'.$get_attendance_details[2].'</td>';
                                                echo '<td align="center">'.$get_attendance_details[3].'</td>';

                                                echo '</tr>';

                                        }


                                        echo'
                                        </tbody>
                                 </table>
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div><!-- row-fluid -->





                        ';
}
//function to teacher attendence
 function admin_teacher_attendance()
{

echo '<div class="row-fluid">

                            <div class="span12  widget clearfix">

                                <div class="widget-header">
                                    <span><i class="icon-align-center"></i> Attendance</span>
                                </div><!-- End widget-header -->	

                                <div class="widget-content"><br />
                                    <div class="row-fluid">

                                         <div class="span4">
 <a href="teacher_attendance_monthly.php">       <div class="shoutcutBox"> <span class="ico color chat-exclamation"></span> <em><b style="color:green">Month wise</b></em> </div>
<a href="teacher_attendance_weekly.php">   <div class="shoutcutBox"> <span class="ico color chat-exclamation"></span> <em><b style="color:green">Week wise</b></em> </div></a>
 <a href="teacher_attendance_select_date.php">   <div class="shoutcutBox"> <span class="ico color chat-exclamation"></span> <em><b style="color:green">Select date</b></em> </div></a>


                                          </div><!-- span4 column-->



                                    </div><!-- row-fluid column-->
                                </div><!--  end widget-content -->
                            </div><!-- widget  span12 clearfix-->

                    </div><!-- row-fluid -->
';			

}



//function to attendance_class_month

function admin_teacher_attendance_class_month()
{   	        echo '<div class="row-fluid">
                                <div class="span12  widget clearfix">
                                <div class="widget-header">
                                 <span><i class="icon-align-center"></i> Attendance</span>
                              </div><!-- End widget-header -->	
                              <div class="widget-content"><br />
                                  <div class="row-fluid">
                                  <div class="section">

                                       <form action="admin_attendance_select_class.php" method="get" >

                                            <label>select Month<small></label>   
                                            <div>

                                             <select data-placeholder="Choose a Month..." class="chzn-select" tabindex="2" name="month">        
                                         <option value="january">january</option>
                                                                                <option value="february">feburary</option>
                                                                                <option value="march">march</option>
                                                                                <option value="april">april</option>
                                                                                <option value="may">may</option>
                                                                                <option value="june">june</option>
                                                                                <option value="july">july</option>
                                                                                <option value="august">august</option>
                                                                                <option value="september">september</option>
                                                                                <option value="october">october</option>
                                                                                <option value="november">november</option>
                                                                                <option value="december">december</option> 
</select>       
</div>
</div>
<br></br>
<br></br>
   <div class="section last">
<button class="uibutton submit_form">Go</button>
    </div>
      </div>
</form>
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->

</div><!-- row-fluid -->
';										

}

//function to select class for selected month
function admin_attendance_select_class()
{
        $month=$_GET['month'];
        echo '<div class="row-fluid">
        <div class="span12  widget clearfix">
          <div class="widget-header">
          <span><i class="icon-align-center"></i> Select Class</span>
          </div><!-- End widget-header -->	
          <div class="widget-content"><br />
         <div class="row-fluid"> 
        ';
        $teacher_id = $_SESSION['user_id'];
        //Query to get the class of the teacher
          $select_uid="SELECT DISTINCT`class`
                      FROM `teachers`
                                  WHERE `tId`=$teacher_id";
                                  $exe_teacher_id=mysql_query($select_uid);
                                  while($fetch_class=mysql_fetch_array( $exe_teacher_id))
                                  { 
                                         $select_classid="SELECT cId 
                         FROM class_index
                                         WHERE class_name='".$fetch_class['class']."' ";

                                         $exe_class_id=mysql_query($select_classid);
                                        $fetch_class_id=mysql_fetch_array($exe_class_id);

                                          //query to get class id acc to class name

echo '<a class="uibutton large" href="admin_attendance_for_teacher_date.php?month='.$month.'&&classId='.$fetch_class_id['cId'].'" onclick="">'.$fetch_class['class'].'</a>'; 
                                  }
        echo'
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
';										
}



//function to select class for selected date
 function admin_attendance_for_teacher_date()
 {
        //Function to generate list of students in the particular class for the present date.
        //Class id to be taken from GET

        $classId = $_GET['classId'];	
        $selected_month = $_GET['month'];
        $no=1;
        $queryClassName = "
        SELECT `class_name`
        FROM `class_index`
        WHERE `cId` = '$classId'
        ";
        $getClassName = mysql_query($queryClassName);
        $className = mysql_fetch_array($getClassName);
        $className = $className[0];		//Name of the class

        //Query to get the name of students and there SID for attendance
        $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$_GET['classId']."'
                AND session_id='".$_SESSION['current_session_id']."'";
        $exe = mysql_query($query);	 
        echo '<div class="row-fluid">

           <div class="span12  widget clearfix">

                <div class="widget-header">
                        <span><i class="icon-align-center"></i> Attendance</span>
                </div><!-- End widget-header -->	

                <div class="widget-content"><br />

        <table  class="table table-bordered table-striped" id="dataTable" >
                <thead>
                   <tr>
                        <th>S.No</th>
                        <th>Name</th>
                        <th>Admission No</th>
                        <th>Action</th>					
                   </tr>
                   </thead>	   	
        <tbody align="center">

'; while($fe=mysql_fetch_array($exe)){
   echo'
  <tr id="txtboxes_view_return">
          <td class="align-center">'.$no.'</td></td>
          <td class="align-center"> '.$fe['Name'].'</td>
          <td class="align-center">'.$fe['admission_no'].' </td>';

                                                        $sql="SELECT Id
                                                        FROM attendance_date_comment 
                                                        WHERE sId='".$fe['sId']."'";

                        $query_exe=mysql_query($sql);
                        $query_fetch=mysql_fetch_array($query_exe);

echo' <td class="align-center" id="'.$fe['sId'].'"><a href="admin_show_chart.php?sid='.$fe["sId"].'&month='.$selected_month.'" style="cursor:pointer">View attendance</a>
                                                ';
                                                echo'

                                                </tr>
                                                ';++$no; }echo'
                                        </tbody>

                     </table>
                                        </div><!-- row-fluid column-->
                                    </div><!--  end widget-content -->
                                </div><!-- widget  span12 clearfix-->

                         </div><!-- row-fluid -->
';										




 }

//function to show chart to teacher 
function admin_show_chart()
 {
         $student_id=$_GET['sid'];
        $month=$_GET['month'];


                //Get details of the absentees with the date
            $query_get_student_details_for_attendance = "
                SELECT dates_d.date_id
                FROM dates_d

                INNER JOIN working_days
                ON working_days.Date_working_id = dates_d.date_id

                INNER JOIN attendance_date_comment
                ON attendance_date_comment.dId = dates_d.date_id

                WHERE attendance_date_comment.sId = $student_id AND dates_d.month='$month'
        ";

        $execute_absent_no_days = mysql_query($query_get_student_details_for_attendance);

        $absent_student_date_id = array();
        $ctr_date_id_absent = 0;

        while($get_student_details_for_attendance = mysql_fetch_array($execute_absent_no_days))
        {
                //We have the list of date_id when student was absent.
        $absent_student_date_id[$ctr_date_id_absent] = $get_student_details_for_attendance[0];
                ++$ctr_date_id_absent;

        }


        //Now we have the date_id of the current Month
                 $query_get_month_days_id = "
                        SELECT dates_d.date_id, dates_d.day_of_week
                        FROM dates_d, session_table
                        WHERE dates_d.date BETWEEN session_start_date AND session_end_date AND sId=".$_SESSION['current_session_id']." AND month='".$month."'
                ";

        $execute_get_month_days_id = mysql_query($query_get_month_days_id);

        $dates_id_month_array = array();
        $day_of_week = array();
        $ctr_dates_of_month = 0;

        while($get_month_days_id = mysql_fetch_array($execute_get_month_days_id))
        {
                //Get all the dateIds of the month

          $dates_id_month_array[$ctr_dates_of_month] = $get_month_days_id[0];		//All date Id's of the month
          $day_of_week[$ctr_dates_of_month] = $get_month_days_id[1];		//Day of week details of each date

                ++$ctr_dates_of_month;                     //Total no. of days of that month
         $strt_id_absent=$dates_id_month_array[0];
        /* $end_id_absent=$dates_id_month_array[$ctr_dates_of_month];*/

        }
        // to know the no of days(total no of days in the month)
        $no_of_days=$ctr_dates_of_month;  

        //to know end id of the foillowing month
        $end_id_absent=$strt_id_absent+$no_of_days-1;


        $array_value_day=array();
 for($q=0;$q<$ctr_date_id_absent;$q++)
         {
                 //To get day of week where date id and absent id are equal
           $sql_day_value_select="
          SELECT day_of_week,date_id

         FROM dates_d 

         WHERE date_id=".$absent_student_date_id[$q]."";

         $exe_sql_day_value=mysql_query($sql_day_value_select);
         $fetch_value_day=mysql_fetch_array($exe_sql_day_value);
          $array_value_day[$q]=$fetch_value_day['day_of_week']."&nbsp;";
          $array_value_d_id[$q]=$fetch_value_day['date_id']."<br>";

        }

        //Create a 2 dimensional array to store the days week wise

        $days_week_wise_array = array();
        $ctr_week = 0;
        $ctr_day = 0;
        while($ctr_dates_of_month)
        {

                //iterate the loop and take all the days and store them in each week in a 2 dimensional array[week][day]
                if($day_of_week[$ctr_day] == '7')
                {
                        $days_week_wise_array[$ctr_week][$ctr_day] = $day_of_week[$ctr_day];

                        ++$ctr_week;

                }

                else
                {
                        $days_week_wise_array[$ctr_week][$ctr_day] = $day_of_week[$ctr_day];

                }

                ++$ctr_day;

                --$ctr_dates_of_month;
        }
        $sql_weekend="SELECT COUNT(dates_d.day) 
FROM `dates_d`,`session_table` 
WHERE dates_d.date BETWEEN session_start_date AND session_end_date AND sId=".$_SESSION['current_session_id']." AND month='".$month."'";
$exe_weekend=mysql_query($sql_weekend);
$fetch_weekends=mysql_fetch_array($exe_weekend);
$weekends=$fetch_weekends[0];
$d = array();// it is used to cont no. of days in that week
$e=array();
for($i=0;$i<=$ctr_week;$i++)
{
        $d[$i]=count($days_week_wise_array[$i],1);


// we have no of days in that week

/*	
$sql_query="SELECT date_id 
           FROM dates_d WHERE d";*/
}
//get the number of days of the week
$get_days_absent=
"SELECT COUNT(sId)
FROM attendance_date_comment
INNER JOIN dates_d 
ON dates_d.date_id = attendance_date_comment.dId
WHERE month= '".$month."'
";
$exe_days_absent=mysql_query($get_days_absent);
  echo '<div class="row-fluid">

           <div class="span12  widget clearfix">

                <div class="widget-header">
                <span><i class="icon-align-center"></i> Attendance</span>
                </div><!-- End widget-header -->	
                <div class="widget-content"><br />



        ';
        echo '<h2>'.$_GET['month'].'</h2>';

        echo " 
         <script type='text/javascript'>

$(function ()
 {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'column',
                margin: [ 50, 50, 50, 50]
            },
            title: {
                text: 'attendance of the following month'
            },

            xAxis: {
                categories: [";
                                for($i=0;$i<=$ctr_week;$i++)
                                {
                                        $j = $i+1;
                                        echo '\'week'.$j.'('.$d[$i].'days)\'';
                                        if($ctr_week == $i)
                                        {

                                        }
                                        else
                                        {
                                                echo ',';
                                        }
                                }


//'week(".$d." days)'
       echo"],
                labels: {
                    rotation: -10,
                    align: 'right',
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif,bold'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ' (days)'
                }
            },
            legend: {
                enabled: false
            },

            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        'present : '+ Highcharts.numberFormat(this.y, 1) +
                        ' days';

                            }

            },
                series: [{
                name: 'Population',
                data: [
                        1,2,3,4,5


                                ],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    x: -3,
                    y: 10,
                    formatter: function() {
                        return this.y;
                    },
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    });

});
   </script>"; echo '
<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                        </div><!-- row-fluid column-->
                          </div><!--  end widget-content -->
                         </div><!-- widget  span12 clearfix-->
                         </div><!-- row-fluid -->
';										
         }
 function admin_manager_deleted_books()
{

         echo '<div class="row-fluid">
           <div class="span12  widget clearfix">
           <div class="widget-header">
                <span><i class="icon-align-center"></i> Attendance</span>
                </div><!-- End widget-header -->	
                <div class="widget-content"><br />                                                                      	 
        <table  class="table table-bordered table-striped" id="dataTable" >
                <thead>
                   <tr>
                        <th>Book Id</th>
                        <th>Title</th>
                        <th>Author</th>
                        <th>ISBN</th>
                        <th>Publisher</th>		
                        <th>Copies in Library</th>		
                        <th>Copies Deleted</th>		
                        <th>Date and Time</th>							
                   </tr>
                   </thead>	   	
        <tbody align="center">
        </table>								     
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->
     ';
}
 //admin attendance today 
  function admin_attendance_today()
 {
echo '<div class="row-fluid">
 <div class="span12  widget clearfix">
<div><!--  end widget-content -->	
 <div class="widget-header">
  <span><i class="icon-align-center"></i>Select class for marking attendance</span>
  </div><!-- End widget-header -->
  
  <a href="admin_attendance_roll_no.php" ><button class="uibutton normal">Roll No. Wise</button></a>
  
  <a href="admin_attendance_admission_no.php" ><button class="uibutton normal">Admission No. Wise</button></a>
  	
  '; 
        $time_offset ="525"; // Change this to your time zone
        $time_a = ($time_offset * 120);
        $today = date("jS F Y");
        echo "<h2>"; 
        echo '<h5 style="color:grey" align="center"> '.$today.'</h5>';
        echo "</h2>";
$disp='';
 $q="SELECT `cId`,`class_name` FROM class_index    ORDER BY class+0, section  ASC ";
$q_res=mysql_query($q);
echo "<br>";
 echo '<ol class="rounded-list">';
while($res=mysql_fetch_array($q_res))
{
   echo '<tr class="row-fluid" id="added_rows">
             <td><div class="row-fluid">
          <div class="span6">							                                                              
      <li><a href="admin_attendance_for_class.php?class_id='.$res['cId'].'">'.$res[1].'</a>                                                                    	                                 
        </div>
        </div><!-- end row-fluid  -->
</td>
</tr>';	 	 
}
echo'
</ol>	                            
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 
}

//Attendance roll No wise
function admin_attendance_roll_no()
{
	echo '<div class="row-fluid">
 <div class="span12  widget clearfix">
<div><!--  end widget-content -->	
 <div class="widget-header">
  <span><i class="icon-align-center"></i>Select class for marking attendance</span>
  </div><!-- End widget-header -->
  
  <a href="admin_attendance_roll_no.php" ><button class="uibutton normal">Roll No. Wise</button></a>
  
  <a href="admin_attendance_admission_no.php" ><button class="uibutton normal">Admission No. Wise</button></a>
  	
  '; 
        $date_today = date("Y-m-d");
        $time_offset ="525"; // Change this to your time zone
        $time_a = ($time_offset * 120);
        $today = date("jS F Y");
        echo "<h2>"; 
        echo '<h5 style="color:grey" align="center"> '.$today.'</h5>';
        echo "</h2>";
$disp='';
 $q="SELECT `cId`,`class_name` FROM class_index    ORDER BY class+0, section  ASC ";
$q_res=mysql_query($q);
echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><strong>Please Enter Roll No. Seperated by comma's</strong></span><br>";
 echo '
     <input type="hidden" value="'.$today.','.$date_today.'" id="date_attendance" />
 <table width="100%" align="center">
 ';
while($res=mysql_fetch_array($q_res))
{
   echo '
   <tr>
   <td></td>
   <td></td>
   <td></td>
   <td></td>
   	<td><strong>'.$res[1].'</strong></td>
	<td><input type="text" id="attendance'.$res[0].'" /></td>
        <td id="absent'.$res[0].'"><button class="uibutton normal" onclick="mark_absent_roll_no('.$res[0].');" >Absent</button></td>
        <td><button class="uibutton normal" onclick="mark_leave_roll_no('.$res[0].');">Leave</button></td>
   
   </tr>   
';	 	 
}
echo'
</table>
<a href="admin_attendance_admission_no.php" ><button class="uibutton normal">Submit</button></a>
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 
	
}

//Function attendance admission no wise
function admin_attendance_admission_no()
{
echo '
                <div class="row-fluid">

                        <!-- Widget -->
                    <div class="widget  span12 clearfix">

                        <div class="widget-header">
                            <span><i class="icon-align-left"></i> Admission No Wise</span>
                        </div><!-- End widget-header -->	

                        <div class="widget-content">
                            <!-- title box -->
                                                                ';

$date_today = date("Y-m-d");
        $time_offset ="525"; // Change this to your time zone
        $time_a = ($time_offset * 120);
        $today = date("jS F Y");
        echo "<h2>"; 
        echo '<h5 style="color:grey" align="center"> '.$today.'</h5>';
        echo "</h2>";
$disp='';
 $q="SELECT `cId`,`class_name` FROM class_index    ORDER BY class+0, section  ASC ";
$q_res=mysql_query($q);
echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><strong>Please Enter Admission No. Seperated by comma's</strong></span><br>";
 echo '
     <input type="hidden" value="'.$today.','.$date_today.'" id="date_attendance" />';

																echo '

  
                                     <div class="section">
                                         
               <div> <textarea name="message_school" class="validate[required] large" id="attendance1"  cols="" rows="">
               </textarea></div>   
                                     </div>

                                    <div class="section last">
                                        <div><button class="uibutton normal" onclick="mark_absent_admission_no(1)" id="absent1">Mark Absent</button> 
                                      </div>
                                   </div>
                                
                        </div><!--  end widget-content -->
                    </div><!-- widget  span12 clearfix-->

            </div><!-- row-fluid -->

';
}

 //function  attendance for class 
function attendance_for_class() 
{
echo '<iframe id="I1" style="display:none" ></iframe>';		 
echo '<div class="row-fluid">
 <div class="span12  widget clearfix">
 <div class="widget-header">
  <span><i class="icon-align-center"></i>Kindly select students to mark absent(AND SMS alert will be sent)</span>
  </div><!-- End widget-header -->
   <div class="widget-content">
 '; 
$name = $_SESSION['name'];

$id=$_GET['class_id'];
$time_offset ="525"; // Change this to your time zone
        $time_a = ($time_offset * 120);
        $today = date("jS F Y");
        echo "<h2>"; 
        echo '<h5 style="color:grey" align="center"> '.$today.'</h5>';



               echo "</h2>";
                            $q="SELECT * 
                              FROM class_index 
                WHERE cId='".$_GET['class_id']."'";
                $execute=mysql_query($q);
                $fetch=mysql_fetch_array($execute);
                $class_name_stu=$fetch['class_name'];

        echo '<h5 style="color:green" align="center">Class: '.$class_name_stu.'</h5>';
        //echo'<a href="attendance_mark_admission_no.php?class_id='.$id.'" class="uibutton confirm" >Mark absent</a> ';	
                $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$_GET['class_id']."'
                AND session_id='".$_SESSION['current_session_id']."'";
                $exe=mysql_query($query);

                $no=1;
$time_offset ="525"; // Change this to your time zone
$time_a = ($time_offset * 120);
$today_date = date("Y-m-d");

//Query to get the date id of the current date from `dates_d` table
$queryDateId = "
SELECT `date_id`
FROM `dates_d`
WHERE `date` = '$today_date'
";

$getDateId = mysql_query($queryDateId);
$dateId = mysql_fetch_array($getDateId);
$dateId = $dateId[0];

echo '
        <a href="mark_student_multiple_absent.php?class_id='.$id.'" class="uibutton normal">Mark multiple absent</a> 
        <table  class="table table-bordered table-striped" id="dataTable" >
                <thead>
                    <tr>
        <th>S.No</th>
         <th>Name</th>
           <th>Admission</th>
            <th>Action</th>							
</tr>
</thead>	   	
<tbody align="center">';		

                                                                while($fe=mysql_fetch_array($exe)){
                       echo'
                         <tr id="txtboxes_view_return">
          <td class="align-center">'.$no.'</td></td>
          <td class="align-center"> '.$fe['Name'].'</td>
          <td class="align-center">'.$fe['admission_no'].' </td>';
          $sql="SELECT Id 
          FROM attendance_date_comment 
          WHERE dId='".$dateId."' 
          AND sId='".$fe['sId']."'";

                                                                      $query_exe=mysql_query($sql);
                                                                      $query_fetch=mysql_fetch_array($query_exe);
          if(isset($query_fetch['Id']))
          {
                  echo '<td id="'.$fe['sId'].'" class="align-center"><a href="#" onclick="mark_present('.$fe['sId'].')">Mark present</a></td>';	
          }
          else 
          {
                  echo' <td class="align-center" id="'.$fe['sId'].'"><a style="cursor:pointer" onclick="mark_absent('.$fe['sId'].')">Absent</a>
  ';}
  echo'

  </tr>
  ';++$no; }echo'
</tbody>

  </table>   ';											

echo'	                         

</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 	

}
//function to mark multiple students absent
function mark_multiple_absent()
{
echo '<div class="row-fluid">

 <div class="span12  widget clearfix">

 <div class="widget-header">
  <span><i class="icon-align-center"></i>Kindly select students to mark absent(AND SMS alert will be sent)</span>
  </div><!-- End widget-header -->
   <div class="widget-content">


        '; 

$name = $_SESSION['name'];

$id=$_GET['class_id'];
$time_offset ="525"; // Change this to your time zone
        $time_a = ($time_offset * 120);
        $today = date("jS F Y");
        echo "<h2>"; 
        echo '<h5 style="color:grey" align="center"> '.$today.'</h5>';


                $q="SELECT * 
                FROM class_index 
                WHERE cId='".$_GET['class_id']."'";
                $execute=mysql_query($q);
                $fetch=mysql_fetch_array($execute);
                $class_name_stu=$fetch['class_name'];

        echo '<h5 style="color:green" align="center">Class: '.$class_name_stu.'</h5>';

        echo "</h2>";
        $q="SELECT * 
            FROM class_index 
                WHERE cId='".$_GET['class_id']."'";
                $execute=mysql_query($q);
                $fetch=mysql_fetch_array($execute);


                $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
                INNER JOIN class 
                ON class.sId=student_user.sId
                WHERE class.classId='".$_GET['class_id']."'
                AND session_id='".$_SESSION['current_session_id']."'";
                $exe=mysql_query($query);

                $no=1;
$time_offset ="525"; // Change this to your time zone
$time_a = ($time_offset * 120);
$today_date = date("Y-m-d");

//Query to get the date id of the current date from `dates_d` table
$queryDateId = "
SELECT `date_id`
FROM `dates_d`
WHERE `date` = '$today_date'
";

$getDateId = mysql_query($queryDateId);
$dateId = mysql_fetch_array($getDateId);
$dateId = $dateId[0];
$id='';
echo '

<form action="attendance/mark_absent_multiple_today.php" method="post">
<table  class="table table-bordered table-striped" id="dataTable" >
<thead>
   <tr>
  <th>S.No</th>
   <th>Name</th>
   <th>Admission</th>
  <th>Uncheck students who are present</th>							
   </tr>
   </thead>	   	
<tbody align="center">';
while($fe=mysql_fetch_array($exe)){
                        echo'
                       <tr id="txtboxes_view_return">
                               <td class="align-center">'.$no.'</td></td>
                               <td class="align-center"> '.$fe['Name'].'</td>
                               <td class="align-center">'.$fe['admission_no'].' </td>';
                               $sql="SELECT Id 
                               FROM attendance_date_comment 
                               WHERE dId='".$dateId."' 
                               AND sId='".$fe['sId']."'";

$query_exe=mysql_query($sql);
$query_fetch=mysql_fetch_array($query_exe);
                               if(isset($query_fetch['Id']))
                               {
                                       echo '<td id="'.$fe['sId'].'" class="align-center"><a href="#" onclick="mark_present('.$fe['sId'].')">Mark present</a></td>';	
                               }
                               else 
                               {
                                       echo' <td class="align-center" id="'.$fe['sId'].'"><input type="checkbox" name="stu_'.$fe['sId'].'" checked="checked"/>
                       ';}
                       echo'
                       </tr>
                       ';$no++; 
                       $id.=$fe['sId']." ";
                       }echo'
               </tbody>
                </table>
                <input type="hidden" value="'.$id.'" name="id"/>
                <input type="hidden" value="'.$no.'" name="max_val"/>
                <input type="hidden" value="'.$id.'" name="class_id"/>
                <button type="submit" class="uibutton submit">GO</button>  
                </form> ';											

echo'	                         

</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 	


}

//function to select date
 function admin_select_date()
 {
echo '<div class="row-fluid">
        <!-- Widget -->
        <div class="widget  span12 clearfix">
                <div class="widget-header">
               <span><i class="icon-home"></i>Select Date</span>
                </div><!-- End widget-header -->	
                <div class="widget-content">

        ';
         echo'
         <table>
         <tr>
         <td>
           <b>Select Date</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	 
        </td>
        <td align=""><input type="text"  class=" birthday  small" name="datePicker" id="datePicker" onchange="date_selected();" required="required"/>
        </td>
        </tr>
        </table>	
         ';
         echo'
    <div id="classes_container_div" style="display:none" class="dashboard_menu_wrapper">
     <ul class="dashboard_menu">
     <h4 style="color:grey" align="center">Kindly select the class</h4>

      ';
          $time_offset ="525"; // Change this to your time zone
        $time_a = ($time_offset * 120);
        $today = date("jS F Y");
   echo "<h2>"; 
  echo "</h2>";
$disp='';
$q="SELECT `cId`,`class_name` FROM class_index ORDER BY class+0 ASC,section ASC";
$q_res=mysql_query($q);
echo '<h2 align="center">';
echo '</h2>';
echo "<br>";
echo "<br>";
echo "<br>";
 echo '<ol class="rounded-list">';
while($res=mysql_fetch_array($q_res))
{
        echo '<tr class="row-fluid" id="added_rows">
      <td><div class="row-fluid">
          <div class="span6">							                                                              
      <li><a  onclick="redirect_for_selected_date_class('.$res[0].');">'.$res[1].'</a>                                                                    	                                 
        </div>
        </div><!-- end row-fluid  -->
</td>
</tr>';
  }
echo'	                         
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 	

   }
//function to archive select date	 

function attendance_archive_select_date()	 
         {

   // the class and students name will come where you can put the attendance for the particular student.
        $time_offset ="525"; // Change this to your time zone
        $time_a = ($time_offset * 120);
        $today = date("jS F Y");

                //Query to display all the classes
                $query = "
                                SELECT `cId`,`class_name`
                                FROM `class_index`
                                ORDER BY class+0 ASC,section ASC
                                ";
        $getClasses = mysql_query($query);

echo '<div class="row-fluid">
          <!-- Widget -->
        <div class="widget  span12 clearfix">
       <div class="widget-header">
        <span><i class="icon-home"></i>Select Date</span>
      </div><!-- End widget-header -->	
       <div class="widget-content">
        ';  
         echo'
         <table>
         <tr>
         <td>
           <b>Select Date</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	 
        </td>
        <td align=""><input type="text"  class=" birthday  small" name="datePicker" id="datePicker" onchange="date_selected();" required="required"/>
        </td>
        </tr>
        </table>						
        <div id="classes_container_div" style="display:none" class="dashboard_menu_wrapper">
                                        <ul class="dashboard_menu">

                                        <h5 style="color:grey" align="center">Kindly select the class</h5>
                                        <div class="span4">

                                        ';
                                        $sno = 0;

                                        //Blocks of class
                                        while($class = mysql_fetch_array($getClasses))
                                        {
                                                $cId = $class[0];
                                                $className = $class[1];
                                                ++$sno;
        echo'  
         <div class="shoutcutBox" onclick="redirect_for_selected_date_class_archive('.$cId.',\''.$className.'\');"> <span class="ico color chat-exclamation"><em style="color:green"><b>'.$className.'</em> </b></div>
       <div class="breaks"><span></span></div>
 ';
   }				 

echo'	                         
</div>
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 	
    }

        // view attendance  details

  function attendance_view_archive()
 {

         $date_in_format = $_GET['date'];
        $class_id = $_GET['classId'];
        $class_name = $_GET['class_name'];

        //Query to get the date_id
        $query_get_date_id = "
                SELECT date_id
                FROM dates_d
                WHERE date = '$date_in_format'
        ";
        $execute_get_date_id = mysql_query($query_get_date_id);
        $get_date_id = mysql_fetch_array($execute_get_date_id);
        $date_id = $get_date_id[0];


        //Query to get all the students of the class

        $query_get_details_students = "
                SELECT student_user.Name, student_user.sId
                FROM student_user

                INNER JOIN class
                ON class.sId = student_user.sId

                WHERE class.classId = $class_id
        ";

        $execute_get_details_students = mysql_query($query_get_details_students);



echo '<div class="row-fluid">

<!-- Widget -->
<div class="widget  span12 clearfix">

        <div class="widget-header">
                <span><i class="icon-table"></i>Select Date</span>
        </div><!-- End widget-header -->	
<div class="widget-content">
<h5 align="center">Class : '.$class_name.'</h5>
                        <h5 align="center">Date : '.$date_in_format.'</h5>									

<table  class="table table-bordered table-striped" id="dataTable" >
        <thead>
           <tr>
                <th>S.No</th>
                <th>Name</th>
                <th>Status</th>
                <th>Reason Given</th>

           </tr>
           </thead>	   	
<tbody align="center">


        ';  

         $sno = 0;
            while($get_details_students = mysql_fetch_array($execute_get_details_students))
            {
                    echo '<tr>';
                            echo '<td align="center">'.++$sno.'</td>';
                            echo '<td align="center">'.$get_details_students[0].'</td>';	//Display the name

                            //Query to get the attendance details of the student.

                            $query_get_attendance_details = "
                                    SELECT comment
                                    FROM attendance_date_comment
                                    WHERE dId = '$date_id' AND sId = '".$get_details_students[1]."'
                            ";
                            $execute_get_attendance_details = mysql_query($query_get_attendance_details);
                            if(mysql_num_rows($execute_get_attendance_details))
                            {
                                    echo '<td align="center"><p style="color:RED">ABSENT</p></td>';	//Attendance status is absent
                                    $get_attendance_details = mysql_fetch_array($execute_get_attendance_details);
                                    $attendance_details = $get_attendance_details[0];
                                    echo '<td align="center">'.$attendance_details.'</td>';
                            }
                            else
                            {
                                    echo '<td align="center"><p style="color:GREEN">Present</p></td>';	//Attendance status is Present
                                    echo '<td align="center">N/A</td>';
                            }

                    echo '</tr>';
            }










echo'	
</tbody>
</table>                         
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 	





         }



//manish functions
//function to call admin_attendance_weekly.php
function admin_attendance_weekly()
{

echo '<div class="row-fluid">	  
<div class="span12  widget clearfix"> 
<div class="widget-header">
<span><i class="icon-align-center"></i>Weekly Attendance</span>
</div><!-- End widget-header -->
<div class="widget-content">
        '; 
        //query to get year and date id
          $get_visitor_year="SELECT DISTINCT `year`
                               FROM  `dates_d`
                                          ";
        echo' 
                <form action="admin_attendance_weekly_class.php" method="get" >
                <div class="section">
        <label>Select Year<small></small></label>           
                 <div >
                 <select  data-placeholder="Choose  Year..." class="chzn-select" tabindex="2" id="year" name="year" >        
                 <option value=""></option>'; 								
                        //query to get year		

        $execute_visitor_year=mysql_query($get_visitor_year);
        while($fetch_visitor_year=mysql_fetch_array($execute_visitor_year))//looping for getting year
              {   
                         $fetch_year=$fetch_visitor_year['year'];		

        echo'
                     <option value="'.$fetch_year.'">'. $fetch_year.'</option>';
                  }

         echo'  </select> 
                </div>
                   </div> 
                                   ';	
                                  $get_visitor_month="SELECT DISTINCT `month`,`month_of_year`
                                           FROM  `dates_d`
                                                       WHERE `year`=". $fetch_year." ";			
                        echo' 
                 <div class="section ">
         <label>Select Month<small></small></label>              
                 <div >
                 <select  data-placeholder="Choose  month..." class="chzn-select" tabindex="2" id="month" name="month">        
                            <option value=""></option>'; 								
                        //query to get month and month of year		

        $execute_visitor_month=mysql_query($get_visitor_month);
        while($fetch_visitor_month=mysql_fetch_array($execute_visitor_month))//looping for getting month
              {   

                         $fetch_month=$fetch_visitor_month['month'];
                         $fetch_month_year=$fetch_visitor_month['month_of_year'];

        echo'
                     <option value="'.$fetch_month_year.'">'. $fetch_month.'</option>';
                  }

echo'    </select> 
             </div>  
                 </div> 	  
<div class="section numericonly ">
  <label>FROM <small></small></label>   
  <div> 
  <input name="from_date" id="from_date" align="center" type="text"  class="medium" maxlength="2">
  </div>
  </div>
  <div class="section numericonly">
  <label>TO<small></small></label>   
  <div> 
   <input name="to_date" id="to_date" align="center" type="text" class="medium" maxlength="2">
  </div>
  </div> 
 <div class="section last">
 <div><button  class="uibutton submit_form" value="submit" >Go</button>
  </div>
  </div>		 
</form> 
'; 	 

        echo'	                   
</div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 		
}



//function to call admin_attendance_weekly_class
function admin_attendance_weekly_class()
{

        echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select Class</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
        '; 


                   /*	$year_name="SELECT DISTINCT`year`,`month`
                                              FROM `dates_d`
                                                          WHERE `month`=".$_GET['month']." AND`year`=".$_GET['year']."  ";
                                $exe_year_name=mysql_query($year_name);
                                $fetch_year_name=mysql_fetch_array($exe_year_name);


                                */
                                //query to get month name
                                $get_month_name="SELECT `month`
                                                 FROM `dates_d`
                                WHERE `month_of_year`=".$_GET['month']." ";
                                        $exe_month_name=mysql_query($get_month_name);
                                        $fetch_month=mysql_fetch_array($exe_month_name);
                                        $month_name=$fetch_month['month'];			 

  if(($_GET['year'] == "")||($_GET['month'] == ""))
 {

        header('location:admin_attendance_monthly.php');

  }
  else
                                  echo '<h5 style="color:green"align="center"><td >'.$_GET['year'].'</td></h5>';	

                                  echo '<h5 style="color:green"align="center"><td >'.$fetch_month['month'].'</td></h5>';		
                 $disp='';
                 $q="SELECT `cId`,`class_name` FROM class_index 
                              ORDER BY class+0 ASC,section ASC";
                 $q_res=mysql_query($q);
          echo' <div class="span4"><h3 style="color:">Kindly select class</h3></div>';
          echo '</h2>';
          echo "<br>";
          echo "<br>";
          echo "<br>";
           echo '<ol class="rounded-list">';
while($res=mysql_fetch_array($q_res))//looping for getting  class id
{	$class=$res['class_name'];
        echo '<tr class="row-fluid" id="added_rows">
          <td><div class="row-fluid">
          <div class="span6">							                                                              
  <li><a href="admin_attendance_view_weekly.php?year='.$_GET['year'].'&&month='.$_GET['month'].'&&from_date='.$_GET['from_date'].'&&to_date='.$_GET['to_date'].'&&class_id='.$res['cId'].'">'.$res['class_name'].'</a>                                                                    	                                 
           </div>
           </div><!-- end row-fluid  -->
       </td>
       </tr>';	 	 
}		 																
echo'	                       
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 			
        }
//function to call admin_attendance_view_weekly.php
function attendance_view_weekly()
{
        echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>View Attendance</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
        '; 
$class_id=$_GET['class_id'];
$get_year=$_GET['year'];
$month=$_GET['month'];
$from=$_GET['from_date'];
$to=$_GET['to_date'];
                  //query to get month name
                                $get_month_name="SELECT `month`
                                                 FROM `dates_d`
                                 WHERE `month_of_year`=".$_GET['month']." ";
                                        $exe_month_name=mysql_query($get_month_name);
                                        $fetch_month=mysql_fetch_array($exe_month_name);
                                        $month_name=$fetch_month['month'];			 

  if(($_GET['year'] == "")||($_GET['month'] == ""))
 {

        header('location:admin_attendance_weekly.php');

  }
  else
  //query to get class name
  $q="SELECT `cId`,`class_name` FROM class_index 
                             WHERE `cId`=".$_GET['class_id']."";
                 $q_res=mysql_query($q);
                         $res=mysql_fetch_array($q_res);
            $class=$res['class_name'];


 echo '<h5 style="color:green"align="center"><td >'.$_GET['year'].'</td></h5>';	
 echo '<h6 style="color:green"align="center"><td >'.$fetch_month['month'].' (from '.$_GET['from_date'].' to '.$_GET['to_date'].')</td></h6>'; 
echo ' <h5 style="color:green"align="left"><td >CLASS '.$class.'</td></h5>';		

//get all the date and extract years
                echo'
        <table  class="table table-bordered table-striped"  >
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>		
                        <th>Number of days absent</th>								
                    </tr>
                    </thead>	   	
        <tbody align="center"> 

        '; 	
//query to get details student adn class table
 $query_get_details_students = "
                                                        SELECT DISTINCT student_user.Name, student_user.sId
                                                        FROM student_user

                                                        INNER JOIN class
                                                        ON class.sId = student_user.sId

                                                        INNER JOIN attendance_date_comment
                                                        ON attendance_date_comment.sId = student_user.sId

                                                        WHERE class.classId = ".$_GET['class_id']." 
                                                ";
                               $execute_get_details_students = mysql_query($query_get_details_students);
                                           while($get_details_students = mysql_fetch_array($execute_get_details_students))
                                               {     

                                                   $sid=$get_details_students['sId'];

                                          //get date id on the date and year
                                        $get_date_id_from = 
                                          "SELECT date_id
                                          FROM dates_d
                                          WHERE date = '".$get_year."-".$month."-".$from."'";
                                          $exe_date_from_date=mysql_query($get_date_id_from);
                                          $fetch_date_id_from=mysql_fetch_array($exe_date_from_date);
                                          //for to date id
                                                $get_date_id_to = 
                                          "SELECT date_id
                                          FROM dates_d
                                          WHERE date = '".$get_year."-".$month."-".$to."'";
                                          $exe_date_to_date=mysql_query($get_date_id_to);
                                          $fetch_date_id_to=mysql_fetch_array($exe_date_to_date);
                                          //get students absent on the following date id's
                                          $get_list=
                                          "SELECT COUNT(sId)
                                          FROM attendance_date_comment
                                          WHERE dId BETWEEN '".$fetch_date_id_from[0]."' AND '".$fetch_date_id_to[0]."'
                                          AND sId = ".$sid."";
                                          $exe_get_list=mysql_query($get_list);
                                          $fetch_data_from_list=mysql_fetch_array($exe_get_list);


                                        /*  //query to get timestamp
                                            $get_day=
                                                  "SELECT `timestamp`
                                                  FROM   `attendance_date_comment`
                                                  ";
                                                  $exe_day=mysql_query($get_day);
                                                  while($fetch_day=mysql_fetch_array($exe_day))
                                                  {
                                                  $date= $fetch_day[0];
                                                  $date_array = explode("-",$date);
                                                  $day=$date_array[2];
                                                  $day_array_font=explode(" ",$day);
                                                  $split=$day_array_font[1];

echo $get_details=
"SELECT COUNT(sId)
FROM  `attendance_date_comment`
WHERE `timestamp` > '".$get_year."-".$month."-".$from." ".$split."' AND `timestamp` < '".$get_year."-".$month."-".$to." ".$split."'
AND `sId`=".$sid."";					   
$execute_attendance=mysql_query($get_details);
$fetch_sid=mysql_fetch_array($execute_attendance);	
                                                  */






                                  echo '<tr>
                                                           <td>'.$get_details_students['Name'].'</td>';							

                                                  if($fetch_data_from_list[0] > 0)
                                                  {
                                                  echo '<td><label style="color:red">Absent</label></td>';
                                                  }
                                                  else
                                                  {
                                                  echo '<td><label style="color:green">Present</label></td>';	
                                                  }
                                                  echo '<td>'.$fetch_data_from_list[0].'</td>';

                                                  echo '</tr>';
               }			


        //query to get sid from student_user	
                /*	 $get_details=
                                "SELECT COUNT(sId)
                                FROM attendance_date_comment
                                INNER JOIN dates_d
                                ON dates_d.date_id = attendance_date_comment.dId
                                WHERE dates_d.year=".$get_year."
                                AND dates_d.month_of_year=".$month."
                                AND attendance_date_comment.sId = ".$sid." ";
                                $exe_get_details=mysql_query($get_details);
                                while($fetch_get_details=mysql_fetch_array($exe_get_details))
                                                {						
                                                 echo '<tr>
                                                           <td>'.$get_details_students['Name'].'</td>
                                                             ';							
                                                        if($fetch_get_details[0] > 0)
                                                        {
                                                        echo '<td><label style="color:red">Absent</label></td>';
                                                        }
                                                        else
                                                        {
                                                        echo '<td><label style="color:green">Present</label></td>';	
                                                        }
                                                        echo '<td>'.$fetch_get_details[0].'</td>';

                                                    echo '</tr>';
                                                        }			*/



echo' </tbody>
         </table>   	                       
 </div><!-- row-fluid column-->
</div><!--  end widget-content -->
</div><!-- widget  span12 clearfix-->
</div><!-- row-fluid -->	
'; 	
}
//end of manish functions 



 //Author By manish 
//Priv Admin For teacher attendence

//function to called by admin_teacher_attendence_today.php
function admin_select_teacher()
{

        echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
        <div class="widget-header">
        <span><i class="icon-align-center"></i>Teachers Attendence</span>
        </div><!-- End widget-header -->		
        <div class="widget-content"><br />

    ';	
   //query to get teacher details
    $query_get_teachers_details = "
                SELECT users.Name, users.uId
                FROM users

                INNER JOIN `login`
                ON login.lId = users.uId

                WHERE login.privilege = 2

                ORDER BY users.Name ASC
                ";

        $execute_teachers_details = mysql_query($query_get_teachers_details);

        $time_offset ="525"; // Change this to your time zone
        $time_a = ($time_offset * 120);
        $today_date = date("Y-m-d");
        $date_format=date("jS F Y",strtotime($today_date));

        //Query to get the date id of the current date from `dates_d` table
        $queryDateId = "
                SELECT `date_id`
                FROM `dates_d`
                WHERE `date` ='".$today_date."'
                ";
        $getDateId = mysql_query($queryDateId);
        $dateId = mysql_fetch_array($getDateId);
        $dateId = $dateId[0];
        echo '<h5 style="color:green" align="center"> '.$date_format.'</h5>';	

        echo' 		                                                                         	 
        <table  class="table table-bordered table-striped" id="dataTable" >
        <thead align="center">
    <tr>		   

        <th><h5>Name</h5></th>	
        <th><h5>Action</h5></th>	
        <th><h5>Modify</h5></th>

    </tr>
    </thead>	 	
        <tbody align="center">';					   
        //Loop to generate the table
        $sno = 0;
        while($get_name_teachers = mysql_fetch_array($execute_teachers_details))
                {
                 $id=$get_name_teachers['1'];

                $sql="
                        SELECT id 
                        FROM `teacher_attendence_date` 
                        WHERE did='".$dateId."' 
                        AND tid='".$get_name_teachers[1]."'";
                $query_exe=mysql_query($sql);
                $query_fetch=mysql_fetch_array($query_exe);
                        echo '<tr>';

                        //Display the name of the teacher
                        echo '<td align="center"><b>'.$get_name_teachers[0].'</b></td>';		
                if(isset($query_fetch['id']))
                        {
                        echo '<td class="align-center" style="color:red">Teacher Absent</td>';	
                        }
                        else
                        {
                        echo '<td id="'.$id.'" class="align-center""><a  style="cursor:pointer" onclick="mark_teacher_absent('.$id.')">Mark  Absent</a></td>
                        ';
                        }
                        echo '
                        <td><a href="admin_show_alternate_teacher.php?id_teacher='.$id.'">View</a></td>
                        </tr>';
                }
                echo'	
                </tbody>
                </table>                         
                </div><!-- row-fluid column-->
                </div><!--  end widget-content -->
                </div><!-- widget  span12 clearfix-->
                </div><!-- row-fluid -->	
                '; 		

        }	


//functon to called by admin_view_teacher_attendence.php
function  admin_view_teacher_attendence()
{

        echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
        <div class="widget-header">
        <span><i class="icon-align-center"></i>View Teachers Attendence</span>
        </div><!-- End widget-header -->		
        <div class="widget-content"><br />
    ';
        $sno=0;	
        //query to get teacher details
    $query_get_teachers_details = "
                SELECT users.Name, users.uId
                FROM users

                INNER JOIN `login`
                ON login.lId = users.uId

                WHERE login.privilege = 2

                ORDER BY users.Name ASC
                ";

        $execute_teachers_details = mysql_query($query_get_teachers_details);
        echo' 		                                                                         	 
        <table  class="table table-bordered table-striped" id="dataTable" >
        <thead>
    <tr>		   
        <td ><h4 align="center" style="color:brown">Sno.</h5></td>
        <td><h4 align="center" style="color:brown">Name</h5></td>	
    </tr>
    </thead>	 	
        <tbody align="center">';				
        while($get_name_teachers = mysql_fetch_array($execute_teachers_details))
                {    
                     $sno++;
                     $id=$get_name_teachers['uId'];
                        echo '<tr>';
                        echo '<td><b>'.$sno.'</b></td>';
                        //Display the name of the teacher
                        echo '<td align="center"><a href="admin_view_teacher_absent_details.php?id='.$id.'">'.$get_name_teachers['Name'].'</a></td>
                        </tr>';	

                }
        echo'	
                </tbody>
                </table>                         
                </div><!-- row-fluid column-->
                </div><!--  end widget-content -->
                </div><!-- widget  span12 clearfix-->
                </div><!-- row-fluid -->	
                '; 		



}	

//function to called by admin_view_teacher_absent_details.php
function admin_view_teacher_absent_details()
{
echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
        <div class="widget-header">
        <span><i class="icon-align-center"></i>Absent Details</span>
        </div><!-- End widget-header -->		
        <div class="widget-content"><br/>';	

        echo' 		                                                                         	 
        <table  class="table table-bordered table-striped" id="dataTable" >
        <thead>
    <tr>		   
        <td ><h4 align="center" style="color:brown">Teacher Id.</h5></td>
        <td><h4 align="center" style="color:brown">Name</h5></td>
        <td><h4 align="center" style="color:brown">Status</h5></td>

        <td><h4 align="center" style="color:brown">Reasion Given</h5></td>	
        <td><h4 align="center" style="color:brown">Date</h5></td>	
    </tr>
    </thead>	 	
        <tbody align="center">';	

        //query to get absent details
        $get_absent_details="SELECT *
                             FROM teacher_attendence_date
                                                 WHERE tid=".$_GET['id']." ";
                $exe_absent_details=mysql_query($get_absent_details);
                while($fetch_absent_details=mysql_fetch_array($exe_absent_details))	 
                                {	
                                  $did=$fetch_absent_details['did'];
                                  $tid=$fetch_absent_details['tid'];
                                  $comment=$fetch_absent_details['comment'];


                        //query to get teacher details
    $query_get_teachers_details = "
                SELECT users.Name, users.uId
                FROM users

                INNER JOIN `login`
                ON login.lId = users.uId

                WHERE login.privilege = 2 AND users.uId=".$tid."
                ";

        $execute_teachers_details = mysql_query($query_get_teachers_details);	
        $get_name_teachers = mysql_fetch_array($execute_teachers_details);
        $name=$get_name_teachers[0];				 
        //Query to get the date id of the current date from `dates_d` table
        $queryDateId = "
                SELECT `date_id`,`date`
                FROM `dates_d`
                WHERE `date_id` ='".$did."'
                ";
        $getDateId = mysql_query($queryDateId);
        $dateId = mysql_fetch_array($getDateId);
        $date = $dateId[1];

                                echo'<tr>
                                <td>'.$tid.'</td>
                                <td>'.$name.'</td>
                                <td style="color:red">Absent</td>
                                <td>'.$comment.'</td>
                                <td>'.$date.'</td>
                                ';

                                }
 echo'	
                </tbody>
                </table>                         
                </div><!-- row-fluid column-->
                </div><!--  end widget-content -->
                </div><!-- widget  span12 clearfix-->
                </div><!-- row-fluid -->	
                '; 		
}


//end manish code
function return_day($day_code)
{
        //This function will return the day corresponding to the day Code passed as an argument
        switch($day_code)
        {
                case 0 : return 'Monday'; break;
                case 1 : return 'Tuesday'; break;
                case 2 : return 'Wednesday'; break;
                case 3 : return 'Thursday'; break;
                case 4 : return 'Friday'; break;
                case 5 : return 'Saturday'; break;		
        }

}

function admin_show_alternate_teacher()
{
        $id_teacher=$_GET['id_teacher'];

        //get class and name of teacher
        $get_details_teacher=
        "SELECT users.Name
        FROM users

        WHERE users.uId = ".$id_teacher."";
        $exe_details_teacher=mysql_query($get_details_teacher);
        $fetch_details_teacher=mysql_fetch_array($exe_details_teacher);
        $name_teacher=$fetch_details_teacher[0];

echo '<div class="row-fluid">                            
     <div class="span12  widget clearfix">                            
        <div class="widget-header">
        <span><i class="icon-align-center"></i>Set Arrangements</span>
        </div><!-- End widget-header -->		
        <div class="widget-content">
    <h5 style="color:orange">Time table of teacher:'.$name_teacher.'</h5>';	
//get list of all the teachers to select alternate
        //get class and period in a time table format of the teacher and get time tabel of the teacher selected on on change
        // show absent teacher time table
        $query_get_time_slot = "
                SELECT `time_slot_id`, `time_slot_name`
                FROM `time_slots`
        ";
        $execute_get_time_slot = mysql_query($query_get_time_slot);

        $day = 0;		//Set to zero as the week starts with Monday

        //Now Store all the time_slot ID and Name in an array
        $ctr_time_slot = 0;
        $array_time_slot_id = array();		//Array to store the time_slot_id
        $array_time_slot_name = array();	//Array to store the time_slot_name

        while($get_time_slot = mysql_fetch_array($execute_get_time_slot))
        {
                $array_time_slot_id[$ctr_time_slot] = $get_time_slot['time_slot_id'];
                $array_time_slot_name[$ctr_time_slot] = $get_time_slot['time_slot_name'];
                ++$ctr_time_slot;
        }
        echo '   <table  class="table table-bordered table-striped">


<thead>   
                            <tr>     
                                          <td>Day &darr;</td>';
                                          //Loop to get the time_slot_name in place
                                          for($i=0;$i<$ctr_time_slot;$i++)
                                          {
                                                  echo '<td>'.$array_time_slot_name[$i].'</td>';
                                          }
                                  echo'</tr>     
                                  </thead> ';

                                  echo'	<tbody>';
  //End of the header part

                  //Now loop to generate the days and also get the details of the subject and teacher

                  for($day=0;$day<=5;$day++)
                  {
                          //Loop will generate days, 0->Monday, 1-> Tuesday etc...
                          echo '<tr id=\"drag\">';

                                  echo '<td>'.return_day($day).'</td>';

          //Loop to get the details from the time_table table for the corresponding day, time_slot_id, class_id

                                  for($ctr = 0;$ctr < $ctr_time_slot; $ctr++)
                                  {
                                          $query_get_details_time_table = "
                                                  SELECT subject.subject, users.Name,users.uId,time_table.time_table_id,subject.subject_id,class_index.class_name,time_table.arrange_tId,time_table.arrange_subId
                                                  FROM subject

                                                  INNER JOIN time_table
                                                  ON time_table.subject_id = subject.subject_id

                                                  INNER JOIN users
                                                  ON users.uId = time_table.teacher_id

                                                  INNER JOIN class_index
                                                  ON class_index.cId = time_table.class_id

                                                   WHERE time_table.day = '$day' AND time_table.time_slot_id = '$array_time_slot_id[$ctr]' AND time_table.session_id = ".$_SESSION['current_session_id']."
                                                  AND time_table.teacher_id = ".$id_teacher."
                                          ";

                                          $execute_get_details_time_table = mysql_query($query_get_details_time_table);
                                          $get_details_time_table = mysql_fetch_array($execute_get_details_time_table);
                                          $subject = $get_details_time_table[0];

                                          $teacher = $get_details_time_table[1];
                                          $time_table_id = $get_details_time_table[3];
                                          $sid = $get_details_time_table[4];
                                          $tid=$get_details_time_table[2];
                                          $arr_id=$get_details_time_table['arrange_tId'];

                                          if(($tid == "")&&($sid == ""))
                                          {

                                                  echo "<td>-</td>";	
                                          }
                                          else
                                          {

                                          echo "<td id='val_".$day."_".$array_time_slot_id[$ctr]."' align=\"center\" onclick='edit_time_for_arrange(".$day.",".$array_time_slot_id[$ctr].",".$id_teacher.",".$sid.",".$time_table_id.",".$tid.");' style='cursor:pointer'><small><b><label style='color:green'>".$get_details_time_table['class_name']."-</label></b><br></small><strong>$subject</strong><br>($teacher)<br><b>Arrange teacher<br>(
                                          ";
                                          if($arr_id!=0)
                                          {
                                          //get arrange teacher name
                                          $get_arr_name=
                                          "SELECT Name 
                                          FROM users
                                          WHERE uId=".$arr_id."";
                                          $exe_arr_name=mysql_query($get_arr_name);
                                          $fetch_arr_name=mysql_fetch_array($exe_arr_name);	
                                          //fetch name of the sub
                                     $fetch_subject=
                                          "SELECT subject
                                          FROM subject
                                          WHERE subject_id=".$get_details_time_table['arrange_subId']."";
                                          $exe_sub=mysql_query($fetch_subject);
                                          $fetch_name_exe_sub=mysql_fetch_array($exe_sub);
                                          echo '<small style="color:red">'.$fetch_arr_name[0].'</small>
                                          <small style="color:green">('.$fetch_name_exe_sub[0].')</small>
                                          ';
                                          }
                                          else
                                          {

                                          }
                                          echo "

                                          )</b></td>";
                                          }

                                  }

                          echo '</tr>';
                  }

  echo'</tbody>	


                          </table>
                          <form>';
//fetch list of all the teachers
$get_list_teachers=
"SELECT *
FROM users
INNER JOIN login
WHERE login.lId = users.uId
AND login.privilege = 2
AND login.lId <> ".$id_teacher."";
$exe_list_teachers=mysql_query($get_list_teachers);
echo '
<div class="section">
<label>Select teacher<small>for arrangements</small></label>
<div>
<select class="chzn-select" onchange="show_alternate('.$id_teacher.');" id="arr_tea">
<option value="0">select</option>';
while($fetch_teachers_list=mysql_fetch_array($exe_list_teachers))
{

echo '<option value="'.$fetch_teachers_list[0].'">'.$fetch_teachers_list['Name'].'</option>';	

}
echo '
</select>
</div>
</div>
<input type="hidden" value="'.$id_teacher.'" id="teacher_id"/>';
echo '<span id="show_alternate_time"></span>
</form>';

        echo 
        '</div>
        </div>
        </div>';




}


 function  admin_attendance_admission_today()
 {


echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Attendance</span>
      </div><!-- End widget-header -->
      <div class="widget-content">';   

     $name = $_SESSION['name'];

$id=$_GET['class_id'];
$time_offset ="525"; // Change this to your time zone
        $time_a = ($time_offset * 120);
        $today = date("jS F Y");
        echo "<h2>"; 
        echo '<h5 style="color:grey" align="center"> '.$today.'</h5>';				
        echo "</h2>";
        $q="SELECT * 
            FROM class_index 
        WHERE cId='".$_GET['class_id']."'";
        $execute=mysql_query($q);
        $fetch=mysql_fetch_array($execute);
        $class_name_stu=$fetch['class_name'];

         echo '<h5 style="color:green" align="center">Class: '.$class_name_stu.'</h5>';
        $query="SELECT student_user.Name,student_user.admission_no,student_user.sId from student_user 
            INNER JOIN class 
            ON class.sId=student_user.sId
            WHERE class.classId='".$_GET['class_id']."'
            AND session_id='".$_SESSION['current_session_id']."'";
            $exe=mysql_query($query);
                         echo'	
        <form id="validation_demo"      action="attendance/student_admission_no_absent_mark.php"  method="get" enctype="multipart/form-data">

        <div class="section ">
        <label> Admission No.<small></small></label>   
        <div> 
        <input type="text" class="validate[required] medium" name="admission_no"  id="admission_no"  autocomplete="off"/>
        </div>
        </div>	
                            <div class="section ">
        <label> Absent Reasion.<small></small></label>   
        <div> 
        <input type="text" class="validate[required] medium" name="reasion"  id="reasion"  autocomplete="off"/>
        </div>
        </div>	
                          <div class="section last">	
        <button class="uibutton icon   " type="submit" align="center"  onclick="mark_admission_no_student(this.value);" >Mark Absent</button> 
                       </form>
                       <span id="show_admission_no">';
   echo'
   </div><!-- row-fluid column-->
   </div><!--  end widget-content -->
   </div><!-- widget  span12 clearfix-->
   </div><!-- row-fluid --> '; 		     





 }
//attendance download in excel date wise.........................................................................................................................................
 
 function admin_excel_select_dates()
 {
    
echo '<div class="row-fluid">			  
      <div class="span12  widget clearfix">
      <div class="widget-header">
      <span><i class="icon-align-center"></i>Select dates</span>
      </div><!-- End widget-header -->
      <div class="widget-content">
        '; 
        echo '
<form>
<div class="section">
<label>FROM</label>
<div>
<input type="tel" class="birthday" id="from_date"/>
</div>
</div>
<div class="section">
<label>To</label>
<div>
<input type="tel" class="birthday" id="to_date"/>
</div>
</div>

<div class="section last">
<div>
<button class="uibutton submit" type="button" onclick="show_excel_class()">Go</button>
</div>
</div>
</form>	
<div id="show_attendance_for_dates"></div>  ';
        echo '
        </div>
        </div>
        </div>';	
                 
 }
 
 
 
 
 
 
 
?>