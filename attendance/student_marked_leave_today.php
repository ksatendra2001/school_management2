<?php

/* While sending alerts check whether the user is an admin or a normal user as an Admin will mark the
	student absent for the whole day, whereas a teacher will mark him absent for a particular class. Send 
	alert message accordingly.
*/

require_once('../include/functions_dashboard.php');
require_once('../config/config.php');
require_once('../include/session.php');
require_once('../include/check.php');
require_once('../include/userdetail.php');
require_once('../sendsms/sendsms.php');

$studentId = $_GET['sid'];
$class_id=$_GET['cid'];
//echo $absentReason = $_GET['reason'];
//query to get student name acc to that id
/*echo $sql_query=
"SELECT `Name` ,`Guardian's Contact`
FROM `student_user`
WHERE `sId`=".$studentId."";
$exe_get_student_name=mysql_query($sql_query);
$fetch_name=mysql_fetch_array($exe_get_student_name);
$name_of_student=$fetch_name[0];
$number=$fetch_name[1];






$uname = 'username';
$pword = 'password';
$from = "mayoschool";
$mobile = $number;
$message = "Sir Your student ".$name_of_student." Is absent today";
$message = urlencode($message);

// Prepare data for POST request
$data = "uname=".$uname."&pword=".$pword."&message=".$message
."&from=". $from."&selectednums=".$selectednums."&info=".$info."&test=".$test."$date=".$today;*/

//Query to get the date id of the current date from `dates_d` table
//query to get student name acc to that id
$sql_query=
"SELECT `Name` ,`Phone No`
FROM `student_user`
WHERE `sId`=".$studentId."";
$exe_get_student_name=mysql_query($sql_query);
$fetch_name=mysql_fetch_array($exe_get_student_name);
$name_of_student=$fetch_name[0];
$number=$fetch_name[1];

//get class for that id
$get_class=
"SELECT class_index.class_name
FROM class_index
INNER JOIN class
ON class.classId = class_index.cId
WHERE sId=".$studentId."";
$exe_name_class=mysql_query($get_class);
$fetch_class_name=mysql_fetch_array($exe_name_class);


$class_name=$fetch_class_name['class_name'];
$mobile = '91'.$number;





//Query to get the date id of the current date from `dates_d` table

date_default_timezone_set('Asia/Kolkata');

$today = date("Y-m-d");
$date_message = date("jS F Y");

$message = "D/P, Your ward $name_of_student of class $class_name was absent on $date_message.";

$queryDateId = "
SELECT `date_id`
FROM `dates_d`
WHERE `date` = '$today'
";

$getDateId = mysql_query($queryDateId);
$dateId = mysql_fetch_array($getDateId);
$dateId = $dateId[0];

//Now put the values in the table to mark the student as absent

$dId = $dateId;
$comment = $absentReason;
$tId = $user_id;
$sId = $studentId;
if($priv == 1)
{
	$by_admin = 1;
}
else
{
	$by_admin = 0;
}



$queryInsertForAbsent = "
INSERT INTO `attendance_date_comment`
(`dId`,`comment`,`tId`,`sId`,`by_admin`)
VALUES
(".$dId.",'Leave',".$_SESSION['user_id'].",".$studentId.",$by_admin)
";
mysql_query($queryInsertForAbsent);


if(mysql_affected_rows() == '1')
{
	header("location:../admin_attendance_for_class.php?class_id=$class_id");
}
else
{
	//header("location:../notes_confirm.php?error_deleting_note");
}


?>
