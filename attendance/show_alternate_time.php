<?php
require_once('../include/functions_dashboard.php');
require_once('../config/config.php');
require_once('../include/session.php');
require_once('../include/check.php');
function return_day($day_code)
{
	//This function will return the day corresponding to the day Code passed as an argument
	switch($day_code)
	{
		case 0 : return 'Monday'; break;
		case 1 : return 'Tuesday'; break;
		case 2 : return 'Wednesday'; break;
		case 3 : return 'Thursday'; break;
		case 4 : return 'Friday'; break;
		case 5 : return 'Saturday'; break;		
	}
	
}
 
  
  $id_teacher=$_GET['arr_id'];
	
	//get class and name of teacher
	$get_details_teacher=
	"SELECT users.Name
	FROM users
	
	WHERE users.uId = ".$id_teacher."";
	$exe_details_teacher=mysql_query($get_details_teacher);
	$fetch_details_teacher=mysql_fetch_array($exe_details_teacher);
	$name_teacher=$fetch_details_teacher[0];
  
  echo '<h5 style="color:orange">Time table of teacher:'.$name_teacher.'( showing all lectures)</h5>';	
//get list of all the teachers to select alternate
	//get class and period in a time table format of the teacher and get time tabel of the teacher selected on on change
	// show absent teacher time table
	$query_get_time_slot = "
		SELECT `time_slot_id`, `time_slot_name`
		FROM `time_slots`
	";
	$execute_get_time_slot = mysql_query($query_get_time_slot);
	
	$day = 0;		//Set to zero as the week starts with Monday
	
	//Now Store all the time_slot ID and Name in an array
	$ctr_time_slot = 0;
	$array_time_slot_id = array();		//Array to store the time_slot_id
	$array_time_slot_name = array();	//Array to store the time_slot_name
	
	while($get_time_slot = mysql_fetch_array($execute_get_time_slot))
	{
		$array_time_slot_id[$ctr_time_slot] = $get_time_slot['time_slot_id'];
		$array_time_slot_name[$ctr_time_slot] = $get_time_slot['time_slot_name'];
		++$ctr_time_slot;
	}
	echo '   <table  class="table table-bordered table-striped">

		
		                              <thead>   
									  <tr>     
									  		<td>Day &darr;</td>';
											//Loop to get the time_slot_name in place
											for($i=0;$i<$ctr_time_slot;$i++)
											{
												echo '<td>'.$array_time_slot_name[$i].'</td>';
											}
										echo'</tr>     
										</thead> ';
																
										echo'	<tbody>';
                                        	//End of the header part
								
								//Now loop to generate the days and also get the details of the subject and teacher
								
								for($day=0;$day<=5;$day++)
								{
									//Loop will generate days, 0->Monday, 1-> Tuesday etc...
									echo '<tr id=\"drag\">';
									
										echo '<td>'.return_day($day).'</td>';
										
							//Loop to get the details from the time_table table for the corresponding day, time_slot_id, class_id
							
										for($ctr = 0;$ctr < $ctr_time_slot; $ctr++)
										{
											$query_get_details_time_table = "
												SELECT subject.subject, users.Name,users.uId,time_table.time_table_id,subject.subject_id,class_index.class_name,arrange_subId
												FROM subject
												
												INNER JOIN time_table
												ON time_table.subject_id = subject.subject_id
												
												INNER JOIN users
												ON users.uId = time_table.teacher_id
												
												INNER JOIN class_index
												ON class_index.cId = time_table.class_id
												
												 WHERE time_table.day = '$day' AND time_table.time_slot_id = '$array_time_slot_id[$ctr]' AND time_table.session_id = ".$_SESSION['current_session_id']."
												AND (time_table.teacher_id = ".$id_teacher." OR time_table.arrange_tId=".$id_teacher.")
											";
											
											$execute_get_details_time_table = mysql_query($query_get_details_time_table);
											$get_details_time_table = mysql_fetch_array($execute_get_details_time_table);
											$subject = $get_details_time_table[0];
											
											$teacher = $get_details_time_table[1];
											$time_table_id = $get_details_time_table[3];
											$sid = $get_details_time_table[4];
											$tid=$get_details_time_table[2];
											if($get_details_time_table[0]=="")
											{
												
											echo '<td>-</td>';	
												
											}
											else
											{
										 if(($tid == "")&&($sid == ""))
											{
												
												echo "<td id='val_".$day."_".$array_time_slot_id[$ctr]."' align=\"center\" onclick='edit_time_no_sub_tea(".$day.",".$array_time_slot_id[$ctr].");' style='cursor:pointer'><strong>$subject</strong><br>($teacher)</td>";	
											}
											else if($get_details_time_table['arrange_subId']!=0)
										{
											
										
											
										
										
												//fetch name of the sub
										    $fetch_subject=
											"SELECT subject
											FROM subject
											WHERE subject_id=".$get_details_time_table['arrange_subId']."";
											$exe_sub=mysql_query($fetch_subject);
											$fetch_name_exe_sub=mysql_fetch_array($exe_sub);
										
										
											
										
										
											echo '
										<td><b>Arrange Lecture</b><br>
										<small style="color:green"><b>('.$fetch_name_exe_sub[0].')<br>('.$get_details_time_table['class_name'].')</small></td>';	
										}
											
										
											else
											{
										
												
											
											echo "<td id='val_".$day."_".$array_time_slot_id[$ctr]."' align=\"center\" onclick='edit_time(".$day.",".$array_time_slot_id[$ctr].",".$tid.",".$sid.",".$time_table_id.");' style='cursor:pointer'><small><b><label style='color:green'>".$get_details_time_table['class_name']."-</label></b><br></small><strong>$subject</strong><br>($teacher)<br></td>";
											
											}
											
											}
										}
									echo '</tr>';
								}

                         			echo'</tbody>	
									
		
									</table>';


?>