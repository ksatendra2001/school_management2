<?php

/* While sending alerts check whether the user is an admin or a normal user as an Admin will mark the
	student absent for the whole day, whereas a teacher will mark him absent for a particular class. Send 
	alert message accordingly.
*/

	require_once('../include/functions_dashboard.php');
	require_once('../config/config.php');
	require_once('../include/session.php');
	require_once('../include/check.php');
	require_once('../include/userdetail.php');
	
	$teacher_id = $_GET['teacher_id'];
	$absentReason = $_GET['reason'];
	
	date_default_timezone_set('Asia/Kolkata');
	
	$today = date("Y-m-d");
	$queryDateId = "
		SELECT `date_id`
		FROM `dates_d`
		WHERE `date` = '$today'
		";
		
	$getDateId = mysql_query($queryDateId);
	$dateId = mysql_fetch_array($getDateId);
	$dateId = $dateId[0];
	
	//Now put the values in the table to mark the student as absent
	
	$dId = $dateId;
	$comment = $absentReason;
	
	if($priv == 1)
		{
		$by_admin = 1;
		}
	else
		{
		$by_admin = 0;
		}
	
	
	
	  $queryInsertForAbsent = "
		INSERT INTO `teacher_attendence_date` 
		(`tid`,`did`,`comment`,`by_admin`)
		VALUES(".$teacher_id.",'".$dId."','".$comment."',".$by_admin.")
		";
	$exe=mysql_query($queryInsertForAbsent);
	if(mysql_affected_rows()=='1')
	{
		//Student marked absent successfully
		echo '1';
	}
	else
	{
		echo '0';
	}

?>
